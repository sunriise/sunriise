package com.hungle.money.vaadin;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan("com.hungle.money.component,com.hungle.money.vaadin")
public class Application {
    private static final Logger LOGGER = LogManager.getLogger(Application.class);

    public static void main(String[] args) {
        // http://localhost:8080/api/msmoney/accounts

        SpringApplication app = null;
        try {
            app = new SpringApplication(Application.class);
            app.run(args);
        } finally {
            if (app != null) {
                app = null;
            }
        }

    }

    public static final Path findMnyFile(Path targetDir, String suffix) throws IOException {
        return Files.list(targetDir).filter((p) -> {
            if (Files.isRegularFile(p)) {
                return p.getFileName().toString().endsWith(suffix);
            } else {
                return false;
            }
        }).findFirst().orElse(null);
    }

    public static final String argsGetPassword(ApplicationArguments args) {
        String password = null;
        if (args.containsOption("password")) {
            List<String> values = args.getOptionValues("password");
            if (values.size() > 0) {
                password = values.get(0);
            }
        }
        return password;
    }

    public static final String argsGetFileName(ApplicationArguments args) {
        String fileName = null;
        if (args.containsOption("file")) {
            List<String> values = args.getOptionValues("file");
            if (values.size() > 0) {
                fileName = values.get(0);
            }
        }
        return fileName;
    }
}

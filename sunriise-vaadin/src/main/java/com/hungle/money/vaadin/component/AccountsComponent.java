package com.hungle.money.vaadin.component;

import java.io.IOException;
import java.util.List;

import javax.annotation.PostConstruct;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;

import com.hungle.money.component.AccountFilterArg;
import com.hungle.money.component.AccountFilterArgBuilder;
import com.hungle.money.component.MnyFilterFactory;
import com.hungle.money.component.MsMoneyModelInterface;
import com.hungle.sunriise.mnyobject.Account;
import com.hungle.sunriise.util.AndMnyFilter;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.value.ValueChangeMode;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.spring.annotation.SpringComponent;

@SpringComponent
@Route("accounts")
public class AccountsComponent extends VerticalLayout {
    private static final Logger LOGGER = LogManager.getLogger(AccountsComponent.class);

    @Autowired
    private MsMoneyModelInterface model;

    private Grid<Account> grid = new Grid<>(Account.class);
    private TextField filterText = new TextField();

    public AccountsComponent() {
        grid.setColumns("id", "name", "accountType");
        grid.addItemClickListener(event -> Notification.show("Value: " + event.getItem()));

        filterText.setPlaceholder("Filter by name...");
        filterText.setClearButtonVisible(true);
        filterText.setValueChangeMode(ValueChangeMode.EAGER);
        filterText.addValueChangeListener(e -> {
            try {
                updateList();
            } catch (IOException e1) {
                LOGGER.error(e1.getMessage(), e1);
            }
        });

        add(filterText, grid);

        setSizeFull();

    }

    @PostConstruct
    private void postConstruct() throws IOException {
        updateList();
    }

    private void updateList() throws IOException {
        String name = filterText.getValue();
        List<Account> accounts;
        if (StringUtils.hasText(name)) {
            AccountFilterArg arg = new AccountFilterArgBuilder().setName(name).build();
            AndMnyFilter<Account> filter = MnyFilterFactory.createAndAccountFilter(arg);
            accounts = model.getAccounts(filter);
        } else {
            accounts = model.getAccounts();
        }
        grid.setItems(accounts);
    }
}
package com.hungle.sunriise.accountviewer.spring;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.hungle.sunriise.accountviewer.AccountViewerApp;

@Configuration
public class AccountViewerConfiguration {
    @Bean
    public AccountViewerHolder accountViewerHolder() {
        return new AccountViewerHolder(AccountViewerApp.getInstance());
    }
}

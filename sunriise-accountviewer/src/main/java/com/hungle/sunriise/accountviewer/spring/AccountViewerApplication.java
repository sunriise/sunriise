package com.hungle.sunriise.accountviewer.spring;

import org.apache.logging.log4j.Logger;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

@SpringBootApplication(exclude = {
//        MongoAutoConfiguration.class, 
//        MongoDataAutoConfiguration.class,
//        HibernateJpaAutoConfiguration.class 
})
public class AccountViewerApplication {
    private static final Logger LOGGER = org.apache.logging.log4j.LogManager.getLogger(AccountViewerApplication.class);

    public static String[] ARGS = null;

    public static void main(String[] args) {
        AccountViewerApplication.ARGS = args;

        SpringApplication app = null;
        try {
            LOGGER.info("> SpringApplication.run() STARTED");
            app = new SpringApplication(AccountViewerApplication.class);
            app.run(args);

        } finally {
            LOGGER.info("< SpringApplication.run() ENDED");
            if (app != null) {
                app = null;
            }
        }
    }

    public static ConfigurableApplicationContext run(String[] args) {
        SpringApplication app = new SpringApplication(AccountViewerApplication.class);
        ConfigurableApplicationContext configurableApplicationContext = app.run(args);
        return configurableApplicationContext;
    }
}

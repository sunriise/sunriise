package com.hungle.sunriise.accountviewer.spring;

import org.springframework.data.mongodb.repository.MongoRepository;

public interface TransactionRepository extends MongoRepository<Transaction, Integer> {
    // List<Account> findByName(@Param("name") String name);

    // List<Account> findByAccountType(@Param("accountType") String
    // accountType);
}

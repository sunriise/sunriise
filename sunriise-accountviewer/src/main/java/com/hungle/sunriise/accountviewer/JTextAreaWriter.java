package com.hungle.sunriise.accountviewer;

import java.io.IOException;
import java.io.Writer;

import javax.swing.JTextArea;

// TODO: Auto-generated Javadoc
/**
 * The Class JTextAreaWriter.
 */
public class JTextAreaWriter extends Writer {

    /** The text area. */
    private JTextArea textArea;

    /** The max count. */
    private long maxCount = -1L;

    /** The count. */
    private long count = 0L;

    /**
     * Instantiates a new j text area writer.
     *
     * @param textArea the text area
     * @param maxCount the max count
     */
    public JTextAreaWriter(JTextArea textArea, long maxCount) {
        super();
        this.textArea = textArea;
        this.maxCount = maxCount;
    }

    /**
     * Instantiates a new j text area writer.
     *
     * @param textArea the text area
     */
    public JTextAreaWriter(JTextArea textArea) {
        this(textArea, -1L);
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.io.Writer#write(char[], int, int)
     */
    @Override
    public void write(char[] cbuf, int off, int len) throws IOException {
        if (textArea == null) {
            return;
        }

        String str = new String(cbuf, off, len);
        count += str.length();

        if ((maxCount > 0) && (count < maxCount)) {
            textArea.append(str);
        } else {
            textArea.append(str);
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.io.Writer#flush()
     */
    @Override
    public void flush() throws IOException {
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.io.Writer#close()
     */
    @Override
    public void close() throws IOException {
    }

}

package com.hungle.sunriise.accountviewer.spring;

import com.hungle.sunriise.accountviewer.AccountViewerApp;

public class AccountViewerHolder {
    private final AccountViewerApp viewer;

    public AccountViewerHolder(AccountViewerApp viewer) {
        this.viewer = viewer;
    }

    public AccountViewerApp getViewer() {
        return viewer;
    }
}

package com.hungle.sunriise.accountviewer.spring;

public class AccountNotFoundException extends RuntimeException {
    AccountNotFoundException(Integer id) {
        super("Could not find account " + id);
    }
}

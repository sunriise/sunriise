package com.hungle.sunriise.accountviewer.spring;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.repository.query.Param;

//@RepositoryRestResource(collectionResourceRel = "accounts", path = "accounts")
public interface AccountRepository extends MongoRepository<Account, Integer> {
    List<Account> findByName(@Param("name") String name);

    List<Account> findByAccountType(@Param("accountType") String accountType);
}

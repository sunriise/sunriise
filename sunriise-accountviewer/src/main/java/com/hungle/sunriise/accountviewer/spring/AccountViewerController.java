package com.hungle.sunriise.accountviewer.spring;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import com.healthmarketscience.jackcess.Database;
import com.hungle.sunriise.accountviewer.AccountViewerApp;
import com.hungle.sunriise.dbutil.TableAccountUtils;
import com.hungle.sunriise.io.MnyDb;
import com.hungle.sunriise.mnyobject.Account;
import com.hungle.sunriise.mnyobject.MnyContext;
import com.hungle.sunriise.mnyobject.Transaction;

//@RestController
//@RequestMapping("v0")
public class AccountViewerController {
    @Autowired
    private AccountViewerHolder accountViewerHolder;

    @GetMapping(value = "/accounts")
    List<Account> all() {
        List<Account> accounts = new ArrayList<>();

        MnyContext mnyContext = getMnyContext();

        accounts.addAll(mnyContext.getAccounts().values());

        return accounts;
    }

    @GetMapping("/accounts/{id}")
    Account one(@PathVariable Integer id) {
        MnyContext mnyContext = getMnyContext();
        Map<Integer, Account> accounts = mnyContext.getAccounts();
        Account account = accounts.get(id);
        if (account == null) {
            throw new AccountNotFoundException(id);
        }

        return account;
    }

    @GetMapping("/accounts/{id}/transactions")
    List<Transaction> allTransactions(@PathVariable Integer id) throws IOException {
        MnyContext mnyContext = getMnyContext();
        Map<Integer, Account> accounts = mnyContext.getAccounts();
        Account account = accounts.get(id);
        if (account == null) {
            throw new AccountNotFoundException(id);
        }
        TableAccountUtils.addTransactionsToAccount(account, getMnyContext());
        return account.getTransactions();
    }

    @GetMapping("/accounts/{id}/transactions/{tid}")
    Transaction oneTransaction(@PathVariable Integer id, @PathVariable Integer tid) throws IOException {
        MnyContext mnyContext = getMnyContext();
        Map<Integer, Account> accounts = mnyContext.getAccounts();
        Account account = accounts.get(id);
        if (account == null) {
            throw new AccountNotFoundException(id);
        }
        TableAccountUtils.addTransactionsToAccount(account, getMnyContext());
        List<Transaction> transactions = account.getTransactions();
        Transaction t = null;
        for (Transaction transaction : transactions) {
            if (transaction.getId().compareTo(tid) == 0) {
                t = transaction;
                break;
            }
        }
        if (t == null) {
            throw new TransactionNotFoundException(id, tid);
        }

        return t;
    }

    private MnyContext getMnyContext() {
        MnyContext mnyContext = null;
        AccountViewerApp viewer = accountViewerHolder.getViewer();
        if (viewer != null) {
            MnyDb mnyDb = viewer.getMnyDb();
            if (mnyDb != null) {
                Database db = mnyDb.getDb();
                if (db != null) {
                    mnyContext = viewer.getMnyContext();
                }
            }
        }
        if (mnyContext == null) {
            throw new IllegalStateException("mnyContext is null");
        }
        return mnyContext;
    }
}

package com.hungle.sunriise.accountviewer;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

import com.hungle.sunriise.mnyobject.Account;

// TODO: Auto-generated Javadoc
/**
 * The Class FormatterUtil.
 */
public class FormatterUtils {

    static final String CURRENCY_CODE_US = "USD";

    static final String CURRENCY_CODE_CHINA = "CNY";

    static final String CURRENCY_CODE_JAPAN = "JPY";

    static final String CURRENCY_CODE_CANADA = "CAD";

    static final String CURRENCY_CODE_UK = "GBP";

    /** The Constant amountFormatters. */
    private static final Map<String, NumberFormat> amountFormatters = new HashMap<String, NumberFormat>();

    /** The Constant defaultAmountFormatter. */
    private static final NumberFormat defaultAmountFormatter = createDefaultAmountFormatter();

    /**
     * Formatter used to format security quantity.
     */
    private static final NumberFormat securityQuantityFormatter = createDefaultSecurityQuantityFormatter();

    /**
     * Creates the amount formatter.
     *
     * @param currencyCode the currency code
     * @param javaCurrency the java currency
     * @return the number format
     */
    private static NumberFormat createAmountFormatter(String currencyCode, java.util.Currency javaCurrency) {
        NumberFormat nf;

        Locale currencyLocale = getCurrencyLocale(currencyCode);
        if (currencyLocale != null) {
            nf = NumberFormat.getCurrencyInstance(currencyLocale);
        } else {
            nf = NumberFormat.getCurrencyInstance();
        }

        nf.setCurrency(javaCurrency);

        return nf;
    }

    /**
     * Creates the amount formatter.
     *
     * @return the number format
     */
    private static final NumberFormat createDefaultAmountFormatter() {
        return NumberFormat.getCurrencyInstance();
    }

    /**
     * Creates the security quantity formatter.
     *
     * @return the number format
     */
    private static final NumberFormat createDefaultSecurityQuantityFormatter() {
        NumberFormat nf = NumberFormat.getIntegerInstance();
        nf.setGroupingUsed(true);
        if (nf instanceof DecimalFormat) {
            DecimalFormat df = (DecimalFormat) nf;
            df.setMinimumFractionDigits(8);
            df.setMaximumFractionDigits(8);
        }
        return nf;
    }

    /**
     * Format ammount.
     *
     * @param account             the account
     * @param amount              the amount
     * @param defaultCurrencyCode TODO
     * @return the string
     */
    static final String formatAmount(Account account, BigDecimal amount, String defaultCurrencyCode) {
        String currencyCode = account.getCurrency().getIsoCode();
        if (StringUtils.isEmpty(currencyCode)) {
            currencyCode = defaultCurrencyCode;
        }
        NumberFormat nf = FormatterUtils.getCurrencyCodeFormatter(currencyCode, amountFormatters);
        return nf.format(amount);
    }

    public static String formatAmount(Account account, BigDecimal bigDecimal) {
        String defaultCurrencyCode = null;
        return formatAmount(account, bigDecimal, defaultCurrencyCode);
    }

    /**
     * Format security quantity.
     *
     * @param quantity the quantity
     * @return the string
     */
    static final String formatSecurityQuantity(Double quantity) {
        return securityQuantityFormatter.format(quantity);
    }

    /**
     * Sets the currency code.
     *
     * @param currencyCode     the new currency code
     * @param amountFormatters the amount formatters
     * @return the currency code formatter
     */
    private static final NumberFormat getCurrencyCodeFormatter(String currencyCode,
            Map<String, NumberFormat> amountFormatters) {
        NumberFormat nf = null;

        if (currencyCode == null) {
            nf = defaultAmountFormatter;
        } else {
            // do we have a formatter for this currencyCode?
            NumberFormat cachedNf = amountFormatters.get(currencyCode);
            if (cachedNf != null) {
                // yes, has formatter
                nf = cachedNf;
            } else {
                // no
                java.util.Currency javaCurrency = java.util.Currency.getInstance(currencyCode);
                if (javaCurrency != null) {
                    cachedNf = createAmountFormatter(currencyCode, javaCurrency);
                } else {
                    cachedNf = defaultAmountFormatter;
                }
                amountFormatters.put(currencyCode, cachedNf);
                nf = cachedNf;
            }
        }

        return nf;
    }

    /**
     * Gets the currency locale.
     *
     * @param currencyCode the currency code
     * @return the currency locale
     */
    private static final Locale getCurrencyLocale(String currencyCode) {
        Locale currencyLocale = null;

        if (currencyCode.equals(CURRENCY_CODE_US)) {
            currencyLocale = Locale.US;
        } else if (currencyCode.equals(CURRENCY_CODE_UK)) {
            currencyLocale = Locale.UK;
        } else if (currencyCode.equals(CURRENCY_CODE_CANADA)) {
            currencyLocale = Locale.CANADA;
        } else if (currencyCode.equals(CURRENCY_CODE_JAPAN)) {
            currencyLocale = Locale.JAPAN;
        } else if (currencyCode.equals(CURRENCY_CODE_CHINA)) {
            currencyLocale = Locale.CHINA;
        } else {
            currencyLocale = null;
        }

        return currencyLocale;
    }
}

package com.hungle.sunriise.accountviewer;

import java.io.IOException;

import com.hungle.sunriise.io.MnyDb;
import com.hungle.sunriise.mnyobject.Account;
import com.hungle.sunriise.mnyobject.Transaction;

public interface AccountViewerListener {

    void mnyDbSelected(final MnyDb newMnyDb);

    void accountSelected(final Account account) throws IOException;

    void transactionSelected(final Transaction transaction);

}
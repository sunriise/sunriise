/*******************************************************************************
 * Copyright (c) 2010 Hung Le
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *******************************************************************************/
package com.hungle.sunriise.accountviewer;

import java.math.BigDecimal;
import java.sql.Date;
import java.util.List;

import com.hungle.sunriise.mnyobject.Account;
import com.hungle.sunriise.mnyobject.Transaction;

// TODO: Auto-generated Javadoc
/**
 * The Class DefaultAccountViewerTableModel.
 */
public class DefaultAccountViewerTableModel extends AbstractAccountViewerTableModel {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 1L;

    /** The Constant COLUMN_ID. */
    private static final int COLUMN_ID = 0;

    /** The Constant COLUMN_DATE. */
    private static final int COLUMN_DATE = COLUMN_ID + 1;

    /** The Constant COLUMN_PAYEE. */
    private static final int COLUMN_PAYEE = COLUMN_DATE + 1;

    /** The Constant COLUMN_CATEGORY. */
    private static final int COLUMN_CATEGORY = COLUMN_PAYEE + 1;

    /** The Constant COLUMN_AMOUNT. */
    // private static final int COLUMN_CLASSIFICATION = 4;
    private static final int COLUMN_AMOUNT = COLUMN_CATEGORY + 1;

    /** The Constant COLUMN_BALANCE. */
    private static final int COLUMN_BALANCE = COLUMN_AMOUNT + 1;

    /** The Constant COLUMN_VOIDED. */
    private static final int COLUMN_VOIDED = COLUMN_BALANCE + 1;

    /**
     * Instantiates a new default account viewer table model.
     *
     * @param account the account
     */
    public DefaultAccountViewerTableModel(Account account) {
        super(account);
    }

    /*
     * (non-Javadoc)
     * 
     * @see javax.swing.table.TableModel#getRowCount()
     */
    @Override
    public int getRowCount() {
        if (getAccount() != null) {
            return getAccount().getTransactions().size();
        } else {
            return 0;
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see javax.swing.table.TableModel#getColumnCount()
     */
    @Override
    public int getColumnCount() {
        return 7;
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
        Class<?> value = null;

        switch (columnIndex) {
        case COLUMN_ID:
            value = Integer.class;
            break;
        case COLUMN_DATE:
            value = Date.class;
            break;
        case COLUMN_PAYEE:
            value = String.class;
            break;
        case COLUMN_CATEGORY:
            value = String.class;
            break;
        // case COLUMN_CLASSIFICATION:
        // value = "classification";
        // break;
        case COLUMN_AMOUNT:
            value = BigDecimal.class;
            break;
        case COLUMN_BALANCE:
            value = BigDecimal.class;
            break;
        case COLUMN_VOIDED:
            value = Boolean.class;
            break;

        default:
            value = null;
            break;
        }

        return value;
    }

    /*
     * (non-Javadoc)
     * 
     * @see javax.swing.table.TableModel#getValueAt(int, int)
     */
    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Object value = null;
        List<Transaction> transactions = getAccount().getTransactions();
        Transaction transaction = transactions.get(rowIndex);
        switch (columnIndex) {
        case COLUMN_ID:
            value = transaction.getId();
            break;
        case COLUMN_DATE:
            value = transaction.getDate();
            break;
        case COLUMN_PAYEE:
            value = transaction.getPayee().getName();
            break;
        case COLUMN_CATEGORY:
            value = transaction.getCategory().getFullName();
            break;
        // case COLUMN_CLASSIFICATION:
        // value = "classification";
        // break;
        case COLUMN_AMOUNT:
            value = transaction.getAmount();
            break;
        case COLUMN_BALANCE:
//            value = CalculateMonthlySummary.getRunningBalance(rowIndex, getAccount());
            value = transaction.getRunningBalance();
            break;
        case COLUMN_VOIDED:
            value = transaction.isVoid();
            break;

        default:
            value = null;
            break;
        }

        return value;
    }

    /*
     * (non-Javadoc)
     * 
     * @see javax.swing.table.AbstractTableModel#getColumnName(int)
     */
    @Override
    public String getColumnName(int column) {
        String value = null;
        switch (column) {
        case COLUMN_ID:
            value = "ID";
            break;
        case COLUMN_DATE:
            value = "Date";
            break;
        case COLUMN_PAYEE:
            value = "Payee";
            break;
        case COLUMN_CATEGORY:
            value = "Category";
            break;
        // case COLUMN_CLASSIFICATION:
        // value = "Classification";
        // break;
        case COLUMN_AMOUNT:
            value = "Amount";
            break;
        case COLUMN_BALANCE:
            value = "Balance";
            break;
        case COLUMN_VOIDED:
            value = "Voided";
            break;
        default:
            value = null;
            break;
        }

        return value;
    }
}
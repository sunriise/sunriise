package com.hungle.sunriise.accountviewer.spring;

import java.math.BigDecimal;
import java.util.ArrayList;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Document(collection = "transactions")
@JsonIgnoreProperties(ignoreUnknown = true)
public class Transaction {
    @Id
    private Integer id;

    private String number;

    private BigDecimal amount;

    @Field("account.name")
    private String accountName;

    @Field("account._id")
    private Integer accountId;

    private String date;

    private String memo;

    private ArrayList<TransactionSplit> splits;

    private Payee payee;

    @Field("category.fullName")
    private String category;

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public String getAccountName() {
        return accountName;
    }

    public void setAccountName(String accountName) {
        this.accountName = accountName;
    }

    public Integer getAccountId() {
        return accountId;
    }

    public void setAccountId(Integer accountId) {
        this.accountId = accountId;
    }

    public Integer getTid() {
        return id;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getMemo() {
        return memo;
    }

    public void setMemo(String memo) {
        this.memo = memo;
    }

    public ArrayList<TransactionSplit> getSplits() {
        return splits;
    }

    public void setSplits(ArrayList<TransactionSplit> splits) {
        this.splits = splits;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public Payee getPayee() {
        return payee;
    }

    public void setPayee(Payee payeeInfo) {
        this.payee = payeeInfo;
    }
}

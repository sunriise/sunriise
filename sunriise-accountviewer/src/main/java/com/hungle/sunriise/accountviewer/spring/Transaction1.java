package com.hungle.sunriise.accountviewer.spring;

import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Document(collection = "transactions")
@JsonIgnoreProperties(ignoreUnknown = true)
public class Transaction1 extends com.hungle.sunriise.mnyobject.impl.DefaultTransaction {
}

package com.hungle.sunriise.accountviewer.spring;

import java.io.File;
import java.io.IOException;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import de.flapdoodle.embed.mongo.config.IMongodConfig;
import de.flapdoodle.embed.mongo.config.MongodConfigBuilder;
import de.flapdoodle.embed.mongo.config.Net;
import de.flapdoodle.embed.mongo.config.Storage;
import de.flapdoodle.embed.mongo.distribution.Version;
import de.flapdoodle.embed.process.runtime.Network;

// https://docs.spring.io/spring-boot/docs/current/reference/html/common-application-properties.html
// https://docs.spring.io/spring-boot/docs/current/reference/html/boot-features-nosql.html#boot-features-mongo-embedded
@Configuration
public class MongoDbConfiguration {
    private static final String DEFAULT_BIND_IP = "127.0.0.1";
//    public static final int DEFAULT_PORT = 37017;

    // spring.data.mongodb.port
    @Value("${spring.data.mongodb.port:37017}")
    private int port;

    @Bean
    public IMongodConfig getMongodConfig() throws IOException {
        File dir = new File(new File(".sunriise"), "databaseDir");
        String databaseDir = dir.getAbsolutePath();
        Storage replication = new Storage(databaseDir, null, 0);

        String bindIp = DEFAULT_BIND_IP;

        Net net = new Net(bindIp, port, Network.localhostIsIPv6());

        IMongodConfig mongodConfig = new MongodConfigBuilder().version(Version.Main.PRODUCTION).replication(replication)
                .net(net).build();

        return mongodConfig;
    }
}

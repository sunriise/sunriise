/*******************************************************************************
 * Copyright (c) 2010 Hung Le
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *******************************************************************************/
package com.hungle.sunriise.accountviewer;

import java.util.List;

import com.hungle.sunriise.mnyobject.Account;
import com.hungle.sunriise.mnyobject.InvestmentInfo;
import com.hungle.sunriise.mnyobject.InvestmentTransaction;
import com.hungle.sunriise.mnyobject.Security;
import com.hungle.sunriise.mnyobject.Transaction;

// TODO: Auto-generated Javadoc
/**
 * The Class InvestmentTableModel.
 */
public class InvestmentTableModel extends DefaultAccountViewerTableModel {

    private static final int COLUMN_NAME_ACTIVITY = 2;
    private static final int COLUMN_NAME_INVESTMENT = 3;
    private static final int COLUMN_NAME_QUANTITY = 5;
    private static final int COLUMN_NAME_PRICE = 6;
    private static final int COLUMN_NAME_VOIDED = 7;

    /**
     * Instantiates a new investment table model.
     *
     * @param account the account
     */
    public InvestmentTableModel(Account account) {
        super(account);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.hungle.sunriise.accountviewer.DefaultAccountViewerTableModel#
     * getColumnCount()
     */
    @Override
    public int getColumnCount() {
        return super.getColumnCount() + 1;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.hungle.sunriise.accountviewer.DefaultAccountViewerTableModel#
     * getValueAt(int, int)
     */
    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Object value = super.getValueAt(rowIndex, columnIndex);

        List<Transaction> transactions = getAccount().getTransactions();
        Transaction transaction = transactions.get(rowIndex);
        InvestmentTransaction investmentTransaction = null;
        if (transaction.isInvestment()) {
            InvestmentInfo investmentInfo = transaction.getInvestmentInfo();
            if (investmentInfo != null) {
                investmentTransaction = investmentInfo.getTransaction();
            }
        }
        switch (columnIndex) {
        case COLUMN_NAME_ACTIVITY:
            if (investmentTransaction != null) {
                value = transaction.getInvestmentInfo().getActivity().getLabel();
            } else {
                value = null;
            }
            break;
        case COLUMN_NAME_INVESTMENT:
            if (investmentTransaction != null) {
                Security security = transaction.getInvestmentInfo().getSecurity();
                if (security != null) {
                    String symbol = security.getSymbol();
                    if (symbol == null) {
                        symbol = security.getName();
                    }
                    value = symbol;
                }
            } else {
                value = null;
            }
            break;
        case COLUMN_NAME_QUANTITY:
            if (investmentTransaction != null) {
                value = investmentTransaction.getQuantity();
            } else {
                value = null;
            }
            break;
        case COLUMN_NAME_PRICE:
            if (investmentTransaction != null) {
                value = investmentTransaction.getPrice();
            } else {
                value = null;
            }
            break;
        case COLUMN_NAME_VOIDED:
            value = transaction.isVoid();
            break;
        }
        return value;
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
        Class<?> value = super.getColumnClass(columnIndex);
        switch (columnIndex) {
        case COLUMN_NAME_ACTIVITY:
            value = String.class;
            break;
        case COLUMN_NAME_QUANTITY:
            value = Double.class;
            break;
        case COLUMN_NAME_PRICE:
            value = Double.class;
            break;
        case COLUMN_NAME_VOIDED:
            value = Boolean.class;
            break;
        }
        return value;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.hungle.sunriise.accountviewer.DefaultAccountViewerTableModel#
     * getColumnName(int)
     */
    @Override
    public String getColumnName(int column) {
        String columnName = super.getColumnName(column);

        switch (column) {
        case COLUMN_NAME_ACTIVITY:
            columnName = "Activity";
            break;
        case COLUMN_NAME_INVESTMENT:
            columnName = "Investment";
            break;
        case COLUMN_NAME_QUANTITY:
            columnName = "Quantity";
            break;
        case COLUMN_NAME_PRICE:
            columnName = "Price";
            break;
        case COLUMN_NAME_VOIDED:
            columnName = "Voided";
            break;
        }
        return columnName;
    }
}
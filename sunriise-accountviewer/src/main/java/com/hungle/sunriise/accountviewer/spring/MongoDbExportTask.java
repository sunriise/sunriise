package com.hungle.sunriise.accountviewer.spring;

import java.io.File;
import java.io.IOException;

import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.web.client.RestTemplate;

import com.hungle.sunriise.export.mongodb.ExportToMongoDb;
import com.hungle.sunriise.export.mongodb.ExportToMongoDbCmd;
import com.hungle.sunriise.io.FileUtils;
import com.hungle.sunriise.io.sample.MnySampleFile;
import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;

@Component
public class MongoDbExportTask {
    private static final Logger LOGGER = org.apache.logging.log4j.LogManager.getLogger(MongoDbExportTask.class);

    // spring.data.mongodb.port
    @Value("${spring.data.mongodb.port:37017}")
    private int mongoDbPort;

    // server.port
    @Value("${server.port:8080}")
    private int serverPort;

    @Value("${mny.fileName}")
    private String mnyFileName;

    @Value("${mny.password}")
    private String mnyPassword;

    @EventListener(ApplicationReadyEvent.class)
    public void doSomethingAfterStartup() {
        LOGGER.info("Spring is ready!");

        RestTemplate restTemplate = new RestTemplate();
        String resourceUrl = "http://localhost:" + serverPort + "/hello/";
        ResponseEntity<String> response = restTemplate.getForEntity(resourceUrl, String.class);
        LOGGER.info("REST response=" + response);

        String mongoClientUrl = "mongodb://" + "localhost" + ":" + mongoDbPort;
        boolean exportToMongoDb = false;
        if (exportToMongoDb) {
            exportToMongoDb(mongoClientUrl);
        }

        LOGGER.info("HTTP server is listening at " + resourceUrl);
        LOGGER.info("MongoDb server is listening at " + mongoClientUrl);
        try (MongoClient mongoClient = new MongoClient("localhost", mongoDbPort)) {
            for (String databaseName : mongoClient.listDatabaseNames()) {
                LOGGER.info("databaseName=" + databaseName);
            }
        }
    }

    private void exportToMongoDb(String mongoClientUrl) {
        try {
            File dbFile = null;
            if (!StringUtils.isEmpty(mnyFileName)) {
                if (mnyFileName.compareToIgnoreCase("sample") == 0) {
                    dbFile = FileUtils.getSunsetSampleFile();
                    mnyPassword = MnySampleFile.SUNSET_SAMPLE_PWD_MNY_PASSWORD;
                } else {
                    dbFile = new File(mnyFileName);
                }
            } else {
                dbFile = null;
            }

            if ((dbFile != null) && (dbFile.exists())) {
                MongoClientURI mongoClientURI = ExportToMongoDb.createMongoClientURI(mongoClientUrl);
                ExportToMongoDbCmd.export(dbFile, mnyPassword, mongoClientURI);
                LOGGER.info("Exported to dbName=" + ExportToMongoDb.DEFAULT_DATABASE_NAME);
            }
        } catch (IOException e) {
            LOGGER.error(e.getMessage(), e);
        }
    }

}

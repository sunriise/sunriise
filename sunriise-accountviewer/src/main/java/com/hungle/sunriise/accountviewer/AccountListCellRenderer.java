package com.hungle.sunriise.accountviewer;

import java.awt.Component;

import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.ListCellRenderer;

import com.hungle.sunriise.mnyobject.Account;

public class AccountListCellRenderer extends JLabel implements ListCellRenderer {

    public AccountListCellRenderer() {
        super();
        setOpaque(true);
    }

    @Override
    public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected,
            boolean cellHasFocus) {
//        String code = country.getCode();
//        ImageIcon imageIcon = new ImageIcon(getClass().getResource("/images/" + code + ".png"));

        Account account = (Account) value;
//        setIcon(imageIcon);
        StringBuilder sb = new StringBuilder();
        sb.append(account.getName());
        sb.append(" / ");
        sb.append(account.getAccountType());
        setText(sb.toString());

        if (isSelected) {
            setBackground(list.getSelectionBackground());
            setForeground(list.getSelectionForeground());
        } else {
            setBackground(list.getBackground());
            setForeground(list.getForeground());
        }

        return this;
    }

}

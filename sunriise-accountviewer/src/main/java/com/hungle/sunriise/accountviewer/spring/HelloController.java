package com.hungle.sunriise.accountviewer.spring;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloController {
    @Autowired
    private AccountRepository accountRepository;

    @GetMapping(value = "/hello")
    String hello() {
        return "Hello";
    }

    @GetMapping(value = "/all")
    List<Account> all() {
        return accountRepository.findAll();
    }

}

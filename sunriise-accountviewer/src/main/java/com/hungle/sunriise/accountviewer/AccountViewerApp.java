/*******************************************************************************
 * Copyright (c) 2010 Hung Le
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *******************************************************************************/
package com.hungle.sunriise.accountviewer;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.io.IOException;
import java.io.Writer;
import java.math.BigDecimal;
import java.util.BitSet;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.prefs.Preferences;

import javax.swing.AbstractAction;
import javax.swing.BorderFactory;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.ListCellRenderer;
import javax.swing.ListSelectionModel;
import javax.swing.RowSorter;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import javax.swing.table.TableModel;
import javax.swing.table.TableRowSorter;

import org.apache.logging.log4j.Logger;
import org.fife.ui.rsyntaxtextarea.RSyntaxDocument;
import org.fife.ui.rsyntaxtextarea.RSyntaxTextArea;
import org.fife.ui.rsyntaxtextarea.SyntaxConstants;
import org.fife.ui.rtextarea.RTextScrollPane;
import org.jdesktop.beansbinding.AutoBinding;
import org.jdesktop.beansbinding.AutoBinding.UpdateStrategy;
import org.jdesktop.beansbinding.BeanProperty;
import org.jdesktop.beansbinding.Bindings;
import org.jdesktop.beansbinding.ELProperty;
import org.jdesktop.swingbinding.JListBinding;
import org.jdesktop.swingbinding.SwingBindings;

import com.healthmarketscience.jackcess.Row;
import com.hungle.sunriise.accountviewer.model.AccountViewerDataModel;
import com.hungle.sunriise.accountviewer.spring.AccountViewerApplication;
import com.hungle.sunriise.dbutil.CalculateMonthlySummary;
import com.hungle.sunriise.dbutil.CalculateMonthlySummary.SummaryEntry;
import com.hungle.sunriise.dbutil.TableAccountUtils;
import com.hungle.sunriise.dbutil.TableCurrencyUtils;
import com.hungle.sunriise.dbutil.TableTransactionUtils;
import com.hungle.sunriise.diskusage.DiskUsageView;
import com.hungle.sunriise.export.cvs2.Csv2Utils;
import com.hungle.sunriise.export.qif.TransactionMapper;
import com.hungle.sunriise.export.ui.ExportToContext;
import com.hungle.sunriise.export.ui.ExportToCsv2Action;
import com.hungle.sunriise.export.ui.ExportToJSONAction;
import com.hungle.sunriise.gui.AbstractSamplesMenu;
import com.hungle.sunriise.io.MnyDb;
import com.hungle.sunriise.json.JSONUtils;
import com.hungle.sunriise.mnyobject.Account;
import com.hungle.sunriise.mnyobject.EnumAccountType;
import com.hungle.sunriise.mnyobject.MnyContext;
import com.hungle.sunriise.mnyobject.Security;
import com.hungle.sunriise.mnyobject.SecurityHolding;
import com.hungle.sunriise.mnyobject.Transaction;
import com.hungle.sunriise.mnyobject.XferInfo;
import com.hungle.sunriise.util.JavaInfo;
import com.hungle.sunriise.util.MnyContextUtils;
import com.hungle.sunriise.util.MnyFilter;
import com.hungle.sunriise.util.StopWatch;
import com.hungle.sunriise.util.SunriiseBuildNumber;
import com.hungle.sunriise.viewer.MnyHttpServer;
import com.hungle.sunriise.viewer.MnyViewer;
import com.hungle.sunriise.viewer.cell.MyTableCellRenderer;
import com.hungle.sunriise.viewer.menu.PopupListener;
import com.hungle.sunriise.viewer.open.OpenDbAction;
import com.hungle.sunriise.viewer.open.OpenDbDialog;
import com.hungle.sunriise.viewer.table.TableUtils;

// TODO: Auto-generated Javadoc
/**
 * The Class AccountViewer.
 */
public class AccountViewerApp implements AccountViewerListener {

    /** The Constant log. */
    private static final Logger LOGGER = org.apache.logging.log4j.LogManager.getLogger(AccountViewerApp.class);

//    private ConfigurableApplicationContext configurableApplicationContext;

    private final class AccountCheckBoxItemListener implements ItemListener {
        private int index;

        private BitSet bitset;

        public AccountCheckBoxItemListener(BitSet bitset, int index) {
            super();
            this.index = index;
            this.bitset = bitset;
        }

        @Override
        public void itemStateChanged(ItemEvent e) {

            boolean state = false;
            if (e.getStateChange() == ItemEvent.SELECTED) {
                state = true;
            } else {
                state = false;
            }

            if (LOGGER.isDebugEnabled()) {
                LOGGER.debug("AccountCheckBoxItemListener, index=" + index + ", state=" + state);
            }

            bitset.set(index, state);

            updateAccountListView();
        }
    }

    private final class GotoXferPopupListener extends PopupListener {
        private GotoXferPopupListener(JPopupMenu popup) {
            super(popup);
        }

        @Override
        protected boolean vetoPopup(MouseEvent event) {
            int selectedRowIndex = getSelectedRow();
            boolean veto = true;
            Transaction transaction = null;
            Integer transferredAccountId = null;
            if (selectedRowIndex < 0) {
                // no row selected, don't show this popup menu
                LOGGER.warn("no row selected, set veto to true");
                veto = true;
            } else {
                transaction = getTransaction(selectedRowIndex);
                if (transaction != null) {
                    XferInfo xferInfo = transaction.getXferInfo();
                    transferredAccountId = xferInfo.getXferAccountId();
                    if ((transferredAccountId == null) || (transferredAccountId < 0)) {
                        if (LOGGER.isDebugEnabled()) {
                            LOGGER.debug("transferredAccountId not valid, set veto to true");
                        }
                        veto = true;
                    } else {
                        veto = false;
                    }
                } else {
                    LOGGER.warn("transaction is null, set veto to true");
                    veto = true;
                }
            }

            if (!veto) {
                // Database db = mnyDb.getDb();
                final Integer xferAccountId = transferredAccountId;
                String accountName = getMnyContext().getAccounts().get(xferAccountId).getName();
                if (accountName == null) {
                    accountName = "" + xferAccountId;
                }
                // final Integer xferTransactionId =
                // TableTransationXferUtil.getXferId(db, transaction);
                final Integer xferTransactionId = transaction.getXferInfo().getXferTransactionId();
                getPopup().removeAll();
                JMenuItem item = new JMenuItem(
                        new GotoXferTransactionAction("Goto " + accountName + ":" + xferTransactionId, xferAccountId,
                                accountName, xferTransactionId));
                getPopup().add(item);
            } else {
                if (LOGGER.isDebugEnabled()) {
                    LOGGER.debug("veto == true");
                }
            }

            return veto;
        }
    }

    /**
     * The Class GotoXferTransactionAction.
     */
    private final class GotoXferTransactionAction extends AbstractAction {

        /** The Constant serialVersionUID. */
        private static final long serialVersionUID = 1L;

        /** The xfer account id. */
        private final Integer xferAccountId;

        /** The xfer transaction id. */
        private final Integer xferTransactionId;

        private String xferAccountName;

        /**
         * Instantiates a new goto xfer transaction action.
         *
         * @param name              the name
         * @param xferAccountId     the xfer account id
         * @param xferAccountName
         * @param xferTransactionId the xfer transaction id
         */
        private GotoXferTransactionAction(String name, Integer xferAccountId, String xferAccountName,
                Integer xferTransactionId) {
            super(name);
            this.xferAccountId = xferAccountId;
            this.xferAccountName = xferAccountName;
            this.xferTransactionId = xferTransactionId;
        }

        /*
         * (non-Javadoc)
         * 
         * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.
         * ActionEvent)
         */
        @Override
        public void actionPerformed(ActionEvent event) {
            LOGGER.info(xferAccountId + ":" + xferTransactionId);
            // final Account account =
            // mnyContext.getAccounts().get(xferAccountId);
            // if (account == null) {
            // log.warn("Cannot find account for xferAccountId=" +
            // xferAccountId);
            // return;
            // }

            Account account = null;
            int rowIndex = -1;
            for (Account ac : dataModel.getAccounts()) {
                rowIndex++;
                if (ac.getId().equals(xferAccountId)) {
                    account = ac;
                    break;
                }
            }

            if ((rowIndex < 0) || (account == null)) {
                Object source = event.getSource();

                String message = "Cannot find account for accountName=" + xferAccountName
                        + ". Check list of accounts on the left side.";
                LOGGER.warn(message);
                String title = "Account not found";
                Component parentComponent = null;
                if (source instanceof Component) {
                    parentComponent = (Component) source;
                }
                // String message = null;
                int messageType = JOptionPane.ERROR_MESSAGE;
                JOptionPane.showMessageDialog(parentComponent, message, title, messageType);
                return;
            }

            // select account
            LOGGER.info("Selecting account's list rowIndex=" + rowIndex);
            accountList.ensureIndexIsVisible(rowIndex);
            accountList.setSelectedIndex(rowIndex);

            // select row
            Transaction transaction = null;
            rowIndex = -1;
            List<Transaction> transactions = account.getTransactions();
            for (Transaction t : transactions) {
                rowIndex++;
                if (t.getId().equals(xferTransactionId)) {
                    transaction = t;
                    break;
                }
            }
            if ((rowIndex < 0) || (transaction == null)) {
                LOGGER.warn("Cannot find transaction for xferTransactionId=" + xferTransactionId);
            }

            LOGGER.info("Selecting transaction's list rowIndex=" + rowIndex);
            table.setRowSelectionInterval(rowIndex, rowIndex);
            int columnIndex = 0;
            TableUtils.scrollToCenter(table, rowIndex, columnIndex);
            // table.scrollRectToVisible(table.getCellRect(rowIndex,
            // columnIndex, true));
        }
    }

    /**
     * The Class MyOpenDbAction.
     */
    private final class MyOpenDbAction extends OpenDbAction {

        /**
         * Instantiates a new my open db action.
         *
         * @param locationRelativeTo the location relative to
         * @param prefs              the prefs
         * @param mnyDb              the opened db
         */
        private MyOpenDbAction(Component locationRelativeTo, Preferences prefs, MnyDb mnyDb) {
            super(locationRelativeTo, prefs, mnyDb);
            setDisableReadOnlyCheckBox(true);
        }

        @Override
        public void dbFileOpened(MnyDb newMnyDb, OpenDbDialog dialog) {
            mnyDbSelected(newMnyDb);
        }
    }

    /**
     * The Class RowSelectedAction.
     */
    private final class TransactionRowSelectedAction implements Runnable {

        /** The transaction id. */
        private final Integer transactionId;

        /** The transactions. */
        private final List<Transaction> transactions;

        /**
         * Instantiates a new row selected action.
         *
         * @param transactionId the transaction id
         * @param transactions  the transactions
         */
        private TransactionRowSelectedAction(Integer transactionId, List<Transaction> transactions) {
            this.transactionId = transactionId;
            this.transactions = transactions;
        }

        /**
         * Log details.
         *
         * @param transactionId the transaction id
         */
        private void logDetails(Integer transactionId) {
            Transaction transaction = getTransaction(transactionId, transactions);

            if (transaction != null) {
                transactionSelected(transaction);
            }
        }

        /**
         * Log flags.
         *
         * @param id the id
         */
        private void logFlags(final Integer id) {
            try {
                Row row = TableTransactionUtils.getRowById(getMnyDb(), id);
                if (row != null) {
                    StringBuilder sb = new StringBuilder();
                    String[] keys = { "act", "grftt", "grftf" };
                    int i = 0;
                    for (String key : keys) {
                        if (i > 0) {
                            sb.append(", ");
                        }
                        sb.append(key + "=" + row.get(key));
                        i++;
                    }
                    LOGGER.info(sb.toString());
                }
            } catch (IOException e) {
                LOGGER.warn(e);
            }
        }

        /*
         * (non-Javadoc)
         * 
         * @see java.lang.Runnable#run()
         */
        @Override
        public void run() {
            // logFlags(transactionId);
            logDetails(transactionId);
        }
    }

    // private boolean dbReadOnly;

    /** The Constant ID_COLUMN_INDEX. */
    private static final int ID_COLUMN_INDEX = 0;

    /** The Constant prefs. */
    private static final Preferences prefs = Preferences.userNodeForPackage(AccountViewerApp.class);

    /** The Constant threadPool. */
    private static final ExecutorService threadPool = Executors.newCachedThreadPool();

    private static final int OPEN_BITSET = 0;

    private static final int CLOSED_BITSET = 1;

    protected static final int ASSET_BITSET = 2;

    protected static final int BANKING_BITSET = 3;

    protected static final int CASH_BITSET = 4;

    protected static final int CREDIT_CARD_BITSET = 5;

    protected static final int INVESTMENT_BITSET = 6;

    protected static final int LIABILITY_BITSET = 7;

    protected static final int LOAN_BITSET = 8;

    protected static final int UNKNOWN_BITSET = 90;

    private static AccountViewerApp instance;

    private final MnyHttpServer mnyHttpServer;

    /** The frame. */
    private JFrame frame;

    /** The opened db. */
    private MnyDb mnyDb = new MnyDb();

    /** The data model. */
    private AccountViewerDataModel dataModel = new AccountViewerDataModel();

    /** The account list. */
    private JList accountList;

    /** The table. */
    private JTable table;

    /** The mny context. */
    private MnyContext mnyContext = new MnyContext();

    /** The account type label. */
    private JLabel accountTypeLabel;

    /** The starting balance label. */
    private JLabel startingBalanceLabel;

    /** The ending balance label. */
    private JLabel endingBalanceLabel;

    /** The selected account. */
    private Account selectedAccount;

    /** The account info text area. */
    private JTextArea accountInfoTextArea;

    /** The account json text area. */
    private JTextArea accountJsonTextArea;

    /** The transaction qif text area. */
    private JTextArea transactionQifTextArea;

    /** The transaction json text area. */
    private JTextArea transactionJsonTextArea;

    private JTextArea transactionCsvTextArea;

    /** The export menu. */
    private JMenu exportJsonMenu;

    private JMenuItem exportToCsvMenuItem;

    private BitSet accountFilterBitSet = new BitSet();
    private BitSet accountTypeFilterBitSet = new BitSet();

    private RSyntaxTextArea jythonScriptTextAread;

    private RSyntaxTextArea jythonResultTextAread;

    private RSyntaxTextArea accountCsvTextArea;

    private JMenu toolsMenu;

    private JMenu diskUsageMenu;

    /**
     * Launch the application.
     *
     * @param args the arguments
     */
    public static void main(String[] args) {
        // try {
        // Class.forName("net.ucanaccess.jdbc.UcanaccessDriver");
        // } catch (ClassNotFoundException e1) {
        // log.error(e1);
        // System.exit(1);
        // }

        EventQueue.invokeLater(new Runnable() {

            @Override
            public void run() {
                try {
                    JavaInfo.logInfo();

                    LOGGER.info("> Starting AccountViewer");

                    AccountViewerApp accountViewer = new AccountViewerApp();
                    AccountViewerApp.setInstance(accountViewer);
                    showMainFrame(accountViewer);

                    String buildNumber = SunriiseBuildNumber.getBuildnumber();
                    LOGGER.info("BuildNumber: " + buildNumber);

                    AccountViewerApp.threadPool.execute(new Runnable() {
                        @Override
                        public void run() {
                            boolean httpServer = Boolean.valueOf(System.getProperty("httpServer", "false"));
                            LOGGER.info("httpServer={}", httpServer);
                            if (httpServer) {
                                accountViewer.mnyHttpServer.restartHttpServer(args);
                            }
                        }
                    });

//                    Runnable startupTask = new Runnable() {
//                        @Override
//                        public void run() {
//                            boolean httpServer = Boolean.valueOf(System.getProperty("httpServer", "false"));
//                            if (httpServer) {
//                                ConfigurableApplicationContext configurableApplicationContext = AccountViewerApplication.run(args);
//                                accountViewer.notifyConfigurableApplicationContext(configurableApplicationContext);
//                            }
//                        }
//                    };
//                    AccountViewer.threadPool.execute(startupTask);

                } catch (Exception e) {
                    LOGGER.error(e, e);
                }
            }

            protected void showMainFrame(AccountViewerApp window) {
                JFrame mainFrame = window.getFrame();

                String title = MnyViewer.TITLE_NO_OPENED_DB;
                mainFrame.setTitle(title);

                Dimension preferredSize = new Dimension(900, 700);
                mainFrame.setPreferredSize(preferredSize);

                mainFrame.pack();

                mainFrame.setLocationRelativeTo(null);

                mainFrame.setVisible(true);
                LOGGER.info("setVisible to true");
            }
        });
    }

//    protected void notifyConfigurableApplicationContext(ConfigurableApplicationContext configurableApplicationContext) {
//        SwingUtilities.invokeLater(new Runnable() {
//
//            @Override
//            public void run() {
//                setConfigurableApplicationContext(configurableApplicationContext);
//            }
//        });
//    }

    /**
     * Create the application.
     */
    public AccountViewerApp() {
        initialize();

        this.mnyHttpServer = new MnyHttpServer(AccountViewerApplication.class);
    }

    /**
     * App closed.
     */
    protected void appClosed() {
        if (getMnyDb() != null) {
            try {
                getMnyDb().close();
            } catch (IOException e) {
                LOGGER.warn(e);
            }
        }
        setMnyDb(null);
    }

    /**
     * Creates the account info view.
     *
     * @return the component
     */
    private Component createAccountTextView() {
        JPanel view = new JPanel();
        view.setLayout(new BorderLayout());

        RSyntaxTextArea textArea = new RSyntaxTextArea();
        textArea.setSyntaxEditingStyle(SyntaxConstants.SYNTAX_STYLE_PROPERTIES_FILE);
        RTextScrollPane sp = new RTextScrollPane(textArea);
        accountInfoTextArea = textArea;
        accountInfoTextArea.setEditable(false);
        view.add(sp, BorderLayout.CENTER);

        return view;
    }

    /**
     * Creates the account json view.
     *
     * @return the component
     */
    private Component createAccountJsonView() {
        JPanel view = new JPanel();
        view.setLayout(new BorderLayout());

        RSyntaxTextArea textArea = new RSyntaxTextArea();
        textArea.setSyntaxEditingStyle(SyntaxConstants.SYNTAX_STYLE_JAVASCRIPT);
        RTextScrollPane sp = new RTextScrollPane(textArea);
        accountJsonTextArea = textArea;
        accountJsonTextArea.setEditable(false);
        view.add(sp, BorderLayout.CENTER);

        return view;
    }

    private Component createAccountCsvView() {
        JPanel view = new JPanel();
        view.setLayout(new BorderLayout());

        RSyntaxTextArea textArea = new RSyntaxTextArea();
        textArea.setSyntaxEditingStyle(SyntaxConstants.SYNTAX_STYLE_PROPERTIES_FILE);
        RTextScrollPane sp = new RTextScrollPane(textArea);
        accountCsvTextArea = textArea;
        accountCsvTextArea.setEditable(false);
        view.add(sp, BorderLayout.CENTER);

        return view;
    }

    /**
     * Creates the bottom component.
     *
     * @return the component
     */
    private Component createBottomComponent() {
        JTabbedPane tabbedPane = new JTabbedPane();
        tabbedPane.setBorder(BorderFactory.createTitledBorder("Transaction details"));

        // tabbedPane.addTab("QIF", createTransactionQif());

        tabbedPane.addTab("JSON", createTransactionJson());
        tabbedPane.addTab("CSV", createTransactionCsv());
        tabbedPane.addTab("QIF", createTransactionQif());

        // tabbedPane.addTab("Transaction info", createTransactionInfoView());

        return tabbedPane;
    }

    /**
     * Creates the left component.
     *
     * @return the j panel
     */
    private JPanel createAccountsViewLeftComponent() {
        JPanel view = new JPanel();
        // leftComponent.setPreferredSize(new Dimension(80, -1));
        view.setLayout(new BorderLayout());

        view.setBorder(BorderFactory.createTitledBorder("Accounts"));

        JPanel filterView = new JPanel();
        filterView.setLayout(new GridLayout(0, 2));
        view.add(filterView, BorderLayout.NORTH);

        JCheckBox checkBox = new JCheckBox("Open");
        boolean state = true;
        checkBox.setSelected(state);
        accountFilterBitSet.set(OPEN_BITSET, state);
        checkBox.addItemListener(new AccountCheckBoxItemListener(accountFilterBitSet, OPEN_BITSET));
        filterView.add(checkBox);

        checkBox = new JCheckBox("Closed");
        state = false;
        checkBox.setSelected(state);
        accountFilterBitSet.set(CLOSED_BITSET, state);
        checkBox.addItemListener(new AccountCheckBoxItemListener(accountFilterBitSet, CLOSED_BITSET));
        filterView.add(checkBox);

        for (EnumAccountType type : EnumAccountType.values()) {
            checkBox = new JCheckBox(type.toString());
            state = false;

            int index = 0;
            switch (type) {
            case ASSET:
                index = ASSET_BITSET;
                break;
            case BANKING:
                state = true;
                index = BANKING_BITSET;
                break;
            case CASH:
                index = CASH_BITSET;
                break;
            case CREDIT_CARD:
                index = CREDIT_CARD_BITSET;
                break;
            case INVESTMENT:
                index = INVESTMENT_BITSET;
                break;
            case LIABILITY:
                index = LIABILITY_BITSET;
                break;
            case LOAN:
                index = LOAN_BITSET;
                break;
            case UNKNOWN:
                index = UNKNOWN_BITSET;
                break;
            default:
                break;
            }

            checkBox.setSelected(state);
            accountTypeFilterBitSet.set(index, state);
            checkBox.addItemListener(new AccountCheckBoxItemListener(accountTypeFilterBitSet, index));
            filterView.add(checkBox);
        }

        // the account list
        JScrollPane scrollPane = new JScrollPane();
        view.add(scrollPane, BorderLayout.CENTER);

        accountList = new JList();
        accountList.addListSelectionListener(new ListSelectionListener() {

            @Override
            public void valueChanged(ListSelectionEvent event) {
                if (event.getValueIsAdjusting()) {
                    return;
                }
                final Account account = (Account) accountList.getSelectedValue();
                if (account != null) {
                    try {
                        accountSelected(account);
                    } catch (IOException e) {
                        LOGGER.error(e);
                    }
                }
            }

        });
        accountList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        accountList.setVisibleRowCount(-1);
        ListCellRenderer cellRenderer = new AccountListCellRenderer();
        accountList.setCellRenderer(cellRenderer);
        scrollPane.setViewportView(accountList);

        return view;
    }

    /**
     * Creates the right component.
     *
     * @return the j panel
     */
    private JPanel createAccountsViewRightComponent() {
        JPanel view = new JPanel();
        view.setLayout(new BorderLayout());

        Component topComponent = createTopComponent();
        Component bottomComponent = createBottomComponent();

        JSplitPane vSplitPane = new JSplitPane(JSplitPane.VERTICAL_SPLIT, topComponent, bottomComponent);
        view.add(vSplitPane, BorderLayout.CENTER);

        vSplitPane.setResizeWeight(0.50);
        vSplitPane.setDividerLocation(0.50);

        return view;
    }

    /**
     * Creates the top component.
     *
     * @return the component
     */
    private Component createTopComponent() {
        JTabbedPane tabbedPane = new JTabbedPane();
        tabbedPane.setBorder(BorderFactory.createTitledBorder("Account details"));

        tabbedPane.addTab("Transactions", createTransactionsView());

        tabbedPane.addTab("JSON", createAccountJsonView());

        tabbedPane.addTab("CSV", createAccountCsvView());

        tabbedPane.addTab("Text", createAccountTextView());

        return tabbedPane;
    }

    /**
     * Creates the transaction info view.
     *
     * @return the component
     */
    private Component createTransactionInfoView() {
        JPanel view = new JPanel();
        view.setLayout(new BorderLayout());
        return view;
    }

    /**
     * Creates the transaction json.
     *
     * @return the component
     */
    private Component createTransactionJson() {
        JPanel view = new JPanel();
        view.setLayout(new BorderLayout());

        RSyntaxTextArea textArea = new RSyntaxTextArea();
        textArea.setSyntaxEditingStyle(SyntaxConstants.SYNTAX_STYLE_JAVASCRIPT);
        RTextScrollPane sp = new RTextScrollPane(textArea);
        transactionJsonTextArea = textArea;
        transactionJsonTextArea.setEditable(false);
        view.add(sp, BorderLayout.CENTER);

        return view;
    }

    private Component createTransactionCsv() {
        JPanel view = new JPanel();
        view.setLayout(new BorderLayout());

        RSyntaxTextArea textArea = new RSyntaxTextArea();
        textArea.setSyntaxEditingStyle(SyntaxConstants.SYNTAX_STYLE_PROPERTIES_FILE);
        RTextScrollPane sp = new RTextScrollPane(textArea);
        transactionCsvTextArea = textArea;
        transactionCsvTextArea.setEditable(false);
        view.add(sp, BorderLayout.CENTER);

        return view;
    }

    /**
     * Creates the transaction qif.
     *
     * @return the component
     */
    private Component createTransactionQif() {
        JPanel view = new JPanel();
        view.setLayout(new BorderLayout());

        RSyntaxTextArea textArea = new RSyntaxTextArea();
        textArea.setSyntaxEditingStyle(SyntaxConstants.SYNTAX_STYLE_NONE);
        RSyntaxDocument doc = (RSyntaxDocument) textArea.getDocument();
//        TokenMaker tokenMaker = new QifTokenMaker();
//        doc.setSyntaxStyle(tokenMaker);
        RTextScrollPane sp = new RTextScrollPane(textArea);
        transactionQifTextArea = textArea;
        transactionQifTextArea.setEditable(false);
        view.add(sp, BorderLayout.CENTER);

        return view;
    }

    /**
     * Creates the transactions view.
     *
     * @return the component
     */
    private Component createTransactionsView() {
        JPanel view = new JPanel();
        view.setLayout(new BorderLayout());

        JPanel topStatusView = new JPanel();
        view.add(topStatusView, BorderLayout.NORTH);
        topStatusView.setLayout(new BorderLayout(0, 0));

        accountTypeLabel = new JLabel("");
        topStatusView.add(accountTypeLabel, BorderLayout.WEST);

        startingBalanceLabel = new JLabel("");
        updateStartingBalanceLabel(new BigDecimal(0.00), null);
        topStatusView.add(startingBalanceLabel, BorderLayout.EAST);

        JScrollPane scrollPane = new JScrollPane();
        view.add(scrollPane, BorderLayout.CENTER);

        table = new JTable() {

            @Override
            public void setModel(TableModel dataModel) {
                super.setModel(dataModel);
                RowSorter<? extends TableModel> rs = getRowSorter();
                if (rs != null) {
                    TableRowSorter<? extends TableModel> rowSorter = (rs instanceof TableRowSorter)
                            ? (TableRowSorter<? extends TableModel>) rs
                            : null;

                    if (rowSorter == null) {
                        throw new RuntimeException("Cannot find appropriate rowSorter: " + rs);
                    }
                    LOGGER.info("ROW_FILTER: reset");
                    rowSorter.setRowFilter(null);
                }

                TableColumnModel columnModel = this.getColumnModel();
                int cols = columnModel.getColumnCount();

                for (int i = 0; i < cols; i++) {
                    TableColumn column = columnModel.getColumn(i);
                    MyTableCellRenderer renderer = new MyTableCellRenderer(column.getCellRenderer());
                    column.setCellRenderer(renderer);
                }
            }
        };
        table.setAutoCreateRowSorter(true);
        table.setDefaultRenderer(BigDecimal.class, new DefaultTableCellRenderer() {

            @Override
            public void setValue(Object value) {
                if (LOGGER.isDebugEnabled()) {
                    LOGGER.debug("cellRenderer: value=" + value + ", " + value.getClass().getName());
                }
                String renderedValue = ((value == null) || (getSelectedAccount() == null)) ? ""
                        : FormatterUtils.formatAmount(getSelectedAccount(), (BigDecimal) value, null);
                if (LOGGER.isDebugEnabled()) {
                    LOGGER.debug("cellRenderer: renderedValue=" + renderedValue);
                }

                setText(renderedValue);

                BigDecimal bigDecimal = (BigDecimal) value;
                if (bigDecimal.compareTo(BigDecimal.ZERO) < 0) {
                    setForeground(Color.RED);
                } else {
                    setForeground(Color.BLACK);
                }
            }
        });
        // table.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
        table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        table.getSelectionModel().addListSelectionListener(new ListSelectionListener() {

            @Override
            public void valueChanged(ListSelectionEvent event) {
                if (event.getValueIsAdjusting()) {
                    return;
                }

                for (int selectedRowIndex : table.getSelectedRows()) {
                    selectedRowIndex = getSelectedRow(selectedRowIndex);
                    transactionRowSelected(selectedRowIndex);
                }
            }
        });
        scrollPane.setViewportView(table);

        JPopupMenu tablePopupMenu = new JPopupMenu();
        MouseListener tablePopupListener = new GotoXferPopupListener(tablePopupMenu);
        table.addMouseListener(tablePopupListener);

        JPanel bottomStatusView = new JPanel();
        view.add(bottomStatusView, BorderLayout.SOUTH);
        bottomStatusView.setLayout(new BorderLayout(0, 0));

        endingBalanceLabel = new JLabel("");
        updateEndingBalanceLabel(new BigDecimal(0.00), null);
        bottomStatusView.add(endingBalanceLabel, BorderLayout.EAST);

        return view;
    }

    /**
     * Gets the account type label.
     *
     * @return the account type label
     */
    public JLabel getAccountTypeLabel() {
        return accountTypeLabel;
    }

    /**
     * Gets the ending balance label.
     *
     * @return the ending balance label
     */
    private JLabel getEndingBalanceLabel() {
        return endingBalanceLabel;
    }

    /**
     * Gets the frame.
     *
     * @return the frame
     */
    private JFrame getFrame() {
        return frame;
    }

    /**
     * Gets the opened db.
     *
     * @return the opened db
     */
    public MnyDb getMnyDb() {
        return mnyDb;
    }

    /**
     * Log json.
     *
     * @param transaction the transaction
     */
    private void logJson(Transaction transaction) {
        try {
            final String jsonStr = JSONUtils.serialize(transaction);

            if (LOGGER.isDebugEnabled()) {
                LOGGER.debug("Transaction JSON:");
                LOGGER.debug("\n" + jsonStr);
            }

            if (transactionJsonTextArea != null) {
                Runnable doRun = new Runnable() {

                    @Override
                    public void run() {
                        transactionJsonTextArea.setText(jsonStr);
                        transactionJsonTextArea.setCaretPosition(0);
                    }
                };
                SwingUtilities.invokeLater(doRun);
            }
        } catch (IOException e) {
            LOGGER.warn(e);
        } finally {
        }
    }

    private void logCsv(Transaction transaction) {
        try {
            final String csvStr = Csv2Utils.serialize(transaction);

            if (LOGGER.isDebugEnabled()) {
                LOGGER.debug("Transaction CSV:");
                LOGGER.debug("\n" + csvStr);
            }

            if (transactionJsonTextArea != null) {
                Runnable doRun = new Runnable() {

                    @Override
                    public void run() {
                        transactionCsvTextArea.setText(csvStr);
                        transactionCsvTextArea.setCaretPosition(0);
                    }
                };
                SwingUtilities.invokeLater(doRun);
            }
        } catch (IOException e) {
            LOGGER.warn(e);
        } finally {
        }
    }

    /**
     * Log qif.
     *
     * @param transaction the transaction
     */
    private void logQif(Transaction transaction) {
//        StringWriter stringWriter = new StringWriter();
//        PrintWriter writer = null;
        try {
            final String qifStr = TransactionMapper.getTransactionBlock(getMnyContext(), getSelectedAccount(),
                    transaction);

//            writer = new PrintWriter(stringWriter);
//            QifExportUtils.logQif(transaction, mnyContext, writer);
//            writer.flush();
//            final String qifStr = stringWriter.getBuffer().toString();

            if (LOGGER.isDebugEnabled()) {
                LOGGER.debug("Transaction QIF:");
                LOGGER.debug("\n" + qifStr);
            }

            if (transactionQifTextArea != null) {
                Runnable doRun = new Runnable() {

                    @Override
                    public void run() {
                        transactionQifTextArea.setText(qifStr);
                        transactionQifTextArea.setCaretPosition(0);
                    }
                };
                SwingUtilities.invokeLater(doRun);
            }
        } finally {
//            if (writer != null) {
//                writer.close();
//                writer = null;
//            }
        }
    }

    /**
     * Gets the starting balance label.
     *
     * @return the starting balance label
     */
    private JLabel getStartingBalanceLabel() {
        return startingBalanceLabel;
    }

    /**
     * Gets the table model.
     *
     * @return the table model
     */
    private AbstractAccountViewerTableModel getTableModel() {
        return (AbstractAccountViewerTableModel) dataModel.getTableModel();
    }

    /**
     * Gets the transaction.
     *
     * @param selectedRowIndex the selected row index
     * @return the transaction
     */
    private Transaction getTransaction(int selectedRowIndex) {
        AbstractAccountViewerTableModel tableModel = getTableModel();
        final Integer transactionId = getTransactionId(selectedRowIndex, tableModel);
        final List<Transaction> transactions = getTransactions(tableModel);
        Transaction transaction = getTransaction(transactionId, transactions);
        return transaction;
    }

    /**
     * Gets the transaction.
     *
     * @param transactionId the transaction id
     * @param transactions  the transactions
     * @return the transaction
     */
    private Transaction getTransaction(Integer transactionId, List<Transaction> transactions) {
        // TODO: Better look up

        Transaction transaction = null;
        for (Transaction t : transactions) {
            if (t.getId().equals(transactionId)) {
                transaction = t;
                LOGGER.info("Selected transactionId=" + transaction.getId());
                break;
            }
        }
        return transaction;
    }

    /**
     * Gets the transaction id.
     *
     * @param selectedRowIndex the selected row index
     * @param tableModel       the table model
     * @return the transaction id
     */
    private Integer getTransactionId(int selectedRowIndex, AbstractAccountViewerTableModel tableModel) {
        int rowIndex = selectedRowIndex;
        int columnIndex = ID_COLUMN_INDEX;

        final Integer findId = (Integer) tableModel.getValueAt(rowIndex, columnIndex);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("id=" + findId);
        }
        return findId;
    }

    /**
     * Gets the transactions.
     *
     * @param tableModel the table model
     * @return the transactions
     */
    private List<Transaction> getTransactions(AbstractAccountViewerTableModel tableModel) {
        final Account account = tableModel.getAccount();
        final List<Transaction> transactions = account.getTransactions();
        return transactions;
    }

    /**
     * Inits the data bindings.
     */
    protected void initDataBindings() {
        BeanProperty<AccountViewerDataModel, List<Account>> accountViewerDataModelBeanProperty = BeanProperty
                .create("accounts");
        JListBinding<Account, AccountViewerDataModel, JList> jListBinding = SwingBindings
                .createJListBinding(UpdateStrategy.READ, dataModel, accountViewerDataModelBeanProperty, accountList);
        jListBinding.bind();
        //
        BeanProperty<AccountViewerDataModel, TableModel> accountViewerDataModelBeanProperty_1 = BeanProperty
                .create("tableModel");
        ELProperty<JTable, Object> jTableEvalutionProperty = ELProperty.create("${model}");
        AutoBinding<AccountViewerDataModel, TableModel, JTable, Object> autoBinding = Bindings.createAutoBinding(
                UpdateStrategy.READ, dataModel, accountViewerDataModelBeanProperty_1, table, jTableEvalutionProperty);
        autoBinding.bind();
    }

    /**
     * Initialize the contents of the frame.
     */
    private void initialize() {
        setFrame(new JFrame());

        // frame.setBounds(100, 100, 450, 300);
        getFrame().setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        getFrame().addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosed(WindowEvent event) {
                super.windowClosed(event);
                LOGGER.info("> windowClosed");
            }

            @Override
            public void windowClosing(WindowEvent event) {
                super.windowClosing(event);
                LOGGER.info("> windowClosing");
                if (getMnyDb() != null) {
                    appClosed();
                }
            }

        });

        initMainMenuBar();

        // JSplitPane hSplitPane = createRegisterView();

        JTabbedPane tabbedPane = new JTabbedPane(SwingConstants.BOTTOM);

        tabbedPane.add("Accounts", createAccountsView());

        getFrame().getContentPane().add(tabbedPane, BorderLayout.CENTER);

        initDataBindings();
    }

    protected JSplitPane createAccountsView() {
        JPanel left = createAccountsViewLeftComponent();
        JPanel right = createAccountsViewRightComponent();
        JSplitPane splitPane = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, left, right);
        splitPane.setResizeWeight(0.30);
        splitPane.setDividerLocation(0.30);
        return splitPane;
    }

    /**
     * Inits the main menu bar.
     */
    private void initMainMenuBar() {
        LOGGER.info("> initMainMenuBar");

        JMenuBar menuBar = new JMenuBar();
        getFrame().setJMenuBar(menuBar);

        JMenu fileMenu = new JMenu("File");
        menuBar.add(fileMenu);

        JMenuItem fileOpenMenuItem = new JMenuItem("Open");
        fileOpenMenuItem.addActionListener(new MyOpenDbAction(AccountViewerApp.this.frame, prefs, getMnyDb()));

        fileMenu.add(fileOpenMenuItem);

        addExportJsonMenu(fileMenu);
        addExportCsv2Menu(fileMenu);

        fileMenu.addSeparator();

        JMenuItem exitMenuItem = new JMenuItem("Exit");
        exitMenuItem.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                appClosed();

                LOGGER.info("User selects File -> Exit");
                System.exit(0);
            }
        });
        fileMenu.add(exitMenuItem);

        File sampleTopDir = new File("../sunriise-core");
        AbstractSamplesMenu samplesMenu = new AbstractSamplesMenu() {
            @Override
            protected void mnyDbSelected(MnyDb newMnyDb) {
                AccountViewerApp.this.mnyDbSelected(newMnyDb);
            }
        };
        samplesMenu.addSamplesMenu(menuBar, sampleTopDir);
//        Launcher.addHelpMenu(frame, menuBar);

        addToolsMenu(menuBar);
    }

    private void addExportCsv2Menu(JMenu parentMenu) {
        exportToCsvMenuItem = new JMenuItem("Export to CSV");
        parentMenu.add(exportToCsvMenuItem);
        exportToCsvMenuItem.setEnabled(false);
        ExportToContext exportToContext = new ExportToContext() {

            @Override
            public Component getParentComponent() {
                return getFrame();
            }

            @Override
            public MnyDb getMnyDb() {
                return AccountViewerApp.this.getMnyDb();
            }

            @Override
            public Executor getThreadPool() {
                return MnyViewer.getThreadPool();
            }
        };
        exportToCsvMenuItem.addActionListener(new ExportToCsv2Action(exportToContext));
    }

    private void addExportJsonMenu(JMenu parentMenu) {
        exportJsonMenu = new JMenu("Export JSON");
        parentMenu.add(exportJsonMenu);
        exportJsonMenu.setEnabled(false);

        JMenuItem toDirectory = new JMenuItem("To directory");
        ExportToContext exportToContext = new ExportToContext() {

            @Override
            public Component getParentComponent() {
                return getFrame();
            }

            @Override
            public MnyDb getMnyDb() {
                return AccountViewerApp.this.getMnyDb();
            }

            @Override
            public Executor getThreadPool() {
                return MnyViewer.getThreadPool();
            }
        };
        toDirectory.addActionListener(new ExportToJSONAction(exportToContext));
        exportJsonMenu.add(toDirectory);

        JMenuItem toMongoDbServer = new JMenuItem("To mongoDB server ");
        exportToContext = new ExportToContext() {

            @Override
            public Component getParentComponent() {
                return getFrame();
            }

            @Override
            public MnyDb getMnyDb() {
                return AccountViewerApp.this.getMnyDb();
            }

            @Override
            public Executor getThreadPool() {
                return MnyViewer.getThreadPool();
            }
        };
        toMongoDbServer.addActionListener(new ExportToJSONAction(exportToContext));
        exportJsonMenu.add(toMongoDbServer);
    }

//    private void addSamplesMenu(JMenuBar menuBar, File sampleTopDir) {
//        if (sampleTopDir.exists() && (sampleTopDir.isDirectory())) {
//            JMenu sampleMenu = new JMenu("Samples");
//            menuBar.add(sampleMenu);
//
//            for (MnySampleFile dbFile : MnySampleFile.SAMPLE_FILES) {
//                if (dbFile.isBackup()) {
//                    continue;
//                }
//
//                File sampleFile = MnySampleFile.getSampleFileFromModuleProject(dbFile);
//
//                String fileName = sampleFile.getName();
//
//                JMenuItem menuItem = new JMenuItem(fileName);
//                sampleMenu.add(menuItem);
//                OpenMnySampleFileAction action = new OpenMnySampleFileAction(dbFile) {
//                    @Override
//                    void mnyDbOpened(MnyDb newMnyDb) {
//                        AccountViewer.this.mnyDbSelected(newMnyDb);
//                    }
//                };
//                menuItem.addActionListener(action);
//            }
//        }
//    }

    protected void updateAccountListView() {
        final Map<Integer, Account> accounts = getMnyContext().getAccounts();

        MnyFilter<Account> filter = new MnyFilter<Account>() {
            @Override
            public boolean accepts(Account account) {
                if (account == null) {
                    return false;
                }

                BitSet openClosedBitSet = new BitSet();
                openClosedBitSet.set(OPEN_BITSET, !account.getClosed());
                openClosedBitSet.set(CLOSED_BITSET, account.getClosed());

                BitSet typeBitSet = new BitSet();
                EnumAccountType type = account.getAccountType();
                switch (type) {
                case ASSET:
                    typeBitSet.set(ASSET_BITSET, true);
                    break;
                case BANKING:
                    typeBitSet.set(BANKING_BITSET, true);
                    break;
                case CASH:
                    typeBitSet.set(CASH_BITSET, true);
                    break;
                case CREDIT_CARD:
                    typeBitSet.set(CREDIT_CARD_BITSET, true);
                    break;
                case INVESTMENT:
                    typeBitSet.set(INVESTMENT_BITSET, true);
                    break;
                case LIABILITY:
                    typeBitSet.set(LIABILITY_BITSET, true);
                    break;
                case LOAN:
                    typeBitSet.set(LOAN_BITSET, true);
                    break;
                case UNKNOWN:
                    typeBitSet.set(UNKNOWN_BITSET, true);
                    break;
                default:
                    LOGGER.warn("Invalid accountType=" + type);
                    break;
                }

                if (LOGGER.isDebugEnabled()) {
                    LOGGER.debug("accountFilterBitSet=" + accountFilterBitSet);
                    LOGGER.debug("openClosedBitSet=" + openClosedBitSet);
                    LOGGER.debug("accountTypeFilterBitSet=" + accountTypeFilterBitSet);
                    LOGGER.debug("typeBitSet=" + typeBitSet);
                }

                openClosedBitSet.and(accountFilterBitSet);
                if (LOGGER.isDebugEnabled()) {
                    LOGGER.debug("AFTER_AND openClosedBitSet=" + openClosedBitSet);
                }
                typeBitSet.and(accountTypeFilterBitSet);
                if (LOGGER.isDebugEnabled()) {
                    LOGGER.debug("AFTER_AND typeBitSet=" + typeBitSet);
                }
                return ((openClosedBitSet.cardinality() > 0) && (typeBitSet.cardinality() > 0));
            }
        };

        List<Account> accountList = TableAccountUtils.toAccountsList(accounts, filter);

        notifyViewNewAccountList(accountList);
    }

    protected void notifyViewNewAccountList(List<Account> accounts) {
        LOGGER.info("> notifyViewNewAccountList");

        AccountViewerApp.this.dataModel.setAccounts(accounts);

        Runnable doRun = new Runnable() {
            @Override
            public void run() {
                exportJsonMenu.setEnabled(true);
                exportToCsvMenuItem.setEnabled(true);

                if (diskUsageMenu != null) {
                    diskUsageMenu.setEnabled(true);
                }

                Account account = null;
                // clear out currently select account, if any
                try {
                    accountSelected(account);
                } catch (IOException e) {
                    LOGGER.warn(e);
                }
            }
        };
        SwingUtilities.invokeLater(doRun);
    }

    /**
     * Row selected.
     *
     * @param selectedRowIndex the selected row index
     */
    private void transactionRowSelected(int selectedRowIndex) {
        AbstractAccountViewerTableModel tableModel = getTableModel();

        transactionRowSelected(selectedRowIndex, tableModel);
    }

    /**
     * Row selected.
     *
     * @param selectedRowIndex the selected row index
     * @param tableModel       the table model
     */
    private void transactionRowSelected(int selectedRowIndex, AbstractAccountViewerTableModel tableModel) {
        final Integer transactionId = getTransactionId(selectedRowIndex, tableModel);
        final List<Transaction> transactions = getTransactions(tableModel);

        Runnable command = new TransactionRowSelectedAction(transactionId, transactions);
        threadPool.execute(command);
    }

    /**
     * Sets the frame.
     *
     * @param frame the new frame
     */
    private void setFrame(JFrame frame) {
        this.frame = frame;
    }

    /**
     * Update account info pane.
     *
     * @param account the account
     */
    private void updateAccountInfoPane(final Account account) {
        if (accountInfoTextArea == null) {
            return;
        }

        accountInfoTextArea.setText("");

        if (account == null) {
            return;
        }

        accountInfoTextArea.append("Name: " + account.getName() + "\n");
        accountInfoTextArea.append("ID: " + account.getId() + "\n");
        accountInfoTextArea.append("Account type: " + account.getAccountType().toString() + "\n");
        accountInfoTextArea.append("Number of transactions: " + account.getTransactions().size() + "\n");
        if (EnumAccountType.isInvestment(account)) {
            accountInfoTextArea.append("Retirement: " + account.getRetirement().toString() + "\n");
        }
        accountInfoTextArea.append("Currency: " + TableCurrencyUtils.getCurrencyName(account, getMnyContext()) + "\n");
        accountInfoTextArea.append(
                "Starting balance: " + FormatterUtils.formatAmount(account, account.getStartingBalance(), null) + "\n");
        accountInfoTextArea.append(
                "Ending balance: " + FormatterUtils.formatAmount(account, account.getCurrentBalance(), null) + "\n");

        if (EnumAccountType.isInvestment(account)) {
            accountInfoTextArea.append("# SecurityHolding" + "\n");
            List<SecurityHolding> securityHoldings = account.getSecurityHoldings();
            int count = 0;
            for (SecurityHolding securityHolding : securityHoldings) {
                Security security = securityHolding.getSecurity();
                StringBuilder sb = new StringBuilder();
                sb.append("SecurityHolding." + count++ + ": ");
                sb.append(security.getName());
                sb.append(", ");
                sb.append(security.getSymbol());
                sb.append(", ");
                // sb.append(FormatterUtil.formatSecurityQuantity(securityHolding.getQuantity()));
                sb.append(securityHolding.getQuantity());
                sb.append(", ");
                sb.append(FormatterUtils.formatAmount(account, securityHolding.getPrice(), null));
                sb.append(", ");
                sb.append(FormatterUtils.formatAmount(account, securityHolding.getMarketValue(), null));
                sb.append("\n");

                accountInfoTextArea.append(sb.toString());
            }
        }

        Account relatedToAccount = account.getRelatedToAccount();
        if (relatedToAccount != null) {
            accountInfoTextArea.append("# RelatedToAccount" + "\n");

            String accountLabel = "Cash account";
            if (EnumAccountType.isInvestment(account)) {
                accountLabel = "Cash account";
            } else {
                accountLabel = "Related account";
            }
            String name = relatedToAccount.getName();
            accountInfoTextArea.append(accountLabel + ": " + name + "\n");
            BigDecimal currentBalance = relatedToAccount.getCurrentBalance();
            if (currentBalance != null) {
                accountInfoTextArea.append(accountLabel + " balance: "
                        + FormatterUtils.formatAmount(relatedToAccount, currentBalance, null) + "\n");
            } else {
                accountInfoTextArea.append(accountLabel + " balance: " + currentBalance + "\n");
            }
        }

        if (EnumAccountType.isCreditCard(account)) {
            accountInfoTextArea.append("# Credit Card Info" + "\n");
            BigDecimal amountLimit = account.getCreditCardAmountLimit();
            if (amountLimit == null) {
                amountLimit = new BigDecimal(0.0);
            }
            accountInfoTextArea.append("Credit card limit amount: "
                    + FormatterUtils.formatAmount(account, new BigDecimal(Math.abs(amountLimit.doubleValue())), null)
                    + "\n");
        }

        accountInfoTextArea.setCaretPosition(0);
    }

    /**
     * Update account json pane.
     *
     * @param account the account
     */
    private void updateAccountJsonPane(Account account) {
        accountJsonTextArea.setText("");

        if (account == null) {
            return;
        }

        Writer writer = null;
        try {
            writer = new JTextAreaWriter(accountJsonTextArea);
            JSONUtils.writeValue(account, writer);
        } catch (IOException e) {
            LOGGER.warn(e);
        } finally {
            if (writer != null) {
                try {
                    writer.close();
                } catch (IOException e) {
                    LOGGER.warn(e);
                } finally {
                    writer = null;
                }
            }
            accountJsonTextArea.setCaretPosition(0);
        }
    }

    private void updateAccountCsvPane(Account account) {
        accountCsvTextArea.setText("");

        if (account == null) {
            return;
        }

        Writer writer = null;
        try {
            final String csvStr = Csv2Utils.serialize(account);
            writer = new JTextAreaWriter(accountCsvTextArea);
            writer.write(csvStr);
        } catch (IOException e) {
            LOGGER.warn(e);
        } finally {
            if (writer != null) {
                try {
                    writer.close();
                } catch (IOException e) {
                    LOGGER.warn(e);
                } finally {
                    writer = null;
                }
            }
            accountCsvTextArea.setCaretPosition(0);
        }
    }

    /**
     * Update ending balance label.
     *
     * @param balance the balance
     * @param account the account
     */
    private void updateEndingBalanceLabel(BigDecimal balance, Account account) {
        LOGGER.info("> updateEndingBalanceLabel, balance=" + balance + ", account=" + account);
        String label = "Ending balance: ";
        if ((balance != null) && (account != null)) {
            getEndingBalanceLabel().setText(label + FormatterUtils.formatAmount(account, balance, null));
        } else {
            getEndingBalanceLabel().setText(label);
        }
    }

    /**
     * Update starting balance label.
     *
     * @param balance the balance
     * @param account the account
     */
    private void updateStartingBalanceLabel(BigDecimal balance, Account account) {
        LOGGER.info("> updateStartingBalanceLabel, balance=" + balance + ", account=" + account);

        String label = "Starting balance: ";

        if ((balance != null) && (account != null)) {
            getStartingBalanceLabel().setText(label + FormatterUtils.formatAmount(account, balance, null));
        } else {
            getStartingBalanceLabel().setText(label);
        }
    }

    private int getSelectedRow() {
        int selectedRowIndex = table.getSelectedRow();
        return getSelectedRow(selectedRowIndex);
    }

    private int getSelectedRow(int selectedRowIndex) {
        RowSorter<? extends TableModel> rowSorter = table.getRowSorter();
        if (rowSorter != null) {
            selectedRowIndex = rowSorter.convertRowIndexToModel(selectedRowIndex);
        }
        return selectedRowIndex;
    }

    private void setMnyDb(MnyDb mnyDb) {
        this.mnyDb = mnyDb;
    }

    /**
     * Open opened db.
     *
     * @param newMnyDb the new opened db
     */
    @Override
    public void mnyDbSelected(final MnyDb newMnyDb) {
        if (newMnyDb != null) {
            AccountViewerApp.this.setMnyDb(newMnyDb);
        }

        setFrameTitle();

        try {
            MnyContextUtils.initMnyContext(getMnyDb(), getMnyContext());
            updateAccountListView();
        } catch (IOException e) {
            LOGGER.warn(e);
        }
    }

    protected void setFrameTitle() {
        File dbFile = getMnyDb().getDbFile();
        if (dbFile != null) {
            getFrame().setTitle(dbFile.getAbsolutePath());
        } else {
            String title = MnyViewer.TITLE_NO_OPENED_DB;
            getFrame().setTitle(title);
        }
    }

    /**
     * Account selected.
     *
     * @param account the account
     * @throws IOException Signals that an I/O exception has occurred.
     */
    @Override
    public void accountSelected(final Account account) throws IOException {
        StopWatch stopWatch = new StopWatch();

        LOGGER.info("> accountSelected, account=" + account);
        setSelectedAccount(account);

        try {
            if (account != null) {
                LOGGER.info("select account=" + account);

                TableAccountUtils.addTransactionsToAccount(account, getMnyContext());
                stopWatch.logTiming(LOGGER, "POST addTransactionsToAccount");

                BigDecimal currentBalance = account.getCurrentBalance();
                stopWatch.logTiming(LOGGER, "POST getCurrentBalance");

                if (LOGGER.isDebugEnabled()) {
                    LOGGER.debug(account.getName() + ", " + account.getAccountType() + ", "
                            + TableCurrencyUtils.getCurrencyName(account, getMnyContext()) + ", "
                            + account.getStartingBalance() + ", " + currentBalance + ", "
                            + account.getCreditCardAmountLimit());
                }

                // UI
                updateStartingBalanceLabel(account.getStartingBalance(), account);
                stopWatch.logTiming(LOGGER, "POST updateStartingBalanceLabel");

                updateEndingBalanceLabel(currentBalance, account);
                stopWatch.logTiming(LOGGER, "POST updateEndingBalanceLabel");

                getAccountTypeLabel().setText(account.getAccountType().toString());

                boolean calculateMonthlySummary = false;
                if (calculateMonthlySummary) {
                    CalculateMonthlySummary monthlySummary = new CalculateMonthlySummary(account);
                    List<SummaryEntry> months = monthlySummary.getSummaries();
                    LOGGER.info("### Monthly summary:");
                    LOGGER.info("  account.name={}", monthlySummary.getAccount().getName());
                    LOGGER.info("  startDate={}", monthlySummary.getStartDate());
                    LOGGER.info("  endDate={}", monthlySummary.getEndDate());
                    for (SummaryEntry month : months) {
                        LOGGER.info("  month={}", month);
                    }
                    stopWatch.logTiming(LOGGER, "POST calculateMonthlySummary");
                }
            }

            AbstractAccountViewerTableModel tableModel = null;

            if (account != null) {
                EnumAccountType accountType = account.getAccountType();
                switch (accountType) {
                case INVESTMENT:
                    tableModel = new InvestmentTableModel(account);
                    break;
                default:
                    break;
                }
            }

            if (tableModel == null) {
                tableModel = new DefaultAccountViewerTableModel(account);
            }

            tableModel.setMnyContext(getMnyContext());

            dataModel.setTableModel(tableModel);

            updateAccountInfoPane(account);
            updateAccountJsonPane(account);
            updateAccountCsvPane(account);

            if (transactionQifTextArea != null) {
                transactionQifTextArea.setText("");
            }
            transactionJsonTextArea.setText("");
            transactionCsvTextArea.setText("");
            transactionQifTextArea.setText("");
        } finally {
            int size = -1;
            if (account != null) {
                List<Transaction> transactions = account.getTransactions();
                if (transactions != null) {
                    size = transactions.size();
                }
            }
            stopWatch.logTiming(LOGGER, "< accountSelected, transactions.size=" + size);
        }
    }

    /**
     * Log transaction.
     *
     * @param transaction the transaction
     */
    @Override
    public void transactionSelected(final Transaction transaction) {
        logJson(transaction);
        logCsv(transaction);
        logQif(transaction);
    }

    public MnyContext getMnyContext() {
        return mnyContext;
    }

    private void setMnyContext(MnyContext mnyContext) {
        this.mnyContext = mnyContext;
    }

    public static AccountViewerApp getInstance() {
        return instance;
    }

    private static void setInstance(AccountViewerApp instance) {
        AccountViewerApp.instance = instance;
    }

    /**
     * Adds the tools menu.
     *
     * @param menuBar the menu bar
     */
    private void addToolsMenu(JMenuBar menuBar) {
        LOGGER.info("> addToolsMenu");

        toolsMenu = new JMenu("Tools");
        menuBar.add(toolsMenu);

        diskUsageMenu = new JMenu("Disk usage");
        toolsMenu.add(diskUsageMenu);

        JMenuItem diskUsageMenuItem = new JMenuItem(new AbstractAction("Show") {
            public void actionPerformed(ActionEvent event) {
                Component parentComponent = AccountViewerApp.this.getFrame();
                Object message = null;

                MnyDb mnyDb = AccountViewerApp.this.getMnyDb();
                if ((mnyDb != null) && (mnyDb.getDb() != null)) {
                    try {
                        DiskUsageView view = new DiskUsageView(mnyDb);
                        message = view;
                    } catch (IOException e) {
                        message = e.getMessage();
                        LOGGER.error(e.getMessage());
                    }
                    if (message == null) {
                        message = "No data available";
                    }
                } else {
                    message = "No db data available";
                }

                JOptionPane.showMessageDialog(parentComponent, message, "Disk usage", JOptionPane.INFORMATION_MESSAGE);
            }
        });
        diskUsageMenu.setEnabled(false);
        diskUsageMenu.add(diskUsageMenuItem);
    }

    public Account getSelectedAccount() {
        return selectedAccount;
    }

    public void setSelectedAccount(Account selectedAccount) {
        this.selectedAccount = selectedAccount;
    }
}

/*******************************************************************************
 * Copyright (c) 2010 Hung Le
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *******************************************************************************/
package com.hungle.sunriise.accountviewer;

import javax.swing.table.AbstractTableModel;

import com.hungle.sunriise.mnyobject.Account;
import com.hungle.sunriise.mnyobject.MnyContext;

// TODO: Auto-generated Javadoc
/**
 * The Class AbstractAccountViewerTableModel.
 */
public abstract class AbstractAccountViewerTableModel extends AbstractTableModel {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 1L;

    /** The account. */
    private Account account;

    /** The mny context. */
    private MnyContext mnyContext;

    /**
     * Instantiates a new abstract account viewer table model.
     *
     * @param account the account
     */
    public AbstractAccountViewerTableModel(Account account) {
        this.account = account;
    }

    /**
     * Gets the account.
     *
     * @return the account
     */
    public Account getAccount() {
        return account;
    }

    /**
     * Gets the mny context.
     *
     * @return the mny context
     */
    public MnyContext getMnyContext() {
        return mnyContext;
    }

    /**
     * Sets the mny context.
     *
     * @param mnyContext the new mny context
     */
    public void setMnyContext(MnyContext mnyContext) {
        this.mnyContext = mnyContext;
    }
}

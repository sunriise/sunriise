package com.hungle.sunriise.accountviewer;

import java.math.BigDecimal;

import org.junit.Assert;
import org.junit.Test;

import com.hungle.sunriise.mnyobject.Account;
import com.hungle.sunriise.mnyobject.Currency;
import com.hungle.sunriise.mnyobject.impl.DefaultAccount;
import com.hungle.sunriise.mnyobject.impl.DefaultCurrency;

public class FormatterUtilsTest {
    @Test
    public void testFormatSecurityQuantity() {
        Assert.assertEquals("0.00000000", FormatterUtils.formatSecurityQuantity(0.0));

        Assert.assertEquals("0.00000000", FormatterUtils.formatSecurityQuantity(0.0000));
        Assert.assertEquals("0.00001000", FormatterUtils.formatSecurityQuantity(0.00001));
        Assert.assertEquals("0.00005000", FormatterUtils.formatSecurityQuantity(0.00005));

        Assert.assertEquals("0.00006000", FormatterUtils.formatSecurityQuantity(0.00006));

        Assert.assertEquals("1.00000000", FormatterUtils.formatSecurityQuantity(1.0));
        Assert.assertEquals("1.06666000", FormatterUtils.formatSecurityQuantity(1.06666));
    }

    @Test
    public void textCurrency() {
        Account account = new DefaultAccount();
        Currency currency = new DefaultCurrency();
        account.setCurrency(currency);

        account.getCurrency().setIsoCode(FormatterUtils.CURRENCY_CODE_US);
        Assert.assertEquals("$0.00", FormatterUtils.formatAmount(account, new BigDecimal(0)));
        Assert.assertEquals("$1.00", FormatterUtils.formatAmount(account, new BigDecimal(1)));
        Assert.assertEquals("$1.01", FormatterUtils.formatAmount(account, new BigDecimal(1.01)));
        Assert.assertEquals("$1.01", FormatterUtils.formatAmount(account, new BigDecimal(1.0101)));
        Assert.assertEquals("$1.01", FormatterUtils.formatAmount(account, new BigDecimal(1.0141)));
        Assert.assertEquals("$1.02", FormatterUtils.formatAmount(account, new BigDecimal(1.0151)));

        account.getCurrency().setIsoCode(FormatterUtils.CURRENCY_CODE_UK);
        Assert.assertEquals("£0.00", FormatterUtils.formatAmount(account, new BigDecimal(0)));

        account.getCurrency().setIsoCode(FormatterUtils.CURRENCY_CODE_CANADA);
        Assert.assertEquals("$0.00", FormatterUtils.formatAmount(account, new BigDecimal(0)));

        account.getCurrency().setIsoCode(FormatterUtils.CURRENCY_CODE_CHINA);
        Assert.assertEquals("￥0.00", FormatterUtils.formatAmount(account, new BigDecimal(0)));

        account.getCurrency().setIsoCode(FormatterUtils.CURRENCY_CODE_JAPAN);
        Assert.assertEquals("￥0", FormatterUtils.formatAmount(account, new BigDecimal(0)));

        account.getCurrency().setIsoCode(FormatterUtils.CURRENCY_CODE_US);
        Assert.assertEquals("$0.00", FormatterUtils.formatAmount(account, new BigDecimal(0)));

    }
}

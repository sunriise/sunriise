package com.hungle.sunriise.accountviewer.model;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Assert;
import org.junit.Test;

import com.hungle.sunriise.dbutil.TableAccountUtils;
import com.hungle.sunriise.mnyobject.Account;

public class AccountViewerDataModelTest {
    @Test
    public void testAspect() {
        AccountViewerDataModel model = new AccountViewerDataModel();
        PropertyChangeListener listener = new PropertyChangeListener() {

            @Override
            public void propertyChange(PropertyChangeEvent evt) {
                Assert.assertNotNull(evt);
                Assert.assertEquals("accounts", evt.getPropertyName());
                Assert.assertNotNull(evt.getSource());
                Assert.assertNotNull(evt.getNewValue());
                Assert.assertTrue(evt.getNewValue() instanceof List<?>);
                List<?> accounts = (List<?>) evt.getNewValue();
                Assert.assertTrue(accounts.size() == 0);
            }
        };
        model.addPropertyChangeListener(listener);

        Map<Integer, Account> accounts = new HashMap<Integer, Account>();
        model.setAccounts(TableAccountUtils.toAccountsList(accounts));

    }
}

# A script to extract the header page of a mny file
# Usage
# 
# python mny-header.py -i sunset-sample-pwd.mny -o header.pag
#

import argparse

def main():
  # Construct the argument parser
  ap = argparse.ArgumentParser()

  # Add the arguments to the parser
  ap.add_argument("-i", "--in", required=True,
   help="Input *.mny file")
  ap.add_argument("-o", "--out", required=True,
   help="Output fileName")
  args = vars(ap.parse_args())

  src = args['in']
  dst = args['out']
  print("in={}".format(src))
  print("out={}".format(dst))
  read_header(src, dst)

def read_header(src, dst):
  CHUNK_SIZE = 4096
  #src='../sunriise-core/src/main/resources/sunset-sample-pwd.mny'
  #src=in

  #dst='header.pag'
  #dst=out

  with open(src, "rb") as f:
    chunk = f.read(CHUNK_SIZE)
    with open(dst, "wb") as header_page:
      header_page.write(chunk)

if __name__ == "__main__":
    main()

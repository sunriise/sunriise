package com.hungle.sunriise.crypt;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.time.DurationFormatUtils;
import org.apache.logging.log4j.Logger;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import com.hungle.sunriise.io.FileUtils;
import com.hungle.sunriise.io.sample.MnySampleFile;
import com.hungle.sunriise.util.StopWatch;

public class PasswordUtilsTest {
    private static final Logger LOGGER = org.apache.logging.log4j.LogManager.getLogger(FileUtils.class);

    private static HeaderPage headerPage;
    private static MnySampleFile mnySampleFile;

    @BeforeClass
    public static final void init() throws IOException {
        PasswordUtilsTest.mnySampleFile = MnySampleFile.getMnySampleFile(MnySampleFile.SUNSET_SAMPLE_PWD_6_MNY);
        File sampleFile = MnySampleFile.getSampleFileFromModuleProject(mnySampleFile);
        PasswordUtilsTest.headerPage = new HeaderPage(sampleFile);
    }

    @Test
    public void testOneGoodPassword() throws IOException {
        String testPassword = mnySampleFile.getPassword();
        Assert.assertTrue(PasswordUtils.checkUsingHeaderPage(headerPage, testPassword));
    }

    @Test
    public void testOneBadPassword() throws IOException {
        String testPassword = mnySampleFile.getPassword();
        testPassword = testPassword + testPassword;
        Assert.assertFalse(PasswordUtils.checkUsingHeaderPage(headerPage, testPassword));
    }

    @Test
    public void testListGoodPassword() throws IOException {
        boolean found = false;

        List<String> list = new ArrayList<>();

        int max = 100000;
        PasswordUtils.populateRandomList(list, max);
        // not found
        List<String> candidates;

        candidates = new ArrayList<>(list);
        found = PasswordUtils.checkSequentially(headerPage, candidates);
        Assert.assertFalse(found);

        // password at the beginning
        candidates = new ArrayList<>(list);
        candidates.set(0, mnySampleFile.getPassword());
        found = PasswordUtils.checkSequentially(headerPage, candidates);
        Assert.assertTrue(found);

        // password at the middle
        candidates = new ArrayList<>(list);
        candidates.set(candidates.size() / 2, mnySampleFile.getPassword());
        found = PasswordUtils.checkSequentially(headerPage, candidates);
        Assert.assertTrue(found);

        // password at the end
        candidates = new ArrayList<>(list);
        candidates.set(candidates.size() - 1, mnySampleFile.getPassword());
        found = PasswordUtils.checkSequentially(headerPage, candidates);
        Assert.assertTrue(found);
    }

    @Test
    public void test1M() throws IOException {
        List<String> list = new ArrayList<>();
        int max = 1000000;
        PasswordUtils.populateRandomList(list, max);

        StopWatch stopWatch = new StopWatch();

        // Possible combinations = possible number of characters
        // to-the-power-of(Password length)
        //
//        When creating a password, the following characters are usually available:
//
//            Numbers (10 different: 0-9)
//            Letters (52 different: AZ and az)
//            Special characters (32 different).

        // common
        // letter: 52 (upper + lower case)
        // numer: 10
        // special chars: 4 !@#*
        // 7 characters
        // (52 + 10 + 4) = 62 to the power of 7 (62^7) = 3,521,614,606,208
        // (60 * 60 * 24) = 86,400
        // 3,521,614,606,208 / 333,333 = 10564854.383478383478383
        // 10564854.383478383478383 / 86,400 = 122.278407216184994 days
        List<String> candidates = list;
        try {
            stopWatch.click();
            boolean found = PasswordUtils.checkSequentially(headerPage, candidates);
            Assert.assertFalse(found);
        } finally {
            long delta = stopWatch.click();
            LOGGER.info("> size={}, delta={}, perSec={}", candidates.size(),
                    DurationFormatUtils.formatDurationHMS(delta), candidates.size() / (delta / 1000));
        }

    }
}

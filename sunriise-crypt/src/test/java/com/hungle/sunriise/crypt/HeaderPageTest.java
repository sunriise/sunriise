package com.hungle.sunriise.crypt;

import java.io.File;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;

import org.junit.Assert;
import org.junit.Test;

import com.healthmarketscience.jackcess.impl.JetFormat;
import com.hungle.sunriise.io.sample.MnySampleFile;

public class HeaderPageTest {
    @Test
    public void testHeaderPage() throws IOException {
        for (MnySampleFile mnySampleFile : MnySampleFile.SAMPLE_FILES) {
            if (mnySampleFile.isBackup()) {
                continue;
            }

            File mnyFile = MnySampleFile.getSampleFileFromModuleProject(mnySampleFile);

            HeaderPage headerPage = new HeaderPage(mnyFile);

            System.out.println(mnyFile);
            HeaderPage.printHeaderPage(headerPage);

            // check version
            Assert.assertEquals(JetFormat.VERSION_MSISAM, headerPage.getJetFormat());
            Assert.assertEquals(Charset.forName("UTF-16LE"), headerPage.getCharset());

            String fileName = mnyFile.getName();
            String password = mnySampleFile.getPassword();
            if (fileName.equalsIgnoreCase("money2001-pwd.mny")) {
                Assert.assertEquals(fileName, Boolean.FALSE, headerPage.isNewEncryption());
                Assert.assertEquals(fileName, Boolean.FALSE, headerPage.isUseSha1());
                Assert.assertEquals(fileName, password, headerPage.getEmbeddedDatabasePassword());
            } else {
                Assert.assertEquals(fileName, Boolean.TRUE, headerPage.isNewEncryption());
                if (fileName.contains("2002")) {
                    Assert.assertEquals(fileName, Boolean.FALSE, headerPage.isUseSha1());
                } else if (fileName.contains("2004")) {
                    Assert.assertEquals(fileName, Boolean.FALSE, headerPage.isUseSha1());
                } else if (fileName.contains("2005")) {
                    Assert.assertEquals(fileName, Boolean.FALSE, headerPage.isUseSha1());
                } else {
                    Assert.assertEquals(fileName, Boolean.TRUE, headerPage.isUseSha1());
                }
            }

            AbstractHeaderPagePasswordChecker checker = null;

            checker = new HeaderPagePasswordChecker(headerPage);
            Assert.assertEquals(Boolean.TRUE, checker.check(password));

            boolean checkUsingJDK = false;
            if (checkUsingJDK) {
                checker = new JDKHeaderPagePasswordChecker(headerPage);
                if (password != null) {
                    checker.check(password);
                }
            }
        }
    }

    @Test
    public void readJustHeaderPage() throws IOException {
        String fileName = "src/test/data/mny/sunset-sample-pwd.pag";
        File mnyFile = MnySampleFile.getSampleFileFromModuleProject(fileName);
        Assert.assertNotNull(mnyFile);

        HeaderPage headerPage = new HeaderPage(mnyFile);
        Assert.assertNotNull(headerPage);

        HeaderPage.printHeaderPage(headerPage);

        ByteBuffer buffer = headerPage.getBuffer();
        Assert.assertNotNull(buffer);

        // https://github.com/brianb/mdbtools/blob/master/HACKING
        byte jetVersion = buffer.get(0x14);
        System.out.println(String.format("version=%x", jetVersion));
    }
}

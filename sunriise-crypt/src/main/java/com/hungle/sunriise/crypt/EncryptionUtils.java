/*******************************************************************************
 * Copyright (c) 2010 Hung Le
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *******************************************************************************/
package com.hungle.sunriise.crypt;

import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.charset.Charset;
import java.util.Arrays;

import org.apache.logging.log4j.Logger;
import org.bouncycastle.crypto.Digest;
import org.bouncycastle.crypto.digests.MD5Digest;
import org.bouncycastle.crypto.digests.SHA1Digest;
import org.bouncycastle.crypto.engines.RC4Engine;
import org.bouncycastle.crypto.params.KeyParameter;

import com.healthmarketscience.jackcess.impl.ByteUtil;
import com.healthmarketscience.jackcess.impl.ColumnImpl;
import com.healthmarketscience.jackcess.impl.JetCryptCodecHandler;
import com.healthmarketscience.jackcess.impl.JetFormat;
import com.healthmarketscience.jackcess.impl.JetFormat.CodecType;
import com.healthmarketscience.jackcess.impl.MSISAMCryptCodecHandler;
import com.healthmarketscience.jackcess.impl.PageChannel;

// TODO: Auto-generated Javadoc
/**
 * The Class EncryptionUtils.
 */
public class EncryptionUtils {
    private static final Logger log = org.apache.logging.log4j.LogManager.getLogger(EncryptionUtils.class);

    /** The Constant NEW_ENCRYPTION. */
    private static final int NEW_ENCRYPTION = 0x6;

    /** The Constant PASSWORD_DIGEST_LENGTH. */
    private static final int PASSWORD_DIGEST_LENGTH = 0x10;

    /** The Constant USE_SHA1. */
    private static final int USE_SHA1 = 0x20;

    /** The Constant PASSWORD_LENGTH. */
    private static final int PASSWORD_LENGTH = 0x28;

    /** The Constant SALT_OFFSET. */
    private static final int SALT_OFFSET = 0x72;

    /** The Constant CRYPT_CHECK_START. */
    private static final int CRYPT_CHECK_START = 0x2e9;

    /** The Constant ENCRYPTION_FLAGS_OFFSET. */
    private static final int ENCRYPTION_FLAGS_OFFSET = 0x298;

    /** The Constant CHARSET_PROPERTY_PREFIX. */
    public static final String CHARSET_PROPERTY_PREFIX = "com.le.sunriise.charset.";

    /**
     * Creates the password digest.
     *
     * @param buffer   the buffer
     * @param password the password
     * @param charset  the charset
     * @return the byte[]
     */
    private static byte[] createPasswordDigest(ByteBuffer buffer, String password, Charset charset) {
        Digest digest = (((buffer.get(ENCRYPTION_FLAGS_OFFSET) & USE_SHA1) != 0) ? new SHA1Digest() : new MD5Digest());

        byte[] passwordBytes = new byte[PASSWORD_LENGTH];

        if (password != null) {
            ByteBuffer bb = ColumnImpl.encodeUncompressedText(password.toUpperCase(), charset);
            bb.get(passwordBytes, 0, Math.min(passwordBytes.length, bb.remaining()));
        }

        digest.update(passwordBytes, 0, passwordBytes.length);

        // Get digest value
        byte[] digestBytes = new byte[digest.getDigestSize()];
        digest.doFinal(digestBytes, 0);

        // Truncate to 128 bit to match Max key length as per MSDN
        if (digestBytes.length != PASSWORD_DIGEST_LENGTH) {
            digestBytes = ByteUtil.copyOf(digestBytes, PASSWORD_DIGEST_LENGTH);
        }

        return digestBytes;
    }

    /**
     * Gets the digest.
     *
     * @param buffer the buffer
     * @return the digest
     */
    private static Digest getDigest(ByteBuffer buffer) {
        Digest digest = (((buffer.get(ENCRYPTION_FLAGS_OFFSET) & USE_SHA1) != 0) ? new SHA1Digest() : new MD5Digest());
        return digest;
    }

    /**
     * Gets the codec handler name.
     *
     * @param buffer the buffer
     * @return the codec handler name
     */
    private static String getCodecHandlerName(ByteBuffer buffer) {
        if ((buffer.get(ENCRYPTION_FLAGS_OFFSET) & NEW_ENCRYPTION) != 0) {
            return MSISAMCryptCodecHandler.class.getName();
        }

        return JetCryptCodecHandler.class.getName();
    }

    /**
     * Concat.
     *
     * @param b1 the b1
     * @param b2 the b2
     * @return the byte[]
     */
    private static byte[] concat(byte[] b1, byte[] b2) {
        byte[] out = new byte[b1.length + b2.length];
        System.arraycopy(b1, 0, out, 0, b1.length);
        System.arraycopy(b2, 0, out, b1.length, b2.length);
        return out;
    }

    /**
     * Gets the password test bytes.
     *
     * @param buffer the buffer
     * @return the password test bytes
     */
    private static byte[] getPasswordTestBytes(ByteBuffer buffer) {
        byte[] encrypted4BytesCheck = new byte[4];

        int cryptCheckOffset = ByteUtil.getUnsignedByte(buffer, SALT_OFFSET);
        buffer.position(CRYPT_CHECK_START + cryptCheckOffset);
        buffer.get(encrypted4BytesCheck);

        return encrypted4BytesCheck;
    }

    /**
     * Gets the decrypted4 bytes check.
     *
     * @param encrypted4BytesCheck the encrypted4 bytes check
     * @param testEncodingKey      the test encoding key
     * @return the decrypted4 bytes check
     */
    private static byte[] getDecrypted4BytesCheck(byte[] encrypted4BytesCheck, byte[] testEncodingKey) {
        RC4Engine engine = new RC4Engine();
        // decrypt
        engine.init(false, new KeyParameter(testEncodingKey));

        // byte[] encrypted4BytesCheck = getPasswordTestBytes(buffer);
        byte[] decrypted4BytesCheck = new byte[4];
        engine.processBytes(encrypted4BytesCheck, 0, encrypted4BytesCheck.length, decrypted4BytesCheck, 0);

        return decrypted4BytesCheck;
        // if (!Arrays.equals(decrypted4BytesCheck, testBytes)) {
        // throw new IllegalStateException("Incorrect password provided");
        // }
    }

    /**
     * Gets the salt.
     *
     * @param buffer the buffer
     * @return the salt
     */
    private static byte[] getSalt(ByteBuffer buffer) {
        byte[] salt = new byte[8];
        buffer.position(SALT_OFFSET);
        buffer.get(salt);
        return salt;
    }

    /**
     * Parses the header.
     *
     * @param mdbFile  the mdb file
     * @param password the password
     * @throws IOException Signals that an I/O exception has occurred.
     */
    private static void parseHeader(final File mdbFile, final String password) throws IOException {
        boolean readOnly = true;
        RandomAccessFile randomAccessFile = null;
        FileChannel channel = null;
        try {
            final String mode = (readOnly ? "r" : "rw");
            randomAccessFile = new RandomAccessFile(mdbFile, mode);
            channel = randomAccessFile.getChannel();
            PageChannel pageChannel = null;
            try {
                boolean closeChannel = true;
                JetFormat format = JetFormat.getFormat(channel);
                boolean autoSync = true;

                pageChannel = new PageChannel(channel, closeChannel, format, autoSync);

                Charset charset = getDefaultCharset(format);

                ByteBuffer buffer = pageChannel.createPageBuffer();
                pageChannel.readPage(buffer, 0);

                StringBuilder sb = new StringBuilder();
                sb.append("format: " + format);
                sb.append("\n");

                sb.append("charset: " + charset);
                sb.append("\n");

                if (format.CODEC_TYPE == CodecType.MSISAM) {
                    EncryptionUtils.appendMSISAMInfo(buffer, password, charset, format, sb);
                }
                System.out.println(sb.toString());
            } finally {
                if (pageChannel != null) {
                    pageChannel.close();
                    pageChannel = null;
                }
                if (randomAccessFile != null) {
                    randomAccessFile.close();
                }
            }

        } finally {
            if (channel != null) {
                try {
                    channel.close();
                } finally {
                    channel = null;
                }
            }
        }
    }

    /**
     * Append msisam info.
     *
     * @param buffer   the buffer
     * @param password the password
     * @param charset  the charset
     * @param format
     * @param sb       the sb
     */
    public static void appendMSISAMInfo(ByteBuffer buffer, String password, Charset charset, JetFormat format,
            StringBuilder sb) {
        sb.append("codecHandlerName: " + getCodecHandlerName(buffer));
        sb.append("\n");

        Digest digest = getDigest(buffer);
        sb.append("digest: " + digest.getAlgorithmName());
        sb.append("\n");
        sb.append("\n");

        byte[] salt = getSalt(buffer);
        sb.append("salt: " + ByteUtil.toHexString(salt));
        sb.append("\n");

        byte[] pwdDigest = createPasswordDigest(buffer, password, charset);
        sb.append("pwdDigest: " + ByteUtil.toHexString(pwdDigest));
        sb.append("\n");

        final int SALT_LENGTH = 0x4;
        byte[] baseSalt = Arrays.copyOf(salt, SALT_LENGTH);
        byte[] testEncodingKey = concat(pwdDigest, salt);
        sb.append("testEncodingKey: " + ByteUtil.toHexString(testEncodingKey));
        sb.append("\n");

        byte[] encrypted4BytesCheck = getPasswordTestBytes(buffer);
        sb.append("encrypted4BytesCheck: " + ByteUtil.toHexString(encrypted4BytesCheck));
        sb.append("\n");

        byte[] decrypted4BytesCheck = getDecrypted4BytesCheck(encrypted4BytesCheck, testEncodingKey);
        sb.append("decrypted4BytesCheck: " + ByteUtil.toHexString(decrypted4BytesCheck));
        sb.append("\n");
//        sb.append(" / ");

        byte[] testBytes = baseSalt;
        sb.append("testBytes: " + ByteUtil.toHexString(testBytes));
        sb.append("\n");

        boolean newEncryption = HeaderPage.getNewEncryptionFlag(buffer);
        sb.append("newEncryption: " + newEncryption);
        sb.append("\n");

        if (!newEncryption) {
            try {
                String embeddedDatabasePassword = HeaderPage.readEmbeddedDatabasePassword(buffer, format, charset);
                if (embeddedDatabasePassword != null) {
                    sb.append("embeddedDatabasePassword: " + embeddedDatabasePassword);
                    sb.append("\n");
                }
            } catch (IOException e) {
                log.error(e.getMessage());
            }
        }

    }

    /**
     * Returns the default Charset for the given JetFormat. This may or may not be
     * platform specific, depending on the format, but can be overridden using a
     * system property composed of the prefix {@value #CHARSET_PROPERTY_PREFIX}
     * followed by the JetFormat version to which the charset should apply, e.g.
     * {@code "com.healthmarketscience.jackcess.charset.VERSION_3"}.
     *
     * @param format the format
     * @return the default charset
     */
    private static Charset getDefaultCharset(JetFormat format) {
        String csProp = System.getProperty(CHARSET_PROPERTY_PREFIX + format);
        if (csProp != null) {
            csProp = csProp.trim();
            if (csProp.length() > 0) {
                return Charset.forName(csProp);
            }
        }

        // use format default
        return format.CHARSET;
    }
}

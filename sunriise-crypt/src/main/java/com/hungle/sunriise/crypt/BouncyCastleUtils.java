/*******************************************************************************
 * Copyright (c) 2012 Hung Le
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *******************************************************************************/
package com.hungle.sunriise.crypt;

import org.apache.logging.log4j.Logger;
import org.bouncycastle.crypto.Digest;
import org.bouncycastle.crypto.digests.MD5Digest;
import org.bouncycastle.crypto.digests.SHA1Digest;
import org.bouncycastle.crypto.engines.RC4Engine;
import org.bouncycastle.crypto.params.KeyParameter;

// TODO: Auto-generated Javadoc
/**
 * The Class BouncyCastleUtils.
 */
public class BouncyCastleUtils {

    /** The Constant log. */
    private static final Logger LOGGER = org.apache.logging.log4j.LogManager.getLogger(BouncyCastleUtils.class);

    /**
     * Decrypt using r c4.
     *
     * @param engine     the engine
     * @param ciphertext the ciphertext
     * @param key        the key
     * @return the byte[]
     */
    public static byte[] decryptUsingRC4(RC4Engine engine, byte[] ciphertext, byte[] key) {
        // RC4Engine engine = null;
        // engine = getEngine();
        // engine = new RC4Engine();

        boolean forEncryption = false;
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("key.length=" + key.length + ", " + (key.length * 8));
        }
        engine.init(forEncryption, new KeyParameter(key));

        byte[] plaintext = new byte[4];
        engine.processBytes(ciphertext, 0, ciphertext.length, plaintext, 0);
        return plaintext;
    }

    /**
     * Creates the digest bytes.
     *
     * @param bytes   the bytes
     * @param useSha1 the use sha1
     * @return the byte[]
     */
    public static byte[] createDigestBytes(byte[] bytes, boolean useSha1) {
        // boolean useSha1 = headerPage.isUseSha1();
        Digest digest = (useSha1 ? new SHA1Digest() : new MD5Digest());
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("digest=" + digest.getAlgorithmName());
        }

        digest.update(bytes, 0, bytes.length);

        // Get digest value
        byte[] digestBytes = new byte[digest.getDigestSize()];
        digest.doFinal(digestBytes, 0);
        return digestBytes;
    }

}

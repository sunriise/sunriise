package com.hungle.sunriise.crypt;

import java.io.File;
import java.io.IOException;

import org.apache.logging.log4j.Logger;

public class Recover2001Password {
    private static final Logger LOGGER = org.apache.logging.log4j.LogManager.getLogger(Recover2001Password.class);

    public static void main(String[] args) {
        File dbFile = null;

        if (args.length == 1) {
            dbFile = new File(args[0]);
        } else {
            Class<Recover2001Password> clz = Recover2001Password.class;
            System.out.println("Usage: java " + clz.getName() + " 2001file.mny");
            System.exit(1);
        }

        if (!dbFile.exists()) {
            System.err.println("Does not exist, dbFile=" + dbFile);
            System.exit(1);
        }

        try {
            HeaderPage headerPage = new HeaderPage(dbFile);
            if (headerPage.isNewEncryption()) {
                System.err.println("Cannot recover password for this version of *.mny file");
                return;
            }

            String password = headerPage.getEmbeddedDatabasePassword();
            System.out.println("password=" + password);
        } catch (IOException e) {
            LOGGER.error(e, e);
        }

    }

}

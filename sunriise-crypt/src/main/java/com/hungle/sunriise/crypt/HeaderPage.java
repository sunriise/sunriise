/*******************************************************************************
 * Copyright (c) 2010 Hung Le
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *******************************************************************************/
package com.hungle.sunriise.crypt;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.channels.FileChannel;
import java.nio.charset.Charset;
import java.util.Arrays;

import org.apache.commons.io.HexDump;
import org.apache.logging.log4j.Logger;

import com.healthmarketscience.jackcess.impl.ByteUtil;
import com.healthmarketscience.jackcess.impl.ColumnImpl;
import com.healthmarketscience.jackcess.impl.JetFormat;
import com.hungle.sunriise.backup.BackupFileUtils;

// TODO: Auto-generated Javadoc
/**
 * The Class HeaderPage.
 */
public class HeaderPage {

    /** The Constant log. */
    private static final Logger log = org.apache.logging.log4j.LogManager.getLogger(HeaderPage.class);

    /** The Constant CHARSET_PROPERTY_PREFIX. */
    private static final String CHARSET_PROPERTY_PREFIX = "com.healthmarketscience.jackcess.charset.";

    /** The Constant DEFAULT_BYTE_ORDER. */
    private static final ByteOrder DEFAULT_BYTE_ORDER = ByteOrder.LITTLE_ENDIAN;

    /** The Constant ENCRYPTION_FLAGS_OFFSET. */
    private static final int ENCRYPTION_FLAGS_OFFSET = 0x298;

    /** The Constant NEW_ENCRYPTION. */
    private static final int NEW_ENCRYPTION = 0x6;

    /** The Constant USE_SHA1. */
    private static final int USE_SHA1 = 0x20;

    /** The Constant SALT_OFFSET. */
    private static final int SALT_OFFSET = 0x72;

    /** The Constant SALT_LENGTH. */
    private static final int SALT_LENGTH = 0x4;

    /** The Constant CRYPT_CHECK_START. */
    private static final int CRYPT_CHECK_START = 0x2e9;

    /** The jet format. */
    private JetFormat jetFormat;

    /** The charset. */
    private Charset charset;

    /** The buffer. */
    private ByteBuffer buffer;

    /** The new encryption. */
    private boolean newEncryption;

    /** The embedded database password. */
    private String embeddedDatabasePassword = null;

    /** The use sha1. */
    private boolean useSha1;

    /** The salt. */
    private byte[] salt;

    /** The base salt. */
    private byte[] baseSalt;

    /** The encrypted4 bytes check. */
    private byte[] encrypted4BytesCheck;

    /** The db file. */
    private File dbFile;

    /**
     * Instantiates a new header page.
     *
     * @param dbFile the db file
     * @throws IOException Signals that an I/O exception has occurred.
     */
    public HeaderPage(File dbFile) throws IOException {
        super();
        this.dbFile = parse(dbFile);
    }

    /**
     * Parses the.
     *
     * @param dbFile the db file
     * @return the file
     * @throws IOException Signals that an I/O exception has occurred.
     */
    private File parse(File dbFile) throws IOException {
        String fileName = dbFile.getName();

        if (BackupFileUtils.isMnyBackupFile(fileName)) {
            dbFile = BackupFileUtils.createBackupAsTempFile(dbFile, true, 4096L * 2);
        }

        RandomAccessFile rFile = null;
        FileChannel fileChannel = null;
        try {
            rFile = new RandomAccessFile(dbFile, "r");
            rFile.seek(0L);
            fileChannel = rFile.getChannel();

            jetFormat = JetFormat.getFormat(fileChannel);

            charset = getDefaultCharset(jetFormat);

            buffer = readHeaderPage(jetFormat, fileChannel);

            newEncryption = HeaderPage.getNewEncryptionFlag(buffer);

            if (newEncryption) {
                useSha1 = (buffer.get(ENCRYPTION_FLAGS_OFFSET) & USE_SHA1) != 0;

                salt = new byte[8];
                buffer.position(SALT_OFFSET);
                buffer.get(salt);

                baseSalt = Arrays.copyOf(salt, SALT_LENGTH);

                encrypted4BytesCheck = readEncrypted4BytesCheck(buffer);
            } else {
                embeddedDatabasePassword = readEmbeddedDatabasePassword();
            }
        } finally {
            if (fileChannel != null) {
                try {
                    fileChannel.close();
                } catch (IOException e) {
                    log.warn(e);
                } finally {
                    fileChannel = null;
                }
            }

            if (rFile != null) {
                try {
                    rFile.close();
                } catch (IOException e) {
                    log.warn(e);
                } finally {
                    rFile = null;
                }
            }
        }

        return dbFile;
    }

    public static final boolean getNewEncryptionFlag(ByteBuffer buffer) {
        boolean newEncryption = false;
        if ((buffer.get(ENCRYPTION_FLAGS_OFFSET) & NEW_ENCRYPTION) != 0) {
            newEncryption = true;
        } else {
            newEncryption = false;
        }
        return newEncryption;
    }

    /**
     * Gets the default charset.
     *
     * @param format the format
     * @return the default charset
     */
    private static Charset getDefaultCharset(JetFormat format) {
        String csProp = System.getProperty(CHARSET_PROPERTY_PREFIX + format);
        if (log.isDebugEnabled()) {
            log.debug("csProp=" + csProp);
        }
        if (csProp != null) {
            csProp = csProp.trim();
            if (csProp.length() > 0) {
                return Charset.forName(csProp);
            }
        }

        // use format default
        Charset cs = format.CHARSET;
        if (log.isDebugEnabled()) {
            log.debug("format.CHARSET=" + cs);
        }
        return cs;
    }

    /**
     * Creates the buffer.
     *
     * @param size the size
     * @return the byte buffer
     */
    private static ByteBuffer createBuffer(int size) {
        return createBuffer(size, DEFAULT_BYTE_ORDER);
    }

    /**
     * Creates the buffer.
     *
     * @param size  the size
     * @param order the order
     * @return the byte buffer
     */
    private static ByteBuffer createBuffer(int size, ByteOrder order) {
        ByteBuffer buffer = ByteBuffer.allocate(size);
        buffer.order(order);
        return buffer;
    }

    /**
     * Read header page.
     *
     * @param jetFormat   the jet format
     * @param fileChannel the file channel
     * @return the byte buffer
     * @throws IOException Signals that an I/O exception has occurred.
     */
    private static ByteBuffer readHeaderPage(JetFormat jetFormat, FileChannel fileChannel) throws IOException {
        int size = jetFormat.PAGE_SIZE;
        log.info("readHeaderPage, size=" + size);

        ByteBuffer buffer = createBuffer(size);
        readPage(buffer, jetFormat, fileChannel);
        return buffer;
    }

    /**
     * Read page.
     *
     * @param buffer      the buffer
     * @param jetFormat   the jet format
     * @param fileChannel the file channel
     * @throws IOException Signals that an I/O exception has occurred.
     */
    private static void readPage(ByteBuffer buffer, JetFormat jetFormat, FileChannel fileChannel) throws IOException {
        int pageNumber = 0;
        long pageSize = jetFormat.PAGE_SIZE;
        if (log.isDebugEnabled()) {
            log.debug("readPage, pageNumber=" + pageNumber + ", pageSize=" + pageSize);
        }

        if (log.isDebugEnabled()) {
            log.debug("Reading in page " + Integer.toHexString(pageNumber));
        }
        buffer.clear();
        int bytesRead = fileChannel.read(buffer, pageNumber * pageSize);
        buffer.flip();
        if (bytesRead != jetFormat.PAGE_SIZE) {
            throw new IOException("Failed attempting to read " + jetFormat.PAGE_SIZE + " bytes from page " + pageNumber
                    + ", only read " + bytesRead);
        }

        if (pageNumber == 0) {
            // de-mask header (note, page 0 never has additional encoding)
            applyHeaderMask(buffer, jetFormat);
        } else {
            throw new IOException("Cannot read non-header page, pageNumber=" + pageNumber);
        }
    }

    /**
     * Apply header mask.
     *
     * @param buffer    the buffer
     * @param jetFormat the jet format
     */
    private static void applyHeaderMask(ByteBuffer buffer, JetFormat jetFormat) {
        // de/re-obfuscate the header
        byte[] headerMask = jetFormat.HEADER_MASK;
        for (int index = 0; index < headerMask.length; ++index) {
            int pos = index + jetFormat.OFFSET_MASKED_HEADER;
            byte b = (byte) (buffer.get(pos) ^ headerMask[index]);
            buffer.put(pos, b);
        }
    }

    /**
     * Gets the jet format.
     *
     * @return the jet format
     */
    public JetFormat getJetFormat() {
        return jetFormat;
    }

    /**
     * Gets the charset.
     *
     * @return the charset
     */
    public Charset getCharset() {
        return charset;
    }

    /**
     * Gets the buffer.
     *
     * @return the buffer
     */
    public ByteBuffer getBuffer() {
        return buffer;
    }

    /**
     * Checks if is new encryption.
     *
     * @return true, if is new encryption
     */
    public boolean isNewEncryption() {
        return newEncryption;
    }

    /**
     * Read embedded database password.
     *
     * @return the string
     * @throws IOException Signals that an I/O exception has occurred.
     */
    private String readEmbeddedDatabasePassword() throws IOException {
        JetFormat jetFormat = getJetFormat();
        ByteBuffer buffer = getBuffer();
        Charset charset = getCharset();
        return HeaderPage.readEmbeddedDatabasePassword(buffer, jetFormat, charset);
    }

    /**
     * Read embedded database password.
     *
     * @param buffer    the buffer
     * @param jetFormat the jet format
     * @return the string
     * @throws IOException Signals that an I/O exception has occurred.
     */
    static final String readEmbeddedDatabasePassword(ByteBuffer buffer, JetFormat jetFormat, Charset charset)
            throws IOException {
        byte[] pwdBytes = new byte[jetFormat.SIZE_PASSWORD];
        buffer.position(jetFormat.OFFSET_PASSWORD);
        buffer.get(pwdBytes);

        if (log.isDebugEnabled()) {
            log.debug("preMask pwdBytes=" + pwdBytes.length);
        }

        // de-mask password using extra password mask if necessary (the
        // extra
        // password mask is generated from the database creation date stored
        // in
        // the header)
        byte[] pwdMask = getPasswordMask(buffer, jetFormat);
        if (pwdMask != null) {
            for (int i = 0; i < pwdBytes.length; ++i) {
                pwdBytes[i] ^= pwdMask[i % pwdMask.length];
            }
        }

        boolean hasPassword = false;
        for (int i = 0; i < pwdBytes.length; ++i) {
            if (pwdBytes[i] != 0) {
                hasPassword = true;
                break;
            }
        }

        if (!hasPassword) {
            return null;
        }

        if (log.isDebugEnabled()) {
            log.debug("postMask pwdBytes=" + pwdBytes.length);
        }

//        Charset charset = getCharset();
        if (log.isDebugEnabled()) {
            log.info("charset=" + charset);
        }
        String password = ColumnImpl.decodeUncompressedText(pwdBytes, charset);

        // remove any trailing null chars
        int idx = password.indexOf('\0');
        if (idx >= 0) {
            password = password.substring(0, idx);
        }

        return password;
    }

    /**
     * Gets the password mask.
     *
     * @param buffer the buffer
     * @param format the format
     * @return the password mask
     */
    private static byte[] getPasswordMask(ByteBuffer buffer, JetFormat format) {
        // get extra password mask if necessary (the extra password mask is
        // generated from the database creation date stored in the header)
        int pwdMaskPos = format.OFFSET_HEADER_DATE;
        if (pwdMaskPos < 0) {
            return null;
        }

        buffer.position(pwdMaskPos);
        double dateVal = Double.longBitsToDouble(buffer.getLong());
        if (log.isDebugEnabled()) {
            log.debug("dateVal=" + dateVal);
        }

        byte[] pwdMask = new byte[4];
        ByteBuffer.wrap(pwdMask).order(DEFAULT_BYTE_ORDER).putInt((int) dateVal);

        return pwdMask;
    }

    /**
     * Gets the embedded database password.
     *
     * @return the embedded database password
     */
    public String getEmbeddedDatabasePassword() {
        return embeddedDatabasePassword;
    }

    /**
     * Checks if is use sha1.
     *
     * @return true, if is use sha1
     */
    public boolean isUseSha1() {
        return useSha1;
    }

    /**
     * Gets the salt.
     *
     * @return the salt
     */
    public byte[] getSalt() {
        return salt;
    }

    /**
     * Gets the base salt.
     *
     * @return the base salt
     */
    public byte[] getBaseSalt() {
        return baseSalt;
    }

    /**
     * Read encrypted4 bytes check.
     *
     * @param buffer the buffer
     * @return the byte[]
     */
    private static byte[] readEncrypted4BytesCheck(ByteBuffer buffer) {
        byte[] encrypted4BytesCheck = new byte[4];

        int cryptCheckOffset = ByteUtil.getUnsignedByte(buffer, SALT_OFFSET);
        buffer.position(CRYPT_CHECK_START + cryptCheckOffset);
        buffer.get(encrypted4BytesCheck);

        return encrypted4BytesCheck;
    }

    /**
     * Gets the encrypted4 bytes check.
     *
     * @return the encrypted4 bytes check
     */
    public byte[] getEncrypted4BytesCheck() {
        return encrypted4BytesCheck;
    }

    /**
     * To hex string.
     *
     * @param bytes the bytes
     * @return the string
     */
    public static String toHexString(byte[] bytes) {
        if (bytes == null) {
            return null;
        }

        String str = null;
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        try {
            HexDump.dump(bytes, 0, stream, 0);
            stream.flush();
        } catch (ArrayIndexOutOfBoundsException e) {
            log.warn(e);
        } catch (IllegalArgumentException e) {
            log.warn(e);
        } catch (IOException e) {
            log.warn(e);
        } finally {
            if (stream != null) {
                str = new String(stream.toByteArray());
            }
        }

        return str;
    }

    /**
     * Gets the db file.
     *
     * @return the db file
     */
    public File getDbFile() {
        return dbFile;
    }

    /**
     * Prints the header page.
     *
     * @param headerPage  the header page
     * @param indentation the indentation
     */
    public static void printHeaderPage(HeaderPage headerPage, String indentation) {
        System.out.println("");

        System.out.println(indentation + "getJetFormat: " + headerPage.getJetFormat());
        System.out.println(indentation + "getJetFormat.PAGE_SIZE: " + headerPage.getJetFormat().PAGE_SIZE);
        System.out.println(indentation + "getCharset: " + headerPage.getCharset());

        System.out.println(indentation + "isNewEncryption: " + headerPage.isNewEncryption());
        if (!headerPage.isNewEncryption()) {
            System.out
                    .println(indentation + "getEmbeddedDatabasePassword: " + headerPage.getEmbeddedDatabasePassword());
        }

        System.out.println(indentation + "isUseSha1: " + headerPage.isUseSha1());
        System.out.println(indentation + "getSalt: " + HeaderPage.toHexString(headerPage.getSalt()));
        System.out.println(indentation + "getBaseSalt: " + HeaderPage.toHexString(headerPage.getBaseSalt()));
        System.out.println(
                indentation + "encrypted4BytesCheck: " + HeaderPage.toHexString(headerPage.getEncrypted4BytesCheck()));

        System.out.println("");
    }

    /**
     * Prints the header page.
     *
     * @param headerPage the header page
     */
    public static void printHeaderPage(HeaderPage headerPage) {
        printHeaderPage(headerPage, "");
    }
}
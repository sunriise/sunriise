package com.hungle.sunriise.crypt;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Desktop;
import java.awt.Dimension;
import java.awt.HeadlessException;
import java.awt.event.ActionEvent;
import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import javax.swing.AbstractAction;
import javax.swing.BorderFactory;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

import org.apache.logging.log4j.Logger;
import org.fife.ui.rsyntaxtextarea.RSyntaxTextArea;
import org.fife.ui.rtextarea.RTextScrollPane;

import com.healthmarketscience.jackcess.impl.ByteUtil;
import com.hungle.sunriise.gui.FileDropHandler;

public class MnyHeaderFrame extends JFrame {
    private static final Logger LOGGER = org.apache.logging.log4j.LogManager.getLogger(MnyHeaderFrame.class);

    private static final String OPEN_EXPORTS_DIRECTORY = "Open exports directory";

    private static final String OPEN_WIKI_PAGE = "Open wiki page";

    private ExecutorService threadPool = Executors.newCachedThreadPool();

    private File exportDir;

    private final class HeaderDropHandler extends FileDropHandler {
        private final JPanel view;

        private HeaderDropHandler(JPanel view) {
            this.view = view;
        }

        @Override
        public void handleFile(File file) {
            if (!file.isFile()) {
                String message = String.format("'%s' is not a file.", file.getName());
                String title = "File error";
                JOptionPane.showMessageDialog(view, message, title, JOptionPane.ERROR_MESSAGE);
                return;
            }
            String name = file.getName();
            if (!name.endsWith(".mny")) {
                String message = String.format("'%s' is not a *.mny file.", file.getName());
                String title = "File error";
                JOptionPane.showMessageDialog(view, message, title, JOptionPane.ERROR_MESSAGE);
                return;
            }

            try {
                HeaderPage headerPage = new HeaderPage(file);
                processHeaderPage(headerPage);
            } catch (Exception e) {
                LOGGER.error(e.getMessage(), e);
                String message = e.getMessage();
                String title = "Exception";
                JOptionPane.showMessageDialog(view, message, title, JOptionPane.ERROR_MESSAGE);
            }
        }
    }

    public MnyHeaderFrame() throws HeadlessException {
        super("MnyHeaderParser");

        File cwd = new File(".");
        exportDir = new File(cwd, "exports");
        if (!exportDir.exists()) {
            exportDir.mkdirs();
        }

        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        Dimension preferredSize = new Dimension(400, 400);
        setPreferredSize(preferredSize);

        init(getContentPane());
    }

    public void processHeaderPage(HeaderPage headerPage) {
        LOGGER.info("> processHeaderPage");

        LOGGER.info("dbFile={}", headerPage.getDbFile());

        LOGGER.info("newEncryption={}", headerPage.isNewEncryption());

        LOGGER.info("baseSalt={}", toHexString(headerPage.getBaseSalt()));
        LOGGER.info("encrypted4BytesCheck={}", toHexString(headerPage.getEncrypted4BytesCheck()));
        LOGGER.info("salt={}", toHexString(headerPage.getSalt()));
        LOGGER.info("charset={}", headerPage.getCharset());
        LOGGER.info("embeddedDatabasePassword={}", headerPage.getEmbeddedDatabasePassword());
        LOGGER.info("jetFormat={}", headerPage.getJetFormat());
    }

    private Object toHexString(byte[] bytes) {
        return (bytes != null) ? ByteUtil.toHexString(bytes) : bytes;
    }

    private void init(Container contentPane) {
        addMenus(contentPane);

        final JPanel view = new JPanel();
        view.setLayout(new BorderLayout());
        view.setBorder(BorderFactory.createEmptyBorder(3, 3, 3, 3));
        contentPane.add(view);

        addCenter(view);
    }

    private void addMenus(Container contentPane) {
        JMenuBar menuBar = new JMenuBar();

        JMenu menu = null;
        JMenuItem menuItem = null;

        menu = new JMenu("File");
        menuBar.add(menu);

        menuItem = new JMenuItem(new AbstractAction(OPEN_EXPORTS_DIRECTORY) {

            @Override
            public void actionPerformed(ActionEvent event) {
                try {
                    Desktop.getDesktop().open(exportDir);
                } catch (IOException e) {
                    LOGGER.error(e.getMessage(), e);
                }

            }
        });
        menu.add(menuItem);

        menuItem = new JMenuItem(new AbstractAction(OPEN_WIKI_PAGE) {

            @Override
            public void actionPerformed(ActionEvent event) {
                try {
                    URI uri = new URI("https://bitbucket.org/hleofxquotesteam/dist-sunriise/wiki/Home");
                    Desktop.getDesktop().browse(uri);
                } catch (Exception e) {
                    LOGGER.error(e.getMessage(), e);
                }

            }
        });
        menu.add(menuItem);

        menu.addSeparator();

        menuItem = new JMenuItem(new AbstractAction("Exit") {

            @Override
            public void actionPerformed(ActionEvent e) {
                System.exit(0);

            }
        });
        menu.add(menuItem);

        setJMenuBar(menuBar);
    }

    private void addCenter(JPanel parent) {
        String eol = "\r\n";
        JPanel view = new JPanel();
        view.setLayout(new BorderLayout());

        parent.add(view, BorderLayout.CENTER);

        RSyntaxTextArea textArea = new RSyntaxTextArea();
        textArea.setTransferHandler(new HeaderDropHandler(view));

        // textArea.setSyntaxEditingStyle(SyntaxConstants.SYNTAX_STYLE_JAVASCRIPT);
        RTextScrollPane sp = new RTextScrollPane(textArea);
        // accountJsonTextArea = textArea;
        textArea.setEditable(false);

        textArea.append("To export, just drag-and-drop a *.mny file here." + eol);

        textArea.append(eol);
        textArea.append("To see the export file(s):" + eol);
        textArea.append("  Select menu 'File -> " + OPEN_EXPORTS_DIRECTORY + "'" + eol);
        // textArea.append(" to open the export directory." + eol);

        textArea.append(eol);
        textArea.append("To get additional info:" + eol);
        textArea.append("  Select menu 'File -> " + OPEN_WIKI_PAGE + "'" + eol);

        // textArea.append(eol);
        // textArea.append("For more information see:" + eol);
        // textArea.append("
        // https://bitbucket.org/hleofxquotesteam/dist-sunriise/wiki/Home" +
        // eol);

        view.add(sp, BorderLayout.CENTER);
    }

    public static void main(String[] args) {
        final MnyHeaderFrame mainView = new MnyHeaderFrame();
        Runnable doRun = new Runnable() {
            @Override
            public void run() {
                mainView.showMainFrame();
            }
        };
        SwingUtilities.invokeLater(doRun);
        // doRun.run();
    }

    private void showMainFrame() {
        setLocation(100, 100);
        pack();
        setVisible(true);
    }
}

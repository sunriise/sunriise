/*******************************************************************************
 * Copyright (c) 2010 Hung Le
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *******************************************************************************/
package com.hungle.sunriise.crypt;

import java.io.IOException;

// TODO: Auto-generated Javadoc
/**
 * The Class JDKHeaderPagePasswordChecker.
 */
public class JDKHeaderPagePasswordChecker extends AbstractHeaderPagePasswordChecker {

    /**
     * Instantiates a new JDK header page password checker.
     *
     * @param headerPage the header page
     * @throws IOException Signals that an I/O exception has occurred.
     */
    public JDKHeaderPagePasswordChecker(HeaderPage headerPage) throws IOException {
        super(headerPage);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.hungle.sunriise.crypt.AbstractHeaderPagePasswordChecker#
     * createDigestBytes(byte[], boolean)
     */
    @Override
    protected byte[] createDigestBytes(byte[] passwordBytes, boolean useSha1) {
        return JDKUtils.createDigestBytes(passwordBytes, useSha1);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.hungle.sunriise.crypt.AbstractHeaderPagePasswordChecker#
     * decryptUsingRC4(byte[], byte[])
     */
    @Override
    protected byte[] decryptUsingRC4(byte[] encrypted4BytesCheck, byte[] testKey) {
        boolean useJDK = true;
        if (useJDK) {
            return JDKUtils.decryptUsingRC4(encrypted4BytesCheck, testKey);
        } else {
            return decryptUsingLocalRC4(encrypted4BytesCheck, testKey);
        }
    }

    /**
     * Decrypt using local r c4.
     *
     * @param encrypted4BytesCheck the encrypted4 bytes check
     * @param testKey              the test key
     * @return the byte[]
     */
    private byte[] decryptUsingLocalRC4(byte[] encrypted4BytesCheck, byte[] testKey) {
        byte[] decrypted4BytesCheck = new byte[4];
        return decrypted4BytesCheck;
    }

}

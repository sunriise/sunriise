package com.hungle.sunriise.stream;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;
import java.util.stream.Stream;

import org.apache.logging.log4j.Logger;
import org.junit.Assert;
import org.junit.Test;

import com.healthmarketscience.jackcess.Database;
import com.hungle.sunriise.dbutil.TableAccountUtils;
import com.hungle.sunriise.io.MnyDb;
import com.hungle.sunriise.io.sample.MnySampleFile;
import com.hungle.sunriise.mnyobject.EnumAccountType;
import com.hungle.sunriise.mnyobject.InvestmentInfo;
import com.hungle.sunriise.mnyobject.MnyContext;
import com.hungle.sunriise.mnyobject.Transaction;
import com.hungle.sunriise.mnyobject.impl.IAKeyValue;
import com.hungle.sunriise.mnyobject.impl.InvestmentActivity;
import com.hungle.sunriise.mnyobject.impl.InvestmentActivityTable;
import com.hungle.sunriise.util.MnyContextUtils;

public class StreamTest {

    /** The Constant LOGGER. */
    private static final Logger LOGGER = org.apache.logging.log4j.LogManager.getLogger(StreamTest.class);

    /** The Constant DEFAULT_COMPLEX_FILENAME. */
    private static final String DEFAULT_COMPLEX_FILENAME = MnySampleFile.LOCAL_LARGE_FILE_MNY;

    public class InvestmentAccountCollector {

        private long count;

        private final Map<String, List<Transaction>> activities = new TreeMap<String, List<Transaction>>();

        public void collect(Transaction transaction) {
            count++;

            InvestmentInfo info = transaction.getInvestmentInfo();
            InvestmentActivity activity = info.getActivity();
            List<Transaction> list = activities.get(activity.getLabel());
            if (list == null) {
                list = new ArrayList<Transaction>();
                activities.put(activity.getLabel(), list);
            }
            list.add(transaction);

            Integer type = activity.getType();

            IAKeyValue value = InvestmentActivityTable.getInstance().getByType(type);
            if (value == null) {
                LOGGER.warn("Unknown InvestmentActivity type={}", type);
            }

        }

        public long getCount() {
            return count;
        }

        public Map<String, List<Transaction>> getActivities() {
            return activities;
        }

        public Stream<Entry<String, List<Transaction>>> getActivityStream() {
            return getActivities().entrySet().stream();
        }

    }

    @Test
    public void testInvestmentTransactions() throws IOException {
        String fileName = DEFAULT_COMPLEX_FILENAME;
        String password = null;

        File dbFile = new File(fileName);
        if (!dbFile.exists()) {
            return;
        }

        try (MnyDb mnyDb = new MnyDb(dbFile, password)) {
            Assert.assertNotNull(mnyDb);

            Database db = mnyDb.getDb();
            Assert.assertNotNull(db);

            MnyContext mnyContext = MnyContextUtils.createMnyContext(mnyDb);

            // select only investment account
            mnyContext.getAccountStream().filter(account -> {
                Assert.assertNotNull(account);
                return EnumAccountType.isInvestment(account);
            }).forEach(account -> {
                Assert.assertNotNull(account);
                try {
                    TableAccountUtils.addTransactionsToAccount(account, mnyContext);
                    LOGGER.info("Account: " + account.getName());

                    InvestmentAccountCollector collector = new InvestmentAccountCollector();
                    try {
                        account.getTransactionStream().filter(t -> {
                            return t.isInvestment();
                        }).forEach(t -> {
                            collector.collect(t);
                        });
                    } finally {
                        LOGGER.info("  activityCount={}", collector.getCount());
                        collector.getActivityStream().forEach(e -> {
                            LOGGER.info("    activity={}, count={}", e.getKey(), e.getValue().size());
                        });
                    }
                } catch (IOException e) {
                    LOGGER.error(e.getMessage());
                }
            });
        }
    }

}

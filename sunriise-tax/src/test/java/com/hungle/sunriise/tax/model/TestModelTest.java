package com.hungle.sunriise.tax.model;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import org.junit.Assert;
import org.junit.Test;

public class TestModelTest {
    @Test
    public void testAspect() {
        TestModel model = new TestModel();
        PropertyChangeListener listener = new PropertyChangeListener() {

            @Override
            public void propertyChange(PropertyChangeEvent evt) {
                Assert.assertNotNull(evt);
                Assert.assertEquals("id", evt.getPropertyName());
                Assert.assertNotNull(evt.getSource());
                Assert.assertNotNull(evt.getNewValue());
                Assert.assertTrue(evt.getNewValue() instanceof Integer);
                Integer id = (Integer) evt.getNewValue();
                Assert.assertEquals(0, id.intValue());
            }
        };
        model.addPropertyChangeListener(listener);

        // Map<Integer, Account> accounts = new HashMap<Integer, Account>();
        // model.setAccounts(MnyObjectUtil.toAccountsList(accounts));

        int id = 0;
        model.setId(id);

    }
}

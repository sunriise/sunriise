package com.hungle.sunriise.tax.model;

import java.beans.Introspector;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;

import org.apache.logging.log4j.Logger;

public aspect BeanMakerAspect {
    private static final Logger LOGGER = org.apache.logging.log4j.LogManager.getLogger(BeanMakerAspect.class);

	// FIXME: package name
    // All class in model package will have bean support
    declare parents:com.hungle.sunriise.tax.model.*Model implements BeanSupport;

    // The aspect introduces a member propertyChangeSupport of type
    // PropertyChangeSupport
    private transient PropertyChangeSupport BeanSupport.propertyChangeSupport;

    /*
     * Similarly, you introduce two new methods addPropertyChangeListener() and
     * removePropertyChangeListener() with public access. T
     */
    public void BeanSupport.addPropertyChangeListener(PropertyChangeListener listener) {
        if (propertyChangeSupport == null) {
            propertyChangeSupport = new PropertyChangeSupport(this);
        }

        propertyChangeSupport.addPropertyChangeListener(listener);
    }

    public void BeanSupport.removePropertyChangeListener(PropertyChangeListener listener) {
        if (propertyChangeSupport != null) {
            propertyChangeSupport.removePropertyChangeListener(listener);
        }
    }

    pointcut beanPropertyChange(BeanSupport bean, Object newValue):execution(void BeanSupport+.set*(*))&&args(newValue)&&this(bean);

    void around(BeanSupport bean, Object newValue):beanPropertyChange(bean,newValue){
        if (bean.propertyChangeSupport == null) {
            proceed(bean, newValue);
        } else {
            String methodName = thisJoinPointStaticPart.getSignature().getName();
            String propertyName = Introspector.decapitalize(methodName.substring(3));
            if (LOGGER.isDebugEnabled()) {
                LOGGER.debug("> around,"
                        + " jointPoint=" + thisJoinPoint.toString()
                        + " methodName=" + methodName + ", propertyName=" + propertyName
                        + " newValue=" + newValue
                        );
            }
            proceed(bean, newValue);

            bean.propertyChangeSupport.firePropertyChange(propertyName, null, newValue);
        }
    }

}

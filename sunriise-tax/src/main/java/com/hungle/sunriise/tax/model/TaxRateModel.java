package com.hungle.sunriise.tax.model;

import com.hungle.sunriise.account.tax.TaxRateInfo;
import com.hungle.sunriise.mnyobject.impl.TaxBracket;

public class TaxRateModel {
    private Double bracketLow_0;
    private Double bracketLow_1;
    private Double bracketLow_2;
    private Double bracketLow_3;
    private Double bracketLow_4;
    private Double bracketLow_5;

    private Double bracketHigh_0;
    private Double bracketHigh_1;
    private Double bracketHigh_2;
    private Double bracketHigh_3;
    private Double bracketHigh_4;
    private Double bracketHigh_5;

    private Double bracketRate_0;
    private Double bracketRate_1;
    private Double bracketRate_2;
    private Double bracketRate_3;
    private Double bracketRate_4;
    private Double bracketRate_5;

    private Double ltcGainsRate;

    private Double dividendsRate;

    private Double standardDeduction;

    private Double blind;

    private Double exemptionAmount;

    private Double over65;

    private Double exemptionCutoff;

    private Double deductionCutoff;

    private Double maximumCapitalLoss;

    public Double getBracketLow_0() {
        return bracketLow_0;
    }

    public void setBracketLow_0(Double bracketLowTF_0) {
        this.bracketLow_0 = bracketLowTF_0;
    }

    public Double getBracketLow_1() {
        return bracketLow_1;
    }

    public void setBracketLow_1(Double bracketLowTF_1) {
        this.bracketLow_1 = bracketLowTF_1;
    }

    public Double getBracketLow_2() {
        return bracketLow_2;
    }

    public void setBracketLow_2(Double bracketLowTF_2) {
        this.bracketLow_2 = bracketLowTF_2;
    }

    public Double getBracketLow_3() {
        return bracketLow_3;
    }

    public void setBracketLow_3(Double bracketLowTF_3) {
        this.bracketLow_3 = bracketLowTF_3;
    }

    public Double getBracketLow_4() {
        return bracketLow_4;
    }

    public void setBracketLow_4(Double bracketLowTF_4) {
        this.bracketLow_4 = bracketLowTF_4;
    }

    public Double getBracketLow_5() {
        return bracketLow_5;
    }

    public void setBracketLow_5(Double bracketLowTF_5) {
        this.bracketLow_5 = bracketLowTF_5;
    }

    public Double getBracketHigh_0() {
        return bracketHigh_0;
    }

    public void setBracketHigh_0(Double bracketHighTF_0) {
        this.bracketHigh_0 = bracketHighTF_0;
    }

    public Double getBracketHigh_1() {
        return bracketHigh_1;
    }

    public void setBracketHigh_1(Double bracketHighTF_1) {
        this.bracketHigh_1 = bracketHighTF_1;
    }

    public Double getBracketHigh_2() {
        return bracketHigh_2;
    }

    public void setBracketHigh_2(Double bracketHighTF_2) {
        this.bracketHigh_2 = bracketHighTF_2;
    }

    public Double getBracketHigh_3() {
        return bracketHigh_3;
    }

    public void setBracketHigh_3(Double bracketHighTF_3) {
        this.bracketHigh_3 = bracketHighTF_3;
    }

    public Double getBracketHigh_4() {
        return bracketHigh_4;
    }

    public void setBracketHigh_4(Double bracketHighTF_4) {
        this.bracketHigh_4 = bracketHighTF_4;
    }

    public Double getBracketHigh_5() {
        return bracketHigh_5;
    }

    public void setBracketHigh_5(Double bracketHighTF_5) {
        this.bracketHigh_5 = bracketHighTF_5;
    }

    public Double getBracketRate_0() {
        return bracketRate_0;
    }

    public void setBracketRate_0(Double bracketRateTF_0) {
        this.bracketRate_0 = bracketRateTF_0;
    }

    public Double getBracketRate_1() {
        return bracketRate_1;
    }

    public void setBracketRate_1(Double bracketRateTF_1) {
        this.bracketRate_1 = bracketRateTF_1;
    }

    public Double getBracketRate_2() {
        return bracketRate_2;
    }

    public void setBracketRate_2(Double bracketRateTF_2) {
        this.bracketRate_2 = bracketRateTF_2;
    }

    public Double getBracketRate_3() {
        return bracketRate_3;
    }

    public void setBracketRate_3(Double bracketRateTF_3) {
        this.bracketRate_3 = bracketRateTF_3;
    }

    public Double getBracketRate_4() {
        return bracketRate_4;
    }

    public void setBracketRate_4(Double bracketRateTF_4) {
        this.bracketRate_4 = bracketRateTF_4;
    }

    public Double getBracketRate_5() {
        return bracketRate_5;
    }

    public void setBracketRate_5(Double bracketRateTF_5) {
        this.bracketRate_5 = bracketRateTF_5;
    }

    public Double getLtcGainsRate() {
        return ltcGainsRate;
    }

    public void setLtcGainsRate(Double ltcGainsRate) {
        this.ltcGainsRate = ltcGainsRate;
    }

    public Double getDividendsRate() {
        return dividendsRate;
    }

    public void setDividendsRate(Double dividendsRate) {
        this.dividendsRate = dividendsRate;
    }

    public Double getStandardDeduction() {
        return standardDeduction;
    }

    public void setStandardDeduction(Double standardDeduction) {
        this.standardDeduction = standardDeduction;
    }

    public Double getBlind() {
        return blind;
    }

    public void setBlind(Double blind) {
        this.blind = blind;
    }

    public Double getExemptionAmount() {
        return exemptionAmount;
    }

    public void setExemptionAmount(Double exemptionAmount) {
        this.exemptionAmount = exemptionAmount;
    }

    public Double getOver65() {
        return over65;
    }

    public void setOver65(Double over65) {
        this.over65 = over65;
    }

    public Double getExemptionCutoff() {
        return exemptionCutoff;
    }

    public void setExemptionCutoff(Double exemptionCutoff) {
        this.exemptionCutoff = exemptionCutoff;
    }

    public Double getDeductionCutoff() {
        return deductionCutoff;
    }

    public void setDeductionCutoff(Double deductionCutoff) {
        this.deductionCutoff = deductionCutoff;
    }

    public Double getMaximumCapitalLoss() {
        return maximumCapitalLoss;
    }

    public void setMaximumCapitalLoss(Double maximumCapitalLoss) {
        this.maximumCapitalLoss = maximumCapitalLoss;
    }

    public void load(TaxRateInfo taxRateInfo) {
        TaxBracket taxBracket = null;

        taxBracket = taxRateInfo.getTaxBracket(0);
        setBracketLow_0(taxBracket.getLow());
        setBracketHigh_0(taxBracket.getHigh());
        setBracketRate_0(taxBracket.getRate());

        taxBracket = taxRateInfo.getTaxBracket(1);
        setBracketLow_1(taxBracket.getLow());
        setBracketHigh_1(taxBracket.getHigh());
        setBracketRate_1(taxBracket.getRate());

        taxBracket = taxRateInfo.getTaxBracket(2);
        setBracketLow_2(taxBracket.getLow());
        setBracketHigh_2(taxBracket.getHigh());
        setBracketRate_2(taxBracket.getRate());

        taxBracket = taxRateInfo.getTaxBracket(3);
        setBracketLow_3(taxBracket.getLow());
        setBracketHigh_3(taxBracket.getHigh());
        setBracketRate_3(taxBracket.getRate());

        taxBracket = taxRateInfo.getTaxBracket(4);
        setBracketLow_4(taxBracket.getLow());
        setBracketHigh_4(taxBracket.getHigh());
        setBracketRate_4(taxBracket.getRate());

        taxBracket = taxRateInfo.getTaxBracket(5);
        setBracketLow_5(taxBracket.getLow());
        setBracketHigh_5(taxBracket.getHigh());
        setBracketRate_5(taxBracket.getRate());

//      private Double ltcGainsRate;
        setLtcGainsRate(taxRateInfo.getLtcGainsRate());

//        private Double dividendsRate;
        setDividendsRate(taxRateInfo.getDividendsRate());
//
//        private Double standardDeduction;
        setStandardDeduction(taxRateInfo.getStandardDeduction());
//
//        private Double blind;
        setBlind(taxRateInfo.getBlind());
//
//        private Double exemptionAmount;
        setExemptionAmount(taxRateInfo.getExemptionAmount());
//
//        private Double over65;
        setOver65(taxRateInfo.getOver65());
//
//        private Double exemptionCutoff;
        setExemptionCutoff(taxRateInfo.getExemptionCutoff());
//
//        private Double deductionCutoff;
        setDeductionCutoff(taxRateInfo.getDeductionCutoff());
//
//        private Double maximumCapitalLoss;     
        setMaximumCapitalLoss(taxRateInfo.getMaximumCapitalLoss());
    }
}

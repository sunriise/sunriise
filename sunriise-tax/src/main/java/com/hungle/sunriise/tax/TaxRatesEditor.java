package com.hungle.sunriise.tax;

import java.awt.Component;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import java.util.prefs.Preferences;

import javax.swing.ComboBoxModel;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JSeparator;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;

import org.apache.logging.log4j.Logger;
import org.jdesktop.beansbinding.AutoBinding;
import org.jdesktop.beansbinding.AutoBinding.UpdateStrategy;
import org.jdesktop.beansbinding.BeanProperty;
import org.jdesktop.beansbinding.Bindings;

import com.healthmarketscience.jackcess.Cursor;
import com.healthmarketscience.jackcess.CursorBuilder;
import com.healthmarketscience.jackcess.Row;
import com.healthmarketscience.jackcess.Table;
import com.hungle.sunriise.account.tax.TaxRateInfo;
import com.hungle.sunriise.dbutil.TableTaxRatesUtils;
import com.hungle.sunriise.gui.AbstractSamplesMenu;
import com.hungle.sunriise.io.MnyDb;
import com.hungle.sunriise.tax.model.TaxRateModel;
import com.hungle.sunriise.viewer.MnyViewer;
import com.hungle.sunriise.viewer.open.OpenDbAction;
import com.hungle.sunriise.viewer.open.OpenDbDialog;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.FormSpecs;
import com.jgoodies.forms.layout.RowSpec;

public class TaxRatesEditor {
    private static final String FILING_STATUS_SINGLE = "TRSingle";
    private static final String FILING_STATUS_SINGLE_LABEL = "Single";

    private static final String FILING_STATUS_JOINT = "TRJoint";
    private static final String FILING_STATUS_JOINT_LABEL = "Filing Jointly";

    private static final String FILING_STATUS_CUSTOM = "TRCustom";
    private static final String FILING_STATUS_CUSTOM_LABEL = "Custom";

    private static final String FILING_STATUS_H_HOUSE = "TRHHouse";
    private static final String FILING_STATUS_H_HOUSE_LABEL = "Heads of Household";

    private static final String FILING_STATUS_SEPARATE = "TRSeparate";
    private static final String FILING_STATUS_SEPARATE_LABEL = "Filing Separately";

    private static final Logger LOGGER = org.apache.logging.log4j.LogManager.getLogger(TaxRatesEditor.class);

    private static final Preferences prefs = Preferences.userNodeForPackage(TaxRatesEditor.class);

    private JFrame frame;
    private JTextField bracketLowTF_0;
    private JTextField bracketHighTF_0;
    private JTextField bracketRateTF_0;
    private JTextField bracketLowTF_1;
    private JTextField bracketHighTF_1;
    private JTextField bracketLowTF_4;
    private JTextField bracketLowTF_3;
    private JTextField bracketLowTF_2;
    private JTextField bracketHighTF_2;
    private JTextField bracketHighTF_3;
    private JTextField bracketHighTF_4;
    private JTextField bracketRateTF_1;
    private JTextField bracketRateTF_2;
    private JTextField bracketRateTF_3;
    private JTextField bracketRateTF_4;
    private JTextField ltcGainsRateTF;
    private JTextField dividendsRateTF;
    private JTextField blindTF;
    private JTextField standardDeductionTF;
    private JTextField exemptionAmountTF;
    private JTextField exemptionCutoffTF;
    private JTextField over65TF;
    private JTextField deductionCutoffTF;

    private MnyDb mnyDb = new MnyDb();

    private Set<Integer> taxYears;

    private JComboBox<Integer> taxYearsComboBox;

    private Table table;

    private JComboBox<FilingStatus> filingStatusComboBox;

    private List<Row> rows;

//    private Map<String, Row> filingStatuses;

    private JTextField[] bracketLowTextField = new JTextField[6];

    private JTextField[] bracketHighTextField = new JTextField[6];

    private JTextField[] bracketRateTextField = new JTextField[6];
    private JTextField bracketLowTF_5;
    private JTextField bracketHighTF_5;
    private JTextField bracketRateTF_5;

    private JTextField blindTextField;

    private JTextField deductionCutoffTextField;

    private JTextField dividendsRateTextField;

    private JTextField exemptionAmountTextField;

    private JTextField exemptionCutoffTextField;

    private JTextField ltcGainsRateTextField;

    private JTextField over65TextField;

    private JTextField standardDeductionTextField;
    private JTextField maximumCapitalLossTF;

    private JTextField maximumCapitalLossTextFile;

    private FilingStatus filingStatus;

    private Map<String, Row> rowsCache;

    private TaxRateModel model = new TaxRateModel();

    private Map<String, TaxRateModel> modelsCache;

    private static final class FilingStatus {
        public FilingStatus(String filingStatus, String label) {
            super();
            this.filingStatus = filingStatus;
            this.label = label;
        }

        private String filingStatus;
        private String label;

        public String getFilingStatus() {
            return filingStatus;
        }

        public void setFilingStatus(String filingStatus) {
            this.filingStatus = filingStatus;
        }

        public String getLabel() {
            return label;
        }

        public void setLabel(String label) {
            this.label = label;
        }

        @Override
        public String toString() {
//            return "FilingStatus [filingStatus=" + filingStatus + ", label=" + label + "]";
            return getLabel();
        }

    }

    private static final Map<String, FilingStatus> filingStatusMap = new HashMap<String, FilingStatus>();
    static {
        filingStatusMap.put(FILING_STATUS_SINGLE_LABEL,
                new FilingStatus(FILING_STATUS_SINGLE, FILING_STATUS_SINGLE_LABEL));
        filingStatusMap.put(FILING_STATUS_JOINT_LABEL,
                new FilingStatus(FILING_STATUS_JOINT, FILING_STATUS_JOINT_LABEL));
        filingStatusMap.put(FILING_STATUS_SEPARATE_LABEL,
                new FilingStatus(FILING_STATUS_SEPARATE, FILING_STATUS_SEPARATE_LABEL));
        filingStatusMap.put(FILING_STATUS_H_HOUSE_LABEL,
                new FilingStatus(FILING_STATUS_H_HOUSE, FILING_STATUS_H_HOUSE_LABEL));
        filingStatusMap.put(FILING_STATUS_CUSTOM_LABEL,
                new FilingStatus(FILING_STATUS_CUSTOM, FILING_STATUS_CUSTOM_LABEL));
    }

    private final class MnyViewerOpenDbAction extends OpenDbAction {

        /**
         * Instantiates a new mny viewer open db action.
         *
         * @param locationRelativeTo the location relative to
         * @param prefs              the prefs
         * @param mnyDb              the opened db
         */
        private MnyViewerOpenDbAction(Component locationRelativeTo, Preferences prefs, MnyDb mnyDb) {
            super(locationRelativeTo, prefs, mnyDb);
            setDisableReadOnlyCheckBox(true);
        }

        @Override
        public void dbFileOpened(MnyDb newMnyDb, OpenDbDialog dialog) {
            boolean readOnly = dialog.getReadOnlyCheckBox().isSelected();

            mnyDbOpened(newMnyDb, readOnly);
        }
    }

    /**
     * Launch the application.
     */
    public static void main(String[] args) {
        EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                try {
                    TaxRatesEditor window = new TaxRatesEditor();
                    window.frame.setVisible(true);
                } catch (Exception e) {
                    LOGGER.error(e, e);
                }
            }
        });
    }

    /**
     * Create the application.
     */
    public TaxRatesEditor() {
        initialize();
    }

    /**
     * Initialize the contents of the frame.
     */
    private void initialize() {
        frame = new JFrame();
        frame.setBounds(100, 100, 660, 500);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosed(WindowEvent event) {
                super.windowClosed(event);
                LOGGER.info("> windowClosed");
            }

            @Override
            public void windowClosing(WindowEvent event) {
                super.windowClosing(event);
                LOGGER.info("> windowClosing");
                if (getMnyDb() != null) {
                    appClosed();
                }
            }

        });
        FormLayout formLayout = new FormLayout(new ColumnSpec[] { FormSpecs.RELATED_GAP_COLSPEC,
                ColumnSpec.decode("default:grow"), FormSpecs.RELATED_GAP_COLSPEC, ColumnSpec.decode("30dlu:grow"),
                FormSpecs.RELATED_GAP_COLSPEC, ColumnSpec.decode("default:grow"), FormSpecs.RELATED_GAP_COLSPEC,
                ColumnSpec.decode("default:grow"), FormSpecs.RELATED_GAP_COLSPEC, ColumnSpec.decode("30dlu:grow"),
                FormSpecs.RELATED_GAP_COLSPEC, ColumnSpec.decode("default:grow"), FormSpecs.RELATED_GAP_COLSPEC,
                ColumnSpec.decode("max(15dlu;min)"), FormSpecs.RELATED_GAP_COLSPEC, FormSpecs.DEFAULT_COLSPEC, },
                new RowSpec[] { FormSpecs.RELATED_GAP_ROWSPEC, FormSpecs.DEFAULT_ROWSPEC, FormSpecs.RELATED_GAP_ROWSPEC,
                        FormSpecs.DEFAULT_ROWSPEC, FormSpecs.RELATED_GAP_ROWSPEC, FormSpecs.DEFAULT_ROWSPEC,
                        FormSpecs.RELATED_GAP_ROWSPEC, FormSpecs.DEFAULT_ROWSPEC, FormSpecs.RELATED_GAP_ROWSPEC,
                        FormSpecs.DEFAULT_ROWSPEC, FormSpecs.RELATED_GAP_ROWSPEC, FormSpecs.DEFAULT_ROWSPEC,
                        FormSpecs.RELATED_GAP_ROWSPEC, FormSpecs.DEFAULT_ROWSPEC, FormSpecs.RELATED_GAP_ROWSPEC,
                        FormSpecs.DEFAULT_ROWSPEC, FormSpecs.RELATED_GAP_ROWSPEC, FormSpecs.DEFAULT_ROWSPEC,
                        FormSpecs.RELATED_GAP_ROWSPEC, FormSpecs.DEFAULT_ROWSPEC, FormSpecs.RELATED_GAP_ROWSPEC,
                        FormSpecs.DEFAULT_ROWSPEC, FormSpecs.RELATED_GAP_ROWSPEC, FormSpecs.DEFAULT_ROWSPEC,
                        FormSpecs.RELATED_GAP_ROWSPEC, FormSpecs.DEFAULT_ROWSPEC, FormSpecs.RELATED_GAP_ROWSPEC,
                        FormSpecs.DEFAULT_ROWSPEC, FormSpecs.RELATED_GAP_ROWSPEC, FormSpecs.DEFAULT_ROWSPEC,
                        FormSpecs.RELATED_GAP_ROWSPEC, FormSpecs.DEFAULT_ROWSPEC, });
        formLayout.setColumnGroups(new int[][] { new int[] { 4, 6 }, new int[] { 10, 12 } });
        frame.getContentPane().setLayout(formLayout);

        JLabel lblNewLabel = new JLabel("Tax year");
        frame.getContentPane().add(lblNewLabel, "2, 4, right, default");

        JComboBox comboBox = new JComboBox();
        comboBox.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JComboBox cb = (JComboBox) e.getSource();
                Integer taxYear = (Integer) cb.getSelectedItem();
                taxYearSelected(taxYear);
            }
        });
        this.taxYearsComboBox = comboBox;
        frame.getContentPane().add(comboBox, "4, 4, 3, 1, fill, default");

        JLabel lblNewLabel_2 = new JLabel("Filing status");
        frame.getContentPane().add(lblNewLabel_2, "10, 4, right, default");

        JComboBox comboBox_1 = new JComboBox();
        comboBox_1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JComboBox cb = (JComboBox) e.getSource();
                FilingStatus filingStatus = (FilingStatus) cb.getSelectedItem();
                filingStatusSelected(filingStatus);
            }
        });
        this.filingStatusComboBox = comboBox_1;
        frame.getContentPane().add(comboBox_1, "12, 4, 3, 1, fill, default");

        JLabel lblNewLabel_1 = new JLabel("Income");
        frame.getContentPane().add(lblNewLabel_1, "2, 6, right, default");

        bracketLowTF_0 = new JTextField();
        bracketLowTF_0.setHorizontalAlignment(SwingConstants.RIGHT);
        this.bracketLowTextField[0] = bracketLowTF_0;
        frame.getContentPane().add(bracketLowTF_0, "4, 6, fill, default");
        bracketLowTF_0.setColumns(10);

        bracketHighTF_0 = new JTextField();
        bracketHighTF_0.setHorizontalAlignment(SwingConstants.RIGHT);
        this.bracketHighTextField[0] = bracketHighTF_0;
        frame.getContentPane().add(bracketHighTF_0, "6, 6, fill, default");
        bracketHighTF_0.setColumns(10);

        JLabel lblNewLabel_3 = new JLabel("is taxed at");
        frame.getContentPane().add(lblNewLabel_3, "10, 6, right, default");

        bracketRateTF_0 = new JTextField();
        bracketRateTF_0.setHorizontalAlignment(SwingConstants.RIGHT);
        this.bracketRateTextField[0] = bracketRateTF_0;
        frame.getContentPane().add(bracketRateTF_0, "12, 6, fill, default");
        bracketRateTF_0.setColumns(10);

        JLabel lblNewLabel_4 = new JLabel("%");
        frame.getContentPane().add(lblNewLabel_4, "14, 6");

        JLabel label_1 = new JLabel("Income");
        frame.getContentPane().add(label_1, "2, 8, right, default");

        bracketLowTF_1 = new JTextField();
        bracketLowTF_1.setHorizontalAlignment(SwingConstants.RIGHT);
        this.bracketLowTextField[1] = bracketLowTF_1;
        frame.getContentPane().add(bracketLowTF_1, "4, 8, fill, default");
        bracketLowTF_1.setColumns(10);

        bracketHighTF_1 = new JTextField();
        bracketHighTF_1.setHorizontalAlignment(SwingConstants.RIGHT);
        this.bracketHighTextField[1] = bracketHighTF_1;
        frame.getContentPane().add(bracketHighTF_1, "6, 8, fill, default");
        bracketHighTF_1.setColumns(10);

        JLabel label_4 = new JLabel("is taxed at");
        frame.getContentPane().add(label_4, "10, 8, right, default");

        bracketRateTF_1 = new JTextField();
        bracketRateTF_1.setHorizontalAlignment(SwingConstants.RIGHT);
        this.bracketRateTextField[1] = bracketRateTF_1;
        frame.getContentPane().add(bracketRateTF_1, "12, 8, fill, default");
        bracketRateTF_1.setColumns(10);

        JLabel label_8 = new JLabel("%");
        frame.getContentPane().add(label_8, "14, 8");

        JLabel label = new JLabel("Income");
        frame.getContentPane().add(label, "2, 10, right, default");

        bracketLowTF_2 = new JTextField();
        bracketLowTF_2.setHorizontalAlignment(SwingConstants.RIGHT);
        this.bracketLowTextField[2] = bracketLowTF_2;
        frame.getContentPane().add(bracketLowTF_2, "4, 10, fill, default");
        bracketLowTF_2.setColumns(10);

        bracketHighTF_2 = new JTextField();
        bracketHighTF_2.setHorizontalAlignment(SwingConstants.RIGHT);
        this.bracketHighTextField[2] = bracketHighTF_2;
        frame.getContentPane().add(bracketHighTF_2, "6, 10, fill, default");
        bracketHighTF_2.setColumns(10);

        JLabel label_5 = new JLabel("is taxed at");
        frame.getContentPane().add(label_5, "10, 10, right, default");

        bracketRateTF_2 = new JTextField();
        bracketRateTF_2.setHorizontalAlignment(SwingConstants.RIGHT);
        this.bracketRateTextField[2] = bracketRateTF_2;
        frame.getContentPane().add(bracketRateTF_2, "12, 10, fill, default");
        bracketRateTF_2.setColumns(10);

        JLabel label_9 = new JLabel("%");
        frame.getContentPane().add(label_9, "14, 10");

        JLabel label_2 = new JLabel("Income");
        frame.getContentPane().add(label_2, "2, 12, right, default");

        bracketLowTF_3 = new JTextField();
        bracketLowTF_3.setHorizontalAlignment(SwingConstants.RIGHT);
        this.bracketLowTextField[3] = bracketLowTF_3;
        frame.getContentPane().add(bracketLowTF_3, "4, 12, fill, default");
        bracketLowTF_3.setColumns(10);

        bracketHighTF_3 = new JTextField();
        bracketHighTF_3.setHorizontalAlignment(SwingConstants.RIGHT);
        this.bracketHighTextField[3] = bracketHighTF_3;
        frame.getContentPane().add(bracketHighTF_3, "6, 12, fill, default");
        bracketHighTF_3.setColumns(10);

        JLabel label_6 = new JLabel("is taxed at");
        frame.getContentPane().add(label_6, "10, 12, right, default");

        bracketRateTF_3 = new JTextField();
        bracketRateTF_3.setHorizontalAlignment(SwingConstants.RIGHT);
        this.bracketRateTextField[3] = bracketRateTF_3;
        frame.getContentPane().add(bracketRateTF_3, "12, 12, fill, default");
        bracketRateTF_3.setColumns(10);

        JLabel label_10 = new JLabel("%");
        frame.getContentPane().add(label_10, "14, 12");

        JLabel label_3 = new JLabel("Income");
        frame.getContentPane().add(label_3, "2, 14, right, default");

        bracketLowTF_4 = new JTextField();
        bracketLowTF_4.setHorizontalAlignment(SwingConstants.RIGHT);
        this.bracketLowTextField[4] = bracketLowTF_4;
        frame.getContentPane().add(bracketLowTF_4, "4, 14, fill, default");
        bracketLowTF_4.setColumns(10);

        bracketHighTF_4 = new JTextField();
        bracketHighTF_4.setHorizontalAlignment(SwingConstants.RIGHT);
        this.bracketHighTextField[4] = bracketHighTF_4;
        frame.getContentPane().add(bracketHighTF_4, "6, 14, fill, default");
        bracketHighTF_4.setColumns(10);

        JLabel label_7 = new JLabel("is taxed at");
        frame.getContentPane().add(label_7, "10, 14, right, default");

        bracketRateTF_4 = new JTextField();
        bracketRateTF_4.setHorizontalAlignment(SwingConstants.RIGHT);
        this.bracketRateTextField[4] = bracketRateTF_4;
        frame.getContentPane().add(bracketRateTF_4, "12, 14, fill, default");
        bracketRateTF_4.setColumns(10);

        JLabel label_11 = new JLabel("%");
        frame.getContentPane().add(label_11, "14, 14");

        JLabel label_14 = new JLabel("Income");
        frame.getContentPane().add(label_14, "2, 16, right, default");

        bracketLowTF_5 = new JTextField();
        bracketLowTF_5.setHorizontalAlignment(SwingConstants.RIGHT);
        this.bracketLowTextField[5] = bracketLowTF_5;
        bracketLowTF_5.setColumns(10);
        frame.getContentPane().add(bracketLowTF_5, "4, 16, fill, default");

        bracketHighTF_5 = new JTextField();
        bracketHighTF_5.setHorizontalAlignment(SwingConstants.RIGHT);
        this.bracketHighTextField[5] = bracketHighTF_5;
        bracketHighTF_5.setColumns(10);
        frame.getContentPane().add(bracketHighTF_5, "6, 16, fill, default");

        JLabel label_15 = new JLabel("is taxed at");
        frame.getContentPane().add(label_15, "10, 16, right, default");

        bracketRateTF_5 = new JTextField();
        bracketRateTF_5.setHorizontalAlignment(SwingConstants.RIGHT);
        this.bracketRateTextField[5] = bracketRateTF_5;
        bracketRateTF_5.setColumns(10);
        frame.getContentPane().add(bracketRateTF_5, "12, 16, fill, default");

        JLabel label_16 = new JLabel("%");
        frame.getContentPane().add(label_16, "14, 16");

        JLabel lblNewLabel_5 = new JLabel("Long-Term Capital Gains taxed at maximum rate of");
        frame.getContentPane().add(lblNewLabel_5, "2, 18, 9, 1, right, default");

        ltcGainsRateTF = new JTextField();
        this.ltcGainsRateTextField = ltcGainsRateTF;
        ltcGainsRateTF.setHorizontalAlignment(SwingConstants.RIGHT);
        frame.getContentPane().add(ltcGainsRateTF, "12, 18, fill, default");
        ltcGainsRateTF.setColumns(10);

        JLabel label_12 = new JLabel("%");
        frame.getContentPane().add(label_12, "14, 18");

        JLabel lblDividendsTaxedAt = new JLabel("Dividends taxed at maximum rate of");
        frame.getContentPane().add(lblDividendsTaxedAt, "2, 20, 9, 1, right, default");

        dividendsRateTF = new JTextField();
        this.dividendsRateTextField = dividendsRateTF;
        dividendsRateTF.setHorizontalAlignment(SwingConstants.RIGHT);
        frame.getContentPane().add(dividendsRateTF, "12, 20, fill, default");
        dividendsRateTF.setColumns(10);

        JLabel label_13 = new JLabel("%");
        frame.getContentPane().add(label_13, "14, 20");

        JLabel lblNewLabel_6 = new JLabel("Standard Deduction");
        frame.getContentPane().add(lblNewLabel_6, "2, 22, right, default");

        standardDeductionTF = new JTextField();
        this.standardDeductionTextField = standardDeductionTF;
        standardDeductionTF.setHorizontalAlignment(SwingConstants.RIGHT);
        frame.getContentPane().add(standardDeductionTF, "4, 22, 3, 1, fill, default");
        standardDeductionTF.setColumns(10);

        JLabel lblNewLabel_10 = new JLabel("Blind");
        frame.getContentPane().add(lblNewLabel_10, "10, 22, right, default");

        blindTF = new JTextField();
        this.blindTextField = blindTF;
        blindTF.setHorizontalAlignment(SwingConstants.RIGHT);
        frame.getContentPane().add(blindTF, "12, 22, fill, default");
        blindTF.setColumns(10);

        JLabel lblNewLabel_7 = new JLabel("Exemption Amount");
        frame.getContentPane().add(lblNewLabel_7, "2, 24, right, default");

        exemptionAmountTF = new JTextField();
        this.exemptionAmountTextField = exemptionAmountTF;
        exemptionAmountTF.setHorizontalAlignment(SwingConstants.RIGHT);
        exemptionAmountTF.setColumns(10);
        frame.getContentPane().add(exemptionAmountTF, "4, 24, 3, 1, fill, default");

        JLabel lblNewLabel_11 = new JLabel("Over 65");
        frame.getContentPane().add(lblNewLabel_11, "10, 24, right, default");

        over65TF = new JTextField();
        this.over65TextField = over65TF;
        over65TF.setHorizontalAlignment(SwingConstants.RIGHT);
        over65TF.setColumns(10);
        frame.getContentPane().add(over65TF, "12, 24, fill, default");

        JLabel lblNewLabel_8 = new JLabel("Exemption Cutoff");
        frame.getContentPane().add(lblNewLabel_8, "2, 26, right, default");

        exemptionCutoffTF = new JTextField();
        this.exemptionCutoffTextField = exemptionCutoffTF;
        exemptionCutoffTF.setHorizontalAlignment(SwingConstants.RIGHT);
        exemptionCutoffTF.setColumns(10);
        frame.getContentPane().add(exemptionCutoffTF, "4, 26, 3, 1, fill, default");

        JLabel lblNewLabel_12 = new JLabel("Deduction Cutoff");
        frame.getContentPane().add(lblNewLabel_12, "10, 26, right, default");

        deductionCutoffTF = new JTextField();
        this.deductionCutoffTextField = deductionCutoffTF;
        deductionCutoffTF.setHorizontalAlignment(SwingConstants.RIGHT);
        deductionCutoffTF.setColumns(10);
        frame.getContentPane().add(deductionCutoffTF, "12, 26, fill, default");

        JLabel lblNewLabel_9 = new JLabel("Maximum Capital Loss");
        frame.getContentPane().add(lblNewLabel_9, "2, 28, right, default");

        JButton btnNewButton = new JButton("Cancel");
        btnNewButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                appClosed();

                LOGGER.info("User selects Cancel");
                System.exit(0);
            }
        });

        maximumCapitalLossTF = new JTextField();
        maximumCapitalLossTF.setHorizontalAlignment(SwingConstants.RIGHT);
        this.maximumCapitalLossTextFile = maximumCapitalLossTF;
        frame.getContentPane().add(maximumCapitalLossTF, "4, 28, 3, 1, fill, default");
        maximumCapitalLossTF.setColumns(10);
        frame.getContentPane().add(btnNewButton, "10, 32");

        JButton btnNewButton_1 = new JButton("Save");
        btnNewButton_1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
            }
        });
        frame.getContentPane().add(btnNewButton_1, "12, 32");

        JMenuBar menuBar = new JMenuBar();
        frame.setJMenuBar(menuBar);

        JMenu mnNewMenu = new JMenu("File");
        menuBar.add(mnNewMenu);

        JMenuItem mntmNewMenuItem = new JMenuItem("Open");
        mntmNewMenuItem.addActionListener(new MnyViewerOpenDbAction(TaxRatesEditor.this.getFrame(), prefs, getMnyDb()));
        mnNewMenu.add(mntmNewMenuItem);

        JSeparator separator = new JSeparator();
        mnNewMenu.add(separator);

        JMenuItem mntmNewMenuItem_1 = new JMenuItem("Exit");
        mntmNewMenuItem_1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                appClosed();

                LOGGER.info("User selects File -> Exit");
                System.exit(0);
            }
        });
        mnNewMenu.add(mntmNewMenuItem_1);

        File sampleTopDir = new File("../sunriise-core");
        AbstractSamplesMenu samplesMenu = new AbstractSamplesMenu() {
            @Override
            protected void mnyDbSelected(MnyDb newMnyDb) {
                LOGGER.info("> mnyDbSelected, {}", newMnyDb.getDbFile().getAbsoluteFile());
                boolean readOnly = true;
                TaxRatesEditor.this.mnyDbOpened(newMnyDb, readOnly);
            }
        };
        samplesMenu.addSamplesMenu(menuBar, sampleTopDir);

        initDataBindings();
    }

    private void mnyDbOpened(MnyDb newMnyDb, boolean readOnly) {
        if (newMnyDb != null) {
            TaxRatesEditor.this.setMnyDb(newMnyDb);
        }

        String title = MnyViewer.TITLE_NO_OPENED_DB;
        File dbFile = getMnyDb().getDbFile();
        if (dbFile != null) {
            title = dbFile.getAbsolutePath();
        }
        getFrame().setTitle(title);

        try {
            taxYears = new TreeSet<Integer>();
            table = this.getMnyDb().getDb().getTable(TableTaxRatesUtils.TABLE_NAME_TAX_RATE);
            Cursor cursor = CursorBuilder.createCursor(table);
            while (cursor.moveToNextRow()) {
                Row row = cursor.getCurrentRow();
                Integer taxYear = row.getInt(TableTaxRatesUtils.COL_TAX_YEAR);
                if (taxYear != null) {
                    taxYears.add(taxYear);
                }
            }

            Integer[] array = new Integer[0];
            ComboBoxModel<Integer> model = new DefaultComboBoxModel<Integer>(taxYears.toArray(array));
            this.taxYearsComboBox.setModel(model);
            int currentYear = Calendar.getInstance().get(Calendar.YEAR);
            this.taxYearsComboBox.setSelectedItem(new Integer(currentYear));
        } catch (IOException e) {
            LOGGER.error(e, e);
        }
    }

    protected void taxYearSelected(Integer taxYear) {
        LOGGER.info("Selected taxYear=" + taxYear);

        try {
            rows = new ArrayList<Row>();
            Cursor cursor = CursorBuilder.createCursor(table);
            while (cursor.moveToNextRow()) {
                Row row = cursor.getCurrentRow();
                Integer ty = row.getInt(TableTaxRatesUtils.COL_TAX_YEAR);
                if (ty == null) {
                    continue;
                }
                if (ty.equals(taxYear)) {
                    rows.add(row);
                }
            }

            rowsCache = new LinkedHashMap<String, Row>();
            for (Row row : rows) {
                String filingStatus = row.getString(TaxRateInfo.COL_FILING_STATUS);
                String label = getFilingStatusLabel(filingStatus);
                if (label == null) {
                    LOGGER.warn("Invalid filingStatus=" + filingStatus);
                    continue;
                }

                rowsCache.put(filingStatus, row);
            }
            if (LOGGER.isDebugEnabled()) {
                LOGGER.debug("filingStatuses=" + rowsCache);
            }
            modelsCache = new LinkedHashMap<String, TaxRateModel>();

//            String[] filingStatusLabels = new String[filingStatuses.size()];
//            Set<String> keySet = filingStatuses.keySet();
            FilingStatus[] filingStatusArray = new FilingStatus[0];
            ComboBoxModel<FilingStatus> model = new DefaultComboBoxModel<FilingStatus>(
                    filingStatusMap.values().toArray(filingStatusArray));
            filingStatusComboBox.setModel(model);
            if (this.filingStatus == null) {
                this.filingStatus = filingStatusMap.get(FILING_STATUS_SINGLE_LABEL);
            }
            filingStatusComboBox.setSelectedItem(this.filingStatus);
        } catch (IOException e) {
            LOGGER.error(e, e);
        }
    }

    private String getFilingStatusLabel(String filingStatus) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("filingStatus=" + filingStatus);
        }

        String label = null;
        Collection<FilingStatus> values = filingStatusMap.values();
        for (FilingStatus value : values) {
            if (value.getFilingStatus().compareTo(filingStatus) == 0) {
                label = value.getLabel();
                break;
            }
        }
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("filingStatus=" + filingStatus + ", label=" + label);
        }

        return label;
    }

    protected void filingStatusSelected(FilingStatus filingStatus) {
        this.filingStatus = filingStatus;

        final String filingStatusKey = filingStatus.getFilingStatus();
        LOGGER.info("Selected filingStatus=" + filingStatusKey);

        Row row = rowsCache.get(filingStatusKey);
        LOGGER.info("row=" + row);

        Runnable doRun = new Runnable() {
            @Override
            public void run() {
                TaxRateInfo taxRateInfo = TaxRateInfo.toTaxRateInfo(row);
                model.load(taxRateInfo);
            }
        };
        SwingUtilities.invokeLater(doRun);
    }

    protected void appClosed() {
        if (getMnyDb() != null) {
            try {
                getMnyDb().close();
            } catch (IOException e) {
                LOGGER.warn(e);
            }
        }
        setMnyDb(null);
    }

    public JFrame getFrame() {
        return frame;
    }

    public void setFrame(JFrame frame) {
        this.frame = frame;
    }

    protected void initDataBindings() {
        BeanProperty<TaxRateModel, Double> taxRateModelBeanProperty = BeanProperty.create("bracketLow_0");
        BeanProperty<JTextField, String> jTextFieldBeanProperty = BeanProperty.create("text");
        AutoBinding<TaxRateModel, Double, JTextField, String> autoBinding = Bindings.createAutoBinding(
                UpdateStrategy.READ_WRITE, model, taxRateModelBeanProperty, bracketLowTF_0, jTextFieldBeanProperty);
        autoBinding.bind();
        //
        BeanProperty<TaxRateModel, Double> taxRateModelBeanProperty_1 = BeanProperty.create("bracketHigh_0");
        BeanProperty<JTextField, String> jTextFieldBeanProperty_1 = BeanProperty.create("text");
        AutoBinding<TaxRateModel, Double, JTextField, String> autoBinding_1 = Bindings.createAutoBinding(
                UpdateStrategy.READ_WRITE, model, taxRateModelBeanProperty_1, bracketHighTF_0,
                jTextFieldBeanProperty_1);
        autoBinding_1.bind();
        //
        BeanProperty<TaxRateModel, Double> taxRateModelBeanProperty_2 = BeanProperty.create("bracketRate_0");
        BeanProperty<JTextField, String> jTextFieldBeanProperty_2 = BeanProperty.create("text");
        AutoBinding<TaxRateModel, Double, JTextField, String> autoBinding_2 = Bindings.createAutoBinding(
                UpdateStrategy.READ_WRITE, model, taxRateModelBeanProperty_2, bracketRateTF_0,
                jTextFieldBeanProperty_2);
        autoBinding_2.bind();
        //
        BeanProperty<TaxRateModel, Double> taxRateModelBeanProperty_3 = BeanProperty.create("bracketLow_1");
        BeanProperty<JTextField, String> jTextFieldBeanProperty_3 = BeanProperty.create("text");
        AutoBinding<TaxRateModel, Double, JTextField, String> autoBinding_3 = Bindings.createAutoBinding(
                UpdateStrategy.READ_WRITE, model, taxRateModelBeanProperty_3, bracketLowTF_1, jTextFieldBeanProperty_3);
        autoBinding_3.bind();
        //
        BeanProperty<TaxRateModel, Double> taxRateModelBeanProperty_4 = BeanProperty.create("bracketHigh_1");
        BeanProperty<JTextField, String> jTextFieldBeanProperty_4 = BeanProperty.create("text");
        AutoBinding<TaxRateModel, Double, JTextField, String> autoBinding_4 = Bindings.createAutoBinding(
                UpdateStrategy.READ_WRITE, model, taxRateModelBeanProperty_4, bracketHighTF_1,
                jTextFieldBeanProperty_4);
        autoBinding_4.bind();
        //
        BeanProperty<TaxRateModel, Double> taxRateModelBeanProperty_5 = BeanProperty.create("bracketRate_1");
        BeanProperty<JTextField, String> jTextFieldBeanProperty_5 = BeanProperty.create("text");
        AutoBinding<TaxRateModel, Double, JTextField, String> autoBinding_5 = Bindings.createAutoBinding(
                UpdateStrategy.READ_WRITE, model, taxRateModelBeanProperty_5, bracketRateTF_1,
                jTextFieldBeanProperty_5);
        autoBinding_5.bind();
        //
        BeanProperty<TaxRateModel, Double> taxRateModelBeanProperty_6 = BeanProperty.create("bracketLow_2");
        BeanProperty<JTextField, String> jTextFieldBeanProperty_6 = BeanProperty.create("text");
        AutoBinding<TaxRateModel, Double, JTextField, String> autoBinding_6 = Bindings.createAutoBinding(
                UpdateStrategy.READ_WRITE, model, taxRateModelBeanProperty_6, bracketLowTF_2, jTextFieldBeanProperty_6);
        autoBinding_6.bind();
        //
        BeanProperty<TaxRateModel, Double> taxRateModelBeanProperty_7 = BeanProperty.create("bracketHigh_2");
        BeanProperty<JTextField, String> jTextFieldBeanProperty_7 = BeanProperty.create("text");
        AutoBinding<TaxRateModel, Double, JTextField, String> autoBinding_7 = Bindings.createAutoBinding(
                UpdateStrategy.READ_WRITE, model, taxRateModelBeanProperty_7, bracketHighTF_2,
                jTextFieldBeanProperty_7);
        autoBinding_7.bind();
        //
        BeanProperty<TaxRateModel, Double> taxRateModelBeanProperty_8 = BeanProperty.create("bracketRate_2");
        BeanProperty<JTextField, String> jTextFieldBeanProperty_8 = BeanProperty.create("text");
        AutoBinding<TaxRateModel, Double, JTextField, String> autoBinding_8 = Bindings.createAutoBinding(
                UpdateStrategy.READ_WRITE, model, taxRateModelBeanProperty_8, bracketRateTF_2,
                jTextFieldBeanProperty_8);
        autoBinding_8.bind();
        //
        BeanProperty<TaxRateModel, Double> taxRateModelBeanProperty_9 = BeanProperty.create("bracketLow_3");
        BeanProperty<JTextField, String> jTextFieldBeanProperty_9 = BeanProperty.create("text");
        AutoBinding<TaxRateModel, Double, JTextField, String> autoBinding_9 = Bindings.createAutoBinding(
                UpdateStrategy.READ_WRITE, model, taxRateModelBeanProperty_9, bracketLowTF_3, jTextFieldBeanProperty_9);
        autoBinding_9.bind();
        //
        BeanProperty<TaxRateModel, Double> taxRateModelBeanProperty_10 = BeanProperty.create("bracketHigh_3");
        BeanProperty<JTextField, String> jTextFieldBeanProperty_10 = BeanProperty.create("text");
        AutoBinding<TaxRateModel, Double, JTextField, String> autoBinding_10 = Bindings.createAutoBinding(
                UpdateStrategy.READ_WRITE, model, taxRateModelBeanProperty_10, bracketHighTF_3,
                jTextFieldBeanProperty_10);
        autoBinding_10.bind();
        //
        BeanProperty<TaxRateModel, Double> taxRateModelBeanProperty_11 = BeanProperty.create("bracketRate_3");
        BeanProperty<JTextField, String> jTextFieldBeanProperty_11 = BeanProperty.create("text");
        AutoBinding<TaxRateModel, Double, JTextField, String> autoBinding_11 = Bindings.createAutoBinding(
                UpdateStrategy.READ_WRITE, model, taxRateModelBeanProperty_11, bracketRateTF_3,
                jTextFieldBeanProperty_11);
        autoBinding_11.bind();
        //
        BeanProperty<TaxRateModel, Double> taxRateModelBeanProperty_12 = BeanProperty.create("bracketLow_4");
        BeanProperty<JTextField, String> jTextFieldBeanProperty_12 = BeanProperty.create("text");
        AutoBinding<TaxRateModel, Double, JTextField, String> autoBinding_12 = Bindings.createAutoBinding(
                UpdateStrategy.READ_WRITE, model, taxRateModelBeanProperty_12, bracketLowTF_4,
                jTextFieldBeanProperty_12);
        autoBinding_12.bind();
        //
        BeanProperty<TaxRateModel, Double> taxRateModelBeanProperty_13 = BeanProperty.create("bracketHigh_4");
        BeanProperty<JTextField, String> jTextFieldBeanProperty_13 = BeanProperty.create("text");
        AutoBinding<TaxRateModel, Double, JTextField, String> autoBinding_13 = Bindings.createAutoBinding(
                UpdateStrategy.READ_WRITE, model, taxRateModelBeanProperty_13, bracketHighTF_4,
                jTextFieldBeanProperty_13);
        autoBinding_13.bind();
        //
        BeanProperty<TaxRateModel, Double> taxRateModelBeanProperty_14 = BeanProperty.create("bracketRate_4");
        BeanProperty<JTextField, String> jTextFieldBeanProperty_14 = BeanProperty.create("text");
        AutoBinding<TaxRateModel, Double, JTextField, String> autoBinding_14 = Bindings.createAutoBinding(
                UpdateStrategy.READ_WRITE, model, taxRateModelBeanProperty_14, bracketRateTF_4,
                jTextFieldBeanProperty_14);
        autoBinding_14.bind();
        //
        BeanProperty<TaxRateModel, Double> taxRateModelBeanProperty_15 = BeanProperty.create("bracketLow_5");
        BeanProperty<JTextField, String> jTextFieldBeanProperty_15 = BeanProperty.create("text");
        AutoBinding<TaxRateModel, Double, JTextField, String> autoBinding_15 = Bindings.createAutoBinding(
                UpdateStrategy.READ_WRITE, model, taxRateModelBeanProperty_15, bracketLowTF_5,
                jTextFieldBeanProperty_15);
        autoBinding_15.bind();
        //
        BeanProperty<TaxRateModel, Double> taxRateModelBeanProperty_16 = BeanProperty.create("bracketHigh_5");
        BeanProperty<JTextField, String> jTextFieldBeanProperty_16 = BeanProperty.create("text");
        AutoBinding<TaxRateModel, Double, JTextField, String> autoBinding_16 = Bindings.createAutoBinding(
                UpdateStrategy.READ_WRITE, model, taxRateModelBeanProperty_16, bracketHighTF_5,
                jTextFieldBeanProperty_16);
        autoBinding_16.bind();
        //
        BeanProperty<TaxRateModel, Double> taxRateModelBeanProperty_17 = BeanProperty.create("bracketRate_5");
        BeanProperty<JTextField, String> jTextFieldBeanProperty_17 = BeanProperty.create("text");
        AutoBinding<TaxRateModel, Double, JTextField, String> autoBinding_17 = Bindings.createAutoBinding(
                UpdateStrategy.READ_WRITE, model, taxRateModelBeanProperty_17, bracketRateTF_5,
                jTextFieldBeanProperty_17);
        autoBinding_17.bind();
        //
        BeanProperty<TaxRateModel, Double> taxRateModelBeanProperty_18 = BeanProperty.create("ltcGainsRate");
        BeanProperty<JTextField, String> jTextFieldBeanProperty_18 = BeanProperty.create("text");
        AutoBinding<TaxRateModel, Double, JTextField, String> autoBinding_18 = Bindings.createAutoBinding(
                UpdateStrategy.READ_WRITE, model, taxRateModelBeanProperty_18, ltcGainsRateTF,
                jTextFieldBeanProperty_18);
        autoBinding_18.bind();
        //
        BeanProperty<TaxRateModel, Double> taxRateModelBeanProperty_19 = BeanProperty.create("dividendsRate");
        BeanProperty<JTextField, String> jTextFieldBeanProperty_19 = BeanProperty.create("text");
        AutoBinding<TaxRateModel, Double, JTextField, String> autoBinding_19 = Bindings.createAutoBinding(
                UpdateStrategy.READ_WRITE, model, taxRateModelBeanProperty_19, dividendsRateTF,
                jTextFieldBeanProperty_19);
        autoBinding_19.bind();
        //
        BeanProperty<TaxRateModel, Double> taxRateModelBeanProperty_20 = BeanProperty.create("standardDeduction");
        BeanProperty<JTextField, String> jTextFieldBeanProperty_20 = BeanProperty.create("text");
        AutoBinding<TaxRateModel, Double, JTextField, String> autoBinding_20 = Bindings.createAutoBinding(
                UpdateStrategy.READ_WRITE, model, taxRateModelBeanProperty_20, standardDeductionTF,
                jTextFieldBeanProperty_20);
        autoBinding_20.bind();
        //
        BeanProperty<TaxRateModel, Double> taxRateModelBeanProperty_21 = BeanProperty.create("blind");
        BeanProperty<JTextField, String> jTextFieldBeanProperty_21 = BeanProperty.create("text");
        AutoBinding<TaxRateModel, Double, JTextField, String> autoBinding_21 = Bindings.createAutoBinding(
                UpdateStrategy.READ_WRITE, model, taxRateModelBeanProperty_21, blindTF, jTextFieldBeanProperty_21);
        autoBinding_21.bind();
        //
        BeanProperty<TaxRateModel, Double> taxRateModelBeanProperty_22 = BeanProperty.create("exemptionAmount");
        BeanProperty<JTextField, String> jTextFieldBeanProperty_22 = BeanProperty.create("text");
        AutoBinding<TaxRateModel, Double, JTextField, String> autoBinding_22 = Bindings.createAutoBinding(
                UpdateStrategy.READ_WRITE, model, taxRateModelBeanProperty_22, exemptionAmountTF,
                jTextFieldBeanProperty_22);
        autoBinding_22.bind();
        //
        BeanProperty<TaxRateModel, Double> taxRateModelBeanProperty_23 = BeanProperty.create("over65");
        BeanProperty<JTextField, String> jTextFieldBeanProperty_23 = BeanProperty.create("text");
        AutoBinding<TaxRateModel, Double, JTextField, String> autoBinding_23 = Bindings.createAutoBinding(
                UpdateStrategy.READ_WRITE, model, taxRateModelBeanProperty_23, over65TF, jTextFieldBeanProperty_23);
        autoBinding_23.bind();
        //
        BeanProperty<TaxRateModel, Double> taxRateModelBeanProperty_24 = BeanProperty.create("exemptionCutoff");
        BeanProperty<JTextField, String> jTextFieldBeanProperty_24 = BeanProperty.create("text");
        AutoBinding<TaxRateModel, Double, JTextField, String> autoBinding_24 = Bindings.createAutoBinding(
                UpdateStrategy.READ_WRITE, model, taxRateModelBeanProperty_24, exemptionCutoffTF,
                jTextFieldBeanProperty_24);
        autoBinding_24.bind();
        //
        BeanProperty<TaxRateModel, Double> taxRateModelBeanProperty_25 = BeanProperty.create("deductionCutoff");
        BeanProperty<JTextField, String> jTextFieldBeanProperty_25 = BeanProperty.create("text");
        AutoBinding<TaxRateModel, Double, JTextField, String> autoBinding_25 = Bindings.createAutoBinding(
                UpdateStrategy.READ_WRITE, model, taxRateModelBeanProperty_25, deductionCutoffTF,
                jTextFieldBeanProperty_25);
        autoBinding_25.bind();
        //
        BeanProperty<TaxRateModel, Double> taxRateModelBeanProperty_26 = BeanProperty.create("maximumCapitalLoss");
        BeanProperty<JTextField, String> jTextFieldBeanProperty_26 = BeanProperty.create("text");
        AutoBinding<TaxRateModel, Double, JTextField, String> autoBinding_26 = Bindings.createAutoBinding(
                UpdateStrategy.READ_WRITE, model, taxRateModelBeanProperty_26, maximumCapitalLossTF,
                jTextFieldBeanProperty_26);
        autoBinding_26.bind();
    }

    private MnyDb getMnyDb() {
        return mnyDb;
    }

    private void setMnyDb(MnyDb mnyDb) {
        this.mnyDb = mnyDb;
    }
}

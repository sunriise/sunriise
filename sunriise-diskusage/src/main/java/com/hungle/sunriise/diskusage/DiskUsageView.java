package com.hungle.sunriise.diskusage;

import java.awt.Color;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.border.MatteBorder;
import javax.swing.table.AbstractTableModel;

import org.apache.logging.log4j.Logger;

import com.hungle.sunriise.io.MnyDb;
import com.hungle.sunriise.mnystat.MnyStat;
import com.hungle.sunriise.mnystat.TableStat;

public class DiskUsageView extends JPanel {
    private static final Logger LOGGER = org.apache.logging.log4j.LogManager.getLogger(DiskUsageView.class);

    private final MnyDb mnyDb;
    private final MnyStat mnyStat;

    public DiskUsageView(MnyDb mnyDb) throws IOException {
        this.mnyDb = mnyDb;
        this.mnyStat = new MnyStat(this.mnyDb);

        initView();
    }

    protected void initView() {
        List<TableStat> stats = new ArrayList<>();
        try {
            stats.addAll(mnyStat.getTableStats());
            stats.addAll(mnyStat.getSystemTableStats());
        } catch (IOException e) {
            LOGGER.error(e.getMessage());
        }

        final String[] columnNames = { "Table", "Bytes", "Rows", "Columns" };

        AbstractTableModel tableModel = new AbstractTableModel() {

            @Override
            public String getColumnName(int column) {
                return columnNames[column];
            }

            @Override
            public Class<?> getColumnClass(int columnIndex) {
                switch (columnIndex) {
                case 0:
                    return String.class;
                case 1:
                    return Integer.class;
                case 2:
                    return Integer.class;
                case 3:
                    return Integer.class;
                }
                return String.class;
            }

            @Override
            public int getRowCount() {
                return stats.size();
            }

            @Override
            public int getColumnCount() {
                return columnNames.length;
            }

            @Override
            public Object getValueAt(int rowIndex, int columnIndex) {
                TableStat row = stats.get(rowIndex);
                switch (columnIndex) {
                case 0:
                    return row.getTableName();
                case 1:
                    return row.getBytes();
                case 2:
                    return row.getRowCount();
                case 3:
                    return row.getColumnCount();
                }
                return null;
            }

        };
        JTable table = new JTable(tableModel);
        table.getTableHeader().setBorder(new MatteBorder(0, 0, 1, 0, Color.BLACK));

        table.setAutoCreateRowSorter(true);

        JScrollPane view = new JScrollPane(table);
        table.setFillsViewportHeight(true);

//        Dimension preferredSize = new Dimension(300, 300);
//        view.setPreferredSize(preferredSize);

        this.add(view);
    }

}

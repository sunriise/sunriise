package com.hungle.sunriise.diskusage;

import java.awt.Component;
import java.awt.Container;
import java.awt.HeadlessException;
import java.io.IOException;

import javax.swing.JMenuBar;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

import org.apache.logging.log4j.Logger;
import org.fife.ui.rsyntaxtextarea.RSyntaxTextArea;

import com.hungle.sunriise.gui.AbstractFileDropFrame;
import com.hungle.sunriise.gui.AbstractSamplesMenu;
import com.hungle.sunriise.io.MnyDb;

public class DiskUsageFrame extends AbstractFileDropFrame {
    @Override
    protected JMenuBar addMenus(Container contentPane) {
        // TODO Auto-generated method stub
        JMenuBar menuBar = super.addMenus(contentPane);

        AbstractSamplesMenu abstractSamplesMenu = new AbstractSamplesMenu() {
            @Override
            protected void mnyDbSelected(MnyDb newMnyDb) {
                try {
                    handleMnyDb(newMnyDb, null);
                } catch (IOException e) {
                    LOGGER.error(e, e);
                }
            }
        };
        abstractSamplesMenu.addSamplesMenu(menuBar);

        return menuBar;
    }

    private static final Logger LOGGER = org.apache.logging.log4j.LogManager.getLogger(DiskUsageFrame.class);

    public DiskUsageFrame(String title) throws HeadlessException {
        super(title);
    }

    public static void main(String[] args) {
        final DiskUsageFrame mainView = new DiskUsageFrame("DiskUsage");
        Runnable doRun = new Runnable() {
            @Override
            public void run() {
                mainView.showMainFrame();
            }
        };
        SwingUtilities.invokeLater(doRun);
    }

    @Override
    public void handleMnyDb(MnyDb mnyDb, JPanel view) throws IOException {
        Component parentComponent = DiskUsageFrame.this;

        DiskUsageView diskUsageView = new DiskUsageView(mnyDb);
        Object message = diskUsageView;

        JOptionPane.showMessageDialog(parentComponent, message, "Disk usage", JOptionPane.INFORMATION_MESSAGE);
    }

    @Override
    protected void addTextToMainView(RSyntaxTextArea textArea, String eol) {
        textArea.append("To open, just drag-and-drop a *.mny file here." + eol);

//        textArea.append(eol);
//        textArea.append("To select different export type:" + eol);
//        textArea.append("  * " + ExportType.CSV + ": export to *.csv files" + eol);
//        textArea.append("  * " + ExportType.JSON + ": export to *.json files" + eol);
//        textArea.append("  * " + ExportType.MDB + ": export to *.mdb file" + eol);
//
//        textArea.append(eol);
//        textArea.append("To see the export file(s):" + eol);
//        textArea.append("  Select menu 'File -> " + OPEN_EXPORTS_DIRECTORY + "'" + eol);
//        // textArea.append(" to open the export directory." + eol);
//
//        textArea.append(eol);
//        textArea.append("To get additional info:" + eol);
//        textArea.append("  Select menu 'File -> " + OPEN_WIKI_PAGE + "'" + eol);

        // textArea.append(eol);
        // textArea.append("For more information see:" + eol);
        // textArea.append("
        // https://bitbucket.org/hleofxquotesteam/dist-sunriise/wiki/Home" +
        // eol);
    }

}

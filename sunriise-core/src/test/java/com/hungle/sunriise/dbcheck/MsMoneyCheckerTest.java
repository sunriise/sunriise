package com.hungle.sunriise.dbcheck;

import java.io.File;
import java.io.IOException;

import org.junit.Test;

import com.hungle.sunriise.io.sample.MnySampleFile;

// TODO: Auto-generated Javadoc
/**
 * The Class MsMoneyCheckerTest.
 */
public class MsMoneyCheckerTest {

    /**
     * Test.
     *
     * @throws IOException Signals that an I/O exception has occurred.
     */
    @Test
    public void test() throws IOException {
        for (MnySampleFile sampleFile : MnySampleFile.SAMPLE_FILES) {
            if (sampleFile.isBackup()) {
                continue;
            }

            File file = new File(sampleFile.getFileName());
            String password = sampleFile.getPassword();

            MsMoneyChecker checker = null;
            try {
                checker = new MsMoneyChecker();
                checker.check(file, password);
            } finally {

            }

        }
    }
}

package com.hungle.sunriise.json;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.Logger;
import org.junit.Assert;
import org.junit.Test;

import com.hungle.sunriise.mnyobject.Account;
import com.hungle.sunriise.mnyobject.EnumAccountType;
import com.hungle.sunriise.mnyobject.EnumInvestmentSubType;
import com.hungle.sunriise.mnyobject.EnumTransactionState;
import com.hungle.sunriise.mnyobject.Frequency;
import com.hungle.sunriise.mnyobject.TransactionInfo;
import com.hungle.sunriise.mnyobject.impl.DefaultAccount;
import com.hungle.sunriise.mnyobject.impl.DefaultBill;
import com.hungle.sunriise.mnyobject.impl.DefaultCategory;
import com.hungle.sunriise.mnyobject.impl.DefaultCurrency;
import com.hungle.sunriise.mnyobject.impl.DefaultFrequency;
import com.hungle.sunriise.mnyobject.impl.DefaultInvestmentTransaction;
import com.hungle.sunriise.mnyobject.impl.DefaultPayee;
import com.hungle.sunriise.mnyobject.impl.DefaultSecurity;
import com.hungle.sunriise.mnyobject.impl.DefaultSecurityHolding;
import com.hungle.sunriise.mnyobject.impl.DefaultTransaction;
import com.hungle.sunriise.mnyobject.impl.DefaultTransactionInfo;
import com.hungle.sunriise.mnyobject.impl.DefaultTransactionSplit;
import com.hungle.sunriise.mnyobject.impl.DefaultTransactionXfer;

// TODO: Auto-generated Javadoc
/**
 * The Class JsonTest.
 */
public class JsonTest {

    /** The Constant LOGGER. */
    private static final Logger LOGGER = org.apache.logging.log4j.LogManager.getLogger(JsonTest.class);

    /**
     * The Class TestType.
     *
     * @param <T> the generic type
     */
    private class TestType<T> {

        /**
         * Instantiates a new test type.
         *
         * @param instance the instance
         */
        public TestType(T instance) {
            super();
            this.instance = instance;
        }

        /** The instance. */
        private T instance;

        /**
         * Gets the single instance of TestType.
         *
         * @return single instance of TestType
         */
        public T getInstance() {
            return instance;
        }
    }

    /**
     * Test serialize.
     *
     * @throws IOException Signals that an I/O exception has occurred.
     */
    @Test
    public void testSerialize() throws IOException {
        List<TestType<Object>> values = new ArrayList<TestType<Object>>();

        // Account
        values.add(new TestType<Object>(new DefaultAccount()));

        // AccountType
        for (EnumAccountType accountType : EnumAccountType.values()) {
            values.add(new TestType<Object>(accountType));
        }

        // Bill
        values.add(new TestType<Object>(new DefaultBill()));

        // Category
        values.add(new TestType<Object>(new DefaultCategory()));

        // TransactionInfo
        values.add(new TestType<Object>(new DefaultTransactionInfo()));

        // Currency
        values.add(new TestType<Object>(new DefaultCurrency()));

        // Frequency
        values.add(
                new TestType<Object>(new DefaultFrequency(Frequency.FRQ_MONTHLY, Frequency.DEFAULT_FRQ_INSTRUCTION)));

        // InvestmentSubType
        for (EnumInvestmentSubType investmentSubType : EnumInvestmentSubType.values()) {
            values.add(new TestType<Object>(investmentSubType));
        }

        // InvestmentTransaction
        values.add(new TestType<Object>(new DefaultInvestmentTransaction()));

        // Payee
        values.add(new TestType<Object>(new DefaultPayee()));

        // Security
        values.add(new TestType<Object>(new DefaultSecurity()));

        // SecurityHolding
        values.add(new TestType<Object>(new DefaultSecurityHolding()));

        // Transaction
        values.add(new TestType<Object>(new DefaultTransaction()));

        // TransactionFilter

        // TransactionInfo
        values.add(new TestType<Object>(new DefaultTransactionInfo()));

        // TransactionSplit
        values.add(new TestType<Object>(new DefaultTransactionSplit()));

        // TransactionState
        for (EnumTransactionState transactionState : EnumTransactionState.values()) {
            values.add(new TestType<Object>(transactionState));
        }

        // TransactionXfer
        values.add(new TestType<Object>(new DefaultTransactionXfer()));

        for (TestType<Object> value : values) {
            Object instance = value.getInstance();
            if (LOGGER.isDebugEnabled()) {
                LOGGER.debug(instance.getClass());
            }
            String str = JSONUtils.writeValueAsString(instance);
            if (LOGGER.isDebugEnabled()) {
                LOGGER.debug(str);
            }
        }
    }

    /**
     * Test deserialize.
     *
     * @throws IOException Signals that an I/O exception has occurred.
     */
    @Test
    public void testDeserializeTransactionInfo() throws IOException {
        Object src = this;
        Class<TransactionInfo> valueType = TransactionInfo.class;

        InputStream stream = JSONUtils.getTestResourceAsStream(src, valueType);
        TransactionInfo valueObject = JSONUtils.deserializeFromStream(stream, valueType);
        Assert.assertNotNull(valueObject);
    }

    @Test
    public void testDeserializeAccount() throws IOException {
        Object src = this;
        Class<Account> valueType = Account.class;

        InputStream stream = JSONUtils.getTestResourceAsStream(src, valueType);
        Account valueObject = JSONUtils.deserializeFromStream(stream, valueType);
        Assert.assertNotNull(valueObject);
    }
}

package com.hungle.sunriise.json;

import static org.hamcrest.CoreMatchers.containsString;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

import java.io.IOException;

import org.apache.logging.log4j.Logger;
import org.junit.Test;

import com.fasterxml.jackson.databind.ObjectMapper;

// TODO: Auto-generated Javadoc
/**
 * The Class ViewTest.
 */
public class ViewTest {

    /** The Constant LOGGER. */
    private static final Logger LOGGER = org.apache.logging.log4j.LogManager.getLogger(ViewTest.class);

    /**
     * Serialize default view test.
     *
     * @throws IOException Signals that an I/O exception has occurred.
     */
    @Test
    public void serializeDefaultViewTest() throws IOException {
        ViewTestUser user = new ViewTestUser(1, "John", "john@email.com");

        String result = JSONUtils.serialize(user);
        LOGGER.info("result=" + result);

        assertThat(result, containsString("John"));

        // MapperFeature.DEFAULT_VIEW_INCLUSION is enable by default
        assertThat(result, containsString("1"));

//        assertThat(result, not(containsString("john@email.com")));
    }

    /**
     * Serialize internal view test.
     *
     * @throws IOException Signals that an I/O exception has occurred.
     */
    @Test
    public void serializeInternalViewTest() throws IOException {
        ViewTestUser user = new ViewTestUser(1, "John", "john@email.com");

        String result = JSONUtils.serialize(user, Views.Internal.class);
        LOGGER.info("result=" + result);

        assertThat(result, containsString("John"));

        // MapperFeature.DEFAULT_VIEW_INCLUSION is enable by default
        assertThat(result, containsString("1"));

        assertThat(result, containsString("john@email.com"));
    }

    /**
     * When use json view to deserialize then correct.
     *
     * @throws IOException Signals that an I/O exception has occurred.
     */
    @Test
    public void whenUseJsonViewToDeserialize_thenCorrect() throws IOException {
        String json = "{\"id\":1,\"name\":\"John\"}";

        ObjectMapper mapper = new ObjectMapper();
        ViewTestUser user = mapper.readerWithView(Views.Public.class).forType(ViewTestUser.class).readValue(json);
        LOGGER.info(user);

        assertEquals(1, user.getId());
        assertEquals("John", user.getName());
    }
}

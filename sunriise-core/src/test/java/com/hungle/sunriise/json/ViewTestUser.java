package com.hungle.sunriise.json;

import com.fasterxml.jackson.annotation.JsonView;

// TODO: Auto-generated Javadoc
/**
 * The Class ViewTestUser.
 */
public class ViewTestUser {

    /** The id. */
    private int id;

    /** The name. */
    @JsonView(Views.Public.class)
    private String name;

    /** The email. */
    @JsonView(Views.Internal.class)
    private String email;

    /**
     * Gets the email.
     *
     * @return the email
     */
    public String getEmail() {
        return email;
    }

    /**
     * Sets the email.
     *
     * @param email the new email
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * Instantiates a new view test user.
     */
    public ViewTestUser() {
        super();
    }

    /**
     * Instantiates a new view test user.
     *
     * @param id    the id
     * @param name  the name
     * @param email the email
     */
    public ViewTestUser(int id, String name, String email) {
        this.id = id;
        this.name = name;
        this.email = email;
    }

    /**
     * Gets the id.
     *
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * Sets the id.
     *
     * @param id the new id
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * Gets the name.
     *
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the name.
     *
     * @param name the new name
     */
    public void setName(String name) {
        this.name = name;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "ViewTestUser [id=" + id + ", name=" + name + "]";
    }
}

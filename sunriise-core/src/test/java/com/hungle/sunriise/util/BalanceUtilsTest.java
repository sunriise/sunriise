/*******************************************************************************
 * Copyright (c) 2016 Hung Le
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *******************************************************************************/
package com.hungle.sunriise.util;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.Logger;
import org.junit.Assert;
import org.junit.Test;

import com.hungle.sunriise.dbutil.TableAccountUtils;
import com.hungle.sunriise.io.MnyDb;
import com.hungle.sunriise.io.sample.MnySampleFileFactory;
import com.hungle.sunriise.mnyobject.Account;
import com.hungle.sunriise.mnyobject.MnyContext;
import com.hungle.sunriise.mnyobject.Transaction;
import com.hungle.sunriise.mnyobject.impl.DefaultAccount;
import com.hungle.sunriise.mnyobject.impl.DefaultFrequency;
import com.hungle.sunriise.mnyobject.impl.DefaultTransaction;

/**
 * The Class BalanceTest.
 */
public class BalanceUtilsTest {
    private static final Logger LOGGER = org.apache.logging.log4j.LogManager.getLogger(BalanceUtilsTest.class);

    public class ExpectedData {
        private BigDecimal balance;

        public BigDecimal getBalance() {
            return balance;
        }

        public void setBalance(BigDecimal balance) {
            this.balance = balance;
        }
    }

    /**
     * Test balance.
     */
    @Test
    public void testBalance() {
//        BigDecimal zero = BigDecimal.ZERO.setScale(2, BigDecimal.ROUND_HALF_EVEN);
        BigDecimal zero = BalanceUtils.formatBalance(BigDecimal.ZERO);

        Account account = new DefaultAccount();
        account.setStartingBalance(zero);

        List<Transaction> transactions = new ArrayList<Transaction>();
        account.setTransactions(transactions);

        BigDecimal currentBalance = null;

        BalanceUtils.calculateAndSetNonInvestmentBalance(account);
        currentBalance = account.getCurrentBalance();
        Assert.assertNotNull(currentBalance);
        Assert.assertEquals(zero, currentBalance);

        Transaction transaction = new DefaultTransaction();
        transaction.setId(0);
        transaction.setFrequency(new DefaultFrequency());
//        BigDecimal expected = new BigDecimal(1.00).setScale(2, BigDecimal.ROUND_HALF_EVEN);
        BigDecimal expected = BalanceUtils.formatBalance(new BigDecimal(1.00));
        transaction.setAmount(expected);

        transactions.add(transaction);
        BalanceUtils.calculateAndSetNonInvestmentBalance(account);
        currentBalance = account.getCurrentBalance();
        Assert.assertNotNull(currentBalance);
        Assert.assertEquals(expected, currentBalance);
    }

    @Test
    public void testSample() throws IOException {
        MnyDb sampleFile = MnySampleFileFactory.getSunsetSample();
        Assert.assertNotNull(sampleFile);

        MnyContext mnyContext = MnyContextUtils.createMnyContext(sampleFile);
        TableAccountUtils.populateAccounts(mnyContext);
        Assert.assertNotNull(mnyContext);

        Map<Integer, ExpectedData> expected = new HashMap<Integer, BalanceUtilsTest.ExpectedData>();
        ExpectedData value = null;

//      2, 157954.72
        createExpectedData(expected, 2, 157954.72);

//      3, -1594.62
        createExpectedData(expected, 3, -1594.62);

//        66, 4244.65
        createExpectedData(expected, 66, 4244.65);

//        67, -57916.20
        createExpectedData(expected, 67, -57916.20);

//        68, 22946.30
        createExpectedData(expected, 68, 22946.30);

//        69, 28100.00
        createExpectedData(expected, 69, 28100.00);

//        16, 38223.90
        createExpectedData(expected, 16, 38223.90);

//        17, 13192.68
        createExpectedData(expected, 17, 13192.68);

//        18, -149122.08
        createExpectedData(expected, 18, -149122.08);

//        19, 355000.00
        createExpectedData(expected, 19, 355000.00);

//        26, 3996.56
        createExpectedData(expected, 26, 3996.56);

//        39, 19305.74
        createExpectedData(expected, 39, 19305.74);

//        42, 22871.06
        createExpectedData(expected, 42, 22871.06);

//        63, -836.00
        createExpectedData(expected, 63, -836.00);

//        72, 19818.60
        createExpectedData(expected, 72, 19818.60);

//        73, -10004.15
        createExpectedData(expected, 73, -10004.15);

//        74, -984.25
        createExpectedData(expected, 74, -984.25);

//        75, 0.00
        createExpectedData(expected, 75, 0.00);

//        76, 0.00
        createExpectedData(expected, 76, 0.00);

//        77, 9000.00
        createExpectedData(expected, 77, 9000.00);

//        78, 255.00
        createExpectedData(expected, 78, 255.00);

//        79, 3870.00
        createExpectedData(expected, 79, 3870.00);

//        80, 2310.00
        createExpectedData(expected, 80, 2310.00);

        Map<Integer, Account> accountsMap = mnyContext.getAccounts();
        Collection<Account> accounts = accountsMap.values();
        for (Account account : accounts) {
            Assert.assertNotNull(account);
//            TableAccountUtils.addTransactionsToAccount(account, mnyContext);

            LOGGER.info("account={}, account.id={}", account.getName(), account.getId());
            BalanceUtils.calculateAndSetBalance(account, mnyContext);
//            System.out.println(account.getId() + ", " + account.getCurrentBalance());

            value = expected.get(account.getId());
            if (value != null) {
                checkAccountBalance(account, value);
            }

        }
    }

    private ExpectedData createExpectedData(Map<Integer, ExpectedData> expected, int id, double balance) {
        ExpectedData value;
        value = new ExpectedData();
        value.setBalance(BalanceUtils.formatBalance(new BigDecimal(balance)));
        expected.put(id, value);
        return value;
    }

    private void checkAccountBalance(Account account, ExpectedData value) {
        Assert.assertEquals(value.getBalance(), account.getCurrentBalance());
    }
}

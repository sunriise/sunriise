package com.hungle.sunriise.util;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Set;

import org.apache.logging.log4j.Logger;
import org.junit.Assert;
import org.junit.Test;

import com.healthmarketscience.jackcess.Table;
import com.hungle.sunriise.diskusage.GetDiskUsageCmd;
import com.hungle.sunriise.io.sample.MnySampleFile;
import com.hungle.sunriise.io.sample.MnySampleFileFactory;

public class GetDiskUsageCmdTest {
    private static final Logger LOGGER = org.apache.logging.log4j.LogManager.getLogger(GetDiskUsageCmdTest.class);

    @Test
    public void testCalculate() throws IOException {
        MnySampleFile sampleFile = MnySampleFileFactory.getSunsetSampleFile();
        Assert.assertNotNull(sampleFile);

        File file = new File(sampleFile.getFileName());
        Assert.assertNotNull(file);
        Assert.assertTrue(file.exists());

        String password = sampleFile.getPassword();

        try (GetDiskUsageCmd cmd = new GetDiskUsageCmd(file, password)) {
            DefaultGetDiskUsageCollector collector = new DefaultGetDiskUsageCollector();
            cmd.calculate(collector);

            Assert.assertEquals(7979673L, collector.getBytesCount());

            Set<String> systemTableNames = collector.getSystemTableNames();
            Assert.assertEquals(4, systemTableNames.size());

            Set<String> tableNames = collector.getTableNames();
            Assert.assertEquals(84, tableNames.size());

            long runningByteCount = 0L;
            List<Table> tables = collector.getTables();
            Assert.assertEquals(88, tables.size());
            for (Table table : tables) {
                String name = table.getName();
                int rowCount = table.getRowCount();
                int indexCount = table.getIndexes().size();
                long byteCount = GetDiskUsageCmd.getBytesCount(table);

                String str = name + "," + rowCount + "," + indexCount + "," + byteCount;
                LOGGER.info(str);

                runningByteCount += byteCount;
            }
            Assert.assertEquals(collector.getBytesCount(), runningByteCount);
        }
    }
}

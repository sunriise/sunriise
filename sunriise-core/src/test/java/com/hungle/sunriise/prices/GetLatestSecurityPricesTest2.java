package com.hungle.sunriise.prices;

import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.Logger;
import org.junit.Assert;
import org.junit.Test;

import com.healthmarketscience.jackcess.CursorBuilder;
import com.healthmarketscience.jackcess.Database;
import com.healthmarketscience.jackcess.IndexCursor;
import com.healthmarketscience.jackcess.Row;
import com.healthmarketscience.jackcess.Table;
import com.healthmarketscience.jackcess.util.EntryIterableBuilder;
import com.hungle.sunriise.dbutil.TableSecurityPriceUtils;
import com.hungle.sunriise.dbutil.TableSecurityUtils;
import com.hungle.sunriise.io.DefaultMnyDbTest;
import com.hungle.sunriise.io.sample.MnySampleFile;
import com.hungle.sunriise.mnyobject.Security;
import com.hungle.sunriise.mnyobject.SecurityPrice;

// TODO: Auto-generated Javadoc
/**
 * The Class TableSecurityPriceUtilTest.
 */
public class GetLatestSecurityPricesTest2 extends DefaultMnyDbTest {

    /** The Constant LOGGER. */
    private static final Logger LOGGER = org.apache.logging.log4j.LogManager
            .getLogger(GetLatestSecurityPricesTest2.class);

    @Override
    public MnySampleFile getMnySampleFile() {
        boolean useLargeFileIfAvailable = true;
        return getMnySampleFile(super.getMnySampleFile(), useLargeFileIfAvailable);
    }

    @Test
    public void testIndexCursor() throws IOException {
        Database db = getMnyDb().getDb();

        Table table = TableSecurityPriceUtils.getTable(db);
        IndexCursor cursor = CursorBuilder.createCursor(table.getIndex("HsecDateSrcSp"));

        Map<Integer, Security> securities = TableSecurityUtils.getMap(db);

        ArrayList<Security> values = new ArrayList<Security>();
        values.addAll(securities.values());
        Collections.sort(values, new TableSecurityUtils.CompareSecurityByName());

        for (Security security : values) {
            SecurityPrice securityPrice = testIndexCursor(cursor, security.getId());
            System.out.println("secId=" + security.getId() + ", symbol=" + security.getSymbol() + ", name="
                    + security.getName() + ", securityPrice=" + securityPrice);
        }
    }

    private SecurityPrice testIndexCursor(IndexCursor cursor, Integer secId) {
        SecurityPrice securityPrice = null;
        Date lastDate = null;
        cursor.beforeFirst();
        EntryIterableBuilder newEntryIterable = cursor.newEntryIterable(secId);
        for (Row row : newEntryIterable) {
            securityPrice = TableSecurityPriceUtils.deserialize(row);
            Assert.assertEquals(secId, securityPrice.getSecId());
            Date date = securityPrice.getDate();
            if (lastDate != null) {
                Assert.assertTrue(date.compareTo(lastDate) >= 0);
            }
            lastDate = date;
        }
        return securityPrice;
    }

    @Test
    public void testLastPriceByIndex() throws IOException, ParseException {
        Database db = getMnyDb().getDb();

        try {
            Table table = TableSecurityPriceUtils.getTable(db);
            IndexCursor cursor = CursorBuilder.createCursor(table.getIndex("HsecDateSrcSp"));

            Map<Integer, Security> securities = TableSecurityUtils.getMap(db);
            List<Security> list = new ArrayList<Security>();
            list.addAll(securities.values());
            Comparator<Security> comparator = new TableSecurityUtils.CompareSecurityByName();
            Collections.sort(list, comparator);
            for (Security security : list) {
                SecurityPrice secPrice = GetLatestSecurityPrices.getLastestSecurityPrice(security.getId(), cursor);

                LOGGER.info(security.getSymbol() + ", " + security.getName() + ", " + secPrice);
            }

        } finally {

        }
    }
}

package com.hungle.sunriise.prices;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import org.apache.logging.log4j.Logger;
import org.junit.Assert;
import org.junit.Assume;
import org.junit.BeforeClass;
import org.junit.Test;

import com.healthmarketscience.jackcess.CursorBuilder;
import com.healthmarketscience.jackcess.Database;
import com.healthmarketscience.jackcess.IndexCursor;
import com.healthmarketscience.jackcess.Row;
import com.healthmarketscience.jackcess.Table;
import com.healthmarketscience.jackcess.util.EntryIterableBuilder;
import com.hungle.sunriise.dbutil.TableSecurityPriceUtils;
import com.hungle.sunriise.dbutil.TableSecurityUtils;
import com.hungle.sunriise.io.DefaultMnyDbTest;
import com.hungle.sunriise.io.MnyDb;
import com.hungle.sunriise.io.sample.MnySampleFile;
import com.hungle.sunriise.mnyobject.Security;
import com.hungle.sunriise.mnyobject.SecurityPrice;

// TODO: Auto-generated Javadoc
/**
 * The Class TableSecurityPriceUtilTest.
 */
public class GetLatestSecurityPricesTest extends DefaultMnyDbTest {

    /** The Constant LOGGER. */
    private static final Logger LOGGER = org.apache.logging.log4j.LogManager
            .getLogger(GetLatestSecurityPricesTest.class);

    @BeforeClass
    public static final void beforeClass() {
        // TODO: work-around until we can understand how to deal with this.
//        String tzId = "America/Los_Angeles";
//        tzId = "UTC";
//        TimeZone zone = TimeZone.getTimeZone(tzId);
//        TimeZone.setDefault(zone);
    }

    @Override
    public void initialize() throws IOException {
        File file = new File(MnySampleFile.LOCAL_LARGE_FILE_MNY);
        if (file.exists()) {
            String password = null;
            TimeZone timeZone = null;

            if (timeZone == null) {
                setMnyDb(new MnyDb(file, password));
            } else {
                setMnyDb(new MnyDb(file, password, timeZone));
            }
            Assume.assumeTrue(getMnyDb() != null);
        } else {
            super.initialize();
        }
    }

    @Test
    public void test01() throws IOException {
        Database db = getMnyDb().getDb();

        int expectedSize = 1393;
        int expectedBucketsSize = 33;

//        String name = mnyDb.getDbFile().getName();
        if (isLargeFile()) {
//            expectedSize = 42841;
//            expectedBucketsSize= 85;
            expectedSize = -1;
            expectedBucketsSize = -1;
        }

        checkLatestPrices(db, expectedSize, expectedBucketsSize);
    }

    private boolean isLargeFile() {
        String name = getMnyDb().getDbFile().getName();
        return MnySampleFile.LOCAL_LARGE_FILE_MNY.endsWith(name);
    }

    private void checkLatestPrices(Database db, int expectedSize, int expectedBucketsSize) throws IOException {
        List<SecurityPrice> prices = TableSecurityPriceUtils.getSecurityPrices(db);
        Assert.assertNotNull(prices);
        if (expectedSize > 0) {
            Assert.assertEquals(expectedSize, prices.size());
        }

        Map<Integer, List<SecurityPrice>> priceBuckets = GetLatestSecurityPricesUtils.createPriceBuckets(db, prices);
        if (expectedBucketsSize > 0) {
            Assert.assertEquals(expectedBucketsSize, priceBuckets.size());
        }

//        Table table = TableSecurityPriceUtil.getTable(db);
        GetLatestSecurityPrices getLatestSecurityPrices = new GetLatestSecurityPrices(db);
        Map<Integer, Security> securities = TableSecurityUtils.getMap(db);
        for (Integer securityId : securities.keySet()) {
            SecurityPrice securityPrice = getLatestSecurityPrices.getPrice(securityId);
            SecurityPrice expectedSecurityPrice = OldGetLatestSecurityPricesUtils.getLatestPrice(securityId,
                    priceBuckets);

            assertSecurityPriceEquals(expectedSecurityPrice, securityPrice);
        }
    }

    private void assertSecurityPriceEquals(SecurityPrice expected, SecurityPrice actual) {
        if (expected == null) {
            Assert.assertNull(actual);
            return;
        } else {
            Assert.assertNotNull(actual);
        }

//        Assert.assertEquals(36.250, Math.round(securityPrice.getPrice() * 1000.0) / 1000.0, 0.0);
        if (expected.getPrice() == null) {
            Assert.assertNull(actual.getPrice());
        } else {
            Assert.assertEquals(expected.getPrice(), actual.getPrice());
        }

//        Assert.assertEquals("07/10/2007", dateFormat.format(actual.getDate()));
        if (expected.getDate() == null) {
            Assert.assertNull(actual.getDate());
        } else {
            Assert.assertEquals(expected.getDate(), actual.getDate());
        }

//        Assert.assertEquals(EnumSecurityPriceSrc.ONLINE, actual.getSrc());  
        if (expected.getSrc() == null) {
            Assert.assertNull(actual.getSrc());
        } else {
            Assert.assertEquals(expected.getSrc(), actual.getSrc());
        }
    }

    @Test
    public void testIndexCursor() throws IOException {
        Database db = getMnyDb().getDb();
        Table table = TableSecurityPriceUtils.getTable(db);
        IndexCursor cursor = CursorBuilder.createCursor(table.getIndex("HsecDateSrcSp"));

        Map<Integer, Security> securities = TableSecurityUtils.getMap(db);

        ArrayList<Security> values = new ArrayList<Security>();
        values.addAll(securities.values());
        Collections.sort(values, new TableSecurityUtils.CompareSecurityByName());

        for (Security security : values) {
            SecurityPrice securityPrice = testIndexCursor(cursor, security.getId());
            System.out.println("secId=" + security.getId() + ", symbol=" + security.getSymbol() + ", name="
                    + security.getName() + ", securityPrice=" + securityPrice);
        }
    }

    private SecurityPrice testIndexCursor(IndexCursor cursor, Integer secId) {
        SecurityPrice securityPrice = null;
        Date lastDate = null;
        cursor.beforeFirst();
        EntryIterableBuilder rows = cursor.newEntryIterable(secId);
        for (Row row : rows) {
            securityPrice = TableSecurityPriceUtils.deserialize(row);
            Assert.assertEquals(secId, securityPrice.getSecId());
            Date date = securityPrice.getDate();
            if (lastDate != null) {
                Assert.assertTrue(date.compareTo(lastDate) >= 0);
            }
            lastDate = date;
        }
        return securityPrice;
    }
}

package com.hungle.sunriise.prices;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import org.apache.logging.log4j.Logger;
import org.junit.Assert;
import org.junit.Assume;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

import com.healthmarketscience.jackcess.CursorBuilder;
import com.healthmarketscience.jackcess.Database;
import com.healthmarketscience.jackcess.IndexCursor;
import com.healthmarketscience.jackcess.Row;
import com.healthmarketscience.jackcess.Table;
import com.healthmarketscience.jackcess.util.EntryIterableBuilder;
import com.hungle.sunriise.dbutil.TableSecurityPriceUtils;
import com.hungle.sunriise.dbutil.TableSecurityUtils;
import com.hungle.sunriise.io.DefaultMnyDbTest;
import com.hungle.sunriise.io.MnyDb;
import com.hungle.sunriise.io.sample.MnySampleFile;
import com.hungle.sunriise.mnyobject.EnumSecurityPriceSrc;
import com.hungle.sunriise.mnyobject.Security;
import com.hungle.sunriise.mnyobject.SecurityPrice;

// TODO: Auto-generated Javadoc
/**
 * The Class TableSecurityPriceUtilTest.
 */
public class OldGetLatestSecurityPricesUtilsTest extends DefaultMnyDbTest {

    /** The Constant LOGGER. */
    private static final Logger LOGGER = org.apache.logging.log4j.LogManager
            .getLogger(OldGetLatestSecurityPricesUtilsTest.class);

    private static final String KEY_ID = "id";
    private static final String KEY_NAME = "name";
    private static final String KEY_SYMBOL = "symbol";
    private static final String KEY_SRC = "src";
    private static final String KEY_DATE = "date";
    private static final String KEY_PRICE = "price";
    private static final String KEY_SEC_ID = "secId";

    @BeforeClass
    public static final void beforeClass() {
        // TODO: work-around until we can understand how to deal with this.
//        String tzId = "America/Los_Angeles";
//        tzId = "UTC";
//        TimeZone zone = TimeZone.getTimeZone(tzId);
//        TimeZone.setDefault(zone);
    }

    @Override
    public void initialize() throws IOException {
        File file = new File(MnySampleFile.LOCAL_LARGE_FILE_MNY);
        if (file.exists()) {
            String password = null;
            TimeZone timeZone = null;

            if (timeZone == null) {
                setMnyDb(new MnyDb(file, password));
            } else {
                setMnyDb(new MnyDb(file, password, timeZone));
            }
            Assume.assumeTrue(getMnyDb() != null);
        } else {
            super.initialize();
        }
    }

    /**
     * Test get price.
     *
     * @throws IOException Signals that an I/O exception has occurred.
     */
    @Test
    @Ignore
    public void testGetPrice() throws IOException {

        Database db = getMnyDb().getDb();

        Date date = null;

        Map<Integer, Security> securities = TableSecurityUtils.getMap(db);
        for (Integer key : securities.keySet()) {
            Security security = securities.get(key);
            Integer securityId = key;

            Double price1 = OldGetLatestSecurityPricesUtils.getSecurityLatestPriceByIteratingBackward(securityId, date,
                    db);
            // Assert.assertNotNull(price);
            Double price2 = OldGetLatestSecurityPricesUtils.getSecurityLatestPriceByCollecting(securityId, date, db);

            LOGGER.info(security + ", date=" + date + ", price1=" + price1 + ", price2=" + price2);

            Assert.assertEquals(price1, price2);

        }
    }

    private boolean isLargeFile() {
        String name = getMnyDb().getDbFile().getName();
        return MnySampleFile.LOCAL_LARGE_FILE_MNY.endsWith(name);
    }

    private void todo(Database db, Map<Integer, List<SecurityPrice>> priceBuckets) throws IOException {
        // SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/YYYY");
        Integer secId = null;
        SecurityPrice securityPrice = null;
        SecurityPrice expectedSecurityPrice = null;

        /*
         * DefaultSecurity [id=98, name=Adatum Corp, symbol=ADM]
         */
        secId = 98;
        securityPrice = OldGetLatestSecurityPricesUtils.getLatestPrice(secId, db);
        expectedSecurityPrice = OldGetLatestSecurityPricesUtils.getLatestPrice(secId, priceBuckets);
        assertSecurityPriceEquals(expectedSecurityPrice, securityPrice);
//        Assert.assertNotNull(securityPrice);
//        Assert.assertEquals(36.250, Math.round(securityPrice.getPrice() * 1000.0) / 1000.0, 0.0);
//        Assert.assertEquals("07/10/2007", dateFormat.format(securityPrice.getDate()));
//        Assert.assertEquals(EnumSecurityPriceSrc.ONLINE, securityPrice.getSrc());

        /*
         * DefaultSecurity [id=99, name=America Online, symbol=null]
         */
        secId = 99;
        securityPrice = OldGetLatestSecurityPricesUtils.getLatestPrice(secId, db);
        expectedSecurityPrice = OldGetLatestSecurityPricesUtils.getLatestPrice(secId, priceBuckets);
        assertSecurityPriceEquals(expectedSecurityPrice, securityPrice);
//        Assert.assertNotNull(securityPrice);
//        Assert.assertNull(securityPrice.getPrice());

        /*
         * DefaultSecurity [id=100, name=Avery Dennison, symbol=null]
         */
        secId = 100;
        securityPrice = OldGetLatestSecurityPricesUtils.getLatestPrice(secId, db);
        expectedSecurityPrice = OldGetLatestSecurityPricesUtils.getLatestPrice(secId, priceBuckets);
        assertSecurityPriceEquals(expectedSecurityPrice, securityPrice);
//        Assert.assertNull(securityPrice);

        /*
         * DefaultSecurity [id=75, name=Bank of America Corp, symbol=null]
         */
        secId = 75;
        securityPrice = OldGetLatestSecurityPricesUtils.getLatestPrice(secId, db);
        expectedSecurityPrice = OldGetLatestSecurityPricesUtils.getLatestPrice(secId, priceBuckets);
        assertSecurityPriceEquals(expectedSecurityPrice, securityPrice);
//        Assert.assertNull(securityPrice);

        /*
         * DefaultSecurity [id=93, name=Blue Skies Airlines 5.375% 7/07, symbol=null]
         */
        secId = 93;
        securityPrice = OldGetLatestSecurityPricesUtils.getLatestPrice(secId, db);
        expectedSecurityPrice = OldGetLatestSecurityPricesUtils.getLatestPrice(secId, priceBuckets);
        assertSecurityPriceEquals(expectedSecurityPrice, securityPrice);
//        Assert.assertNull(securityPrice);

        /*
         * DefaultSecurity [id=88, name=Blue Yonder Airlines 6.7% 11/06, symbol=null]
         */
        secId = 88;
        securityPrice = OldGetLatestSecurityPricesUtils.getLatestPrice(secId, db);
        expectedSecurityPrice = OldGetLatestSecurityPricesUtils.getLatestPrice(secId, priceBuckets);
        assertSecurityPriceEquals(expectedSecurityPrice, securityPrice);
//        Assert.assertNull(securityPrice);
        /*
         * DefaultSecurity [id=67, name=Blue Yonder Airlines, symbol=null]
         */
        secId = 67;
        securityPrice = OldGetLatestSecurityPricesUtils.getLatestPrice(secId, db);
        expectedSecurityPrice = OldGetLatestSecurityPricesUtils.getLatestPrice(secId, priceBuckets);
        assertSecurityPriceEquals(expectedSecurityPrice, securityPrice);
        expectedSecurityPrice = OldGetLatestSecurityPricesUtils.getLatestPrice(secId, priceBuckets);
        assertSecurityPriceEquals(expectedSecurityPrice, securityPrice);
//        Assert.assertNotNull(securityPrice);
//        Assert.assertEquals(100.750, Math.round(securityPrice.getPrice() * 1000.0) / 1000.0, 0.0);
//        Assert.assertEquals("02/14/2003", dateFormat.format(securityPrice.getDate()));
//        Assert.assertEquals(EnumSecurityPriceSrc.BUY, securityPrice.getSrc());

        /*
         * DefaultSecurity [id=79, name=Borland Software Corporation, symbol=null]
         */
        secId = 79;
        securityPrice = OldGetLatestSecurityPricesUtils.getLatestPrice(secId, db);
        expectedSecurityPrice = OldGetLatestSecurityPricesUtils.getLatestPrice(secId, priceBuckets);
        assertSecurityPriceEquals(expectedSecurityPrice, securityPrice);
//        Assert.assertNull(securityPrice);
        /*
         * DefaultSecurity [id=82, name=Carnival Corp, symbol=null]
         */
        secId = 82;
        securityPrice = OldGetLatestSecurityPricesUtils.getLatestPrice(secId, db);
        expectedSecurityPrice = OldGetLatestSecurityPricesUtils.getLatestPrice(secId, priceBuckets);
        assertSecurityPriceEquals(expectedSecurityPrice, securityPrice);
//        Assert.assertNull(securityPrice);

        /*
         * DefaultSecurity [id=117, name=*Cash*, symbol=null]
         */
        secId = 117;
        securityPrice = OldGetLatestSecurityPricesUtils.getLatestPrice(secId, db);
        expectedSecurityPrice = OldGetLatestSecurityPricesUtils.getLatestPrice(secId, priceBuckets);
        assertSecurityPriceEquals(expectedSecurityPrice, securityPrice);
//        Assert.assertNull(securityPrice);

        /*
         * DefaultSecurity [id=74, name=Coho Wineries, symbol=null]
         */
        secId = 74;
        securityPrice = OldGetLatestSecurityPricesUtils.getLatestPrice(secId, db);
        Assert.assertNull(securityPrice);

        /*
         * DefaultSecurity [id=68, name=Coho Winery, symbol=COHO]
         */
        secId = 68;
        securityPrice = OldGetLatestSecurityPricesUtils.getLatestPrice(secId, db);
        expectedSecurityPrice = OldGetLatestSecurityPricesUtils.getLatestPrice(secId, priceBuckets);
        assertSecurityPriceEquals(expectedSecurityPrice, securityPrice);
//        Assert.assertNotNull(securityPrice);
//        Assert.assertEquals(0.00, Math.round(securityPrice.getPrice() * 1000.0) / 1000.0, 0.0);
//        Assert.assertEquals("07/06/2006", dateFormat.format(securityPrice.getDate()));
//        Assert.assertEquals(EnumSecurityPriceSrc.ONLINE, securityPrice.getSrc());

        /*
         * DefaultSecurity [id=77, name=Consolidated Messengers, symbol=CONSO]
         */
        secId = 77;
        securityPrice = OldGetLatestSecurityPricesUtils.getLatestPrice(secId, db);
        expectedSecurityPrice = OldGetLatestSecurityPricesUtils.getLatestPrice(secId, priceBuckets);
        assertSecurityPriceEquals(expectedSecurityPrice, securityPrice);
//        Assert.assertNotNull(securityPrice);
//        Assert.assertEquals(69.150, Math.round(securityPrice.getPrice() * 1000.0) / 1000.0, 0.0);
//        Assert.assertEquals("10/15/2002", dateFormat.format(securityPrice.getDate()));
//        Assert.assertEquals(EnumSecurityPriceSrc.ONLINE, securityPrice.getSrc());
    }

    private void assertSecurityPriceEquals(SecurityPrice expected, SecurityPrice actual) {
        if (expected == null) {
            Assert.assertNull(actual);
            return;
        } else {
            Assert.assertNotNull(actual);
        }

//        Assert.assertEquals(36.250, Math.round(securityPrice.getPrice() * 1000.0) / 1000.0, 0.0);
        if (expected.getPrice() == null) {
            Assert.assertNull(actual.getPrice());
        } else {
            Assert.assertEquals(expected.getPrice(), actual.getPrice());
        }

//        Assert.assertEquals("07/10/2007", dateFormat.format(actual.getDate()));
        if (expected.getDate() == null) {
            Assert.assertNull(actual.getDate());
        } else {
            Assert.assertEquals(expected.getDate(), actual.getDate());
        }

//        Assert.assertEquals(EnumSecurityPriceSrc.ONLINE, actual.getSrc());  
        if (expected.getSrc() == null) {
            Assert.assertNull(actual.getSrc());
        } else {
            Assert.assertEquals(expected.getSrc(), actual.getSrc());
        }
    }

    @Test
    public void testIndexCursor() throws IOException {
        Database db = getMnyDb().getDb();
        Table table = TableSecurityPriceUtils.getTable(db);
        IndexCursor cursor = CursorBuilder.createCursor(table.getIndex("HsecDateSrcSp"));

        Map<Integer, Security> securities = TableSecurityUtils.getMap(db);

        ArrayList<Security> values = new ArrayList<Security>();
        values.addAll(securities.values());
        Collections.sort(values, new TableSecurityUtils.CompareSecurityByName());

        for (Security security : values) {
            SecurityPrice securityPrice = testIndexCursor(cursor, security.getId());
            System.out.println("secId=" + security.getId() + ", symbol=" + security.getSymbol() + ", name="
                    + security.getName() + ", securityPrice=" + securityPrice);
        }
    }

    private static final SecurityPrice getLastestSecurityPrice(IndexCursor cursor, Integer secId) {
        SecurityPrice securityPrice = null;
        Date lastDate = null;
        cursor.beforeFirst();
        EntryIterableBuilder newEntryIterable = cursor.newEntryIterable(secId);
        for (Row row : newEntryIterable) {
            securityPrice = TableSecurityPriceUtils.deserialize(row);
            Date date = securityPrice.getDate();
            lastDate = date;
        }
        return securityPrice;
    }

    private SecurityPrice testIndexCursor(IndexCursor cursor, Integer secId) {
        SecurityPrice securityPrice = null;
        Date lastDate = null;
        cursor.beforeFirst();
        EntryIterableBuilder newEntryIterable = cursor.newEntryIterable(secId);
        for (Row row : newEntryIterable) {
            securityPrice = TableSecurityPriceUtils.deserialize(row);
            Assert.assertEquals(secId, securityPrice.getSecId());
            Date date = securityPrice.getDate();
            if (lastDate != null) {
                Assert.assertTrue(date.compareTo(lastDate) >= 0);
            }
            lastDate = date;
        }
        return securityPrice;
    }

    @Test
    public void testLastPriceByIndex() throws IOException, ParseException {
        Assume.assumeTrue(!isLargeFile());

        Database db = getMnyDb().getDb();

        BufferedReader reader = null;
        try {
            Table table = TableSecurityPriceUtils.getTable(db);
            IndexCursor cursor = CursorBuilder.createCursor(table.getIndex("HsecDateSrcSp"));

            InputStream stream = this.getClass().getResourceAsStream("sunset-sample-pwd-secprice-01.txt");
            Assert.assertNotNull(stream);
            reader = new BufferedReader(new InputStreamReader(stream, Charset.forName("UTF-8")));
            String line = null;
            while ((line = reader.readLine()) != null) {
                Map<String, Object> expectedValues = parseExpectedLine(line);
                testLastPriceByIndex(cursor, expectedValues);
            }
        } finally {
            if (reader != null) {
                reader.close();
                reader = null;
            }
        }
    }

    private void testLastPriceByIndex(IndexCursor cursor, Map<String, Object> expectedValues) throws IOException {
        LOGGER.info(expectedValues);

        Integer secId = (Integer) expectedValues.get(KEY_SEC_ID);
        Assert.assertNotNull(secId);

        SecurityPrice secPrice = getLastestSecurityPrice(cursor, secId);

        Double price = (Double) expectedValues.get(KEY_PRICE);

        if (price == null) {
            if (secPrice != null) {
                Assert.assertNull(secPrice.getPrice());
            } else {
                Assert.assertNull(secPrice);
            }
        } else {
            Assert.assertNotNull(secPrice);
            Assert.assertEquals(price, Math.round(secPrice.getPrice() * 1000.0) / 1000.0, 0.0005);

            Date date = (Date) expectedValues.get(KEY_DATE);
            Date secDate = secPrice.getDate();
//            // XXX: date stored in *.mny is local time at time of writing WITHOUT timezone
//            TimeZone dbTimeZone = TimeZone.getTimeZone("PDT");
//            Date adjustedSecDate = adjustSecDate(secDate, dbTimeZone);
            Date adjustedSecDate = secDate;

            if (LOGGER.isDebugEnabled()) {
                LOGGER.debug("expected: " + date);
                LOGGER.debug("actual: " + adjustedSecDate);
            }

            Assert.assertEquals(date, adjustedSecDate);

            EnumSecurityPriceSrc src = (EnumSecurityPriceSrc) expectedValues.get(KEY_SRC);
            Assert.assertEquals(src, secPrice.getSrc());
        }
    }

    private Map<String, Object> parseExpectedLine(String line) throws ParseException {
        Map<String, Object> values = new HashMap<String, Object>();

        LOGGER.info("***" + line);

        // secId=117, symbol=null, name=*Cash*, securityPrice=null
        if (line == null) {
            return values;
        }
        line = line.trim();

        String[] tokens = line.split(",");
        if (tokens == null) {
            return values;
        }

        Integer secId = null;
        String symbol = null;
        String name = null;
        Integer id = null;
        // Sat Dec 01 00:00:00 PST 2007
        SimpleDateFormat dateFormat = new SimpleDateFormat("EEE MMM dd HH:mm:ss zzz yyyy");
        Date date = null;
        Double price = null;
        EnumSecurityPriceSrc src = null;
        for (String token : tokens) {
            String[] pair = token.split("=");
            if (pair == null) {
                continue;
            }
            if (pair.length < 2) {
                continue;
            }
            String n = pair[0].trim();
            String v = pair[1].trim();

            if (n.compareTo(KEY_SEC_ID) == 0) {
                secId = Integer.valueOf(v);
            } else if (n.compareTo(KEY_SYMBOL) == 0) {
                symbol = v;
            } else if (n.compareTo(KEY_NAME) == 0) {
                name = v;
            } else if (n.compareTo("securityPrice") == 0) {
                // securityPrice=DefaultSecurityPrice [id=2144
                int indexOf = v.indexOf("[");
                if (indexOf >= 0) {
                    n = v.substring(indexOf + 1);
                    n = n.trim();

                    if (n.compareToIgnoreCase(KEY_ID) == 0) {
                        v = pair[2].trim();
                        v = v.trim();
                        id = Integer.valueOf(v);
                    }
                }
            } else if (n.compareTo(KEY_DATE) == 0) {
                // date=Sat Dec 01 00:00:00 PST 2007, price=16.25, src=UPDATE]
                LOGGER.info("Input expected date string: " + v);
                date = dateFormat.parse(v);
            } else if (n.compareTo(KEY_PRICE) == 0) {
                // date=Sat Dec 01 00:00:00 PST 2007, price=16.25, src=UPDATE]
                try {
                    price = Double.valueOf(v);
                } catch (NumberFormatException e) {
                    LOGGER.warn(e);
                }
            } else if (n.compareTo(KEY_SRC) == 0) {
                // date=Sat Dec 01 00:00:00 PST 2007, price=16.25, src=UPDATE]
                int indexOf = v.indexOf("]");
                if (indexOf >= 0) {
                    v = v.substring(0, indexOf);
                }
                src = EnumSecurityPriceSrc.valueOf(v);
            }

        }
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("  secId=" + secId);
            LOGGER.debug("  symbol=" + symbol);
            LOGGER.debug("  name=" + name);
            LOGGER.debug("  id=" + id);
            LOGGER.debug("  date=" + date);
            LOGGER.debug("  price=" + price);
            LOGGER.debug("  src=" + src);
        }

        values.put(KEY_SEC_ID, secId);
        values.put(KEY_SYMBOL, symbol);
        values.put(KEY_NAME, name);
        values.put(KEY_ID, id);
        values.put(KEY_DATE, date);
        values.put(KEY_PRICE, price);
        values.put(KEY_SRC, src);

        return values;

    }
}

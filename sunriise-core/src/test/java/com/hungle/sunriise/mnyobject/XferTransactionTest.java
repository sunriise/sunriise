package com.hungle.sunriise.mnyobject;

import java.io.File;
import java.io.IOException;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.apache.logging.log4j.Logger;
import org.junit.Assert;
import org.junit.Test;

import com.healthmarketscience.jackcess.Database;
import com.hungle.sunriise.dbutil.TableTransactionUtils;
import com.hungle.sunriise.dbutil.TableTransationXferUtils;
import com.hungle.sunriise.io.MnyDb;
import com.hungle.sunriise.io.sample.MnySampleFile;

// TODO: Auto-generated Javadoc
/**
 * The Class XferTransactionTest.
 */
public class XferTransactionTest {

    /** The Constant LOGGER. */
    private static final Logger LOGGER = org.apache.logging.log4j.LogManager.getLogger(XferTransactionTest.class);

    /**
     * Test xfer.
     *
     * @throws IOException Signals that an I/O exception has occurred.
     */
    @Test
    public void testXfer() throws IOException {
        File file = null;

        file = new File(MnySampleFile.LOCAL_LARGE_FILE_MNY);
        testXfer(file);

        file = MnySampleFile.getSampleFile();
        testXfer(file);
    }

    /**
     * Test xfer.
     *
     * @param file the file
     * @throws IOException Signals that an I/O exception has occurred.
     */
    private void testXfer(File file) throws IOException {
        MnyDb mnyDb = null;
        try {
            String password = null;
            if (!file.exists()) {
                return;
            }
            mnyDb = new MnyDb(file, password);
            Assert.assertNotNull(mnyDb);

            Database db = mnyDb.getDb();

            Map<Integer, Transaction> transactions = TableTransactionUtils.getTransactions(db);

            Map<Integer, TransactionXfer> transactionXfers = TableTransationXferUtils.getTransactionXfers(db);
            Set<Integer> keySet = transactionXfers.keySet();
            Assert.assertNotNull(keySet);
            int xferCount = keySet.size();
            LOGGER.info("Number of xfer entries: " + xferCount);
            Assert.assertTrue(xferCount > 0);

            Set<Integer> fromIds = new HashSet<Integer>();
            Set<Integer> toIds = new HashSet<Integer>();
            for (Integer key : keySet) {
                TransactionXfer xfer = transactionXfers.get(key);

                Transaction transaction = null;

                Integer fromId = xfer.getFromTransactionId();
                Assert.assertNotNull(fromId);
                transaction = transactions.get(fromId);
                Assert.assertNotNull(transaction);
                fromIds.add(fromId);

                Integer toId = xfer.getToTransactionId();
                Assert.assertNotNull(toId);
                transaction = transactions.get(toId);
                Assert.assertNotNull(transaction);
                toIds.add(toId);

                Assert.assertNotEquals(fromId, toId);
            }

            int isXferTransactionCount = 0;
            Set<Integer> inBoth = new HashSet<Integer>();
            Set<Integer> inToOnly = new HashSet<Integer>();
            Set<Integer> inFromOnly = new HashSet<Integer>();

            Set<Integer> badFromXfer = new HashSet<Integer>();
            Set<Integer> goodFromXfer = new HashSet<Integer>();
            Set<Integer> badToXfer = new HashSet<Integer>();
            Set<Integer> goodToXfer = new HashSet<Integer>();
            for (Integer key : transactions.keySet()) {
                Transaction transaction = transactions.get(key);
                if (transaction.isTransfer()) {
                    isXferTransactionCount++;

                    Integer id = transaction.getId();
                    boolean inToIds = toIds.contains(id);
                    boolean inFromIds = fromIds.contains(id);

                    if (inToIds && inFromIds) {
                        inBoth.add(id);
                    } else if (inToIds) {
                        inToOnly.add(id);
                        if (!transaction.getTransactionInfo().isTransferTo()) {
                            badToXfer.add(id);
                        } else {
                            goodToXfer.add(id);
                        }
                    } else if (inFromIds) {
                        inFromOnly.add(id);
                        if (transaction.getTransactionInfo().isTransferTo()) {
                            badFromXfer.add(id);
                        } else {
                            goodFromXfer.add(id);
                        }
                    } else {
                        Assert.assertFalse("transaction id=" + id + " does not have xfer's id", true);
                    }
                }
            }

            if (LOGGER.isDebugEnabled()) {
                LOGGER.debug("Number of transaction.isTransfer entries: " + isXferTransactionCount);
                LOGGER.debug(
                        "both=" + inBoth.size() + ", toOnly=" + inToOnly.size() + ", fromOnly=" + inFromOnly.size());
                LOGGER.debug("badToXfer=" + badToXfer.size() + ", fromOnly=" + badFromXfer.size());
            }

            Assert.assertTrue("There are positive number of transaction with badToXfer.", badToXfer.size() == 0);
            Assert.assertTrue("There are positive number of transaction with badFromXfer.", badFromXfer.size() == 0);

            Assert.assertTrue("There are positive number of transaction with both to and from xfer.",
                    inBoth.size() == 0);

        } finally {
            if (mnyDb != null) {
                mnyDb.close();
            }
        }
    }
}

package com.hungle.sunriise.mnyobject;

import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Assert;
import org.junit.Test;

import com.healthmarketscience.jackcess.Database;
import com.healthmarketscience.jackcess.Row;
import com.hungle.sunriise.dbutil.CalculateMonthlySummary;
import com.hungle.sunriise.dbutil.TableAccountUtils;
import com.hungle.sunriise.dbutil.TableCategoryUtils;
import com.hungle.sunriise.dbutil.TableTransactionUtils;
import com.hungle.sunriise.dbutil.TableTransationXferUtils;
import com.hungle.sunriise.io.MnyDb;
import com.hungle.sunriise.io.sample.MnySampleFile;
import com.hungle.sunriise.json.JSONUtils;
import com.hungle.sunriise.mnyobject.impl.DefaultAccount;
import com.hungle.sunriise.util.BalanceUtils;
import com.hungle.sunriise.util.MnyContextUtils;

/**
 * The Class AccountTest.
 */
public class AccountTest {

    /** The Constant LOGGER. */
    private static final Logger LOGGER = LogManager.getLogger(AccountTest.class);

    private Map<String, BigDecimal> expectRunningBalances = new HashMap<>();

    /**
     * Test create.
     */
    @Test
    public void testCreate() {
        Account account = new DefaultAccount();
        Assert.assertNotNull(account);
    }

    /**
     * Test sunset sample file.
     *
     * @throws IOException Signals that an I/O exception has occurred.
     */
    @Test
    public void testSunsetSampleFile() throws IOException {
        initExpectRunningBalances(expectRunningBalances);

        String fileName = MnySampleFile.SUNSET_SAMPLE_PWD_MNY;
        int accountCount = 23;

        MnySampleFile mnySampleFile = MnySampleFile.getMnySampleFile(fileName);
        Assert.assertNotNull(mnySampleFile);

        MnyDb mnyDb = null;
        try {
            File file = new File(mnySampleFile.getFileName());
            String password = mnySampleFile.getPassword();
            mnyDb = new MnyDb(file, password, MnyDb.OPEN_MODE.READ_ONLY);
            Assert.assertNotNull(mnyDb);

            MnyContext mnyContext = MnyContextUtils.createMnyContext(mnyDb);
            TableAccountUtils.populateAccounts(mnyContext);
            testMnyDb(mnyDb, mnyContext, accountCount);
        } finally {
            if (mnyDb != null) {
                mnyDb.close();
            }
        }
    }

    private void initExpectRunningBalances(Map<String, BigDecimal> runningBalances) {

        // Charlie & May’s Joint Inv (Cash)
        initExpectRunningBalance("Charlie & May’s Joint Inv (Cash)", new BigDecimal(-10004.15), runningBalances);

        // Charlie's 401(k) (Contributions)
        initExpectRunningBalance("Charlie's 401(k) (Contributions)", new BigDecimal(13192.68), runningBalances);

        // Commodities (Cash)
        initExpectRunningBalance("Commodities (Cash)", new BigDecimal(255.00), runningBalances);

        // ETF Brokerage Account (Cash)
        initExpectRunningBalance("ETF Brokerage Account (Cash)", new BigDecimal(2310.00), runningBalances);

        // Escrow Account
        initExpectRunningBalance("Escrow Account", new BigDecimal(28100.00), runningBalances);

        // Home Loan
        initExpectRunningBalance("Home Loan", new BigDecimal(-149122.08), runningBalances);

        // Previous card (No longer used)
        initExpectRunningBalance("Previous card (No longer used)", new BigDecimal(-984.25), runningBalances);

        // Primary Residence
        initExpectRunningBalance("Primary Residence", new BigDecimal(355000.00), runningBalances);

        // WoodGrove Finance Stock Options (Cash)
        initExpectRunningBalance("WoodGrove Finance Stock Options (Cash)", new BigDecimal(0.00), runningBalances);

        // Woodgrove Bank Checking
        initExpectRunningBalance("Woodgrove Bank Checking", new BigDecimal(22871.06), runningBalances);

        // Woodgrove Bank Credit Card
        initExpectRunningBalance("Woodgrove Bank Credit Card", new BigDecimal(19305.74), runningBalances);

        // "Woodgrove Bank Savings";
        initExpectRunningBalance("Woodgrove Bank Savings", new BigDecimal(22946.30), runningBalances);

        // "Woodgrove Bond Account (Cash)";
        initExpectRunningBalance("Woodgrove Bond Account (Cash)", new BigDecimal(-57916.20), runningBalances);

        // "Woodgrove Investments (Cash)";
        initExpectRunningBalance("Woodgrove Investments (Cash)", new BigDecimal(-1594.62), runningBalances);

        // Woodgrove Platinum Card
        initExpectRunningBalance("Woodgrove Platinum Card", new BigDecimal(-836.00), runningBalances);
    }

    private void initExpectRunningBalance(String key, BigDecimal value, Map<String, BigDecimal> runningBalances) {
//        key = "Charlie & May’s Joint Inv (Cash)";
//        value = new BigDecimal(-10004.15);
        runningBalances.put(key, value);
    }

    /**
     * Test opened db.
     *
     * @param mnyDb        the opened db
     * @param context      the context
     * @param accountCount the account count
     * @throws IOException Signals that an I/O exception has occurred.
     */
    private void testMnyDb(MnyDb mnyDb, MnyContext context, int accountCount) throws IOException {
        Assert.assertNotNull(mnyDb);

        Database db = mnyDb.getDb();
        Assert.assertNotNull(db);

        Assert.assertNotNull(context);
        Assert.assertNotNull(context.getCategories());
        testCategories(context.getCategories());

        Map<Integer, Account> accounts = context.getAccounts();
        Assert.assertNotNull(accounts);
        Assert.assertEquals(accountCount, accounts.size());

        Set<Integer> testedIds = new HashSet<Integer>();

        Collection<Account> collection = accounts.values();

        boolean sort = true;
        if (sort) {
            List<Account> sortedAccounts = new ArrayList<>();
            sortedAccounts.addAll(collection);
            Comparator<Account> comparator = new Comparator<Account>() {
                @Override
                public int compare(Account o1, Account o2) {
                    return o1.getName().compareTo(o2.getName());
                }
            };
            Collections.sort(sortedAccounts, comparator);
            collection = sortedAccounts;
        }
        for (Account account : collection) {
            testAccount(account, db, context, testedIds);
        }
    }

    private void testCategories(Map<Integer, Category> categories) {
        for (Category category : categories.values()) {
            LOGGER.info("id={}, expense={}, name={}, fullName={}", category.getId(), category.getExpense(),
                    category.getName(), category.getFullName());
        }
    }

    /**
     * Test account.
     *
     * @param account   the account
     * @param db        the db
     * @param context   the context
     * @param testedIds the tested ids
     * @throws IOException Signals that an I/O exception has occurred.
     */
    private void testAccount(Account account, Database db, MnyContext context, Set<Integer> testedIds)
            throws IOException {
        LOGGER.info("account.name={}", account.getName());
        Integer id = account.getId();

        // make sure id is good and we are not looping
        Assert.assertTrue(id >= 0);
        Assert.assertFalse(testedIds.contains(id));
        testedIds.add(id);

        List<Transaction> transactions = account.getTransactions();
        for (Transaction transaction : transactions) {
            testTransaction(transaction, account, context);
        }

        if (account.getAccountType() != EnumAccountType.INVESTMENT) {
            testBalance(account);
        }
    }

    private void testBalance(Account account) {
        BigDecimal runningBalance = null;

        List<Transaction> transactions = account.getTransactions();
        for (Transaction transaction : transactions) {
            runningBalance = transaction.getRunningBalance();
//            if (runningBalance == null) {
//                LOGGER.error("account={}", account.getName());
//            }
            Assert.assertNotNull(runningBalance);
        }
        // in case, there are no transactions
        if (runningBalance == null) {
            runningBalance = account.getStartingBalance();
        }
        if (runningBalance == null) {
            runningBalance = new BigDecimal(0.00);
        }

        String accountName = account.getName();
        CalculateMonthlySummary calculateMonthlySummary = new CalculateMonthlySummary(account);
        LOGGER.info("startDate={}, endDate={}", calculateMonthlySummary.getStartDate(),
                calculateMonthlySummary.getEndDate());

        BigDecimal expectRunningBalance = expectRunningBalances.get(accountName);
        Assert.assertNotNull("account.name=" + accountName, expectRunningBalance);
        Assert.assertEquals("account.name=" + accountName,
//                expectRunningBalance.setScale(2, BigDecimal.ROUND_HALF_EVEN),
                BalanceUtils.formatBalance(expectRunningBalance),
//                runningBalance.setScale(2, BigDecimal.ROUND_HALF_EVEN)
                BalanceUtils.formatBalance(runningBalance));
    }

    /**
     * Test transaction.
     *
     * @param transaction the transaction
     * @param account     the account
     * @param context     the context
     * @throws IOException Signals that an I/O exception has occurred.
     */
    private void testTransaction(Transaction transaction, Account account, MnyContext context) throws IOException {
        Assert.assertEquals(account.getId(), transaction.getAccount().getId());

        if (transaction.isTransfer()) {
            testTransfer(context, transaction);
        }

        if (transaction.hasSplits()) {
            testSplits(transaction, account, context);
        }

        if (transaction.getTransactionInfo().isTransferTo()) {
            // will deal with this later
        }

        if (transaction.isInvestment()) {
            testInvestment(transaction);
        }

        Integer categoryId = transaction.getCategory().getId();
        if (categoryId == null) {
            // OK, we can have transaction with no category
            if (LOGGER.isDebugEnabled()) {
                try {
                    LOGGER.debug("categoryId == null, " + JSONUtils.serialize(transaction));
                } catch (IOException e) {
                    LOGGER.warn(e);
                }
            }
        } else {
            Category category = TableCategoryUtils.getCategory(categoryId, context);
            Assert.assertNotNull(category);
            String categoryName = TableCategoryUtils.getCategoryFullName(transaction, context);
//            LOGGER.info("transaction={}" + JSONUtils.serialize(transaction));
            if (category.getLevel() > 0) {
                Assert.assertNotNull(categoryName);
            }
        }

    }

    private void testSplits(Transaction transaction, Account account, MnyContext context) throws IOException {
        List<TransactionSplit> splits = transaction.getSplits();
        Assert.assertNotNull(splits);
        Assert.assertTrue(splits.size() > 0);

        Set<Integer> seenSplitIds = new HashSet<Integer>();
        testSplitForLoop(transaction, splits, seenSplitIds);

        for (TransactionSplit split : splits) {
            Integer parentId = split.getParentId();
            Assert.assertNotNull(parentId);

            // check parentId to make sure split's id is that of transaction
            Assert.assertEquals(transaction.getId(), parentId);

            Transaction splitTxn = split.getTransaction();
            Assert.assertEquals(splitTxn.getId(), split.getId());

            Row row = TableTransactionUtils.findRowByPrimaryKey(context.getDb(), splitTxn.getId());
            Assert.assertNotNull(row);
//            Transaction splitTxn = TableTransactionUtils.createTransaction(context.getDb(), row);
            // XXX - watch out for infinite loop
            testTransaction(splitTxn, account, context);
        }

    }

    /**
     * Test split for loop.
     *
     * @param transaction  the transaction
     * @param seenSplitIds the seen split ids
     */
    private void testSplitForLoop(Transaction transaction, List<TransactionSplit> splits, Set<Integer> seenSplitIds) {
//        List<TransactionSplit> splits = transaction.getSplits();
//        Assert.assertNotNull(splits);
//        Assert.assertTrue(splits.size() > 0);

        for (TransactionSplit split : splits) {
            Transaction splitTxn = split.getTransaction();
            Assert.assertNotNull(splitTxn);
            Assert.assertFalse(seenSplitIds.contains(splitTxn.getId()));
            seenSplitIds.add(splitTxn.getId());
            if (splitTxn.hasSplits()) {
                testSplitForLoop(splitTxn, splits, seenSplitIds);
            }
        }
    }

    private void testInvestment(Transaction transaction) {
        // TODO Auto-generated method stub

    }

    /**
     * Test transfer.
     *
     * @param context     the context
     * @param transaction the transaction
     * @throws IOException Signals that an I/O exception has occurred.
     */
    private void testTransfer(MnyContext context, Transaction transaction) throws IOException {
        // MUST have xferinfo
        XferInfo xferInfo = transaction.getXferInfo();
        Assert.assertNotNull(xferInfo);

        // MUST have a valid transferredAccountId
        Integer transferredAccountId = xferInfo.getXferAccountId();
        Assert.assertNotNull(transferredAccountId);
        Assert.assertTrue(transferredAccountId >= 0);

        // MUST have a valid transferredAccount
        Account transferredAccount = context.getAccounts().get(transferredAccountId);
        Assert.assertNotNull(transferredAccount);

        // MUST have a valid xferTransactionId
        Integer xferTransactionId = xferInfo.getXferTransactionId();
        if (xferTransactionId == null) {
            LOGGER.error("transaction.id=" + transaction.getId());
            LOGGER.info(JSONUtils.writeValueAsString(transaction));
            LOGGER.info("XXX, " + transaction);
            LOGGER.info("XXX, " + transaction.getXferInfo());
        }
        Assert.assertNotNull("transaction.id=" + transaction.getId(), xferTransactionId);
        Assert.assertTrue(xferTransactionId >= 0);

        // Now double-check by manually lookup
        Database db = context.getDb();
        Integer xferId = TableTransationXferUtils.getXferId(db, transaction);
        Assert.assertNotNull(xferId);
        Assert.assertTrue(xferId >= 0);

        Assert.assertEquals(xferTransactionId, xferId);

        // lookup xferTransaction and make sure that amount will negate each
        // other out
        Row row = TableTransactionUtils.findRowByPrimaryKey(context.getDb(), xferId);
        Assert.assertNotNull(row);
        Transaction xferTransaction = TableTransactionUtils.createTransaction(context.getDb(), row);
        BigDecimal amount = transaction.getAmount();
        BigDecimal xferAmount = xferTransaction.getAmount();
        Assert.assertEquals(amount.doubleValue(), -xferAmount.doubleValue(), 0.00);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("amount={}, xferAmount={}", amount, xferAmount);
        }
    }
}

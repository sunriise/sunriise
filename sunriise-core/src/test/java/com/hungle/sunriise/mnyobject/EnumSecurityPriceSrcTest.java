package com.hungle.sunriise.mnyobject;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.Logger;
import org.junit.Assert;
import org.junit.Test;

public class EnumSecurityPriceSrcTest {
    private static final Logger LOGGER = org.apache.logging.log4j.LogManager.getLogger(EnumSecurityPriceSrcTest.class);

    @Test
    public void test01() {
        List<String> names = new ArrayList<String>();

        for (EnumSecurityPriceSrc src : EnumSecurityPriceSrc.values()) {
            Assert.assertNotNull(src);
            names.add(src.toString());
        }

        for (String name : names) {
            LOGGER.info(name);
            EnumSecurityPriceSrc src = EnumSecurityPriceSrc.valueOf(name);
            Assert.assertNotNull(src);
        }
    }
}

package com.hungle.sunriise.mnyobject;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.Logger;
import org.junit.Assert;
import org.junit.Test;

import com.healthmarketscience.jackcess.Database;
import com.hungle.sunriise.dbutil.TableAccountUtils;
import com.hungle.sunriise.dbutil.TableBillUtils;
import com.hungle.sunriise.io.MnyDb;
import com.hungle.sunriise.io.sample.MnySampleFile;
import com.hungle.sunriise.json.JSONUtils;
import com.hungle.sunriise.util.MnyContextUtils;

// TODO: Auto-generated Javadoc
/**
 * The Class BillTest.
 */
public class BillTest {

    /** The Constant LOGGER. */
    private static final Logger LOGGER = org.apache.logging.log4j.LogManager.getLogger(BillTest.class);

    /** The Constant DEFAULT_COMPLEX_FILENAME. */
    private static final String DEFAULT_COMPLEX_FILENAME = MnySampleFile.LOCAL_LARGE_FILE_MNY;

    /**
     * Test bill.
     *
     * @throws IOException Signals that an I/O exception has occurred.
     */
    @Test
    public void testBill() throws IOException {
        MnyDb mnyDb = MnySampleFile.openDbReadOnly(MnySampleFile.SUNSET_BILL_MNY);
        Assert.assertNotNull(mnyDb);

        Database db = mnyDb.getDb();
        Assert.assertNotNull(db);

        Map<Integer, Bill> bills = TableBillUtils.getBills(db);
        Assert.assertNotNull(bills);

        Assert.assertEquals(3, bills.size());

        MnyContext mnyContext = MnyContextUtils.createMnyContext(mnyDb);

        Map<Integer, Account> accounts = mnyContext.getAccounts();
        Assert.assertNotNull(accounts);
        Assert.assertEquals(2, accounts.values().size());

        Account account = null;
        List<Transaction> transactions = null;

        // Investments to Watch
        account = accounts.get(1);
        Assert.assertNotNull(account);
        Assert.assertEquals("Investments to Watch", account.getName());
        TableAccountUtils.addTransactionsToAccount(account, mnyContext);
        transactions = account.getTransactions();
        Assert.assertNotNull(transactions);
        Assert.assertEquals(3, transactions.size());
        Assert.assertEquals(0, account.getFilteredTransactions().size());

        // Checking #1
        account = accounts.get(2);
        Assert.assertNotNull(account);
        Assert.assertEquals("Checking #1", account.getName());
        TableAccountUtils.addTransactionsToAccount(account, mnyContext);
        transactions = account.getTransactions();
        Assert.assertNotNull(transactions);
        Assert.assertEquals(2, transactions.size());
        Assert.assertEquals(3, account.getFilteredTransactions().size());

        for (Bill bill : bills.values()) {
            Assert.assertNotNull(bill);
            Integer transactionId = bill.getTransactionId();
            Assert.assertNotNull(transactionId);

            Transaction transaction = mnyContext.getTransaction(transactionId);
            Assert.assertNotNull("transactionId=" + transactionId, transaction);
            Assert.assertTrue("transactionId=" + transactionId, transaction.isRecurring());

            if (LOGGER.isDebugEnabled()) {
                LOGGER.debug(JSONUtils.serialize(bill));
                LOGGER.debug(JSONUtils.serialize(transaction));
            }
        }
    }

    /**
     * Check dangling bill.
     *
     * @throws IOException Signals that an I/O exception has occurred.
     */
    @Test
    public void checkDanglingBill() throws IOException {
        String fileName = DEFAULT_COMPLEX_FILENAME;
        if (fileName == null) {
            return;
        }

        File dbFile = new File(fileName);
        if (!dbFile.exists()) {
            return;
        }

        String password = null;
        MnyDb mnyDb = null;
        try {
            mnyDb = new MnyDb(dbFile, password);
            Assert.assertNotNull(mnyDb);

            Database db = mnyDb.getDb();
            Assert.assertNotNull(db);

            MnyContext mnyContext = MnyContextUtils.createMnyContext(mnyDb);

            Map<Integer, Account> accounts = mnyContext.getAccounts();
            Assert.assertNotNull(accounts);
            for (Account account : accounts.values()) {
                Assert.assertNotNull(account);
                TableAccountUtils.addTransactionsToAccount(account, mnyContext);
            }

            Map<Integer, Bill> bills = TableBillUtils.getBills(db);
            Assert.assertNotNull(bills);
            for (Bill bill : bills.values()) {
                Assert.assertNotNull(bill);
                Integer transactionId = bill.getTransactionId();
                Assert.assertNotNull(transactionId);
                Transaction transaction = mnyContext.getTransaction(transactionId);
                Assert.assertNotNull("transactionId=" + transactionId, transaction);
                Assert.assertTrue("transactionId=" + transactionId, transaction.isRecurring());

                transaction = mnyContext.getTransaction(transactionId);
                Assert.assertNotNull("transactionId=" + transactionId, transaction);
                Assert.assertTrue("transactionId=" + transactionId, transaction.isRecurring());

                // log.info(JSONUtils.valueToString(bill));
            }
        } finally {
            if (mnyDb != null) {
                mnyDb.close();
            }
        }
    }
}

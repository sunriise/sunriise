package com.hungle.sunriise.fx;

import java.io.IOException;

import org.apache.logging.log4j.Logger;
import org.junit.Test;

import com.healthmarketscience.jackcess.Database;
import com.hungle.sunriise.io.DefaultMnyDbTest;
import com.hungle.sunriise.io.sample.MnySampleFile;

public class UpdateExchangeRatesTest extends DefaultMnyDbTest {
    private static final Logger LOGGER = org.apache.logging.log4j.LogManager.getLogger(UpdateExchangeRatesTest.class);

    @Override
    public MnySampleFile getMnySampleFile() {
        boolean useLargeFileIfAvailable = true;
        boolean makeCopy = true;
        return getMnySampleFile(super.getMnySampleFile(), useLargeFileIfAvailable, makeCopy);
    }

    @Test
    public void test2() throws IOException {
        Database db = getMnyDb().getDb();
        UpdateExchangeRates updateExchangeRates = new UpdateExchangeRates(db);
        updateExchangeRates.setDryrun(true);
        for (FxRateInfo fxRateInfo : updateExchangeRates.getFxRateInfo()) {
            System.out.println(
                    String.format("%s%s=X", fxRateInfo.getFrom().getIsoCode(), fxRateInfo.getTo().getIsoCode()));
        }
    }

    @Test
    public void testUpdateExchangeRates() throws IOException {
        Database db = getMnyDb().getDb();
        FxRates fxRates = new FxRates() {

            @Override
            public Double getRateByIsoCode(String from, String to) {
                return 1.00;
            }
        };
        UpdateExchangeRates updateExchangeRates = new UpdateExchangeRates(db);
        updateExchangeRates.setDryrun(true);
        updateExchangeRates.update(fxRates);
    }
}

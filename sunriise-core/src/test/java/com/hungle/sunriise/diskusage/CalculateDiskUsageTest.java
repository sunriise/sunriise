package com.hungle.sunriise.diskusage;

import java.io.File;
import java.io.IOException;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;

import com.hungle.sunriise.io.sample.MnySampleFile;
import com.hungle.sunriise.io.sample.MnySampleFileFactory;

public class CalculateDiskUsageTest {
    @Test
    public void testCalculate() throws IOException {
        CalculateDiskUsage cmd = new CalculateDiskUsage();

        MnySampleFile sampleFile = MnySampleFileFactory.getSunsetSampleFile();
        Assert.assertNotNull(sampleFile);

        File file = new File(sampleFile.getFileName());
        Assert.assertNotNull(file);
        Assert.assertTrue(file.exists());

        String password = sampleFile.getPassword();

        DefaultDiskUsageCollector collector = new DefaultDiskUsageCollector();
        cmd.calculate(file, password, collector);

        File dbFile = collector.getDbFile();
        Assert.assertEquals(7864320, dbFile.length());

        Assert.assertEquals(7405568, collector.getRunningBytes());

        Assert.assertEquals(88, collector.getTableCount());

        List<DefaultDiskUsageCollector.TableStat> tableStats = collector.getTableStats();
        int byteCounts = 0;
        Assert.assertEquals(88, tableStats.size());
        for (DefaultDiskUsageCollector.TableStat tableStat : tableStats) {
            byteCounts += tableStat.getTableByteCount();
            System.out.println(tableStat.toCsvString());
        }
        Assert.assertEquals(collector.getRunningBytes(), byteCounts);
    }
}

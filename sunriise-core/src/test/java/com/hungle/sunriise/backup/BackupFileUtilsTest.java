package com.hungle.sunriise.backup;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.Logger;
import org.junit.Assert;
import org.junit.Test;

import com.hungle.sunriise.dbutil.TableAccountUtils;
import com.hungle.sunriise.dbutil.TableTransactionUtils;
import com.hungle.sunriise.io.MnyDb;
import com.hungle.sunriise.io.sample.MnySampleFile;
import com.hungle.sunriise.mnyobject.Account;
import com.hungle.sunriise.mnyobject.MnyContext;
import com.hungle.sunriise.mnyobject.Transaction;
import com.hungle.sunriise.util.MnyContextUtils;

public class BackupFileUtilsTest {
    private static final Logger LOGGER = org.apache.logging.log4j.LogManager.getLogger(BackupFileUtilsTest.class);

    @Test
    public void testReadFromBackup() throws IOException {
        String fileName = MnySampleFile.SRC_TEST_DATA_MNY + MnySampleFile.SUNSET_SAMPLE_MBF;

        File sampleFile = MnySampleFile.getSampleFileFromModuleProject(fileName);
        Assert.assertNotNull(sampleFile);

        LOGGER.info("sampleFile=" + sampleFile);
        Assert.assertTrue(sampleFile.exists());

        File cabFile = sampleFile;
        boolean deleteOnExit = false;
        long maxByteCount = -1;
        File dbFile = BackupFileUtils.createBackupAsTempFile(cabFile, deleteOnExit, maxByteCount);
        LOGGER.info("dbFile=" + dbFile);

        Assert.assertNotNull(dbFile);
        Assert.assertTrue(dbFile.exists());

        try (MnyDb mnyDb = new MnyDb(dbFile, null)) {
            MnyContext mnyContext = MnyContextUtils.createMnyContext(mnyDb);
            Assert.assertNotNull(mnyContext);

            Map<Integer, Account> accounts = mnyContext.getAccounts();
            Assert.assertNotNull(accounts);
            Assert.assertEquals(30, accounts.size());

            boolean found = false;
            List<Account> list = TableAccountUtils.toAccountsList(accounts);
            for (Account account : list) {
                if (account.getName().compareToIgnoreCase("Woodgrove Bank Checking") == 0) {
                    found = true;
                }
            }
            Assert.assertTrue(found);

            Map<Integer, Transaction> transactions = TableTransactionUtils.getTransactions(mnyDb.getDb());
            Assert.assertNotNull(transactions);
            Assert.assertEquals(4638, transactions.size());
        }

    }
}

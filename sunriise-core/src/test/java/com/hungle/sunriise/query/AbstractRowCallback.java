package com.hungle.sunriise.query;

import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.Map;

import com.healthmarketscience.jackcess.Database;
import com.healthmarketscience.jackcess.Row;

// TODO: Auto-generated Javadoc
/**
 * The Class AbstractRowCallback.
 *
 * @param <T> the generic type
 */
public abstract class AbstractRowCallback<T> implements RowCallback {

    /** The collections. */
    private final Map<Integer, T> collections;

    /** The db. */
    private final Database db;

    /**
     * Instantiates a new abstract row callback.
     *
     * @param db the db
     */
    public AbstractRowCallback(Database db) {
        this.db = db;
        this.collections = new LinkedHashMap<>();
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.hungle.sunriise.query.RowCallback#collect(com.healthmarketscience.
     * jackcess.Row)
     */
    @Override
    public void collect(Row row) throws IOException {
        T type = createType(row);
        Integer key = getKey(type);
        collections.put(key, type);
    }

    /**
     * Gets the key.
     *
     * @param type the type
     * @return the key
     */
    public abstract Integer getKey(T type);

    /**
     * Creates the type.
     *
     * @param row the row
     * @return the t
     * @throws IOException Signals that an I/O exception has occurred.
     */
    public abstract T createType(Row row) throws IOException;

    /**
     * Gets the collections.
     *
     * @return the collections
     */
    public Map<Integer, T> getCollections() {
        return collections;
    }

    /**
     * Gets the db.
     *
     * @return the db
     */
    public Database getDb() {
        return db;
    }
}
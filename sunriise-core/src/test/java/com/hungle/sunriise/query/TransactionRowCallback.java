package com.hungle.sunriise.query;

import java.io.IOException;

import com.healthmarketscience.jackcess.Database;
import com.healthmarketscience.jackcess.Row;
import com.hungle.sunriise.dbutil.TableTransactionUtils;
import com.hungle.sunriise.mnyobject.Transaction;

// TODO: Auto-generated Javadoc
/**
 * The Class TransactionRowCallback.
 */
final class TransactionRowCallback extends AbstractRowCallback<Transaction> {

    /**
     * Instantiates a new transaction row callback.
     *
     * @param db the db
     */
    TransactionRowCallback(Database db) {
        super(db);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.hungle.sunriise.query.AbstractRowCallback#createType(com.
     * healthmarketscience.jackcess.Row)
     */
    @Override
    public Transaction createType(Row row) throws IOException {
        return TableTransactionUtils.createTransaction(getDb(), row);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.hungle.sunriise.query.AbstractRowCallback#getKey(java.lang.Object)
     */
    @Override
    public Integer getKey(Transaction transaction) {
        return transaction.getId();
    }
}
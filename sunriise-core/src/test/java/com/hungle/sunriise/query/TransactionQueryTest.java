package com.hungle.sunriise.query;

import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.apache.logging.log4j.Logger;
import org.junit.Assert;
import org.junit.Test;

import com.healthmarketscience.jackcess.Database;
import com.hungle.sunriise.io.MnyDb;
import com.hungle.sunriise.io.sample.MnyFileAction;
import com.hungle.sunriise.io.sample.MnySampleFile;
import com.hungle.sunriise.mnyobject.Account;
import com.hungle.sunriise.mnyobject.Transaction;
import com.hungle.sunriise.mnyobject.impl.DefaultAccount;

// TODO: Auto-generated Javadoc
/**
 * The Class TransactionQueryTest.
 */
public class TransactionQueryTest {

    /** The Constant LOGGER. */
    private static final Logger LOGGER = org.apache.logging.log4j.LogManager.getLogger(TransactionQueryTest.class);

    /**
     * The Class Expected.
     */
    private class Expected {

        /** The tx count. */
        final private int txCount;

        /** The tx no account count. */
        final private int txNoAccountCount;

        /** The account count. */
        final private int accountCount;

        /**
         * Instantiates a new expected.
         *
         * @param txCount          the tx count
         * @param txNoAccountCount the tx no account count
         * @param accountCount     the account count
         */
        public Expected(int txCount, int txNoAccountCount, int accountCount) {
            super();
            this.txCount = txCount;
            this.txNoAccountCount = txNoAccountCount;
            this.accountCount = accountCount;
        }

        /**
         * Gets the tx count.
         *
         * @return the tx count
         */
        public int getTxCount() {
            return txCount;
        }

        /**
         * Gets the tx no account count.
         *
         * @return the tx no account count
         */
        public int getTxNoAccountCount() {
            return txNoAccountCount;
        }

        /**
         * Gets the account count.
         *
         * @return the account count
         */
        public int getAccountCount() {
            return accountCount;
        }
    }

    /**
     * Test transactions.
     *
     * @throws IOException Signals that an I/O exception has occurred.
     */
    @Test
    public void testTransactions() throws IOException {
        Map<String, Expected> expectedMap = new HashMap<>();
        expectedMap.put(MnySampleFile.MONEY2001_PWD_MNY, new Expected(3, 0, 1));
        expectedMap.put(MnySampleFile.MONEY2002_MNY, new Expected(60, 0, 1));
        expectedMap.put(MnySampleFile.MONEY2004_PWD_MNY, new Expected(0, 0, 0));
        expectedMap.put(MnySampleFile.MONEY2005_PWD_MNY, new Expected(115, 0, 5));
        expectedMap.put(MnySampleFile.MONEY2008_PWD_MNY, new Expected(3, 0, 1));
        expectedMap.put(MnySampleFile.SUNSET_401K_MNY, new Expected(3, 0, 1));
        expectedMap.put(MnySampleFile.SUNSET_SAMPLE_PWD_5_MNY, new Expected(4629, 4, 19));
        expectedMap.put(MnySampleFile.SUNSET_SAMPLE_PWD_6_MNY, new Expected(4645, 4, 19));
        expectedMap.put(MnySampleFile.SUNSET_SAMPLE_PWD_MNY, new Expected(4629, 4, 19));
        expectedMap.put(MnySampleFile.SUNSET01_MNY, new Expected(3, 0, 1));
        expectedMap.put(MnySampleFile.SUNSET02_MNY, new Expected(3, 0, 1));
        expectedMap.put(MnySampleFile.SUNSET03_XFER_MNY, new Expected(11, 0, 3));
        expectedMap.put(MnySampleFile.SUNSET_BILL_MNY, new Expected(8, 0, 2));

        MnyFileAction action = new MnyFileAction() {

            @Override
            public void run(MnyDb mnyDb) throws IOException {
                Database db = mnyDb.getDb();
                Expected expected = expectedMap.get(mnyDb.getDbFile().getName());
                if (expected == null) {
                    LOGGER.warn("Cannot find expected entry for name=" + mnyDb.getDbFile().getName());
                } else {
                    testTransactions(expected, db);
                }
            }
        };
        MnySampleFile.forEachSampleFile(action);

    }

    /**
     * Test transactions.
     *
     * @param expected the expected
     * @param db       the db
     * @throws IOException Signals that an I/O exception has occurred.
     */
    private void testTransactions(Expected expected, Database db) throws IOException {
        AbstractRowCallback<Transaction> callback = null;
        Map<Integer, Transaction> transactions = null;

        callback = new TransactionRowCallback(db);
        TransactionQuery.findTransactions(db, callback);
        transactions = callback.getCollections();
        Assert.assertEquals("Invalid getTxCount", expected.getTxCount(), transactions.size());

        Set<Transaction> noAccount = new HashSet<>();
        Set<Integer> accountIds = new HashSet<>();
        for (Transaction transaction : transactions.values()) {
            Integer accountId = transaction.getAccount().getId();
            if (accountId == null) {
                noAccount.add(transaction);
            } else {
                accountIds.add(transaction.getAccount().getId());
            }
        }

        Assert.assertEquals("Invalid getTxNoAccountCount", expected.getTxNoAccountCount(), noAccount.size());
        Assert.assertEquals("Invalid getAccountCount", expected.getAccountCount(), accountIds.size());

        int count = noAccount.size();
        for (Integer accountId : accountIds) {
            Account account = new DefaultAccount();
            account.setId(accountId);

            callback = new TransactionRowCallback(db);
            TransactionQuery.findTransactionsForAccount(db, account, callback);
            transactions = callback.getCollections();
            count += transactions.size();
            LOGGER.info("account.id=" + account.getId() + ", transactions.size=" + transactions.size());
        }
        Assert.assertEquals(expected.getTxCount(), count);
    }
}

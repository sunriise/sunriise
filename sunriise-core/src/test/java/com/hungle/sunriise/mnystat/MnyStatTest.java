package com.hungle.sunriise.mnystat;

import java.io.File;
import java.io.IOException;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;

import com.hungle.sunriise.io.MnyDb;
import com.hungle.sunriise.io.sample.MnySampleFile;

// TODO: Auto-generated Javadoc
/**
 * The Class MnyStatTest.
 */
public class MnyStatTest {

    /**
     * Test sunset sample.
     *
     * @throws IOException Signals that an I/O exception has occurred.
     */
    @Test
    public void testSunsetSample() throws IOException {
        MnySampleFile mnyTestFile = MnySampleFile.getMnySampleFile(MnySampleFile.SUNSET_SAMPLE_PWD_MNY);
        Assert.assertNotNull(mnyTestFile);

        MnyDb mnyDb = null;
        try {
            mnyDb = MnySampleFile.openSampleFileFromModuleProject(mnyTestFile);
            Assert.assertNotNull(mnyDb);

            createMnyStat(mnyDb);
        } finally {
            if (mnyDb != null) {
                mnyDb.close();
                mnyDb = null;
            }
        }
    }

    /**
     * Test large file.
     *
     * @throws IOException Signals that an I/O exception has occurred.
     */
    @Test
    public void testLargeFile() throws IOException {

        MnyDb mnyDb = null;
        try {
            File file = new File(MnySampleFile.LOCAL_LARGE_FILE_MNY);
            if (!file.exists()) {
                return;
            }
            String password = null;
            mnyDb = new MnyDb(file, password);
            Assert.assertNotNull(mnyDb);

            MnyStat mnyStat = createMnyStat(mnyDb);
            Assert.assertNotNull(mnyStat);

            List<TableStat> tableStats = null;

            tableStats = mnyStat.getTableStats();
            Assert.assertNotNull(tableStats);
            printStats(tableStats);

            tableStats = mnyStat.getSystemTableStats();
            Assert.assertNotNull(tableStats);
            printStats(tableStats);

        } finally {
            if (mnyDb != null) {
                mnyDb.close();
                mnyDb = null;
            }
        }
    }

    private void printStats(List<TableStat> tableStats) {
        System.out.println("tableStats.size=" + tableStats.size());

        System.out.println("tableName,bytes,rows,indexes");

        for (TableStat tableStat : tableStats) {
            StringBuilder sb = new StringBuilder();
            sb.append(tableStat.getTableName());

            sb.append(",");
            sb.append(tableStat.getBytes());

            sb.append(",");
            sb.append(tableStat.getRowCount());

            sb.append(",");
            sb.append(tableStat.getIndexCount());

            System.out.println(sb.toString());
        }
    }

    /**
     * Creates the mny stat.
     *
     * @param mnyDb the mny db
     * @return
     * @throws IOException Signals that an I/O exception has occurred.
     */
    private MnyStat createMnyStat(MnyDb mnyDb) throws IOException {
        MnyStat mnyStat = new MnyStat(mnyDb);
        return mnyStat;
    }
}

package com.hungle.sunriise.io;

// TODO: Auto-generated Javadoc
/**
 * The Class ExpectedSize.
 */
class ExpectedSize {

    /** The name. */
    private String name;

    /** The bytes count. */
    private int bytesCount;

    /**
     * Instantiates a new expected size.
     *
     * @param name       the name
     * @param bytesCount the bytes count
     */
    public ExpectedSize(String name, int bytesCount) {
        super();
        this.name = name;
        this.bytesCount = bytesCount;
    }

    /**
     * Gets the name.
     *
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the name.
     *
     * @param name the new name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Gets the bytes count.
     *
     * @return the bytes count
     */
    public int getBytesCount() {
        return bytesCount;
    }

    /**
     * Sets the bytes count.
     *
     * @param bytesCount the new bytes count
     */
    public void setBytesCount(int bytesCount) {
        this.bytesCount = bytesCount;
    }
}
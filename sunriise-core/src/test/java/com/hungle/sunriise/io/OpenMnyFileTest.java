/*******************************************************************************
 * Copyright (c) 2010 Hung Le
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *******************************************************************************/

package com.hungle.sunriise.io;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.Logger;
import org.junit.Assert;
import org.junit.Test;

import com.healthmarketscience.jackcess.Database;
import com.hungle.sunriise.dbutil.TableAccountUtils;
import com.hungle.sunriise.dbutil.TableTransactionUtils;
import com.hungle.sunriise.diskusage.GetDiskUsageCmd;
import com.hungle.sunriise.io.sample.MnyFileAction;
import com.hungle.sunriise.io.sample.MnySampleFile;
import com.hungle.sunriise.mnyobject.Account;
import com.hungle.sunriise.mnyobject.MnyContext;
import com.hungle.sunriise.mnyobject.Transaction;
import com.hungle.sunriise.util.MnyContextUtils;

// TODO: Auto-generated Javadoc
/**
 * The Class OpenMnyFileTest.
 */
public class OpenMnyFileTest {

    /** The Constant log. */
    private static final Logger LOGGER = org.apache.logging.log4j.LogManager.getLogger(OpenMnyFileTest.class);

    /** The expected size map. */
    private Map<String, ExpectedSize> expectedSizeMap = new HashMap<String, ExpectedSize>();

    /**
     * Instantiates a new open mny file test.
     */
    public OpenMnyFileTest() {
        super();

        // "money2001-pwd.mny"
        addExpectedSize(MnySampleFile.MONEY2001_PWD_MNY, 1784021);
        // "money2002.mny"
        addExpectedSize(MnySampleFile.MONEY2002_MNY, 2346644);
        // "money2004-pwd.mny"
        addExpectedSize(MnySampleFile.MONEY2004_PWD_MNY, 1984722);
        // "money2005-pwd.mny"
        addExpectedSize(MnySampleFile.MONEY2005_PWD_MNY, 3813359);
        // "money2008-pwd.mny"
        addExpectedSize(MnySampleFile.MONEY2008_PWD_MNY, 3128956);
        // "sunset_401k.mny"
        addExpectedSize(MnySampleFile.SUNSET_401K_MNY, 3145448);
        // "sunset-sample-pwd-5.mny"
        addExpectedSize(MnySampleFile.SUNSET_SAMPLE_PWD_5_MNY, 7979655);
        // "sunset-sample-pwd-6.mny"
        addExpectedSize(MnySampleFile.SUNSET_SAMPLE_PWD_6_MNY, 8000211);
        // "sunset-sample-pwd.mny"
        addExpectedSize(MnySampleFile.SUNSET_SAMPLE_PWD_MNY, 7979673);
        addExpectedSize(MnySampleFile.SUNSET01_MNY, 3128894);
        addExpectedSize(MnySampleFile.SUNSET02_MNY, 3128894);
        addExpectedSize(MnySampleFile.SUNSET03_XFER_MNY, 3139594);
        addExpectedSize(MnySampleFile.SUNSET_BILL_MNY, 3184289);
    }

    /**
     * Adds the expected size.
     *
     * @param name       the name
     * @param bytesCount the bytes count
     */
    private void addExpectedSize(String name, int bytesCount) {
        ExpectedSize expectedSize;
        expectedSize = new ExpectedSize(name, bytesCount);
        expectedSizeMap.put(name, expectedSize);
    }

    /**
     * Read sample files.
     *
     * @throws IOException Signals that an I/O exception has occurred.
     */
    @Test
    public void readSampleFiles() throws IOException {
        MnyFileAction action = new MnyFileAction() {
            @Override
            public void run(MnyDb mnyDb) throws IOException {
                Assert.assertNotNull(mnyDb);

                readSampleFile(mnyDb);
            }
        };
        MnySampleFile.forEachSampleFile(action);
    }

    /**
     * Read sample file. Calculate disk space usage and compare against known
     * values.
     *
     * @param mnyDb the mny db
     * @throws IOException Signals that an I/O exception has occurred.
     */
    private void readSampleFile(MnyDb mnyDb) throws IOException {

        Database db = mnyDb.getDb();
        Assert.assertNotNull(db);

        long calculatedBytes = GetDiskUsageCmd.getAllTablesBytesCount(db);

        long fileSize = db.getFile().length();
        long delta = fileSize - calculatedBytes;
        String fileName = db.getFile().getName();
        LOGGER.info("db=" + fileName + ", calculatedBytes=" + GetDiskUsageCmd.humanReadableByteCount(calculatedBytes)
                + ", fileSize=" + GetDiskUsageCmd.humanReadableByteCount(fileSize) + ", delta="
                + GetDiskUsageCmd.humanReadableByteCount(delta));

        ExpectedSize expectedSize = expectedSizeMap.get(fileName);
        Assert.assertNotNull(fileName, expectedSize);
        Assert.assertEquals(fileName, expectedSize.getBytesCount(), calculatedBytes);
    }

    /**
     * Open sample files.
     *
     * @throws IOException Signals that an I/O exception has occurred.
     */
    @Test
    public void openSampleFiles() throws IOException {
        MnyFileAction action = new MnyFileAction() {
            @Override
            public void run(MnyDb mnyDb) {
                Assert.assertNotNull(mnyDb);
            }
        };
        MnySampleFile.forEachSampleFile(action);
    }

    /**
     * Test sunset.
     *
     * @throws IOException Signals that an I/O exception has occurred.
     */
    @Test
    public void testSunset() throws IOException {
        String fileName = null;

        fileName = MnySampleFile.SUNSET01_MNY;
        testSunset(fileName);

        fileName = MnySampleFile.SUNSET02_MNY;
        testSunset(fileName);
    }

    /**
     * Test sunset.
     *
     * @param fileName the file name
     * @throws IOException Signals that an I/O exception has occurred.
     */
    protected void testSunset(final String fileName) throws IOException {
        if (fileName == null) {
            return;
        }

        MnySampleFile testFile;
        String password;
        File dbFile;

        testFile = MnySampleFile.getMnySampleFile(fileName);
        Assert.assertNotNull(testFile);
        if (LOGGER.isDebugEnabled()) {
            String fn = testFile.getFileName();
            LOGGER.debug("fileName: " + fn);
        }
        dbFile = new File(testFile.getFileName());
        password = testFile.getPassword();
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("password: " + password);
        }
        testSunset(dbFile, password);
    }

    /**
     * Open db.
     *
     * @param dbFile   the db file
     * @param password the password
     * @throws IOException Signals that an I/O exception has occurred.
     */
    private void testSunset(File dbFile, String password) throws IOException {
        MnyDb mnyDb = null;

        try {
            mnyDb = new MnyDb(dbFile, password);
            Assert.assertNotNull(mnyDb);

            testSunset(mnyDb);
        } finally {
            if (mnyDb != null) {
                mnyDb.close();
            }
        }
    }

    /**
     * Todo.
     *
     * @param mnyDb the mny db
     * @throws IOException Signals that an I/O exception has occurred.
     */
    private void testSunset(MnyDb mnyDb) throws IOException {
        MnyContext mnyContext = MnyContextUtils.createMnyContext(mnyDb);
        Assert.assertNotNull(mnyContext);

        Map<Integer, Account> accounts = mnyContext.getAccounts();
        Assert.assertNotNull(accounts);
        Assert.assertEquals(1, accounts.size());

        List<Account> list = TableAccountUtils.toAccountsList(accounts);
        Account account = list.get(0);
        Assert.assertNotNull(account);
        Assert.assertEquals("Investments to Watch", account.getName());

        Map<Integer, Transaction> transactions = TableTransactionUtils.getTransactions(mnyDb.getDb());
        Assert.assertNotNull(transactions);
        Assert.assertEquals(3, transactions.size());
    }

}

package com.hungle.sunriise.io;

import java.io.File;
import java.io.IOException;
import java.util.TimeZone;

import org.junit.After;
import org.junit.Assume;
import org.junit.Before;

import com.hungle.sunriise.io.sample.MnySampleFile;

// TODO: Auto-generated Javadoc

public abstract class AbstractMnyDbTest {

    /** The opened db. */
    private MnyDb mnyDb = null;

    /**
     * Instantiates a new abstract opened db test.
     */
    public AbstractMnyDbTest() {
        super();
    }

    /**
     * Gets the mny test file.
     *
     * @return the mny test file
     */
    public abstract MnySampleFile getMnySampleFile();

    /**
     * Initialize.
     *
     * @throws IOException Signals that an I/O exception has occurred.
     */
    @Before
    public void initialize() throws IOException {
        MnySampleFile mnySampleFile = getMnySampleFile();

        File file = new File(mnySampleFile.getFileName());
        String password = mnySampleFile.getPassword();
        TimeZone timeZone = mnySampleFile.getTimeZone();

        if (timeZone == null) {
            setMnyDb(new MnyDb(file, password));
        } else {
            setMnyDb(new MnyDb(file, password, timeZone));
        }
        Assume.assumeTrue(getMnyDb() != null);
    }

    /**
     * Cleanup.
     *
     * @throws IOException Signals that an I/O exception has occurred.
     */
    @After
    public void cleanup() throws IOException {
        if (getMnyDb() != null) {
            getMnyDb().close();
        }
    }

    protected MnyDb getMnyDb() {
        return mnyDb;
    }

    protected void setMnyDb(MnyDb mnyDb) {
        this.mnyDb = mnyDb;
    }

}
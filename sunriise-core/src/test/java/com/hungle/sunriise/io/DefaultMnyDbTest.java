package com.hungle.sunriise.io;

import java.io.File;
import java.io.IOException;
import java.util.TimeZone;

import org.apache.logging.log4j.Logger;

import com.hungle.sunriise.io.sample.MnySampleFile;

// TODO: Auto-generated Javadoc

public class DefaultMnyDbTest extends AbstractMnyDbTest {
    private static final Logger LOGGER = org.apache.logging.log4j.LogManager.getLogger(DefaultMnyDbTest.class);

    @Override
    public MnySampleFile getMnySampleFile() {
        MnySampleFile sampleFile = MnySampleFile.getMnySampleFile(getMnyTestFileName());
        String tzId = "America/Los_Angeles";
        TimeZone timeZone = TimeZone.getTimeZone(tzId);
        sampleFile.setTimeZone(timeZone);
        return sampleFile;
    }

    protected String getMnyTestFileName() {
        return MnySampleFile.SUNSET_SAMPLE_PWD_MNY;
    }

    protected MnySampleFile getMnySampleFile(MnySampleFile mnyTestFile, boolean useLargeFileIfAvailable) {
        return getMnySampleFile(mnyTestFile, useLargeFileIfAvailable, false);
    }

    protected MnySampleFile getMnySampleFile(MnySampleFile mnyTestFile, boolean useLargeFileIfAvailable,
            boolean makeCopy) {
        File file = new File(MnySampleFile.LOCAL_LARGE_FILE_MNY);
        if ((useLargeFileIfAvailable) && (file.exists())) {
            mnyTestFile = new MnySampleFile(file.getAbsolutePath());
        }

        if (makeCopy) {
            try {
                mnyTestFile = FileUtils.makeCopy(mnyTestFile);
            } catch (IOException e) {
                LOGGER.error(e, e);
            }
        }

        return mnyTestFile;
    }

}

package com.hungle.sunriise.dbutil;

import java.io.IOException;
import java.util.Map;

import org.junit.Assert;
import org.junit.Test;

import com.healthmarketscience.jackcess.Database;
import com.hungle.sunriise.io.MnyDb;
import com.hungle.sunriise.io.sample.MnyFileAction;
import com.hungle.sunriise.io.sample.MnySampleFile;
import com.hungle.sunriise.mnyobject.TaxRate;

public class TableTaxRatesUtilsTest {
    @Test
    public void testTraverse() throws IOException {
        MnyFileAction action = new MnyFileAction() {

            @Override
            public void run(MnyDb mnyDb) throws IOException {
                Database db = mnyDb.getDb();
                Map<Integer, TaxRate> taxRates = TableTaxRatesUtils.getTaxRates(db);
                Assert.assertNotNull(taxRates);

                for (TaxRate taxRate : taxRates.values()) {
                    Assert.assertNotNull(taxRate);

                    Assert.assertNotNull(taxRate.getId());
                    String fileName = mnyDb.getDbFile().getName();
                    if (hasTaxYear(fileName)) {
                        Assert.assertNotNull(taxRate.getTaxYear());
                    }
                    String name = taxRate.getName();
                    Assert.assertNotNull(name);
                    Assert.assertTrue(TableTaxRatesUtils.FILING_STATUSES_SET.contains(name));

                    Assert.assertNotNull(taxRate.getTaxBrackets());
                    Assert.assertEquals(TableTaxRatesUtils.MAX_BRACKETS, taxRate.getTaxBrackets().size());

                }
            }

            private boolean hasTaxYear(String fileName) {
                if (fileName.contains("2001")) {
                    return false;
                } else if (fileName.contains("2002")) {
                    return false;
                }
                return true;
            }

        };
        MnySampleFile.forEachSampleFile(action);
    }
}

package com.hungle.sunriise.dbutil;

import org.apache.logging.log4j.Logger;
import org.junit.Assert;
import org.junit.Test;

import com.hungle.sunriise.dbutil.TableSecurityUtils.CompareSecurityByName;
import com.hungle.sunriise.dbutil.TableSecurityUtils.CompareSecurityHoldingsByName;
import com.hungle.sunriise.mnyobject.SecurityHolding;
import com.hungle.sunriise.mnyobject.impl.DefaultSecurity;
import com.hungle.sunriise.mnyobject.impl.DefaultSecurityHolding;

public class CompareSecurityHoldingsTest {
    private static final Logger LOGGER = org.apache.logging.log4j.LogManager
            .getLogger(CompareSecurityHoldingsTest.class);

    @Test
    public void testCompareSecurityHoldingsByName() {
        CompareSecurityHoldingsByName comparator = new CompareSecurityHoldingsByName();

        SecurityHolding o1 = new DefaultSecurityHolding();
        DefaultSecurity security1 = new DefaultSecurity();
        o1.setSecurity(security1);

        SecurityHolding o2 = new DefaultSecurityHolding();
        DefaultSecurity security2 = new DefaultSecurity();
        o2.setSecurity(security2);

        Assert.assertTrue(comparator.compare(o1, o2) == 0);

        security1.setName("IBM");
        security2.setName(null);
        Assert.assertTrue(comparator.compare(o1, o2) > 0);

        security1.setName(null);
        security2.setName("IBM");
        Assert.assertTrue(comparator.compare(o1, o2) < 0);

        security1.setName("IBM");
        security2.setName("IBM");
        Assert.assertTrue(comparator.compare(o1, o2) == 0);

        security1.setName("AAA");
        security2.setName("AAPL");
        Assert.assertTrue(comparator.compare(o1, o2) < 0);

        security1.setName("AAB");
        security2.setName("AAA");
        Assert.assertTrue(comparator.compare(o1, o2) > 0);

    }

    @Test
    public void testCompareSecurityByName() {
        CompareSecurityByName comparator = new CompareSecurityByName();

        DefaultSecurity o1 = new DefaultSecurity();

        DefaultSecurity o2 = new DefaultSecurity();

        Assert.assertTrue(comparator.compare(o1, o2) == 0);

        o1.setName("IBM");
        o2.setName(null);
        Assert.assertTrue(comparator.compare(o1, o2) > 0);

        o1.setName(null);
        o2.setName("IBM");
        Assert.assertTrue(comparator.compare(o1, o2) < 0);

        o1.setName("IBM");
        o2.setName("IBM");
        Assert.assertTrue(comparator.compare(o1, o2) == 0);

        o1.setName("AAA");
        o2.setName("AAPL");
        Assert.assertTrue(comparator.compare(o1, o2) < 0);

        o1.setName("AAB");
        o2.setName("AAA");
        Assert.assertTrue(comparator.compare(o1, o2) > 0);

    }
}

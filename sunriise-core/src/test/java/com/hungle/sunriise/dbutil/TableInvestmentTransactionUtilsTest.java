package com.hungle.sunriise.dbutil;

import java.io.IOException;

import org.junit.Assert;
import org.junit.Test;

import com.healthmarketscience.jackcess.Database;
import com.healthmarketscience.jackcess.Row;
import com.hungle.sunriise.io.DefaultMnyDbTest;
import com.hungle.sunriise.mnyobject.InvestmentTransaction;

// TODO: Auto-generated Javadoc
/**
 * The Class TableInvestmentTransactionUtilTest.
 */
public class TableInvestmentTransactionUtilsTest extends DefaultMnyDbTest {

    /**
     * Test create from row.
     */
    @Test
    public void testCreateFromRow() {
        Row row = new DefaultMockRow();
        row.put(TableInvestmentTransactionUtils.COL_ID, new Integer(1));
        row.put(TableInvestmentTransactionUtils.COL_PRICE, new Double(2.0));
        row.put(TableInvestmentTransactionUtils.COL_QUANTITY, new Double(3.0));

        InvestmentTransaction transaction = TableInvestmentTransactionUtils.deserialize(row);
        Assert.assertNotNull(transaction);

        Assert.assertEquals(new Integer(1), transaction.getId());
        Assert.assertEquals(new Double(2.0), transaction.getPrice());
        Assert.assertEquals(new Double(3.0), transaction.getQuantity());
    }

    /**
     * Test create from db.
     *
     * @throws IOException Signals that an I/O exception has occurred.
     */
    @Test
    public void testCreateFromDb() throws IOException {
        Database db = getMnyDb().getDb();
        Integer keyId = new Integer(1965);
        InvestmentTransaction transaction = TableInvestmentTransactionUtils.createInvestmentTransaction(db, keyId);
        Assert.assertNotNull(transaction);

        Assert.assertEquals(keyId, transaction.getId());
        Assert.assertEquals(new Double(1.0425), transaction.getPrice());
        Assert.assertEquals(new Double(15000), transaction.getQuantity());

        keyId = new Integer(-1);
        transaction = TableInvestmentTransactionUtils.createInvestmentTransaction(db, keyId);
        Assert.assertNull(transaction);
    }
}

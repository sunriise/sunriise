package com.hungle.sunriise.dbutil;

import java.io.IOException;
import java.util.Map;

import org.apache.logging.log4j.Logger;
import org.junit.Assert;
import org.junit.Test;

import com.hungle.sunriise.io.MnyDb;
import com.hungle.sunriise.io.sample.MnySampleFile;
import com.hungle.sunriise.io.sample.MnySampleFileFactory;
import com.hungle.sunriise.mnyobject.Category;
import com.hungle.sunriise.mnyobject.MnyContext;
import com.hungle.sunriise.util.MnyContextUtils;

public class TableCategoryUtilsTest {
    private static final Logger LOGGER = org.apache.logging.log4j.LogManager.getLogger(TableCategoryUtilsTest.class);

    @Test
    public void testSample() throws IOException {
        MnyDb sampleFile = MnySampleFileFactory.getSunsetSample();
        MnyContext mnyContext = MnyContextUtils.createMnyContext(sampleFile);

//        Map<Integer, Category> map = TableCategoryUtils.getMap(sampleFile.getDb());
        Map<Integer, Category> map = mnyContext.getCategories();

        Assert.assertEquals(MnySampleFile.SAMPLE_CATEGORY_COUNT, map.keySet().size());

        int expectedMin = MnySampleFile.SAMPLE_CATEGORY_MIN;
        int expectedMax = MnySampleFile.SAMPLE_CATEGORY_MAX;
        TableAccountUtilsTest.checkMinMaxKeys(expectedMin, expectedMax, map);
//		Integer min = null;
//		Integer max = null;
//		for (Integer key : map.keySet()) {
////			LOGGER.info("key={}", key);
//			if (min == null) {
//				min = key;
//			} else {
//				min = Math.min(min, key);
//			}
//			if (max == null) {
//				max = key;
//			} else {
//				max = Math.max(max, key);
//			}
//		}
//		Assert.assertEquals(new Integer(MnySampleFile.SAMPLE_CATEGORY_MIN), min);
//		Assert.assertEquals(new Integer(MnySampleFile.SAMPLE_CATEGORY_MAX), max);

        Iterable<Category> iter = map.values();
        for (Category value : iter) {
//			LOGGER.info("value={}", JSONUtils.writeValueAsString(value));
            if (value.getId() == 130) {
                TableCategoryUtilsTest.checkCategory130(mnyContext, value);
            }
            if (value.getId() == 131) {
                TableCategoryUtilsTest.checkCategory131(mnyContext, value);
            }

            if (value.getId() == 234) {
                TableCategoryUtilsTest.checkCategory234(mnyContext, value);
            }

            if (value.getId() == 239) {
                TableCategoryUtilsTest.checkCategory239(mnyContext, value);
            }
        }
    }

    private static void checkCategory130(MnyContext mnyContext, Category category) {
        Assert.assertEquals("INCOME", category.getName());

        String categoryName = TableCategoryUtils.getCategoryName(category.getId(), mnyContext);
        Assert.assertEquals("INCOME", categoryName);

        String categoryFullName = TableCategoryUtils.getCategoryFullName(category.getId(), mnyContext);
        Assert.assertNull(categoryFullName);
        Assert.assertNull(category.getFullName());

        boolean expense = TableCategoryUtils.isExpense(category.getId(), mnyContext.getCategories());
        Assert.assertFalse(expense);
        Assert.assertFalse(category.getExpense());
    }

    private static void checkCategory131(MnyContext mnyContext, Category category) {
        Assert.assertEquals("EXPENSE", category.getName());

        String categoryName = TableCategoryUtils.getCategoryName(category.getId(), mnyContext);
        Assert.assertEquals("EXPENSE", categoryName);

        String categoryFullName = TableCategoryUtils.getCategoryFullName(category.getId(), mnyContext);
        Assert.assertNull(categoryFullName);
        Assert.assertNull(category.getFullName());

        boolean expense = TableCategoryUtils.isExpense(category.getId(), mnyContext.getCategories());
        Assert.assertTrue(expense);
        Assert.assertTrue(category.getExpense());
    }

    public static final void checkCategory239(MnyContext mnyContext, Category category) {
        Assert.assertEquals("House Cleaning", category.getName());

        String categoryName = TableCategoryUtils.getCategoryName(category.getId(), mnyContext);
        Assert.assertEquals("House Cleaning", categoryName);
        Assert.assertTrue(category.getExpense());

        String categoryFullName = TableCategoryUtils.getCategoryFullName(category.getId(), mnyContext);
        Assert.assertEquals("Household:House Cleaning", categoryFullName);

        Assert.assertEquals(new Integer(237), category.getParentId());

        String parentCategoryFullName = TableCategoryUtils.getCategoryFullName(category.getParentId(), mnyContext);
        Assert.assertEquals("Household", parentCategoryFullName);
    }

    public static final void checkCategory234(MnyContext mnyContext, Category category) {
        Assert.assertEquals("Bank Charges", category.getName());

        String categoryName = TableCategoryUtils.getCategoryName(category.getId(), mnyContext);
        Assert.assertEquals("Bank Charges", categoryName);
        Assert.assertTrue(category.getExpense());

        String categoryFullName = TableCategoryUtils.getCategoryFullName(category.getId(), mnyContext);
        Assert.assertEquals("Bank Charges", categoryFullName);
    }
}

package com.hungle.sunriise.dbutil;

import java.io.IOException;
import java.util.Map;

import org.apache.logging.log4j.Logger;
import org.junit.Assert;
import org.junit.Test;

import com.hungle.sunriise.io.MnyDb;
import com.hungle.sunriise.io.sample.MnySampleFile;
import com.hungle.sunriise.io.sample.MnySampleFileFactory;
import com.hungle.sunriise.mnyobject.Currency;

public class TableCurrencyUtilsTest {
    private static final Logger LOGGER = org.apache.logging.log4j.LogManager.getLogger(TableCurrencyUtilsTest.class);

    @Test
    public void testSample() throws IOException {
        MnyDb sampleFile = MnySampleFileFactory.getSunsetSample();

        Map<Integer, Currency> map = TableCurrencyUtils.getMap(sampleFile.getDb());
        Assert.assertEquals(MnySampleFile.SAMPLE_CURRENCY_COUNT, map.keySet().size());

        int expectedMin = MnySampleFile.SAMPLE_CURRENCY_MIN;
        int expectedMax = MnySampleFile.SAMPLE_CURRENCY_MAX;
        TableAccountUtilsTest.checkMinMaxKeys(expectedMin, expectedMax, map);

        for (Currency value : map.values()) {
//			LOGGER.info("value={}", JSONUtils.writeValueAsString(value));
            if (value.getId() == 45) {
                checkCurrency45(value);
            }
        }
    }

    public static final void checkCurrency45(Currency currency) {
        Assert.assertEquals("US dollar", currency.getName());
        Assert.assertEquals("USD", currency.getIsoCode());
    }
}

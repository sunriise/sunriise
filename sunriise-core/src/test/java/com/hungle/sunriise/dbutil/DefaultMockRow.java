package com.hungle.sunriise.dbutil;

import java.io.IOException;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.LinkedHashMap;

import com.healthmarketscience.jackcess.Row;
import com.healthmarketscience.jackcess.RowId;
import com.healthmarketscience.jackcess.complex.ComplexValueForeignKey;
import com.healthmarketscience.jackcess.util.OleBlob;

// TODO: Auto-generated Javadoc
/**
 * The Class DefaultMockRow.
 */
public class DefaultMockRow extends LinkedHashMap<String, Object> implements Row {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 7836186539329968248L;

    /*
     * (non-Javadoc)
     * 
     * @see com.healthmarketscience.jackcess.Row#getId()
     */
    @Override
    public RowId getId() {
        return null;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.healthmarketscience.jackcess.Row#getString(java.lang.String)
     */
    @Override
    public String getString(String name) {
        return (String) get(name);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.healthmarketscience.jackcess.Row#getBoolean(java.lang.String)
     */
    @Override
    public Boolean getBoolean(String name) {
        return (Boolean) get(name);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.healthmarketscience.jackcess.Row#getByte(java.lang.String)
     */
    @Override
    public Byte getByte(String name) {
        return (Byte) get(name);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.healthmarketscience.jackcess.Row#getShort(java.lang.String)
     */
    @Override
    public Short getShort(String name) {
        // TODO Auto-generated method stub
        return (Short) get(name);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.healthmarketscience.jackcess.Row#getInt(java.lang.String)
     */
    @Override
    public Integer getInt(String name) {
        // TODO Auto-generated method stub
        return (Integer) get(name);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.healthmarketscience.jackcess.Row#getBigDecimal(java.lang.String)
     */
    @Override
    public BigDecimal getBigDecimal(String name) {
        // TODO Auto-generated method stub
        return (BigDecimal) get(name);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.healthmarketscience.jackcess.Row#getFloat(java.lang.String)
     */
    @Override
    public Float getFloat(String name) {
        // TODO Auto-generated method stub
        return (Float) get(name);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.healthmarketscience.jackcess.Row#getDouble(java.lang.String)
     */
    @Override
    public Double getDouble(String name) {
        // TODO Auto-generated method stub
        return (Double) get(name);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.healthmarketscience.jackcess.Row#getDate(java.lang.String)
     */
    @Override
    public Date getDate(String name) {
        // TODO Auto-generated method stub
        return (Date) get(name);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.healthmarketscience.jackcess.Row#getBytes(java.lang.String)
     */
    @Override
    public byte[] getBytes(String name) {
        // TODO Auto-generated method stub
        return (byte[]) get(name);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.healthmarketscience.jackcess.Row#getForeignKey(java.lang.String)
     */
    @Override
    public ComplexValueForeignKey getForeignKey(String name) {
        // TODO Auto-generated method stub
        return null;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.healthmarketscience.jackcess.Row#getBlob(java.lang.String)
     */
    @Override
    public OleBlob getBlob(String name) throws IOException {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public LocalDateTime getLocalDateTime(String name) {
        // TODO Auto-generated method stub
        return null;
    }
}

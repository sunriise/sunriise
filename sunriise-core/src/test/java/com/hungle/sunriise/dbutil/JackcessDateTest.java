package com.hungle.sunriise.dbutil;

import java.io.File;
import java.io.IOException;
import java.nio.channels.FileChannel;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.TimeZone;

import org.apache.logging.log4j.Logger;
import org.junit.Assert;
import org.junit.Test;

import com.healthmarketscience.jackcess.ColumnBuilder;
import com.healthmarketscience.jackcess.CursorBuilder;
import com.healthmarketscience.jackcess.DataType;
import com.healthmarketscience.jackcess.Database;
import com.healthmarketscience.jackcess.Database.FileFormat;
import com.healthmarketscience.jackcess.DatabaseBuilder;
import com.healthmarketscience.jackcess.DateTimeType;
import com.healthmarketscience.jackcess.Row;
import com.healthmarketscience.jackcess.Table;
import com.healthmarketscience.jackcess.TableBuilder;

/**
 * The Class JackcessDateTest.
 */
public class JackcessDateTest {
    private static final Logger LOGGER = org.apache.logging.log4j.LogManager.getLogger(JackcessDateTest.class);

    /** The Constant TABLE_NAME. */
    private static final String TABLE_NAME = "test";

    /** The Constant COL_NAME. */
    private static final String COL_NAME = "name";

    /** The Constant COL_DATE. */
    private static final String COL_DATE = "date";

    /** The Constant _autoSync. */
    private static final ThreadLocal<Boolean> _autoSync = new ThreadLocal<Boolean>();

    /**
     * Test date.
     *
     * @throws Exception the exception
     */
    @Test
    public void testDate() throws Exception {

        File tempFile = createTempFile(false);

        String rowId = "row 01";

        Calendar calendar = Calendar.getInstance();
        // month: 0 is January
        calendar.set(2017, 00, 01);
        Date createdDate = calendar.getTime();

        createTestDatabase(tempFile, rowId, createdDate);

        // Read in local timezone
        Assert.assertTrue(checkReadDate(tempFile, rowId, createdDate));

        TimeZone localTimeZone = TimeZone.getDefault();
        TimeZone utcTimeZone = TimeZone.getTimeZone("UTC");

        Date now = new Date();
        int localOffset = localTimeZone.getOffset(now.getTime());
        int utcOffset = utcTimeZone.getOffset(now.getTime());

        LOGGER.info("Local timeZone=" + localTimeZone.getID());
        LOGGER.info("UTC timeZone=" + utcTimeZone.getID());

        if (localOffset != utcOffset) {
            // Let's say file was created in PST and now being read in UTC

            // simulate reading in UTC timezone
            TimeZone.setDefault(utcTimeZone);

            Assert.assertFalse(checkReadDate(tempFile, rowId, createdDate));
        } else {
            // if local and utc are the same, then there is no point to this
            // test
            LOGGER.info("SKIP checking createdDate and dbDate because local and UTC timezone are the same.");
        }
    }

    /**
     * Create a test database with two columns: COL_NAME and COL_DATE. Add a test
     * row with rowId and createdDate
     * 
     * This is done in local timezone.
     *
     * @param tempFile    the temp file
     * @param rowId       the row id
     * @param createdDate the created date
     * @throws Exception the exception
     */
    private void createTestDatabase(File tempFile, String rowId, Date createdDate) throws Exception {
        FileFormat fileFormat = FileFormat.V2010;
        Database db = null;
        try {
            db = JackcessDateTest.create(tempFile, fileFormat);
            Table table = new TableBuilder(TABLE_NAME).addColumn(new ColumnBuilder(COL_NAME, DataType.TEXT))
                    .addColumn(new ColumnBuilder(COL_DATE, DataType.SHORT_DATE_TIME)).toTable(db);

            table.addRow(rowId, createdDate);
        } finally {
            if (db != null) {
                db.close();
                db = null;
            }
        }
    }

    /**
     * Check the date we get back from db is the same as the original setDate().
     *
     * @param tempFile    the temp file
     * @param rowId       the row id
     * @param createdDate the created date
     * @return
     * @throws IOException Signals that an I/O exception has occurred.
     */
    private boolean checkReadDate(File tempFile, String rowId, Date createdDate) throws IOException {
        boolean same = false;
        Database db = null;
        try {
            db = DatabaseBuilder.open(tempFile);
            if (db != null) {
                DateTimeType dateTimeType = DateTimeType.DATE;
                db.setDateTimeType(dateTimeType);
            }
            Table table = db.getTable(TABLE_NAME);
            Row row = CursorBuilder.findRow(table, Collections.singletonMap(COL_NAME, rowId));
            Assert.assertNotNull(row);

            Date dbDate = row.getDate(COL_DATE);
            Assert.assertNotNull(dbDate);

            LOGGER.info("createdDate=" + createdDate);
            LOGGER.info("dbDate=" + dbDate);

            same = (createdDate.compareTo(dbDate) == 0);
        } finally {
            if (db != null) {
                db.close();
                db = null;
            }
        }

        return same;
    }

    /**
     * Creates the.
     *
     * @param tempFile   the temp file
     * @param fileFormat the file format
     * @return the database
     * @throws Exception the exception
     */
    private static Database create(File tempFile, FileFormat fileFormat) throws Exception {
        FileChannel channel = null;

        Database db = new DatabaseBuilder(tempFile).setFileFormat(fileFormat).setAutoSync(getTestAutoSync())
                .setChannel(channel).create();
        if (db != null) {
            DateTimeType dateTimeType = DateTimeType.DATE;
            db.setDateTimeType(dateTimeType);
        }
        return db;
    }

    /**
     * Creates the temp file.
     *
     * @param keep the keep
     * @return the file
     * @throws Exception the exception
     */
    private static File createTempFile(boolean keep) throws Exception {
        File tmp = File.createTempFile("databaseTest", ".mdb");
        if (keep) {
            LOGGER.info("Created " + tmp);
        } else {
            tmp.deleteOnExit();
        }
        return tmp;
    }

    /**
     * Gets the test auto sync.
     *
     * @return the test auto sync
     */
    private static boolean getTestAutoSync() {
        Boolean autoSync = _autoSync.get();
        return ((autoSync != null) ? autoSync : Database.DEFAULT_AUTO_SYNC);
    }
}

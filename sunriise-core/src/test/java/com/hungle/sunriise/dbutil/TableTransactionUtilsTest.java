package com.hungle.sunriise.dbutil;

import java.io.IOException;
import java.util.Date;
import java.util.Map;

import org.apache.logging.log4j.Logger;
import org.junit.Assert;
import org.junit.Test;

import com.healthmarketscience.jackcess.Cursor;
import com.healthmarketscience.jackcess.Database;
import com.healthmarketscience.jackcess.Table;
import com.hungle.sunriise.io.MnyDb;
import com.hungle.sunriise.io.sample.MnyFileAction;
import com.hungle.sunriise.io.sample.MnySampleFile;
import com.hungle.sunriise.io.sample.MnySampleFileFactory;
import com.hungle.sunriise.mnyobject.AccountLink;
import com.hungle.sunriise.mnyobject.CategoryLink;
import com.hungle.sunriise.mnyobject.Transaction;

// TODO: Auto-generated Javadoc
/**
 * The Class TableTransactionUtilTest.
 */
public class TableTransactionUtilsTest {
    private static final Logger LOGGER = org.apache.logging.log4j.LogManager.getLogger(TableTransactionUtilsTest.class);

    /**
     * Test not resolved.
     *
     * @throws IOException Signals that an I/O exception has occurred.
     */
    @Test
    public void testNotResolved() throws IOException {
        MnyFileAction action = new MnyFileAction() {

            @Override
            public void run(MnyDb mnyDb) throws IOException {
                Database db = mnyDb.getDb();

                Table table = TableTransactionUtils.getTable(db);
                Assert.assertNotNull(table);

                Cursor cursor = TableTransactionUtils.createCursor(db);
                Assert.assertNotNull(cursor);

                Assert.assertEquals(table, cursor.getTable());

                Map<Integer, Transaction> transactions = TableTransactionUtils.getTransactions(db);
                Assert.assertNotNull(transactions);

                for (Transaction transaction : transactions.values()) {
                    AccountLink account = transaction.getAccount();
                    Assert.assertNotNull(account);

                    // we have not resolved the account's name
                    Assert.assertNull(account.getName());

                    Date date = transaction.getDate();
                    Assert.assertNotNull(date);

                    CategoryLink category = transaction.getCategory();
                    Assert.assertNotNull(category);
                    // we have not resolved the category's name
                    Assert.assertNull(category.getName());

                }
            }
        };
        MnySampleFile.forEachSampleFile(action);
    }

    @Test
    public void testSample() throws IOException {
        MnyDb sampleFile = MnySampleFileFactory.getSunsetSample();
//			MnyContext mnyContext = MnyContextUtils.createMnyContext(sampleFile);

        Map<Integer, Transaction> map = TableTransactionUtils.getMap(sampleFile.getDb());
        Assert.assertEquals(MnySampleFile.SAMPLE_TRANSACTION_COUNT, map.keySet().size());

        int expectedMin = MnySampleFile.SAMPLE_TRANSACTION_MIN;
        int expectedMax = MnySampleFile.SAMPLE_TRANSACTION_MAX;
        TableAccountUtilsTest.checkMinMaxKeys(expectedMin, expectedMax, map);

        for (Transaction value : map.values()) {
//			LOGGER.info("value={}", JSONUtils.writeValueAsString(value));

//			{
//				  "_id" : 2301,
//				  "date" : "2002-01-05T08:00:00.000+0000",
//				  "number" : null,
//				  "amount" : -24.00,
//				  "category" : {
//				    "_id" : 170,
//				    "name" : "Health Club",
//				    "parentId" : 160,
//				    "parentName" : "Bills",
//				    "fullName" : "Bills:Health Club"
//				  },
//				  "memo" : null,
//				  "payee" : {
//				    "_id" : 100,
//				    "name" : "The Gym"
//				  },
//				  "splits" : null,
//				  "account" : {
//				    "_id" : 42,
//				    "name" : "Woodgrove Bank Checking"
//				  },
//				  "void" : false,
//				  "cleared" : false,
//				  "reconciled" : true,
//				  "transfer" : false,
//				  "xferInfo" : {
//				    "xferAccountId" : null,
//				    "xferTransactionId" : null
//				  },
//				  "investment" : false,
//				  "investmentInfo" : {
//				    "activity" : {
//				      "label" : "ACTIVITY_UNKNOWN",
//				      "type" : -1
//				    },
//				    "security" : null,
//				    "transaction" : null
//				  },
//				  "recurring" : false,
//				  "frequency" : {
//				    "label" : "Non-recurring",
//				    "recurring" : false,
//				    "type" : -1
//				  },
//				  "classification1" : {
//				    "_id" : -1,
//				    "name" : null
//				  }
//				}
            if (value.getId() == 2301) {
                checkTransaction2301(value);
            }

        }
    }

    public static final void checkTransaction2301(Transaction transaction) {
        Assert.assertEquals(-24.00, transaction.getAmount().doubleValue(), 0.00001);

        CategoryLink category = transaction.getCategory();
        Assert.assertNotNull(category);
    }
}

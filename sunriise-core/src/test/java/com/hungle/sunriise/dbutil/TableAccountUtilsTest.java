package com.hungle.sunriise.dbutil;

import java.io.IOException;
import java.util.Map;

import org.apache.logging.log4j.Logger;
import org.junit.Assert;
import org.junit.Test;

import com.healthmarketscience.jackcess.Database;
import com.hungle.sunriise.io.MnyDb;
import com.hungle.sunriise.io.sample.MnyFileAction;
import com.hungle.sunriise.io.sample.MnySampleFile;
import com.hungle.sunriise.io.sample.MnySampleFileFactory;
import com.hungle.sunriise.json.JSONUtils;
import com.hungle.sunriise.mnyobject.Account;
import com.hungle.sunriise.mnyobject.EnumAccountType;
import com.hungle.sunriise.mnyobject.Transaction;

// TODO: Auto-generated Javadoc
/**
 * The Class TableAccountUtilTest.
 */
public class TableAccountUtilsTest {
    private static final Logger LOGGER = org.apache.logging.log4j.LogManager.getLogger(TableAccountUtilsTest.class);

    /**
     * Test 01.
     *
     * @throws IOException Signals that an I/O exception has occurred.
     */
    @Test
    public void testTraverse() throws IOException {
        MnyFileAction action = new MnyFileAction() {

            @Override
            public void run(MnyDb mnyDb) throws IOException {
                Database db = mnyDb.getDb();
                Map<Integer, Account> accounts = TableAccountUtils.getAccounts(db);
                Assert.assertNotNull(accounts);

                for (Account account : accounts.values()) {
                    String name = account.getName();
                    Assert.assertNotNull(name);
                    Account relatedToAccount = account.getRelatedToAccount();
                    if (relatedToAccount != null) {
                        Account relatedToAccount2 = relatedToAccount.getRelatedToAccount();
                        if (relatedToAccount2 != null) {
                            Assert.assertEquals(relatedToAccount2.getId(), account.getId());
                        } else {
                            if (account.getAccountType() != EnumAccountType.LOAN) {
                                LOGGER.warn("account=" + JSONUtils.writeValueAsString(account));
                                LOGGER.warn("relatedToAccount=" + JSONUtils.writeValueAsString(relatedToAccount));
                            }
                        }
                    }
                }

                Map<Integer, Transaction> transactions = TableTransactionUtils.getTransactions(db);
                for (Transaction transaction : transactions.values()) {
                    // transaction's account must be valid
                    Integer accountId = transaction.getAccount().getId();
                    if (accountId != null) {
                        Account account = accounts.get(accountId);
                        Assert.assertNotNull("Cannot find account.id=" + accountId, account);
                    }
                }
            }

        };
        MnySampleFile.forEachSampleFile(action);
    }

    @Test
    public void testSample() throws IOException {
        MnyDb sampleFile = MnySampleFileFactory.getSunsetSample();
//		MnyContext mnyContext = MnyContextUtils.createMnyContext(sampleFile);

        Map<Integer, Account> map = TableAccountUtils.getMap(sampleFile.getDb());

        Assert.assertEquals(MnySampleFile.SAMPLE_ACCOUNT_COUNT, map.keySet().size());

        int expectedMin = MnySampleFile.SAMPLE_ACCOUNT_MIN;
        int expectedMax = MnySampleFile.SAMPLE_ACCOUNT_MAX;
        checkMinMaxKeys(expectedMin, expectedMax, map);

        Iterable<Account> iter = map.values();
        for (Account value : iter) {
//			LOGGER.info("value={}", JSONUtils.writeValueAsString(value));

            if (value.getId() == 42) {
                TableAccountUtilsTest.checkAccount42(value);
            }

            if (value.getId() == 72) {
                TableAccountUtilsTest.checkAccount72(value);
            }
        }
    }

    public static final void checkAccount72(Account account) {
        Assert.assertEquals("Charlie & May’s Joint Investment", account.getName());
        Assert.assertEquals(EnumAccountType.INVESTMENT, account.getAccountType());
        Assert.assertEquals(Boolean.FALSE, account.getClosed());

        Account relatedToAccount = account.getRelatedToAccount();
        Assert.assertNotNull(relatedToAccount);
        Assert.assertEquals("Charlie & May’s Joint Inv (Cash)", relatedToAccount.getName());
        Assert.assertEquals(EnumAccountType.BANKING, relatedToAccount.getAccountType());
        Assert.assertEquals(Boolean.FALSE, relatedToAccount.getClosed());

        relatedToAccount = relatedToAccount.getRelatedToAccount();
        Assert.assertEquals(new Integer(72), relatedToAccount.getId());
    }

    public static final void checkAccount42(Account account) {
        Assert.assertEquals("Woodgrove Bank Checking", account.getName());
        Assert.assertEquals(EnumAccountType.BANKING, account.getAccountType());
        Assert.assertEquals(Boolean.FALSE, account.getClosed());
    }

    public static final <T> void checkMinMaxKeys(int expectedMin, int expectedMax, Map<Integer, T> map) {
        Integer min = null;
        Integer max = null;
        for (Integer key : map.keySet()) {
//			LOGGER.info("key={}", key);
            if (min == null) {
                min = key;
            } else {
                min = Math.min(min, key);
            }
            if (max == null) {
                max = key;
            } else {
                max = Math.max(max, key);
            }
        }
        Assert.assertEquals(new Integer(expectedMin), min);
        Assert.assertEquals(new Integer(expectedMax), max);
    }
}

package com.hungle.sunriise.dbutil;

import java.io.IOException;
import java.util.Map;

import org.apache.logging.log4j.Logger;
import org.junit.Assert;
import org.junit.Test;

import com.hungle.sunriise.io.MnyDb;
import com.hungle.sunriise.io.sample.MnySampleFile;
import com.hungle.sunriise.io.sample.MnySampleFileFactory;
import com.hungle.sunriise.mnyobject.Security;

public class TableSecurityUtilsTest {
    private static final Logger LOGGER = org.apache.logging.log4j.LogManager.getLogger(TableSecurityUtilsTest.class);

    @Test
    public void testSample() throws IOException {
        MnyDb sampleFile = MnySampleFileFactory.getSunsetSample();

        Map<Integer, Security> map = TableSecurityUtils.getMap(sampleFile.getDb());
        Assert.assertEquals(MnySampleFile.SAMPLE_SECURITY_COUNT, map.keySet().size());

        int expectedMin = MnySampleFile.SAMPLE_SECURITY_MIN;
        int expectedMax = MnySampleFile.SAMPLE_SECURITY_MAX;
        TableAccountUtilsTest.checkMinMaxKeys(expectedMin, expectedMax, map);

        for (Security value : map.values()) {
//			LOGGER.info("value={}", JSONUtils.writeValueAsString(value));
            if (value.getId() == 78) {
                checkSecurity78(value);
            }
        }
    }

    public static final void checkSecurity78(Security security) {
        Assert.assertEquals("Contoso Pharmaceuticals", security.getName());
        Assert.assertEquals("LEH", security.getSymbol());
    }
}

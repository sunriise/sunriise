package com.hungle.sunriise.dbutil;

import java.io.IOException;
import java.util.Map;

import org.apache.logging.log4j.Logger;
import org.junit.Assert;
import org.junit.Test;

import com.hungle.sunriise.io.MnyDb;
import com.hungle.sunriise.io.sample.MnySampleFile;
import com.hungle.sunriise.io.sample.MnySampleFileFactory;
import com.hungle.sunriise.mnyobject.Payee;

public class TablePayeeUtilsTest {
    private static final Logger LOGGER = org.apache.logging.log4j.LogManager.getLogger(TablePayeeUtilsTest.class);

    @Test
    public void testSample() throws IOException {
        MnyDb sampleFile = MnySampleFileFactory.getSunsetSample();

        Map<Integer, Payee> map = TablePayeeUtils.getMap(sampleFile.getDb());
        Assert.assertEquals(MnySampleFile.SAMPLE_PAYEE_COUNT, map.keySet().size());

        int expectedMin = MnySampleFile.SAMPLE_PAYEE_MIN;
        int expectedMax = MnySampleFile.SAMPLE_PAYEE_MAX;
        TableAccountUtilsTest.checkMinMaxKeys(expectedMin, expectedMax, map);

        Iterable<Payee> iter = map.values();
        for (Payee value : iter) {
//			LOGGER.info("value={}", JSONUtils.writeValueAsString(value));
            if (value.getId() == 108) {
                TablePayeeUtilsTest.checkPayeeId108(value);
            }
        }
    }

    public static final void checkPayeeId108(Payee payee) {
        Assert.assertEquals("Humongous Insurance", payee.getName());
        Assert.assertNull(payee.getParent());
    }
}

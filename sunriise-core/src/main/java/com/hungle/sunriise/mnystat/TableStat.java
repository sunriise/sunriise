package com.hungle.sunriise.mnystat;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.healthmarketscience.jackcess.Table;
import com.hungle.sunriise.diskusage.CalculateDiskUsage;

// TODO: Auto-generated Javadoc
/**
 * The Class TableStat.
 */
@JsonPropertyOrder({ "tableName", "bytes", "rowCount", "columnCount" })
public final class TableStat {

    /** The table. */
    @JsonIgnore
    private Table table;

    /**
     * Instantiates a new table stat.
     *
     * @param table the table
     */
    public TableStat(Table table) {
        this.table = table;
    }

    /**
     * Gets the table name.
     *
     * @return the table name
     */
    public String getTableName() {
        return table.getName();
    }

    /**
     * Gets the row count.
     *
     * @return the row count
     */
    public int getRowCount() {
        return table.getRowCount();
    }

    /**
     * Gets the index count.
     *
     * @return the index count
     */
    public int getIndexCount() {
        return table.getIndexes().size();
    }

    /**
     * Gets the bytes.
     *
     * @return the bytes
     */
    public int getBytes() {
        int bytes = CalculateDiskUsage.calculateTableByteCount(table);
        return bytes;
    }

    /**
     * Gets the column count.
     *
     * @return the column count
     */
    public int getColumnCount() {
        return table.getColumns().size();
    }
}
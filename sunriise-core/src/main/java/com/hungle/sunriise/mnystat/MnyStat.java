package com.hungle.sunriise.mnystat;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import org.apache.commons.codec.digest.DigestUtils;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.healthmarketscience.jackcess.Database;
import com.healthmarketscience.jackcess.Table;
import com.hungle.sunriise.dbutil.TableAccountUtils;
import com.hungle.sunriise.io.MnyDb;
import com.hungle.sunriise.mnyobject.Account;
import com.hungle.sunriise.mnyobject.MnyContext;
import com.hungle.sunriise.util.MnyContextUtils;

// TODO: Auto-generated Javadoc
/**
 * The Class MnyStat.
 */
@JsonPropertyOrder({ "fileName", "fileSize", "numberOfTables", "tableStats" })
public class MnyStat {

    /** The opened db. */
    @JsonIgnore
    private final MnyDb mnyDb;

    /** The db. */
    @JsonIgnore
    private final Database db;

    /** The mny context. */
    @JsonIgnore
    private final MnyContext mnyContext;

    /**
     * Instantiates a new mny stat.
     *
     * @param mnyDb the opened db
     * @param parseAccounts the parse accounts
     * @throws IOException Signals that an I/O exception has occurred.
     */
    public MnyStat(MnyDb mnyDb, boolean parseAccounts) throws IOException {
        this.mnyDb = mnyDb;
        this.db = mnyDb.getDb();
        this.mnyContext = MnyContextUtils.createMnyContext(mnyDb);

        if (parseAccounts) {
            // Assert.assertNotNull(mnyContext);
            TableAccountUtils.populateAccounts(mnyContext);
        }
    }

    /**
     * Instantiates a new mny stat.
     *
     * @param mnyDb the mny db
     * @throws IOException Signals that an I/O exception has occurred.
     */
    public MnyStat(MnyDb mnyDb) throws IOException {
        this(mnyDb, false);
    }

    /**
     * Gets the file name.
     *
     * @return the file name
     */
    public String getFileName() {
        String fileName = db.getFile().getAbsolutePath();
        fileName = DigestUtils.sha1Hex(fileName);
        return fileName;
    }

    /**
     * Gets the file size.
     *
     * @return the file size
     */
    public long getFileSize() {
        return db.getFile().length();
    }

    /**
     * Gets the number of system tables.
     *
     * @return the number of system tables
     * @throws IOException Signals that an I/O exception has occurred.
     */
    public int getNumberOfSystemTables() throws IOException {
        return db.getSystemTableNames().size();
    }

    /**
     * Gets the number of tables.
     *
     * @return the number of tables
     * @throws IOException Signals that an I/O exception has occurred.
     */
    public int getNumberOfTables() throws IOException {
        return db.getTableNames().size();
    }

    /**
     * Gets the system table stats.
     *
     * @return the system table stats
     * @throws IOException Signals that an I/O exception has occurred.
     */
    public List<TableStat> getSystemTableStats() throws IOException {
        List<TableStat> tableStats = new ArrayList<TableStat>();
        Set<String> tableNames = new TreeSet<String>(db.getSystemTableNames());
        for (String tableName : tableNames) {
            Table table = db.getSystemTable(tableName);
            tableStats.add(new TableStat(table));
        }
        return tableStats;
    }

    /**
     * Gets the table stats.
     *
     * @return the table stats
     * @throws IOException Signals that an I/O exception has occurred.
     */
    public List<TableStat> getTableStats() throws IOException {
        List<TableStat> tableStats = new ArrayList<TableStat>();
        Set<String> tableNames = new TreeSet<String>(db.getTableNames());
        for (String tableName : tableNames) {
            Table table = db.getTable(tableName);
            tableStats.add(new TableStat(table));
        }
        return tableStats;
    }

    /**
     * Gets the number of accounts.
     *
     * @return the number of accounts
     */
    public int getNumberOfAccounts() {
        return mnyContext.getAccounts().size();
    }

    /**
     * Gets the account stats.
     *
     * @return the account stats
     */
    public List<AccountStat> getAccountStats() {
        List<AccountStat> accountStats = new ArrayList<AccountStat>();
        ArrayList<Account> accounts = new ArrayList<Account>();
        accounts.addAll(mnyContext.getAccounts().values());
        Comparator<Account> comparator = new TableAccountUtils.CompareAccountsByName();
        Collections.sort(accounts, comparator);
        for (Account account : accounts) {
            accountStats.add(new AccountStat(account));
        }
        return accountStats;
    }
}

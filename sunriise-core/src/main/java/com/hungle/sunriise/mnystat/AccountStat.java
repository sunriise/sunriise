package com.hungle.sunriise.mnystat;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.codec.digest.DigestUtils;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.hungle.sunriise.dbutil.TableTransactionUtils;
import com.hungle.sunriise.json.JSONUtils;
import com.hungle.sunriise.mnyobject.Account;
import com.hungle.sunriise.mnyobject.EnumAccountType;
import com.hungle.sunriise.mnyobject.Transaction;

// TODO: Auto-generated Javadoc
/**
 * The Class AccountStat.
 */
@JsonPropertyOrder({ "accountName", "accountId", "accountType", "numberOfTransactions" })
public class AccountStat {

    /** The account. */
    @JsonIgnore
    private Account account;

    /**
     * Instantiates a new account stat.
     *
     * @param account the account
     */
    public AccountStat(Account account) {
        this.account = account;
    }

    /**
     * Gets the account name.
     *
     * @return the account name
     */
    public String getAccountName() {
        String name = account.getName();
        name = DigestUtils.sha1Hex(name);
        return name;
    }

    /**
     * Gets the account id.
     *
     * @return the account id
     */
    public Integer getAccountId() {
        return account.getId();
    }

    /**
     * Gets the account type.
     *
     * @return the account type
     */
    public EnumAccountType getAccountType() {
        return account.getAccountType();
    }

    /**
     * Gets the number of transactions.
     *
     * @return the number of transactions
     */
    public int getNumberOfTransactions() {
        return account.getTransactions().size();
    }

    /**
     * Gets the number of filtered transactions.
     *
     * @return the number of filtered transactions
     */
    public int getNumberOfFilteredTransactions() {
        return account.getFilteredTransactions().size();
    }

    /**
     * Gets the date stats.
     *
     * @return the date stats
     */
    public Map<String, String> getDateStats() {
        Map<String, String> map = new HashMap<String, String>();

        List<Transaction> transactions = account.getTransactions();
        List<Transaction> sortByDate = new ArrayList<Transaction>();
        sortByDate.addAll(transactions);
        Comparator<Transaction> comparator = new TableTransactionUtils.CompareTransactionsByDate();
        Collections.sort(sortByDate, comparator);
        Date oldestTransaction = null;
        Date newestTransaction = null;
        if (sortByDate.size() > 1) {
            oldestTransaction = sortByDate.get(0).getDate();
            newestTransaction = sortByDate.get(sortByDate.size() - 1).getDate();
        } else if (sortByDate.size() > 0) {
            oldestTransaction = sortByDate.get(0).getDate();
            newestTransaction = oldestTransaction;
        } else {
            oldestTransaction = null;
            newestTransaction = null;
        }

        if (oldestTransaction != null) {
            map.put("oldestTransaction", JSONUtils.toISODateTimeFormatString(oldestTransaction));
        } else {
            map.put("oldestTransaction", null);
        }
        if (newestTransaction != null) {
            map.put("newestTransaction", JSONUtils.toISODateTimeFormatString(newestTransaction));
        } else {
            map.put("newestTransaction", null);
        }

        return map;
    }
}

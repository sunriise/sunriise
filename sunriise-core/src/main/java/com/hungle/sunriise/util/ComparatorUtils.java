package com.hungle.sunriise.util;

public class ComparatorUtils {
    public static final <T extends Comparable<T>> int compareNullOrder(T o1, T o2) {
        if ((o1 == null) && (o2 == null)) {
            return 0;
        } else if (o2 == null) {
            return 1;
        } else if (o1 == null) {
            return -1;
        } else {
            return o1.compareTo(o2);
        }
    }
}

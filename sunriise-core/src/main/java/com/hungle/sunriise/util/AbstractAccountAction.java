package com.hungle.sunriise.util;

import java.io.IOException;

import com.hungle.sunriise.mnyobject.Account;
import com.hungle.sunriise.mnyobject.MnyContext;

// TODO: Auto-generated Javadoc
/**
 * The Class AbstractAccountAction.
 */
public abstract class AbstractAccountAction {

    /** The account. */
    private Account account;

    /** The mny context. */
    private MnyContext mnyContext;

    /**
     * Gets the account.
     *
     * @return the account
     */
    public Account getAccount() {
        return account;
    }

    /**
     * Sets the account.
     *
     * @param account the new account
     */
    public void setAccount(Account account) {
        this.account = account;
    }

    /**
     * Gets the mny context.
     *
     * @return the mny context
     */
    public MnyContext getMnyContext() {
        return mnyContext;
    }

    /**
     * Sets the mny context.
     *
     * @param mnyContext the new mny context
     */
    public void setMnyContext(MnyContext mnyContext) {
        this.mnyContext = mnyContext;
    }

    /**
     * Execute.
     *
     * @throws IOException Signals that an I/O exception has occurred.
     */
    public abstract void execute() throws IOException;

}
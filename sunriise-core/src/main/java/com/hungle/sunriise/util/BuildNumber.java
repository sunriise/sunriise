/*******************************************************************************
 * Copyright (c) 2016 Hung Le
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *******************************************************************************/
package com.hungle.sunriise.util;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.Enumeration;
import java.util.jar.Attributes;
import java.util.jar.Manifest;

import org.apache.logging.log4j.Logger;

// TODO: Auto-generated Javadoc
/**
 * The Class BuildNumber.
 */
public class BuildNumber {

    /** The Constant log. */
    private static final Logger LOGGER = org.apache.logging.log4j.LogManager.getLogger(BuildNumber.class);

    /**
     * Find builder number.
     *
     * @param implementationVendorId the implementation vendor id
     * @param resourceName           the resource name
     * @param classLoader            the class loader
     * @return the string
     */
    public static String findBuilderNumber(String implementationVendorId, String resourceName,
            ClassLoader classLoader) {
        String buildNumber = null;
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("> findBuilderNumber: resourceName=" + resourceName + ", classLoader=" + classLoader);
        }

        if (classLoader == null) {
            return null;
        }
        Enumeration<URL> resources = null;
        try {
            resources = classLoader.getResources(resourceName);
            if (resources == null) {
                LOGGER.warn("classLoader.getResources return null");
                return null;
            }
        } catch (IOException e) {
            LOGGER.warn(e);
            return null;
        }

        while (resources.hasMoreElements()) {
            URL resource = resources.nextElement();
            if (LOGGER.isDebugEnabled()) {
                LOGGER.debug("  resource=" + resource);
            }
            if (resource == null) {
                break;
            }

            InputStream stream = null;
            try {
                stream = resource.openStream();
                if (stream == null) {
                    LOGGER.warn("  stream is null.");
                    break;
                }

                Manifest mf = null;
                try {
                    mf = new Manifest();
                    mf.read(stream);
                    Attributes attributes = mf.getMainAttributes();
                    // Implementation-Vendor-Id: com.le.tools.moneyutils
                    String id = attributes.getValue("Implementation-Vendor-Id");
                    if ((id == null) || (id.length() <= 0)) {
                        continue;
                    }
                    // String implementationVendorId =
                    // "com.le.tools.moneyutils";
                    if (id.compareTo(implementationVendorId) != 0) {
                        continue;
                    }
                    LOGGER.info("FOUND Manifest with id='" + implementationVendorId + "'");
                    String build = attributes.getValue("Implementation-Build");
                    if (build != null) {
                        // GUI.VERSION = build;
                        buildNumber = build;
                        break;
                    } else {
                        if (LOGGER.isDebugEnabled()) {
                            LOGGER.debug(
                                    "Manifest has no value for \"Implementation-Build\", classLoader=" + classLoader);
                            LOGGER.debug("START - Dumping Manifest, resource=" + resource);
                            for (Object key : attributes.keySet()) {
                                Object value = attributes.get(key);
                                LOGGER.debug("    " + key + ": " + value);
                            }
                            LOGGER.debug("END - Dumping Manifest, resource=" + resource);
                        }
                    }
                } catch (IOException e) {
                    LOGGER.warn(e);
                } finally {
                    if (mf != null) {
                        mf = null;
                    }
                }
            } catch (IOException e) {
                LOGGER.warn(e);
            } finally {
                if (stream != null) {
                    try {
                        stream.close();
                    } catch (IOException e) {
                        LOGGER.warn(e);
                    } finally {
                        stream = null;
                    }
                }
            }
        }
        return buildNumber;
    }

    /**
     * Find builder number.
     *
     * @param implementationVendorId the implementation vendor id
     * @param defaultBuildNumber     the default build number
     * @return the string
     */
    public static String findBuilderNumber(String implementationVendorId, String defaultBuildNumber) {
        LOGGER.info("> findBuilderNumber: implementationVendorId=" + implementationVendorId);
        String buildNumber = null;
        String resourceName = "META-INF/MANIFEST.MF";
        ClassLoader classLoader = null;
        if (buildNumber == null) {
            classLoader = Thread.currentThread().getContextClassLoader();
            LOGGER.debug("> findBuilderNumber (contextClassLoader): resourceName=" + resourceName + ", classLoader="
                    + classLoader);
            buildNumber = findBuilderNumber(implementationVendorId, resourceName, classLoader);
        }
        if (buildNumber == null) {
            classLoader = LOGGER.getClass().getClassLoader();
            LOGGER.debug("> findBuilderNumber (logger classLoader): resourceName=" + resourceName + ", classLoader="
                    + classLoader);
            buildNumber = findBuilderNumber(implementationVendorId, resourceName, classLoader);
        }
        if (buildNumber == null) {
            classLoader = ClassLoader.getSystemClassLoader();
            LOGGER.debug("> findBuilderNumber (systemClassLoader): resourceName=" + resourceName + ", classLoader="
                    + classLoader);
            buildNumber = findBuilderNumber(implementationVendorId, resourceName, classLoader);
        }

        if (buildNumber == null) {
            LOGGER.warn("Cannot find buildNumber from Manifest.");
            LOGGER.warn("Using built-in buildNumber which is likely to be wrong!");
            buildNumber = defaultBuildNumber;
        }

        return buildNumber;
    }

    /**
     * Find builder number.
     *
     * @param implementationVendorId the implementation vendor id
     * @return the string
     */
    public static String findBuilderNumber(String implementationVendorId) {
        String defaultBuildNumber = "0.0.0";
        return findBuilderNumber(implementationVendorId, defaultBuildNumber);
    }

}

package com.hungle.sunriise.util;

public interface MnyFilter<T> {

    boolean accepts(T type);

}

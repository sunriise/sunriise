/*******************************************************************************
 * Copyright (c) 2013 Hung Le
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *******************************************************************************/
package com.hungle.sunriise.util;

import org.apache.logging.log4j.Logger;

// TODO: Auto-generated Javadoc
/**
 * The Class JavaInfo.
 */
public class JavaInfo {

    /** The Constant log. */
    private static final Logger LOGGER = org.apache.logging.log4j.LogManager.getLogger(JavaInfo.class);

    /** The Constant names. */
    private static final PropertyName[] names = {
            new JavaInfo.PropertyName("java.home", "The directory in which Java is installed"),
            new JavaInfo.PropertyName("java.class.path", "The value of the CLASSPATH environment variable"),
            new JavaInfo.PropertyName("java.version", "The version of the Java interpreter"), };

    /**
     * The Class PropertyName.
     */
    private static final class PropertyName {

        /** The name. */
        private String name;

        /** The description. */
        private String description;

        /**
         * Instantiates a new property name.
         *
         * @param name        the name
         * @param description the description
         */
        public PropertyName(String name, String description) {
            super();
            setName(name);
            setDescription(description);
        }

        /**
         * Gets the name.
         *
         * @return the name
         */
        public String getName() {
            return name;
        }

        /**
         * Sets the name.
         *
         * @param name the new name
         */
        public void setName(String name) {
            this.name = name;
        }

        /**
         * Gets the description.
         *
         * @return the description
         */
        public String getDescription() {
            return description;
        }

        /**
         * Sets the description.
         *
         * @param description the new description
         */
        public void setDescription(String description) {
            this.description = description;
        }
    }

    /**
     * Log info.
     */
    public static final void logInfo() {
        for (PropertyName name : names) {
            String value = System.getProperty(name.getName());
            LOGGER.info("Java system property - name=" + name.getName() + ", value=" + value + ", desc="
                    + name.getDescription());
        }
    }
}

package com.hungle.sunriise.util;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Date;

public class DateUtils {

    private DateUtils() {
        super();
    }

    public static final LocalDateTime toLocalDateTime(Date date) {
        ZoneId zoneId = ZoneId.systemDefault();
        return date.toInstant().atZone(zoneId).toLocalDateTime();
    }

    public static final DateTimeFormatter getDefaultDateTimeFormatter() {
        return DateTimeFormatter.ISO_DATE_TIME;
    }

}

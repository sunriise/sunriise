/*******************************************************************************
 * Copyright (c) 2010 Hung Le
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *******************************************************************************/
package com.hungle.sunriise.util;

import org.apache.commons.lang3.time.DurationFormatUtils;
import org.apache.logging.log4j.Logger;

// TODO: Auto-generated Javadoc
/**
 * The Class StopWatch.
 */
public class StopWatch {

    /** The Constant log. */
    private static final Logger LOGGER = org.apache.logging.log4j.LogManager.getLogger(StopWatch.class);

    /** The start time. */
    private long startTime;

    /** The end time. */
    private long endTime;

    /**
     * Instantiates a new stop watch.
     */
    public StopWatch() {
        click();
    }

    /**
     * Click.
     *
     * @return the long
     */
    public long click() {
        return click(true);
    }

    /**
     * Click.
     *
     * @param reset the reset
     * @return the long
     */
    public long click(boolean reset) {
        long delta = 0L;

        endTime = System.currentTimeMillis();

        delta = endTime - startTime;

        if (reset) {
            startTime = endTime;
        }
        return delta;
    }

    /**
     * Log timing.
     *
     * @param logger  the use this log
     * @param message the message
     */
    public void logTiming(Logger logger, String message) {
        StopWatch.logTiming(this, logger, message);
    }

    /**
     * Log timing.
     *
     * @param stopWatch the stop watch
     * @param logger    the use this log
     * @param message   the message
     */
    private static final void logTiming(StopWatch stopWatch, Logger logger, String message) {
        long durationMillis = stopWatch.click();
        String str = DurationFormatUtils.formatDurationWords(durationMillis, true, true);
        if (logger == null) {
            logger = LOGGER;
        }
        if (logger.isDebugEnabled()) {
            logger.debug("TIMING - " + "(took " + str + ")" + " ms=" + durationMillis + ", " + message);
        }
    }
}

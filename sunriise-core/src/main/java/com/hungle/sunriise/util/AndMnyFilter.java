package com.hungle.sunriise.util;

import java.util.ArrayList;
import java.util.List;

public class AndMnyFilter<T> implements MnyFilter<T> {
    private List<MnyFilter<T>> filters = new ArrayList<>();

    public List<MnyFilter<T>> addFilter(MnyFilter<T> filter) {
        filters.add(filter);
        return filters;
    }

    @Override
    public boolean accepts(T type) {
        for (MnyFilter<T> filter : filters) {
            if (!filter.accepts(type)) {
                return false;
            }
        }
        return true;
    }

}
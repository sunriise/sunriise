/*******************************************************************************
 * Copyright (c) 2016 Hung Le
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *******************************************************************************/
package com.hungle.sunriise.util;

// TODO: Auto-generated Javadoc
/**
 * The Class SunriiseBuildNumber.
 */
public class SunriiseBuildNumber {

    /** The Constant implementationVendorId. */
    private static final String implementationVendorId = "com.le.sunriise";

    /** The default build number. */
    private static String defaultBuildNumber = "0.0.0";

    /** The Constant buildNumber. */
    private static final String buildNumber = BuildNumber.findBuilderNumber(implementationVendorId, defaultBuildNumber);

    /**
     * Gets the buildnumber.
     *
     * @return the buildnumber
     */
    public static String getBuildnumber() {
        return buildNumber;
    }
}

package com.hungle.sunriise.util;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import com.healthmarketscience.jackcess.Table;
import com.hungle.sunriise.diskusage.GetDiskUsageCmd.GetDiskUsageCollector;

public final class DefaultGetDiskUsageCollector implements GetDiskUsageCollector {
    private long bytesCount;

    private final List<Table> tables = new ArrayList<>();

    private Set<String> systemTableNames;

    private Set<String> tableNames;

    @Override
    public void collectTable(Table table) {
        tables.add(table);
    }

    @Override
    public void collectBytesCount(File inFile, long bytesCount) {
        // TODO Auto-generated method stub
        this.bytesCount = bytesCount;
    }

    @Override
    public void collectSystemTableNames(Set<String> systemTableNames) {
        this.systemTableNames = systemTableNames;
    }

    @Override
    public void collectTableNames(Set<String> tableNames) {
        this.tableNames = tableNames;
    }

    public long getBytesCount() {
        return bytesCount;
    }

    public void setBytesCount(long bytesCount) {
        this.bytesCount = bytesCount;
    }

    public Set<String> getSystemTableNames() {
        return systemTableNames;
    }

    public void setSystemTableNames(Set<String> systemTableNames) {
        this.systemTableNames = systemTableNames;
    }

    public Set<String> getTableNames() {
        return tableNames;
    }

    public void setTableNames(Set<String> tableNames) {
        this.tableNames = tableNames;
    }

    public List<Table> getTables() {
        return tables;
    }
}
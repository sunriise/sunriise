/*******************************************************************************
 * Copyright (c) 2016 Hung Le
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *******************************************************************************/
package com.hungle.sunriise.util;

import java.io.IOException;
import java.util.Map;
import java.util.stream.Stream;

import org.apache.logging.log4j.Logger;

import com.healthmarketscience.jackcess.Database;
import com.hungle.sunriise.dbutil.TableAccountUtils;
import com.hungle.sunriise.dbutil.TableCategoryUtils;
import com.hungle.sunriise.dbutil.TableCurrencyUtils;
import com.hungle.sunriise.dbutil.TablePayeeUtils;
import com.hungle.sunriise.dbutil.TableSecurityUtils;
import com.hungle.sunriise.io.MnyDb;
import com.hungle.sunriise.mnyobject.Account;
import com.hungle.sunriise.mnyobject.Category;
import com.hungle.sunriise.mnyobject.Currency;
import com.hungle.sunriise.mnyobject.MnyContext;
import com.hungle.sunriise.mnyobject.Payee;
import com.hungle.sunriise.mnyobject.Security;

// TODO: Auto-generated Javadoc
/**
 * The Class MnyContextUtil.
 */
public class MnyContextUtils {
    private static final Logger LOGGER = org.apache.logging.log4j.LogManager.getLogger(MnyContextUtils.class);

    /**
     * Creates the mny context.
     *
     * @param mnyDb the opened db
     * @return the mny context
     * @throws IOException Signals that an I/O exception has occurred.
     */
    public static MnyContext createMnyContext(MnyDb mnyDb) throws IOException {
        MnyContext mnyContext = new MnyContext();
        MnyContextUtils.initMnyContext(mnyDb, mnyContext);
        return mnyContext;
    }

    /**
     * Inits the mny context.
     *
     * @param mnyDb      the opened db
     * @param mnyContext the mny context
     * @return the mny context
     * @throws IOException Signals that an I/O exception has occurred.
     */
    public static MnyContext initMnyContext(MnyDb mnyDb, MnyContext mnyContext) throws IOException {
        Database db = mnyDb.getDb();
        mnyContext.setDb(db);

        Map<Integer, Payee> payees = TablePayeeUtils.getMap(db);
        mnyContext.setPayees(payees);

        Map<Integer, Category> categories = TableCategoryUtils.getMap(db);
        mnyContext.setCategories(categories);
        for (Category category : categories.values()) {
            String fullName = TableCategoryUtils.getCategoryFullName(category.getId(), categories);
            category.setFullName(fullName);

            boolean expense = TableCategoryUtils.isExpense(category.getId(), categories);
            category.setExpense(expense);
            if (LOGGER.isDebugEnabled()) {
                LOGGER.debug("id={}, name={}, fullName={}, expense={}", category.getId(), category.getName(),
                        category.getFullName(), category.getExpense());
            }
        }

        Map<Integer, Currency> currencies = TableCurrencyUtils.getMap(db);
        mnyContext.setCurrencies(currencies);

        Map<Integer, Security> securities = TableSecurityUtils.getMap(db);
        mnyContext.setSecurities(securities);

        Map<Integer, Account> accounts = TableAccountUtils.getMap(db);
        mnyContext.setAccounts(accounts);

        MnyContextUtils.setCurrencyCode(accounts, currencies);

        return mnyContext;
    }

    /**
     * If account has currencyId, look up and set the currency code.
     * 
     * @param accounts   the accounts
     * @param currencies the currencies
     */
    private static void setCurrencyCode(Map<Integer, Account> accounts, Map<Integer, Currency> currencies) {
        for (Account account : accounts.values()) {
            setCurrencyCode(account, currencies);
        }
    }

    public static void setCurrencyCode(Account account, Map<Integer, Currency> currencies) {
        Integer currencyId = account.getCurrency().getId();
        String currencyCode = MnyContextUtils.lookupCurrencyCode(currencyId, currencies);
        if (currencyCode != null) {
            account.getCurrency().setIsoCode(currencyCode);
        }
    }

    /**
     * Lookup currency code.
     *
     * @param currencyId the currency id
     * @param currencies the currencies
     * @return the string
     */
    private static final String lookupCurrencyCode(Integer currencyId, Map<Integer, Currency> currencies) {
        String currencyCode = null;

        if (currencyId == null) {
            return null;
        }

        Currency currency = currencies.get(currencyId);
        if (currency == null) {
            return null;
        }
        currencyCode = currency.getIsoCode();

        return currencyCode;
    }

    public static Stream<Account> getAccountsStream(MnyContext mnyContext) {
        Map<Integer, Account> accounts = mnyContext.getAccounts();

        return accounts.values().stream();
    }
}

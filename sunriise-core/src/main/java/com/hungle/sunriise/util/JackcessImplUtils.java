/*******************************************************************************
 * Copyright (c) 2016 Hung Le
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *******************************************************************************/
package com.hungle.sunriise.util;

import java.io.IOException;
import java.util.List;

import com.healthmarketscience.jackcess.Database;
import com.healthmarketscience.jackcess.Index;
import com.healthmarketscience.jackcess.Table;
import com.healthmarketscience.jackcess.impl.ColumnImpl;
import com.healthmarketscience.jackcess.impl.DatabaseImpl;
import com.healthmarketscience.jackcess.impl.IndexData;
import com.healthmarketscience.jackcess.impl.IndexData.ColumnDescriptor;
import com.healthmarketscience.jackcess.impl.IndexImpl;
import com.healthmarketscience.jackcess.impl.IndexImpl.ForeignKeyReference;
import com.healthmarketscience.jackcess.impl.JetFormat;
import com.healthmarketscience.jackcess.impl.PageChannel;
import com.healthmarketscience.jackcess.impl.TableImpl;

// TODO: Auto-generated Javadoc
/**
 * The Class JackcessImplUtil.
 */
public class JackcessImplUtils {

    /**
     * Gets the columns.
     *
     * @param index the index
     * @return the columns
     */
    public static final List<ColumnDescriptor> getColumns(Index index) {
        return ((IndexImpl) index).getColumns();
    }

    /**
     * Gets the reference.
     *
     * @param index the index
     * @return the reference
     */
    public static final ForeignKeyReference getReference(Index index) {
        return ((IndexImpl) index).getReference();
    }

    /**
     * Gets the columns.
     *
     * @param table the table
     * @return the columns
     */
    public static final List<ColumnImpl> getColumns(Table table) {
        return ((TableImpl) table).getColumns();
    }

    /**
     * Gets the default code page.
     *
     * @param db the db
     * @return the default code page
     * @throws IOException Signals that an I/O exception has occurred.
     */
    public static final short getDefaultCodePage(Database db) throws IOException {
        return ((DatabaseImpl) db).getDefaultCodePage();
    }

    /**
     * Gets the system catalog.
     *
     * @param db the db
     * @return the system catalog
     */
    public static final Table getSystemCatalog(Database db) {
        return ((DatabaseImpl) db).getSystemCatalog();
    }

    /**
     * Gets the index data.
     *
     * @param index the index
     * @return the index data
     */
    public static final IndexData getIndexData(Index index) {
        return ((IndexImpl) index).getIndexData();
    }

    /**
     * Gets the unique entry count.
     *
     * @param index the index
     * @return the unique entry count
     */
    public static final int getUniqueEntryCount(Index index) {
        return ((IndexImpl) index).getUniqueEntryCount();
    }

    /**
     * Gets the approximate owned page count.
     *
     * @param table the table
     * @return the approximate owned page count
     */
    public static final int getApproximateOwnedPageCount(Table table) {
        return ((TableImpl) table).getApproximateOwnedPageCount();
    }

    /**
     * Gets the page channel.
     *
     * @param db the db
     * @return the page channel
     */
    public static final PageChannel getPageChannel(Database db) {
        return ((DatabaseImpl) db).getPageChannel();
    }

    /**
     * Gets the format.
     *
     * @param table the table
     * @return the format
     */
    public static JetFormat getFormat(Table table) {
        return ((TableImpl) table).getFormat();
    }
}

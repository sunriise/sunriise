/*******************************************************************************
 * Copyright (c) 2016 Hung Le
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *******************************************************************************/
package com.hungle.sunriise.util;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.Logger;

import com.healthmarketscience.jackcess.Database;
import com.hungle.sunriise.dbutil.TableSecurityUtils;
import com.hungle.sunriise.mnyobject.Account;
import com.hungle.sunriise.mnyobject.EnumAccountType;
import com.hungle.sunriise.mnyobject.InvestmentInfo;
import com.hungle.sunriise.mnyobject.InvestmentTransaction;
import com.hungle.sunriise.mnyobject.MnyContext;
import com.hungle.sunriise.mnyobject.Security;
import com.hungle.sunriise.mnyobject.SecurityHolding;
import com.hungle.sunriise.mnyobject.Transaction;
import com.hungle.sunriise.mnyobject.impl.DefaultSecurityHolding;
import com.hungle.sunriise.mnyobject.impl.InvestmentActivity;
import com.hungle.sunriise.prices.OldGetLatestSecurityPricesUtils;

// TODO: Auto-generated Javadoc
/**
 * The Class BalanceUtil.
 */
public class BalanceUtils {

    /** The Constant log. */
    private static final Logger LOGGER = org.apache.logging.log4j.LogManager.getLogger(BalanceUtils.class);

    /**
     * Calculate current balance.
     *
     * @param account the related to account
     * @param date    the date
     */
    // public static void calculateNonInvestmentBalance(Account account) {
    // Date date = null;
    // BalanceUtil.calculateNonInvestmentBalance(account, date);
    // }

    /**
     * Calculate balance.
     *
     * @param account    the account
     * @param date       the date
     * @param mnyContext the mny context
     */
    public static void calculateAndSetBalance(Account account, Date date, MnyContext mnyContext) {
        EnumAccountType accountType = account.getAccountType();
        switch (accountType) {
        case INVESTMENT:
            BalanceUtils.calculateAndSetInvestmentBalance(account, date, mnyContext);
            break;
        default:
            BalanceUtils.calculateAndSetNonInvestmentBalance(account, date);
            break;
        }
    }

    static void calculateAndSetBalance(Account account, MnyContext mnyContext) {
        Date date = null;
        BalanceUtils.calculateAndSetBalance(account, date, mnyContext);
    }

    static void calculateAndSetNonInvestmentBalance(Account account) {
        Date date = null;
        BalanceUtils.calculateAndSetNonInvestmentBalance(account, date);
    }

    /**
     * Calculate current balance of an account. Will set account's currentBalance.
     *
     * @param account the account
     * @param date    if non-null, include only transactions up to this date.
     */
    static void calculateAndSetNonInvestmentBalance(Account account, Date date) {
        BigDecimal currentBalance = calculateNonInvestmentBalance(account, date);
        account.setCurrentBalance(currentBalance);
    }

    private static BigDecimal calculateNonInvestmentBalance(Account account, Date date) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.info("> calculateNonInvestmentBalance, account.name={}", account.getName());
        }

        BigDecimal currentBalance = account.getStartingBalance();
        if (LOGGER.isDebugEnabled()) {
            LOGGER.info("  transactions.startingBalance={}", currentBalance);
        }
        if (currentBalance == null) {
            LOGGER.warn("Starting balance is null. Set to 0. Account's id=" + account.getId());
            currentBalance = BigDecimal.ZERO;
        }

        List<Transaction> transactions = account.getTransactions();
        if (LOGGER.isDebugEnabled()) {
            LOGGER.info("  transactions.count={}", transactions.size());
        }
        for (Transaction transaction : transactions) {
            if (BalanceUtils.isNonBalanceTransaction(transaction)) {
                transaction.setRunningBalance(currentBalance);
                continue;
            }

            if (date != null) {
                Date transactionDate = transaction.getDate();
                if (transactionDate.compareTo(date) > 0) {
                    continue;
                }
            }

            BigDecimal amount = transaction.getAmount();
            if (amount != null) {
                currentBalance = currentBalance.add(amount);
            } else {
                LOGGER.warn("Transaction with no amount, id=" + transaction.getId());
            }
            transaction.setRunningBalance(currentBalance);
        }

        // round off to cents
//        currentBalance = currentBalance.setScale(2, BigDecimal.ROUND_HALF_EVEN);
        currentBalance = BalanceUtils.formatBalance(currentBalance);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.info("< calculateNonInvestmentBalance, account.currentBalance={}", account.getCurrentBalance());
        }
        return currentBalance;
    }

    /**
     * Checks if a transaction is a non balance transaction: does not contribute to
     * the account balance.
     *
     * @param transaction the transaction
     * @return true, if is non balance transaction
     */
    public static boolean isNonBalanceTransaction(Transaction transaction) {
        if (transaction.isVoid()) {
            return true;
        }
        if (transaction.isRecurring()) {
            return true;
        }
        return false;
    }

    /**
     * Calculate investment balance.
     *
     * @param account    the account
     * @param date       the date
     * @param mnyContext the mny context
     */
    // public static void calculateInvestmentBalance(Account account, MnyContext
    // mnyContext) {
    // Date date = null;
    // BalanceUtil.calculateInvestmentBalance(account, date, mnyContext);
    // }

    /**
     * Calculate investment balance. Will update the account's securityHolding and
     * currentBalance.
     *
     * @param account    the account
     * @param date       the date
     * @param mnyContext the mny context
     */
    private static void calculateAndSetInvestmentBalance(Account account, Date date, MnyContext mnyContext) {
        BigDecimal marketValue = BalanceUtils.calculateInvestmentBalance(account, date, mnyContext);

        account.setCurrentBalance(marketValue);
    }

    private static BigDecimal calculateInvestmentBalance(Account account, Date date, MnyContext mnyContext) {
        List<SecurityHolding> securityHoldings = BalanceUtils.collectSecurityHolding(account, date, mnyContext);
        account.setSecurityHoldings(securityHoldings);

        BigDecimal marketValue = account.getStartingBalance();
        if (marketValue == null) {
            LOGGER.warn("Starting balance is null. Set to 0. Account's id=" + account.getId());
            marketValue = BigDecimal.ZERO;
        }

        for (SecurityHolding securityHolding : securityHoldings) {
            marketValue = marketValue.add(securityHolding.getMarketValue());
        }

        try {
            BigDecimal cashAccountValue = BalanceUtils.getCashAccountValue(account, date);
            if (LOGGER.isDebugEnabled()) {
                LOGGER.debug("cashAccountValue=" + cashAccountValue);
            }
            if (cashAccountValue != null) {
                marketValue = marketValue.add(cashAccountValue);
            }
        } catch (IOException e) {
            LOGGER.warn(e);
        }

        // round off to cents
//        marketValue = marketValue.setScale(2, BigDecimal.ROUND_HALF_EVEN);
        marketValue = BalanceUtils.formatBalance(marketValue);
        return marketValue;
    }

    /**
     * Collect security holding.
     *
     * @param account    the account
     * @param date       the date
     * @param mnyContext the mny context
     * @return the list
     */
    private static List<SecurityHolding> collectSecurityHolding(Account account, Date date, MnyContext mnyContext) {
        // get table of securityId, quantities
        Map<Integer, Double> quantities = BalanceUtils.collectInvestmentQuantity(account, date);

        boolean skipRealySmallHoldingValue = false;
        List<SecurityHolding> securityHoldings = new ArrayList<SecurityHolding>();

        for (Integer securityId : quantities.keySet()) {
            if (LOGGER.isDebugEnabled()) {
                LOGGER.debug("securityId=" + securityId);
            }

            Double quantity = quantities.get(securityId);
            if (skipRealySmallHoldingValue) {
                // TODO: skip really small holding value
                if (quantity < 0.00000001) {
                    LOGGER.warn("SKIP security with very small holding, id=" + securityId + ", quantity="
                            + String.format("%.10f", quantity));
                    continue;
                }
            }

            // populate security holding
            SecurityHolding securityHolding = new DefaultSecurityHolding();
            // SET quantity
            securityHolding.setQuantity(quantity);

            Map<Integer, Security> securities = mnyContext.getSecurities();
            Security security = securities.get(securityId);
            // SET security
            if (security == null) {
                LOGGER.warn("Cannot find Security object for securityId=" + securityId);
            } else {
                securityHolding.setSecurity(security);
            }

            try {
                Database db = mnyContext.getDb();
                Double price = OldGetLatestSecurityPricesUtils.getSecurityLatestPrice(securityId, date, db);
                if (price == null) {
                    price = new Double(0.0);
                    if (quantity > 0.00000001) {
                        if (LOGGER.isDebugEnabled()) {
                            LOGGER.warn("Set price=" + price + ". Cannot find latest price for securityId=" + securityId
                                    + ", date=" + date + ", quantity=" + String.format("%.10f", quantity));
                        }
                    }
                }
                // SET price
                securityHolding.setPrice(new BigDecimal(price));
            } catch (IOException e) {
                LOGGER.warn("Cannot find latest price for securityId=" + securityId, e);
            }

            BigDecimal price = securityHolding.getPrice();
            BigDecimal marketValue = new BigDecimal(price.doubleValue() * securityHolding.getQuantity());
//            marketValue = marketValue.setScale(2, BigDecimal.ROUND_HALF_EVEN);
            marketValue = BalanceUtils.formatBalance(marketValue);
            // SET market value
            securityHolding.setMarketValue(marketValue);

            // update result list
            securityHoldings.add(securityHolding);
        }

        boolean sortBySecurityName = true;
        if (sortBySecurityName) {
            Collections.sort(securityHoldings, new TableSecurityUtils.CompareSecurityHoldingsByName());
        }

        return securityHoldings;
    }

    /**
     * Iterate through the list of account's transactions. For each investment
     * transaction, collect the total quantities of each security.
     *
     * @param account the account
     * @param date    the date
     * @return the map
     */
    private static Map<Integer, Double> collectInvestmentQuantity(Account account, Date date) {
        // table of securityId, quantities

        List<Transaction> transactions = account.getTransactions();
        return collectInvestmentQuantity(transactions, date);
    }

    public static Map<Integer, Double> collectInvestmentQuantity(Collection<Transaction> transactions, Date date) {
        if (transactions == null) {
            transactions = new ArrayList<>();
        }

        Map<Integer, Double> quantities = new HashMap<Integer, Double>();
        for (Transaction transaction : transactions) {
            // Skip transactions that we don't need
            if (isNonBalanceTransaction(transaction)) {
                continue;
            }

            if (!transaction.isInvestment()) {
                continue;
            }

            // Skip transaction not within user-specified date range
            if (date != null) {
                Date transactionDate = transaction.getDate();
                if (transactionDate.compareTo(date) > 0) {
                    continue;
                }
            }

            InvestmentInfo investmentInfo = transaction.getInvestmentInfo();
            InvestmentTransaction investmentTransaction = investmentInfo.getTransaction();
            if (investmentTransaction == null) {
                LOGGER.warn("Transaction is an investment transaction but investmentTransaction is null");
                continue;
            }

            Integer securityId = investmentInfo.getSecurity().getId();
            InvestmentActivity investmentActivity = investmentInfo.getActivity();
            Double runningQuantity = quantities.get(securityId);
            if (runningQuantity == null) {
                runningQuantity = new Double(0.0);
                quantities.put(securityId, runningQuantity);
            }

            Double quantity = investmentTransaction.getQuantity();
            if (quantity == null) {
                quantity = new Double(0.0);
            }

            if (investmentActivity.isAdded()) {
                runningQuantity += quantity;
            } else {
                runningQuantity -= quantity;
            }

            quantities.put(securityId, runningQuantity);
        }

        return quantities;
    }

    public static Map<Integer, Double> collectInvestmentQuantity(Collection<Transaction> transactions) {
        Date date = null;
        return BalanceUtils.collectInvestmentQuantity(transactions, date);
    }

    /**
     * Gets the cash account value.
     *
     * @param account the account
     * @param date    the date
     * @return the cash account value
     * @throws IOException Signals that an I/O exception has occurred.
     */
    private static BigDecimal getCashAccountValue(Account account, Date date) throws IOException {
        BigDecimal cashAccountValue = null;

        Account cashAccount = account.getRelatedToAccount();
        if ((cashAccount == null) || (cashAccount.getName() == null)) {
            // // not yet initialized
            // Database db = mnyContext.getDb();
            // relatedToAccount = TableAccountUtil.getRelatedToAccount(db,
            // relatedToAccount.getId());
            // account.setRelatedToAccount(relatedToAccount);
            // TableAccountUtil.getTransactions(db, relatedToAccount);
            LOGGER.warn("getCashAccountValue - relatedToAccount is null." + " account=" + account.getName());
        } else {
            calculateAndSetNonInvestmentBalance(cashAccount, date);
            BigDecimal currentBalance = cashAccount.getCurrentBalance();
            if (currentBalance != null) {
                cashAccountValue = currentBalance;
            } else {
                cashAccountValue = BigDecimal.ZERO;
            }
        }

        return cashAccountValue;
    }

    public static BigDecimal formatBalance(BigDecimal value) {
        return value.setScale(2, BigDecimal.ROUND_HALF_EVEN);
    }

}

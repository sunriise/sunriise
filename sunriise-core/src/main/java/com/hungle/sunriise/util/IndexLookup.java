/*******************************************************************************
 * Copyright (c) 2010 Hung Le
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *******************************************************************************/
package com.hungle.sunriise.util;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.Logger;

import com.healthmarketscience.jackcess.Column;
import com.healthmarketscience.jackcess.Cursor;
import com.healthmarketscience.jackcess.CursorBuilder;
import com.healthmarketscience.jackcess.Database;
import com.healthmarketscience.jackcess.Index;
import com.healthmarketscience.jackcess.Table;
import com.healthmarketscience.jackcess.impl.ByteUtil;
import com.healthmarketscience.jackcess.impl.IndexData;
import com.healthmarketscience.jackcess.impl.IndexData.ColumnDescriptor;

// TODO: Auto-generated Javadoc
/**
 * The Class IndexLookup.
 */
public class IndexLookup {

    /** The Constant log. */
    private static final Logger LOGGER = org.apache.logging.log4j.LogManager.getLogger(IndexLookup.class);

    /**
     * The Class ColumnNameComparator.
     */
    private class ColumnNameComparator implements Comparator<Column> {

        /*
         * (non-Javadoc)
         * 
         * @see java.util.Comparator#compare(java.lang.Object, java.lang.Object)
         */
        @Override
        public int compare(Column o1, Column o2) {
            String o1Name = o1.getTable().getName() + "." + o1.getName();
            String o2Name = o2.getTable().getName() + "." + o2.getName();
            return ComparatorUtils.compareNullOrder(o1Name, o2Name);
//            return o1Name.compareTo(o2Name);
        }
    }

    /** The column name comparator. */
    private ColumnNameComparator columnNameComparator = new ColumnNameComparator();

    /**
     * Checks if is primary key column.
     *
     * @param column the column
     * @return true, if is primary key column
     */
    public boolean isPrimaryKeyColumn(Column column) {
        Table table = column.getTable();
        for (Index index : table.getIndexes()) {
            if (index.isPrimaryKey()) {
                for (IndexData.ColumnDescriptor col : JackcessImplUtils.getColumns(index)) {
                    if (columnNameComparator.compare(col.getColumn(), column) == 0) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    /**
     * Gets the referencing.
     *
     * @param column the column
     * @return the referencing
     * @throws IOException Signals that an I/O exception has occurred.
     */
    public List<Column> getReferencing(Column column) throws IOException {
        List<Column> columns = new ArrayList<Column>();
        Table table = column.getTable();
        for (Index index : table.getIndexes()) {
            if (index.isForeignKey() && JackcessImplUtils.getReference(index).isPrimaryTable()) {
                if (index.getColumns().size() != 1) {
                    LOGGER.warn("ForeignKey index must have exactly 1 column, index=" + index.getName());
                    continue;
                }
                if (indexHasColumn(index, column)) {
                    // load index/table from referenced table
                    Index otherIndex = index.getReferencedIndex();
                    if (otherIndex.getColumns().size() != 1) {
                        LOGGER.warn("ForeignKey index must have exactly 1 column, index=" + otherIndex.getName());
                        continue;
                    }
                    ColumnDescriptor columnDescriptor = JackcessImplUtils.getColumns(otherIndex).get(0);
                    columns.add(columnDescriptor.getColumn());
                    // Table otherTable = otherIndex.getTable();
                }
            }
        }
        return columns;
    }

    /**
     * Index has column.
     *
     * @param index  the index
     * @param column the column
     * @return true, if successful
     */
    private boolean indexHasColumn(Index index, Column column) {
        for (ColumnDescriptor col : JackcessImplUtils.getColumns(index)) {
            if (col.getName().equals(column.getName())) {
                return true;
            }
        }
        return false;
    }

    /**
     * Gets the referenced columns.
     *
     * @param column the column
     * @return the referenced columns
     * @throws IOException Signals that an I/O exception has occurred.
     */
    public List<Column> getReferencedColumns(Column column) throws IOException {
        List<Column> columns = new ArrayList<Column>();
        Database db = column.getDatabase();
        for (String tableName : db.getTableNames()) {
            Table table = db.getTable(tableName);
            if (table == null) {
                LOGGER.warn("Cannot find table=" + tableName);
                continue;
            }
            findReferencedColumns(table, column, columns);
        }

        return columns;
    }

    /**
     * Find referenced columns.
     *
     * @param table   the table
     * @param column  the column
     * @param columns the columns
     * @throws IOException Signals that an I/O exception has occurred.
     */
    private void findReferencedColumns(Table table, Column column, List<Column> columns) throws IOException {
        // Table table = column.getTable();
        for (Index index : table.getIndexes()) {
            if (index.isForeignKey() && JackcessImplUtils.getReference(index).isPrimaryTable()) {
                if (index.getColumns().size() != 1) {
                    LOGGER.warn("ForeignKey index must have exactly 1 column, index=" + index.getName());
                    continue;
                }
                // load index/table from referenced table
                Index otherIndex = index.getReferencedIndex();
                if (otherIndex.getColumns().size() != 1) {
                    LOGGER.warn("ForeignKey index must have exactly 1 column, index=" + otherIndex.getName());
                    continue;
                }
                ColumnDescriptor columnDescriptor = JackcessImplUtils.getColumns(otherIndex).get(0);
                if (columnNameComparator.compare(columnDescriptor.getColumn(), column) == 0) {
                    columns.add(index.getColumns().get(0).getColumn());
                }
                // Table otherTable = otherIndex.getTable();
            }
        }
    }

    /**
     * Gets the max.
     *
     * @param column the column
     * @return the max
     * @throws IOException Signals that an I/O exception has occurred.
     */
    public Long getMax(Column column) throws IOException {
        Long max = null;

        Cursor cursor = CursorBuilder.createCursor(column.getTable());
        Collection<String> columnNames = new ArrayList<String>();
        columnNames.add(column.getName());
        Map<String, Object> row = null;
        while ((row = cursor.getNextRow(columnNames)) != null) {
            Object[] rowData = row.values().toArray();
            if (rowData == null) {
                continue;
            }
            if (rowData.length != 1) {
                continue;
            }
            Object obj = rowData[0];
            if (obj == null) {
                continue;
            }
            String value = null;
            if (obj instanceof byte[]) {
                value = ByteUtil.toHexString((byte[]) obj);
            } else {
                value = String.valueOf(obj);
            }
            Long currentValue = null;
            try {
                currentValue = Long.valueOf(value);
            } catch (NumberFormatException e) {
                if (LOGGER.isDebugEnabled()) {
                    LOGGER.warn(e);
                }
                continue;
            }
            if (currentValue == null) {
                continue;
            }
            if (max == null) {
                max = currentValue;
            } else {
                max = Math.max(max, currentValue);
            }
        }

        return max;
    }
}

package com.hungle.sunriise.util;

import java.io.File;

import com.healthmarketscience.jackcess.Table;

public interface DiskUsageCollector {

    void collectFileStat(File dbFile);

    void collectTableStat(Table table, int tableByteCount);

    void collectSummary(int tableCount, long runningBytes);

}
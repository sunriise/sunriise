package com.hungle.sunriise.prices;

import java.io.File;
import java.io.IOException;

import org.apache.logging.log4j.Logger;

import com.hungle.sunriise.io.FileUtils;
import com.hungle.sunriise.io.sample.MnySampleFile;

// TODO: Auto-generated Javadoc
/**
 * The Class DbInfo.
 */
public final class DbInfo {

    public static final String SUNSET_SAMPLE = "+sunsetSample";

    /** The Constant LOGGER. */
    private static final Logger LOGGER = org.apache.logging.log4j.LogManager.getLogger(DbInfo.class);

    /** The db file. */
    private File dbFile;

    /** The password. */
    private String password;

    /**
     * Gets the db file.
     *
     * @return the db file
     */
    public File getDbFile() {
        return dbFile;
    }

    /**
     * Sets the db file.
     *
     * @param dbFile the new db file
     */
    public void setDbFile(File dbFile) {
        this.dbFile = dbFile;
    }

    /**
     * Gets the password.
     *
     * @return the password
     */
    public String getPassword() {
        return password;
    }

    /**
     * Sets the password.
     *
     * @param password the new password
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * Gets the db file.
     *
     * @param fileName the file name
     * @return the db file
     */
    public static DbInfo getDbFile(String fileName) {
        DbInfo dbInfo = new DbInfo();
        try {
            if (fileName.compareToIgnoreCase(SUNSET_SAMPLE) == 0) {
                dbInfo.setDbFile(FileUtils.getSunsetSampleFile());
                dbInfo.setPassword(MnySampleFile.SUNSET_SAMPLE_PWD_MNY_PASSWORD);
            } else {
                dbInfo.setDbFile(new File(fileName));
                dbInfo.setPassword(null);
            }
        } catch (IOException e) {
            LOGGER.warn(e);
        }
        return dbInfo;
    }
}
package com.hungle.sunriise.prices;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.Logger;

import com.healthmarketscience.jackcess.CursorBuilder;
import com.healthmarketscience.jackcess.Database;
import com.healthmarketscience.jackcess.IndexCursor;
import com.healthmarketscience.jackcess.Row;
import com.healthmarketscience.jackcess.Table;
import com.healthmarketscience.jackcess.util.EntryIterableBuilder;
import com.hungle.sunriise.dbutil.TableSecurityPriceUtils;
import com.hungle.sunriise.dbutil.TableSecurityUtils;
import com.hungle.sunriise.dbutil.TableTransactionUtils;
import com.hungle.sunriise.io.FileUtils;
import com.hungle.sunriise.io.MnyDb;
import com.hungle.sunriise.mnyobject.Security;
import com.hungle.sunriise.mnyobject.SecurityPrice;
import com.hungle.sunriise.mnyobject.Transaction;
import com.hungle.sunriise.util.BalanceUtils;

public class GetLatestSecurityPrices {

    private static final Logger LOGGER = org.apache.logging.log4j.LogManager.getLogger(GetLatestSecurityPrices.class);

    private static final String INDEX_NAME = "HsecDateSrcSp";

    private IndexCursor cursor;

    // private String[] headers = DEFAULT_HEADERS;

    public static final class Result {
        private Security security;
        private SecurityPrice price;
        private Double quantities;

        public Security getSecurity() {
            return security;
        }

        public void setSecurity(Security security) {
            this.security = security;
        }

        public SecurityPrice getPrice() {
            return price;
        }

        public void setPrice(SecurityPrice price) {
            this.price = price;
        }

        public Double getQuantities() {
            return quantities;
        }

        public void setQuantities(Double quantities) {
            this.quantities = quantities;
        }
    }

    public GetLatestSecurityPrices(Database db) throws IOException {
        super();
        Table table = TableSecurityPriceUtils.getTable(db);
        final String indexName = INDEX_NAME;
        cursor = CursorBuilder.createCursor(table.getIndex(indexName));
    }

    public SecurityPrice getPrice(Integer securityId) {
        SecurityPrice securityPrice = GetLatestSecurityPrices.getLastestSecurityPrice(securityId, cursor);
        return securityPrice;
    }

    public static final SecurityPrice getLastestSecurityPrice(Integer securityId, IndexCursor cursor) {
        SecurityPrice securityPrice = null;
        Date lastDate = null;
        cursor.beforeFirst();
        EntryIterableBuilder rows = cursor.newEntryIterable(securityId);
        for (Row row : rows) {
            securityPrice = TableSecurityPriceUtils.deserialize(row);
            Date date = securityPrice.getDate();
            lastDate = date;
        }
        return securityPrice;
    }

    public static final List<Result> getLatestSecurityPrices(MnyDb mnyDb) throws IOException {
        List<Result> results = new ArrayList<Result>();

        Database db = mnyDb.getDb();

        Map<Integer, Transaction> transactions = TableTransactionUtils.getTransactions(db);
        Map<Integer, Double> investmentQuantities = BalanceUtils.collectInvestmentQuantity(transactions.values());

        Table table = TableSecurityPriceUtils.getTable(db);
        final String indexName = INDEX_NAME;
        IndexCursor cursor = CursorBuilder.createCursor(table.getIndex(indexName));

        Map<Integer, Security> securities = TableSecurityUtils.getMap(db);
        List<Security> list = TableSecurityUtils.createSortedListByName(securities);
        for (Security security : list) {
            SecurityPrice secPrice = GetLatestSecurityPrices.getLastestSecurityPrice(security.getId(), cursor);
            if (LOGGER.isDebugEnabled()) {
                LOGGER.debug(security.getSymbol() + ", " + security.getName() + ", " + secPrice);
            }
//            if (secPrice != null) {
            Result result = new Result();
            result.setSecurity(security);
            result.setPrice(secPrice);
            result.setQuantities(investmentQuantities.get(security.getId()));

            results.add(result);
//            }
        }

        return results;
    }

    public static void main(String[] args) {
        File dbFile = null;

        String password = null;

        File outFile = null;

        String arg = null;
        if (args.length == 2) {
            arg = args[0];
            DbInfo dbInfo = DbInfo.getDbFile(arg);
            dbFile = dbInfo.getDbFile();

            password = dbInfo.getPassword();

            arg = args[1];
            outFile = new File(arg);
        } else if (args.length == 3) {
            arg = args[0];
            DbInfo dbInfo = DbInfo.getDbFile(arg);
            dbFile = dbInfo.getDbFile();

            arg = args[1];
            password = arg;

            arg = args[2];
            outFile = new File(arg);
        } else {
            Class<GetLatestSecurityPrices> clz = GetLatestSecurityPrices.class;
            System.out.println("Usage: java " + clz.getName() + " file.mny [password] prices.csv");
            FileUtils.printArgs(args);

            System.exit(1);
        }

        MnyDb mnyDb = null;
        try {
            mnyDb = new MnyDb(dbFile, password);

            // GetLatestSecurityPrices getter = new GetLatestSecurityPrices();
            List<Result> results = GetLatestSecurityPrices.getLatestSecurityPrices(mnyDb);

            GetLatestSecurityPricesUtils.writeResults(GetLatestSecurityPricesUtils.DEFAULT_HEADERS, results, outFile);
            LOGGER.info("Saved output to file=" + outFile.getAbsolutePath());
        } catch (IOException e) {
            LOGGER.error(e);
        } finally {
            if (mnyDb != null) {
                try {
                    mnyDb.close();
                    mnyDb = null;
                } catch (IOException e) {
                    LOGGER.warn(e);
                }
            }
        }
    }

}

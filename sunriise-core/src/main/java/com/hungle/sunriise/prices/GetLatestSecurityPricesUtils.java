package com.hungle.sunriise.prices;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.Logger;

import com.healthmarketscience.jackcess.Database;
import com.hungle.sunriise.dbutil.TableSecurityUtils;
import com.hungle.sunriise.mnyobject.EnumSecurityPriceSrc;
import com.hungle.sunriise.mnyobject.Security;
import com.hungle.sunriise.mnyobject.SecurityPrice;

public class GetLatestSecurityPricesUtils {
    private static final Logger LOGGER = org.apache.logging.log4j.LogManager
            .getLogger(GetLatestSecurityPricesUtils.class);

    static final String[] DEFAULT_HEADERS = { "symbol", "name", "price", "date (yyyy-MM-dd)", "time (HH:mm:ss)",
            "tz offset", "readable date", "type" };

    private static final DecimalFormat priceFormat = new DecimalFormat("0.0000");
    private static final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
    private static final SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm:ss");
    private static final SimpleDateFormat timeZoneFormat = new SimpleDateFormat("Z");
    private static final SimpleDateFormat fullDateFormat = new SimpleDateFormat("EEE, d MMM yyyy HH:mm:ss z");

    private static final String formatPrice(Double price) {
        if (price == null) {
            return "";
        }

        return priceFormat.format(price);
    }

    private static final String formatDate(Date date) {
        if (date == null) {
            return "";
        }

        return dateFormat.format(date);
    }

    private static final String formatTime(Date date) {
        if (date == null) {
            return "";
        }

        return timeFormat.format(date);
    }

    private static final String formatTimeZone(Date date) {
        if (date == null) {
            return "";
        }

        return timeZoneFormat.format(date);
    }

    private static final String formatReadableDate(Date date) {
        // String.format("%tFT%<tRZ",

        if (date == null) {
            return "";
        }

        return fullDateFormat.format(date);
    }

    static final void writeResults(String[] headers, List<GetLatestSecurityPrices.Result> results, File outFile)
            throws IOException {
        PrintWriter writer = null;

        String delimiter = ",";
        try {
            writer = new PrintWriter(new FileWriter(outFile));

            GetLatestSecurityPricesUtils.writeHeaders(headers, delimiter, writer);

            for (GetLatestSecurityPrices.Result result : results) {
                Security security = result.getSecurity();
                SecurityPrice secPrice = result.getPrice();

                StringBuilder sb = new StringBuilder();

                GetLatestSecurityPricesUtils.writeValue(security.getSymbol(), sb);

                sb.append(delimiter);
                GetLatestSecurityPricesUtils.writeValue(security.getName(), sb);

                Double price = null;
                if (secPrice != null) {
                    price = secPrice.getPrice();
                }

                sb.append(delimiter);
                GetLatestSecurityPricesUtils.writeValue(formatPrice(price), sb);

                sb.append(delimiter);
                Date date = (secPrice == null) ? null : secPrice.getDate();
                GetLatestSecurityPricesUtils.writeValue(formatDate(date), sb);

                sb.append(delimiter);
                GetLatestSecurityPricesUtils.writeValue(formatTime(date), sb);

                sb.append(delimiter);
                GetLatestSecurityPricesUtils.writeValue(formatTimeZone(date), sb);

                sb.append(delimiter);
                GetLatestSecurityPricesUtils.writeValue(formatReadableDate(date), sb);

                sb.append(delimiter);
                EnumSecurityPriceSrc src = (secPrice == null) ? null : secPrice.getSrc();
                GetLatestSecurityPricesUtils.writeValue(src, sb);

                writer.println(sb.toString());
            }
        } finally {
            if (writer != null) {
                writer.close();
                writer = null;
            }
        }

    }

    private static final void writeHeaders(String[] headers, String delimiter, PrintWriter writer) {
        StringBuilder sb = new StringBuilder();

        int i = 0;
        for (String header : headers) {
            if (i > 0) {
                sb.append(delimiter);
            }
            sb.append("\"");
            sb.append(header);
            sb.append("\"");
            i++;
        }

        writer.println(sb.toString());
    }

    private static void writeValue(Object obj, StringBuilder sb) {
        if (obj == null) {
            obj = "";
        }

        sb.append("\"");
        sb.append(obj);
        sb.append("\"");
    }

    public static final Map<Integer, List<SecurityPrice>> createPriceBuckets(Database db, List<SecurityPrice> prices)
            throws IOException {
        Map<Integer, List<SecurityPrice>> priceBuckets;
        priceBuckets = new HashMap<>();
        for (SecurityPrice price : prices) {
            Integer secId = price.getSecId();

            List<SecurityPrice> list = priceBuckets.get(secId);
            if (list == null) {
                list = new ArrayList<SecurityPrice>();
                priceBuckets.put(secId, list);
            }
            list.add(price);
        }
        // Assert.assertEquals(33, priceBuckets.size());

        // sort by dates
        Comparator<SecurityPrice> byDateComparator = new OldGetLatestSecurityPricesUtils.CompareSecurityPricesByDate();
        Map<Integer, Security> securities = TableSecurityUtils.getMap(db);
        for (Integer id : priceBuckets.keySet()) {
            List<SecurityPrice> list = priceBuckets.get(id);
            if (list != null) {
                Collections.sort(list, byDateComparator);
            }
            if (LOGGER.isDebugEnabled()) {
                for (SecurityPrice price : list) {
                    Security security = securities.get(price.getSecId());
                    if (security != null) {
                        LOGGER.debug(security.getSymbol() + ", " + security.getName() + " " + price);
                    } else {
                        LOGGER.debug(price);
                    }
                }
            }
        }

        if (LOGGER.isDebugEnabled()) {
            for (Security security : securities.values()) {
                LOGGER.debug(security);
            }
        }

        return priceBuckets;
    }

}

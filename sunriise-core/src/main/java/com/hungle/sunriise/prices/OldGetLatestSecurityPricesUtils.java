package com.hungle.sunriise.prices;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.Logger;

import com.healthmarketscience.jackcess.Cursor;
import com.healthmarketscience.jackcess.CursorBuilder;
import com.healthmarketscience.jackcess.Database;
import com.healthmarketscience.jackcess.Index;
import com.healthmarketscience.jackcess.IndexCursor;
import com.healthmarketscience.jackcess.Row;
import com.healthmarketscience.jackcess.Table;
import com.hungle.sunriise.dbutil.TableSecurityPriceUtils;
import com.hungle.sunriise.mnyobject.SecurityPrice;
import com.hungle.sunriise.util.ComparatorUtils;
import com.hungle.sunriise.util.StopWatch;

public class OldGetLatestSecurityPricesUtils {
    private static final Logger LOGGER = org.apache.logging.log4j.LogManager
            .getLogger(OldGetLatestSecurityPricesUtils.class);

    public static class CompareSecurityPricesByDate implements Comparator<SecurityPrice> {
        @Override
        public int compare(SecurityPrice o1, SecurityPrice o2) {
            return ComparatorUtils.compareNullOrder(o1.getDate(), o2.getDate());
        }
    }

    /**
     * Gets the security latest price.
     *
     * @param securityId the security id
     * @param date       the date
     * @param db         the db
     * @return the security latest price
     * @throws IOException Signals that an I/O exception has occurred.
     */
    public static Double getSecurityLatestPrice(Integer securityId, Date date, Database db) throws IOException {
        // Double securityLatestPrice = getSecurityLatestPrice01(securityId,
        // date, db);
        Double securityLatestPrice = getSecurityLatestPriceByCollecting(securityId, date, db);

        return securityLatestPrice;
    }

    static Double getSecurityLatestPriceByIteratingBackward(Integer securityId, Date date, Database db)
            throws IOException {
        Double price = null;

        StopWatch stopWatch = new StopWatch();
        try {
            Cursor cursor = TableSecurityPriceUtils.createCursor(db);

            // XXX: assuming that row is already sorted in increasing date order
            // we will start from end
            cursor.afterLast();

            // iterating backward
            while (cursor.moveToPreviousRow()) {
                Row row = cursor.getCurrentRow();
                Integer id = row.getInt(TableSecurityPriceUtils.COL_SEC_ID);
                if (id.compareTo(securityId) != 0) {
                    continue;
                }
                if (date != null) {
                    Date priceDate = row.getDate(TableSecurityPriceUtils.COL_DATE);
                    if (priceDate != null) {
                        if (priceDate.compareTo(date) > 0) {
                            continue;
                        }
                    }
                }
                price = row.getDouble(TableSecurityPriceUtils.COL_PRICE);
                break;
            }
        } finally {
            long delta = stopWatch.click();

            if (LOGGER.isDebugEnabled()) {
                LOGGER.debug("< getSecurityLatestPrice, securityId=" + securityId + ", delta=" + delta);
            }
        }

        return price;
    }

    static Double getSecurityLatestPriceByCollecting(Integer securityId, Date date, Database db) throws IOException {
        Double price = null;

        StopWatch stopWatch = new StopWatch();
        try {
            Cursor cursor = TableSecurityPriceUtils.createCursor(db);

            List<Row> rows = new ArrayList<Row>();
            Row row = null;
            while (cursor.moveToNextRow()) {
                row = cursor.getCurrentRow();
                Integer id = row.getInt(TableSecurityPriceUtils.COL_SEC_ID);
                if (id.compareTo(securityId) != 0) {
                    continue;
                }
                rows.add(row);
            }

            Collections.sort(rows, new Comparator<Row>() {

                @Override
                public int compare(Row o1, Row o2) {
                    int rv = ComparatorUtils.compareNullOrder(o1.getDate(TableSecurityPriceUtils.COL_DATE),
                            o2.getDate(TableSecurityPriceUtils.COL_DATE));
                    return -rv;
                    // Date d1 = o1.getDate(COL_DATE);
                    // Date d2 = o2.getDate(COL_DATE);
                    // return -(d1.compareTo(d2));
                }
            });

            if (rows.isEmpty()) {
                return null;
            }

            row = rows.get(0);
            price = row.getDouble(TableSecurityPriceUtils.COL_PRICE);

        } finally {
            long delta = stopWatch.click();

            if (LOGGER.isDebugEnabled()) {
                LOGGER.debug("< getSecurityLatestPrice, securityId=" + securityId + ", delta=" + delta);
            }
        }

        return price;
    }

    private static SecurityPrice getLatestPriceByIndex(Integer securityId, Database db) throws IOException {
        SecurityPrice securityPrice = null;

        StopWatch stopWatch = new StopWatch();

        try {
            // > HsecDateSrcSp (4)
            // type=com.healthmarketscience.jackcess.impl.IndexData
            // uniqueEntryCount=1393
            // unique=false
            // shouldIgnoreNulls=false
            // reference=null
            // referencedIndex=null
            // SP.hsec
            // SP.dt
            // SP.src
            // SP.hsp

            Table table = TableSecurityPriceUtils.getTable(db);
            Index index = table.getIndex("HsecDateSrcSp");

            IndexCursor cursor = CursorBuilder.createCursor(index);

            Integer hsec = securityId;
            Date dt = null;
            Integer src = null;
            Integer hsp = null;
            cursor.findClosestRowByEntry(hsec, dt, src, hsp);
            LOGGER.info("hsec=" + hsec + ", row=" + cursor.getCurrentRow());

            HashMap<String, Object> rowPattern = new HashMap<String, Object>();
            rowPattern.put("hsec", securityId);
            Row row = null;
            if (!cursor.currentRowMatches(rowPattern)) {
                LOGGER.warn("NOT_FOUND starting row for hsec=" + hsec);
                return null;
            }

            if (cursor.isAfterLast()) {
                LOGGER.warn("NOT_FOUND starting row for hsec=" + hsec + ", isAfterLast");
                return null;
            }

            List<Row> rows = new ArrayList<Row>();
            while ((row = cursor.getCurrentRow()) != null) {
                Integer secId = row.getInt(TableSecurityPriceUtils.COL_SEC_ID);
                if (secId.compareTo(securityId) != 0) {
                    break;
                }
                rows.add(row);
                cursor.moveToNextRow();
            }

            Collections.sort(rows, new Comparator<Row>() {

                @Override
                public int compare(Row o1, Row o2) {
                    int rv = ComparatorUtils.compareNullOrder(o1.getDate(TableSecurityPriceUtils.COL_DATE),
                            o2.getDate(TableSecurityPriceUtils.COL_DATE));
                    return -rv;
                    // Date d1 = o1.getDate(COL_DATE);
                    // Date d2 = o2.getDate(COL_DATE);
                    // return -(d1.compareTo(d2));
                }
            });

            if (rows.isEmpty()) {
                return null;
            }

            row = rows.get(0);
            securityPrice = TableSecurityPriceUtils.deserialize(row);

        } finally {
            long delta = stopWatch.click();

            if (LOGGER.isDebugEnabled()) {
                LOGGER.debug("< getLatestPriceByIndex, securityId=" + securityId + ", delta=" + delta);
            }
        }
        return securityPrice;
    }

    static SecurityPrice getLatestPrice(Integer securityId, Database db) throws IOException {
        SecurityPrice securityPrice = null;

        // securityPrice = getLatestPriceByIteration(securityId, db);
        securityPrice = getLatestPriceByIndex(securityId, db);

        return securityPrice;
    }

    static SecurityPrice getLatestPrice(Integer securityId, Map<Integer, List<SecurityPrice>> priceBuckets) {
        SecurityPrice securityPrice = null;

        List<SecurityPrice> prices = priceBuckets.get(securityId);
        if ((prices != null) && (prices.size() > 0)) {
            securityPrice = prices.get(prices.size() - 1);
        }

        return securityPrice;
    }

}

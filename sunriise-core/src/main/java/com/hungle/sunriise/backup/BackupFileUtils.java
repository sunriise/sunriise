/*******************************************************************************
 * Copyright (c) 2010 Hung Le
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *******************************************************************************/
package com.hungle.sunriise.backup;

import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.channels.FileChannel.MapMode;
import java.nio.file.CopyOption;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;

import org.apache.logging.log4j.Logger;

import dorkbox.cabParser.CabException;
import dorkbox.cabParser.extractor.CabExtractor;
import dorkbox.cabParser.extractor.CabFileFilter;
import dorkbox.cabParser.extractor.CabFileSaver;
import dorkbox.cabParser.extractor.DefaultCabFileSaver;
import dorkbox.cabParser.structure.CabFileEntry;

// TODO: Auto-generated Javadoc
/**
 * The Class BackupFileUtils.
 */
public class BackupFileUtils {

    /** The Constant log. */
    private static final Logger LOGGER = org.apache.logging.log4j.LogManager.getLogger(BackupFileUtils.class);

    /** The Constant MNY_BACKUP_SUFFIX. */
    public static final String MNY_BACKUP_SUFFIX = ".mbf";

    /** The Constant MNY_SUFFIX. */
    public static final String MNY_SUFFIX = ".mny";

    // 4d 53 49 53 MSIS
    // 41 4d 20 44 AM D
    // 61 74 61 62 atab
    // 61 73 65 ase
    // MSISAM Database
    /** The Constant MSISAM_MAGIC_HEADER. */
    // at location 0x4
    private static final byte[] MSISAM_MAGIC_HEADER = { 0x4d, 0x53, 0x49, 0x53, 0x41, 0x4d, 0x20, 0x44, 0x61, 0x74,
            0x61, 0x62, 0x61, 0x73, 0x65 };

    /**
     * Find magic header.
     *
     * @param srcFile the src file
     * @return the int
     * @throws IOException Signals that an I/O exception has occurred.
     */
    @SuppressWarnings("resource")
    public static final int findMagicHeader(File srcFile) throws IOException {
        int index = -1;
        FileChannel srcChannel = null;
        MappedByteBuffer mappedByteBuffer = null;
        try {
            srcChannel = new RandomAccessFile(srcFile, "r").getChannel();
            mappedByteBuffer = srcChannel.map(MapMode.READ_ONLY, 0, srcChannel.size());
            KMPMatch matcher = new KMPMatch(MSISAM_MAGIC_HEADER);
            byte[] data = new byte[4096 * 2];
            for (int i = 0; i < data.length; i++) {
                data[i] = mappedByteBuffer.get();
            }
            index = matcher.indexOf(data, 0, data.length);
            index -= 4;
        } finally {
            if (mappedByteBuffer != null) {
                mappedByteBuffer = null;
            }
            if (srcChannel != null) {
                try {
                    srcChannel.close();
                } catch (IOException e) {
                    LOGGER.warn(e);
                } finally {
                    srcChannel = null;
                }
            }
        }
        return index;
    }

    /**
     * Copy backup file.
     *
     * @param srcFile      the src file
     * @param destFile     the dest file
     * @param offset       the offset
     * @param maxByteCount the max byte count
     * @return the file
     * @throws IOException Signals that an I/O exception has occurred.
     */
    public static File copyBackupFile(File srcFile, File destFile, long offset, long maxByteCount) throws IOException {
        LOGGER.info("> Copying file from srcFile=" + srcFile);
        LOGGER.info("    destFile=" + destFile);
        LOGGER.info("    offset=" + offset);
        LOGGER.info("    maxByteCount=" + maxByteCount);

        File newFile = destFile;

        long totalBytes = 0L;
        RandomAccessFile srcFileRAF = null;
        FileChannel srcChannel = null;
        RandomAccessFile destFileRAF = null;
        FileChannel destChannel = null;
        try {
            srcFileRAF = new RandomAccessFile(srcFile, "r");
            srcChannel = srcFileRAF.getChannel();

            // Create channel on the destination
            destFileRAF = new RandomAccessFile(destFile, "rwd");
            destChannel = destFileRAF.getChannel();

            if (LOGGER.isDebugEnabled()) {
                LOGGER.debug("srcFile=" + srcFile);
                LOGGER.debug("  size=" + srcChannel.size());
                LOGGER.debug("destFile=" + destFile);
            }
            // Copy file contents from source to destination
            if (maxByteCount < 0) {
                maxByteCount = srcChannel.size();
                LOGGER.info("    maxByteCount 222=" + maxByteCount);
            }

            maxByteCount -= offset;
            if (LOGGER.isDebugEnabled()) {
                LOGGER.debug("offset=" + offset);
                LOGGER.debug("maxByteCount=" + maxByteCount);
            }

            while (maxByteCount > 0) {
                long count = srcChannel.transferTo(offset, maxByteCount, destChannel);
                totalBytes += count;
                if (LOGGER.isDebugEnabled()) {
                    LOGGER.debug("count=" + count);
                }
                maxByteCount -= count;
                offset += count;
            }
        } finally {
            LOGGER.info("< DONE copying file to destFile=" + destFile);
            LOGGER.info("  totalBytes=" + totalBytes);

            if (srcChannel != null) {
                try {
                    srcChannel.close();
                } catch (IOException e) {
                    LOGGER.warn(e);
                } finally {
                    srcChannel = null;
                }
            }
            if (srcFileRAF != null) {
                srcFileRAF.close();
            }
            if (destChannel != null) {
                try {
                    destChannel.close();
                } catch (IOException e) {
                    LOGGER.warn(e);
                } finally {
                    destChannel = null;
                }
            }
            if (destFileRAF != null) {
                destFileRAF.close();
            }
        }
        return newFile;
    }

    /**
     * Checks if is mny backup file.
     *
     * @param name the name
     * @return true, if is mny backup file
     */
    public static final boolean isMnyBackupFile(String name) {
        return name.endsWith(MNY_BACKUP_SUFFIX);
    }

    /**
     * Checks if is mny backup file.
     *
     * @param file the file
     * @return true, if is mny backup file
     */
    public static final boolean isMnyBackupFile(File file) {
        String dbFileName = file.getName();
        return isMnyBackupFile(dbFileName);
    }

    /**
     * Checks if is mny file.
     *
     * @param name the name
     * @return true, if is mny file
     */
    public static final boolean isMnyFile(String name) {
        return name.endsWith(MNY_SUFFIX);
    }

    /**
     * Checks if is mny file.
     *
     * @param file the file
     * @return true, if is mny file
     */
    public static boolean isMnyFile(File file) {
        String dbFileName = file.getName();
        return isMnyFile(dbFileName);
    }

    /**
     * Checks if is mny files.
     *
     * @param name the name
     * @return true, if is mny files
     */
    public static boolean isMnyFiles(String name) {
        return isMnyFile(name) || isMnyBackupFile(name);
    }

    /**
     * Creates the backup as temp file.
     *
     * @param dbFile       the db file
     * @param deleteOnExit the delete on exit
     * @param maxByteCount the max byte count
     * @return the file
     * @throws IOException Signals that an I/O exception has occurred.
     */
    public static final File createBackupAsTempFile(File dbFile, boolean deleteOnExit, long maxByteCount)
            throws IOException {
        boolean oldMethod = false;

        if (oldMethod) {
            return createBackupAsTempFile0(dbFile, deleteOnExit, maxByteCount);
        } else {
            return createBackupAsTempFile1(dbFile, deleteOnExit, maxByteCount);
        }
    }

    private static File createBackupAsTempFile1(File cabFile, boolean deleteOnExit, long maxByteCount)
            throws IOException {
        if (!cabFile.exists()) {
            throw new IOException("File " + cabFile + " does not exist.");
        }

        File tempFile = File.createTempFile("sunriise", MNY_SUFFIX);
        if (deleteOnExit) {
            tempFile.deleteOnExit();
        }

        try {
            CabFileFilter cabFileFilter = new CabFileFilter() {

                @Override
                public boolean test(CabFileEntry cabFileEntry) {
                    LOGGER.info("> cabFileEntry=" + cabFileEntry);
                    return true;
                }
            };

            File topDir = tempFile.getAbsoluteFile().getParentFile();
            CabFileSaver cabFileSaver = new DefaultCabFileSaver(topDir);
            CabExtractor cabExtractor = new CabExtractor(cabFile, cabFileFilter, cabFileSaver);
            CabFileEntry[] files = cabExtractor.getFiles();
            CabFileEntry entry = null;
            if (files == null) {
                LOGGER.warn("CAB file has less than 1 entry.");
            } else if (files.length < 1) {
                LOGGER.warn("CAB file has less than 1 entry.");
            } else if (files.length > 1) {
                LOGGER.warn("CAB file has more than 1 entry.");
                entry = files[0];
            } else {
                entry = files[0];
            }
            LOGGER.info("files=" + files.length);
            if (entry == null) {
                throw new IOException("CAB file has no valid entry.");
            }

            if (!cabExtractor.extract()) {
                throw new IOException("Failed to extract from backup file=" + cabFile.getAbsolutePath());
            }

            File file = new File(topDir, entry.getName());
            CopyOption copyOption = StandardCopyOption.REPLACE_EXISTING;
            Files.copy(file.toPath(), tempFile.toPath(), copyOption);
            file.deleteOnExit();
        } catch (CabException e) {
            throw new IOException(e);
        }

        return tempFile;
    }

    private static File createBackupAsTempFile0(File dbFile, boolean deleteOnExit, long maxByteCount)
            throws IOException {
        File tempFile = File.createTempFile("sunriise", MNY_SUFFIX);
        if (deleteOnExit) {
            tempFile.deleteOnExit();
        }
        long headerOffset = findMagicHeader(dbFile);
        LOGGER.info("headerOffset 111=" + headerOffset);
        if (headerOffset < 0) {
            headerOffset = 80; // compression header?
            LOGGER.info("No magic header, guessing headerOffset=" + headerOffset);
        }
        dbFile = copyBackupFile(dbFile, tempFile, headerOffset, maxByteCount);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("Temp converted backup file=" + dbFile);
        }
        return dbFile;
    }

    /**
     * The main method.
     *
     * @param args the arguments
     */
    public static void main(String[] args) {
        String dbFileName = null;
        try {
            File dbFile = new File(dbFileName);
            String fileName = dbFile.getName();
            if (isMnyBackupFile(fileName)) {
                dbFile = BackupFileUtils.createBackupAsTempFile(dbFile, true, -1L);
            }
        } catch (IOException e) {
            LOGGER.error(e, e);
        } finally {
            LOGGER.info("< DONE");
        }
    }

}

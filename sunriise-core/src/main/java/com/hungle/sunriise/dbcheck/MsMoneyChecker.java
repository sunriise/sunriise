package com.hungle.sunriise.dbcheck;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Set;

import org.apache.logging.log4j.Logger;

import com.healthmarketscience.jackcess.Cursor;
import com.healthmarketscience.jackcess.CursorBuilder;
import com.healthmarketscience.jackcess.Database;
import com.healthmarketscience.jackcess.Index;
import com.healthmarketscience.jackcess.Index.Column;
import com.healthmarketscience.jackcess.Row;
import com.healthmarketscience.jackcess.Table;
import com.hungle.sunriise.account.DefaultAccountVisitor;
import com.hungle.sunriise.io.MnyDb;

// TODO: Auto-generated Javadoc
/**
 * The Class MsMoneyChecker.
 */
public class MsMoneyChecker extends DefaultAccountVisitor {

    /** The Constant LOGGER. */
    private static final Logger LOGGER = org.apache.logging.log4j.LogManager.getLogger(MsMoneyChecker.class);

    /**
     * The main method.
     *
     * @param args the arguments
     */
    public static void main(String[] args) {

        File dbFile = null;
        String password = null;

        if (args.length == 1) {
            dbFile = new File(args[0]);
        } else if (args.length == 3) {
            dbFile = new File(args[0]);
            password = args[1];
        } else {
            Class<MsMoneyChecker> clz = MsMoneyChecker.class;
            System.out.println("Usage: java " + clz.getName() + " in.mny [password]");
            System.exit(1);
        }

        MsMoneyChecker checker = new MsMoneyChecker();
        try {
            LOGGER.info("> START, dbFile=" + dbFile);

            checker.check(dbFile, password);
        } catch (IOException e) {
            LOGGER.error(e, e);
        } finally {
            LOGGER.info("< DONE.");
            if (checker != null) {
                checker = null;
            }
        }

    }

    /**
     * Check.
     *
     * @param dbFile   the db file
     * @param password the password
     * @throws IOException Signals that an I/O exception has occurred.
     */
    public void check(File dbFile, String password) throws IOException {
        this.startVisit(dbFile, password);
    }

    @Override
    public void visit(MnyDb mnyDb) throws IOException {
        Database db = getMnyContext().getDb();

        Set<String> tableNames = db.getTableNames();
        for (String tableName : tableNames) {
            Table table = db.getTable(tableName);

            Index keyIndex = table.getPrimaryKeyIndex();
            List<? extends Column> columns = keyIndex.getColumns();
            if (columns.size() > 1) {
                if (LOGGER.isDebugEnabled()) {
                    LOGGER.debug("tableName=" + tableName + ", primaryKeyIndex.size=" + columns.size());
                }
                for (Column column : columns) {
                    if (LOGGER.isDebugEnabled()) {
                        LOGGER.debug("    " + column.getName());
                    }
                }
            }

            Cursor cursor = CursorBuilder.createCursor(table);
            while (cursor.moveToNextRow()) {
                Row row = cursor.getCurrentRow();
                row.getClass();
            }

        }

    }

}

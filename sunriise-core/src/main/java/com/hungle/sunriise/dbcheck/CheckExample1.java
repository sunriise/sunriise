package com.hungle.sunriise.dbcheck;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import org.apache.logging.log4j.Logger;

import com.hungle.sunriise.dbutil.TableAccountUtils;
import com.hungle.sunriise.dbutil.TableCategoryUtils;
import com.hungle.sunriise.io.MnyDb;
import com.hungle.sunriise.mnyobject.Account;
import com.hungle.sunriise.mnyobject.MnyContext;
import com.hungle.sunriise.mnyobject.Transaction;
import com.hungle.sunriise.util.AbstractAccountAction;
import com.hungle.sunriise.util.MnyContextUtils;

// TODO: Auto-generated Javadoc
/**
 * The Class CheckExample1.
 */
public class CheckExample1 extends AbstractAccountAction {

    /** The Constant LOGGER. */
    private static final Logger LOGGER = org.apache.logging.log4j.LogManager.getLogger(CheckExample1.class);

    /** The result. */
    private Map<Integer, List<Transaction>> result = new HashMap<Integer, List<Transaction>>();

    /** The categories. */
    private Set<Integer> categories;

    /*
     * (non-Javadoc)
     * 
     * @see com.hungle.sunriise.utils.AbstractAccountAction#execute()
     */
    @Override
    public void execute() {
        for (Transaction transaction : getAccount().getTransactions()) {
            if (!acceptTransaction(transaction)) {
                continue;
            }

            if (!hasClassification1(transaction)) {
                notifyMissingClassification1(transaction);
            }
        }
    }

    /**
     * Sets the result.
     *
     * @param result the result
     */
    public void setResult(Map<Integer, List<Transaction>> result) {
        this.result = result;
    }

    /**
     * Sets the categories.
     *
     * @param categories the new categories
     */
    public void setCategories(Set<Integer> categories) {
        this.categories = categories;
    }

    /**
     * Accept transaction.
     *
     * @param transaction the transaction
     * @return true, if successful
     */
    public boolean acceptTransaction(Transaction transaction) {
        if (!TableCategoryUtils.transactionHasCategory(transaction, categories, getMnyContext())) {
            return false;
        } else {
            return true;
        }
    }

    /**
     * Checks for classification 1.
     *
     * @param transaction the transaction
     * @return true, if successful
     */
    private boolean hasClassification1(Transaction transaction) {
        Integer classification1Id = transaction.getClassification1().getId();
        if (classification1Id == null) {
            return false;
        }
        if (classification1Id.intValue() < 0) {
            return false;
        }

        return true;
    }

    /**
     * Notify missing classification 1.
     *
     * @param transaction the transaction
     */
    protected void notifyMissingClassification1(Transaction transaction) {
        LOGGER.warn("Found transaction with missing classification1");

        List<Transaction> transactions = result.get(transaction.getAccount().getId());
        if (transactions == null) {
            transactions = new ArrayList<Transaction>();
            result.put(transaction.getAccount().getId(), transactions);
        }
        transactions.add(transaction);

        for (Integer key : result.keySet()) {
            transactions = result.get(key);
            LOGGER.warn(key + ": " + transactions.size());
        }
    }

    /**
     * Check.
     *
     * @param mnyDb the opened db
     * @return the map
     * @throws IOException Signals that an I/O exception has occurred.
     */
    private Map<Account, List<Transaction>> check(MnyDb mnyDb) throws IOException {
        MnyContext mnyContext = MnyContextUtils.createMnyContext(mnyDb);
        AbstractAccountAction accountAction = this;
        mnyContext.forEachAccount(accountAction);

        Comparator<Account> comparator = new TableAccountUtils.CompareAccountsByName();
        Map<Account, List<Transaction>> finalResult = new TreeMap<Account, List<Transaction>>(comparator);

        Map<Integer, Account> accountsMap = mnyContext.getAccounts();
        for (Integer accountId : result.keySet()) {
            Account account = accountsMap.get(accountId);
            List<Transaction> transactions = result.get(accountId);
            finalResult.put(account, transactions);
        }

        return finalResult;
    }

    /**
     * The main method.
     *
     * @param args the arguments
     */
    public static void main(String[] args) {

        String fileName = null;
        String password = null;

        if (args.length == 1) {
            fileName = args[0];
            password = null;
        } else if (args.length == 2) {
            fileName = args[0];
            password = args[1];
        } else {
            Class<CheckExample1> clz = CheckExample1.class;
            System.out.println("Usage: java " + clz.getName() + " fileName.mny password");
        }

        MnyDb mnyDb = null;
        try {
            mnyDb = new MnyDb(fileName);
            CheckExample1 checker = new CheckExample1();

            final Set<Integer> categories = new HashSet<Integer>();
            categories.add(398); // 589 16th Ave Income
            categories.add(418); // 593 16th Ave Income
            categories.add(458); // 595 16th Ave Income
            categories.add(196); // 589 16th Ave Expense
            categories.add(203); // 595 16th Ave Expense

            checker.setCategories(categories);

            Map<Account, List<Transaction>> result = checker.check(mnyDb);
            for (Account account : result.keySet()) {
                LOGGER.info(account.getName() + ", " + result.get(account).size());
                List<Transaction> transactions = result.get(account);
                for (Transaction transaction : transactions) {
                    LOGGER.info("  " + transaction.getDate() + ", " + transaction.getAmount());
                }
            }

        } catch (IOException e) {
            LOGGER.error(e, e);
        } finally {
            if (mnyDb != null) {
                try {
                    mnyDb.close();
                } catch (IOException e) {
                    LOGGER.warn(e);
                }
            }
        }
    }
}

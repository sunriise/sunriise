package com.hungle.sunriise.mnyobject;

import javax.persistence.Id;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonView;
import com.kjetland.jackson.jsonSchema.annotations.JsonSchemaDescription;

public interface MnyObjectInterface extends Comparable<MnyObjectInterface> {
    /**
     * An internal MsMoney id for this account.
     * 
     * @return an non-negative Integer which is the internal MsMoney id for this
     *         account.
     */
    @JsonView(View.Summary.class)
    @JsonSchemaDescription("A unique ID for Account")
    @Id
    @JsonProperty("_id")
    public abstract Integer getId();

    /**
     * Set the internal MsMoney id for this account.
     * 
     * @param id an non-negative Integer which is the internal MsMoney id for this
     *           account.
     */
    @Id
    @JsonProperty("_id")
    public abstract void setId(Integer id);
}

/*******************************************************************************
 * Copyright (c) 2013 Hung Le
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *******************************************************************************/

package com.hungle.sunriise.mnyobject;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.hungle.sunriise.mnyobject.impl.DefaultCurrencyExchange;

// TODO: Auto-generated Javadoc
/**
 * The Interface Currency.
 */
@JsonPropertyOrder({ "hcrncFrom", "hcrncTo", "rate", "date" })
@JsonDeserialize(as = DefaultCurrencyExchange.class)
public interface CurrencyExchange extends MnyObjectInterface {

    void setSzSymbol(String szSymbol);

    String getSzSymbol();

    void setfHist(Boolean fHist);

    Boolean getfHist();

    void setExchgid(Integer exchgid);

    Integer getExchgid();

    void setfThroughEuro(Boolean fThroughEuro);

    Boolean getfThroughEuro();

    void setfReversed(Boolean fReversed);

    Boolean getfReversed();

    void setDt(Date dt);

    Date getDt();

    void setRate(Double rate);

    Double getRate();

    void setHcrncTo(Integer hcrncTo);

    Integer getHcrncTo();

    void setHcrncFrom(Integer hcrncFrom);

    Integer getHcrncFrom();
}
/*******************************************************************************
 * Copyright (c) 2010 Hung Le
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *******************************************************************************/
package com.hungle.sunriise.mnyobject.impl;

import javax.persistence.Entity;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.hungle.sunriise.mnyobject.InvestmentTransaction;
import com.hungle.sunriise.mnyobject.MnyObject;

// TODO: Auto-generated Javadoc
/**
 * The Class DefaultInvestmentTransaction.
 */
@JsonSerialize(as = InvestmentTransaction.class)
@Entity
public class DefaultInvestmentTransaction extends MnyObject implements InvestmentTransaction {

    /** The price. */
    private Double price;

    /** The quantity. */
    private Double quantity;

    @Override
    public String toString() {
        return "DefaultInvestmentTransaction [id=" + getId() + ", price=" + price + ", quantity=" + quantity + "]";
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.le.sunriise.mnyobject.InvestmentTransaction#getId()
     */

    /*
     * (non-Javadoc)
     * 
     * @see com.le.sunriise.mnyobject.InvestmentTransaction#setId(java.lang.Integer)
     */

    /*
     * (non-Javadoc)
     * 
     * @see com.le.sunriise.mnyobject.InvestmentTransaction#getPrice()
     */

    @Override
    public Double getPrice() {
        return price;
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.le.sunriise.mnyobject.InvestmentTransaction#setPrice(java.lang.Double )
     */

    @Override
    public void setPrice(Double price) {
        this.price = price;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.le.sunriise.mnyobject.InvestmentTransaction#getQuantity()
     */

    @Override
    public Double getQuantity() {
        return quantity;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.le.sunriise.mnyobject.InvestmentTransaction#setQuantity(java.lang
     * .Double)
     */

    @Override
    public void setQuantity(Double quantity) {
        this.quantity = quantity;
    }

}

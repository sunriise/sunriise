/*******************************************************************************
 * Copyright (c) 2010 Hung Le
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *******************************************************************************/
package com.hungle.sunriise.mnyobject.impl;

import java.util.Date;

import javax.persistence.Entity;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.hungle.sunriise.mnyobject.EnumSecurityPriceSrc;
import com.hungle.sunriise.mnyobject.MnyObject;
import com.hungle.sunriise.mnyobject.SecurityPrice;

// TODO: Auto-generated Javadoc
/**
 * The Class DefaultSecurity.
 */
@JsonSerialize(as = SecurityPrice.class)
@Entity
public class DefaultSecurityPrice extends MnyObject implements SecurityPrice {
    private Integer secId;

    private Date date;

    private Double price;

    private EnumSecurityPriceSrc src;

    @Override
    public String toString() {
        return "DefaultSecurityPrice [id=" + getId() + ", secId=" + secId + ", date=" + date + ", price=" + price
                + ", src=" + src + "]";
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.le.sunriise.mnyobject.Security#getId()
     */

    /*
     * (non-Javadoc)
     * 
     * @see com.le.sunriise.mnyobject.Security#setId(java.lang.Integer)
     */

    @Override
    public void setDate(Date date) {
        this.date = date;
    }

    @Override
    public void setPrice(Double price) {
        this.price = price;
    }

    @Override
    public void setSrc(Integer src) {
        this.src = EnumSecurityPriceSrc.toEnum(src);
    }

    @Override
    public Date getDate() {
        return date;
    }

    @Override
    public Double getPrice() {
        return price;
    }

    @Override
    public EnumSecurityPriceSrc getSrc() {
        return src;
    }

    @Override
    public void setSecId(Integer secId) {
        this.secId = secId;

    }

    @Override
    public Integer getSecId() {
        return secId;
    }

}

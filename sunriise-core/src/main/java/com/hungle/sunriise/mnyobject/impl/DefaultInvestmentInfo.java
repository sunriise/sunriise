package com.hungle.sunriise.mnyobject.impl;

import com.hungle.sunriise.mnyobject.InvestmentInfo;
import com.hungle.sunriise.mnyobject.InvestmentTransaction;
import com.hungle.sunriise.mnyobject.Security;

// TODO: Auto-generated Javadoc
/**
 * The Class DefaultInvestmentInfo.
 */
public class DefaultInvestmentInfo implements InvestmentInfo {

    /** The investment activity. */
    private InvestmentActivity investmentActivity;

    /** The investment transaction. */
    private InvestmentTransaction investmentTransaction;

    /** The security. */
    private Security security;

    @Override
    public String toString() {
        return "DefaultInvestmentInfo [investmentActivity=" + investmentActivity + ", investmentTransaction="
                + investmentTransaction + ", security=" + security + "]";
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.hungle.sunriise.mnyobject.InvestmentInfo#getInvestmentActivity()
     */
    @Override
    public InvestmentActivity getActivity() {
        return investmentActivity;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.hungle.sunriise.mnyobject.InvestmentInfo#getInvestmentTransaction()
     */
    @Override
    public InvestmentTransaction getTransaction() {
        return investmentTransaction;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.hungle.sunriise.mnyobject.InvestmentInfo#setInvestmentActivity(com.
     * hungle.sunriise.mnyobject.impl.InvestmentActivity)
     */
    @Override
    public void setActivity(InvestmentActivity investmentActivity) {
        this.investmentActivity = investmentActivity;
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.hungle.sunriise.mnyobject.InvestmentInfo#setInvestmentTransaction(com
     * .hungle.sunriise.mnyobject.InvestmentTransaction)
     */
    @Override
    public void setTransaction(InvestmentTransaction investmentTransaction) {
        this.investmentTransaction = investmentTransaction;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.hungle.sunriise.mnyobject.InvestmentInfo#getSecurity()
     */
    @Override
    public Security getSecurity() {
        return security;
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.hungle.sunriise.mnyobject.InvestmentInfo#setSecurity(com.hungle.sunriise.
     * mnyobject.Security)
     */
    @Override
    public void setSecurity(Security security) {
        this.security = security;
    }

}

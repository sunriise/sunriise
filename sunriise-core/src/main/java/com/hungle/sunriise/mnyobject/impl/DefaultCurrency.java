/*******************************************************************************
 * Copyright (c) 2010 Hung Le
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *******************************************************************************/
package com.hungle.sunriise.mnyobject.impl;

import java.util.Objects;

import javax.persistence.Entity;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.hungle.sunriise.mnyobject.Currency;
import com.hungle.sunriise.mnyobject.MnyObject;

// TODO: Auto-generated Javadoc
/**
 * The Class DefaultCurrency.
 */
@JsonSerialize(as = Currency.class)
@Entity
public class DefaultCurrency extends MnyObject implements Currency {

    @Override
    public int hashCode() {
        return getClass().hashCode();
    }

    @Override
    public boolean equals(Object o) {
        if (o == null || this.getClass() != o.getClass()) {
            return false;
        }
        Currency other = (Currency) o;
        return getId() != null && getId().equals(other.getId());
    }

    /** The name. */
    private String name;

    /** The iso code. */
    private String isoCode;

    /*
     * (non-Javadoc)
     * 
     * @see com.le.sunriise.mnyobject.Currency#getId()
     */

    /*
     * (non-Javadoc)
     * 
     * @see com.le.sunriise.mnyobject.Currency#setId(java.lang.Integer)
     */

    /*
     * (non-Javadoc)
     * 
     * @see com.le.sunriise.mnyobject.Currency#getName()
     */

    @Override
    public String getName() {
        return name;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.le.sunriise.mnyobject.Currency#setName(java.lang.String)
     */

    @Override
    public void setName(String name) {
        this.name = name;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.le.sunriise.mnyobject.Currency#getIsoCode()
     */

    @Override
    public String getIsoCode() {
        return isoCode;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.le.sunriise.mnyobject.Currency#setIsoCode(java.lang.String)
     */

    @Override
    public void setIsoCode(String isoCode) {
        this.isoCode = isoCode;
    }

}

/*******************************************************************************
 * Copyright (c) 2010 Hung Le
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *******************************************************************************/
package com.hungle.sunriise.mnyobject.impl;

import javax.persistence.Entity;

import org.apache.logging.log4j.Logger;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.hungle.sunriise.mnyobject.Category;
import com.hungle.sunriise.mnyobject.MnyObject;

/**
 * The Class DefaultCategory.
 */
@JsonSerialize(as = Category.class)
@Entity
public class DefaultCategory extends MnyObject implements Category {
    private static final Logger LOGGER = org.apache.logging.log4j.LogManager.getLogger(DefaultCategory.class);

    /** The parent id. */
    private Integer parentId;

    /** The name. */
    private String name;

    /** The classification id. */
    private Integer classificationId;

    /** The level. */
    private Integer level;

    private boolean expense;

    private String fullName;

    /*
     * (non-Javadoc)
     * 
     * @see com.le.sunriise.mnyobject.Category#getParentId()
     */

    public DefaultCategory() {
        super();
    }

    @Override
    public Integer getParentId() {
        return parentId;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.le.sunriise.mnyobject.Category#setParentId(java.lang.Integer)
     */

    @Override
    public void setParentId(Integer parent) {
        this.parentId = parent;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.le.sunriise.mnyobject.Category#getName()
     */

    @Override
    public String getName() {
        return name;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.le.sunriise.mnyobject.Category#setName(java.lang.String)
     */

    @Override
    public void setName(String name) {
        this.name = name;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.le.sunriise.mnyobject.Category#getClassificationId()
     */

    @Override
    public Integer getClassificationId() {
        return classificationId;
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.le.sunriise.mnyobject.Category#setClassificationId(java.lang.Integer)
     */

    @Override
    public void setClassificationId(Integer classificationId) {
        this.classificationId = classificationId;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.le.sunriise.mnyobject.Category#getLevel()
     */

    @Override
    public Integer getLevel() {
        return level;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.le.sunriise.mnyobject.Category#setLevel(java.lang.Integer)
     */

    @Override
    public void setLevel(Integer level) {
        this.level = level;
    }

    @Override
    public void setExpense(boolean expense) {
        this.expense = expense;
    }

    @Override
    public boolean getExpense() {
        return expense;
    }

    @Override
    public String getFullName() {
        return fullName;
    }

    @Override
    public void setFullName(String fullName) {
        this.fullName = fullName;
    }
}

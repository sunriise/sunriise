/*******************************************************************************
 * Copyright (c) 2010 Hung Le
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *******************************************************************************/
package com.hungle.sunriise.mnyobject.impl;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.hungle.sunriise.mnyobject.AccountLink;
import com.hungle.sunriise.mnyobject.CategoryLink;
import com.hungle.sunriise.mnyobject.Classification1Link;
import com.hungle.sunriise.mnyobject.EnumTransactionState;
import com.hungle.sunriise.mnyobject.Frequency;
import com.hungle.sunriise.mnyobject.InvestmentInfo;
import com.hungle.sunriise.mnyobject.MnyObject;
import com.hungle.sunriise.mnyobject.PayeeLink;
import com.hungle.sunriise.mnyobject.Transaction;
import com.hungle.sunriise.mnyobject.TransactionInfo;
import com.hungle.sunriise.mnyobject.TransactionSplit;
import com.hungle.sunriise.mnyobject.XferInfo;

// TODO: Auto-generated Javadoc
/**
 * The Class DefaultTransaction.
 */
@JsonSerialize(as = Transaction.class)
@Entity
public class DefaultTransaction extends MnyObject implements Transaction {

    /** The account id. */
//    @ManyToOne(targetEntity = DefaultAccountLink.class)
//    @JoinColumn(name = "fk_account")
    @Transient
    private AccountLink account;

    /** The amount. */
    private BigDecimal amount;

    /** The date. */
    private Date date;

    /** The number. */
    private String number;

    /** The payee info. */
//    @OneToOne(targetEntity = DefaultPayeeLink.class)
    @Transient
    private PayeeLink payeeInfo;

    /** The category id. */
//    @OneToOne(targetEntity = DefaultCategoryLink.class)
    @Transient
    private CategoryLink category;

    /** The classification 1 id. */
//    @OneToOne(targetEntity = DefaultClassification1Link.class)
    @Transient
    private Classification1Link classification1Id;

    /** The splits. */
//    @OneToMany(targetEntity = DefaultTransactionSplit.class)
    @Transient
    private List<TransactionSplit> splits;

    /** The memo. */
    private String memo;

    /** The transaction info. */
    @Transient
    private TransactionInfo transactionInfo;

    /** The state. */
    private EnumTransactionState state = EnumTransactionState.UNRECONCILED;

    /** The cleared state. */
    private Integer clearedState;

    /** The status flag. */
    // column: grftt
    private Integer statusFlag;

    /** The frequency. */
    @Transient
    private Frequency frequency;

    /** The fi transaction id. */
    private String fiTransactionId;

    /** The xfer info. */
    @Transient
    private XferInfo xferInfo;

    /** The running balance. */
    @JsonIgnore
    private BigDecimal runningBalance;

    /** The unaccepted. */
    private boolean unaccepted;

    /** The investment info. */
    @Transient
    private InvestmentInfo investmentInfo;

    /**
     * Instantiates a new default transaction.
     */
    public DefaultTransaction() {
        super();

        setXferInfo(new DefaultXferInfo());
        setInvestmentInfo(new DefaultInvestmentInfo());
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.le.sunriise.mnyobject.Transaction#getSplits()
     */

    @Override
    public List<TransactionSplit> getSplits() {
        return splits;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.le.sunriise.mnyobject.Transaction#setSplits(java.util.List)
     */

    @Override
    public void setSplits(List<TransactionSplit> splits) {
        this.splits = splits;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.le.sunriise.mnyobject.Transaction#getAmount()
     */

    @Override
    public BigDecimal getAmount() {
        return amount;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.le.sunriise.mnyobject.Transaction#setAmount(java.math.BigDecimal)
     */

    @Override
    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.le.sunriise.mnyobject.Transaction#getStatusFlag()
     */

    @Override
    public Integer getStatusFlag() {
        return statusFlag;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.le.sunriise.mnyobject.Transaction#setStatusFlag(java.lang.Integer)
     */

    @Override
    public void setStatusFlag(Integer status) {
        this.statusFlag = status;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.le.sunriise.mnyobject.Transaction#isVoid()
     */

    @Override
    public boolean isVoid() {
        if (statusFlag == null) {
            return false;
        }

        if (transactionInfo.isVoid()) {
            return true;
        }
        // if ((statusFlag & 256) == 256) {
        // if (!transactionInfo.isVoid()) {
        // log.warn("isVoid does not match");
        // }
        // return true;
        // }

        // TODO: hack to skip unknown transactions
        if (statusFlag == 2490368) {
            return true;
        }
        if (statusFlag == 2490400) {
            return true;
        }
        if (statusFlag > 2359302) {
            return true;
        }
        if (statusFlag > 2097152) {
            return true;
        }

        return false;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.le.sunriise.mnyobject.Transaction#getDate()
     */

    @Override
    public Date getDate() {
        return date;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.le.sunriise.mnyobject.Transaction#setDate(java.util.Date)
     */

    @Override
    public void setDate(Date date) {
        this.date = date;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.le.sunriise.mnyobject.Transaction#isRecurring()
     */

    @Override
    public boolean isRecurring() {
        if (frequency == null) {
            return false;
        }
        return frequency.isRecurring();
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.le.sunriise.mnyobject.Transaction#getRunningBalance()
     */

    @Override
    public BigDecimal getRunningBalance() {
        return runningBalance;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.le.sunriise.mnyobject.Transaction#setRunningBalance(java.math.
     * BigDecimal )
     */

    @Override
    public void setRunningBalance(BigDecimal runningBalance) {
        this.runningBalance = runningBalance;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.le.sunriise.mnyobject.Transaction#getCategoryId()
     */

    @Override
    public CategoryLink getCategory() {
        return category;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.le.sunriise.mnyobject.Transaction#setCategoryId(java.lang.Integer)
     */

    @Override
    public void setCategory(CategoryLink category) {
        this.category = category;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.le.sunriise.mnyobject.Transaction#getFrequency()
     */
    @Override
    public Frequency getFrequency() {
        return frequency;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.le.sunriise.mnyobject.Transaction#setFrequency(com.le.sunriise.
     * mnyobject .Frequency)
     */

    @Override
    public void setFrequency(Frequency frequency) {
        this.frequency = frequency;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.le.sunriise.mnyobject.Transaction#getTransactionInfo()
     */

    @Override
    public TransactionInfo getTransactionInfo() {
        return transactionInfo;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.le.sunriise.mnyobject.Transaction#setTransactionInfo(com.le.sunriise
     * .mnyobject.TransactionInfo)
     */

    @Override
    public void setTransactionInfo(TransactionInfo transactionInfo) {
        this.transactionInfo = transactionInfo;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.le.sunriise.mnyobject.Transaction#getSecurityId()
     */

    /*
     * (non-Javadoc)
     * 
     * @see com.le.sunriise.mnyobject.Transaction#isInvestment()
     */

    @Override
    public boolean isInvestment() {
        if (transactionInfo == null) {
            return false;
        }
        return transactionInfo.isInvestment();
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.le.sunriise.mnyobject.Transaction#isTransfer()
     */

    @Override
    public boolean isTransfer() {
        XferInfo xferInfo = getXferInfo();
        if (xferInfo == null) {
            return false;
        }

        return xferInfo.getXferAccountId() != null;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.le.sunriise.mnyobject.Transaction#getClearedState()
     */

    @Override
    public Integer getClearedState() {
        return clearedState;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.le.sunriise.mnyobject.Transaction#setClearedState(java.lang.Integer)
     */

    @Override
    public void setClearedState(Integer clearedState) {
        this.clearedState = clearedState;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.le.sunriise.mnyobject.Transaction#isCleared()
     */

    @Override
    public boolean isCleared() {
        return (clearedState != null) && (clearedState == 1);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.le.sunriise.mnyobject.Transaction#isReconciled()
     */

    @Override
    public boolean isReconciled() {
        return (clearedState != null) && (clearedState == 2);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.le.sunriise.mnyobject.Transaction#hasSplits()
     */

    @Override
    public boolean hasSplits() {
        if (splits == null) {
            return false;
        }
        return splits.size() > 0;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.le.sunriise.mnyobject.Transaction#getMemo()
     */

    @Override
    public String getMemo() {
        return memo;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.le.sunriise.mnyobject.Transaction#setMemo(java.lang.String)
     */

    @Override
    public void setMemo(String memo) {
        this.memo = memo;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.le.sunriise.mnyobject.Transaction#getNumber()
     */

    @Override
    public String getNumber() {
        return number;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.le.sunriise.mnyobject.Transaction#setNumber(java.lang.String)
     */

    @Override
    public void setNumber(String number) {
        this.number = number;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.le.sunriise.mnyobject.Transaction#getState()
     */

    @Override
    public EnumTransactionState getState() {
        return state;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.le.sunriise.mnyobject.Transaction#setState(com.le.sunriise.mnyobject
     * .TransactionState)
     */

    @Override
    public void setState(EnumTransactionState state) {
        this.state = state;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.hungle.sunriise.mnyobject.Transaction#getAccountId()
     */

    @Override
    public AccountLink getAccount() {
        return account;
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.hungle.sunriise.mnyobject.Transaction#setAccountId(java.lang.Integer)
     */

    @Override
    public void setAccount(AccountLink accountId) {
        this.account = accountId;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.hungle.sunriise.mnyobject.Transaction#getFiTransactionId()
     */
    @Override
    public String getFiTransactionId() {
        return fiTransactionId;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.hungle.sunriise.mnyobject.Transaction#setFiTransactionId(java.lang.
     * String)
     */
    @Override
    public void setFiTransactionId(String fiTransactionId) {
        this.fiTransactionId = fiTransactionId;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.hungle.sunriise.mnyobject.Transaction#getClassification1Id()
     */
    @Override
    public Classification1Link getClassification1() {
        return classification1Id;
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.hungle.sunriise.mnyobject.Transaction#setClassification1Id(java.lang.
     * Integer)
     */
    @Override
    public void setClassification1(Classification1Link classification1Id) {
        this.classification1Id = classification1Id;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.hungle.sunriise.mnyobject.Transaction#isUnaccepted()
     */
    @Override
    public boolean isUnaccepted() {
        return unaccepted;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.hungle.sunriise.mnyobject.Transaction#setUnaccepted(boolean)
     */
    @Override
    public void setUnaccepted(boolean unaccepted) {
        this.unaccepted = unaccepted;
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.hungle.sunriise.mnyobject.Transaction#setXferInfo(com.hungle.sunriise
     * .mnyobject.XferInfo)
     */
    @Override
    public void setXferInfo(XferInfo xferInfo) {
        this.xferInfo = xferInfo;

    }

    /*
     * (non-Javadoc)
     * 
     * @see com.hungle.sunriise.mnyobject.Transaction#getXferInfo()
     */
    @Override
    public XferInfo getXferInfo() {
        return xferInfo;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.hungle.sunriise.mnyobject.Transaction#getInvestmentInfo()
     */
    @Override
    public InvestmentInfo getInvestmentInfo() {
        return investmentInfo;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.hungle.sunriise.mnyobject.Transaction#setInvestmentInfo(com.hungle.
     * sunriise.mnyobject.InvestmentInfo)
     */
    @Override
    public void setInvestmentInfo(InvestmentInfo investmentInfo) {
        this.investmentInfo = investmentInfo;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.hungle.sunriise.mnyobject.Transaction#getPayee()
     */
    @Override
    public PayeeLink getPayee() {
        return payeeInfo;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.hungle.sunriise.mnyobject.Transaction#setPayee(com.hungle.sunriise.
     * mnyobject.Transaction.Payee)
     */
    @Override
    public void setPayee(PayeeLink payeeInfo) {
        this.payeeInfo = payeeInfo;
    }

}

package com.hungle.sunriise.mnyobject.impl;

import com.healthmarketscience.jackcess.Row;
import com.hungle.sunriise.dbutil.TableTaxRatesUtils;

public class TaxBracket {
    private Double low;
    private Double high;
    private Double rate;

    public Double getLow() {
        return low;
    }

    public void setLow(Double from) {
        this.low = from;
    }

    public Double getHigh() {
        return high;
    }

    public void setHigh(Double to) {
        this.high = to;
    }

    public Double getRate() {
        return rate;
    }

    public void setRate(Double rate) {
        this.rate = rate;
    }

    @Override
    public String toString() {
        return "TaxBracket [low=" + low + ", high=" + high + ", rate=" + rate + "]";
    }

    public static TaxBracket deserialize(Row row, int i) {
        TaxBracket taxBracket = new TaxBracket();

        taxBracket.setLow(row.getDouble(TableTaxRatesUtils.COL_BRACKET_LOW_PREFIX + i));
        taxBracket.setHigh(row.getDouble(TableTaxRatesUtils.COL_BRACKET_HIGH_PREFIX + i));
        taxBracket.setRate(row.getDouble(TableTaxRatesUtils.COL_BRACKET_RATE_PREFIX + i));

        return taxBracket;
    }
}

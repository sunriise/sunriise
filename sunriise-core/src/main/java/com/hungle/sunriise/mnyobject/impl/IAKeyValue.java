package com.hungle.sunriise.mnyobject.impl;

public class IAKeyValue {
    private int key;
    private String value;
    private boolean added = true;

    public IAKeyValue(int key, String value) {
        this(key, value, true);
    }

    public IAKeyValue(int key, String value, boolean added) {
        super();
        this.key = key;
        this.value = value;
        this.added = added;
    }

    public int getKey() {
        return key;
    }

    public void setKey(int key) {
        this.key = key;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public boolean isAdded() {
        return added;
    }

    public void setAdded(boolean added) {
        this.added = added;
    }
}

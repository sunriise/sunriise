/*******************************************************************************
 * Copyright (c) 2016 Hung Le
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *******************************************************************************/
package com.hungle.sunriise.mnyobject.impl;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.hungle.sunriise.mnyobject.TransactionXfer;

// TODO: Auto-generated Javadoc
/**
 * The Class DefaultTransationXfer.
 */
@JsonSerialize(as = TransactionXfer.class)
public class DefaultTransactionXfer implements TransactionXfer {

    /** The from transaction id. */
    // htrnFrom
    private Integer fromTransactionId;

    /** The to transaction id. */
    // htrnLink
    private Integer toTransactionId;

    /*
     * (non-Javadoc)
     * 
     * @see com.hungle.sunriise.mnyobject.TransactionXfer#getFromTransactionId()
     */
    @Override
    public Integer getFromTransactionId() {
        return fromTransactionId;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.hungle.sunriise.mnyobject.TransactionXfer#setFromTransactionId(java.
     * lang.Integer)
     */
    @Override
    public void setFromTransactionId(Integer fromTransactionId) {
        this.fromTransactionId = fromTransactionId;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.hungle.sunriise.mnyobject.TransactionXfer#getToTransactionId()
     */
    @Override
    public Integer getToTransactionId() {
        return toTransactionId;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.hungle.sunriise.mnyobject.TransactionXfer#setToTransactionId(java.
     * lang.Integer)
     */
    @Override
    public void setToTransactionId(Integer toTransactionId) {
        this.toTransactionId = toTransactionId;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "DefaultTransationXfer [fromTransactionId=" + fromTransactionId + ", toTransactionId=" + toTransactionId
                + "]";
    }

}

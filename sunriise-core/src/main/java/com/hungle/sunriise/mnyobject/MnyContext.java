/*******************************************************************************
 * Copyright (c) 2010 Hung Le
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *******************************************************************************/
package com.hungle.sunriise.mnyobject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Stream;

import org.apache.logging.log4j.Logger;

import com.healthmarketscience.jackcess.Database;
import com.hungle.sunriise.dbutil.TableAccountUtils;
import com.hungle.sunriise.util.AbstractAccountAction;

// TODO: Auto-generated Javadoc
/**
 * The Class MnyContext.
 */
public class MnyContext {

    /** The Constant LOGGER. */
    private static final Logger LOGGER = org.apache.logging.log4j.LogManager.getLogger(MnyContext.class);

    /** The accounts. */
    private Map<Integer, Account> accounts;

    /** The payees. */
    private Map<Integer, Payee> payees;

    /** The categories. */
    private Map<Integer, Category> categories;

    /** The currencies. */
    private Map<Integer, Currency> currencies;

    /** The securities. */
    private Map<Integer, Security> securities;

    /** The db. */
    private Database db;

    /**
     * Gets the accounts.
     *
     * @return the accounts
     */
    public Map<Integer, Account> getAccounts() {
        return accounts;
    }

    /**
     * Sets the accounts.
     *
     * @param accounts the accounts
     */
    public void setAccounts(Map<Integer, Account> accounts) {
        this.accounts = accounts;
    }

    /**
     * Gets the payees.
     *
     * @return the payees
     */
    public Map<Integer, Payee> getPayees() {
        return payees;
    }

    /**
     * Sets the payees.
     *
     * @param payees the payees
     */
    public void setPayees(Map<Integer, Payee> payees) {
        this.payees = payees;
    }

    /**
     * Gets the categories.
     *
     * @return the categories
     */
    public Map<Integer, Category> getCategories() {
        return categories;
    }

    /**
     * Sets the categories.
     *
     * @param categories the categories
     */
    public void setCategories(Map<Integer, Category> categories) {
        this.categories = categories;
    }

    /**
     * Gets the currencies.
     *
     * @return the currencies
     */
    public Map<Integer, Currency> getCurrencies() {
        return currencies;
    }

    /**
     * Sets the currencies.
     *
     * @param currencies the currencies
     */
    public void setCurrencies(Map<Integer, Currency> currencies) {
        this.currencies = currencies;
    }

    /**
     * Gets the securities.
     *
     * @return the securities
     */
    public Map<Integer, Security> getSecurities() {
        return securities;
    }

    /**
     * Sets the securities.
     *
     * @param securities the securities
     */
    public void setSecurities(Map<Integer, Security> securities) {
        this.securities = securities;
    }

    /**
     * Gets the db.
     *
     * @return the db
     */
    public Database getDb() {
        return db;
    }

    /**
     * Sets the db.
     *
     * @param db the new db
     */
    public void setDb(Database db) {
        this.db = db;
    }

    /**
     * Gets the transaction.
     *
     * @param transactionId              the transaction id
     * @param searchFilteredTransactions the search filtered transactions
     * @return the transaction
     */
    private Transaction getTransaction(Integer transactionId, boolean searchFilteredTransactions) {
        for (Account account : accounts.values()) {
            List<Transaction> transactions = null;

            if (searchFilteredTransactions) {
                transactions = account.getFilteredTransactions();
                // TODO: slow search
                if (transactions != null) {
                    for (Transaction transaction : transactions) {
                        if (transaction.getId().equals(transactionId)) {
                            return transaction;
                        }
                    }
                }
            }

            transactions = account.getTransactions();
            // TODO: slow search
            if (transactions != null) {
                for (Transaction transaction : transactions) {
                    if (transaction.getId().equals(transactionId)) {
                        return transaction;
                    }
                }
            }
        }

        return null;
    }

    /**
     * Gets the transaction.
     *
     * @param transactionId the transaction id
     * @return the transaction
     */
    public Transaction getTransaction(Integer transactionId) {
        boolean searchFilteredTransactions = true;
        return getTransaction(transactionId, searchFilteredTransactions);
    }

    /**
     * For each account.
     *
     * @param accountAction the account action
     * @throws IOException Signals that an I/O exception has occurred.
     */
    public void forEachAccount(AbstractAccountAction accountAction) throws IOException {
        List<Account> accounts = new ArrayList<Account>(this.getAccounts().values());
        Comparator<Account> comparator = new TableAccountUtils.CompareAccountsByName();
        Collections.sort(accounts, comparator);

        int i = 0;
        for (Account account : accounts) {
            i++;
            LOGGER.info("# " + i + "/" + accounts.size());

            accountAction.setMnyContext(this);
            TableAccountUtils.addTransactionsToAccount(account, this);
            accountAction.setAccount(account);
            accountAction.execute();
        }
    }

    public Stream<Account> getAccountStream() {
        return getAccounts().values().stream();
    }
}

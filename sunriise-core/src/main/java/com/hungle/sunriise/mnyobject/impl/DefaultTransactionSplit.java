/*******************************************************************************
 * Copyright (c) 2010 Hung Le
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *******************************************************************************/
package com.hungle.sunriise.mnyobject.impl;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.hungle.sunriise.mnyobject.MnyObject;
import com.hungle.sunriise.mnyobject.Transaction;
import com.hungle.sunriise.mnyobject.TransactionSplit;

// TODO: Auto-generated Javadoc
/**
 * The Class DefaultTransactionSplit.
 */
@JsonSerialize(as = TransactionSplit.class)
//@Entity
public class DefaultTransactionSplit extends MnyObject implements TransactionSplit {

    /** The parent id. */
    private Integer parentId;

    /** The row id. */
    private Integer rowId;

    /** The transaction. */
    private Transaction transaction;

    /*
     * (non-Javadoc)
     * 
     * @see com.le.sunriise.mnyobject.TransactionSplit#getParentId()
     */

    @Override
    public Integer getParentId() {
        return parentId;
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.le.sunriise.mnyobject.TransactionSplit#setParentId(java.lang.Integer)
     */

    @Override
    public void setParentId(Integer parentId) {
        this.parentId = parentId;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.le.sunriise.mnyobject.TransactionSplit#getRowId()
     */

    @Override
    public Integer getRowId() {
        return rowId;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.le.sunriise.mnyobject.TransactionSplit#setRowId(java.lang.Integer)
     */

    @Override
    public void setRowId(Integer rowId) {
        this.rowId = rowId;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.le.sunriise.mnyobject.TransactionSplit#getTransaction()
     */

    @Override
    public Transaction getTransaction() {
        return transaction;
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.le.sunriise.mnyobject.TransactionSplit#setTransaction(com.le.sunriise
     * .mnyobject.TransactionImpl)
     */

    @Override
    public void setTransaction(Transaction transaction) {
        this.transaction = transaction;
    }
}

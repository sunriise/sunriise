/*******************************************************************************
 * Copyright (c) 2013 Hung Le
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *******************************************************************************/

package com.hungle.sunriise.mnyobject;

import java.math.BigDecimal;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.hungle.sunriise.json.BalanceSerializer;
import com.hungle.sunriise.json.InvestmentQuantitySerializer;
import com.hungle.sunriise.mnyobject.impl.DefaultSecurityHolding;

// TODO: Auto-generated Javadoc
/**
 * The Interface SecurityHolding.
 */
@JsonDeserialize(as = DefaultSecurityHolding.class)
public interface SecurityHolding {

    /**
     * Gets the security.
     *
     * @return the security
     */
    public abstract Security getSecurity();

    /**
     * Sets the security.
     *
     * @param security the new security
     */
    public abstract void setSecurity(Security security);

    /**
     * Gets the quantity.
     *
     * @return the quantity
     */
    @JsonSerialize(using = InvestmentQuantitySerializer.class)
    public abstract Double getQuantity();

    /**
     * Sets the quantity.
     *
     * @param quanity the new quantity
     */
    public abstract void setQuantity(Double quanity);

    /**
     * Gets the price.
     *
     * @return the price
     */
    @JsonSerialize(using = BalanceSerializer.class)
    public abstract BigDecimal getPrice();

    /**
     * Sets the price.
     *
     * @param price the new price
     */
    public abstract void setPrice(BigDecimal price);

    /**
     * Gets the market value.
     *
     * @return the market value
     */
    @JsonSerialize(using = BalanceSerializer.class)
    public abstract BigDecimal getMarketValue();

    /**
     * Sets the market value.
     *
     * @param marketValue the new market value
     */
    public abstract void setMarketValue(BigDecimal marketValue);
}
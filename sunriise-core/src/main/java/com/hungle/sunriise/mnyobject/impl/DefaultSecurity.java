/*******************************************************************************
 * Copyright (c) 2010 Hung Le
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *******************************************************************************/
package com.hungle.sunriise.mnyobject.impl;

import javax.persistence.Entity;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.hungle.sunriise.mnyobject.MnyObject;
import com.hungle.sunriise.mnyobject.Security;

// TODO: Auto-generated Javadoc
/**
 * The Class DefaultSecurity.
 */
@JsonSerialize(as = Security.class)
@Entity
public class DefaultSecurity extends MnyObject implements Security {

    /** The name. */
    private String name;

    /** The symbol. */
    private String symbol;

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "DefaultSecurity [id=" + getId() + ", name=" + name + ", symbol=" + symbol + "]";
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.le.sunriise.mnyobject.Security#getId()
     */

    /*
     * (non-Javadoc)
     * 
     * @see com.le.sunriise.mnyobject.Security#setId(java.lang.Integer)
     */

    /*
     * (non-Javadoc)
     * 
     * @see com.le.sunriise.mnyobject.Security#getName()
     */

    @Override
    public String getName() {
        return name;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.le.sunriise.mnyobject.Security#setName(java.lang.String)
     */

    @Override
    public void setName(String name) {
        this.name = name;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.le.sunriise.mnyobject.Security#getSymbol()
     */

    @Override
    public String getSymbol() {
        return symbol;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.le.sunriise.mnyobject.Security#setSymbol(java.lang.String)
     */

    @Override
    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

}

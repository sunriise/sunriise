/*******************************************************************************
 * Copyright (c) 2010 Hung Le
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *******************************************************************************/
package com.hungle.sunriise.mnyobject.impl;

import com.fasterxml.jackson.annotation.JsonView;
import com.hungle.sunriise.dbutil.TableTransactionUtils;
import com.hungle.sunriise.json.Views;
import com.hungle.sunriise.mnyobject.TransactionInfo;

// TODO: Auto-generated Javadoc
/**
 * The Class DefaultTransactionInfo.
 */
// @JsonSerialize(as = TransactionInfo.class)
public class DefaultTransactionInfo implements TransactionInfo {

    /** The transfer to. */
    private boolean transferTo = false;

    /** The flag. */
    // TRN.grftt bits
    @JsonView(Views.Internal.class)
    private Integer flag = 0;

    /*
     * (non-Javadoc)
     * 
     * @see com.hungle.sunriise.mnyobject.TransactionInfo#getFlag()
     */
    @Override
    public Integer getFlag() {
        return flag;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.hungle.sunriise.mnyobject.TransactionInfo#setFlag(java.lang.Integer)
     */
    @Override
    public void setFlag(Integer flag) {
        if (flag == null) {
            flag = 0;
        }
        this.flag = flag;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.hungle.sunriise.mnyobject.TransactionInfo#isTransfer()
     */
    @JsonView(Views.Internal.class)
    @Override
    public boolean isTransfer() {
        // bit 1 == transfer
        int mask = (1 << 1);
        return isSet(flag, mask);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.hungle.sunriise.mnyobject.TransactionInfo#isTransferTo()
     */
    @Override
    public boolean isTransferTo() {
        return transferTo;
    }

    /**
     * Sets the transfer to.
     *
     * @param transferTo the new transfer to
     */
    public void setTransferTo(boolean transferTo) {
        this.transferTo = transferTo;
    }

    /**
     * Checks if is transfer to using bit mask.
     *
     * @return true, if is transfer to using bit mask
     */
    private boolean isTransferToUsingBitMask() {
        // bit 2 == transfer to
        int mask = (1 << 2);
        return isSet(flag, mask);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.hungle.sunriise.mnyobject.TransactionInfo#isInvestment()
     */
    @Override
    public boolean isInvestment() {
        // bit 4 == investment trn (need to figure out how to tell what
        // kind--other grftt bits?)
        int mask = (1 << 4);
        return isSet(flag, mask);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.hungle.sunriise.mnyobject.TransactionInfo#isSplitParent()
     */
    @Override
    public boolean isSplitParent() {
        // bit 5 == split parent
        int mask = (1 << 5);
        return isSet(flag, mask);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.hungle.sunriise.mnyobject.TransactionInfo#isSplitChild()
     */
    @Override
    public boolean isSplitChild() {
        // bit 6 == split child
        int mask = (1 << 6);
        return isSet(flag, mask);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.hungle.sunriise.mnyobject.TransactionInfo#isVoid()
     */
    @Override
    public boolean isVoid() {
        // bit 8 == void"
        int mask = (1 << 8);
        return isSet(flag, mask);
    }

    /**
     * Gets the binary string.
     *
     * @return the binary string
     */
    public String getBinaryString() {
        return TableTransactionUtils.getBinaryString(flag);
    }

    /**
     * Checks if is sets the.
     *
     * @param flag the flag
     * @param mask the mask
     * @return true, if is sets the
     */
    private static final boolean isSet(Integer flag, int mask) {
        return (flag & mask) == mask;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "DefaultTransactionInfo [flag=" + flag + "( " + getBinaryString() + ") " + "]";
    }

}

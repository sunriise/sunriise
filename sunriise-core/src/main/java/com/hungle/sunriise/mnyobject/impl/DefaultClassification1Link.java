package com.hungle.sunriise.mnyobject.impl;

import javax.persistence.Entity;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.hungle.sunriise.mnyobject.Classification1Link;
import com.hungle.sunriise.mnyobject.MnyObject;

/**
 * The Class DefaultClassification1.
 */
@JsonSerialize(as = Classification1Link.class)
@Entity
public final class DefaultClassification1Link extends MnyObject implements Classification1Link {

    public DefaultClassification1Link() {
        super();
        // TODO Auto-generated constructor stub
    }

    /** The name. */
    private String name;

    /**
     * Instantiates a new default classification 1.
     *
     * @param id the id
     */
    public DefaultClassification1Link(Integer id) {
        setId(id);
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.hungle.sunriise.mnyobject.Transaction.Classification1#setName(java.lang.
     * String)
     */
    @Override
    public void setName(String name) {
        this.name = name;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.hungle.sunriise.mnyobject.Transaction.Classification1#getName()
     */
    @Override
    public String getName() {
        return name;
    }
}
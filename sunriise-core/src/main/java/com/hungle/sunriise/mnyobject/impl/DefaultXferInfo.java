package com.hungle.sunriise.mnyobject.impl;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.hungle.sunriise.mnyobject.XferInfo;

// TODO: Auto-generated Javadoc
/**
 * The Class DefaultXferInfo.
 */
@JsonSerialize(as = XferInfo.class)
public class DefaultXferInfo implements XferInfo {

    /** The transferred account id. */
    private Integer xferAccountId;

    /** The xfer transaction id. */
    private Integer xferTransactionId;

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.hungle.sunriise.mnyobject.XferInfo#setXferAccountId(java.lang.Integer)
     */
    @Override
    public void setXferAccountId(Integer xferAccountId) {
        this.xferAccountId = xferAccountId;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.hungle.sunriise.mnyobject.XferInfo#setXferTransactionId(java.lang.
     * Integer)
     */
    @Override
    public void setXferTransactionId(Integer xferTransactionId) {
        this.xferTransactionId = xferTransactionId;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.hungle.sunriise.mnyobject.XferInfo#getXferAccountId()
     */
    @Override
    public Integer getXferAccountId() {
        return xferAccountId;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.hungle.sunriise.mnyobject.XferInfo#getXferTransactionId()
     */
    @Override
    public Integer getXferTransactionId() {
        return xferTransactionId;
    }

}

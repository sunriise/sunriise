/*******************************************************************************
 * Copyright (c) 2016 Hung Le
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *******************************************************************************/
package com.hungle.sunriise.mnyobject;

// TODO: Auto-generated Javadoc
/**
 * The Interface TransactionXfer.
 */
public interface TransactionXfer {

    /**
     * Gets the from transaction id.
     *
     * @return the from transaction id
     */
    public Integer getFromTransactionId();

    /**
     * Gets the to transaction id.
     *
     * @return the to transaction id
     */
    public Integer getToTransactionId();

    /**
     * Sets the from transaction id.
     *
     * @param fromTransactionId the new from transaction id
     */
    public void setFromTransactionId(Integer fromTransactionId);

    /**
     * Sets the to transaction id.
     *
     * @param toTransactionId the new to transaction id
     */
    public void setToTransactionId(Integer toTransactionId);
}

/*******************************************************************************
 * Copyright (c) 2010 Hung Le
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *******************************************************************************/
package com.hungle.sunriise.mnyobject.impl;

import java.util.Date;

import javax.persistence.Entity;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.hungle.sunriise.mnyobject.CurrencyExchange;
import com.hungle.sunriise.mnyobject.MnyObject;

// TODO: Auto-generated Javadoc
/**
 * The Class DefaultCurrency.
 */
@JsonSerialize(as = CurrencyExchange.class)
@Entity
public class DefaultCurrencyExchange extends MnyObject implements CurrencyExchange {

    private Integer hcrncFrom;

    private Integer hcrncTo;

    private Double rate;

    private Date dt;

    private Boolean fReversed;

    private Boolean fThroughEuro;

    private Integer exchgid;

    private Boolean fHist;

    private String szSymbol;

    @Override
    public Integer getHcrncFrom() {
        return hcrncFrom;
    }

    @Override
    public void setHcrncFrom(Integer hcrncFrom) {
        this.hcrncFrom = hcrncFrom;
    }

    @Override
    public Integer getHcrncTo() {
        return hcrncTo;
    }

    @Override
    public void setHcrncTo(Integer hcrncTo) {
        this.hcrncTo = hcrncTo;
    }

    @Override
    public Double getRate() {
        return rate;
    }

    @Override
    public void setRate(Double rate) {
        this.rate = rate;
    }

    @Override
    public Date getDt() {
        return dt;
    }

    @Override
    public void setDt(Date dt) {
        this.dt = dt;
    }

    @Override
    public Boolean getfReversed() {
        return fReversed;
    }

    @Override
    public void setfReversed(Boolean fReversed) {
        this.fReversed = fReversed;
    }

    @Override
    public Boolean getfThroughEuro() {
        return fThroughEuro;
    }

    @Override
    public void setfThroughEuro(Boolean fThroughEuro) {
        this.fThroughEuro = fThroughEuro;
    }

    @Override
    public Integer getExchgid() {
        return exchgid;
    }

    @Override
    public void setExchgid(Integer exchgid) {
        this.exchgid = exchgid;
    }

    @Override
    public Boolean getfHist() {
        return fHist;
    }

    @Override
    public void setfHist(Boolean fHist) {
        this.fHist = fHist;
    }

    @Override
    public String getSzSymbol() {
        return szSymbol;
    }

    @Override
    public void setSzSymbol(String szSymbol) {
        this.szSymbol = szSymbol;
    }

    @Override
    public String toString() {
        return "DefaultCurrencyExchange [hcrncFrom=" + hcrncFrom + ", hcrncTo=" + hcrncTo + ", rate=" + rate + ", dt="
                + dt + "]";
    }
}

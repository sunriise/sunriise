/*******************************************************************************
 * Copyright (c) 2013 Hung Le
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *******************************************************************************/

package com.hungle.sunriise.mnyobject;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.annotation.JsonView;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.hungle.sunriise.mnyobject.impl.DefaultSecurity;

// TODO: Auto-generated Javadoc
/**
 * The Interface Security.
 */
@JsonPropertyOrder({ "id", "name", "symbol", })
@JsonDeserialize(as = DefaultSecurity.class)
public interface Security extends MnyObjectInterface {

    /**
     * Gets the name.
     *
     * @return the name
     */
    @JsonView(View.Summary.class)
    public abstract String getName();

    /**
     * Sets the name.
     *
     * @param name the new name
     */
    public abstract void setName(String name);

    /**
     * Gets the symbol.
     *
     * @return the symbol
     */
    public abstract String getSymbol();

    /**
     * Sets the symbol.
     *
     * @param symbol the new symbol
     */
    public abstract void setSymbol(String symbol);

}
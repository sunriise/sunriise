/*******************************************************************************
 * Copyright (c) 2013 Hung Le
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *******************************************************************************/

package com.hungle.sunriise.mnyobject;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.hungle.sunriise.mnyobject.impl.DefaultTransactionInfo;

// TODO: Auto-generated Javadoc
/**
 * The Interface TransactionInfo.
 */

// need as = DefaultTransactionInfo.class so that
// Deserialize has a constructor
@JsonDeserialize(as = DefaultTransactionInfo.class)
public interface TransactionInfo {

    /**
     * Gets the flag.
     *
     * @return the flag
     */
    public abstract Integer getFlag();

    /**
     * Sets the flag.
     *
     * @param flag the new flag
     */
    public abstract void setFlag(Integer flag);

    /**
     * Checks if is transfer.
     *
     * @return true, if is transfer
     */
    public abstract boolean isTransfer();

    /**
     * Checks if is transfer to.
     *
     * @return true, if is transfer to
     */
    public abstract boolean isTransferTo();

    /**
     * Checks if is investment.
     *
     * @return true, if is investment
     */
    public abstract boolean isInvestment();

    /**
     * Checks if is split parent.
     *
     * @return true, if is split parent
     */
    public abstract boolean isSplitParent();

    /**
     * Checks if is split child.
     *
     * @return true, if is split child
     */
    public abstract boolean isSplitChild();

    /**
     * Checks if is void.
     *
     * @return true, if is void
     */
    public abstract boolean isVoid();

    public abstract void setTransferTo(boolean transferTo);

}
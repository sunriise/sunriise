/*******************************************************************************
 * Copyright (c) 2010 Hung Le
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *******************************************************************************/
package com.hungle.sunriise.mnyobject.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.hungle.sunriise.mnyobject.Account;
import com.hungle.sunriise.mnyobject.Currency;
import com.hungle.sunriise.mnyobject.EnumAccountType;
import com.hungle.sunriise.mnyobject.EnumInvestmentSubType;
import com.hungle.sunriise.mnyobject.MnyObject;
import com.hungle.sunriise.mnyobject.SecurityHolding;
import com.hungle.sunriise.mnyobject.Transaction;

/**
 * An Account in MsMoney.
 */
@JsonSerialize(as = Account.class)
@Entity
@Table(name = "Account")
public class DefaultAccount extends MnyObject implements Account {

    /** Name of the account. */
    private String name;

    /** Account type. */
    private EnumAccountType accountType;

    /** The starting balance. */
    // @JsonSerialize(using = BalanceSerializer.class)
    private BigDecimal startingBalance;

    /** The current balance. */
    // @JsonSerialize(using = BalanceSerializer.class)
    private BigDecimal currentBalance;

    /** True if account is closed. */
    private Boolean closed;

    /**
     * Account can be linked: for example, Investment and Cash accounts are linked.
     * RelatedToAccount provides the linkage.
     */
    // @JsonSerialize(using = RelatedToAccountSerializer.class)
    @Transient
    private Account relatedToAccount;

    /**
     * True if this is a retirement account: 401k ...
     */
    private Boolean retirement;

    /**
     * Investment sub type. Only meaningful if this is an investment account.
     */
    private EnumInvestmentSubType investmentSubType;

    /**
     * A list of security (stocks, bonds ...) holding. Only meaningful if this is an
     * investment account.
     */
    @Transient
    private List<SecurityHolding> securityHoldings;

    /**
     * Credit card limit. Only meaningful if this is credit card account.
     */
    private BigDecimal creditCardAmountLimit;

    /**
     * List of transactions belong to this account.
     */
    @JsonIgnore
    @Transient
    private List<Transaction> transactions;

    /**
     * List of transactions belong to this account but not contributing to the
     * balance. For example: recurring transactions. XXX: Not really sure why
     * MsMoney does it this way.
     */
    @JsonIgnore
    @Transient
    private List<Transaction> filteredTransactions;

    /** The currency. */
    @Transient
    private com.hungle.sunriise.mnyobject.Currency currency;

    /**
     * Instantiates a new default account.
     */
    public DefaultAccount() {
        super();
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.hungle.sunriise.mnyobject.Account#formatAmmount(java.math.BigDecimal)
     */
    // @Override
    // public String formatAmmount(BigDecimal amount) {
    // return amountFormatter.format(amount);
    // }

    /*
     * (non-Javadoc)
     * 
     * @see com.hungle.sunriise.mnyobject.Account#formatSecurityQuantity(java.lang.
     * Double)
     */
    // @Override
    // public String formatSecurityQuantity(Double quantity) {
    // return securityQuantityFormatter.format(quantity);
    // }

    /*
     * (non-Javadoc)
     * 
     * @see com.hungle.sunriise.mnyobject.Account#getAccountType()
     */
    @Override
    public EnumAccountType getAccountType() {
        return accountType;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.hungle.sunriise.mnyobject.Account#getClosed()
     */
    @Override
    public Boolean getClosed() {
        return closed;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.hungle.sunriise.mnyobject.Account#getCreditCardAmountLimit()
     */
    @Override
    public BigDecimal getCreditCardAmountLimit() {
        return creditCardAmountLimit;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.hungle.sunriise.mnyobject.Account#getCurrentBalance()
     */
    @Override
    public BigDecimal getCurrentBalance() {
        return currentBalance;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.hungle.sunriise.mnyobject.Account#getFilteredTransactions()
     */
    @Override
    public List<Transaction> getFilteredTransactions() {
        return filteredTransactions;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.hungle.sunriise.mnyobject.Account#getInvestmentSubType()
     */
    @Override
    public EnumInvestmentSubType getInvestmentSubType() {
        return investmentSubType;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.hungle.sunriise.mnyobject.Account#getName()
     */
    @Override
    public String getName() {
        return name;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.hungle.sunriise.mnyobject.Account#getRelatedToAccount()
     */
    @Override
    public Account getRelatedToAccount() {
        return relatedToAccount;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.hungle.sunriise.mnyobject.Account#getRetirement()
     */
    @Override
    public Boolean getRetirement() {
        return retirement;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.hungle.sunriise.mnyobject.Account#getSecurityHoldings()
     */
    @Override
    public List<SecurityHolding> getSecurityHoldings() {
        return securityHoldings;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.hungle.sunriise.mnyobject.Account#getStartingBalance()
     */
    @Override
    public BigDecimal getStartingBalance() {
        if (startingBalance == null) {
            return new BigDecimal(0.0);
        }
        return startingBalance;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.hungle.sunriise.mnyobject.Account#getTransactions()
     */
    @Override
    public List<Transaction> getTransactions() {
        return transactions;
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.hungle.sunriise.mnyobject.Account#setAccountType(com.hungle.sunriise.
     * mnyobject.AccountType)
     */
    @Override
    public void setAccountType(EnumAccountType accountType) {
        this.accountType = accountType;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.hungle.sunriise.mnyobject.Account#setClosed(java.lang.Boolean)
     */
    @Override
    public void setClosed(Boolean closed) {
        this.closed = closed;
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.hungle.sunriise.mnyobject.Account#setCreditCardAmountLimit(java.math.
     * BigDecimal)
     */
    @Override
    public void setCreditCardAmountLimit(BigDecimal creditCardAmountLimit) {
        this.creditCardAmountLimit = creditCardAmountLimit;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.hungle.sunriise.mnyobject.Account#setCurrentBalance(java.math.
     * BigDecimal)
     */
    @Override
    public void setCurrentBalance(BigDecimal currentBalance) {
        this.currentBalance = currentBalance;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.hungle.sunriise.mnyobject.Account#setFilteredTransactions(java.util.
     * List)
     */
    @JsonIgnore
    @Override
    public void setFilteredTransactions(List<Transaction> filteredTransactions) {
        this.filteredTransactions = filteredTransactions;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.hungle.sunriise.mnyobject.Account#setInvestmentSubType(com.hungle.
     * sunriise.mnyobject.InvestmentSubType)
     */
    @Override
    public void setInvestmentSubType(EnumInvestmentSubType investmentSubType) {
        this.investmentSubType = investmentSubType;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.hungle.sunriise.mnyobject.Account#setName(java.lang.String)
     */
    @Override
    public void setName(String name) {
        this.name = name;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.hungle.sunriise.mnyobject.Account#setRelatedToAccount(com.hungle.
     * sunriise.mnyobject.Account)
     */
    @Override
    public void setRelatedToAccount(Account relatedToAccount) {
        this.relatedToAccount = relatedToAccount;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.hungle.sunriise.mnyobject.Account#setRetirement(java.lang.Boolean)
     */
    @Override
    public void setRetirement(Boolean retirement) {
        this.retirement = retirement;
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.hungle.sunriise.mnyobject.Account#setSecurityHoldings(java.util.List)
     */
    @Override
    public void setSecurityHoldings(List<SecurityHolding> securityHoldings) {
        this.securityHoldings = securityHoldings;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.hungle.sunriise.mnyobject.Account#setStartingBalance(java.math.
     * BigDecimal)
     */
    @Override
    public void setStartingBalance(BigDecimal startingBalance) {
        this.startingBalance = startingBalance;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.hungle.sunriise.mnyobject.Account#setTransactions(java.util.List)
     */
    @JsonIgnore
    @Override
    public void setTransactions(List<Transaction> transactions) {
        this.transactions = transactions;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return name;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.hungle.sunriise.mnyobject.Account#setCurrency(com.hungle.sunriise.
     * mnyobject.MnyCurrency)
     */
    @Override
    public void setCurrency(Currency currency) {
        this.currency = currency;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.hungle.sunriise.mnyobject.Account#getCurrency()
     */
    @Override
    public Currency getCurrency() {
        // TODO Auto-generated method stub
        return currency;
    }

    @Override
    public Stream<Transaction> getTransactionStream() {
        List<Transaction> transactions = getTransactions();
        if (transactions == null) {
            transactions = new ArrayList<>();
        }
        return transactions.stream();
    }
}

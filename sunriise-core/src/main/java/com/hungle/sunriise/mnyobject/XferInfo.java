package com.hungle.sunriise.mnyobject;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.hungle.sunriise.mnyobject.impl.DefaultXferInfo;

// TODO: Auto-generated Javadoc
/**
 * The Interface XferInfo.
 */
@JsonDeserialize(as = DefaultXferInfo.class)
public interface XferInfo {

    /**
     * Gets the xfer account id.
     *
     * @return the xfer account id
     */
    public abstract Integer getXferAccountId();

    /**
     * Gets the xfer transaction id.
     *
     * @return the xfer transaction id
     */
    public abstract Integer getXferTransactionId();

    /**
     * Sets the xfer account id.
     *
     * @param xferAccountId the new xfer account id
     */
    public abstract void setXferAccountId(Integer xferAccountId);

    /**
     * Sets the xfer transaction id.
     *
     * @param xferTransactionId the new xfer transaction id
     */
    public abstract void setXferTransactionId(Integer xferTransactionId);

}

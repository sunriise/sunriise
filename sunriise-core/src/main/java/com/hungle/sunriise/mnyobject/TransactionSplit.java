/*******************************************************************************
 * Copyright (c) 2013 Hung Le
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *******************************************************************************/

package com.hungle.sunriise.mnyobject;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.hungle.sunriise.mnyobject.impl.DefaultTransactionSplit;

// TODO: Auto-generated Javadoc
/**
 * The Interface TransactionSplit.
 */
@JsonDeserialize(as = DefaultTransactionSplit.class)
public interface TransactionSplit extends MnyObjectInterface {

    /**
     * Gets the parent id.
     *
     * @return the parent id
     */
    public abstract Integer getParentId();

    /**
     * Sets the parent id.
     *
     * @param parentId the new parent id
     */
    public abstract void setParentId(Integer parentId);

    /**
     * Gets the row id.
     *
     * @return the row id
     */
    public abstract Integer getRowId();

    /**
     * Sets the row id.
     *
     * @param rowId the new row id
     */
    public abstract void setRowId(Integer rowId);

    /**
     * Gets the transaction.
     *
     * @return the transaction
     */
    public abstract Transaction getTransaction();

    /**
     * Sets the transaction.
     *
     * @param transaction the new transaction
     */
    public abstract void setTransaction(Transaction transaction);

}
/*******************************************************************************
 * Copyright (c) 2013 Hung Le
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *******************************************************************************/

package com.hungle.sunriise.mnyobject;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.hungle.sunriise.mnyobject.impl.DefaultFrequency;

// TODO: Auto-generated Javadoc
/**
 * The Interface Frequency.
 */
@JsonDeserialize(as = DefaultFrequency.class)
public interface Frequency {

    // public abstract Integer getGrftt();
    //
    // public abstract void setGrftt(Integer grftt);

    /** The frq yearly. */
    int FRQ_YEARLY = 5;

    /** The frq every 3 months. */
    int FRQ_EVERY_3_MONTHS = 4;

    /** The frq monthly. */
    int FRQ_MONTHLY = 3;

    /** The frq weekly. */
    int FRQ_WEEKLY = 2;

    /** The frq daily. */
    int FRQ_DAILY = 1;

    /** The frq once. */
    int FRQ_ONCE = 0;

    /** The default frq instruction. */
    double DEFAULT_FRQ_INSTRUCTION = 1.0;

    /**
     * Gets the frq.
     *
     * @return the frq
     */
    public abstract Integer getType();

    /**
     * Sets the frq.
     *
     * @param frq the new frq
     */
    public abstract void setType(Integer frq);

    /**
     * Gets the c frq inst.
     *
     * @return the c frq inst
     */
    @JsonIgnore
    public abstract Double getcFrqInst();

    /**
     * Sets the c frq inst.
     *
     * @param cFrqInst the new c frq inst
     */
    public abstract void setcFrqInst(Double cFrqInst);

    /**
     * Checks if is recurring.
     *
     * @return true, if is recurring
     */
    public abstract boolean isRecurring();

    /**
     * Gets the frequency string.
     *
     * @return the frequency string
     */
    public abstract String getLabel();

}
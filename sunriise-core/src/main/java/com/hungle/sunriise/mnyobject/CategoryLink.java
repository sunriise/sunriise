package com.hungle.sunriise.mnyobject;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.annotation.JsonView;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.hungle.sunriise.mnyobject.impl.DefaultCategoryLink;

/**
 * The Interface Category.
 */
@JsonPropertyOrder({ "id", "name", "fullName", "parentId", "parentName", })
@JsonDeserialize(as = DefaultCategoryLink.class)
public interface CategoryLink extends MnyObjectInterface {

    /**
     * Gets the full name.
     *
     * @return the full name
     */
    @JsonView(View.Summary.class)
    String getFullName();

    /**
     * Gets the name.
     *
     * @return the name
     */
    String getName();

    Integer getParentId();

    String getParentName();

    /**
     * Sets the full name.
     *
     * @param fullName the new full name
     */
    void setFullName(String fullName);

    /**
     * Sets the name.
     *
     * @param name the new name
     */
    void setName(String name);

    void setParentId(Integer parentId);

    void setParentName(String parentName);
}
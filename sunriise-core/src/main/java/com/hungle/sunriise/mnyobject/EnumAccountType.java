/*******************************************************************************
 * Copyright (c) 2010-2017 Hung Le
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *******************************************************************************/
package com.hungle.sunriise.mnyobject;

import org.apache.logging.log4j.Logger;

// TODO: Auto-generated Javadoc
/**
 * Account's type.
 */
public enum EnumAccountType {

    /** Banking. */
    BANKING(0),

    /** Credit card. */
    CREDIT_CARD(1),

    /**
     * Cash account. Often associated with an investment account.
     */
    CASH(2),

    /** Asset such as house. */
    ASSET(3),

    /**
     * Liability account: money you own somesome else.
     */
    LIABILITY(4),

    /**
     * Investment account: where to buy/sell stocks, bonds ...
     */
    INVESTMENT(5),

    /**
     * Loan: for example home loan.
     */
    LOAN(6),

    /**
     * Unknown type. Please file a bug if you see an unknown account type.
     */
    UNKNOWN(-1);

    /** The Constant LOGGER. */
    private static final Logger LOGGER = org.apache.logging.log4j.LogManager.getLogger(EnumAccountType.class);

    /** Internal representation of an account type. */
    private final int mnyType;

    /**
     * Instantiates a new account type.
     *
     * @param mnyType the mny type
     */
    EnumAccountType(int mnyType) {
        this.mnyType = mnyType;
    }

    /**
     * Gets the mny type.
     *
     * @return the mny type
     */
    public int getMnyType() {
        return mnyType;
    }

    /**
     * Checks if is credit card.
     *
     * @param account the account
     * @return true, if is credit card
     */
    public static final boolean isCreditCard(final Account account) {
        return account.getAccountType() == CREDIT_CARD;
    }

    /**
     * Checks if is investment.
     *
     * @param account the account
     * @return true, if is investment
     */
    public static boolean isInvestment(Account account) {
        return account.getAccountType() == INVESTMENT;
    }

    /**
     * To account type.
     *
     * @param mnyType the mny type
     * @return the account type
     */
    public static EnumAccountType toAccountType(int mnyType) {
        switch (mnyType) {
        case 0:
            return BANKING;
        case 1:
            return CREDIT_CARD;
        case 2:
            return CASH;
        case 3:
            return ASSET;
        case 4:
            return LIABILITY;
        case 5:
            return INVESTMENT;
        case 6:
            return LOAN;
        default:
            LOGGER.warn("Invalid mnyType=" + mnyType);
            return UNKNOWN;
        }
    }
}

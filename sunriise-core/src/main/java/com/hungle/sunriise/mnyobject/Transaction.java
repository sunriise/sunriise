/*******************************************************************************
 * Copyright (c) 2013 Hung Le
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *******************************************************************************/

package com.hungle.sunriise.mnyobject;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.persistence.OneToOne;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.annotation.JsonView;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.hungle.sunriise.json.BalanceSerializer;
import com.hungle.sunriise.mnyobject.impl.DefaultAccountLink;
import com.hungle.sunriise.mnyobject.impl.DefaultTransaction;
import com.kjetland.jackson.jsonSchema.annotations.JsonSchemaFormat;

// TODO: Auto-generated Javadoc
/**
 * The Interface Transaction.
 */
@JsonPropertyOrder({ "id", "date", "number", "amount", "category", "memo", "payee", "splits", "account", "void",
        "cleared", "reconciled", "unaccepted", "transfer", "xferInfo", "investment", "investmentInfo", "recurring",
        "frequency", "classification1", "fiTransactionId", })
@JsonDeserialize(as = DefaultTransaction.class)
public interface Transaction extends MnyObjectInterface {

    /**
     * Gets the account id.
     *
     * @return the account id
     */
    @OneToOne(targetEntity = DefaultAccountLink.class)
    public abstract AccountLink getAccount();

    /**
     * Gets the amount.
     *
     * @return the amount
     */
    @JsonView(View.Summary.class)
    @JsonSerialize(using = BalanceSerializer.class)
    public abstract BigDecimal getAmount();

    /**
     * Gets the category id.
     *
     * @return the category id
     */
    @JsonView(View.Summary.class)
    public abstract CategoryLink getCategory();

    /**
     * Gets the classification1 id.
     *
     * @return the classification1 id
     */
    public abstract Classification1Link getClassification1();

    /**
     * Gets the cleared state.
     *
     * @return the cleared state
     */
    @JsonIgnore
    public abstract Integer getClearedState();

    /**
     * Gets the date.
     *
     * @return the date
     */
    // @JsonSerialize(using = MnyDateSerializer.class)
    @JsonView(View.Summary.class)
    @JsonSchemaFormat("date-time")
//    @JsonFormat(pattern="yyyy-MM-dd'T'HH:mm:ss.SSSZ")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSSZ")
    public abstract Date getDate();

    /**
     * Gets the fi transaction id.
     *
     * @return the fi transaction id
     */
    // mFiStmtId
//    @JsonIgnore
    public abstract String getFiTransactionId();

    /**
     * Gets the frequency.
     *
     * @return the frequency
     */
    public abstract Frequency getFrequency();

    /**
     * Gets the memo.
     *
     * @return the memo
     */
    @JsonView(View.Summary.class)
    public abstract String getMemo();

    /**
     * Gets the number.
     *
     * @return the number
     */
    @JsonView(View.Summary.class)
    public abstract String getNumber();

    /**
     * Gets the running balance.
     *
     * @return the running balance
     */
    @JsonIgnore
    public abstract BigDecimal getRunningBalance();

    /**
     * Gets the splits.
     *
     * @return the splits
     */
    public abstract List<TransactionSplit> getSplits();

    /**
     * Gets the state.
     *
     * @return the state
     */
    @JsonIgnore
    public abstract EnumTransactionState getState();

    /**
     * Gets the status flag.
     *
     * @return the status flag
     */
    @JsonIgnore
    public abstract Integer getStatusFlag();

    /**
     * Gets the transaction info.
     *
     * @return the transaction info
     */
    @JsonIgnore
    public abstract TransactionInfo getTransactionInfo();

    /**
     * Checks for splits.
     *
     * @return true, if successful
     */
    public abstract boolean hasSplits();

    /**
     * Checks if is cleared.
     *
     * @return true, if is cleared
     */
    public abstract boolean isCleared();

    /**
     * Checks if is investment.
     *
     * @return true, if is investment
     */
    public abstract boolean isInvestment();

    /**
     * Checks if is reconciled.
     *
     * @return true, if is reconciled
     */
    public abstract boolean isReconciled();

    /**
     * Checks if is recurring.
     *
     * @return true, if is recurring
     */
    public abstract boolean isRecurring();

    /**
     * Checks if is transfer.
     *
     * @return true, if is transfer
     */
    public abstract boolean isTransfer();

    /**
     * Checks if is unaccepted.
     *
     * @return true, if is unaccepted
     */
    @JsonIgnore
    public abstract boolean isUnaccepted();

    /**
     * Checks if is void.
     *
     * @return true, if is void
     */
    public abstract boolean isVoid();

    /**
     * Sets the account id.
     *
     * @param account the new account id
     */
    public abstract void setAccount(AccountLink account);

    /**
     * Sets the category id.
     *
     * @param category the new category id
     */
    public abstract void setCategory(CategoryLink category);

    /**
     * Sets the classification1 id.
     *
     * @param classification1Id the new classification1 id
     */
    public abstract void setClassification1(Classification1Link classification1Id);

    /**
     * Sets the cleared state.
     *
     * @param clearedState the new cleared state
     */
    public abstract void setClearedState(Integer clearedState);

    /**
     * Sets the date.
     *
     * @param date the new date
     */
    @JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss.SSSZ")
    public abstract void setDate(Date date);

    /**
     * Sets the fi transaction id.
     *
     * @param fiTransactionId the new fi transaction id
     */
    public abstract void setFiTransactionId(String fiTransactionId);

    /**
     * Sets the frequency.
     *
     * @param frequency the new frequency
     */
    public abstract void setFrequency(Frequency frequency);

    /**
     * Sets the memo.
     *
     * @param memo the new memo
     */
    public abstract void setMemo(String memo);

    /**
     * Sets the number.
     *
     * @param number the new number
     */
    public abstract void setNumber(String number);

    /**
     * Sets the running balance.
     *
     * @param runningBalance the new running balance
     */
    public abstract void setRunningBalance(BigDecimal runningBalance);

    /**
     * Sets the splits.
     *
     * @param splits the new splits
     */
    public abstract void setSplits(List<TransactionSplit> splits);

    /**
     * Sets the state.
     *
     * @param state the new state
     */
    public abstract void setState(EnumTransactionState state);

    /**
     * Sets the status flag.
     *
     * @param status the new status flag
     */
    public abstract void setStatusFlag(Integer status);

    /**
     * Sets the transaction info.
     *
     * @param transactionInfo the new transaction info
     */
    public abstract void setTransactionInfo(TransactionInfo transactionInfo);

    /**
     * Sets the unaccepted.
     *
     * @param unaccepted the new unaccepted
     */
    public abstract void setUnaccepted(boolean unaccepted);

    /**
     * Sets the xfer info.
     *
     * @param xferInfo the new xfer info
     */
    public abstract void setXferInfo(XferInfo xferInfo);

    /**
     * Gets the xfer info.
     *
     * @return the xfer info
     */
    public abstract XferInfo getXferInfo();

    /**
     * Sets the investment info.
     *
     * @param investmentInfo the new investment info
     */
    public abstract void setInvestmentInfo(InvestmentInfo investmentInfo);

    /**
     * Gets the investment info.
     *
     * @return the investment info
     */
    public abstract InvestmentInfo getInvestmentInfo();

    /**
     * Sets the amount.
     *
     * @param amount the new amount
     */
    public abstract void setAmount(BigDecimal amount);

    /**
     * Sets the payee.
     *
     * @param payeeInfo the new payee
     */
    public abstract void setPayee(PayeeLink payeeInfo);

    /**
     * Gets the payee.
     *
     * @return the payee
     */
    @JsonView(View.Summary.class)
    public abstract PayeeLink getPayee();

}
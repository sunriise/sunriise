package com.hungle.sunriise.mnyobject.impl;

import javax.persistence.Entity;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.hungle.sunriise.mnyobject.CategoryLink;
import com.hungle.sunriise.mnyobject.MnyObject;

/**
 * The Class DefaultCategory.
 */
@JsonSerialize(as = CategoryLink.class)
@Entity
public final class DefaultCategoryLink extends MnyObject implements CategoryLink {

    public DefaultCategoryLink() {
        super();
        // TODO Auto-generated constructor stub
    }

    /** The full name. */
    private String fullName;

    /** The name. */
    private String name;

    private Integer parentId;

    private String parentName;

    /**
     * Instantiates a new default category.
     *
     * @param id the id
     */
    public DefaultCategoryLink(Integer id) {
        setId(id);
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.hungle.sunriise.mnyobject.Transaction.Category#setName(java.lang.String)
     */
    @Override
    public void setName(String name) {
        this.name = name;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.hungle.sunriise.mnyobject.Transaction.Category#getName()
     */
    @Override
    public String getName() {
        return name;
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.hungle.sunriise.mnyobject.Transaction.Category#setFullName(java.lang.
     * String)
     */
    @Override
    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.hungle.sunriise.mnyobject.Transaction.Category#getFullName()
     */
    @Override
    public String getFullName() {
        return fullName;
    }

    @Override
    public Integer getParentId() {
        return parentId;
    }

    @Override
    public String getParentName() {
        return parentName;
    }

    @Override
    public void setParentId(Integer parentId) {
        this.parentId = parentId;
    }

    @Override
    public void setParentName(String parentName) {
        this.parentName = parentName;
    }

}
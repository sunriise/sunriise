/*******************************************************************************
 * Copyright (c) 2010 Hung Le
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *******************************************************************************/
package com.hungle.sunriise.mnyobject.impl;

import com.fasterxml.jackson.annotation.JsonIgnore;

// TODO: Auto-generated Javadoc
/**
 * The Class DefaultInvestmentActivity.
 */
public class InvestmentActivity {

    /** The flag. */
    private final Integer type;

    /** The string. */
    private String string;

    /** The added. */
    @JsonIgnore
    private boolean added = true;

    public InvestmentActivity() {
        super();
        this.type = -1;
        this.string = InvestmentActivityTable.ACTIVITY_UNKNOWN_STR;
        this.added = true;
    }

    /**
     * Instantiates a new default investment activity.
     *
     * @param flag the flag
     */
    public InvestmentActivity(Integer flag) {
        super();
        this.type = flag;
        initialize();
    }

    public InvestmentActivity(String str) {
        super();
        this.string = str;
        IAKeyValue value = InvestmentActivityTable.getInstance().getByValue(str);
        if (value != null) {
            this.type = value.getKey();
            this.added = value.isAdded();
        } else {
            this.type = -1;
            this.string = InvestmentActivityTable.ACTIVITY_UNKNOWN_STR;
        }
    }

    /**
     * Gets the flag.
     *
     * @return the flag
     */
    public Integer getType() {
        return type;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return string;
    }

    /**
     * Gets the label.
     *
     * @return the label
     */
    public String getLabel() {
        return toString();
    }

    public void setLabel(String label) {
        this.string = label;
    }

    /**
     * Checks if is added.
     *
     * @return true, if is added
     */
    public boolean isAdded() {
        return added;
    }

    private void initialize() {
        this.added = true;

        String str = InvestmentActivityTable.ACTIVITY_UNKNOWN_STR;
        IAKeyValue value = InvestmentActivityTable.getInstance().getByType(type);
        if (value != null) {
            str = value.getValue();
        }

        this.string = str;
    }
}

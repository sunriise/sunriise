package com.hungle.sunriise.mnyobject;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.annotation.JsonView;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.hungle.sunriise.mnyobject.impl.DefaultPayeeLink;

/**
 * The Interface Payee.
 */
@JsonPropertyOrder({ "id", "name", })
@JsonDeserialize(as = DefaultPayeeLink.class)
public interface PayeeLink extends MnyObjectInterface {

    /**
     * Sets the name.
     *
     * @param name the new name
     */
    void setName(String name);

    /**
     * Gets the name.
     *
     * @return the name
     */
    @JsonView(View.Summary.class)
    String getName();

}
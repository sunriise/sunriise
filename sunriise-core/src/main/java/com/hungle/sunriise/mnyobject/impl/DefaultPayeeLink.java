package com.hungle.sunriise.mnyobject.impl;

import javax.persistence.Entity;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.hungle.sunriise.mnyobject.MnyObject;
import com.hungle.sunriise.mnyobject.PayeeLink;

/**
 * The Class DefaultPayee.
 */
@JsonSerialize(as = PayeeLink.class)
@Entity
public final class DefaultPayeeLink extends MnyObject implements PayeeLink {

    public DefaultPayeeLink() {
        super();
    }

    /** The name. */
    private String name;

    /**
     * Instantiates a new default payee.
     *
     * @param id the id
     */
    public DefaultPayeeLink(Integer id) {
        setId(id);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.hungle.sunriise.mnyobject.Transaction.Payee#getName()
     */
    @Override
    public String getName() {
        return name;
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.hungle.sunriise.mnyobject.Transaction.Payee#setName(java.lang.String)
     */
    @Override
    public void setName(String name) {
        this.name = name;
    }

}
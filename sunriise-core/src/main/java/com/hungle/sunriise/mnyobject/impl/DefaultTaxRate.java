package com.hungle.sunriise.mnyobject.impl;

import java.util.List;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.hungle.sunriise.mnyobject.MnyObject;
import com.hungle.sunriise.mnyobject.TaxRate;

@JsonSerialize(as = TaxRate.class)
//@Entity
public class DefaultTaxRate extends MnyObject implements TaxRate {
    private Integer taxYear;

    private String name;

    private List<TaxBracket> taxBrackets;

    // = row.getDouble("damtLow" + index);
    // private Double bracketLow[] = new Double[6];
    // = row.getDouble("damtHigh" + index);
    // private Double bracketHigh[] = new Double[6];
    // = row.getDouble("dRate" + index);
    // private Double bracketRate[] = new Double[6];

    // dRateCapGains
    // this.ltcGainsRateTextField = textField_20;
    // setDouble(row, "dRateCapGains", ltcGainsRateTextField);
    private Double ltcGainsRate;

    // dRateDividends
    // this.dividendsRateTextField = textField_21;
    // setDouble(row, "dRateDividends", dividendsRateTextField);
    private Double dividendsRate;

    // damtStdDed
    // this.standardDeductionTextField = textField_30;
    // setDouble(row, "damtStdDed", standardDeductionTextField);
    private Double standardDeduction;

    // damtDedBlind
    // this.blindTextField = textField_31;
    // setDouble(row, "damtDedBlind", blindTextField);
    private Double blind;

    // damtStdEx
    // this.exemptionAmountTextField = textField_32;
    // setDouble(row, "damtStdEx", exemptionAmountTextField);
    private Double exemptionAmount;

    // damtDedOver65
    // this.over65TextField = textField_33;
    // setDouble(row, "damtDedOver65", over65TextField);
    private Double over65;

    // damtThreshDed
    // this.exemptionCutoffTextField = textField_34;
    // setDouble(row, "damtThreshExemp", exemptionCutoffTextField);
    private Double exemptionCutoff;

    // this.deductionCutoffTextField = textField_35;
    // setDouble(row, "damtThreshDed", deductionCutoffTextField);
    private Double deductionCutoff;

    // damtMaxCapLoss
    // setDouble(row, "damtMaxCapLoss", maximumCapitalLossTextFile);
    private Double maximumCapitalLoss;

    @Override
    public Integer getTaxYear() {
        return taxYear;
    }

    @Override
    public void setTaxYear(Integer taxYear) {
        this.taxYear = taxYear;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public List<TaxBracket> getTaxBrackets() {
        return taxBrackets;
    }

    @Override
    public void setTaxBrackets(List<TaxBracket> taxBrackets) {
        this.taxBrackets = taxBrackets;
    }

    @Override
    public Double getLtcGainsRate() {
        return ltcGainsRate;
    }

    @Override
    public void setLtcGainsRate(Double ltcGainsRate) {
        this.ltcGainsRate = ltcGainsRate;
    }

    @Override
    public Double getDividendsRate() {
        return dividendsRate;
    }

    @Override
    public void setDividendsRate(Double dividendsRate) {
        this.dividendsRate = dividendsRate;
    }

    @Override
    public Double getStandardDeduction() {
        return standardDeduction;
    }

    @Override
    public void setStandardDeduction(Double standardDeduction) {
        this.standardDeduction = standardDeduction;
    }

    @Override
    public Double getBlind() {
        return blind;
    }

    @Override
    public void setBlind(Double blind) {
        this.blind = blind;
    }

    @Override
    public Double getExemptionAmount() {
        return exemptionAmount;
    }

    @Override
    public void setExemptionAmount(Double exemptionAmount) {
        this.exemptionAmount = exemptionAmount;
    }

    @Override
    public Double getOver65() {
        return over65;
    }

    @Override
    public void setOver65(Double over65) {
        this.over65 = over65;
    }

    @Override
    public Double getExemptionCutoff() {
        return exemptionCutoff;
    }

    @Override
    public void setExemptionCutoff(Double exemptionCutoff) {
        this.exemptionCutoff = exemptionCutoff;
    }

    @Override
    public Double getDeductionCutoff() {
        return deductionCutoff;
    }

    @Override
    public void setDeductionCutoff(Double deductionCutoff) {
        this.deductionCutoff = deductionCutoff;
    }

    @Override
    public Double getMaximumCapitalLoss() {
        return maximumCapitalLoss;
    }

    @Override
    public void setMaximumCapitalLoss(Double maximumCapitalLoss) {
        this.maximumCapitalLoss = maximumCapitalLoss;
    }

}

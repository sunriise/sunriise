package com.hungle.sunriise.mnyobject;

import java.util.List;

import com.hungle.sunriise.mnyobject.impl.TaxBracket;

public interface TaxRate extends MnyObjectInterface {
    public abstract void setTaxYear(Integer taxYear);

    public abstract Integer getTaxYear();

    public abstract void setName(String name);

    public abstract String getName();

    public abstract void setTaxBrackets(List<TaxBracket> taxBrackets);

    public abstract List<TaxBracket> getTaxBrackets();

    void setLtcGainsRate(Double ltcGainsRate);

    Double getLtcGainsRate();

    void setMaximumCapitalLoss(Double maximumCapitalLoss);

    Double getMaximumCapitalLoss();

    void setDeductionCutoff(Double deductionCutoff);

    Double getDeductionCutoff();

    void setExemptionCutoff(Double exemptionCutoff);

    Double getExemptionCutoff();

    void setOver65(Double over65);

    Double getOver65();

    void setExemptionAmount(Double exemptionAmount);

    Double getExemptionAmount();

    void setBlind(Double blind);

    Double getBlind();

    void setStandardDeduction(Double standardDeduction);

    Double getStandardDeduction();

    void setDividendsRate(Double dividendsRate);

    Double getDividendsRate();
}

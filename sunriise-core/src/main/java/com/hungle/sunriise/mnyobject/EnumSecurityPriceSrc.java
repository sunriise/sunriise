/*******************************************************************************
 * Copyright (c) 2010-2017 Hung Le
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *******************************************************************************/
package com.hungle.sunriise.mnyobject;

import org.apache.logging.log4j.Logger;

// TODO: Auto-generated Javadoc
/**
 * Account's type.
 */
public enum EnumSecurityPriceSrc {

    /** TODO_0. */
    TODO_0(0),

    /** Buy. */
    BUY(1),

    /**
     * SELL
     */
    SELL(2),

    /** TODO_3. */
    TODO_3(3),

    /**
     * TODO_4
     */
    TODO_4(4),

    /**
     * UPDATE
     */
    UPDATE(5),

    /**
     * ONLINE
     */
    ONLINE(6),

    /**
     * Unknown type. Please file a bug if you see an unknown type.
     */
    UNKNOWN(-1);

    /** The Constant LOGGER. */
    private static final Logger LOGGER = org.apache.logging.log4j.LogManager.getLogger(EnumSecurityPriceSrc.class);

    /** Internal representation of an account type. */
    private final int type;

    /**
     * Instantiates a new account type.
     *
     * @param type the mny type
     */
    EnumSecurityPriceSrc(int type) {
        this.type = type;
    }

    /**
     * Gets the mny type.
     *
     * @return the mny type
     */
    public int getType() {
        return type;
    }

    /**
     * To account type.
     *
     * @param src the mny type
     * @return the account type
     */
    public static EnumSecurityPriceSrc toEnum(int src) {
        switch (src) {
        case 0:
            return TODO_0;
        case 1:
            return BUY;
        case 2:
            return SELL;
        case 3:
            return TODO_3;
        case 4:
            return TODO_4;
        case 5:
            return UPDATE;
        case 6:
            return ONLINE;
        default:
            LOGGER.warn("Invalid src=" + src);
            return UNKNOWN;
        }
    }
}

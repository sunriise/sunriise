/*******************************************************************************
 * Copyright (c) 2013 Hung Le
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *******************************************************************************/

package com.hungle.sunriise.mnyobject;

import java.math.BigDecimal;
import java.util.List;
import java.util.stream.Stream;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.annotation.JsonView;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.hungle.sunriise.json.AccountTransactionsSerializer;
import com.hungle.sunriise.json.BalanceSerializer;
import com.hungle.sunriise.json.RelatedToAccountSerializer;
import com.hungle.sunriise.mnyobject.impl.DefaultAccount;
import com.kjetland.jackson.jsonSchema.annotations.JsonSchemaDescription;

/**
 * This interface represents a msmoney Account.
 */
@JsonPropertyOrder({ "id", "name", "accountType", "currency", "startingBalance", "currentBalance", "transactions",
        "relatedToAccount", "closed", "retirement", "investmentSubType", "securityHoldings", })
@JsonDeserialize(as = DefaultAccount.class)
public interface Account extends MnyObjectInterface {

    /**
     * This account type.
     *
     * @return the account type
     */
    @JsonView(View.Summary.class)
    @JsonSchemaDescription("This account type.")
    public abstract EnumAccountType getAccountType();

    /**
     * True if account is closed.
     *
     * @return the closed
     */
    @JsonSchemaDescription("True if account is closed.")
    public abstract Boolean getClosed();

    /**
     * For credit card, card amount limit.
     *
     * @return the amount limit
     */
    @JsonSchemaDescription("For credit card, card amount limit.")
    public abstract BigDecimal getCreditCardAmountLimit();

    /**
     * Current balance for this account.
     *
     * @return the current balance
     */
    @JsonSchemaDescription("Current balance for this account.")
    @JsonSerialize(using = BalanceSerializer.class)
    public abstract BigDecimal getCurrentBalance();

    /**
     * A list of transactions associated to this account but does NOT contributed to
     * its balance. Maybe a list of scheduled transactions?
     * 
     * @return the filtered transactions
     */
    @JsonSchemaDescription("A list of transactions associated to this account but does NOT contributed to its balance.")
    @JsonSerialize(using = AccountTransactionsSerializer.class)
    @JsonIgnore
    public abstract List<Transaction> getFilteredTransactions();

    /**
     * An investment sub type. Only meaningful for investment account.
     *
     * @return the investment sub type
     */
    @JsonSchemaDescription("Only meaningful for investment account.")
    public abstract EnumInvestmentSubType getInvestmentSubType();

    /**
     * Name of the account. This value is mutable. User can change it.
     *
     * @return the name
     */
    @JsonView(View.Summary.class)
    @JsonSchemaDescription("Name of the account. This value is mutable. User can change it.")
    public abstract String getName();

    /**
     * Non-null only for Investment account which has associated Cash account and
     * vice-versa.
     *
     * @return the related to account
     */
    @JsonSchemaDescription("Non-null only for Investment account which has associated Cash account and vice-versa.")
    @JsonSerialize(using = RelatedToAccountSerializer.class)
    public abstract Account getRelatedToAccount();

    /**
     * True if this account is a retirement account.
     *
     * @return the retirement
     */
    @JsonSchemaDescription("True if this account is a retirement account.")
    public abstract Boolean getRetirement();

    /**
     * Only for Investment account, list of security holding.
     *
     * @return the security holdings
     */
    @JsonSchemaDescription("Only for Investment account, list of security holding.")
    public abstract List<SecurityHolding> getSecurityHoldings();

    /**
     * Account starting balance.
     *
     * @return the starting balance
     */
    @JsonSchemaDescription("Account starting balance.")
    @JsonSerialize(using = BalanceSerializer.class)
    public abstract BigDecimal getStartingBalance();

    /**
     * List of transactions.
     *
     * @return the transactions
     */
    @JsonSchemaDescription("List of transactions.")
    @JsonSerialize(using = AccountTransactionsSerializer.class)
    public abstract List<Transaction> getTransactions();

    /**
     * Sets the account type.
     *
     * @param accountType the new account type
     */
    public abstract void setAccountType(EnumAccountType accountType);

    /**
     * Sets the closed flag.
     *
     * @param closed the new closed
     */
    public abstract void setClosed(Boolean closed);

    /**
     * Sets the amount limit.
     *
     * @param amountLimit the new amount limit
     */
    public abstract void setCreditCardAmountLimit(BigDecimal amountLimit);

    /**
     * Sets the current balance.
     *
     * @param currentBalance the new current balance
     */
    public abstract void setCurrentBalance(BigDecimal currentBalance);

    /**
     * Sets the filtered transactions.
     *
     * @param filteredTransactions the new filtered transactions
     */
    public abstract void setFilteredTransactions(List<Transaction> filteredTransactions);

    /**
     * Sets the investment sub type.
     *
     * @param investmentSubType the new investment sub type
     */
    public abstract void setInvestmentSubType(EnumInvestmentSubType investmentSubType);

    /**
     * Set the account name.
     *
     * @param name the new name
     */
    public abstract void setName(String name);

    /**
     * Sets the related to account.
     *
     * @param relatedToAccount the new related to account
     */
    public abstract void setRelatedToAccount(Account relatedToAccount);

    /**
     * Sets the retirement.
     *
     * @param retirement the new retirement
     */
    public abstract void setRetirement(Boolean retirement);

    /**
     * Sets the security holdings.
     *
     * @param securityHoldings the new security holdings
     */
    public abstract void setSecurityHoldings(List<SecurityHolding> securityHoldings);

    /**
     * Sets the starting balance.
     *
     * @param startingBalance the new starting balance
     */
    public abstract void setStartingBalance(BigDecimal startingBalance);

    /**
     * Sets the transactions.
     *
     * @param transactions the new transactions
     */
//    @JsonIgnore
    public abstract void setTransactions(List<Transaction> transactions);

    /**
     * Sets the currency.
     *
     * @param currency the new currency
     */
    public abstract void setCurrency(Currency currency);

    /**
     * Currency for this account.
     *
     * @return the currency
     */
    @JsonSchemaDescription("Currency for this account")
    public abstract Currency getCurrency();

    public abstract Stream<Transaction> getTransactionStream();
}
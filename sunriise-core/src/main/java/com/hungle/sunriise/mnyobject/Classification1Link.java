package com.hungle.sunriise.mnyobject;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.hungle.sunriise.mnyobject.impl.DefaultClassification1Link;

/**
 * The Interface Classification1.
 */
@JsonDeserialize(as = DefaultClassification1Link.class)
public interface Classification1Link extends MnyObjectInterface {

    /**
     * Sets the name.
     *
     * @param name the new name
     */
    void setName(String name);

    /**
     * Gets the name.
     *
     * @return the name
     */
    String getName();
}
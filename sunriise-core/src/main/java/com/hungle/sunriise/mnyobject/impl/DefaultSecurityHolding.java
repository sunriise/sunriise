/*******************************************************************************
 * Copyright (c) 2010 Hung Le
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *******************************************************************************/
package com.hungle.sunriise.mnyobject.impl;

import java.math.BigDecimal;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.hungle.sunriise.mnyobject.Security;
import com.hungle.sunriise.mnyobject.SecurityHolding;

// TODO: Auto-generated Javadoc
/**
 * The Class DefaultSecurityHolding.
 */
@JsonSerialize(as = SecurityHolding.class)
public class DefaultSecurityHolding implements SecurityHolding {

    /** The security. */
    private Security security;

    /** The quantity. */
    // @JsonSerialize(using = InvestmentQuantitySerializer.class)
    private Double quantity;

    /** The price. */
    // @JsonSerialize(using = BalanceSerializer.class)
    private BigDecimal price;

    /** The market value. */
    // @JsonSerialize(using = BalanceSerializer.class)
    private BigDecimal marketValue;

    /*
     * (non-Javadoc)
     * 
     * @see com.hungle.sunriise.mnyobject.SecurityHolding#getQuantity()
     */
    @Override
    public Double getQuantity() {
        return quantity;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.hungle.sunriise.mnyobject.SecurityHolding#setQuantity(java.lang.
     * Double)
     */
    @Override
    public void setQuantity(Double quanity) {
        this.quantity = quanity;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.hungle.sunriise.mnyobject.SecurityHolding#getPrice()
     */
    @Override
    public BigDecimal getPrice() {
        return price;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.hungle.sunriise.mnyobject.SecurityHolding#setPrice(java.math.
     * BigDecimal)
     */
    @Override
    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.hungle.sunriise.mnyobject.SecurityHolding#getMarketValue()
     */
    @Override
    public BigDecimal getMarketValue() {
        return marketValue;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.hungle.sunriise.mnyobject.SecurityHolding#setMarketValue(java.math.
     * BigDecimal)
     */
    @Override
    public void setMarketValue(BigDecimal marketValue) {
        this.marketValue = marketValue;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.hungle.sunriise.mnyobject.SecurityHolding#getSecurity()
     */
    @Override
    public Security getSecurity() {
        return security;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.hungle.sunriise.mnyobject.SecurityHolding#setSecurity(com.hungle.
     * sunriise.mnyobject.Security)
     */
    @Override
    public void setSecurity(Security security) {
        this.security = security;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "DefaultSecurityHolding [security=" + security + ", quantity=" + quantity + ", price=" + price
                + ", marketValue=" + marketValue + "]";
    }
}

package com.hungle.sunriise.mnyobject;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * The Class AccountEntry.
 */
public class AccountEntry {

    /** The id. */
    @JsonProperty("_id")
    private Integer id;

    /** The name. */
    private String name;

    /** The safe name. */
    private String safeName;

    /**
     * Gets the name.
     *
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the name.
     *
     * @param name the new name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Gets the safe name.
     *
     * @return the safe name
     */
    public String getSafeName() {
        return safeName;
    }

    /**
     * Sets the safe name.
     *
     * @param safeName the new safe name
     */
    public void setSafeName(String safeName) {
        this.safeName = safeName;
    }

    /**
     * Gets the id.
     *
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * Sets the id.
     *
     * @param id the new id
     */
    public void setId(Integer id) {
        this.id = id;
    }
}
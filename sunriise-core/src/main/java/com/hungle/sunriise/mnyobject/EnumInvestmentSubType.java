/*******************************************************************************
 * Copyright (c) 2016 Hung Le
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *******************************************************************************/
package com.hungle.sunriise.mnyobject;

import org.apache.logging.log4j.Logger;

// TODO: Auto-generated Javadoc
/**
 * The Enum InvestmentSubType.
 */
public enum EnumInvestmentSubType {
    // 0: 403(b)
    // 1: 401k
    // 2: IRA
    // 3: Keogh
    /** The _403b. */
    _403b(0),
    /** The _401k. */
    _401k(1),
    /** The ira. */
    IRA(2),
    /** The keogh. */
    KEOGH(3),

    /** The retirement. */
    RETIREMENT(11),
    /** Not applicable. */
    NOT_APPLICABLE(-1),

    /** The unknown. */
    UNKNOWN(-2),;

    /** The Constant LOGGER. */
    private static final Logger LOGGER = org.apache.logging.log4j.LogManager.getLogger(EnumInvestmentSubType.class);

    /** The mny type. */
    private final int mnyType;

    /**
     * Gets the mny type.
     *
     * @return the mny type
     */
    public int getMnyType() {
        return mnyType;
    }

    /**
     * Instantiates a new investment sub type.
     *
     * @param mnyType the mny type
     */
    private EnumInvestmentSubType(int mnyType) {
        this.mnyType = mnyType;
    }

    /**
     * To investment sub type.
     *
     * @param mnyType the mny type
     * @return the investment sub type
     */
    public static EnumInvestmentSubType toInvestmentSubType(int mnyType) {
        switch (mnyType) {
        case -1:
            return NOT_APPLICABLE;
        case 0:
            return _403b;
        case 1:
            return _401k;
        case 2:
            return IRA;
        case 3:
            return KEOGH;
        case 11:
            return RETIREMENT;
        default:
            if (LOGGER.isDebugEnabled()) {
                LOGGER.warn("Invalid mnyType=" + mnyType);
            }
            return UNKNOWN;
        }
    }
}

package com.hungle.sunriise.mnyobject;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.hungle.sunriise.mnyobject.impl.DefaultAccountLink;

/**
 * The Interface Account.
 */
@JsonPropertyOrder({ "id", "name", })
@JsonDeserialize(as = DefaultAccountLink.class)
public interface AccountLink extends MnyObjectInterface {

    /**
     * Sets the name.
     *
     * @param name the new name
     */
    void setName(String name);

    /**
     * Gets the name.
     *
     * @return the name
     */
    String getName();
}
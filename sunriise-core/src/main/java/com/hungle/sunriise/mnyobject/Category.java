/*******************************************************************************
 * Copyright (c) 2013 Hung Le
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *******************************************************************************/

package com.hungle.sunriise.mnyobject;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.annotation.JsonView;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.hungle.sunriise.mnyobject.impl.DefaultCategory;

// TODO: Auto-generated Javadoc
/**
 * The Interface Category.
 */
@JsonPropertyOrder({ "id", "name", "fullName", "expense", "parentId", "level", "classificationId", })
@JsonDeserialize(as = DefaultCategory.class)
public interface Category extends MnyObjectInterface {

    /**
     * Gets the classification id.
     *
     * @return the classification id
     */
    public abstract Integer getClassificationId();

    /**
     * Gets the expense.
     *
     * @return the expense
     */
    public abstract boolean getExpense();

    /**
     * Gets the full name.
     *
     * @return the full name
     */
    public abstract String getFullName();

    /**
     * Gets the level.
     *
     * @return the level
     */
    public abstract Integer getLevel();

    /**
     * Gets the name.
     *
     * @return the name
     */
    @JsonView(View.Summary.class)
    public abstract String getName();

    /**
     * Gets the parent id.
     *
     * @return the parent id
     */
    public abstract Integer getParentId();

    /**
     * Sets the classification id.
     *
     * @param classificationId the new classification id
     */
    public abstract void setClassificationId(Integer classificationId);

    /**
     * Sets the expense.
     *
     * @param expense the new expense
     */
    public abstract void setExpense(boolean expense);

    /**
     * Sets the full name.
     *
     * @param fullName the new full name
     */
    public abstract void setFullName(String fullName);

    /**
     * Sets the level.
     *
     * @param level the new level
     */
    public abstract void setLevel(Integer level);

    /**
     * Sets the name.
     *
     * @param name the new name
     */
    public abstract void setName(String name);

    /**
     * Sets the parent id.
     *
     * @param parent the new parent id
     */
    public abstract void setParentId(Integer parent);
}
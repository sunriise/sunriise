package com.hungle.sunriise.mnyobject.impl;

import javax.persistence.Entity;
import javax.persistence.Table;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.hungle.sunriise.mnyobject.AccountLink;
import com.hungle.sunriise.mnyobject.MnyObject;

/**
 * The Class DefaultAccount.
 */
@JsonSerialize(as = AccountLink.class)
@Entity
@Table(name = "Account")
public final class DefaultAccountLink extends MnyObject implements AccountLink {

    /** The name. */
    private String name;

    public DefaultAccountLink() {
        super();
    }

    /**
     * Instantiates a new default account.
     *
     * @param id the id
     */
    public DefaultAccountLink(Integer id) {
        setId(id);
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.hungle.sunriise.mnyobject.Transaction.Account#setName(java.lang.String)
     */
    @Override
    public void setName(String name) {
        this.name = name;

    }

    /*
     * (non-Javadoc)
     * 
     * @see com.hungle.sunriise.mnyobject.Transaction.Account#getName()
     */
    @Override
    public String getName() {
        return name;
    }

}
package com.hungle.sunriise.mnyobject.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class InvestmentActivityTable {
    private static final InvestmentActivityTable instance = new InvestmentActivityTable();

    // Buy: act=1, grftt=18, grftf=0
    // Sell: act=2, grftt=18, grftf=0
    // Dividend: act=3, grftt=18, grftf=0
    // Interest: act=4, grftt=18, grftf=0
    // Return of Capital: act=8
    // Reinvest Dividend: act=9, grftt=16, grftf=0
    // Reinvest Interest: act=10, grftt=16, grftf=0
    // Remove Shares: act=13
    // Add Shares: act=12, grftt=16, grftf=0
    // S-Term Cap Gains Dist: act=24, grftt=18, grftf=0
    // L-Term Cap Gains Dist: act=26, grftt=18, grftf=0
    // Reinvest S-Term CG Dist: act=27, grftt=16, grftf=0
    // Reinvest L-Term CG Dist: act=29, grftt=16, grftf=0
    // Transfer Shares (in): act=32
    // Transfer Shares (out): act=33

    static final String STOCK_OPTION_EXPIRE_ESOS_STR = "Expire (ESOs)";
    static final String STOCK_OPTION_VEST_STR = "Vest";
    static final String STOCK_OPTION_GRANT_STR = "Grant";
    static final String ACTIVITY_UNKNOWN_STR = "ACTIVITY_UNKNOWN";
    static final String BUY_STR = "Buy";
    static final String SELL_STR = "Sell";
    static final String DIVIDEND_STR = "Dividend";
    static final String INTEREST_STR = "Interest";
    static final String RETURN_OF_CAPITAL_STR = "Return of Capital";
    static final String REINVEST_DIVIDEND_STR = "Reinvest Dividend";
    static final String REINVEST_INTEREST_STR = "Reinvest Interest";
    static final String REMOVE_SHARES_STR = "Remove Shares";
    static final String ADD_SHARES_STR = "Add Shares";
    static final String S_TERM_CAP_GAINS_DIST_STR = "S-Term Cap Gains Dist";
    static final String L_TERM_CAP_GAINS_DIST_STR = "L-Term Cap Gains Dist";
    static final String REINVEST_S_TERM_CG_DIST_STR = "Reinvest S-Term CG Dist";
    static final String REINVEST_L_TERM_CG_DIST_STR = "Reinvest L-Term CG Dist";
    static final String TRANSFER_SHARES_IN_STR = "Transfer Shares (in)";
    static final String TRANSFER_SHARES_OUT_STR = "Transfer Shares (out)";
    /** The Constant TRANSFER_SHARES_OUT. */
    public static final int TRANSFER_SHARES_OUT = 33;
    /** The Constant TRANSFER_SHARES_IN. */
    public static final int TRANSFER_SHARES_IN = 32;
    /** The Constant REINVEST_L_TERM_CG_DIST. */
    public static final int REINVEST_L_TERM_CG_DIST = 29;
    /** The Constant REINVEST_S_TERM_CG_DIST. */
    public static final int REINVEST_S_TERM_CG_DIST = 27;
    /** The Constant L_TERM_CAP_GAINS_DIST. */
    public static final int L_TERM_CAP_GAINS_DIST = 26;
    /** The Constant S_TERM_CAP_GAINS_DIST. */
    public static final int S_TERM_CAP_GAINS_DIST = 24;
    /** The Constant STOCK_OPTION_EXPIRE_ESOS. */
    // 20: Expire (ESOs)
    public static final int STOCK_OPTION_EXPIRE_ESOS = 20;
    /** The Constant STOCK_OPTION_VEST. */
    public static final int STOCK_OPTION_VEST = 18;
    /** The Constant STOCK_OPTION_GRANT. */
    public static final int STOCK_OPTION_GRANT = 17;
    /** The Constant REMOVE_SHARES. */
    public static final int REMOVE_SHARES = 13;
    /** The Constant ADD_SHARES. */
    public static final int ADD_SHARES = 12;
    /** The Constant REINVEST_INTEREST. */
    public static final int REINVEST_INTEREST = 10;
    /** The Constant REINVEST_DIVIDEND. */
    public static final int REINVEST_DIVIDEND = 9;
    /** The Constant RETURN_OF_CAPITAL. */
    public static final int RETURN_OF_CAPITAL = 8;
    /** The Constant INTEREST. */
    public static final int INTEREST = 4;
    /** The Constant DIVIDEND. */
    public static final int DIVIDEND = 3;
    /** The Constant SELL. */
    public static final int SELL = 2;
    /** The Constant BUY. */
    public static final int BUY = 1;
    private Map<Integer, IAKeyValue> mapByType;
    private Map<String, IAKeyValue> mapByValue;

    private InvestmentActivityTable() {
        super();
        initialize();
    }

    private void initialize() {
        List<IAKeyValue> values = new ArrayList<IAKeyValue>();

        values.add(new IAKeyValue(BUY, BUY_STR));
        values.add(new IAKeyValue(SELL, SELL_STR, false));
        values.add(new IAKeyValue(DIVIDEND, DIVIDEND_STR));
        values.add(new IAKeyValue(INTEREST, INTEREST_STR));
        values.add(new IAKeyValue(RETURN_OF_CAPITAL, RETURN_OF_CAPITAL_STR));
        values.add(new IAKeyValue(REINVEST_DIVIDEND, REINVEST_DIVIDEND_STR));
        values.add(new IAKeyValue(REINVEST_INTEREST, REINVEST_INTEREST_STR));
        values.add(new IAKeyValue(REMOVE_SHARES, REMOVE_SHARES_STR, false));
        values.add(new IAKeyValue(ADD_SHARES, ADD_SHARES_STR));
        values.add(new IAKeyValue(S_TERM_CAP_GAINS_DIST, S_TERM_CAP_GAINS_DIST_STR));
        values.add(new IAKeyValue(L_TERM_CAP_GAINS_DIST, L_TERM_CAP_GAINS_DIST_STR));
        values.add(new IAKeyValue(REINVEST_S_TERM_CG_DIST, REINVEST_S_TERM_CG_DIST_STR));
        values.add(new IAKeyValue(REINVEST_L_TERM_CG_DIST, REINVEST_L_TERM_CG_DIST_STR));
        values.add(new IAKeyValue(TRANSFER_SHARES_IN, TRANSFER_SHARES_IN_STR));
        values.add(new IAKeyValue(TRANSFER_SHARES_OUT, TRANSFER_SHARES_OUT_STR, false));
        values.add(new IAKeyValue(STOCK_OPTION_GRANT, STOCK_OPTION_GRANT_STR, false));
        values.add(new IAKeyValue(STOCK_OPTION_VEST, STOCK_OPTION_VEST_STR));
        values.add(new IAKeyValue(STOCK_OPTION_EXPIRE_ESOS, STOCK_OPTION_EXPIRE_ESOS_STR, false));

        mapByType = new HashMap<>();
        mapByValue = new HashMap<>();

        for (IAKeyValue value : values) {
            mapByType.put(value.getKey(), value);
            mapByValue.put(value.getValue(), value);
        }
    }

    public static InvestmentActivityTable getInstance() {
        return instance;
    }

    public IAKeyValue getByType(Integer type) {
        return mapByType.get(type);
    }

    public IAKeyValue getByValue(String str) {
        return mapByValue.get(str);
    }

    /**
     * Initialize.
     */
//    private void initialize() {
//        this.added = true;
//
//        String str = InvestmentActivityTable.ACTIVITY_UNKNOWN_STR;
//        IAKeyValue value = InvestmentActivityTable.getInstance().getByType(type);
//        if (value != null) {
//            str = value.getValue();
//        }
//
//        this.string = str;
//    }

}

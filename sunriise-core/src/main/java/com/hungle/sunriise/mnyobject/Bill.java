/*******************************************************************************
 * Copyright (c) 2016 Hung Le
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *******************************************************************************/
package com.hungle.sunriise.mnyobject;

import java.util.Date;

// TODO: Auto-generated Javadoc
/**
 * The Interface Bill.
 */
public interface Bill extends MnyObjectInterface {
    /**
     * Gets the bill head id.
     *
     * @return the bill head id
     */
    public abstract Integer getBillHeadId();

    /**
     * Gets the date.
     *
     * @return the date
     */
//    @JsonSerialize(using = MnyDateSerializer.class)
    public abstract Date getDate();

    /**
     * Gets the days auto enter.
     *
     * @return the days auto enter
     */
    public abstract Integer getDaysAutoEnter();

    /**
     * Gets the frequency.
     *
     * @return the frequency
     */
    public abstract Frequency getFrequency();

    /**
     * Gets the transaction id.
     *
     * @return the transaction id
     */
    public abstract Integer getTransactionId();

    /**
     * Sets the bill head id.
     *
     * @param billHeadId the new bill head id
     */
    public abstract void setBillHeadId(Integer billHeadId);

    /**
     * Sets the date.
     *
     * @param date the new date
     */
    public abstract void setDate(Date date);

    /**
     * Sets the days auto enter.
     *
     * @param daysAutoEnter the new days auto enter
     */
    public abstract void setDaysAutoEnter(Integer daysAutoEnter);

    /**
     * Sets the frequency.
     *
     * @param frequency the new frequency
     */
    public abstract void setFrequency(Frequency frequency);

    /**
     * Sets the transaction id.
     *
     * @param transactionId the new transaction id
     */
    public abstract void setTransactionId(Integer transactionId);

}

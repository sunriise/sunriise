/*******************************************************************************
 * Copyright (c) 2016 Hung Le
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *******************************************************************************/
package com.hungle.sunriise.mnyobject.impl;

import java.util.Date;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.hungle.sunriise.mnyobject.Bill;
import com.hungle.sunriise.mnyobject.Frequency;
import com.hungle.sunriise.mnyobject.MnyObject;

// TODO: Auto-generated Javadoc
/**
 * The Class DefaultBill.
 */
@JsonSerialize(as = Bill.class)
//@Entity
public class DefaultBill extends MnyObject implements Bill {

    /** The bill head id. */
    private Integer billHeadId;

    /** The transaction id. */
    private Integer transactionId;

    /** The days auto enter. */
    private Integer daysAutoEnter;

    /** The date. */
    // @JsonSerialize(using = DateSerializer.class)
    private Date date;

    /** The frequency. */
    private Frequency frequency;

    /*
     * (non-Javadoc)
     * 
     * @see com.hungle.sunriise.mnyobject.Bill#getDate()
     */
    @Override
    public Date getDate() {
        return date;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.hungle.sunriise.mnyobject.Bill#setDate(java.util.Date)
     */
    @Override
    public void setDate(Date date) {
        this.date = date;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.hungle.sunriise.mnyobject.Bill#getDaysAutoEnter()
     */
    @Override
    public Integer getDaysAutoEnter() {
        return daysAutoEnter;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.hungle.sunriise.mnyobject.Bill#setDaysAutoEnter(java.lang.Integer)
     */
    @Override
    public void setDaysAutoEnter(Integer daysAutoEnter) {
        this.daysAutoEnter = daysAutoEnter;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.hungle.sunriise.mnyobject.Bill#getTransactionId()
     */
    @Override
    public Integer getTransactionId() {
        return transactionId;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.hungle.sunriise.mnyobject.Bill#setTransactionId(java.lang.Integer)
     */
    @Override
    public void setTransactionId(Integer transactionId) {
        this.transactionId = transactionId;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.hungle.sunriise.mnyobject.Bill#getBillHeadId()
     */
    @Override
    public Integer getBillHeadId() {
        return billHeadId;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.hungle.sunriise.mnyobject.Bill#setBillHeadId(java.lang.Integer)
     */
    @Override
    public void setBillHeadId(Integer billHeadId) {
        this.billHeadId = billHeadId;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.hungle.sunriise.mnyobject.Bill#getFrequency()
     */
    @Override
    public Frequency getFrequency() {
        return frequency;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.hungle.sunriise.mnyobject.Bill#setFrequency(com.hungle.sunriise.
     * mnyobject.Frequency)
     */
    @Override
    public void setFrequency(Frequency frequency) {
        this.frequency = frequency;
    }

}

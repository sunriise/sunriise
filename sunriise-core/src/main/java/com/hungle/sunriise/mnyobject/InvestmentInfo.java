package com.hungle.sunriise.mnyobject;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.hungle.sunriise.mnyobject.impl.DefaultInvestmentInfo;
import com.hungle.sunriise.mnyobject.impl.InvestmentActivity;

// TODO: Auto-generated Javadoc
/**
 * The Interface InvestmentInfo.
 */
@JsonDeserialize(as = DefaultInvestmentInfo.class)
public interface InvestmentInfo {

    /**
     * Gets the investment activity.
     *
     * @return the investment activity
     */
    public abstract InvestmentActivity getActivity();

    /**
     * Gets the investment transaction.
     *
     * @return the investment transaction
     */
    public abstract InvestmentTransaction getTransaction();

    /**
     * Sets the investment activity.
     *
     * @param investmentActivity the new investment activity
     */
    public abstract void setActivity(InvestmentActivity investmentActivity);

    /**
     * Sets the investment transaction.
     *
     * @param investmentTransaction the new investment transaction
     */
    public abstract void setTransaction(InvestmentTransaction investmentTransaction);

    /**
     * Sets the security id.
     *
     * @param security the new security id
     */
    public abstract void setSecurity(Security security);

    /**
     * Gets the security id.
     *
     * @return the security id
     */
    public abstract Security getSecurity();

}

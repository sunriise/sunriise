package com.hungle.sunriise.dbutil;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.Logger;

import com.hungle.sunriise.mnyobject.Account;
import com.hungle.sunriise.mnyobject.Transaction;
import com.hungle.sunriise.util.BalanceUtils;

public class CalculateMonthlySummary {
    private static final Logger LOGGER = org.apache.logging.log4j.LogManager.getLogger(CalculateMonthlySummary.class);

    public class SummaryEntry {
        private String key;

        private int count;

        private BigDecimal total;

        private BigDecimal runningBalance;

        public SummaryEntry(String key, int count, BigDecimal total, BigDecimal runningBalance) {
            super();
            this.key = key;
            this.count = count;
            this.total = total;
            this.runningBalance = runningBalance;
        }

        public String getKey() {
            return key;
        }

        @Override
        public String toString() {
            return "SummaryEntry [key=" + key + ", count=" + count + ", total=" + total + ", runningBalance="
                    + runningBalance + "]";
        }
    }

    private Account account;

    private Date startDate;
    private Date endDate;

    private List<SummaryEntry> summaries = new ArrayList<>();

    public CalculateMonthlySummary(Account account, Date startDate, Date endDate, List<SummaryEntry> summaries) {
        super();
        this.account = account;
        this.startDate = startDate;
        this.endDate = endDate;
        this.summaries = summaries;
    }

    public CalculateMonthlySummary(Account account) {
        super();
        this.account = account;
        calculate();
    }

    /**
     * Calculate monthly summary.
     *
     * @param account the account
     */
    private void calculate() {
        List<Transaction> transactions = account.getTransactions();

        Map<String, List<Transaction>> buckets = collect(transactions);

        for (String key : buckets.keySet()) {
            List<Transaction> list = buckets.get(key);
            BigDecimal total = BigDecimal.ZERO;
            BigDecimal runningBalance = null;
            for (Transaction transaction : list) {
                if (BalanceUtils.isNonBalanceTransaction(transaction)) {
                    continue;
                }
                BigDecimal amount = transaction.getAmount();
                if (amount != null) {
                    total = total.add(amount);
                } else {
                    LOGGER.warn("Transaction with no amount, id=" + transaction.getId());
                }
                BigDecimal rb = transaction.getRunningBalance();
                if (rb != null) {
                    runningBalance = rb;
                }
            }
            SummaryEntry summaryEntry = new SummaryEntry(key, list.size(), total, runningBalance);
            summaries.add(summaryEntry);
        }

        Comparator<SummaryEntry> c = new Comparator<CalculateMonthlySummary.SummaryEntry>() {

            @Override
            public int compare(SummaryEntry o1, SummaryEntry o2) {
                return o1.getKey().compareTo(o2.getKey());
            }
        };
        Collections.sort(summaries, c);
    }

    private Map<String, List<Transaction>> collect(List<Transaction> transactions) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM");
        Map<String, List<Transaction>> buckets = new HashMap<>();
        for (Transaction transaction : transactions) {
            Date date = transaction.getDate();

            if (startDate == null) {
                startDate = date;
            } else {
                if (date.compareTo(startDate) < 0) {
                    startDate = date;
                }
            }
            if (endDate == null) {
                endDate = date;
            } else {
                if (date.compareTo(endDate) > 0) {
                    endDate = date;
                }
            }

            String key = sdf.format(date);
            List<Transaction> list = buckets.get(key);
            if (list == null) {
                list = new ArrayList<Transaction>();
                buckets.put(key, list);
            }
            list.add(transaction);
        }
        return buckets;
    }

    public Account getAccount() {
        return account;
    }

    public Date getStartDate() {
        return startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public List<SummaryEntry> getSummaries() {
        return summaries;
    }

}

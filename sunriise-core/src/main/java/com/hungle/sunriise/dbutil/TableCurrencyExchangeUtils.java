/*******************************************************************************
 * Copyright (c) 2016 Hung Le
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *******************************************************************************/
package com.hungle.sunriise.dbutil;

import java.io.IOException;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;

import org.apache.logging.log4j.Logger;

import com.healthmarketscience.jackcess.Column;
import com.healthmarketscience.jackcess.Cursor;
import com.healthmarketscience.jackcess.CursorBuilder;
import com.healthmarketscience.jackcess.Database;
import com.healthmarketscience.jackcess.Row;
import com.healthmarketscience.jackcess.Table;
import com.hungle.sunriise.mnyobject.CurrencyExchange;
import com.hungle.sunriise.mnyobject.impl.DefaultCurrencyExchange;

// TODO: Auto-generated Javadoc
/**
 * The Class TableCurrencyExchangeUtil.
 */
public class TableCurrencyExchangeUtils {
    private static final String COLUMN_TO = "hcrncTo";

    private static final String COLUMN_FROM = "hcrncFrom";

    private static final Logger LOGGER = org.apache.logging.log4j.LogManager
            .getLogger(TableCurrencyExchangeUtils.class);

    /** The Constant TABLE_NAME_CURRENCY. */
    private static final String TABLE_NAME_CURRENCY_EXCHANGE = "CRNC_EXCHG";

    public static final String COLUMN_RATE = "rate";

    public static final String COLUMN_DATE = "dt";

    public static Map<Integer, CurrencyExchange> getMap(Database db) throws IOException {
        return getCurrencies(db);
    }

    /**
     * Gets the currencies.
     *
     * @param db the db
     * @return the currencies
     * @throws IOException Signals that an I/O exception has occurred.
     */
    public static Map<Integer, CurrencyExchange> getCurrencies(Database db) throws IOException {
        Map<Integer, CurrencyExchange> currencyExchanges = new LinkedHashMap<Integer, CurrencyExchange>();

        Cursor cursor = TableCurrencyExchangeUtils.createCursor(db);
        try {
            for (Row row : cursor) {
                addCurrencyExchange(row, currencyExchanges);
            }
        } finally {
            cursor = null;
        }

        return currencyExchanges;
    }

    /**
     * Gets the currency table.
     *
     * @param db the db
     * @return the currency table
     * @throws IOException Signals that an I/O exception has occurred.
     */
    public static Table getTable(Database db) throws IOException {
        String tableName = TABLE_NAME_CURRENCY_EXCHANGE;
        return db.getTable(tableName);
    }

    /**
     * Creates the currency cursor.
     *
     * @param db the db
     * @return the cursor
     * @throws IOException Signals that an I/O exception has occurred.
     */
    public static Cursor createCursor(Database db) throws IOException {
        Table table = TableCurrencyExchangeUtils.getTable(db);
        return CursorBuilder.createCursor(table);
    }

    public static CurrencyExchange deserialize(Row row) {
        CurrencyExchange currencyExchange = new DefaultCurrencyExchange();

        Integer hcrncFrom = row.getInt(COLUMN_FROM);
        currencyExchange.setHcrncFrom(hcrncFrom);

        Integer hcrncTo = row.getInt(COLUMN_TO);
        currencyExchange.setHcrncTo(hcrncTo);

        Double rate = row.getDouble(COLUMN_RATE);
        currencyExchange.setRate(rate);

        Date dt = row.getDate(COLUMN_DATE);
        currencyExchange.setDt(dt);

        return currencyExchange;
    }

    /**
     * Adds the currency.
     *
     * @param row               the row
     * @param currencyExchanges the currencies
     */
    private static void addCurrencyExchange(Row row, Map<Integer, CurrencyExchange> currencyExchanges) {
        CurrencyExchange currencyExchange = deserialize(row);

        currencyExchanges.put(currencyExchange.getId(), currencyExchange);
    }

    public static Column getColumn(Database db, String name) throws IOException {
        Table table = TableCurrencyExchangeUtils.getTable(db);
        return table.getColumn(name);
    }

}

package com.hungle.sunriise.dbutil;

import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import org.apache.logging.log4j.Logger;

import com.healthmarketscience.jackcess.Cursor;
import com.healthmarketscience.jackcess.CursorBuilder;
import com.healthmarketscience.jackcess.Database;
import com.healthmarketscience.jackcess.Index;
import com.healthmarketscience.jackcess.IndexCursor;
import com.healthmarketscience.jackcess.Row;
import com.healthmarketscience.jackcess.Table;
import com.hungle.sunriise.mnyobject.Transaction;
import com.hungle.sunriise.mnyobject.TransactionInfo;
import com.hungle.sunriise.mnyobject.TransactionXfer;
import com.hungle.sunriise.mnyobject.impl.DefaultTransactionXfer;

// TODO: Auto-generated Javadoc
/**
 * The Class TableTransationXferUtil.
 */
public class TableTransationXferUtils {

    /** The Constant LOGGER. */
    private static final Logger LOGGER = org.apache.logging.log4j.LogManager.getLogger(TableTransationXferUtils.class);

    /** The Constant TABLE_NAME_TRANSACTION_XFER. */
    private static final String TABLE_NAME_TRANSACTION_XFER = "TRN_XFER";

    // XXX - msmoney has it backward: from is htrnLink, to is htrnFrom
    /** The Constant COL_FROM_TRANSACTION_ID. */
    // transfer from this transactionId
    private static final String COL_FROM_TRANSACTION_ID = "htrnLink";

    /** The Constant INDEX_FROM_TRANSACTION_ID. */
    private static final String INDEX_FROM_TRANSACTION_ID = "htrnTrnXferTo";

    /** The Constant COL_TO_TRANSACTION_ID. */
    // transfer to this transactionId
    private static final String COL_TO_TRANSACTION_ID = "htrnFrom";

    /** The Constant INDEX_TO_TRANSACTION_ID. */
    private static final String INDEX_TO_TRANSACTION_ID = "htrnTrnXferFrom";

    /**
     * Gets the transaction xfers.
     *
     * @param db the db
     * @return the transaction xfers
     * @throws IOException Signals that an I/O exception has occurred.
     */
    public static Map<Integer, TransactionXfer> getTransactionXfers(Database db) throws IOException {
        Map<Integer, TransactionXfer> transactions = new LinkedHashMap<Integer, TransactionXfer>();

        Cursor cursor = TableTransationXferUtils.createCursor(db);
        try {
            for (Row row : cursor) {
                addTransationXfer(row, transactions);
            }
        } finally {
            cursor = null;
        }

        return transactions;
    }

    /**
     * Adds the transation xfer.
     *
     * @param row          the row
     * @param transactions the transactions
     */
    private static void addTransationXfer(Row row, Map<Integer, TransactionXfer> transactions) {
        TransactionXfer xfer = createTransactionXfer(row);

        Integer id = xfer.getFromTransactionId();
        transactions.put(id, xfer);
    }

    /**
     * New transaction xfer.
     *
     * @param row the row
     * @return the transaction xfer
     */
    private static TransactionXfer createTransactionXfer(Row row) {
        TransactionXfer xfer = new DefaultTransactionXfer();

        Integer fromTransactionId = row.getInt(COL_FROM_TRANSACTION_ID);
        xfer.setFromTransactionId(fromTransactionId);

        Integer toTransactionId = row.getInt(COL_TO_TRANSACTION_ID);
        xfer.setToTransactionId(toTransactionId);

        return xfer;
    }

    /**
     * Find by from id.
     *
     * @param db     the db
     * @param fromId the from id
     * @return the transaction xfer
     * @throws IOException Signals that an I/O exception has occurred.
     */
    private static final TransactionXfer findByFromId(Database db, Integer fromId) throws IOException {
        String indexName = INDEX_FROM_TRANSACTION_ID;
        String colunmName = COL_FROM_TRANSACTION_ID;

        TransactionXfer xfer = findById(db, indexName, colunmName, fromId);

        return xfer;
    }

    /**
     * Find by to id.
     *
     * @param db   the db
     * @param toId the to id
     * @return the transaction xfer
     * @throws IOException Signals that an I/O exception has occurred.
     */
    private static final TransactionXfer findByToId(Database db, Integer toId) throws IOException {
        String indexName = INDEX_TO_TRANSACTION_ID;
        String colunmName = COL_TO_TRANSACTION_ID;

        TransactionXfer xfer = findById(db, indexName, colunmName, toId);

        return xfer;
    }

    /**
     * Find by id.
     *
     * @param db         the db
     * @param indexName  the index name
     * @param colunmName the colunm name
     * @param id         the id
     * @return the transaction xfer
     * @throws IOException Signals that an I/O exception has occurred.
     */
    private static TransactionXfer findById(Database db, String indexName, String colunmName, Integer id)
            throws IOException {
        Index index = TableTransationXferUtils.getTable(db).getIndex(indexName);

        return findById(index, colunmName, id);
    }

    /**
     * Find by id.
     *
     * @param index      the index
     * @param colunmName the colunm name
     * @param id         the id
     * @return the transaction xfer
     * @throws IOException Signals that an I/O exception has occurred.
     */
    private static TransactionXfer findById(Index index, String colunmName, Integer id) throws IOException {
        IndexCursor cursor = CursorBuilder.createCursor(index);
        Map<String, Object> rowPattern = new HashMap<String, Object>();
        rowPattern.put(colunmName, id);
        TransactionXfer xfer = null;
        if (cursor.findFirstRow(rowPattern)) {
            Row row = cursor.getCurrentRow();
            xfer = createTransactionXfer(row);
        }
        return xfer;
    }

    /**
     * Creates the cursor.
     *
     * @param db the db
     * @return the cursor
     * @throws IOException Signals that an I/O exception has occurred.
     */
    private static final Cursor createCursor(Database db) throws IOException {
        Table table = TableTransationXferUtils.getTable(db);
        return CursorBuilder.createCursor(table);
    }

    /**
     * Gets the table.
     *
     * @param db the db
     * @return the table
     * @throws IOException Signals that an I/O exception has occurred.
     */
    private static Table getTable(Database db) throws IOException {
        String tableName = TABLE_NAME_TRANSACTION_XFER;
        Table table = db.getTable(tableName);
        return table;
    }

    /**
     * Gets the xfer id.
     *
     * @param db          the db
     * @param transaction the transaction
     * @return the xfer id
     * @throws IOException Signals that an I/O exception has occurred.
     */
    public static final Integer getXferId(Database db, Transaction transaction) throws IOException {
        Integer id = transaction.getId();
        boolean transferTo = transaction.getTransactionInfo().isTransferTo();
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("id=" + id + ", isTransferTo=" + transferTo);
        }

        TransactionXfer transactionXfer = null;
        Integer xferId = null;
        if (transferTo) {
            transactionXfer = findByToId(db, id);
            if (transactionXfer != null) {
                xferId = transactionXfer.getFromTransactionId();
            }
        } else {
            // from this transaction
            transactionXfer = findByFromId(db, id);
            if (transactionXfer != null) {
                xferId = transactionXfer.getToTransactionId();
            }
        }
        return xferId;
    }

    /**
     * Gets the flag as binary string.
     *
     * @param transactionInfo the transaction info
     * @return the flag as binary string
     */
    public static String getFlagAsBinaryString(TransactionInfo transactionInfo) {
        if (transactionInfo == null) {
            return "null";
        } else {
            return TableTransactionUtils.getBinaryString(transactionInfo.getFlag());
        }
    }

    /**
     * Checks if is transfer to.
     *
     * @param db the db
     * @param id the id
     * @return true, if is transfer to
     * @throws IOException Signals that an I/O exception has occurred.
     */
    public static boolean isTransferTo(Database db, Integer id) throws IOException {
        boolean rv = false;
        TransactionXfer toTransactionXfer = findByToId(db, id);
        TransactionXfer fromTransactionXfer = findByFromId(db, id);

        if ((toTransactionXfer != null) && (fromTransactionXfer != null)) {
            LOGGER.warn("transaction.id=" + id + " has BOTH xferTo and xferFrom.");
            rv = true;
        } else if (toTransactionXfer != null) {
            rv = true;
        } else if (fromTransactionXfer != null) {
            rv = false;
        } else {
            rv = false;
        }

        return rv;
    }
}

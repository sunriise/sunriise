/*******************************************************************************
 * Copyright (c) 2016 Hung Le
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *******************************************************************************/
package com.hungle.sunriise.dbutil;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.Logger;

import com.healthmarketscience.jackcess.Cursor;
import com.healthmarketscience.jackcess.CursorBuilder;
import com.healthmarketscience.jackcess.Database;
import com.healthmarketscience.jackcess.Row;
import com.healthmarketscience.jackcess.Table;
import com.hungle.sunriise.mnyobject.MnyContext;
import com.hungle.sunriise.mnyobject.Security;
import com.hungle.sunriise.mnyobject.SecurityHolding;
import com.hungle.sunriise.mnyobject.impl.DefaultSecurity;
import com.hungle.sunriise.util.ComparatorUtils;
import com.hungle.sunriise.util.MnyFilter;

// TODO: Auto-generated Javadoc
/**
 * The Class TableSecurityUtil.
 */
public class TableSecurityUtils {

    /** The Constant LOGGER. */
    private static final Logger LOGGER = org.apache.logging.log4j.LogManager.getLogger(TableSecurityUtils.class);

    /**
     * The Class CompareSecurityHoldingsByName.
     */
    public static final class CompareSecurityHoldingsByName implements Comparator<SecurityHolding> {

        /*
         * (non-Javadoc)
         * 
         * @see java.util.Comparator#compare(java.lang.Object, java.lang.Object)
         */
        @Override
        public int compare(SecurityHolding o1, SecurityHolding o2) {
            int rv = ComparatorUtils.compareNullOrder(o1.getSecurity().getName(), o2.getSecurity().getName());
            return rv;
        }
    }

    public static final class CompareSecurityByName implements Comparator<Security> {
        /*
         * (non-Javadoc)
         * 
         * @see java.util.Comparator#compare(java.lang.Object, java.lang.Object)
         */
        @Override
        public int compare(Security o1, Security o2) {
            int rv = ComparatorUtils.compareNullOrder(o1.getName(), o2.getName());
            return rv;
        }
    }

    /** The Constant TABLE_NAME_SECURITY. */
    private static final String TABLE_NAME_SECURITY = "SEC";

    /** The Constant COL_ID. */
    private static final String COL_ID = "hsec";

    /** The Constant COL_NAME. */
    private static final String COL_NAME = "szFull";

    /** The Constant COL_SYMBOL. */
    private static final String COL_SYMBOL = "szSymbol";

    public static Map<Integer, Security> getMap(Database db) throws IOException {
        return getSecurities(db);
    }

    /**
     * Gets the securities.
     *
     * @param db the db
     * @return the securities
     * @throws IOException Signals that an I/O exception has occurred.
     */
    public static Map<Integer, Security> getSecurities(Database db) throws IOException {
        Map<Integer, Security> securities = new LinkedHashMap<Integer, Security>();

        Cursor cursor = TableSecurityUtils.createCursor(db);
        try {
            for (Row row : cursor) {
                addSecurity(row, securities);
            }
        } finally {
            cursor = null;
        }

        return securities;
    }

    /**
     * Gets the security table.
     *
     * @param db the db
     * @return the security table
     * @throws IOException Signals that an I/O exception has occurred.
     */
    private static Table getTable(Database db) throws IOException {
        String tableName = TABLE_NAME_SECURITY;
        Table table = db.getTable(tableName);
        return table;
    }

    /**
     * Creates the security cursor.
     *
     * @param db the db
     * @return the cursor
     * @throws IOException Signals that an I/O exception has occurred.
     */
    private static Cursor createCursor(Database db) throws IOException {
        Table table = TableSecurityUtils.getTable(db);
        return CursorBuilder.createCursor(table);
    }

    /**
     * Creates the security.
     *
     * @param row the row
     * @return the security
     */
    private static Security deserialize(Row row) {
        Security security = new DefaultSecurity();

        Integer id = row.getInt(COL_ID);
        security.setId(id);

        String name = row.getString(COL_NAME);
        security.setName(name);

        String symbol = row.getString(COL_SYMBOL);
        security.setSymbol(symbol);

        return security;
    }

    /**
     * Adds the security.
     *
     * @param row        the row
     * @param securities the securities
     */
    private static void addSecurity(Row row, Map<Integer, Security> securities) {
        Security security = deserialize(row);

        if (security != null) {
            if (LOGGER.isDebugEnabled()) {
                LOGGER.debug("Adding security, id=" + security.getId() + ", security=" + security);
            }
            securities.put(security.getId(), security);
        }
    }

    /**
     * Gets the security name.
     *
     * @param securityId the security id
     * @param mnyContext the mny context
     * @return the security name
     */
    public static String getSecurityName(Integer securityId, MnyContext mnyContext) {
        String securityName = null;
        if (securityId != null) {
            Map<Integer, Security> securities = mnyContext.getSecurities();
            Security security = securities.get(securityId);
            if (security != null) {
                securityName = security.getName();
            } else {
                securityName = securityId.toString();
            }
        }
        return securityName;
    }

    public static final List<Security> createSortedListByName(Map<Integer, Security> securities) {
        List<Security> list = new ArrayList<Security>();
        list.addAll(securities.values());
        Comparator<Security> comparator = new CompareSecurityByName();
        Collections.sort(list, comparator);
        return list;
    }

    public static List<Security> toSecuritiesList(Map<Integer, Security> map, MnyFilter<Security> filter) {
        ArrayList<Security> list = new ArrayList<Security>();

        if (filter == null) {
            list.addAll(map.values());
        } else {
            for (Security type : map.values()) {
                if (filter.accepts(type)) {
                    list.add(type);
                } else {
                    if (LOGGER.isDebugEnabled()) {
                        LOGGER.debug("NOT_SHOW security=" + type.getName());
                    }
                }
            }
        }

        Collections.sort(list, new Comparator<Security>() {

            @Override
            public int compare(Security o1, Security o2) {
                String n1 = o1.getName();
                String n2 = o2.getName();
                return ComparatorUtils.compareNullOrder(n1, n2);

                // if ((n1 == null) && (n2 == null)) {
                // return 0;
                // } else if (n1 == null) {
                // return 1;
                // } else {
                // return n1.compareTo(n2);
                // }
            }
        });

        return list;
    }
}

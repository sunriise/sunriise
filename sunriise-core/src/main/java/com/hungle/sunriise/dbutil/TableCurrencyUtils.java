/*******************************************************************************
 * Copyright (c) 2016 Hung Le
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *******************************************************************************/
package com.hungle.sunriise.dbutil;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.Logger;

import com.healthmarketscience.jackcess.Cursor;
import com.healthmarketscience.jackcess.CursorBuilder;
import com.healthmarketscience.jackcess.Database;
import com.healthmarketscience.jackcess.Row;
import com.healthmarketscience.jackcess.Table;
import com.hungle.sunriise.mnyobject.Account;
import com.hungle.sunriise.mnyobject.Currency;
import com.hungle.sunriise.mnyobject.MnyContext;
import com.hungle.sunriise.mnyobject.impl.DefaultCurrency;
import com.hungle.sunriise.util.ComparatorUtils;
import com.hungle.sunriise.util.MnyFilter;

// TODO: Auto-generated Javadoc
/**
 * The Class TableCurrencyUtil.
 */
public class TableCurrencyUtils {
    private static final Logger LOGGER = org.apache.logging.log4j.LogManager.getLogger(TableCurrencyUtils.class);

    /** The Constant TABLE_NAME_CURRENCY. */
    private static final String TABLE_NAME_CURRENCY = "CRNC";

    /** The Constant COL_NAME. */
    private static final String COL_NAME = "szName";

    /** The Constant COL_ID. */
    private static final String COL_ID = "hcrnc";

    /** The Constant COL_ISO_CODE. */
    private static final String COL_ISO_CODE = "szIsoCode";

    public static Map<Integer, Currency> getMap(Database db) throws IOException {
        return getCurrencies(db);
    }

    /**
     * Gets the currencies.
     *
     * @param db the db
     * @return the currencies
     * @throws IOException Signals that an I/O exception has occurred.
     */
    public static Map<Integer, Currency> getCurrencies(Database db) throws IOException {
        Map<Integer, Currency> currencies = new LinkedHashMap<Integer, Currency>();

        Cursor cursor = TableCurrencyUtils.createCursor(db);
        try {
            for (Row row : cursor) {
                addCurrency(row, currencies);
            }
        } finally {
            cursor = null;
        }

        return currencies;
    }

    /**
     * Gets the currency table.
     *
     * @param db the db
     * @return the currency table
     * @throws IOException Signals that an I/O exception has occurred.
     */
    public static Table getTable(Database db) throws IOException {
        String tableName = TABLE_NAME_CURRENCY;
        return db.getTable(tableName);
    }

    /**
     * Creates the currency cursor.
     *
     * @param db the db
     * @return the cursor
     * @throws IOException Signals that an I/O exception has occurred.
     */
    public static Cursor createCursor(Database db) throws IOException {
        Table table = TableCurrencyUtils.getTable(db);
        return CursorBuilder.createCursor(table);
    }

    private static Currency deserialize(Row row) {
        Currency currency = new DefaultCurrency();

        String name = row.getString(COL_NAME);

        currency.setName(name);

        Integer id = row.getInt(COL_ID);
        currency.setId(id);

        String isoCode = row.getString(COL_ISO_CODE);
        currency.setIsoCode(isoCode);

        return currency;
    }

    /**
     * Adds the currency.
     *
     * @param row        the row
     * @param currencies the currencies
     */
    private static void addCurrency(Row row, Map<Integer, Currency> currencies) {
        Currency currency = deserialize(row);

        currencies.put(currency.getId(), currency);
    }

    /**
     * Gets the currency name.
     *
     * @param currencyId the currency id
     * @param currencies the currencies
     * @return the currency name
     */
    static String getCurrencyName(Integer currencyId, Map<Integer, Currency> currencies) {
        Currency currency = currencies.get(currencyId);
        if (currency != null) {
            return currency.getIsoCode();
        } else {
            return null;
        }
    }

    /**
     * Gets the currency name.
     *
     * @param account    the account
     * @param mnyContext the mny context
     * @return the currency name
     */
    public static String getCurrencyName(Account account, MnyContext mnyContext) {
        return getCurrencyName(account.getCurrency().getId(), mnyContext.getCurrencies());
    }

    public static List<Currency> toCurrenciesList(Map<Integer, Currency> map, MnyFilter<Currency> filter) {
        ArrayList<Currency> list = new ArrayList<Currency>();

        if (filter == null) {
            list.addAll(map.values());
        } else {
            for (Currency type : map.values()) {
                if (filter.accepts(type)) {
                    list.add(type);
                } else {
                    if (LOGGER.isDebugEnabled()) {
                        LOGGER.debug("NOT_SHOW currency=" + type.getName());
                    }
                }
            }
        }

        Collections.sort(list, new Comparator<Currency>() {

            @Override
            public int compare(Currency o1, Currency o2) {
                String n1 = o1.getName();
                String n2 = o2.getName();
                return ComparatorUtils.compareNullOrder(n1, n2);

                // if ((n1 == null) && (n2 == null)) {
                // return 0;
                // } else if (n1 == null) {
                // return 1;
                // } else {
                // return n1.compareTo(n2);
                // }
            }
        });

        return list;
    }

}

/*******************************************************************************
 * Copyright (c) 2016 Hung Le
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *******************************************************************************/
package com.hungle.sunriise.dbutil;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.Logger;

import com.healthmarketscience.jackcess.Cursor;
import com.healthmarketscience.jackcess.CursorBuilder;
import com.healthmarketscience.jackcess.Database;
import com.healthmarketscience.jackcess.IndexCursor;
import com.healthmarketscience.jackcess.Row;
import com.healthmarketscience.jackcess.Table;
import com.hungle.sunriise.json.JSONUtils;
import com.hungle.sunriise.mnyobject.Account;
import com.hungle.sunriise.mnyobject.Currency;
import com.hungle.sunriise.mnyobject.EnumAccountType;
import com.hungle.sunriise.mnyobject.EnumInvestmentSubType;
import com.hungle.sunriise.mnyobject.MnyContext;
import com.hungle.sunriise.mnyobject.Transaction;
import com.hungle.sunriise.mnyobject.TransactionFilter;
import com.hungle.sunriise.mnyobject.TransactionSplit;
import com.hungle.sunriise.mnyobject.impl.DefaultAccount;
import com.hungle.sunriise.mnyobject.impl.DefaultCurrency;
import com.hungle.sunriise.util.BalanceUtils;
import com.hungle.sunriise.util.ComparatorUtils;
import com.hungle.sunriise.util.MnyFilter;
import com.hungle.sunriise.util.RecurringTransactionFilter;
import com.hungle.sunriise.util.StopWatch;

// TODO: Auto-generated Javadoc
/**
 * The Class TableAccountUtil.
 */
public class TableAccountUtils {

    /** The Constant log. */
    private static final Logger LOGGER = org.apache.logging.log4j.LogManager.getLogger(TableAccountUtils.class);

    /** The Constant TABLE_NAME_ACCOUNT. */
    private static final String TABLE_NAME_ACCOUNT = "ACCT";

    /** The Constant COL_ID. */
    private static final String COL_ID = "hacct";

    /** The Constant COL_NAME. */
    private static final String COL_NAME = "szFull";

    /** The Constant COL_CLOSED. */
    private static final String COL_CLOSED = "fClosed";

    /** The Constant COL_AMOUNT_LIMIT. */
    private static final String COL_CREDIT_CARD_AMOUNT_LIMIT = "amtLimit";

    /** The Constant COL_TYPE. */
    private static final String COL_TYPE = "at";

    /** The Constant COL_CURRENCY. */
    private static final String COL_CURRENCY = "hcrnc";

    /** The Constant COL_INVESTMENT_SUB_TYPE. */
    private static final String COL_INVESTMENT_SUB_TYPE = "uat";

    /** The Constant COL_RETIREMENT. */
    private static final String COL_RETIREMENT = "fRetirement";

    /** The Constant COL_STARTING_BALANCE. */
    private static final String COL_STARTING_BALANCE = "amtOpen";

    /** The Constant COL_RELATED_TO_ACCOUNT_ID. */
    private static final String COL_RELATED_TO_ACCOUNT_ID = "hacctRel";

    /**
     * The Class CompareAccountsByName.
     */
    public static class CompareAccountsByName implements Comparator<Account> {

        /**
         * Compare.
         *
         * @param o1 the o 1
         * @param o2 the o 2
         * @return the int
         */
        /*
         * (non-Javadoc)
         * 
         * @see java.util.Comparator#compare(java.lang.Object, java.lang.Object)
         */
        @Override
        public int compare(Account o1, Account o2) {
            return ComparatorUtils.compareNullOrder(o1.getName(), o2.getName());
//            return o1.getName().compareTo(o2.getName());
        }
    }

    /**
     * Get a list of transactions from Account.
     *
     * @param db         msmoney
     * @param account    the account
     * @param mnyContext the mny context
     * @throws IOException Signals that an I/O exception has occurred.
     */
    public static void addTransactionsToAccount(Database db, Account account, MnyContext mnyContext)
            throws IOException {
        List<TransactionFilter> transactionFilters = getTransactionFilters();

        addTransactionsToAccount(db, account, mnyContext, transactionFilters);
    }

    /**
     * Gets the transactions.
     *
     * @param db                 the db
     * @param account            the account
     * @param mnyContext         the mny context
     * @param transactionFilters the transaction filters
     * @throws IOException Signals that an I/O exception has occurred.
     */
    private static void addTransactionsToAccount(final Database db, final Account account, MnyContext mnyContext,
            final List<TransactionFilter> transactionFilters) throws IOException {
        if (account == null) {
            throw new RuntimeException("addTransactionsToAccount, account is null.");
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("> addTransactionsToAccount" + ", account=" + account.getName() + ", id=" + account.getId());
        }

        if (account.getTransactions() != null) {
            if (LOGGER.isDebugEnabled()) {
                LOGGER.warn(
                        "SKIP addTransactionsToAccount" + ", account=" + account.getName() + ", id=" + account.getId());
            }
            return;
        }

        StopWatch stopWatch = new StopWatch();

        List<Transaction> transactions = new ArrayList<Transaction>();
        List<Transaction> filteredTransactions = new ArrayList<Transaction>();

        try {
            // iterate through all transaction's row with matching accountId
            // and add to either list: transactions or filteredTransactions
            Integer accountId = account.getId();

            HashMap<String, Object> rowPattern = new HashMap<String, Object>();
            rowPattern.put(COL_ID, accountId);

            int rows = addTransactionsFromDb2(db, accountId, rowPattern, transactionFilters, mnyContext,
                    filteredTransactions, transactions);
            stopWatch.logTiming(LOGGER, "POST addTransactionsFromDb2, rows=" + rows);

            TableTransactionSplitUtils.handleTransactionSplit(db, transactions);
            stopWatch.logTiming(LOGGER, "POST handleTransactionSplit");

            boolean sortByDate = true;
            if (sortByDate) {
                sortByDate(transactions);
                stopWatch.logTiming(LOGGER, "POST sortByDate");
            }
        } finally {
            long delta = stopWatch.click();
            int count = 0;
            if (transactions != null) {
                count = transactions.size();
            }
            if (LOGGER.isDebugEnabled()) {
                if (count <= 0) {
                    LOGGER.debug("< addTransactionsToAccount, delta=" + delta);
                } else {
                    LOGGER.debug(
                            "< addTransactionsToAccount, delta=" + delta + ", " + ((delta * 1.0) / count) + " per txn");
                }
            }
        }

        account.setTransactions(transactions);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("account.name=" + account.getName() + ", transactions.size=" + transactions.size());
        }

        account.setFilteredTransactions(filteredTransactions);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("  account.name=" + account.getName() + ", filteredTransactions.size="
                    + filteredTransactions.size());
        }
    }

    /**
     * Adds the transactions from db.
     *
     * @param db the db
     * @param accountId the account id
     * @param rowPattern the row pattern
     * @param transactionFilters the transaction filters
     * @param mnyContext the mny context
     * @param filteredTransactions the filtered transactions
     * @param transactions the transactions
     * @return the int
     * @throws IOException Signals that an I/O exception has occurred.
     */
    private static int addTransactionsFromDb(final Database db, Integer accountId, HashMap<String, Object> rowPattern,
            final List<TransactionFilter> transactionFilters, MnyContext mnyContext,
            List<Transaction> filteredTransactions, List<Transaction> transactions) throws IOException {

        int rows = 0;

        Cursor cursor = TableTransactionUtils.createCursor(db);
        for (Row row : cursor) {
            if (cursor.currentRowMatches(rowPattern)) {
                Integer actual = row.getInt(COL_ID);
                if (accountId.compareTo(actual) != 0) {
                    throw new RuntimeException("addTransactionsFromDb, cursor.currentRowMatches() gives wrong answer:"
                            + " expected=" + accountId + ", actual=" + actual);
                }
                TableAccountUtils.addTransactionFromRow(db, row, accountId, mnyContext, transactionFilters,
                        transactions, filteredTransactions);
                rows++;
            }
        }

        return rows;
    }

    /**
     * Adds the transactions from db 2.
     *
     * @param db the db
     * @param accountId the account id
     * @param rowPattern the row pattern
     * @param transactionFilters the transaction filters
     * @param mnyContext the mny context
     * @param filteredTransactions the filtered transactions
     * @param transactions the transactions
     * @return the int
     * @throws IOException Signals that an I/O exception has occurred.
     */
    private static int addTransactionsFromDb2(final Database db, Integer accountId, HashMap<String, Object> rowPattern,
            final List<TransactionFilter> transactionFilters, MnyContext mnyContext,
            List<Transaction> filteredTransactions, List<Transaction> transactions) throws IOException {

        int rows = 0;
        Table table = TableTransactionUtils.getTable(db);
        IndexCursor cursor = CursorBuilder.createCursor(table.getIndex("hacctTrn"));
        for (Row row : cursor.newEntryIterable(accountId)) {
            Integer actual = row.getInt(COL_ID);
            if (accountId.compareTo(actual) != 0) {
                throw new RuntimeException("addTransactionsFromDb2, cursor.currentRowMatches() gives wrong answer:"
                        + " expected=" + accountId + ", actual=" + actual);
            }
            TableAccountUtils.addTransactionFromRow(db, row, accountId, mnyContext, transactionFilters, transactions,
                    filteredTransactions);
            rows++;
        }
        return rows;
    }

    /**
     * Sort by date.
     *
     * @param transactions the transactions
     */
    private static void sortByDate(List<Transaction> transactions) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("> sort");
        }
        try {
            Comparator<Transaction> comparator = new TableTransactionUtils.CompareTransactionsByDate();
            Collections.sort(transactions, comparator);
        } finally {
            if (LOGGER.isDebugEnabled()) {
                LOGGER.debug("< sort");
            }
        }
    }

    /**
     * Gets the transaction filters.
     *
     * @return the transaction filters
     */
    private static List<TransactionFilter> getTransactionFilters() {
        List<TransactionFilter> transactionFilters;
        transactionFilters = new ArrayList<TransactionFilter>();
        transactionFilters.add(new RecurringTransactionFilter());

        // http://jythonpodcast.hostjava.net/jythonbook/chapter10.html
        // transactionFilters =
        // JythonTransactionFilters.getFilters("com.le.sunriise.python");

        return transactionFilters;
    }

    /**
     * Get a list of all accounts sorted by name.
     *
     * @param db msmoney database
     * @return a list of accounts
     * @throws IOException Signals that an I/O exception has occurred.
     */
    public static Map<Integer, Account> getAccounts(Database db) throws IOException {
        boolean resolveLink = true;
        return getAccounts(db, resolveLink);
    }

    /**
     * Get a list of all accounts.
     *
     * @param db          the db
     * @param resolveLink the resolve link
     * @return list of account
     * @throws IOException Signals that an I/O exception has occurred.
     */
    private static Map<Integer, Account> getAccounts(Database db, boolean resolveLink) throws IOException {
        Map<Integer, Account> accounts = new LinkedHashMap<Integer, Account>();

        Cursor cursor = TableAccountUtils.createCursor(db);
        for (Row row : cursor) {
            Account account = TableAccountUtils.deserialize(row);
            if (account != null) {
                accounts.put(account.getId(), account);
            }
        }

        if (resolveLink) {
            resolveRelatedToAccountLink(accounts);
        }

        return accounts;
    }

    /**
     * Resolve link.
     *
     * @param accounts the accounts
     */
    private static void resolveRelatedToAccountLink(Map<Integer, Account> accounts) {
        for (Account account : accounts.values()) {
            Account relatedToAccount = account.getRelatedToAccount();
            if (relatedToAccount == null) {
                // does not have related to account
                continue;
            }
            if (relatedToAccount.getName() != null) {
                // has related to account but resolved already
                continue;
            }

            Integer id = relatedToAccount.getId();
            Account resolvedLinkAccount = accounts.get(id);
            if (LOGGER.isDebugEnabled()) {
                LOGGER.debug("account=" + account + ", relatedToAccount.id=" + id + ", resolvedLinkAccount="
                        + resolvedLinkAccount);
            }

            if (resolvedLinkAccount == null) {
                LOGGER.warn("Cannot find relatedToAccount with id=" + id + ", name=" + relatedToAccount.getName());
            } else {
                if (LOGGER.isDebugEnabled()) {
                    LOGGER.debug("resolveLink, account=" + account.toString() + ", relatedToAccount="
                            + resolvedLinkAccount.toString());
                }
                account.setRelatedToAccount(resolvedLinkAccount);
            }
        }
    }

    /**
     * Marshal data from msmoney row into an Account object.
     * 
     * @param row a db row
     * @return Account
     */
    private static Account deserialize(Row row) {
        Account account = new DefaultAccount();

        String name = row.getString(COL_NAME);
        account.setName(name);

        Integer id = row.getInt(COL_ID);
        account.setId(id);

        Integer relatedToAccountId = row.getInt(COL_RELATED_TO_ACCOUNT_ID);
        setRelatedToAccount(relatedToAccountId, account);

        Integer type = row.getInt(COL_TYPE);
        account.setAccountType(EnumAccountType.toAccountType(type));
        if (account.getAccountType() == EnumAccountType.UNKNOWN) {
            LOGGER.warn("AccountType.UNKNOWN for account=" + account.getName());
        }

        Boolean closed = row.getBoolean(COL_CLOSED);
        account.setClosed(closed);

        BigDecimal startingBalance = row.getBigDecimal(COL_STARTING_BALANCE);
        account.setStartingBalance(startingBalance);

        Integer currencyId = row.getInt(COL_CURRENCY);
        Currency currency = new DefaultCurrency();
        currency.setId(currencyId);
        account.setCurrency(currency);

        Boolean retirement = row.getBoolean(COL_RETIREMENT);
        account.setRetirement(retirement);

        // 0: 403(b)
        // 1: 401k
        // 2: IRA
        // 3: Keogh
        Integer mnyType = row.getInt(COL_INVESTMENT_SUB_TYPE);
        EnumInvestmentSubType investmentSubType = EnumInvestmentSubType.toInvestmentSubType(mnyType);
        if (investmentSubType == EnumInvestmentSubType.UNKNOWN) {
            LOGGER.warn("Invalid EnumInvestmentSubType=" + investmentSubType + ", mnyType=" + mnyType + ", accountName="
                    + name);
        }
        account.setInvestmentSubType(investmentSubType);

        // amtLimit
        BigDecimal creditCardAmountLimit = row.getBigDecimal(COL_CREDIT_CARD_AMOUNT_LIMIT);
        account.setCreditCardAmountLimit(creditCardAmountLimit);

        return account;
    }

    /**
     * Sets the related to account.
     *
     * @param relatedToAccountId the related to account id
     * @param account            the account
     */
    private static void setRelatedToAccount(Integer relatedToAccountId, Account account) {
        Account relatedToAccount = new DefaultAccount();
        if (relatedToAccountId != null) {
            relatedToAccount = new DefaultAccount();
            relatedToAccount.setId(relatedToAccountId);
        } else {
            relatedToAccount = null;
        }
        account.setRelatedToAccount(relatedToAccount);
    }

    /**
     * Gets the related to account.
     *
     * @param db                 the db
     * @param relatedToAccountId the related to account id
     * @return the related to account
     * @throws IOException Signals that an I/O exception has occurred.
     */
    private static Account getRelatedToAccount(Database db, Integer relatedToAccountId) throws IOException {
        Account relatedToAccount = null;

        Table table = TableAccountUtils.getTable(db);
        Row row = CursorBuilder.findRowByPrimaryKey(table, relatedToAccountId);
        if (row != null) {
            relatedToAccount = TableAccountUtils.deserialize(row);
        } else {
            LOGGER.warn("Cannot find relatedToAccountId=" + relatedToAccountId);
        }

        return relatedToAccount;
    }

    /**
     * Creates the account cursor.
     *
     * @param db the db
     * @return the cursor
     * @throws IOException Signals that an I/O exception has occurred.
     */
    public static Cursor createCursor(Database db) throws IOException {
        Table table = TableAccountUtils.getTable(db);
        return CursorBuilder.createCursor(table);
    }

    /**
     * Gets the account table.
     *
     * @param db the db
     * @return the account table
     * @throws IOException Signals that an I/O exception has occurred.
     */
    private static Table getTable(Database db) throws IOException {
        String tableName = TABLE_NAME_ACCOUNT;
        Table table = db.getTable(tableName);
        return table;
    }

    /**
     * Populate account.
     *
     * @param account    the account
     * @param mnyContext the mny context
     * @throws IOException Signals that an I/O exception has occurred.
     */
    public static final void addTransactionsToAccount(final Account account, final MnyContext mnyContext)
            throws IOException {
        StopWatch stopWatch = new StopWatch();

        Database db = mnyContext.getDb();

        addTransactionsToAccount(db, account, mnyContext);
        stopWatch.logTiming(LOGGER, "POST addTransactionsToAccount(db, account, mnyContext)");

        Account relatedToAccount = account.getRelatedToAccount();
        if (relatedToAccount != null) {
            addTransactionsToAccount(db, relatedToAccount, mnyContext);
            stopWatch.logTiming(LOGGER, "POST addTransactionsToAccount(db, account, mnyContext), relatedToAccount");
        }

        updateXferTransactions(account.getTransactions(), mnyContext);
        stopWatch.logTiming(LOGGER, "POST updateXferTransactions");

        Date date = null;
        BalanceUtils.calculateAndSetBalance(account, date, mnyContext);
        stopWatch.logTiming(LOGGER, "POST calculateBalance");

    }

    /**
     * Update xfer transactions.
     *
     * @param transactions the transactions
     * @param mnyContext   the mny context
     */
    private static void updateXferTransactions(List<Transaction> transactions, MnyContext mnyContext) {
        Database db = mnyContext.getDb();
        for (Transaction transaction : transactions) {
            if (!transaction.isTransfer()) {
                continue;
            }

            updateXferTransaction(db, transaction);
        }

        for (Transaction transaction : transactions) {
            if (transaction.hasSplits()) {
                List<TransactionSplit> splits = transaction.getSplits();
                for (TransactionSplit split : splits) {
                    Transaction txn = split.getTransaction();
                    if (txn.isTransfer()) {
                        updateXferTransaction(db, txn);
                    }
                }
            }
        }
    }

    /**
     * Update xfer transaction.
     *
     * @param db the db
     * @param transaction the transaction
     */
    private static void updateXferTransaction(Database db, Transaction transaction) {
        Integer xferId;
        try {
            xferId = TableTransationXferUtils.getXferId(db, transaction);
            // XferInfo xferInfo = new DefaultXferInfo();
            // transaction.setXferInfo(xferInfo);
            // LOGGER.info("updateXferTransactions, transaction.id=" +
            // transaction.getId() + ", xferId=" + xferId);
            if (xferId == null) {
                LOGGER.warn(JSONUtils.writeValueAsString(transaction));
                // throw new RuntimeException("Invalid valid value
                // xferId=null for transaction.id=" + transaction.getId());
            }
            transaction.getXferInfo().setXferTransactionId(xferId);
        } catch (IOException e) {
            LOGGER.warn(e);
        }

        // if (transaction.getId() == 660) {
        // try {
        // LOGGER.info("XXX, " + JSONUtils.writeValueAsString(transaction));
        // LOGGER.info("XXX, " + transaction);
        // LOGGER.info("XXX, " + transaction.getXferInfo());
        // } catch (IOException e) {
        // LOGGER.error(e);
        // }
        // }
    }

    /**
     * Populate accounts.
     *
     * @param mnyContext the mny context
     * @throws IOException Signals that an I/O exception has occurred.
     */
    public static final void populateAccounts(MnyContext mnyContext) throws IOException {
        Collection<Account> accounts = mnyContext.getAccounts().values();
        for (Account account : accounts) {
            addTransactionsToAccount(account, mnyContext);
        }
    }

    /**
     * Adds the transaction from row.
     *
     * @param db                   the db
     * @param row                  the row
     * @param accountId            the account id
     * @param mnyContext           the mny context
     * @param filters              the filters
     * @param transactions         the transactions
     * @param filteredTransactions the filtered transactions
     * @return true, if successful
     * @throws IOException Signals that an I/O exception has occurred.
     */
    private static boolean addTransactionFromRow(Database db, Row row, Integer accountId, MnyContext mnyContext,
            List<TransactionFilter> filters, List<Transaction> transactions, List<Transaction> filteredTransactions)
            throws IOException {

        Transaction transaction = TableTransactionUtils.deserialize(db, row, accountId, mnyContext);
//        Transaction transaction = null;
//        if (transaction == null) {
//            return false;
//        }

        boolean accept = true;
        if (filters != null) {
            for (TransactionFilter filter : filters) {
                if (!filter.accept(transaction, row)) {
                    accept = false;
                    break;
                }
            }
        }

        if (accept) {
            transactions.add(transaction);
        } else {
            filteredTransactions.add(transaction);
        }

        return accept;
    }

    /**
     * Gets the transaction's account.
     * 
     * @param transaction the transaction
     * @param mnyContext  the mny context
     *
     * @return the account
     */
    public static Account getTransactionAccount(Transaction transaction, MnyContext mnyContext) {
        Map<Integer, Account> accounts = mnyContext.getAccounts();
        return accounts.get(transaction.getAccount().getId());
    }

    /**
     * Gets the account name for accountId.
     *
     * @param accountId  the account id
     * @param mnyContext the mny context
     * @return the account name
     */
    public static String getAccountNameFromId(Integer accountId, MnyContext mnyContext) {
        String name = null;

        Account account = mnyContext.getAccounts().get(accountId);
        if (account != null) {
            name = account.getName();
        }

        return name;
    }

    /**
     * Gets the map.
     *
     * @param db the db
     * @return the map
     * @throws IOException Signals that an I/O exception has occurred.
     */
    public static Map<Integer, Account> getMap(Database db) throws IOException {
        return getAccounts(db);
    }

    /**
     * To accounts list.
     *
     * @param accounts the accounts
     * @param filter the filter
     * @return the array list
     */
    public static final List<Account> toAccountsList(final Map<Integer, Account> accounts, MnyFilter<Account> filter) {
        ArrayList<Account> list = new ArrayList<Account>();

        if (accounts == null) {
            return list;
        }
        
        if (filter == null) {
            list.addAll(accounts.values());
        } else {
            for (Account type : accounts.values()) {
                if (filter.accepts(type)) {
                    list.add(type);
                } else {
                    if (LOGGER.isDebugEnabled()) {
                        LOGGER.debug("NOT_SHOW account=" + type.getName());
                    }
                }
            }
        }

        Collections.sort(list, new Comparator<Account>() {

            @Override
            public int compare(Account o1, Account o2) {
                String n1 = o1.getName();
                String n2 = o2.getName();
                return ComparatorUtils.compareNullOrder(n1, n2);

                // if ((n1 == null) && (n2 == null)) {
                // return 0;
                // } else if (n1 == null) {
                // return 1;
                // } else {
                // return n1.compareTo(n2);
                // }
            }
        });

        return list;
    }

    /**
     * To accounts list.
     *
     * @param accounts the accounts
     * @return the list
     */
    public static List<Account> toAccountsList(Map<Integer, Account> accounts) {
        MnyFilter<Account> filter = null;
        return toAccountsList(accounts, filter);
    }

    /**
     * Calculate running balance.
     *
     * @param account the account
     * @return the big decimal
     */
    public static BigDecimal calculateRunningBalance(Account account) {

        BigDecimal runningBalance = account.getStartingBalance();
        if (runningBalance == null) {
            runningBalance = new BigDecimal(0);
        }

        List<Transaction> transactions = account.getTransactions();

        return calculateRunningBalance(transactions, runningBalance);
    }

    /**
     * Calculate running balance.
     *
     * @param transactions the transactions
     * @param runningBalance the running balance
     * @return the big decimal
     */
    private static BigDecimal calculateRunningBalance(List<Transaction> transactions, BigDecimal runningBalance) {
        StopWatch stopWatch = new StopWatch();

        try {
            int max = transactions.size();

            for (int i = 0; i < max; i++) {
                Transaction transaction = transactions.get(i);

                BigDecimal amount = TableAccountUtils.getAmount(transaction);

                runningBalance = runningBalance.add(amount);

                if (LOGGER.isDebugEnabled()) {
                    LOGGER.debug("i=" + i + ", amount=" + amount + ", runningBalance=" + runningBalance);
                }

                transaction.setRunningBalance(runningBalance);
            }
        } finally {
            stopWatch.logTiming(LOGGER, "getRunningBalance");
        }

        return runningBalance;
    }

    /**
     * Gets the amount.
     *
     * @param transaction the transaction
     * @return the amount
     */
    private static BigDecimal getAmount(Transaction transaction) {
        BigDecimal amount = null;
        if (transaction.isVoid() || transaction.isRecurring()) {
            amount = new BigDecimal(0);
        } else {
            amount = transaction.getAmount();
        }
        if (amount == null) {
            amount = new BigDecimal(0);
        }
        return amount;
    }
}

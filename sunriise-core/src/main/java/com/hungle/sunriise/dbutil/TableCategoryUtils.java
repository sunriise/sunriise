/*******************************************************************************
 * Copyright (c) 2016 Hung Le
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *******************************************************************************/
package com.hungle.sunriise.dbutil;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.logging.log4j.Logger;

import com.healthmarketscience.jackcess.Cursor;
import com.healthmarketscience.jackcess.CursorBuilder;
import com.healthmarketscience.jackcess.Database;
import com.healthmarketscience.jackcess.Row;
import com.healthmarketscience.jackcess.Table;
import com.hungle.sunriise.mnyobject.Account;
import com.hungle.sunriise.mnyobject.Category;
import com.hungle.sunriise.mnyobject.InvestmentInfo;
import com.hungle.sunriise.mnyobject.MnyContext;
import com.hungle.sunriise.mnyobject.Security;
import com.hungle.sunriise.mnyobject.Transaction;
import com.hungle.sunriise.mnyobject.TransactionSplit;
import com.hungle.sunriise.mnyobject.XferInfo;
import com.hungle.sunriise.mnyobject.impl.DefaultCategory;
import com.hungle.sunriise.util.ComparatorUtils;
import com.hungle.sunriise.util.MnyFilter;

// TODO: Auto-generated Javadoc
/**
 * The Class TableCategoryUtil.
 */
public class TableCategoryUtils {
    
    /** The Constant LOGGER. */
    private static final Logger LOGGER = org.apache.logging.log4j.LogManager.getLogger(TableCategoryUtils.class);

    /** The Constant TABLE_NAME_CATEGORY. */
    private static final String TABLE_NAME_CATEGORY = "CAT";

    /** The Constant COL_ID. */
    private static final String COL_ID = "hcat";

    /** The Constant COL_NAME. */
    private static final String COL_NAME = "szFull";

    /** The Constant COL_CLASSIFICATION_ID. */
    private static final String COL_CLASSIFICATION_ID = "hct";

    /** The Constant COL_PARENT_ID. */
    private static final String COL_PARENT_ID = "hcatParent";

    /** The Constant COL_LEVEL. */
    private static final String COL_LEVEL = "nLevel";

    /**
     * Gets the map.
     *
     * @param db the db
     * @return the map
     * @throws IOException Signals that an I/O exception has occurred.
     */
    public static Map<Integer, Category> getMap(Database db) throws IOException {
        return getCategories(db);
    }

    /**
     * Gets the categories.
     *
     * @param db the db
     * @return the categories
     * @throws IOException Signals that an I/O exception has occurred.
     */
    private static Map<Integer, Category> getCategories(Database db) throws IOException {
        Map<Integer, Category> categories = new LinkedHashMap<Integer, Category>();

        Cursor cursor = TableCategoryUtils.createCursor(db);
        try {
            for (Row row : cursor) {
                addCategory(row, categories);
            }
        } finally {
            cursor = null;
        }

        return categories;
    }

    /**
     * Gets the category table.
     *
     * @param db the db
     * @return the category table
     * @throws IOException Signals that an I/O exception has occurred.
     */
    private static Table getTable(Database db) throws IOException {
        String tableName = TABLE_NAME_CATEGORY;
        Table table = db.getTable(tableName);
        return table;
    }

    /**
     * Creates the category cursor.
     *
     * @param db the db
     * @return the cursor
     * @throws IOException Signals that an I/O exception has occurred.
     */
    private static Cursor createCursor(Database db) throws IOException {
        Table table = TableCategoryUtils.getTable(db);
        return CursorBuilder.createCursor(table);
    }

    /**
     * Deserialize.
     *
     * @param row the row
     * @return the category
     */
    private static Category deserialize(Row row) {
        Category category = new DefaultCategory();

        String name = row.getString(COL_NAME);
        category.setName(name);

        Integer id = row.getInt(COL_ID);
        category.setId(id);

        Integer parentId = row.getInt(COL_PARENT_ID);
        category.setParentId(parentId);

        Integer classificationId = row.getInt(COL_CLASSIFICATION_ID);
        category.setClassificationId(classificationId);

        Integer level = row.getInt(COL_LEVEL);
        category.setLevel(level);

        return category;
    }

    /**
     * Adds the category.
     *
     * @param row        the row
     * @param categories the categories
     */
    private static void addCategory(Row row, Map<Integer, Category> categories) {
        Category category = deserialize(row);

        categories.put(category.getId(), category);
    }

    /**
     * Gets the top level category.
     *
     * @param categoryId    the category id
     * @param categoriesMap the categories map
     * @return the top level category
     */
    private static Integer getTopLevelCategory(Integer categoryId, Map<Integer, Category> categoriesMap) {
        Integer parentCategoryId = null;
        Category category = categoriesMap.get(categoryId);
        if (category != null) {
            parentCategoryId = category.getParentId();
        }

        Integer topLevelCategoryId = null;
        if (parentCategoryId != null) {
            topLevelCategoryId = parentCategoryId;
        } else {
            topLevelCategoryId = categoryId;
        }

        return topLevelCategoryId;
    }

    /**
     * Transaction has category.
     *
     * @param transaction the transaction
     * @param categories  the categories
     * @param mnyContext  the mny context
     * @return true, if successful
     */
    public static final boolean transactionHasCategory(Transaction transaction, Set<Integer> categories,
            MnyContext mnyContext) {
        Integer categoryId = transaction.getCategory().getId();

        Map<Integer, Category> categoriesMap = mnyContext.getCategories();
        Integer topLevelCategoryId = getTopLevelCategory(categoryId, categoriesMap);
        if (topLevelCategoryId == null) {
            // transaction has no categoryId
            return false;
        }

        if (categories.contains(topLevelCategoryId)) {
            return true;
        }

        return false;
    }

    /**
     * Checks if is expense.
     *
     * @param categoryId the category id
     * @param categories the categories
     * @return true, if is expense
     */
    public static final boolean isExpense(Integer categoryId, Map<Integer, Category> categories) {
//        Map<Integer, Category> categories = mnyContext.getCategories();
        Category category = categories.get(categoryId);
        Integer parentId = category.getParentId();
        if (parentId != null) {
            return isExpense(parentId, categories);
        } else {
            String categoryName = category.getName();
            boolean expense = categoryName.compareToIgnoreCase("EXPENSE") == 0;
            return expense;
        }
    }

    /**
     * Checks if is income.
     *
     * @param categoryId the category id
     * @param categories the categories
     * @return true, if is income
     */
    public static final boolean isIncome(Integer categoryId, Map<Integer, Category> categories) {
        return !isExpense(categoryId, categories);
    }

    /**
     * Gets the category.
     *
     * @param categoryId the category id
     * @param mnyContext the mny context
     * @return the category
     */
    public static final Category getCategory(Integer categoryId, MnyContext mnyContext) {
        Map<Integer, Category> categories = mnyContext.getCategories();
        Category category = categories.get(categoryId);
        return category;
    }

    /**
     * Gets the category name.
     *
     * @param categoryId the category id
     * @param mnyContext the mny context
     * @return the category name
     */
    static final String getCategoryName(Integer categoryId, MnyContext mnyContext) {
        String name = null;
        Category category = getCategory(categoryId, mnyContext);
        if (category != null) {
            name = category.getName();
        }

        return name;
    }

    /**
     * To category string.
     * 
     * @param transaction the transaction
     * @param context     the context
     *
     * @return the string
     */
    public static String getCategoryFullName(Transaction transaction, MnyContext context) {
        String value = null;

        XferInfo xferInfo = transaction.getXferInfo();
        Integer xferAccountId = xferInfo.getXferAccountId();
        if (xferAccountId != null) {
            value = TableCategoryUtils.getCategoryFullNameForXfer(xferAccountId, transaction, context, true);
        } else {
            Integer categoryId = transaction.getCategory().getId();
            String categoryName = TableCategoryUtils.getCategoryFullName(categoryId, context);
            value = categoryName;
        }

        if (value == null) {
            if (transaction.hasSplits()) {
                List<TransactionSplit> splits = transaction.getSplits();
                value = "(" + splits.size() + ") Split Transaction";
            }
        }

        if (transaction.isInvestment()) {
            value = getCategoryFullNameForInvestment(transaction, context);
        }

        return value;
    }

    /**
     * Gets the category full name for investment.
     *
     * @param transaction the transaction
     * @param context the context
     * @return the category full name for investment
     */
    public static String getCategoryFullNameForInvestment(Transaction transaction, MnyContext context) {
        String value = null;
        InvestmentInfo investmentInfo = transaction.getInvestmentInfo();
        Security security = investmentInfo.getSecurity();
        if (security != null) {
            Integer securityId = security.getId();
            String securityName = TableSecurityUtils.getSecurityName(securityId, context);
            value = securityName;
        } else {
            value = null;
        }
        return value;
    }

    /**
     * Gets the category full name for xfer.
     *
     * @param xferAccountId the xfer account id
     * @param transaction the transaction
     * @param context the context
     * @param fromTo the from to
     * @return the category full name for xfer
     */
    public static String getCategoryFullNameForXfer(Integer xferAccountId, Transaction transaction, MnyContext context,
            boolean fromTo) {
        Map<Integer, Account> accounts = context.getAccounts();
        String value = null;
        if (accounts != null) {
            for (Account account : accounts.values()) {
                Integer id = account.getId();
                if (id.equals(xferAccountId)) {
                    value = account.getName();
                    if (fromTo) {
                        if (transaction.getTransactionInfo().isTransferTo()) {
                            value = "Transfer from " + value;
                        } else {
                            value = "Transfer to " + value;
                        }
                    }
                    break;
                }
            }
        }
        return value;
    }

    /**
     * Gets the category full name.
     *
     * @param categoryId the category id
     * @param context the context
     * @return the category full name
     */
    public static String getCategoryFullName(Integer categoryId, MnyContext context) {
        Map<Integer, Category> categories = context.getCategories();
        String categoryName = TableCategoryUtils.getCategoryFullName(categoryId, categories);
        return categoryName;
    }

    /**
     * Gets the category.
     * 
     * @param transaction the transaction
     * @param mnyContext  the mny context
     *
     * @return the category
     */
    private static Category getCategory(Transaction transaction, MnyContext mnyContext) {
        Integer categoryId = transaction.getCategory().getId();
        return getCategory(categoryId, mnyContext);
    }

    /**
     * Gets the category parent id.
     *
     * @param categoryId the category id
     * @param mnyContext the mny context
     * @return the category parent id
     */
    static Integer getCategoryParentId(Integer categoryId, MnyContext mnyContext) {
        Category category = mnyContext.getCategories().get(categoryId);
        if (category != null) {
            categoryId = category.getParentId();
        } else {
            categoryId = null;
        }
        return categoryId;
    }

    /**
     * Gets the category parent name.
     *
     * @param categoryId the category id
     * @param mnyContext the mny context
     * @return the category parent name
     */
    static String getCategoryParentName(Integer categoryId, MnyContext mnyContext) {
        categoryId = getCategoryParentId(categoryId, mnyContext);
        String name = null;
        if (categoryId != null) {
            name = getCategoryName(categoryId, mnyContext);
        } else {
            name = null;
        }
        return name;
    }

    /**
     * Gets the category full name.
     *
     * @param categoryId the category id
     * @param categories the categories
     * @return the category full name
     */
    public static String getCategoryFullName(Integer categoryId, Map<Integer, Category> categories) {
        String sep = ":";
        int depth = 0;
        int maxDepth = 2;
        boolean skipLevel0 = true;
        return TableCategoryUtils.getCategoryName(categoryId, categories, sep, depth, maxDepth, skipLevel0);
    }

    /**
     * Gets the category name.
     *
     * @param categoryId the category id
     * @param categories the categories
     * @param sep        the sep
     * @param depth      the depth
     * @param maxDepth   the max depth
     * @param skipLevel0 the skip level 0
     * @return the category name
     */
    public static String getCategoryName(Integer categoryId, Map<Integer, Category> categories, String sep, int depth,
            int maxDepth, boolean skipLevel0) {
        String categoryName = null;
        if (categoryId == null) {
            return null;
        }
        if (categories == null) {
            return null;
        }

        Category category = categories.get(categoryId);
        if (category == null) {
            return null;
        }

        if (skipLevel0) {
            if (category.getLevel() <= 0) {
                return null;
            }
        }

        if (category.getLevel() > 0) {
            Integer parentId = category.getParentId();
            categoryName = category.getName();
            depth = depth + 1;
            if ((parentId != null) && ((maxDepth > 0) && (depth < maxDepth))) {
                String parentName = getCategoryName(parentId, categories, sep, depth, maxDepth, skipLevel0);
                if (parentName != null) {
                    categoryName = parentName + sep + categoryName;
                }
            }
        } else if (category.getLevel() == 0) {
            categoryName = category.getName();
        } else {
            LOGGER.warn("Invalid category.level=" + category.getLevel());
        }

        return categoryName;
    }

    /**
     * To categories list.
     *
     * @param map the map
     * @param filter the filter
     * @return the list
     */
    public static List<Category> toCategoriesList(Map<Integer, Category> map, MnyFilter<Category> filter) {
        ArrayList<Category> list = new ArrayList<Category>();

        if (filter == null) {
            list.addAll(map.values());
        } else {
            for (Category type : map.values()) {
                if (filter.accepts(type)) {
                    list.add(type);
                } else {
                    if (LOGGER.isDebugEnabled()) {
                        LOGGER.debug("NOT_SHOW category=" + type.getName());
                    }
                }
            }
        }

        Collections.sort(list, new Comparator<Category>() {

            @Override
            public int compare(Category o1, Category o2) {
                String n1 = o1.getName();
                String n2 = o2.getName();
                return ComparatorUtils.compareNullOrder(n1, n2);

                // if ((n1 == null) && (n2 == null)) {
                // return 0;
                // } else if (n1 == null) {
                // return 1;
                // } else {
                // return n1.compareTo(n2);
                // }
            }
        });

        return list;
    }
}

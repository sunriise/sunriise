/*******************************************************************************
 * Copyright (c) 2016 Hung Le
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *******************************************************************************/
package com.hungle.sunriise.dbutil;

import java.io.IOException;

import org.apache.logging.log4j.Logger;

import com.healthmarketscience.jackcess.Cursor;
import com.healthmarketscience.jackcess.CursorBuilder;
import com.healthmarketscience.jackcess.Database;
import com.healthmarketscience.jackcess.Row;
import com.healthmarketscience.jackcess.Table;
import com.hungle.sunriise.mnyobject.InvestmentTransaction;
import com.hungle.sunriise.mnyobject.impl.DefaultInvestmentTransaction;

// TODO: Auto-generated Javadoc
/**
 * The Class TableInvestmentTransactionUtil.
 */
public class TableInvestmentTransactionUtils {

    /** The Constant log. */
    private static final Logger LOGGER = org.apache.logging.log4j.LogManager
            .getLogger(TableInvestmentTransactionUtils.class);

    /** Table name. */
    private static final String TABLE_NAME_INVESTMENT_TRANSACTION = "TRN_INV";

    /** Column names. */
    static final String COL_ID = "htrn";

    /** The Constant COL_PRICE. */
    static final String COL_PRICE = "dPrice";

    /** The Constant COL_QUANTITY. */
    static final String COL_QUANTITY = "qty";

    /** The Constant KEY. */
    // (PK) TRN_INV.htrn, 3
    private static final String KEY = COL_ID;

    // TODO: amtCmn
    // TODO: lott
    // TODO: amtInt

    /**
     * Creates the investment transaction cursor.
     *
     * @param db the db
     * @return the cursor
     * @throws IOException Signals that an I/O exception has occurred.
     */
    @SuppressWarnings("unused")
    private static Cursor createCursor(Database db) throws IOException {
        Table table = getTable(db);
        return CursorBuilder.createCursor(table);
    }

    /**
     * Gets the investment transaction table.
     *
     * @param db the db
     * @return the investment transaction table
     * @throws IOException Signals that an I/O exception has occurred.
     */
    private static Table getTable(Database db) throws IOException {
        String tableName = TABLE_NAME_INVESTMENT_TRANSACTION;
        Table table = db.getTable(tableName);
        return table;
    }

    /**
     * Gets the investment transaction.
     *
     * @param db            the db
     * @param transactionId the id
     * @return the investment transaction
     * @throws IOException Signals that an I/O exception has occurred.
     */
    public static InvestmentTransaction createInvestmentTransaction(Database db, Integer transactionId)
            throws IOException {
        Table table = TableInvestmentTransactionUtils.getTable(db);
        Row row = CursorBuilder.findRowByPrimaryKey(table, transactionId);
        if (row != null) {
            return createFromRow(row, transactionId);
        } else {
            if (LOGGER.isDebugEnabled()) {
                LOGGER.warn("NOT FOUND InvestmentTransaction for primaryKey=" + transactionId + " in table="
                        + table.getName());
            }
            return null;
        }
    }

    /**
     * Creates the from row.
     *
     * @param row           the row
     * @param transactionId the transaction id
     * @return the investment transaction
     * @throws IOException Signals that an I/O exception has occurred.
     */
    private static InvestmentTransaction createFromRow(Row row, Integer transactionId) throws IOException {
        InvestmentTransaction investmentTransaction = null;

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("FOUND InvestmentTransaction for transactionId=" + transactionId);
        }
        investmentTransaction = deserialize(row);

        // double check
        Integer investmentTransactionKey = row.getInt(KEY);
        if ((investmentTransactionKey == null) || (investmentTransactionKey.compareTo(transactionId) != 0)) {
            throw new IOException("CursorBuilder.findRowByPrimaryKey() found wrong row, transactionId=" + transactionId
                    + ", investmentTransaction=" + investmentTransactionKey);
        }

        return investmentTransaction;
    }

    /**
     * Creates an InvestmentTransaction from a Row.
     *
     * @param row the row
     * @return the investment transaction
     */
    static InvestmentTransaction deserialize(Row row) {
        InvestmentTransaction investmentTransaction = new DefaultInvestmentTransaction();

        Integer id = row.getInt(COL_ID);
        investmentTransaction.setId(id);

        Double price = row.getDouble(COL_PRICE);
        investmentTransaction.setPrice(price);

        Double quantity = row.getDouble(COL_QUANTITY);
        investmentTransaction.setQuantity(quantity);

        return investmentTransaction;
    }

}

/*******************************************************************************
 * Copyright (c) 2016 Hung Le
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *******************************************************************************/
package com.hungle.sunriise.dbutil;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.logging.log4j.Logger;

import com.healthmarketscience.jackcess.Cursor;
import com.healthmarketscience.jackcess.CursorBuilder;
import com.healthmarketscience.jackcess.Database;
import com.healthmarketscience.jackcess.Row;
import com.healthmarketscience.jackcess.Table;
import com.hungle.sunriise.mnyobject.SecurityPrice;
import com.hungle.sunriise.mnyobject.impl.DefaultSecurityPrice;

// TODO: Auto-generated Javadoc
/**
 * The Class TableSecurityPriceUtil.
 */
public class TableSecurityPriceUtils {

    /** The Constant log. */
    private static final Logger LOGGER = org.apache.logging.log4j.LogManager.getLogger(TableSecurityPriceUtils.class);

    /** The Constant TABLE_NAME_SECURITY_PRICE. */
    private static final String TABLE_NAME_SECURITY_PRICE = "SP";

    /** The Constant COL_ID. */
    private static final String COL_ID = "hsp";

    public static final String COL_SEC_ID = "hsec";

    /** The Constant COL_PRICE. */
    // "variableLength" : true,
    // "length" : 8,
    // "name" : "dPrice",
    // "type" : "DOUBLE"
    public static final String COL_PRICE = "dPrice";

    // "variableLength" : true,
    // "length" : 8,
    // "name" : "dt",
    // "type" : "SHORT_DATE_TIME"
    /** The Constant COL_PRICE_DATE. */
    public static final String COL_DATE = "dt";

    // "variableLength" : true,
    // "length" : 4,
    // "name" : "src",
    // "type" : "LONG"
    private static final String COL_SRC = "src";

    /**
     * Gets the security price table.
     *
     * @param db the db
     * @return the security price table
     * @throws IOException Signals that an I/O exception has occurred.
     */
    public static Table getTable(Database db) throws IOException {
        String tableName = TABLE_NAME_SECURITY_PRICE;
        Table table = db.getTable(tableName);
        return table;
    }

    /**
     * Creates the security price cursor.
     *
     * @param db the db
     * @return the cursor
     * @throws IOException Signals that an I/O exception has occurred.
     */
    public static Cursor createCursor(Database db) throws IOException {
        Table table = getTable(db);
        return CursorBuilder.createCursor(table);
    }

    public static List<SecurityPrice> getSecurityPrices(Database db) throws IOException {
        List<SecurityPrice> prices = new ArrayList<SecurityPrice>();

        Cursor cursor = TableSecurityPriceUtils.createCursor(db);
        int rowCount = cursor.getTable().getRowCount();
        int i = 0;
        try {
            for (Row row : cursor) {
                i++;
                addSecurityPrice(row, prices);
            }
        } finally {
            cursor = null;
        }

        LOGGER.info("i=" + i + ", rowCount=" + rowCount);

        return prices;
    }

    private static void addSecurityPrice(Row row, List<SecurityPrice> prices) {
        SecurityPrice price = deserialize(row);

        if (price != null) {
            if (LOGGER.isDebugEnabled()) {
                LOGGER.debug("Adding securityPrice, id=" + price.getId() + ", securityPrice=" + price);
            }
            prices.add(price);
        }
    }

    public static SecurityPrice deserialize(Row row) {
        SecurityPrice price = new DefaultSecurityPrice();

        Integer id = row.getInt(COL_ID);
        price.setId(id);

        Integer secId = row.getInt(COL_SEC_ID);
        price.setSecId(secId);

        Date priceDate = row.getDate(COL_DATE);
        price.setDate(priceDate);

        Double secPrice = row.getDouble(COL_PRICE);
        price.setPrice(secPrice);

        Integer src = row.getInt(COL_SRC);
        price.setSrc(src);

        return price;
    }
}

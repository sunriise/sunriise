/*******************************************************************************
 * Copyright (c) 2016 Hung Le
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *******************************************************************************/
package com.hungle.sunriise.dbutil;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.Comparator;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.Logger;

import com.healthmarketscience.jackcess.Cursor;
import com.healthmarketscience.jackcess.CursorBuilder;
import com.healthmarketscience.jackcess.Database;
import com.healthmarketscience.jackcess.Row;
import com.healthmarketscience.jackcess.Table;
import com.hungle.sunriise.io.MnyDb;
import com.hungle.sunriise.mnyobject.AccountLink;
import com.hungle.sunriise.mnyobject.CategoryLink;
import com.hungle.sunriise.mnyobject.Classification1Link;
import com.hungle.sunriise.mnyobject.Frequency;
import com.hungle.sunriise.mnyobject.InvestmentInfo;
import com.hungle.sunriise.mnyobject.InvestmentTransaction;
import com.hungle.sunriise.mnyobject.MnyContext;
import com.hungle.sunriise.mnyobject.Payee;
import com.hungle.sunriise.mnyobject.PayeeLink;
import com.hungle.sunriise.mnyobject.Security;
import com.hungle.sunriise.mnyobject.Transaction;
import com.hungle.sunriise.mnyobject.TransactionInfo;
import com.hungle.sunriise.mnyobject.impl.DefaultAccountLink;
import com.hungle.sunriise.mnyobject.impl.DefaultCategoryLink;
import com.hungle.sunriise.mnyobject.impl.DefaultClassification1Link;
import com.hungle.sunriise.mnyobject.impl.DefaultFrequency;
import com.hungle.sunriise.mnyobject.impl.DefaultInvestmentInfo;
import com.hungle.sunriise.mnyobject.impl.DefaultInvestmentTransaction;
import com.hungle.sunriise.mnyobject.impl.DefaultPayeeLink;
import com.hungle.sunriise.mnyobject.impl.DefaultSecurity;
import com.hungle.sunriise.mnyobject.impl.DefaultTransaction;
import com.hungle.sunriise.mnyobject.impl.DefaultTransactionInfo;
import com.hungle.sunriise.mnyobject.impl.InvestmentActivity;
import com.hungle.sunriise.mnyobject.impl.InvestmentActivityTable;
import com.hungle.sunriise.util.ComparatorUtils;

// TODO: Auto-generated Javadoc
/**
 * The Class TableTransactionUtil.
 */
public class TableTransactionUtils {

    /**
     * The Class CompareByDate.
     */
    public static final class CompareTransactionsByDate implements Comparator<Transaction> {

        /*
         * (non-Javadoc)
         * 
         * @see java.util.Comparator#compare(java.lang.Object, java.lang.Object)
         */
        @Override
        public int compare(Transaction transaction1, Transaction transaction2) {
            return ComparatorUtils.compareNullOrder(transaction1.getDate(), transaction2.getDate());
//            Date d1 = transaction1.getDate();
//            Date d2 = transaction2.getDate();
//
//            if ((d1 == null) && (d2 == null)) {
//                return 0;
//            }
//
//            if (d1 == null) {
//                return 1;
//            }
//
//            if (d2 == null) {
//                return -1;
//            }
//
//            return d1.compareTo(d2);
        }
    }

    /** The Constant log. */
    private static final Logger LOGGER = org.apache.logging.log4j.LogManager.getLogger(TableTransactionUtils.class);

    /** The Constant TABLE_NAME_TRANSACTION. */
    private static final String TABLE_NAME_TRANSACTION = "TRN";

    // # Primary keys:
    // (PK) TRN.htrn, 6419
    // (referencing-FK) TRN_INV.htrn
    // (referencing-FK) TRN_SPLIT.htrn
    // (referencing-FK) TRN_SPLIT.htrnParent
    // (referencing-FK) TRN_XFER.htrnFrom
    // (referencing-FK) TRN_XFER.htrnLink
    // (referencing-FK) PMT.htrnPmt
    // (referencing-FK) TRN.htrnSrc
    // (referencing-FK) TRN_INVOICE.htrn
    // (referencing-FK) TRN_OL.htrn
    /** The Constant COL_ID. */
    private static final String COL_ID = "htrn";

    /** The Constant COL_ACCOUNT_ID. */
    public static final String COL_ACCOUNT_ID = "hacct";

    /** The Constant COL_NUMBER. */
    private static final String COL_NUMBER = "szId";

    /** The Constant COL_MEMO. */
    private static final String COL_MEMO = "mMemo";

    /** The Constant COL_INVESTMENT_ACTIVITY. */
    private static final String COL_INVESTMENT_ACTIVITY = "act";

    /** The Constant COL_SECURITY_ID. */
    private static final String COL_SECURITY_ID = "hsec";

    /** The Constant COL_TRANSFERRED_ACCOUNT_ID. */
    private static final String COL_XFER_ACCOUNT_ID = "hacctLink";

    /** The Constant COL_PAYEE_ID. */
    private static final String COL_PAYEE_ID = "lHpay";

    /** The Constant COL_CATEGORY_ID. */
    private static final String COL_CATEGORY_ID = "hcat";

    /** The Constant COL_CLASSIFICATION_1_ID. */
    private static final String COL_CLASSIFICATION_1_ID = "lHcls1";

    /** The Constant COL_FREQUENCY. */
    private static final String COL_FREQUENCY = "frq";

    /** The Constant COL_FREQUENCY_INSTRUCTION. */
    private static final String COL_FREQUENCY_INSTRUCTION = "cFrqInst";

    /** The Constant COL_FI_STMT_ID. */
    private static final String COL_FI_STMT_ID = "mFiStmtId";

    /** The Constant COL_DATE. */
    private static final String COL_DATE = "dt";

    /** The Constant COL_GRFTT. */
    private static final String COL_GRFTT = "grftt";

    /** The Constant COL_CLEARED_STATE. */
    private static final String COL_CLEARED_STATE = "cs";

    /** The Constant COL_UNACCEPTED. */
    private static final String COL_UNACCEPTED = "rt";

    /** The Constant COL_AMOUNT. */
    private static final String COL_AMOUNT = "amt";

    /** The Constant INDEX_TRANSACTION_SPLIT. */
    static final String INDEX_TRANSACTION_SPLIT = "htrnTrnSplit";

    public static Map<Integer, Transaction> getMap(Database db) throws IOException {
        return getTransactions(db);
    }

    /**
     * Gets the transactions.
     *
     * @param db the db
     * @return the transactions
     * @throws IOException Signals that an I/O exception has occurred.
     */
    public static final Map<Integer, Transaction> getTransactions(Database db) throws IOException {
        MnyContext mnyContext = null;
        // traverse transactions
        Cursor cursor = createCursor(db);
        Map<Integer, Transaction> transactions = new LinkedHashMap<>();
        Transaction transaction = null;
        for (Row row : cursor) {
            // row = cursor.getCurrentRow();
            transaction = createTransaction(db, row, mnyContext);
            transactions.put(transaction.getId(), transaction);
        }
        return transactions;
    }

    /**
     * Gets the transaction table.
     *
     * @param db the db
     * @return the transaction table
     * @throws IOException Signals that an I/O exception has occurred.
     */
    public static Table getTable(Database db) throws IOException {
        String tableName = TABLE_NAME_TRANSACTION;
        Table table = db.getTable(tableName);
        return table;
    }

    /**
     * Creates the transaction cursor.
     *
     * @param db the db
     * @return the cursor
     * @throws IOException Signals that an I/O exception has occurred.
     */
    static final Cursor createCursor(Database db) throws IOException {
        Table table = TableTransactionUtils.getTable(db);
        return CursorBuilder.createCursor(table);
    }

    /**
     * Creates the transaction.
     *
     * @param db         the db
     * @param row        the row
     * @param accountId  the account id
     * @param mnyContext the mny context
     * @return the transaction
     * @throws IOException Signals that an I/O exception has occurred.
     */
    static Transaction deserialize(Database db, Row row, Integer accountId, MnyContext mnyContext) throws IOException {
        Transaction transaction = new DefaultTransaction();

        AccountLink account = new DefaultAccountLink(accountId);
        transaction.setAccount(account);
        if (mnyContext != null) {
            account.setName(TableAccountUtils.getAccountNameFromId(accountId, mnyContext));
        } else {
            // TODO: get name from db
        }

        // transaction id
        Integer htrn = row.getInt(COL_ID);
        transaction.setId(htrn);

        // fiTransactionId
        String fiTransactionId = row.getString(COL_FI_STMT_ID);
        transaction.setFiTransactionId(fiTransactionId);

        // amount
        BigDecimal amount = row.getBigDecimal(COL_AMOUNT);
        transaction.setAmount(amount);

        // TableID index ColumnName comments
        // TRN 7 cs "cleared state?
        // 0 == not cleared
        // 1 == cleared
        // 2 == reconciled

        // transfer to account
        Integer xferAccountId = row.getInt(COL_XFER_ACCOUNT_ID);
        // XferInfo xferInfo = new DefaultXferInfo();
        // transaction.setXferInfo(xferInfo);
        // xferInfo.setXferAccountId(xferAccountId);
        transaction.getXferInfo().setXferAccountId(xferAccountId);

        Integer clearedState = row.getInt(COL_CLEARED_STATE);
        transaction.setClearedState(clearedState);

        // rt == 0 unaccepted
        Integer unaccepted = row.getInt(COL_UNACCEPTED);
        if (unaccepted != null) {
            if (unaccepted == -1) {
                transaction.setUnaccepted(false);
            } else {
                transaction.setUnaccepted(true);
            }
        } else {
            transaction.setUnaccepted(false);
        }

        // flags? we are currently using this to figure out which transaction to
        // skip/void
        Integer grftt = row.getInt(COL_GRFTT);
        transaction.setStatusFlag(grftt);
        if (grftt != null) {
            TransactionInfo transactionInfo = new DefaultTransactionInfo();
            transactionInfo.setFlag(grftt);
            boolean transferTo = TableTransationXferUtils.isTransferTo(db, transaction.getId());
            transactionInfo.setTransferTo(transferTo);
            transaction.setTransactionInfo(transactionInfo);
        }

        // date
        Date date = row.getDate(COL_DATE);
        transaction.setDate(date);

        // frequency for recurring transaction?
        Frequency frequency = createFrequency(row);
        transaction.setFrequency(frequency);

        // category
        Integer categoryId = row.getInt(COL_CATEGORY_ID);
        CategoryLink category = new DefaultCategoryLink(categoryId);
        transaction.setCategory(category);
        if (mnyContext != null) {
            category.setFullName(TableCategoryUtils.getCategoryFullName(transaction, mnyContext));
            category.setName(TableCategoryUtils.getCategoryName(categoryId, mnyContext));

            // parent
            category.setParentId(TableCategoryUtils.getCategoryParentId(categoryId, mnyContext));
            category.setParentName(TableCategoryUtils.getCategoryParentName(categoryId, mnyContext));
        } else {
            // TODO: look up from db
        }

        Integer classification1Id = row.getInt(COL_CLASSIFICATION_1_ID);
        Classification1Link classification1 = new DefaultClassification1Link(classification1Id);
        transaction.setClassification1(classification1);

        // payee
        Integer payeeId = row.getInt(COL_PAYEE_ID);
        PayeeLink payee = new DefaultPayeeLink(payeeId);
        transaction.setPayee(payee);
        if (mnyContext != null) {
            payee.setName(TableTransactionUtils.toPayeeString(mnyContext, transaction));
        } else {
            // TODO: lookup from db
        }

        // hsec: security
        InvestmentInfo investmentInfo = new DefaultInvestmentInfo();
        transaction.setInvestmentInfo(investmentInfo);

        // act: Investment activity: Buy, Sell ..
        Integer activity = row.getInt(COL_INVESTMENT_ACTIVITY);
        InvestmentActivity investmentActivity = new InvestmentActivity(activity);
        investmentInfo.setActivity(investmentActivity);

        InvestmentTransaction investmentTransaction = createInvestmentTransaction(db, transaction);
        investmentInfo.setTransaction(investmentTransaction);
        Integer securityId = row.getInt(COL_SECURITY_ID);
        Security security = null;
        if (mnyContext != null) {
            security = mnyContext.getSecurities().get(securityId);
        } else {
            // TODO: lookup from db
            security = new DefaultSecurity();
            security.setId(securityId);
        }
        investmentInfo.setSecurity(security);

        // mMemo
        String memo = row.getString(COL_MEMO);
        transaction.setMemo(memo);

        // szId
        String szId = row.getString(COL_NUMBER);
        transaction.setNumber(szId);

        return transaction;
    }

    /**
     * Creates the investment transaction.
     *
     * @param db          the db
     * @param transaction the transaction
     * @return the investment transaction
     * @throws IOException Signals that an I/O exception has occurred.
     */
    private static InvestmentTransaction createInvestmentTransaction(Database db, Transaction transaction)
            throws IOException {
        InvestmentTransaction investmentTransaction = null;
        if (transaction.isInvestment()) {
            investmentTransaction = TableInvestmentTransactionUtils.createInvestmentTransaction(db,
                    transaction.getId());
            if (investmentTransaction == null) {
                InvestmentInfo investmentInfo = transaction.getInvestmentInfo();
                if (activityHasNoInvestmentTransaction(investmentInfo.getActivity())) {
                    // OK
                } else {
                    LOGGER.warn("Cannot find investmentTransaction for id=" + transaction.getId()
                            + ", investmentActivity=" + investmentInfo.getActivity());
                }
                // create an empty one
                investmentTransaction = new DefaultInvestmentTransaction();
            }
        }
        return investmentTransaction;
    }

    /**
     * Activity has no investment transaction.
     *
     * @param investmentActivity the investment activity
     * @return true, if successful
     */
    private static boolean activityHasNoInvestmentTransaction(InvestmentActivity investmentActivity) {
        // OK, Dividend and Interest might not have
        // investmentTransaction
        boolean rv = false;
        switch (investmentActivity.getType()) {
        case InvestmentActivityTable.DIVIDEND:
        case InvestmentActivityTable.INTEREST:
        case InvestmentActivityTable.S_TERM_CAP_GAINS_DIST:
        case InvestmentActivityTable.L_TERM_CAP_GAINS_DIST:
            rv = true;
            break;
        }

        return rv;
    }

    /**
     * Creates the frequency.
     *
     * @param row the row
     * @return the frequency
     */
    private static Frequency createFrequency(Row row) {
        Frequency frequency = new DefaultFrequency();
        Integer frq = row.getInt(COL_FREQUENCY);
        frequency.setType(frq);
        Double cFrqInst = row.getDouble(COL_FREQUENCY_INSTRUCTION);
        frequency.setcFrqInst(cFrqInst);
        // see: transaction.setStatusFlag(grftt);
        // frequency.setGrftt(grftt);
        return frequency;
    }

    /**
     * Creates the transaction.
     *
     * @param db         the db
     * @param row        the row
     * @param mnyContext the mny context
     * @return the transaction
     * @throws IOException Signals that an I/O exception has occurred.
     */
    public static Transaction createTransaction(Database db, Row row, MnyContext mnyContext) throws IOException {
        Integer accountId = row.getInt(COL_ACCOUNT_ID);
        return deserialize(db, row, accountId, mnyContext);
    }

    /**
     * Find row by primary key.
     *
     * @param db the db
     * @param id the id
     * @return the row
     * @throws IOException Signals that an I/O exception has occurred.
     */
    public static final Row findRowByPrimaryKey(Database db, Integer id) throws IOException {
        return CursorBuilder.findRowByPrimaryKey(getTable(db), id);
    }

    /**
     * Gets the transaction row.
     *
     * @param mnyDb the opened db
     * @param id    the id
     * @return the transaction row
     * @throws IOException Signals that an I/O exception has occurred.
     */
    public static final Row getRowById(MnyDb mnyDb, Integer id) throws IOException {
        if (mnyDb == null) {
            return null;
        }
        Database db = mnyDb.getDb();
        if (db == null) {
            return null;
        }

        Row row = findRowByPrimaryKey(db, id);

        return row;
    }

    /**
     * Gets the unaccepted.
     *
     * @param row the row
     * @return the unaccepted
     */
    public static Integer getUnaccepted(Row row) {
        return row.getInt(COL_UNACCEPTED);
    }

    /**
     * Gets the binary string.
     *
     * @param flag the flag
     * @return the binary string
     */
    public static final String getBinaryString(Integer flag) {
        return StringUtils.leftPad(Integer.toBinaryString(flag), 16, '0');
    }

    /**
     * To payee string.
     *
     * @param mnyContext  the mny context
     * @param transaction the transaction
     * @return the object
     */
    private static final String toPayeeString(MnyContext mnyContext, Transaction transaction) {
        String value;
        Integer payeeId = transaction.getPayee().getId();
        String payeeName = null;
        if (payeeId != null) {
            Map<Integer, Payee> payees = mnyContext.getPayees();
            if (payees != null) {
                Payee payee = payees.get(payeeId);
                if (payee != null) {
                    payeeName = payee.getName();
                }
            }
        }
        if (payeeName == null) {
            payeeName = "";
        }
        value = payeeName;

        if (transaction.isInvestment()) {
            value = "UNKNOWN";
            InvestmentInfo investmentInfo = transaction.getInvestmentInfo();
            InvestmentActivity investmentActivity = investmentInfo.getActivity();
            if (investmentActivity != null) {
                value = investmentActivity.toString();
            }
        }

        return value;
    }

    /**
     * Creates the transaction.
     *
     * @param db  the db
     * @param row the row
     * @return the transaction
     * @throws IOException Signals that an I/O exception has occurred.
     */
    public static Transaction createTransaction(Database db, Row row) throws IOException {
        MnyContext mnyContext = null;
        return createTransaction(db, row, mnyContext);
    }
}

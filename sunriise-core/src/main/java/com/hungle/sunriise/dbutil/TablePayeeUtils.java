/*******************************************************************************
 * Copyright (c) 2016-2019 Hung Le
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *******************************************************************************/
package com.hungle.sunriise.dbutil;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.Logger;

import com.healthmarketscience.jackcess.Cursor;
import com.healthmarketscience.jackcess.CursorBuilder;
import com.healthmarketscience.jackcess.Database;
import com.healthmarketscience.jackcess.Row;
import com.healthmarketscience.jackcess.Table;
import com.hungle.sunriise.mnyobject.Payee;
import com.hungle.sunriise.mnyobject.impl.DefaultPayee;
import com.hungle.sunriise.util.ComparatorUtils;
import com.hungle.sunriise.util.MnyFilter;

// TODO: Auto-generated Javadoc
/**
 * The Class TablePayeeUtil.
 */
public class TablePayeeUtils {
    private static final Logger LOGGER = org.apache.logging.log4j.LogManager.getLogger(TablePayeeUtils.class);

    /** The Constant TABLE_NAME_PAYEE. */
    private static final String TABLE_NAME_PAYEE = "PAY";

    /** The Constant COL_ID. */
    private static final String COL_ID = "hpay";

    /** The Constant COL_NAME. */
    private static final String COL_NAME = "szFull";

    /** The Constant COL_PARENT_ID. */
    private static final String COL_PARENT_ID = "hpayParent";

    public static Map<Integer, Payee> getMap(Database db) throws IOException {
        return getPayees(db);
    }

    /**
     * Gets the payees.
     *
     * @param db the db
     * @return the payees
     * @throws IOException Signals that an I/O exception has occurred.
     */
    public static Map<Integer, Payee> getPayees(Database db) throws IOException {
        Map<Integer, Payee> payees = new LinkedHashMap<Integer, Payee>();

        Cursor cursor = null;
        try {
            cursor = TablePayeeUtils.createCursor(db);
            for (Row row : cursor) {
                addPayee(row, payees);
            }
        } finally {
            cursor = null;
        }

        return payees;
    }

    /**
     * Gets the payee table.
     *
     * @param db the db
     * @return the payee table
     * @throws IOException Signals that an I/O exception has occurred.
     */
    private static Table getTable(Database db) throws IOException {
        String tableName = TABLE_NAME_PAYEE;
        Table table = db.getTable(tableName);
        return table;
    }

    /**
     * Creates the payee cursor.
     *
     * @param db the db
     * @return the cursor
     * @throws IOException Signals that an I/O exception has occurred.
     */
    private static Cursor createCursor(Database db) throws IOException {
        Table table = TablePayeeUtils.getTable(db);
        return CursorBuilder.createCursor(table);
    }

    /**
     * Creates the payee.
     *
     * @param row the row
     * @return the payee
     */
    private static Payee deserialize(Row row) {
        Payee payee = new DefaultPayee();

        String name = row.getString(COL_NAME);
        // if (StringUtils.isEmpty(name)) {
        // continue;
        // }
        payee.setName(name);

        Integer id = row.getInt(COL_ID);
        payee.setId(id);

        Integer parentId = row.getInt(COL_PARENT_ID);
        payee.setParent(parentId);

        return payee;
    }

    /**
     * Adds the payee.
     *
     * @param row    the row
     * @param payees the payees
     */
    private static void addPayee(Row row, Map<Integer, Payee> payees) {
        Payee payee = deserialize(row);

        Integer id = payee.getId();
        payees.put(id, payee);
    }

    public static List<Payee> toPayeesList(Map<Integer, Payee> map, MnyFilter<Payee> filter) {
        ArrayList<Payee> list = new ArrayList<Payee>();

        if (filter == null) {
            list.addAll(map.values());
        } else {
            for (Payee type : map.values()) {
                if (filter.accepts(type)) {
                    list.add(type);
                } else {
                    if (LOGGER.isDebugEnabled()) {
                        LOGGER.debug("NOT_SHOW payee=" + type.getName());
                    }
                }
            }
        }

        Collections.sort(list, new Comparator<Payee>() {

            @Override
            public int compare(Payee o1, Payee o2) {
                String n1 = o1.getName();
                String n2 = o2.getName();
                return ComparatorUtils.compareNullOrder(n1, n2);

                // if ((n1 == null) && (n2 == null)) {
                // return 0;
                // } else if (n1 == null) {
                // return 1;
                // } else {
                // return n1.compareTo(n2);
                // }
            }
        });

        return list;

    }

}

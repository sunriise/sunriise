package com.hungle.sunriise.dbutil;

import java.io.IOException;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;

import com.healthmarketscience.jackcess.Cursor;
import com.healthmarketscience.jackcess.CursorBuilder;
import com.healthmarketscience.jackcess.Database;
import com.healthmarketscience.jackcess.Row;
import com.healthmarketscience.jackcess.Table;
import com.hungle.sunriise.mnyobject.Bill;
import com.hungle.sunriise.mnyobject.Frequency;
import com.hungle.sunriise.mnyobject.impl.DefaultBill;
import com.hungle.sunriise.mnyobject.impl.DefaultFrequency;

// TODO: Auto-generated Javadoc
/**
 * The Class TableBillUtil.
 */
public class TableBillUtils {

    /** The Constant TABLE_NAME_BILL. */
    private static final String TABLE_NAME_BILL = "BILL";

    /** The Constant COL_ID. */
    private static final String COL_ID = "hbill";

    // "name" : "BILL",
    // "name" : "hbill",
    // "name" : "st",
    // "name" : "frq",
    // "name" : "cFrqInst",
    // "name" : "dt",
    // "name" : "iinstNextUnpaid",
    // "name" : "dtMax",
    // "name" : "cInstMax",
    // "name" : "dtSerial",
    // "name" : "itrnLink",
    // "name" : "hbillHead",
    // "name" : "iinst",
    // "name" : "itrn",
    // "name" : "lHtrn",
    // "name" : "iinstLastSkipped",
    // "name" : "cEstInst",
    // "name" : "cDaysAutoEnter",
    // "name" : "sguid",
    // "name" : "fUpdated",

    /** The Constant COL_BILL_HEAD_ID. */
    private static final String COL_BILL_HEAD_ID = "hbillHead";

    /** The Constant COL_DAYS_AUTO_ENTER. */
    private static final String COL_DAYS_AUTO_ENTER = "cDaysAutoEnter";

    /** The Constant COL_DATE. */
    private static final String COL_DATE = "dt";

    /** The Constant COL_TRANSACTION_ID. */
    private static final String COL_TRANSACTION_ID = "lHtrn";

    /** The Constant COL_FREQUENCY. */
    private static final String COL_FREQUENCY = "frq";

    /** The Constant COL_FREQUENCY_INSTRUCTION. */
    private static final String COL_FREQUENCY_INSTRUCTION = "cFrqInst";

    /**
     * Creates the cursor.
     *
     * @param db the db
     * @return the cursor
     * @throws IOException Signals that an I/O exception has occurred.
     */
    private static Cursor createCursor(Database db) throws IOException {
        Table table = TableBillUtils.getTable(db);
        return CursorBuilder.createCursor(table);
    }

    /**
     * Gets the table.
     *
     * @param db the db
     * @return the table
     * @throws IOException Signals that an I/O exception has occurred.
     */
    private static Table getTable(Database db) throws IOException {
        String tableName = TABLE_NAME_BILL;
        Table table = db.getTable(tableName);
        return table;
    }

    /**
     * Gets the bills.
     *
     * @param db the db
     * @return the bills
     * @throws IOException Signals that an I/O exception has occurred.
     */
    public static Map<Integer, Bill> getBills(Database db) throws IOException {
        Map<Integer, Bill> bills = new LinkedHashMap<Integer, Bill>();

        Cursor cursor = TableBillUtils.createCursor(db);
        try {
            for (Row row : cursor) {
                addBill(row, bills);
            }
        } finally {
            cursor = null;
        }

        return bills;
    }

    /**
     * Adds the bill.
     *
     * @param row   the row
     * @param bills the bills
     */
    private static void addBill(Row row, Map<Integer, Bill> bills) {
        Bill bill = deserialize(row);

        bills.put(bill.getId(), bill);
    }

    private static Bill deserialize(Row row) {
        Bill bill = new DefaultBill();

        Integer id = row.getInt(COL_ID);
        bill.setId(id);

        Integer billHeadId = row.getInt(COL_BILL_HEAD_ID);
        bill.setBillHeadId(billHeadId);

        // lHtrn
        Integer transactionId = row.getInt(COL_TRANSACTION_ID);
        bill.setTransactionId(transactionId);

        // cDaysAutoEnter
        Integer daysAutoEnter = row.getInt(COL_DAYS_AUTO_ENTER);
        bill.setDaysAutoEnter(daysAutoEnter);

        // bill date?
        Date date = row.getDate(COL_DATE);
        bill.setDate(date);

        // frequency
        Frequency frequency = createFrequency(row);
        bill.setFrequency(frequency);

        return bill;
    }

    /**
     * Creates the frequency.
     *
     * @param row the row
     * @return the frequency
     */
    private static Frequency createFrequency(Row row) {
        Frequency frequency = new DefaultFrequency();
        Integer frq = row.getInt(COL_FREQUENCY);
        frequency.setType(frq);
        Double cFrqInst = row.getDouble(COL_FREQUENCY_INSTRUCTION);
        frequency.setcFrqInst(cFrqInst);
        // see: transaction.setStatusFlag(grftt);
        // frequency.setGrftt(grftt);
        return frequency;
    }

}

package com.hungle.sunriise.dbutil;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.logging.log4j.Logger;

import com.healthmarketscience.jackcess.Cursor;
import com.healthmarketscience.jackcess.CursorBuilder;
import com.healthmarketscience.jackcess.Database;
import com.healthmarketscience.jackcess.Row;
import com.healthmarketscience.jackcess.Table;
import com.hungle.sunriise.mnyobject.TaxRate;
import com.hungle.sunriise.mnyobject.impl.DefaultTaxRate;
import com.hungle.sunriise.mnyobject.impl.TaxBracket;

public class TableTaxRatesUtils {
    private static final Logger LOGGER = org.apache.logging.log4j.LogManager.getLogger(TableTaxRatesUtils.class);

    public static final String COL_BRACKET_RATE_PREFIX = "dRate";

    public static final String COL_BRACKET_HIGH_PREFIX = "damtHigh";

    public static final String COL_BRACKET_LOW_PREFIX = "damtLow";

    static final int MAX_BRACKETS = 6;

    public static final String COL_ID = "hTax Rate Custom Pool";

    private static final String COL_LTCGAINS_RATE = "dRateCapGains";

    private static final String COL_DIVIDENS_RATE = "dRateDividends";

    private static final String COL_STANDARD_DEDUCTION = "damtStdDed";

    private static final String COL_BLIND = "damtDedBlind";

    private static final String COL_EXEMPTION_AMOUNT = "damtStdEx";

    private static final String COL_OVER_65 = "damtDedOver65";

    private static final String COL_EXEPTION_CUT_OFF = "damtThreshExemp";

    private static final String COL_DEDUCTION_CUT_OFF = "damtThreshDed";

    private static final String COL_MAXIMUM_CAPITAL_LOSS = "damtMaxCapLoss";

    private static final String FILING_STATUS_SINGLE = "TRSingle";
    private static final String FILING_STATUS_SINGLE_LABEL = "Single";

    private static final String FILING_STATUS_JOINT = "TRJoint";
    private static final String FILING_STATUS_JOINT_LABEL = "Filing Jointly";

    private static final String FILING_STATUS_CUSTOM = "TRCustom";
    private static final String FILING_STATUS_CUSTOM_LABEL = "Custom";

    private static final String FILING_STATUS_H_HOUSE = "TRHHouse";
    private static final String FILING_STATUS_H_HOUSE_LABEL = "Heads of Household";

    private static final String FILING_STATUS_SEPARATE = "TRSeparate";
    private static final String FILING_STATUS_SEPARATE_LABEL = "Filing Separately";

    private static final String[] FILING_STATUSES = { FILING_STATUS_SINGLE, FILING_STATUS_JOINT, FILING_STATUS_CUSTOM,
            FILING_STATUS_H_HOUSE, FILING_STATUS_SEPARATE };

    static final Set<String> FILING_STATUSES_SET = new HashSet<String>(Arrays.asList(FILING_STATUSES));

//    {
//        "name" : "Tax Rate Custom Pool",
    public static final String TABLE_NAME_TAX_RATE = "Tax Rate Custom Pool";

//        "hidden" : false,
//        "columns" : [ {
//          "length" : 4,
//          "name" : "hTax Rate Custom Pool",
//          "type" : "LONG",
//          "variableLength" : true
//        }, {
//          "length" : 64,
//          "name" : "szFull",
    public static final String COL_NAME = "szFull";
//          "type" : "TEXT",
//          "variableLength" : true
//        }, {
//          "length" : 16,
//          "name" : "szAls",
//          "type" : "TEXT",
//          "variableLength" : true
//        }, {
//          "length" : 1,
//          "name" : "fHidden",
//          "type" : "BOOLEAN",
//          "variableLength" : false
//        }, {
//          "length" : 4,
//          "name" : "Type",
//          "type" : "LONG",
//          "variableLength" : true
//        }, {
//          "length" : 8,
//          "name" : "damtStdDed",
//          "type" : "DOUBLE",
//          "variableLength" : true
//        }, {
//          "length" : 8,
//          "name" : "damtLow1",
//          "type" : "DOUBLE",
//          "variableLength" : true
//        }, {
//          "length" : 8,
//          "name" : "damtLow2",
//          "type" : "DOUBLE",
//          "variableLength" : true
//        }, {
//          "length" : 8,
//          "name" : "damtLow3",
//          "type" : "DOUBLE",
//          "variableLength" : true
//        }, {
//          "length" : 8,
//          "name" : "damtLow4",
//          "type" : "DOUBLE",
//          "variableLength" : true
//        }, {
//          "length" : 8,
//          "name" : "damtLow5",
//          "type" : "DOUBLE",
//          "variableLength" : true
//        }, {
//          "length" : 8,
//          "name" : "damtHigh1",
//          "type" : "DOUBLE",
//          "variableLength" : true
//        }, {
//          "length" : 8,
//          "name" : "damtHigh2",
//          "type" : "DOUBLE",
//          "variableLength" : true
//        }, {
//          "length" : 8,
//          "name" : "damtHigh3",
//          "type" : "DOUBLE",
//          "variableLength" : true
//        }, {
//          "length" : 8,
//          "name" : "damtHigh4",
//          "type" : "DOUBLE",
//          "variableLength" : true
//        }, {
//          "length" : 8,
//          "name" : "dRate1",
//          "type" : "DOUBLE",
//          "variableLength" : true
//        }, {
//          "length" : 8,
//          "name" : "dRate2",
//          "type" : "DOUBLE",
//          "variableLength" : true
//        }, {
//          "length" : 8,
//          "name" : "dRate3",
//          "type" : "DOUBLE",
//          "variableLength" : true
//        }, {
//          "length" : 8,
//          "name" : "dRate4",
//          "type" : "DOUBLE",
//          "variableLength" : true
//        }, {
//          "length" : 8,
//          "name" : "dRate5",
//          "type" : "DOUBLE",
//          "variableLength" : true
//        }, {
//          "length" : 8,
//          "name" : "dRateCapGains",
//          "type" : "DOUBLE",
//          "variableLength" : true
//        }, {
//          "length" : 8,
//          "name" : "damtStdEx",
//          "type" : "DOUBLE",
//          "variableLength" : true
//        }, {
//          "length" : 8,
//          "name" : "damtDedBlind",
//          "type" : "DOUBLE",
//          "variableLength" : true
//        }, {
//          "length" : 8,
//          "name" : "damtDedOver65",
//          "type" : "DOUBLE",
//          "variableLength" : true
//        }, {
//          "length" : 8,
//          "name" : "damtThreshDed",
//          "type" : "DOUBLE",
//          "variableLength" : true
//        }, {
//          "length" : 8,
//          "name" : "damtThreshExemp",
//          "type" : "DOUBLE",
//          "variableLength" : true
//        }, {
//          "length" : 8,
//          "name" : "damtMaxCapLoss",
//          "type" : "DOUBLE",
//          "variableLength" : true
//        }, {
//          "length" : 8,
//          "name" : "dRateCapGainsMT",
//          "type" : "DOUBLE",
//          "variableLength" : true
//        }, {
//          "length" : 8,
//          "name" : "dRateCapGainsLowBraket",
//          "type" : "DOUBLE",
//          "variableLength" : true
//        }, {
//          "length" : 8,
//          "name" : "dRateCapGainsLongLongT",
//          "type" : "DOUBLE",
//          "variableLength" : true
//        }, {
//          "length" : 8,
//          "name" : "damtLow6",
//          "type" : "DOUBLE",
//          "variableLength" : true
//        }, {
//          "length" : 8,
//          "name" : "damtHigh5",
//          "type" : "DOUBLE",
//          "variableLength" : true
//        }, {
//          "length" : 8,
//          "name" : "dRate6",
//          "type" : "DOUBLE",
//          "variableLength" : true
//        }, {
//          "length" : 4,
    public static final String COL_TAX_YEAR = "lTaxYear";
//          "name" : "lTaxYear",
//          "type" : "LONG",
//          "variableLength" : true
//        }, {
//          "length" : 8,
//          "name" : "dRateDividends",
//          "type" : "DOUBLE",
//          "variableLength" : true
//        } ],
//        "columnCount" : 35,
//        "rowCount" : 65,
//        "system" : false
//      }

    private static Table getTable(Database db) throws IOException {
        String tableName = TABLE_NAME_TAX_RATE;
        Table table = db.getTable(tableName);
        return table;
    }

    private static Cursor createCursor(Database db) throws IOException {
        Table table = TableTaxRatesUtils.getTable(db);
        return CursorBuilder.createCursor(table);
    }

    public static Map<Integer, TaxRate> getTaxRates(Database db) throws IOException {
        Map<Integer, TaxRate> taxRates = new LinkedHashMap<Integer, TaxRate>();

        Cursor cursor = TableTaxRatesUtils.createCursor(db);
        try {
            for (Row row : cursor) {
                addTaxRate(row, taxRates);
            }
        } finally {
            cursor = null;
        }

        return taxRates;
    }

    private static void addTaxRate(Row row, Map<Integer, TaxRate> taxRates) {
        TaxRate taxRate = deserialize(row);

        taxRates.put(taxRate.getId(), taxRate);
    }

    private static TaxRate deserialize(Row row) {
        TaxRate taxRate = new DefaultTaxRate();

        Integer id = row.getInt(COL_ID);
        taxRate.setId(id);

        Integer taxYear = row.getInt(COL_TAX_YEAR);
        taxRate.setTaxYear(taxYear);
//        taxRate.getTaxYear();

        String name = row.getString(COL_NAME);
        taxRate.setName(name);

        List<TaxBracket> taxBrackets = new ArrayList<TaxBracket>();
        int maxBracket = MAX_BRACKETS;
        for (int i = 0; i < maxBracket; i++) {
            TaxBracket taxBracket = TaxBracket.deserialize(row, i + 1);
            taxBrackets.add(taxBracket);
        }
        taxRate.setTaxBrackets(taxBrackets);
        taxBrackets = taxRate.getTaxBrackets();
//        name = taxRate.getName();

//
//        Integer billHeadId = row.getInt(COL_BILL_HEAD_ID);
//        taxRate.setBillHeadId(billHeadId);
//
//        // lHtrn
//        Integer transactionId = row.getInt(COL_TRANSACTION_ID);
//        taxRate.setTransactionId(transactionId);
//
//        // cDaysAutoEnter
//        Integer daysAutoEnter = row.getInt(COL_DAYS_AUTO_ENTER);
//        taxRate.setDaysAutoEnter(daysAutoEnter);
//
//        // bill date?
//        Date date = row.getDate(COL_DATE);
//        taxRate.setDate(date);
//
//        // frequency
//        Frequency frequency = createFrequency(row);
//        taxRate.setFrequency(frequency);

        Double value = null;

        // dRateCapGains
        // this.ltcGainsRateTextField = textField_20;
        // value = setDouble(row, "dRateCapGains", ltcGainsRateTextField);
        value = getColumnValue(row, COL_LTCGAINS_RATE);
        taxRate.setLtcGainsRate(value);

        // dRateDividends
        // this.dividendsRateTextField = textField_21;
        // value = setDouble(row, "dRateDividends", dividendsRateTextField);
        value = getColumnValue(row, COL_DIVIDENS_RATE);
        taxRate.setDividendsRate(value);

        // damtStdDed
        // this.standardDeductionTextField = textField_30;
        // value = setDouble(row, "damtStdDed", standardDeductionTextField);
        value = getColumnValue(row, COL_STANDARD_DEDUCTION);
        taxRate.setStandardDeduction(value);

        // damtDedBlind
        // this.blindTextField = textField_31;
        // value = setDouble(row, "damtDedBlind", blindTextField);
        value = getColumnValue(row, COL_BLIND);
        taxRate.setBlind(value);

        // damtStdEx
        // this.exemptionAmountTextField = textField_32;
        // value = setDouble(row, "damtStdEx", exemptionAmountTextField);
        value = getColumnValue(row, COL_EXEMPTION_AMOUNT);
        taxRate.setExemptionAmount(value);

        // damtDedOver65
        // this.over65TextField = textField_33;
        // value = setDouble(row, "damtDedOver65", over65TextField);
        value = getColumnValue(row, COL_OVER_65);
        taxRate.setOver65(value);

        // damtThreshDed
        // this.exemptionCutoffTextField = textField_34;
        // value = setDouble(row, "damtThreshExemp", exemptionCutoffTextField);
        value = getColumnValue(row, COL_EXEPTION_CUT_OFF);
        taxRate.setExemptionCutoff(value);

        // this.deductionCutoffTextField = textField_35;
        // value = setDouble(row, "damtThreshDed", deductionCutoffTextField);
        value = getColumnValue(row, COL_DEDUCTION_CUT_OFF);
        taxRate.setDeductionCutoff(value);

        // damtMaxCapLoss
        // value = setDouble(row, "damtMaxCapLoss", maximumCapitalLossTextFile);
        value = getColumnValue(row, COL_MAXIMUM_CAPITAL_LOSS);
        taxRate.setMaximumCapitalLoss(value);

        return taxRate;
    }

    private static final Double getColumnValue(Row row, String columnName) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("columnName=" + columnName);
        }
        Double value = row.getDouble(columnName);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("columnName=" + columnName + ", value=" + value);
        }
        return value;
    }

}

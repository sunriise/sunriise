/*******************************************************************************
 * Copyright (c) 2016 Hung Le
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *******************************************************************************/
package com.hungle.sunriise.dbutil;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

import com.healthmarketscience.jackcess.Cursor;
import com.healthmarketscience.jackcess.CursorBuilder;
import com.healthmarketscience.jackcess.Database;
import com.healthmarketscience.jackcess.Row;
import com.healthmarketscience.jackcess.Table;
import com.hungle.sunriise.mnyobject.Transaction;
import com.hungle.sunriise.mnyobject.TransactionSplit;
import com.hungle.sunriise.mnyobject.impl.DefaultTransactionSplit;

// TODO: Auto-generated Javadoc
/**
 * The Class TableTransactionSplitUtil.
 */
public class TableTransactionSplitUtils {

    /** The Constant TABLE_NAME_TRANSACTION_SPLIT. */
    private static final String TABLE_NAME_TRANSACTION_SPLIT = "TRN_SPLIT";

    /** The Constant INDEX_TRANSACTION_SPLIT. */
    private static final String INDEX_TRANSACTION_SPLIT = TableTransactionUtils.INDEX_TRANSACTION_SPLIT;

    /** The Constant COL_TRANSACTION_ID. */
    private static final String COL_TRANSACTION_ID = "htrn";

    /** The Constant COL_ROW_ID. */
    private static final String COL_ROW_ID = "iSplit";

    /** The Constant COL_PARENT_ID. */
    private static final String COL_PARENT_ID = "htrnParent";

    /**
     * Gets the map.
     *
     * @param db the db
     * @return the map
     * @throws IOException Signals that an I/O exception has occurred.
     */
    public static Map<Integer, TransactionSplit> getMap(Database db) throws IOException {
        return getSplits(db);
    }

    /**
     * Gets the splits.
     *
     * @param db the db
     * @return the splits
     * @throws IOException Signals that an I/O exception has occurred.
     */
    public static Map<Integer, TransactionSplit> getSplits(Database db) throws IOException {
        Map<Integer, TransactionSplit> splits = new LinkedHashMap<Integer, TransactionSplit>();

        Cursor cursor = null;
        try {
            cursor = TableTransactionSplitUtils.createCursor(db);
            for (Row row : cursor) {
                addSplit(row, splits);
            }
        } finally {
            cursor = null;
        }

        return splits;
    }

    /**
     * Gets the transaction split.
     *
     * @param cursor      the cursor
     * @param transaction the transaction
     * @return the transaction split
     * @throws IOException Signals that an I/O exception has occurred.
     */
    public static TransactionSplit getTransactionSplit(Cursor cursor, Transaction transaction) throws IOException {
        cursor.reset();
        TransactionSplit transactionSplit = null;

        Map<String, Object> rowPattern = new HashMap<String, Object>();
        rowPattern.put(COL_TRANSACTION_ID, transaction.getId());

        if (cursor.findFirstRow(rowPattern)) {
            Row row = cursor.getCurrentRow();
            transactionSplit = deserialize(row);

//            Integer htrnParent = row.getInt(COL_PARENT_ID);
//            Integer iSplit = row.getInt(COL_ROW_ID);
//
//            transactionSplit = new DefaultTransactionSplit();
            transactionSplit.setTransaction(transaction);
//            transactionSplit.setParentId(htrnParent);
//            transactionSplit.setRowId(iSplit);
        }

        return transactionSplit;
    }

    /**
     * Handle transaction split.
     *
     * @param db           the db
     * @param transactions the transactions
     * @return the map
     * @throws IOException Signals that an I/O exception has occurred.
     */
    public static Map<Integer, List<TransactionSplit>> handleTransactionSplit(Database db,
            List<Transaction> transactions) throws IOException {
        String indexName = INDEX_TRANSACTION_SPLIT;

        Cursor cursor = createCursor(db, indexName);

        Map<Integer, List<TransactionSplit>> bucket = getBuckets(transactions, cursor);

        for (Transaction transaction : transactions) {
            List<TransactionSplit> list = bucket.get(transaction.getId());
            if (list == null) {
                continue;
            }
            if (list.size() <= 0) {
                continue;
            }

            transaction.setSplits(list);
        }

        return bucket;
    }

    /**
     * Creates the transaction split cursor.
     *
     * @param db        the db
     * @param indexName the index name
     * @return the cursor
     * @throws IOException Signals that an I/O exception has occurred.
     */
    private static Cursor createCursor(Database db, String indexName) throws IOException {
        Table table = TableTransactionSplitUtils.getTable(db);
        Cursor cursor = null;
        if (StringUtils.isNotEmpty(indexName)) {
            cursor = CursorBuilder.createCursor(table.getIndex(indexName));
        } else {
            cursor = CursorBuilder.createCursor(table);
        }
        // Cursor cursor = Cursor.createCursor(splitTable);
        return cursor;
    }

    /**
     * Creates the cursor.
     *
     * @param db the db
     * @return the cursor
     * @throws IOException Signals that an I/O exception has occurred.
     */
    private static Cursor createCursor(Database db) throws IOException {
        Table table = TableTransactionSplitUtils.getTable(db);
        return CursorBuilder.createCursor(table);
    }

    /**
     * Gets the transaction split table.
     *
     * @param db the db
     * @return the transaction split table
     * @throws IOException Signals that an I/O exception has occurred.
     */
    private static Table getTable(Database db) throws IOException {
        String tableName = TABLE_NAME_TRANSACTION_SPLIT;
        Table table = db.getTable(tableName);
        return table;
    }

    /**
     * Gets the buckets.
     *
     * @param transactions the transactions
     * @param cursor the cursor
     * @return the buckets
     * @throws IOException Signals that an I/O exception has occurred.
     */
    private static Map<Integer, List<TransactionSplit>> getBuckets(List<Transaction> transactions, Cursor cursor)
            throws IOException {
        Map<Integer, List<TransactionSplit>> splits = new HashMap<Integer, List<TransactionSplit>>();
        ListIterator<Transaction> listIterator = transactions.listIterator();
        while (listIterator.hasNext()) {
            Transaction transaction = listIterator.next();
            TransactionSplit transactionSplit = null;
            transactionSplit = getTransactionSplit(cursor, transaction);

            if (transactionSplit != null) {
                List<TransactionSplit> list = splits.get(transactionSplit.getParentId());
                if (list == null) {
                    list = new ArrayList<TransactionSplit>();
                    splits.put(transactionSplit.getParentId(), list);
                }
                list.add(transactionSplit);

                listIterator.remove();
            }
        }
        return splits;
    }

    /**
     * Adds the split.
     *
     * @param row the row
     * @param splits the splits
     */
    private static void addSplit(Row row, Map<Integer, TransactionSplit> splits) {
        TransactionSplit split = deserialize(row);

        Integer id = split.getId();
        splits.put(id, split);
    }

    /**
     * Deserialize.
     *
     * @param row the row
     * @return the transaction split
     */
    private static TransactionSplit deserialize(Row row) {
        Integer id = row.getInt(COL_TRANSACTION_ID);
        Integer htrnParent = row.getInt(COL_PARENT_ID);
        Integer iSplit = row.getInt(COL_ROW_ID);

        TransactionSplit transactionSplit = new DefaultTransactionSplit();
//        transactionSplit.setTransaction(transaction);
        transactionSplit.setId(id);
        transactionSplit.setParentId(htrnParent);
        transactionSplit.setRowId(iSplit);

        return transactionSplit;
    }

}

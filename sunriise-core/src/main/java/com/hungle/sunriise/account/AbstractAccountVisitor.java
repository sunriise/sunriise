/*******************************************************************************
 * Copyright (c) 2010 Hung Le
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *******************************************************************************/
package com.hungle.sunriise.account;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;

import com.healthmarketscience.jackcess.Database;
import com.hungle.sunriise.dbutil.TableAccountUtils;
import com.hungle.sunriise.io.MnyDb;
import com.hungle.sunriise.mnyobject.Account;
import com.hungle.sunriise.mnyobject.MnyContext;
import com.hungle.sunriise.mnyobject.Transaction;
import com.hungle.sunriise.util.ComparatorUtils;
import com.hungle.sunriise.util.MnyContextUtils;

// TODO: Auto-generated Javadoc
/**
 * The Class AbstractAccountVisitor.
 */
public abstract class AbstractAccountVisitor {

    /** The current account. */
    private Account currentAccount;

    /** The current transaction. */
    private Transaction currentTransaction;

    /** The current filtered transaction. */
    private Transaction currentFilteredTransaction;

    /** The mny context. */
    private MnyContext mnyContext;

    /** The sort accounts. */
    private boolean sortAccounts = true;

    /** The accounts count. */
    private int accountsCount;

    /** The accounts index. */
    private int accountsIndex;

    private Collection<Account> accounts;

    /**
     * Gets the current account.
     *
     * @return the current account
     */
    public Account getCurrentAccount() {
        return currentAccount;
    }

    /**
     * Sets the current account.
     *
     * @param currentAccount the new current account
     */
    public void setCurrentAccount(Account currentAccount) {
        this.currentAccount = currentAccount;
    }

    /**
     * Gets the current transaction.
     *
     * @return the current transaction
     */
    public Transaction getCurrentTransaction() {
        return currentTransaction;
    }

    /**
     * Sets the current transaction.
     *
     * @param currentTransaction the new current transaction
     */
    public void setCurrentTransaction(Transaction currentTransaction) {
        this.currentTransaction = currentTransaction;
    }

    /**
     * Gets the current filtered transaction.
     *
     * @return the current filtered transaction
     */
    public Transaction getCurrentFilteredTransaction() {
        return currentFilteredTransaction;
    }

    /**
     * Sets the current filtered transaction.
     *
     * @param currentFilteredTransaction the new current filtered transaction
     */
    public void setCurrentFilteredTransaction(Transaction currentFilteredTransaction) {
        this.currentFilteredTransaction = currentFilteredTransaction;
    }

    /**
     * Gets the mny context.
     *
     * @return the mny context
     */
    public MnyContext getMnyContext() {
        return mnyContext;
    }

    /**
     * Sets the mny context.
     *
     * @param mnyContext the new mny context
     */
    public void setMnyContext(MnyContext mnyContext) {
        this.mnyContext = mnyContext;
    }

    /**
     * Start visit.
     *
     * @param dbFile   the db file
     * @param password the password
     * @throws IOException Signals that an I/O exception has occurred.
     */
    public void startVisit(File dbFile, String password) throws IOException {
        MnyDb mnyDb = new MnyDb(dbFile, password);
        this._visit(mnyDb);
    }

    /**
     * Pre visit.
     *
     * @param mnyDb the opened db
     * @throws IOException Signals that an I/O exception has occurred.
     */
    public abstract void preVisit(MnyDb mnyDb) throws IOException;

    /**
     * Visit.
     *
     * @param mnyDb the opened db
     * @throws IOException Signals that an I/O exception has occurred.
     */
    public abstract void visit(MnyDb mnyDb) throws IOException;

    public abstract void visit(Database db);

    /**
     * Post visit.
     *
     * @param mnyDb the opened db
     * @throws IOException Signals that an I/O exception has occurred.
     */
    public abstract void postVisit(MnyDb mnyDb) throws IOException;

    /**
     * Visit accounts.
     *
     * @param accounts the accounts
     * @throws IOException Signals that an I/O exception has occurred.
     */
    public abstract void visitAccounts(Map<Integer, Account> accounts) throws IOException;

    /**
     * Visit account.
     *
     * @param account the account
     * @throws IOException Signals that an I/O exception has occurred.
     */
    public abstract void visitAccount(Account account) throws IOException;

    /**
     * Visit transaction.
     *
     * @param transaction the transaction
     * @throws IOException Signals that an I/O exception has occurred.
     */
    public abstract void visitTransaction(Transaction transaction) throws IOException;

    /**
     * Visit filtered transaction.
     *
     * @param transaction the transaction
     * @throws IOException Signals that an I/O exception has occurred.
     */
    public abstract void visitFilteredTransaction(Transaction transaction) throws IOException;

    /**
     * _visit.
     *
     * @param mnyDb the opened db
     * @throws IOException Signals that an I/O exception has occurred.
     */
    public void _visit(MnyDb mnyDb) throws IOException {
        preVisit(mnyDb);
        try {
            this.mnyContext = MnyContextUtils.createMnyContext(mnyDb);

            visit(mnyDb);

            Map<Integer, Account> accountsMap = mnyContext.getAccounts();

            this.accountsCount = accountsMap.size();
            visitAccounts(accountsMap);

            Collection<Account> accounts = accountsMap.values();
            if (isSortAccounts()) {
                accounts = sortAccounts(accounts);
            }
            this.accounts = accounts;

            visitPreAccountIteration();
            this.accountsIndex = 0;
            for (Account account : accounts) {
                accountsIndex++;
                _visitAccount(account);
            }
            visitPostAccountIteration();

            // raw data
            visit(mnyDb.getDb());
        } finally {
            postVisit(mnyDb);
        }
    }

    protected void visitPreAccountIteration() throws IOException {
    }

    protected void visitPostAccountIteration() throws IOException {
    }

    /**
     * Sort accounts.
     *
     * @param accounts the accounts
     * @return the collection
     */
    private static final Collection<Account> sortAccounts(Collection<Account> accounts) {
        Comparator<Account> accountComparator = new Comparator<Account>() {

            @Override
            public int compare(Account o1, Account o2) {
                return ComparatorUtils.compareNullOrder(o1.getName(), o2.getName());
//                return o1.getName().compareTo(o2.getName());
            }
        };
        ArrayList<Account> sortedAccounts = new ArrayList<Account>(accounts);
        Collections.sort(sortedAccounts, accountComparator);
        accounts = sortedAccounts;
        return accounts;
    }

    /**
     * _visit account.
     *
     * @param account the account
     * @throws IOException Signals that an I/O exception has occurred.
     */
    private void _visitAccount(Account account) throws IOException {
        if (!acceptAccount(account)) {
            return;
        }
        this.currentAccount = account;

        TableAccountUtils.addTransactionsToAccount(account, mnyContext);
        MnyContextUtils.setCurrencyCode(account, mnyContext.getCurrencies());

        visitAccount(account);

        List<Transaction> transactions = account.getTransactions();
        if (transactions != null) {
            for (Transaction transaction : transactions) {
                _visitTransaction(transaction);
            }
        }

        List<Transaction> filteredTransactions = account.getFilteredTransactions();
        if (transactions != null) {
            for (Transaction filteredTransaction : filteredTransactions) {
                _visitFilteredTransaction(filteredTransaction);
            }
        }

    }

    /**
     * Accept account.
     *
     * @param account the account
     * @return true, if successful
     */
    protected boolean acceptAccount(Account account) {
        return true;
    }

    /**
     * _visit transaction.
     *
     * @param transaction the transaction
     * @throws IOException Signals that an I/O exception has occurred.
     */
    private void _visitTransaction(Transaction transaction) throws IOException {
        this.currentTransaction = transaction;

        visitTransaction(transaction);
    }

    /**
     * _visit filtered transaction.
     *
     * @param transaction the transaction
     * @throws IOException Signals that an I/O exception has occurred.
     */
    private void _visitFilteredTransaction(Transaction transaction) throws IOException {
        this.currentFilteredTransaction = transaction;

        visitFilteredTransaction(transaction);
    }

    /**
     * Checks if is sort accounts.
     *
     * @return true, if is sort accounts
     */
    public boolean isSortAccounts() {
        return sortAccounts;
    }

    /**
     * Sets the sort accounts.
     *
     * @param sortedAccounts the new sort accounts
     */
    public void setSortAccounts(boolean sortedAccounts) {
        this.sortAccounts = sortedAccounts;
    }

    /**
     * Gets the accounts count.
     *
     * @return the accounts count
     */
    public int getAccountsCount() {
        return accountsCount;
    }

    /**
     * Sets the accounts count.
     *
     * @param accountsCount the new accounts count
     */
    public void setAccountsCount(int accountsCount) {
        this.accountsCount = accountsCount;
    }

    /**
     * Gets the accounts index.
     *
     * @return the accounts index
     */
    public int getAccountsIndex() {
        return accountsIndex;
    }

    /**
     * Sets the accounts index.
     *
     * @param accountsIndex the new accounts index
     */
    public void setAccountsIndex(int accountsIndex) {
        this.accountsIndex = accountsIndex;
    }

    public Collection<Account> getAccounts() {
        return accounts;
    }
}

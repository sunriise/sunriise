package com.hungle.sunriise.account.tax;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.Logger;

import com.healthmarketscience.jackcess.Row;
import com.hungle.sunriise.mnyobject.impl.TaxBracket;

public class TaxRateInfo {
    private static final Logger LOGGER = org.apache.logging.log4j.LogManager.getLogger(TaxRateInfo.class);

    private static final String COL_MAXIMUM_CAPITAL_LOSS = "damtMaxCapLoss";

    private static final String COL_DEDUCTION_CUT_OFF = "damtThreshDed";

    private static final String COL_EXEPTION_CUT_OFF = "damtThreshExemp";

    private static final String COL_OVER_65 = "damtDedOver65";

    private static final String COL_EXEMPTION_AMOUNT = "damtStdEx";

    private static final String COL_BLIND = "damtDedBlind";

    private static final String COL_STANDARD_DEDUCTION = "damtStdDed";

    private static final String COL_DIVIDENS_RATE = "dRateDividends";

    private static final String COL_LTCGAINS_RATE = "dRateCapGains";

    public static final String COL_FILING_STATUS = "szFull";

    public static final String COL_BRACKET_RATE_PREFIX = "dRate";

    public static final String COL_BRACKET_HIGH_PREFIX = "damtHigh";

    public static final String COL_BRACKET_LOW_PREFIX = "damtLow";

    private List<TaxBracket> taxBrackets = new ArrayList<TaxBracket>();

    // = row.getDouble("damtLow" + index);
    // private Double bracketLow[] = new Double[6];
    // = row.getDouble("damtHigh" + index);
    // private Double bracketHigh[] = new Double[6];
    // = row.getDouble("dRate" + index);
    // private Double bracketRate[] = new Double[6];

    // dRateCapGains
    // this.ltcGainsRateTextField = textField_20;
    // setDouble(row, "dRateCapGains", ltcGainsRateTextField);
    private Double ltcGainsRate;

    // dRateDividends
    // this.dividendsRateTextField = textField_21;
    // setDouble(row, "dRateDividends", dividendsRateTextField);
    private Double dividendsRate;

    // damtStdDed
    // this.standardDeductionTextField = textField_30;
    // setDouble(row, "damtStdDed", standardDeductionTextField);
    private Double standardDeduction;

    // damtDedBlind
    // this.blindTextField = textField_31;
    // setDouble(row, "damtDedBlind", blindTextField);
    private Double blind;

    // damtStdEx
    // this.exemptionAmountTextField = textField_32;
    // setDouble(row, "damtStdEx", exemptionAmountTextField);
    private Double exemptionAmount;

    // damtDedOver65
    // this.over65TextField = textField_33;
    // setDouble(row, "damtDedOver65", over65TextField);
    private Double over65;

    // damtThreshDed
    // this.exemptionCutoffTextField = textField_34;
    // setDouble(row, "damtThreshExemp", exemptionCutoffTextField);
    private Double exemptionCutoff;

    // this.deductionCutoffTextField = textField_35;
    // setDouble(row, "damtThreshDed", deductionCutoffTextField);
    private Double deductionCutoff;

    // damtMaxCapLoss
    // setDouble(row, "damtMaxCapLoss", maximumCapitalLossTextFile);
    private Double maximumCapitalLoss;

    public TaxRateInfo(File taxRatesDir) {
        parse(taxRatesDir);
    }

    public TaxRateInfo() {
        // TODO Auto-generated constructor stub
    }

    private void parse(File taxRatesDir) {
        // TODO Auto-generated method stub

    }

    public static final TaxRateInfo toTaxRateInfo(Row row) {
        Double value = null;

        TaxRateInfo taxRateInfo = new TaxRateInfo();
        List<TaxBracket> taxBrackets = new ArrayList<TaxBracket>();
        taxRateInfo.setTaxBrackets(taxBrackets);

        for (int i = 0; i < 6; i++) {
            TaxBracket taxBracket = new TaxBracket();
            taxBrackets.add(taxBracket);

            int index = i + 1;
            // Double bracketLow = row.getDouble(COL_BRACKET_LOW_PREFIX +
            // index);
            // value = setDouble(row, COL_BRACKET_LOW_PREFIX + index,
            // bracketLowTextField[i]);
            value = getColumnValue(row, TaxRateInfo.COL_BRACKET_LOW_PREFIX + index);
            taxBracket.setLow(value);

            // Double bracketHigh = row.getDouble(COL_BRACKET_HIGH_PREFIX +
            // index);
            // value = setDouble(row, COL_BRACKET_HIGH_PREFIX + index,
            // bracketHighTextField[i]);
            value = getColumnValue(row, TaxRateInfo.COL_BRACKET_HIGH_PREFIX + index);
            taxBracket.setHigh(value);

            // Double bracketRate = row.getDouble(COL_BRACKET_RATE_PREFIX +
            // index);
            // value = setDouble(row, COL_BRACKET_RATE_PREFIX + index,
            // bracketRateTextField[i]);
            value = getColumnValue(row, TaxRateInfo.COL_BRACKET_RATE_PREFIX + index);
            taxBracket.setRate(value);

            LOGGER.info("taxBracket=" + taxBracket + ", index=" + index);
            // if (bracketLow != null) {
            // bracketLowTextField[i].setText("" + bracketLow);
            // }
            // if (bracketHigh != null) {
            // bracketHighTextField[i].setText("" + bracketHigh);
            // }
            // if (bracketRate != null) {
            // bracketRateTextField[i].setText("" + bracketRate);
            // }
        }

        // dRateCapGains
        // this.ltcGainsRateTextField = textField_20;
        // value = setDouble(row, "dRateCapGains", ltcGainsRateTextField);
        value = getColumnValue(row, COL_LTCGAINS_RATE);
        taxRateInfo.setLtcGainsRate(value);

        // dRateDividends
        // this.dividendsRateTextField = textField_21;
        // value = setDouble(row, "dRateDividends", dividendsRateTextField);
        value = getColumnValue(row, COL_DIVIDENS_RATE);
        taxRateInfo.setDividendsRate(value);

        // damtStdDed
        // this.standardDeductionTextField = textField_30;
        // value = setDouble(row, "damtStdDed", standardDeductionTextField);
        value = getColumnValue(row, COL_STANDARD_DEDUCTION);
        taxRateInfo.setStandardDeduction(value);

        // damtDedBlind
        // this.blindTextField = textField_31;
        // value = setDouble(row, "damtDedBlind", blindTextField);
        value = getColumnValue(row, COL_BLIND);
        taxRateInfo.setBlind(value);

        // damtStdEx
        // this.exemptionAmountTextField = textField_32;
        // value = setDouble(row, "damtStdEx", exemptionAmountTextField);
        value = getColumnValue(row, COL_EXEMPTION_AMOUNT);
        taxRateInfo.setExemptionAmount(value);

        // damtDedOver65
        // this.over65TextField = textField_33;
        // value = setDouble(row, "damtDedOver65", over65TextField);
        value = getColumnValue(row, COL_OVER_65);
        taxRateInfo.setOver65(value);

        // damtThreshDed
        // this.exemptionCutoffTextField = textField_34;
        // value = setDouble(row, "damtThreshExemp", exemptionCutoffTextField);
        value = getColumnValue(row, COL_EXEPTION_CUT_OFF);
        taxRateInfo.setExemptionCutoff(value);

        // this.deductionCutoffTextField = textField_35;
        // value = setDouble(row, "damtThreshDed", deductionCutoffTextField);
        value = getColumnValue(row, COL_DEDUCTION_CUT_OFF);
        taxRateInfo.setDeductionCutoff(value);

        // damtMaxCapLoss
        // value = setDouble(row, "damtMaxCapLoss", maximumCapitalLossTextFile);
        value = getColumnValue(row, COL_MAXIMUM_CAPITAL_LOSS);
        taxRateInfo.setMaximumCapitalLoss(value);

        return taxRateInfo;
    }

    public Double getLtcGainsRate() {
        return ltcGainsRate;
    }

    public void setLtcGainsRate(Double ltcGainsRate) {
        this.ltcGainsRate = ltcGainsRate;
    }

    public Double getDividendsRate() {
        return dividendsRate;
    }

    public void setDividendsRate(Double dividendsRate) {
        this.dividendsRate = dividendsRate;
    }

    public Double getStandardDeduction() {
        return standardDeduction;
    }

    public void setStandardDeduction(Double standardDeduction) {
        this.standardDeduction = standardDeduction;
    }

    public Double getBlind() {
        return blind;
    }

    public void setBlind(Double blind) {
        this.blind = blind;
    }

    public Double getExemptionAmount() {
        return exemptionAmount;
    }

    public void setExemptionAmount(Double exemptionAmount) {
        this.exemptionAmount = exemptionAmount;
    }

    public Double getOver65() {
        return over65;
    }

    public void setOver65(Double over65) {
        this.over65 = over65;
    }

    public Double getExemptionCutoff() {
        return exemptionCutoff;
    }

    public void setExemptionCutoff(Double exemptionCutoff) {
        this.exemptionCutoff = exemptionCutoff;
    }

    public Double getDeductionCutoff() {
        return deductionCutoff;
    }

    public void setDeductionCutoff(Double deductionCutoff) {
        this.deductionCutoff = deductionCutoff;
    }

    public Double getMaximumCapitalLoss() {
        return maximumCapitalLoss;
    }

    public void setMaximumCapitalLoss(Double maximumCapitalLoss) {
        this.maximumCapitalLoss = maximumCapitalLoss;
    }

    private static final Double getColumnValue(Row row, String columnName) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("columnName=" + columnName);
        }
        Double value = row.getDouble(columnName);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("columnName=" + columnName + ", value=" + value);
        }
        return value;
    }

    public List<TaxBracket> getTaxBrackets() {
        return taxBrackets;
    }

    public void setTaxBrackets(List<TaxBracket> taxBrackets) {
        this.taxBrackets = taxBrackets;
    }

    public TaxBracket getTaxBracket(int i) {
        return taxBrackets.get(i);
    }
}

package com.hungle.sunriise.account.tax;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.apache.logging.log4j.Logger;

import com.healthmarketscience.jackcess.Cursor;
import com.healthmarketscience.jackcess.CursorBuilder;
import com.healthmarketscience.jackcess.Database;
import com.healthmarketscience.jackcess.Row;
import com.healthmarketscience.jackcess.Table;
import com.hungle.sunriise.dbutil.TableTaxRatesUtils;
import com.hungle.sunriise.io.MnyDb;
import com.hungle.sunriise.io.MnyDb.OPEN_MODE;
import com.hungle.sunriise.mnyobject.impl.TaxBracket;

public class UpdateTaxRates {
    private static final Logger LOGGER = org.apache.logging.log4j.LogManager.getLogger(UpdateTaxRates.class);

    private final Integer fromTaxYear;

    private final Integer toTaxYear;

    public UpdateTaxRates(int fromTaxYear, int toTaxYear) {
        super();
        this.fromTaxYear = fromTaxYear;
        this.toTaxYear = toTaxYear;
    }

    private void update(MnyDb mnyDb, File taxRatesDir) throws IOException {
        Database db = mnyDb.getDb();
        Table table = db.getTable(TableTaxRatesUtils.TABLE_NAME_TAX_RATE);
        Cursor cursor = CursorBuilder.createCursor(table);

        int maxId = -1;
        List<Row> fromTaxYearRows = new ArrayList<Row>();
        List<Row> toTaxYearRows = new ArrayList<Row>();
        while (cursor.moveToNextRow()) {
            Row row = cursor.getCurrentRow();
            Integer taxYear = row.getInt(TableTaxRatesUtils.COL_TAX_YEAR);
            Integer id = row.getInt(TableTaxRatesUtils.COL_ID);
            maxId = Math.max(maxId, id);
            if (taxYear.intValue() == fromTaxYear.intValue()) {
                fromTaxYearRows.add(row);
            }
            if (taxYear.intValue() == toTaxYear.intValue()) {
                toTaxYearRows.add(row);
            }
        }

        if (fromTaxYearRows.size() <= 0) {
            LOGGER.warn("Cannot find any rows with taxYear=" + fromTaxYear);
            return;
        }
        if (toTaxYearRows.size() > 0) {
            LOGGER.warn("Already has rows with taxYear=" + toTaxYear);
            return;
        }

        TaxRateInfo taxRateInfo = new TaxRateInfo(taxRatesDir);
        maxId++;
        for (Row row : fromTaxYearRows) {
            row.put(TableTaxRatesUtils.COL_ID, maxId++);
            row.put(TableTaxRatesUtils.COL_TAX_YEAR, toTaxYear);

            String name = row.getString(TableTaxRatesUtils.COL_NAME);
            if (name.compareTo("TRSingle") == 0) {
                setTRSingle(row, taxRateInfo);
            } else if (name.compareTo("TRJoint") == 0) {
                setTRJoint(row, taxRateInfo);
            } else if (name.compareTo("TRSeparate") == 0) {
                setTRSeparate(row, taxRateInfo);
            } else if (name.compareTo("TRHHouse") == 0) {
                setTRHHouse(row, taxRateInfo);
            } else if (name.compareTo("TRCustom") == 0) {
                setTRCustom(row, taxRateInfo);
            } else {
                LOGGER.warn("Invalid filing status name=" + name);
            }

            // Long Term Capital Gains
            setLTCGRate(row, taxRateInfo);

            // Dividends
            setDividendsRate(row, taxRateInfo);

            // Standard Deduction
            setStandardDeduction(row, taxRateInfo);

            // Exemption Amount
            setExemptionAmount(row, taxRateInfo);

            // Exemption Cutoff
            setExemptionCutoff(row, taxRateInfo);

            // Maximum Capital Loss
            setMaximumCapitalLoss(row, taxRateInfo);

            // Blind
            setBlind(row, taxRateInfo);

            // Over 65
            setOver65(row, taxRateInfo);

            // Deduction Cutoff
            setExemptionCutoff(row, taxRateInfo);

            LOGGER.info("ADDING " + row);
            table.addRowFromMap(row);
        }
    }

    private void setTRSingle(Row row, TaxRateInfo taxRateInfo) {
        setTR(row, 1, taxRateInfo);
    }

    private void setTRJoint(Row row, TaxRateInfo taxRateInfo) {
        setTR(row, 2, taxRateInfo);
    }

    private void setTRSeparate(Row row, TaxRateInfo taxRateInfo) {
        setTR(row, 3, taxRateInfo);
    }

    private void setTRHHouse(Row row, TaxRateInfo taxRateInfo) {
        setTR(row, 4, taxRateInfo);
    }

    private void setTRCustom(Row row, TaxRateInfo taxRateInfo) {
        setTR(row, 5, taxRateInfo);
    }

    private void setTR(Row row, int index, TaxRateInfo taxRateInfo) {
        TaxBracket taxBracket = taxRateInfo.getTaxBracket(index);

        String key = null;
        Object value = null;

        key = "damtLow" + index;
        value = taxBracket.getLow();
        row.put(key, value);

        if (index < 5) {
            key = "damtHigh" + index;
            value = taxBracket.getHigh();
            row.put(key, value);
        }
        key = "dRate" + index;
        value = taxBracket.getRate();
        row.put(key, value);
    }

    private void setLTCGRate(Row row, TaxRateInfo taxRateInfo) {
        String key = null;
        Object value = null;

        key = "dRateCapGains";
        value = null;
        row.put(key, value);
    }

    private void setDividendsRate(Row row, TaxRateInfo taxRateInfo) {
        // TODO Auto-generated method stub
        String key = null;
        Object value = null;

        key = null;
        value = null;
        row.put(key, value);
    }

    private void setStandardDeduction(Row row, TaxRateInfo taxRateInfo) {
        // TODO Auto-generated method stub
        String key = null;
        Object value = null;

        key = "damtStdDed";
        value = null;
        row.put(key, value);
    }

    private void setExemptionAmount(Row row, TaxRateInfo taxRateInfo) {
        // TODO Auto-generated method stub
        String key = null;
        Object value = null;

        key = "damtStdEx";
        value = null;
        row.put(key, value);
    }

    private void setMaximumCapitalLoss(Row row, TaxRateInfo taxRateInfo) {
        // TODO Auto-generated method stub
        String key = null;
        Object value = null;

        key = null;
        value = null;
        row.put(key, value);
    }

    private void setBlind(Row row, TaxRateInfo taxRateInfo) {
        // TODO Auto-generated method stub
        String key = null;
        Object value = null;

        key = "damtDedBlind";
        value = null;
        row.put(key, value);
    }

    private void setOver65(Row row, TaxRateInfo taxRateInfo) {
        // TODO Auto-generated method stub
        String key = null;
        Object value = null;

        key = "damtDedOver65";
        value = null;
        row.put(key, value);
    }

    private void setExemptionCutoff(Row row, TaxRateInfo taxRateInfo) {
        // TODO Auto-generated method stub
        String key = null;
        Object value = null;

        key = null;
        value = null;
        row.put(key, value);
    }

    public static void main(String[] args) {
        File file = null;
        String password = null;
        int fromTaxYear = -1;
        int toTaxYear = -1;

        File taxRatesDir = null;

        if (args.length == 2) {
            file = new File(args[0]);
            password = null;
            toTaxYear = Calendar.getInstance().get(Calendar.YEAR);
            fromTaxYear = toTaxYear - 1;
            taxRatesDir = new File(args[1]);
        } else if (args.length == 3) {
            file = new File(args[0]);
            password = args[1];
            toTaxYear = Calendar.getInstance().get(Calendar.YEAR);
            fromTaxYear = toTaxYear - 1;
            taxRatesDir = new File(args[2]);
        } else if (args.length == 4) {
            file = new File(args[0]);
            password = null;
            fromTaxYear = Integer.valueOf(args[1]);
            toTaxYear = Integer.valueOf(args[2]);
            taxRatesDir = new File(args[3]);
        } else if (args.length == 5) {
            file = new File(args[0]);
            password = args[1];
            fromTaxYear = Integer.valueOf(args[2]);
            toTaxYear = Integer.valueOf(args[3]);
            taxRatesDir = new File(args[4]);
        } else {
            Class<UpdateTaxRates> clz = UpdateTaxRates.class;
            System.out.println(
                    "Usage: java " + clz.getName() + " file.mny [password] fromTaxYear toTaxYear taxRatesDirectory");
            System.exit(1);
        }

        LOGGER.info("file=" + file);
        LOGGER.info("fromTaxYear=" + fromTaxYear);
        LOGGER.info("toTaxYear=" + toTaxYear);
        LOGGER.info("taxRatesDir=" + taxRatesDir);

        MnyDb mnyDb = null;
        try {
            mnyDb = new MnyDb(file, password, OPEN_MODE.READ_WRITE);
            UpdateTaxRates updateTaxRates = new UpdateTaxRates(fromTaxYear, toTaxYear);
            updateTaxRates.update(mnyDb, taxRatesDir);
        } catch (IOException e) {
            LOGGER.error(e, e);
        } finally {
            if (mnyDb != null) {
                try {
                    mnyDb.close();
                } catch (IOException e) {
                    LOGGER.warn(e);
                } finally {
                    mnyDb = null;
                }
            }
        }
    }
}

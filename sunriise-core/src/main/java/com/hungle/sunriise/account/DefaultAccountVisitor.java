/*******************************************************************************
 * Copyright (c) 2010 Hung Le
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *******************************************************************************/
package com.hungle.sunriise.account;

import java.io.IOException;
import java.util.Map;

import com.healthmarketscience.jackcess.Database;
import com.hungle.sunriise.io.MnyDb;
import com.hungle.sunriise.mnyobject.Account;
import com.hungle.sunriise.mnyobject.Transaction;

// TODO: Auto-generated Javadoc
/**
 * The Class DefaultAccountVisitor.
 */
public class DefaultAccountVisitor extends AbstractAccountVisitor {
    @Override
    public void preVisit(MnyDb mnyDb) throws IOException {
        // TODO Auto-generated method stub

    }

    @Override
    public void visit(MnyDb mnyDb) throws IOException {
        // TODO Auto-generated method stub

    }

    @Override
    public void visit(Database db) {
        // TODO Auto-generated method stub

    }

    /*
     * (non-Javadoc)
     * 
     * @see com.hungle.sunriise.account.AbstractAccountVisitor#visitAccount(com.
     * hungle.sunriise.mnyobject.Account)
     */
    @Override
    public void visitAccount(Account account) throws IOException {
        // TODO Auto-generated method stub

    }

    /*
     * (non-Javadoc)
     * 
     * @see com.hungle.sunriise.account.AbstractAccountVisitor#visitTransaction(com.
     * hungle.sunriise.mnyobject.Transaction)
     */
    @Override
    public void visitTransaction(Transaction transaction) throws IOException {
        // TODO Auto-generated method stub

    }

    @Override
    public void postVisit(MnyDb mnyDb) throws IOException {
        // TODO Auto-generated method stub

    }

    @Override
    public void visitFilteredTransaction(Transaction transaction) throws IOException {
        // TODO Auto-generated method stub

    }

    /*
     * (non-Javadoc)
     * 
     * @see com.hungle.sunriise.account.AbstractAccountVisitor#visitAccounts(java.
     * util.Map)
     */
    @Override
    public void visitAccounts(Map<Integer, Account> accounts) throws IOException {
        // TODO Auto-generated method stub

    }

}

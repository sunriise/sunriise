package com.hungle.sunriise.diskusage;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import com.healthmarketscience.jackcess.Table;
import com.hungle.sunriise.util.DiskUsageCollector;

public class DefaultDiskUsageCollector implements DiskUsageCollector {

    class TableStat {
        public TableStat(Table table, int tableByteCount) {
            super();
            this.table = table;
            this.tableByteCount = tableByteCount;
        }

        private final Table table;
        private final int tableByteCount;

        public Table getTable() {
            return table;
        }

        public int getTableByteCount() {
            return tableByteCount;
        }

        public String toCsvString() {
            String name = table.getName();
            int rowCount = table.getRowCount();

            String str = name + ", " + rowCount + ", " + tableByteCount + ", "
                    + GetDiskUsageCmd.humanReadableByteCount(tableByteCount);

            return str;
        }
    }

    private File dbFile;

    private List<TableStat> tableStats = new ArrayList<>();

    private int tableCount;
    private long runningBytes;

    @Override
    public void collectFileStat(File dbFile) {
        this.dbFile = dbFile;
//		long fileLength = dbFile.length();
//
//		System.out.println(dbFile + ", " + fileLength + ", " + GetDiskUsageCmd.humanReadableByteCount(fileLength));
    }

    @Override
    public void collectTableStat(Table table, int tableByteCount) {
//		String name = table.getName();
//		int rowCount = table.getRowCount();

        TableStat tableStat = new TableStat(table, tableByteCount);
//		System.out.println(name + ", " + rowCount + ", " + tableByteCount + ", "
//				+ GetDiskUsageCmd.humanReadableByteCount(tableByteCount));
        tableStats.add(tableStat);
    }

    @Override
    public void collectSummary(int tableCount, long runningBytes) {
        this.tableCount = tableCount;
        this.runningBytes = runningBytes;
//		System.out.println("Total: " + tableCount + ", " + GetDiskUsageCmd.humanReadableByteCount(runningBytes));
    }

    public File getDbFile() {
        return dbFile;
    }

    public void setDbFile(File dbFile) {
        this.dbFile = dbFile;
    }

    public List<TableStat> getTableStats() {
        return tableStats;
    }

    public void setTableStats(List<TableStat> tableStats) {
        this.tableStats = tableStats;
    }

    public int getTableCount() {
        return tableCount;
    }

    public void setTableCount(int tableCount) {
        this.tableCount = tableCount;
    }

    public long getRunningBytes() {
        return runningBytes;
    }

    public void setRunningBytes(long runningBytes) {
        this.runningBytes = runningBytes;
    }
}

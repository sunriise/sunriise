package com.hungle.sunriise.diskusage;

import java.io.BufferedWriter;
import java.io.Closeable;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Set;

import org.apache.logging.log4j.Logger;

import com.healthmarketscience.jackcess.Table;
import com.hungle.sunriise.diskusage.GetDiskUsageCmd.GetDiskUsageCollector;

public class FileGetDiskUsageCollector implements GetDiskUsageCollector, Closeable {
    private static final Logger LOGGER = org.apache.logging.log4j.LogManager.getLogger(FileGetDiskUsageCollector.class);

    private PrintWriter writer;

    public FileGetDiskUsageCollector(File outFile) throws IOException {
        super();
        this.writer = new PrintWriter(new BufferedWriter(new FileWriter(outFile)));
    }

    /**
     * Prints the table header.
     */
    public void printTableHeader() {
        if (writer == null) {
            return;
        }

        String sep = ",";

        String[] headers = { "name", "rows", "indexes", "bytes" };
        int count = 0;
        try {
            for (String header : headers) {
                if (count > 0) {
                    writer.print(sep);
                }
                writer.print(header);
                count++;
            }
        } finally {
            writer.println();
        }
    }

    /**
     * Prints the table.
     *
     * @param table the table
     */
    @Override
    public void collectTable(Table table) {
        if (writer == null) {
            return;
        }

        String sep = ",";

        try {
            writer.print(table.getName());

            writer.print(sep);
            writer.print(table.getRowCount());

            writer.print(sep);
            writer.print(table.getIndexes().size());

            writer.print(sep);
            writer.print(GetDiskUsageCmd.getBytesCount(table));
        } catch (IOException e) {
            LOGGER.warn(e.getMessage());
        } finally {
            writer.println();
        }

    }

    /**
     * Gets the bytes.
     *
     * @param table the table
     * @return the bytes
     */
    private static String getBytes(Table table) {
        String str = null;
        // str = humanReadableByteCount(tableImpl.getApproximateOwnedPageCount()
        // * tableImpl.getFormat().PAGE_SIZE);
        int bytes = CalculateDiskUsage.calculateTableByteCount(table);
        str = "" + bytes;

        return str;
    }

    @Override
    public void close() throws IOException {
        if (writer != null) {
            writer.close();
            writer = null;
        }
    }

    @Override
    public void collectBytesCount(File file, long bytesCount) {
        LOGGER.info(file.getName() + ", " + bytesCount + ", " + GetDiskUsageCmd.humanReadableByteCount(bytesCount)
                + ", " + GetDiskUsageCmd.humanReadableByteCount(file.length()));
    }

    @Override
    public void collectSystemTableNames(Set<String> tableNames) {
        LOGGER.info("systemTableNames, count=" + tableNames.size());
    }

    @Override
    public void collectTableNames(Set<String> tableNames) {
        LOGGER.info("tableNames, count=" + tableNames.size());
    }

}

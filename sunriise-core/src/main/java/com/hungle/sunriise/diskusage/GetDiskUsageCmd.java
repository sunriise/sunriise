/*******************************************************************************
 * Copyright (c) 2016 Hung Le
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *******************************************************************************/
package com.hungle.sunriise.diskusage;

import java.io.Closeable;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.apache.logging.log4j.Logger;

import com.healthmarketscience.jackcess.Column;
import com.healthmarketscience.jackcess.Cursor;
import com.healthmarketscience.jackcess.CursorBuilder;
import com.healthmarketscience.jackcess.DataType;
import com.healthmarketscience.jackcess.Database;
import com.healthmarketscience.jackcess.Table;
import com.hungle.sunriise.io.MnyDb;

// TODO: Auto-generated Javadoc
/**
 * The Class GetDiskUsageCmd.
 */
public class GetDiskUsageCmd implements Closeable {

    /** The Constant log. */
    private static final Logger LOGGER = org.apache.logging.log4j.LogManager.getLogger(GetDiskUsageCmd.class);

    /**
     * The Interface GetDiskUsageCollector.
     */
    public interface GetDiskUsageCollector {

//		void printTableHeader();

        /**
 * Collect table.
 *
 * @param table the table
 */
void collectTable(Table table);

        /**
         * Collect bytes count.
         *
         * @param inFile the in file
         * @param bytesCount the bytes count
         */
        void collectBytesCount(File inFile, long bytesCount);

        /**
         * Collect system table names.
         *
         * @param tableNames the table names
         */
        void collectSystemTableNames(Set<String> tableNames);

        /**
         * Collect table names.
         *
         * @param tableNames the table names
         */
        void collectTableNames(Set<String> tableNames);

    };

    /** The in file. */
    private File inFile;

    /** The password. */
    private String password;

    /** The opened db. */
    private MnyDb mnyDb;

    /**
     * Instantiates a new gets the disk usage cmd.
     *
     * @param inFile   the in file
     * @param password the password
     */
    public GetDiskUsageCmd(File inFile, String password) {
        this.inFile = inFile;
        this.password = password;
    }

    /**
     * Gets the all tables bytes count.
     *
     * @param db the db
     * @return the all tables bytes count
     * @throws IOException Signals that an I/O exception has occurred.
     */
    public static long getAllTablesBytesCount(Database db) throws IOException {
        long bytesCount = 0;
        bytesCount += getSystemTableBytesCount(db);
        bytesCount += getTableBytesCount(db);
        return bytesCount;
    }

    /**
     * Gets the bytes count.
     *
     * @param table the table
     * @return the bytes count
     * @throws IOException Signals that an I/O exception has occurred.
     */
    public static final long getBytesCount(Table table) throws IOException {
        long bytes = getTableDiskUsage(table);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("table=" + table.getName() + ", bytes=" + humanReadableByteCount(bytes));
        }

        bytes += getTableOtherDiskUsage(table);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("table=" + table.getName() + ", otherBytes=" + bytes);
        }

        return bytes;
    }

    /**
     * Gets the table disk usage.
     *
     * @param table the table
     * @return the table disk usage
     */
    private static final long getTableDiskUsage(Table table) {
//		int pageSize = JackcessImplUtils.getFormat(table).PAGE_SIZE;
//		int pageCount = 0;
        long bytes = 0;
//
//		pageCount = JackcessImplUtils.getApproximateOwnedPageCount(table);
//		// table data
//		bytes += pageCount * pageSize;

        bytes = CalculateDiskUsage.calculateTableByteCount(table);
        return bytes;
    }

    /**
     * Gets the table other disk usage.
     *
     * @param table the table
     * @return the table other disk usage
     * @throws IOException Signals that an I/O exception has occurred.
     */
    private static final int getTableOtherDiskUsage(Table table) throws IOException {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("> getOtherPageCount, table=" + table.getName());
        }

        int bytesCount = 0;
        List<Column> matchingColumns = GetDiskUsageCmd.getOLEMEMOColumns(table);

        Cursor cursor = CursorBuilder.createCursor(table);
        try {
            while (cursor.moveToNextRow()) {
                for (Column column : matchingColumns) {
                    Object row = cursor.getCurrentRowValue(column);
                    if (row == null) {
                        continue;
                    }
                    if (row instanceof String) {
                        bytesCount += ((String) row).length();
                    } else if (row instanceof byte[]) {
                        bytesCount += ((byte[]) row).length;
                    }
                }
            }
        } catch (IOException e) {
            LOGGER.warn(e);
        }

        return bytesCount;
    }

    /**
     * Gets the OLEMEMO columns.
     *
     * @param table the table
     * @return the OLEMEMO columns
     */
    private static final List<Column> getOLEMEMOColumns(Table table) {
        List<Column> matchingColumns = new ArrayList<Column>();
        List<? extends Column> columns = table.getColumns();
        for (Column column : columns) {
            DataType type = column.getType();
            if ((type == DataType.OLE) || (type == DataType.MEMO)) {
                matchingColumns.add(column);
                if (LOGGER.isDebugEnabled()) {
                    LOGGER.debug("  found OLE/MEMO column=" + column.getName());
                }
            }
        }
        if (matchingColumns.size() <= 0) {
            if (LOGGER.isDebugEnabled()) {
                LOGGER.debug("< getOtherPageCount, has no OLE/MEMO column");
            }
            return matchingColumns;
        }

        return matchingColumns;
    }

    /**
     * Human readable byte count.
     *
     * @param bytes the bytes
     * @param si    the si
     * @return the string
     */
    private static String humanReadableByteCount(long bytes, boolean si) {
        int unit = si ? 1000 : 1024;
        if (bytes < unit)
            return bytes + " B";
        int exp = (int) (Math.log(bytes) / Math.log(unit));
        String pre = (si ? "kMGTPE" : "KMGTPE").charAt(exp - 1) + (si ? "" : "i");
        return String.format("%.1f %sB", bytes / Math.pow(unit, exp), pre);
    }

    /**
     * Human readable byte count.
     *
     * @param bytes the bytes
     * @return the string
     */
    public static String humanReadableByteCount(long bytes) {
        return humanReadableByteCount(bytes, true);
    }

    /**
     * Gets the table bytes count.
     *
     * @param db the db
     * @return the table bytes count
     * @throws IOException Signals that an I/O exception has occurred.
     */
    private static final long getTableBytesCount(Database db) throws IOException {
        int calculatedBytes = 0;

        for (String tableName : db.getTableNames()) {
            Table table = db.getTable(tableName);
            long bytes = getBytesCount(table);
            calculatedBytes += bytes;
        }
        return calculatedBytes;
    }

    /**
     * Gets the system table bytes count.
     *
     * @param db the db
     * @return the system table bytes count
     * @throws IOException Signals that an I/O exception has occurred.
     */
    private static final long getSystemTableBytesCount(Database db) throws IOException {
        long calculatedBytes = 0;

        for (String tableName : db.getSystemTableNames()) {
            Table table = db.getSystemTable(tableName);
            long bytes = getBytesCount(table);
            calculatedBytes += bytes;
        }
        return calculatedBytes;
    }

    /**
     * Calculate.
     *
     * @param collector the collector
     * @return the long
     */
    public long calculate(GetDiskUsageCollector collector) {
        long bytesCount = 0L;
        try {
            this.mnyDb = new MnyDb(inFile, password);

            Database db = mnyDb.getDb();

            bytesCount = calculate(inFile, db, collector);
        } catch (IOException e) {
            LOGGER.error(e, e);
        }

        return bytesCount;
    }

    /**
     * Calculate.
     *
     * @param file the file
     * @param db the db
     * @param collector the collector
     * @return the long
     * @throws IOException Signals that an I/O exception has occurred.
     */
    public static final long calculate(File file, Database db, GetDiskUsageCollector collector) throws IOException {
        long bytesCount = GetDiskUsageCmd.getAllTablesBytesCount(db);

        collector.collectBytesCount(file, bytesCount);

//			collector.printTableHeader();

        Set<String> tableNames = null;

        tableNames = db.getSystemTableNames();
        collector.collectSystemTableNames(tableNames);
        for (String tableName : tableNames) {
            Table table = db.getSystemTable(tableName);
            collector.collectTable(table);
        }

        tableNames = db.getTableNames();
        collector.collectTableNames(tableNames);
        for (String tableName : tableNames) {
            Table table = db.getTable(tableName);
            collector.collectTable(table);
        }
        return bytesCount;
    }

    /**
     * Close.
     *
     * @throws IOException Signals that an I/O exception has occurred.
     */
    /*
     * (non-Javadoc)
     * 
     * @see java.io.Closeable#close()
     */
    @Override
    public void close() throws IOException {
        if (mnyDb != null) {
            mnyDb.close();
            mnyDb = null;
        }
    }

    /**
     * The main method.
     *
     * @param args the arguments
     */
    public static void main(String[] args) {

        String inFileName = null;
        String password = null;
        String outFileName = null;

        if (args.length == 1) {
            inFileName = args[0];
            password = null;
            outFileName = null;
        } else if (args.length == 2) {
            inFileName = args[0];
            password = null;
            outFileName = args[1];
        } else if (args.length == 3) {
            inFileName = args[0];
            password = args[1];
            outFileName = args[2];
        } else {
            Class<GetDiskUsageCmd> clz = GetDiskUsageCmd.class;
            System.out.println("Usage: java " + clz.getName() + " file.mny [password] out.cvs");
            System.exit(1);
        }

        if (outFileName != null) {
            if (outFileName.compareTo("-") == 0) {
                outFileName = null;
            }
        }

        LOGGER.info("inFileName=" + inFileName);
        LOGGER.info("outFileName=" + outFileName);

        File inFile = new File(inFileName);
        File outFile = null;

        if (outFileName != null) {
            outFile = new File(outFileName);
        }

        try (FileGetDiskUsageCollector getDiskUsageCollector = new FileGetDiskUsageCollector(outFile)) {
            try (GetDiskUsageCmd cmd = new GetDiskUsageCmd(inFile, password)) {
                long bytes = cmd.calculate(getDiskUsageCollector);
                LOGGER.info("bytes=" + bytes);
            }
        } catch (IOException e) {
            LOGGER.error(e.getMessage(), e);
        }
    }
}

/*******************************************************************************
 * Copyright (c) 2010 Hung Le
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *******************************************************************************/
package com.hungle.sunriise.diskusage;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.logging.log4j.Logger;

import com.healthmarketscience.jackcess.Column;
import com.healthmarketscience.jackcess.Cursor;
import com.healthmarketscience.jackcess.CursorBuilder;
import com.healthmarketscience.jackcess.DataType;
import com.healthmarketscience.jackcess.Database;
import com.healthmarketscience.jackcess.Table;
import com.healthmarketscience.jackcess.impl.ColumnImpl;
import com.healthmarketscience.jackcess.impl.JetFormat;
import com.healthmarketscience.jackcess.impl.PageChannel;
import com.hungle.sunriise.io.MnyDb;
import com.hungle.sunriise.util.DiskUsageCollector;
import com.hungle.sunriise.util.JackcessImplUtils;

// TODO: Auto-generated Javadoc
/**
 * The Class CalculateDiskUsage.
 */
public class CalculateDiskUsage {

    /** The Constant log. */
    private static final Logger LOGGER = org.apache.logging.log4j.LogManager.getLogger(CalculateDiskUsage.class);

    /**
     * Instantiates a new calculate disk usage.
     */
    public CalculateDiskUsage() {
        super();
    }

    /**
     * Calculate.
     *
     * @param file     the in file
     * @param password the password
     * @throws IOException Signals that an I/O exception has occurred.
     */
    void calculate(File file, String password, DiskUsageCollector diskUsageCollector) throws IOException {
        if (!file.exists()) {
            throw new IOException("File=" + file.getAbsoluteFile().getAbsolutePath() + " does not exist.");
        }

        MnyDb mnyDb = null;
        try {
            mnyDb = new MnyDb(file, password);
            calculate(mnyDb, diskUsageCollector);
        } catch (IllegalStateException e) {
            // java.lang.IllegalStateException: Incorrect password provided
            throw new IOException(e);
        } finally {
            if (mnyDb != null) {
                try {
                    mnyDb.close();
                } finally {
                    mnyDb = null;
                }
            }
        }
    }

    /**
     * Calculate.
     *
     * @param mnyDb              the opened db
     * @param diskUsageCollector
     * @throws IOException Signals that an I/O exception has occurred.
     */
    private void calculate(MnyDb mnyDb, DiskUsageCollector diskUsageCollector) throws IOException {
        File dbFile = mnyDb.getDbFile();

        diskUsageCollector.collectFileStat(dbFile);

        Database db = mnyDb.getDb();
        PageChannel pageChannel = JackcessImplUtils.getPageChannel(db);
        JetFormat format = pageChannel.getFormat();
//		int pageSize = format.PAGE_SIZE;

        long runningBytes = 0L;
        int tableCount = 0;

        List<Set<String>> tables = new ArrayList<>();
        tables.add(db.getTableNames());
        tables.add(db.getSystemTableNames());

        for (Set<String> tableNames : tables) {
//    		Set<String> tableNames = null;
//    		tableNames = db.getTableNames();
            for (String tableName : tableNames) {
                Table table = db.getTable(tableName);
                if (table == null) {
                    table = db.getSystemTable(tableName);
                }
                if (table != null) {
                    int tableByteCount = CalculateDiskUsage.calculateTableByteCount(table);

                    diskUsageCollector.collectTableStat(table, tableByteCount);

                    runningBytes += tableByteCount;
                    tableCount++;
                } else {
                    LOGGER.warn("Cannot find table=" + tableName);
                }
            }
        }

        diskUsageCollector.collectSummary(tableCount, runningBytes);
    }

    public static final int calculateTableByteCount(Table table) {
        int pageSize = JackcessImplUtils.getFormat(table).PAGE_SIZE;
        int pageCount = JackcessImplUtils.getApproximateOwnedPageCount(table);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("  pageCount=" + pageCount);
        }
        int otherPageCount = CalculateDiskUsage.getOtherPageCount(table);
        int tableByteCount = (pageCount + otherPageCount) * pageSize;
        return tableByteCount;
    }

    /**
     * Gets the other page count.
     *
     * @param table the table
     * @return the other page count
     */
    private static final int getOtherPageCount(Table table) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("> getOtherPageCount, table=" + table.getName());
        }

        List<Column> matchingColumns = new ArrayList<Column>();
        List<ColumnImpl> columns = JackcessImplUtils.getColumns(table);
        for (Column column : columns) {
            DataType type = column.getType();
            if ((type == DataType.OLE) || (type == DataType.MEMO)) {
                matchingColumns.add(column);
                if (LOGGER.isDebugEnabled()) {
                    LOGGER.debug("  found OLE/MEMO column=" + column.getName());
                }
            }
        }
        if (matchingColumns.size() <= 0) {
            if (LOGGER.isDebugEnabled()) {
                LOGGER.debug("< getOtherPageCount, has no OLE/MEMO column");
            }
            return 0;
        }

        Set<Integer> pageNums = new HashSet<Integer>();
        try {
            Cursor cursor = CursorBuilder.createCursor(table);
            while (cursor.moveToNextRow()) {
                for (Column column : matchingColumns) {
                    // this is a hack. Not in upstream yet.
                    // cursor.getCurrentRowValue(column, pageNums);
                    column.getClass();
                }
            }
        } catch (IOException e) {
            LOGGER.warn(e);
        }

        int count = pageNums.size();

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("< getOtherPageCount, count=" + count);
        }

        return count;
    }

    /**
     * The main method.
     *
     * @param args the arguments
     */
    public static void main(String[] args) {
        String inFileName = null;
        String password = null;

        if (args.length == 1) {
            inFileName = args[0];
            password = null;
        } else if (args.length == 2) {
            inFileName = args[0];
            password = args[1];
        } else {
            Class<CalculateDiskUsage> clz = CalculateDiskUsage.class;
            System.out.println("Usage: " + clz.getName() + " sample.mny [password]");
            System.exit(1);
        }

        File inFile = new File(inFileName);

        LOGGER.info("inFile=" + inFile);

        try {
            DiskUsageCollector diskUsageCollector = new DefaultDiskUsageCollector();
            CalculateDiskUsage cmd = new CalculateDiskUsage();
            cmd.calculate(inFile, password, diskUsageCollector);
        } catch (IOException e) {
            LOGGER.error(e);
        } finally {
            LOGGER.info("< DONE");
        }
    }
}

/*******************************************************************************
 * Copyright (c) 2010 Hung Le
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *******************************************************************************/
package com.hungle.sunriise.io;

import java.io.Closeable;
import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.TimeZone;

import org.apache.logging.log4j.Logger;

import com.healthmarketscience.jackcess.Database;
import com.healthmarketscience.jackcess.DatabaseBuilder;
import com.healthmarketscience.jackcess.DateTimeType;
import com.healthmarketscience.jackcess.impl.CodecProvider;
import com.hungle.sunriise.backup.BackupFileUtils;

// TODO: Auto-generated Javadoc
public class MnyDb implements Closeable {

    /** The Constant log. */
    private static final Logger LOGGER = org.apache.logging.log4j.LogManager.getLogger(MnyDb.class);

    /** The db file. */
    private File dbFile;

    /** The db lock file. */
    private File dbLockFile;

    /** The db. */
    private Database db;

    /** The password. */
    private String password;

    /** The memory mapped file channel. */
    private ROMemoryMappedFileChannel memoryMappedFileChannel;

    /**
     * The Enum OPEN_MODE.
     */
    public enum OPEN_MODE {

        /** The read only. */
        READ_ONLY,
        /** The read write. */
        READ_WRITE,
    }

    /**
     * Instantiates a new mny db.
     *
     * @param fileName      the file name
     * @param passwordChars the password chars
     * @param readOnly      the read only
     * @param encrypted     the encrypted
     * @throws IOException Signals that an I/O exception has occurred.
     */
    public MnyDb(String fileName, char[] passwordChars, boolean readOnly, boolean encrypted) throws IOException {
        openDb(fileName, passwordChars, readOnly, encrypted);
    }

    /**
     * Instantiates a new mny db.
     *
     * @param fileName the file name
     * @throws IOException Signals that an I/O exception has occurred.
     */
    public MnyDb(String fileName) throws IOException {
        this(new File(fileName), null);
    }

    /**
     * Instantiates a new mny db.
     *
     * @param file     the file
     * @param password the password
     * @throws IOException Signals that an I/O exception has occurred.
     */
    public MnyDb(File file, String password) throws IOException {
        openDb(file, password, OPEN_MODE.READ_ONLY);
    }

    public MnyDb(File file, String password, TimeZone timeZone) throws IOException {
        boolean readOnly = true;
        boolean encrypted = true;
        openDb(file, password, readOnly, encrypted, timeZone);
    }

    /**
     * Instantiates a new mny db.
     *
     * @param file     the file
     * @param password the password
     * @param openMode the open mode
     * @throws IOException Signals that an I/O exception has occurred.
     */
    public MnyDb(File file, String password, OPEN_MODE openMode) throws IOException {
        openDb(file, password, openMode);
    }

    /**
     * Instantiates a new mny db.
     */
    public MnyDb() {
        super();
    }

    /**
     * Gets the memory mapped file channel.
     *
     * @return the memory mapped file channel
     */
    public JackcessFileChannelAdapter getMemoryMappedFileChannel() {
        return memoryMappedFileChannel;
    }

    /**
     * Sets the memory mapped file channel.
     *
     * @param memoryMappedFileChannel the new memory mapped file channel
     */
    public void setMemoryMappedFileChannel(ROMemoryMappedFileChannel memoryMappedFileChannel) {
        this.memoryMappedFileChannel = memoryMappedFileChannel;
    }

    /**
     * Gets the db file.
     *
     * @return the db file
     */
    public File getDbFile() {
        return dbFile;
    }

    /**
     * Sets the db file.
     *
     * @param dbFile the new db file
     */
    public void setDbFile(File dbFile) {
        this.dbFile = dbFile;
    }

    /**
     * Gets the db lock file.
     *
     * @return the db lock file
     */
    public File getDbLockFile() {
        return dbLockFile;
    }

    /**
     * Sets the db lock file.
     *
     * @param dbLockFile the new db lock file
     */
    public void setDbLockFile(File dbLockFile) {
        this.dbLockFile = dbLockFile;
    }

    /**
     * Gets the db.
     *
     * @return the db
     */
    public Database getDb() {
        return db;
    }

    /**
     * Sets the db.
     *
     * @param db the new db
     */
    public void setDb(Database db) {
        this.db = db;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.io.Closeable#close()
     */
    @Override
    public void close() throws IOException {
        if (db != null) {
            LOGGER.info("Closing dbFile=" + dbFile);
            try {
                db.close();

                // since we specify the channel ourselves. We will need to close
                // it ourselves.
                if (memoryMappedFileChannel != null) {
                    try {
                        LOGGER.info(
                                "Closing memoryMappedFileChannel for dbFile=" + memoryMappedFileChannel.getDbFile());
                        memoryMappedFileChannel.close();
                    } catch (IOException e) {
                        LOGGER.warn(e);
                    } finally {
                        memoryMappedFileChannel = null;
                    }
                }
            } finally {
                db = null;
                if (dbLockFile != null) {
                    if (!dbLockFile.delete()) {
                        LOGGER.warn("Could NOT delete db lock file=" + dbLockFile);
                    } else {
                        LOGGER.info("Deleted db lock file=" + dbLockFile);
                        dbLockFile = null;
                    }
                }
            }
        }
    }

    /**
     * Gets the password.
     *
     * @return the password
     */
    public String getPassword() {
        return password;
    }

    /**
     * Sets the password.
     *
     * @param password the new password
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * Checks if is memory mapped.
     *
     * @return true, if is memory mapped
     */
    public boolean isMemoryMapped() {
        return getMemoryMappedFileChannel() != null;
    }

    /**
     * Lock db.
     *
     * @param dbFile the db file
     * @return the file
     * @throws IOException Signals that an I/O exception has occurred.
     */
    private static File lockDb(File dbFile) throws IOException {
        File parentDir = dbFile.getAbsoluteFile().getParentFile().getAbsoluteFile();
        String name = dbFile.getName();
        int i = name.lastIndexOf('.');
        if (i <= 0) {
            LOGGER.warn("Cannot lock dbFile=" + name + ". Cannot find suffix");
            return null;
        }
        name = name.substring(0, i);
        File lockFile = new File(parentDir, name + ".lrd");
        if (lockFile.exists()) {
            LOGGER.warn("Cannot lock dbFile=" + name + ". Lock file exists, lockFile=" + lockFile.getAbsolutePath());
            return null;
        }

        if (lockFile.createNewFile()) {
            lockFile.deleteOnExit();
            return lockFile;
        } else {
            return null;
        }
    }

    /**
     * Open db.
     *
     * @param dbFile   the db file
     * @param password the password
     * @param readOnly the read only
     * @return the opened db
     * @throws IOException Signals that an I/O exception has occurred.
     */
    private void openDb(File dbFile, String password, boolean readOnly) throws IOException {
        openDb(dbFile, password, readOnly, true);
    }

    /**
     * Open db.
     *
     * @param inFile   the in file
     * @param password the password
     * @param openMode the open mode
     * @return the opened db
     * @throws IOException Signals that an I/O exception has occurred.
     */
    private void openDb(File inFile, String password, MnyDb.OPEN_MODE openMode) throws IOException {
        boolean readOnly = false;
        switch (openMode) {
        case READ_ONLY:
            readOnly = true;
            break;
        case READ_WRITE:
            readOnly = false;
            break;
        }

        openDb(inFile, password, readOnly);
    }

    /**
     * Open db.
     *
     * @param dbFileName    the db file name
     * @param passwordChars the password chars
     * @param readOnly      the read only
     * @param encrypted     the encrypted
     * @return the opened db
     * @throws IOException Signals that an I/O exception has occurred.
     */
    private void openDb(String dbFileName, char[] passwordChars, boolean readOnly, boolean encrypted)
            throws IOException {
        String password = null;
        if ((passwordChars != null) && (passwordChars.length > 0)) {
            password = new String(passwordChars);
        }
        openDb(dbFileName, password, readOnly, encrypted);
    }

    /**
     * Open db.
     *
     * @param dbFileName the db file name
     * @param password   the password
     * @param readOnly   the read only
     * @param encrypted  the encrypted
     * @return the opened db
     * @throws IOException Signals that an I/O exception has occurred.
     */
    private void openDb(String dbFileName, String password, boolean readOnly, boolean encrypted) throws IOException {
        File dbFile = new File(dbFileName);
        openDb(dbFile, password, readOnly, encrypted);
    }

    private void openDb(File dbFile, String password, boolean readOnly, boolean encrypted) throws IOException {
        TimeZone tz = null;
        openDb(dbFile, password, readOnly, encrypted, tz);
    }

    /**
     * Open db.
     *
     * @param dbFile    the db file
     * @param password  the password
     * @param readOnly  the read only
     * @param encrypted the encrypted
     * @throws IOException Signals that an I/O exception has occurred.
     */
    private void openDb(File dbFile, String password, boolean readOnly, boolean encrypted, TimeZone timeZone)
            throws IOException {
        LOGGER.info(
                "# Opening dbFile=" + dbFile.getAbsolutePath() + ", readOnly=" + readOnly + ", encrypted=" + encrypted);

        this.setDbFile(dbFile);

        CodecProvider cryptCodecProvider = null;
        if (!encrypted) {
            cryptCodecProvider = null;
        } else {
            cryptCodecProvider = new MnyDbCryptCodecProvider(password);
        }

        try {
            if (LOGGER.isDebugEnabled()) {
                LOGGER.debug("> Database.open, dbFile=" + dbFile);
            }

            if ((!readOnly) && (BackupFileUtils.isMnyFile(dbFile))) {
                File dbLockFile = null;
                if ((dbLockFile = MnyDb.lockDb(dbFile)) == null) {
                    throw new IOException("Cannot lock dbFile=" + dbFile);
                } else {
                    LOGGER.info("Created db lock file=" + dbLockFile);
                }
                this.setDbLockFile(dbLockFile);
            }
            boolean autoSync = true;
            Charset charset = null;
            // TimeZone timeZone = null;
            // TODO
            boolean provideFileChannelForReadOnly = false;
            Database db = null;
            if (provideFileChannelForReadOnly && readOnly) {
                ROMemoryMappedFileChannel channel = new ROMemoryMappedFileChannel(dbFile);
                this.setMemoryMappedFileChannel(channel);
                db = new DatabaseBuilder().setChannel(channel).setReadOnly(readOnly).setAutoSync(autoSync)
                        .setCharset(charset).setTimeZone(timeZone).setCodecProvider(cryptCodecProvider).open();
            } else {
                db = new DatabaseBuilder(dbFile).setReadOnly(readOnly).setAutoSync(autoSync).setCharset(charset)
                        .setTimeZone(timeZone).setCodecProvider(cryptCodecProvider).open();
            }
            if (db != null) {
                DateTimeType dateTimeType = DateTimeType.DATE;
                db.setDateTimeType(dateTimeType);
            }
            this.setDb(db);
            this.setPassword(password);
        } catch (UnsupportedOperationException e) {
            throw new IOException(e);
        } finally {
            if (LOGGER.isDebugEnabled()) {
                LOGGER.debug("< Database.open, dbFile=" + dbFile);
            }
        }
    }
}

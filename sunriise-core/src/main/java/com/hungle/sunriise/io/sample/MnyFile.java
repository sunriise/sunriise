package com.hungle.sunriise.io.sample;

import java.util.TimeZone;

public class MnyFile {

    /** The file name. */
    private String fileName;
    /** The password. */
    private String password;
    /** The comment. */
    private String comment;
    /** The time zone. */
    private TimeZone timeZone;

    /**
     * Instantiates a new mny test file.
     *
     * @param fileName the file name
     * @param password the password
     * @param comment  the comment
     */
    public MnyFile(String fileName, String password, String comment) {
        super();
        this.fileName = fileName;
        this.password = password;
        this.comment = comment;
    }

    /**
     * Instantiates a new mny test file.
     *
     * @param fileName the file name
     * @param password the password
     */
    public MnyFile(String fileName, String password) {
        this(fileName, password, null);
    }

    /**
     * Instantiates a new mny test file.
     *
     * @param fileName the file name
     */
    public MnyFile(String fileName) {
        this(fileName, null);
    }

    /**
     * Gets the file name.
     *
     * @return the file name
     */
    public String getFileName() {
        return fileName;
    }

    /**
     * Sets the file name.
     *
     * @param fileName the new file name
     */
    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    /**
     * Gets the password.
     *
     * @return the password
     */
    public String getPassword() {
        return password;
    }

    /**
     * Sets the password.
     *
     * @param password the new password
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * Gets the comment.
     *
     * @return the comment
     */
    public String getComment() {
        return comment;
    }

    /**
     * Sets the comment.
     *
     * @param comment the new comment
     */
    public void setComment(String comment) {
        this.comment = comment;
    }

    /**
     * Gets the time zone.
     *
     * @return the time zone
     */
    public TimeZone getTimeZone() {
        return timeZone;
    }

    /**
     * Sets the time zone.
     *
     * @param timeZone the new time zone
     */
    public void setTimeZone(TimeZone timeZone) {
        this.timeZone = timeZone;
    }

    /**
     * Checks if is backup.
     *
     * @return true, if is backup
     */
    public boolean isBackup() {
        return fileName.endsWith(".mbf");
    }

}
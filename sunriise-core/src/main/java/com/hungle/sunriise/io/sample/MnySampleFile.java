/*******************************************************************************
 * Copyright (c) 2016 Hung Le
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *******************************************************************************/
package com.hungle.sunriise.io.sample;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.hungle.sunriise.io.MnyDb;

// TODO: Auto-generated Javadoc
/**
 * The Class MnyTestFile.
 */
public class MnySampleFile extends MnyFile {

    /** The Constant SRC_TEST_DATA_MNY. */
    public static final String SRC_TEST_DATA_MNY = "src/test/data/mny/";

    /** The Constant MONEY2001_PWD_MNY. */
    public static final String MONEY2001_PWD_MNY = "money2001-pwd.mny";

    /** The Constant MONEY2001_PWD_MNY_PASSWORD. */
    private static final String MONEY2001_PWD_MNY_PASSWORD = "TEST12345";

    /** The Constant MONEY2002_MNY. */
    public static final String MONEY2002_MNY = "money2002.mny";

    /** The Constant SUNSET02_MNY_PASSWORD. */
    private static final String SUNSET02_MNY_PASSWORD = "12345678";

    /** The Constant MONEY2004_PWD_MNY. */
    public static final String MONEY2004_PWD_MNY = "money2004-pwd.mny";

    /** The Constant MONEY2004_PWD_MNY_PASSWORD. */
    private static final String MONEY2004_PWD_MNY_PASSWORD = "123@abc!";

    /** The Constant MONEY2005_PWD_MNY. */
    public static final String MONEY2005_PWD_MNY = "money2005-pwd.mny";

    /** The Constant MONEY2005_PWD_MNY_PASSWORD. */
    private static final String MONEY2005_PWD_MNY_PASSWORD = "123@abc!";

    /** The Constant MONEY2008_PWD_MNY. */
    public static final String MONEY2008_PWD_MNY = "money2008-pwd.mny";

    /** The Constant MONEY2008_PWD_MNY_PASSWORD. */
    private static final String MONEY2008_PWD_MNY_PASSWORD = "Test12345";

    /** The Constant SUNSET_SAMPLE_MBF. */
    public static final String SUNSET_SAMPLE_MBF = "sunset-sample.mbf";

    /** The Constant SUNSET01_MNY. */
    public static final String SUNSET01_MNY = "sunset01.mny";

    /** The Constant SUNSET02_MNY. */
    public static final String SUNSET02_MNY = "sunset02.mny";

    /** The Constant SUNSET_SAMPLE_PWD_5_MNY. */
    public static final String SUNSET_SAMPLE_PWD_5_MNY = "sunset-sample-pwd-5.mny";

    /** The Constant SUNSET_SAMPLE_PWD_5_MNY_PASSWORD. */
    private static final String SUNSET_SAMPLE_PWD_5_MNY_PASSWORD = "12@a!";

    /** The Constant SUNSET_SAMPLE_PWD_6_MNY. */
    public static final String SUNSET_SAMPLE_PWD_6_MNY = "sunset-sample-pwd-6.mny";

    /** The Constant SUNSET_SAMPLE_PWD_6_MNY_PASSWORD. */
    private static final String SUNSET_SAMPLE_PWD_6_MNY_PASSWORD = "12@ab!";

    /** The Constant SUNSET_401K_MNY. */
    public static final String SUNSET_401K_MNY = "sunset_401k.mny";

    /** The Constant SUNSET_SAMPLE_PWD_MNY. */
    public static final String SUNSET_SAMPLE_PWD_MNY = "sunset-sample-pwd.mny";

    /** The Constant SUNSET_SAMPLE_PWD_MNY_PASSWORD. */
    public static final String SUNSET_SAMPLE_PWD_MNY_PASSWORD = "123@abc!";

    /** The Constant SUNSET03_XFER_MNY. */
    public static final String SUNSET03_XFER_MNY = "sunset03-xfer.mny";

    /** The Constant SUNSET_BILL_MNY. */
    public static final String SUNSET_BILL_MNY = "sunset-bill.mny";

    /** The Constant TMP_LARGE_FILE_MNY. */
    public static final String LOCAL_LARGE_FILE_MNY = "local/largeFile.mny";

    /** The Constant SAMPLE_FILES. */
    public static final List<MnySampleFile> SAMPLE_FILES = new ArrayList<MnySampleFile>();
    static {
        SAMPLE_FILES.add(new MnySampleFile(SRC_TEST_DATA_MNY + MONEY2001_PWD_MNY, MONEY2001_PWD_MNY_PASSWORD));
        SAMPLE_FILES.add(new MnySampleFile(SRC_TEST_DATA_MNY + MONEY2002_MNY));
        SAMPLE_FILES.add(new MnySampleFile(SRC_TEST_DATA_MNY + MONEY2004_PWD_MNY, MONEY2004_PWD_MNY_PASSWORD));
        SAMPLE_FILES.add(new MnySampleFile(SRC_TEST_DATA_MNY + MONEY2005_PWD_MNY, MONEY2005_PWD_MNY_PASSWORD));
        SAMPLE_FILES.add(new MnySampleFile(SRC_TEST_DATA_MNY + MONEY2008_PWD_MNY, MONEY2008_PWD_MNY_PASSWORD));
        SAMPLE_FILES.add(new MnySampleFile(SRC_TEST_DATA_MNY + SUNSET_401K_MNY));
        SAMPLE_FILES
                .add(new MnySampleFile(SRC_TEST_DATA_MNY + SUNSET_SAMPLE_PWD_5_MNY, SUNSET_SAMPLE_PWD_5_MNY_PASSWORD));
        SAMPLE_FILES
                .add(new MnySampleFile(SRC_TEST_DATA_MNY + SUNSET_SAMPLE_PWD_6_MNY, SUNSET_SAMPLE_PWD_6_MNY_PASSWORD));
        SAMPLE_FILES.add(new MnySampleFile(SRC_TEST_DATA_MNY + SUNSET_SAMPLE_PWD_MNY, SUNSET_SAMPLE_PWD_MNY_PASSWORD));
        SAMPLE_FILES.add(new MnySampleFile(SRC_TEST_DATA_MNY + SUNSET_SAMPLE_MBF));
        SAMPLE_FILES.add(new MnySampleFile(SRC_TEST_DATA_MNY + SUNSET01_MNY));
        SAMPLE_FILES.add(new MnySampleFile(SRC_TEST_DATA_MNY + SUNSET02_MNY, SUNSET02_MNY_PASSWORD));
        SAMPLE_FILES.add(new MnySampleFile(SRC_TEST_DATA_MNY + SUNSET03_XFER_MNY));
        SAMPLE_FILES.add(new MnySampleFile(SRC_TEST_DATA_MNY + SUNSET_BILL_MNY));
    }

    public static final int SAMPLE_ACCOUNT_COUNT = 23;

    public static final int SAMPLE_ACCOUNT_MIN = 2;

    public static final int SAMPLE_ACCOUNT_MAX = 80;

    public static final int SAMPLE_PAYEE_MAX = 165;

    public static final int SAMPLE_PAYEE_MIN = 1;

    public static final int SAMPLE_PAYEE_COUNT = 165;

    public static final int SAMPLE_CATEGORY_MAX = 340;

    public static final int SAMPLE_CATEGORY_MIN = 130;

    public static final int SAMPLE_CATEGORY_COUNT = 141;

    public static final int SAMPLE_CURRENCY_MAX = 158;

    public static final int SAMPLE_CURRENCY_MIN = 1;

    public static final int SAMPLE_CURRENCY_COUNT = 155;

    public static final int SAMPLE_SECURITY_MAX = 123;

    public static final int SAMPLE_SECURITY_MIN = 18;

    public static final int SAMPLE_SECURITY_COUNT = 72;

    public static final int SAMPLE_TRANSACTION_MAX = 6419;

    public static final int SAMPLE_TRANSACTION_MIN = 4;

    public static final int SAMPLE_TRANSACTION_COUNT = 4629;

    /**
     * Instantiates a new mny sample file.
     *
     * @param fileName the file name
     * @param password the password
     * @param comment  the comment
     */
    public MnySampleFile(String fileName, String password, String comment) {
        super(fileName, password, comment);
    }

    /**
     * Instantiates a new mny sample file.
     *
     * @param fileName the file name
     * @param password the password
     */
    public MnySampleFile(String fileName, String password) {
        super(fileName, password);
    }

    /**
     * Instantiates a new mny sample file.
     *
     * @param fileName the file name
     */
    public MnySampleFile(String fileName) {
        super(fileName);
    }

    /**
     * Gets the sample file.
     *
     * @param fileName the file name
     * @return the sample file
     */
    public static final File getSampleFile(String fileName) {
        return getSampleFileFromModuleProject(getMnySampleFile(fileName));
    }

    /**
     * Gets the sample file.
     *
     * @param fileName the file name
     * @return the sample file
     */
    public static MnySampleFile getMnySampleFile(String fileName) {
        for (MnySampleFile sampleFile : MnySampleFile.SAMPLE_FILES) {
            String fn = sampleFile.getFileName();
            if (fn == null) {
                continue;
            }
            if (fn.equals(fileName)) {
                return sampleFile;
            }
            File f = new File(fn);
            fn = f.getName();
            if (fn.equals(fileName)) {
                return sampleFile;
            }
        }

        return null;
    }

    /**
     * For each test file.
     *
     * @param action the action
     * @throws IOException Signals that an I/O exception has occurred.
     */
    public static final void forEachSampleFile(MnyFileAction action) throws IOException {
        for (MnySampleFile dbFile : MnySampleFile.SAMPLE_FILES) {
            if (dbFile.isBackup()) {
                continue;
            }

            File file = new File(dbFile.getFileName());
            String password = dbFile.getPassword();

            MnyDb mnyDb = null;
            try {
                mnyDb = new MnyDb(file, password);
                action.run(mnyDb);
            } finally {
                if (mnyDb != null) {
                    mnyDb.close();
                }
            }
        }
    }

    /**
     * Gets the sample file from module project.
     *
     * @param mnySampleFile the db file
     * @return the sample file from module project
     */
    public static final File getSampleFileFromModuleProject(MnySampleFile mnySampleFile) {
        String fileName = mnySampleFile.getFileName();
        return getSampleFileFromModuleProject(fileName);
    }

    public static File getSampleFileFromModuleProject(String fileName) {
        if (fileName.charAt(0) != '/') {
            return new File(new File("../sunriise-core"), fileName);
        } else {
            return new File(fileName);
        }
    }

    /**
     * Gets the sample file.
     *
     * @return the sample file
     */
    public static final File getSampleFile() {
        return getSampleFile(SUNSET03_XFER_MNY);
    }

    /**
     * Gets the sample db.
     *
     * @return the sample db
     * @throws IOException Signals that an I/O exception has occurred.
     */
//    public static final MnyDb getSampleDb() throws IOException {
//        MnySampleFile sampleFile = getSampleMnyFile(SUNSET_SAMPLE_PWD_MNY);
//        File file = getSampleFileFromModuleProject(sampleFile);
//        String password = sampleFile.getPassword();
//        return new MnyDb(file, password, MnyDb.OPEN_MODE.READ_ONLY);
//    }

    /**
     * Gets the sample mny test file.
     *
     * @return the sample mny test file
     */
//    public static final File getSampleMnyFile() {
//        MnySampleFile mnyTestFile = getSampleMnyFile(SUNSET_SAMPLE_PWD_MNY);
//        return getSampleFileFromModuleProject(mnyTestFile);
//    }

    /**
     * Open db read only.
     *
     * @param fileName the file name
     * @return the opened db
     * @throws IOException Signals that an I/O exception has occurred.
     */
    public static final MnyDb openDbReadOnly(String fileName) throws IOException {
        MnySampleFile dbFile = getMnySampleFile(fileName);
        MnyDb mnyDb = null;
        if (dbFile != null) {
            mnyDb = new MnyDb(getSampleFileFromModuleProject(dbFile), dbFile.getPassword());
        }
        return mnyDb;
    }

    /**
     * Open sample file from module project.
     *
     * @param mnySampleFile the mny test file
     * @return the opened db
     * @throws IOException Signals that an I/O exception has occurred.
     */
    public static final MnyDb openSampleFileFromModuleProject(MnySampleFile mnySampleFile) throws IOException {
        File dbFile = getSampleFileFromModuleProject(mnySampleFile);
        String password = mnySampleFile.getPassword();
        MnyDb mnyDb = new MnyDb(dbFile, password);
        return mnyDb;
    }
}
package com.hungle.sunriise.io.sample;

import java.io.IOException;

import com.hungle.sunriise.io.MnyDb;

public class MnySampleFileFactory {
    public static MnySampleFile getSunsetSampleFile() throws IOException {
        MnySampleFile mnySampleFile = MnySampleFile.getMnySampleFile(MnySampleFile.SUNSET_SAMPLE_PWD_MNY);
        return mnySampleFile;
    }

    public static final MnyDb getSunsetSample() throws IOException {
        MnySampleFile mnyFile = getSunsetSampleFile();
        MnyDb mnyDb = new MnyDb(MnySampleFile.getSampleFileFromModuleProject(mnyFile), mnyFile.getPassword());
        return mnyDb;
    }
}

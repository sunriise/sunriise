/*******************************************************************************
 * Copyright (c) 2016 Hung Le
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *******************************************************************************/
package com.hungle.sunriise.io;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.channels.FileLock;
import java.nio.channels.ReadableByteChannel;
import java.nio.channels.WritableByteChannel;

// TODO: Auto-generated Javadoc
/**
 * The Class JackcessFileChannelAdapter.
 */
public class JackcessFileChannelAdapter extends FileChannel {

    /**
     * Instantiates a new jackcess file channel adapter.
     */
    public JackcessFileChannelAdapter() {
        super();
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.nio.channels.FileChannel#read(java.nio.ByteBuffer)
     */
    @Override
    public int read(ByteBuffer dst) throws IOException {
        throw new UnsupportedOperationException();
        // return 0;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.nio.channels.FileChannel#read(java.nio.ByteBuffer[], int, int)
     */
    @Override
    public long read(ByteBuffer[] dsts, int offset, int length) throws IOException {
        throw new UnsupportedOperationException();
        // return 0;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.nio.channels.FileChannel#write(java.nio.ByteBuffer)
     */
    @Override
    public int write(ByteBuffer src) throws IOException {
        throw new UnsupportedOperationException();
        // return 0;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.nio.channels.FileChannel#write(java.nio.ByteBuffer[], int, int)
     */
    @Override
    public long write(ByteBuffer[] srcs, int offset, int length) throws IOException {
        throw new UnsupportedOperationException();
        // return 0;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.nio.channels.FileChannel#position()
     */
    @Override
    public long position() throws IOException {
        throw new UnsupportedOperationException();
        // return 0;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.nio.channels.FileChannel#position(long)
     */
    @Override
    public FileChannel position(long newPosition) throws IOException {
        throw new UnsupportedOperationException();
        // return null;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.nio.channels.FileChannel#truncate(long)
     */
    @Override
    public FileChannel truncate(long size) throws IOException {
        throw new UnsupportedOperationException();
        // return null;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.nio.channels.FileChannel#transferTo(long, long,
     * java.nio.channels.WritableByteChannel)
     */
    @Override
    public long transferTo(long position, long count, WritableByteChannel target) throws IOException {
        throw new UnsupportedOperationException();
        // return 0;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.nio.channels.FileChannel#transferFrom(java.nio.channels.
     * ReadableByteChannel, long, long)
     */
    @Override
    public long transferFrom(ReadableByteChannel src, long position, long count) throws IOException {
        throw new UnsupportedOperationException();
        // return 0;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.nio.channels.FileChannel#write(java.nio.ByteBuffer, long)
     */
    @Override
    public int write(ByteBuffer src, long position) throws IOException {
        throw new UnsupportedOperationException();
        // return 0;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.nio.channels.FileChannel#map(java.nio.channels.FileChannel.MapMode,
     * long, long)
     */
    @Override
    public MappedByteBuffer map(MapMode mode, long position, long size) throws IOException {
        throw new UnsupportedOperationException();
        // return null;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.nio.channels.FileChannel#lock(long, long, boolean)
     */
    @Override
    public FileLock lock(long position, long size, boolean shared) throws IOException {
        throw new UnsupportedOperationException();
        // return null;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.nio.channels.FileChannel#tryLock(long, long, boolean)
     */
    @Override
    public FileLock tryLock(long position, long size, boolean shared) throws IOException {
        throw new UnsupportedOperationException();
        // return null;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.nio.channels.FileChannel#size()
     */
    @Override
    public long size() throws IOException {
        throw new UnsupportedOperationException();
        // return 0;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.nio.channels.FileChannel#force(boolean)
     */
    @Override
    public void force(boolean metaData) throws IOException {
        throw new UnsupportedOperationException();

    }

    /*
     * (non-Javadoc)
     * 
     * @see java.nio.channels.FileChannel#read(java.nio.ByteBuffer, long)
     */
    @Override
    public int read(ByteBuffer dst, long position) throws IOException {
        throw new UnsupportedOperationException();
        // return 0;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.nio.channels.spi.AbstractInterruptibleChannel#implCloseChannel()
     */
    @Override
    protected void implCloseChannel() throws IOException {
        throw new UnsupportedOperationException();

    }

}
/*******************************************************************************
 * Copyright (c) 2010 Hung Le
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *******************************************************************************/
package com.hungle.sunriise.io;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.util.TimeZone;

import org.apache.logging.log4j.Logger;

import com.healthmarketscience.jackcess.Database;
import com.healthmarketscience.jackcess.DatabaseBuilder;
import com.healthmarketscience.jackcess.DateTimeType;
import com.healthmarketscience.jackcess.impl.CodecProvider;
import com.hungle.sunriise.io.sample.MnySampleFile;

// TODO: Auto-generated Javadoc
/**
 * The Class FileUtil.
 */
public class FileUtils {

    /** The Constant log. */
    private static final Logger LOGGER = org.apache.logging.log4j.LogManager.getLogger(FileUtils.class);

    /** The empty file. */
    private static String EMPTY_FILE = "empty-db.mdb";

    /**
     * Creates the empty db.
     *
     * @param destFile the file
     * @return the database
     * @throws IOException Signals that an I/O exception has occurred.
     */
    public static Database createEmptyDb(File destFile) throws IOException {
        String resource = FileUtils.EMPTY_FILE;
        return createDbFromResource(resource, destFile);
    }

    /**
     * Open sample db.
     *
     * @param dbFile the dest file
     * @return the mny db
     * @throws IOException Signals that an I/O exception has occurred.
     */
    public static MnyDb openSampleDb(File dbFile) throws IOException {
        copySunsetSampleToFile(dbFile);

        String password = MnySampleFile.SUNSET_SAMPLE_PWD_MNY_PASSWORD;
        return new MnyDb(dbFile, password);
    }

    private static void copySunsetSampleToFile(File destFile) throws IOException {
        String resource = MnySampleFile.SUNSET_SAMPLE_PWD_MNY;
        copyResourceToFile(resource, destFile);
    }

    /**
     * Copy resource to file.
     *
     * @param resource the resource
     * @param destFile the dest file
     * @throws IOException Signals that an I/O exception has occurred.
     */
    private static void copyResourceToFile(String resource, File destFile) throws IOException {
        InputStream in = null;
        try {
            in = Thread.currentThread().getContextClassLoader().getResourceAsStream(resource);
            if (in == null) {
                throw new IOException("Cannot find resource=" + resource);
            }
            in = new BufferedInputStream(in);
            FileUtils.copyStreamToFile(in, destFile);
        } finally {
            if (in != null) {
                try {
                    in.close();
                } catch (IOException e) {
                    LOGGER.warn(e);
                } finally {
                    in = null;
                }
            }
        }
    }

    /**
     * Creates the db from resource.
     *
     * @param resource the resource
     * @param destFile the dest file
     * @return the database
     * @throws IOException Signals that an I/O exception has occurred.
     */
    private static Database createDbFromResource(String resource, File destFile) throws IOException {
        Database db = null;
        InputStream in = null;
        try {
            in = Thread.currentThread().getContextClassLoader().getResourceAsStream(resource);
            if (in == null) {
                throw new IOException("Cannot find resource=" + resource);
            }
            in = new BufferedInputStream(in);
            db = createDbFromStream(in, destFile);
        } finally {
            if (in != null) {
                try {
                    in.close();
                } catch (IOException e) {
                    LOGGER.warn(e);
                } finally {
                    in = null;
                }
            }
        }
        return db;
    }

    /**
     * Creates the empty db.
     *
     * @param inStream the empty db template stream
     * @param destFile the dest file
     * @return the database
     * @throws IOException Signals that an I/O exception has occurred.
     */
    private static Database createDbFromStream(InputStream inStream, File destFile) throws IOException {
        FileUtils.copyStreamToFile(inStream, destFile);

        boolean readOnly = false;
        boolean autoSync = true;
        Charset charset = null;
        TimeZone timeZone = null;
        CodecProvider codecProvider = null;

        Database db = new DatabaseBuilder(destFile).setReadOnly(readOnly).setAutoSync(autoSync).setCharset(charset)
                .setTimeZone(timeZone).setCodecProvider(codecProvider).open();
        if (db != null) {
            DateTimeType dateTimeType = DateTimeType.DATE;
            db.setDateTimeType(dateTimeType);
        }

        return db;
    }

    /**
     * Copy stream to file.
     *
     * @param srcIn    the src in
     * @param destFile the dest file
     * @throws FileNotFoundException the file not found exception
     * @throws IOException           Signals that an I/O exception has occurred.
     */
    private static void copyStreamToFile(InputStream srcIn, File destFile) throws FileNotFoundException, IOException {
        OutputStream out = null;
        try {
            out = new BufferedOutputStream(new FileOutputStream(destFile));
            FileUtils.copyStream(srcIn, out);
        } finally {

            if (out != null) {
                try {
                    out.close();
                } catch (IOException e) {
                    LOGGER.warn(e);
                } finally {
                    out = null;
                }
            }
        }
    }

    /**
     * Copy stream.
     *
     * @param in  the in
     * @param out the out
     * @throws IOException Signals that an I/O exception has occurred.
     */
    private static void copyStream(InputStream in, OutputStream out) throws IOException {
        int bufSize = 1024;
        byte[] buffer = new byte[bufSize];
        int n = 0;
        while ((n = in.read(buffer, 0, bufSize)) != -1) {
            out.write(buffer, 0, n);
        }
    }

    /**
     * Gets the resource as stream.
     *
     * @param object       the object
     * @param resourceName the name
     * @return the resource as stream
     * @throws FileNotFoundException the file not found exception
     */
    public static final InputStream getResourceAsStream(Object object, String resourceName)
            throws FileNotFoundException {
        InputStream stream = null;

        Class<?> clz = null;

        if (object == null) {
            clz = FileUtils.class;
        } else {
            clz = object.getClass();
        }

        stream = clz.getResourceAsStream(resourceName);
        if (stream != null) {
            return stream;
        }

        stream = clz.getClassLoader().getResourceAsStream(resourceName);
        if (stream != null) {
            return stream;
        }

        stream = Thread.currentThread().getContextClassLoader().getResourceAsStream(resourceName);
        if (stream != null) {
            return stream;
        }

        stream = new FileInputStream(new File(resourceName));

        return stream;
    }

    /**
     * Open sample db.
     *
     * @return the mny db
     * @throws IOException Signals that an I/O exception has occurred.
     */
    public static MnyDb openSampleDb() throws IOException {
        File destFile = createTempDbFile();
        return openSampleDb(destFile);
    }

    private static File createTempDbFile() throws IOException {
        String prefix = "sample_";
        String suffix = ".mny";
        File destFile = File.createTempFile(prefix, suffix);
        destFile.deleteOnExit();

        LOGGER.info("Created temp file=" + destFile.getAbsolutePath());

        return destFile;
    }

    public static File getSunsetSampleFile() throws IOException {
        File dbFile = createTempDbFile();
        copySunsetSampleToFile(dbFile);

        return dbFile;
    }

    public static final void printArgs(final String[] args) {
        if (args.length > 1) {
            System.out.println("args=");
            for (String x : args) {
                System.out.println("  " + x);
            }
        }
    }

    public static final MnySampleFile makeCopy(final MnySampleFile src) throws IOException {
        File srcFile = new File(src.getFileName());

        Path destPath = Files.createTempFile("copy", ".mny");

        Files.copy(srcFile.toPath(), destPath, StandardCopyOption.REPLACE_EXISTING);

        File destFile = destPath.toFile();
        LOGGER.info("Create temp copy file={}", destFile.getAbsolutePath());

        destFile.deleteOnExit();

        MnySampleFile result = new MnySampleFile(destFile.getAbsolutePath());
        result.setComment(src.getComment());
        result.setTimeZone(src.getTimeZone());
        result.setPassword(src.getPassword());

        return result;
    }

    /*
     * private static void printHeaderInfo(File dbFile, String password) throws
     * IOException { HeaderPage headerPage = new HeaderPage(dbFile);
     * HeaderPage.printHeaderPage(headerPage);
     * 
     * HeaderPagePasswordChecker checker = new
     * HeaderPagePasswordChecker(headerPage); if (!checker.check(password)) {
     * log.warn("Invalid password."); }
     * AbstractHeaderPagePasswordChecker.printChecker(checker); }
     */

}

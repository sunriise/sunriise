package com.hungle.sunriise.io;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.hungle.sunriise.dbutil.TableAccountUtils;
import com.hungle.sunriise.mnyobject.Account;
import com.hungle.sunriise.mnyobject.MnyContext;
import com.hungle.sunriise.util.MnyContextUtils;

public class MnyView {

    private MnyDb mnyDb;

    private MnyContext mnyContext;

    private Map<String, Account> accounts;

    private List<String> accountNames;

    public MnyView(File file, String password) throws IOException {
        super();

        this.mnyDb = new MnyDb(file, password);
        this.mnyContext = MnyContextUtils.createMnyContext(this.mnyDb);

        this.accounts = new HashMap<String, Account>();
        Map<Integer, Account> accountMap = this.mnyContext.getAccounts();
        for (Account account : accountMap.values()) {
            accounts.put(account.getName(), null);
        }

        this.accountNames = new ArrayList<>(accounts.keySet());
    }

    public List<String> getAccountName() {
        return this.accountNames;
    }

    public Account getAccount(String name) throws IOException {
        Account account = null;

        account = accounts.get(name);
        if (account != null) {
            return account;
        }

        Map<Integer, Account> accountMap = this.mnyContext.getAccounts();
        for (Account ac : accountMap.values()) {
            if (ac.getName().compareTo(name) == 0) {
                account = ac;
                break;
            }
        }

        if (account == null) {
            return null;
        }

        TableAccountUtils.addTransactionsToAccount(account, mnyContext);
        accounts.put(name, account);

        return account;
    }

}

package com.hungle.sunriise.io;

import java.io.IOException;
import java.nio.charset.Charset;

import com.healthmarketscience.jackcess.CryptCodecProvider;
import com.healthmarketscience.jackcess.impl.CodecHandler;
import com.healthmarketscience.jackcess.impl.DefaultCodecProvider;
import com.healthmarketscience.jackcess.impl.JetFormat;
import com.healthmarketscience.jackcess.impl.MSISAMCryptCodecHandler;
import com.healthmarketscience.jackcess.impl.PageChannel;

// TODO: Auto-generated Javadoc
/**
 * The Class MnyDbCryptCodecProvider.
 */
final class MnyDbCryptCodecProvider extends CryptCodecProvider {

    /**
     * Instantiates a new mny db crypt codec provider.
     *
     * @param password the password
     */
    MnyDbCryptCodecProvider(String password) {
        super(password);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.healthmarketscience.jackcess.CryptCodecProvider#createHandler(com.
     * healthmarketscience.jackcess.impl.PageChannel, java.nio.charset.Charset)
     */
    @Override
    public CodecHandler createHandler(PageChannel channel, Charset charset) throws IOException {
        CodecHandler codecHandler = DefaultCodecProvider.DUMMY_HANDLER;
        JetFormat format = channel.getFormat();
        if (format.CODEC_TYPE == JetFormat.CodecType.MSISAM) {
            codecHandler = super.createHandler(channel, charset);
            if (codecHandler instanceof MSISAMCryptCodecHandler) {
                // TODO: opportunity to modify the handler
                customizeCodecHandler(codecHandler);
            }
        } else {
            codecHandler = super.createHandler(channel, charset);
        }

        return codecHandler;
    }

    /**
     * Customize codec handler.
     *
     * @param codecHandler the codec handler
     */
    protected void customizeCodecHandler(CodecHandler codecHandler) {
    }
}
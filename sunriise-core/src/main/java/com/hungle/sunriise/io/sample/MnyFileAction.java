package com.hungle.sunriise.io.sample;

import java.io.IOException;

import com.hungle.sunriise.io.MnyDb;

// TODO: Auto-generated Javadoc
/**
 * The Interface MnyTestFileAction.
 */
public interface MnyFileAction {

    /**
     * Run.
     *
     * @param mnyDb the mny db
     * @throws IOException Signals that an I/O exception has occurred.
     */
    void run(MnyDb mnyDb) throws IOException;

}

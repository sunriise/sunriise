package com.hungle.sunriise.fx;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.Logger;

import com.healthmarketscience.jackcess.Column;
import com.healthmarketscience.jackcess.Cursor;
import com.healthmarketscience.jackcess.Database;
import com.healthmarketscience.jackcess.Row;
import com.hungle.sunriise.dbutil.TableCurrencyExchangeUtils;
import com.hungle.sunriise.dbutil.TableCurrencyUtils;
import com.hungle.sunriise.mnyobject.Currency;
import com.hungle.sunriise.mnyobject.CurrencyExchange;

public class UpdateExchangeRates {
    private static final Logger LOGGER = org.apache.logging.log4j.LogManager.getLogger(UpdateExchangeRates.class);

    private Database db;

    private Map<Integer, Currency> currencies;

    private Column columnRate;

    private Column columnDate;

    private boolean dryrun;

    public UpdateExchangeRates(Database db) throws IOException {
        this.db = db;
        this.currencies = TableCurrencyUtils.getMap(db);
        this.columnRate = TableCurrencyExchangeUtils.getColumn(db, TableCurrencyExchangeUtils.COLUMN_RATE);
        if (this.columnRate == null) {
            throw new IOException("Cannot find column=" + TableCurrencyExchangeUtils.COLUMN_RATE);
        }
        this.columnDate = TableCurrencyExchangeUtils.getColumn(db, TableCurrencyExchangeUtils.COLUMN_DATE);
        if (this.columnDate == null) {
            throw new IOException("Cannot find column=" + TableCurrencyExchangeUtils.COLUMN_DATE);
        }
    }

    public List<FxRateInfo> getFxRateInfo() throws IOException {
        List<FxRateInfo> list = new ArrayList<FxRateInfo>();

        Cursor cursor = null;
        try {
            cursor = TableCurrencyExchangeUtils.createCursor(db);

            for (Row row : cursor) {
                CurrencyExchange currencyExchange = TableCurrencyExchangeUtils.deserialize(row);

                // get from data
                Integer fromId = currencyExchange.getHcrncFrom();
                Currency fromCurrency = currencies.get(fromId);
                if (fromCurrency == null) {
                    LOGGER.warn("Cannot find currency with id={}", fromId);
                    continue;
                }

                // get to data
                Integer toId = currencyExchange.getHcrncTo();
                Currency toCurrency = currencies.get(toId);
                if (toCurrency == null) {
                    LOGGER.warn("Cannot find currency with id={}", toId);
                    continue;
                }

                list.add(new FxRateInfo(currencyExchange, fromCurrency, toCurrency));
            }
        } finally {
            if (cursor != null) {
                cursor = null;
            }
        }

        return list;
    }

    public void update(FxRates fxRates) throws IOException {
        Cursor cursor = null;
        try {
            cursor = TableCurrencyExchangeUtils.createCursor(db);

            for (Row row : cursor) {
                CurrencyExchange currencyExchange = TableCurrencyExchangeUtils.deserialize(row);
                LOGGER.info(currencyExchange);

                // get from data
                Integer fromId = currencyExchange.getHcrncFrom();
                Currency fromCurrency = currencies.get(fromId);
                if (fromCurrency == null) {
                    LOGGER.warn("Cannot find currency with id={}", fromId);
                    continue;
                }

                // get to data
                Integer toId = currencyExchange.getHcrncTo();
                Currency toCurrency = currencies.get(toId);
                if (toCurrency == null) {
                    LOGGER.warn("Cannot find currency with id={}", toId);
                    continue;
                }

                Double rate = currencyExchange.getRate();
                Double newRate = fxRates.getRateByIsoCode(fromCurrency.getIsoCode(), toCurrency.getIsoCode());

                if (newRate == null) {
                    this.notifyNoNewRate(fromCurrency, toCurrency, rate, newRate);
                } else {
                    updateRow(cursor, fromCurrency, toCurrency, rate, newRate);
                }
            }
        } finally {
            if (cursor != null) {
                cursor = null;
            }
        }
    }

    protected void notifyNoNewRate(Currency fromCurrency, Currency toCurrency, Double rate, Double newRate) {
        String fromIsoCode = fromCurrency.getIsoCode();
        String fromName = fromCurrency.getName();
        String toIsoCode = toCurrency.getIsoCode();
        String toName = toCurrency.getName();

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("");
            LOGGER.debug("# NO NEW FX RATE");
            LOGGER.debug("  CURRENT: " + fromIsoCode + " -> " + toIsoCode + ", " + rate + ", (" + fromName + " -> "
                    + toName + ")");
            LOGGER.debug("  NEW: " + fromIsoCode + " -> " + toIsoCode + ", " + newRate);
        }
    }

    private void updateRow(Cursor cursor, Currency from, Currency to, Double rate, Double newRate) throws IOException {
        if (!isDryrun()) {
            cursor.setCurrentRowValue(columnRate, newRate);
            cursor.setCurrentRowValue(columnDate, new Date());
        } else {
            LOGGER.info("SKIP row update, dryrun={}", isDryrun());
        }
        this.notifyUpdateExistingRate(rate, newRate, from, to);
    }

    protected void notifyUpdateExistingRate(Double rate, Double newRate, Currency fromCurrency, Currency toCurrency) {
        String fromIsoCode = fromCurrency.getIsoCode();
        String fromName = fromCurrency.getName();
        String toIsoCode = toCurrency.getIsoCode();
        String toName = toCurrency.getName();

        LOGGER.info("");
        LOGGER.info("# YES NEW FX RATE");
        LOGGER.info("  CURRENT: " + fromIsoCode + " -> " + toIsoCode + ", " + rate + ", (" + fromName + " -> " + toName
                + ")");
        LOGGER.info("  NEW: " + fromIsoCode + " -> " + toIsoCode + ", " + newRate);
    }

    public boolean isDryrun() {
        return dryrun;
    }

    public void setDryrun(boolean dryrun) {
        this.dryrun = dryrun;
    }
}

package com.hungle.sunriise.fx;

import com.hungle.sunriise.mnyobject.Currency;
import com.hungle.sunriise.mnyobject.CurrencyExchange;

public final class FxRateInfo {
    private final CurrencyExchange currencyExchange;
    private final Currency from;
    private final Currency to;

    public FxRateInfo(CurrencyExchange currencyExchange, Currency from, Currency to) {
        super();
        this.currencyExchange = currencyExchange;
        this.from = from;
        this.to = to;
    }

    public CurrencyExchange getCurrencyExchange() {
        return currencyExchange;
    }

    public Currency getFrom() {
        return from;
    }

    public Currency getTo() {
        return to;
    }

}
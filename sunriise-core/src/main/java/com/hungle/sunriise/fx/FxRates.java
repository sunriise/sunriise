package com.hungle.sunriise.fx;

public interface FxRates {

    Double getRateByIsoCode(String from, String to);

}

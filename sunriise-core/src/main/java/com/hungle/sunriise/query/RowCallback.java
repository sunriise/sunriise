package com.hungle.sunriise.query;

import java.io.IOException;

import com.healthmarketscience.jackcess.Row;

// TODO: Auto-generated Javadoc
/**
 * The Interface RowCallback.
 */
public interface RowCallback {

    /**
     * Collect.
     *
     * @param row the row
     * @throws IOException Signals that an I/O exception has occurred.
     */
    void collect(Row row) throws IOException;

}

package com.hungle.sunriise.query;

import java.io.IOException;
import java.util.Collections;
import java.util.Map;

import com.healthmarketscience.jackcess.Cursor;
import com.healthmarketscience.jackcess.CursorBuilder;
import com.healthmarketscience.jackcess.Database;
import com.healthmarketscience.jackcess.Row;
import com.healthmarketscience.jackcess.Table;
import com.hungle.sunriise.dbutil.TableTransactionUtils;
import com.hungle.sunriise.mnyobject.Account;

// TODO: Auto-generated Javadoc
/**
 * The Class TransactionQuery.
 */
public class TransactionQuery {

    /**
     * Find transactions for account.
     *
     * @param db       the db
     * @param account  the account
     * @param callback the callback
     * @throws IOException Signals that an I/O exception has occurred.
     */
    public static final void findTransactionsForAccount(Database db, Account account, RowCallback callback)
            throws IOException {
        Integer accountId = account.getId();

        Map<String, Integer> criteria = Collections.singletonMap(TableTransactionUtils.COL_ACCOUNT_ID, accountId);

        Table table = TableTransactionUtils.getTable(db);
        Cursor cursor = CursorBuilder.createCursor(table);
        while (cursor.findNextRow(criteria)) {
            Row row = cursor.getCurrentRow();
            callback.collect(row);
        }
    }

    /**
     * Find transactions.
     *
     * @param db       the db
     * @param callback the callback
     * @throws IOException Signals that an I/O exception has occurred.
     */
    public static final void findTransactions(Database db, RowCallback callback) throws IOException {
        Table table = TableTransactionUtils.getTable(db);
        Cursor cursor = CursorBuilder.createCursor(table);
        for (Row row : cursor) {
            callback.collect(row);
        }
    }
}

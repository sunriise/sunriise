/*******************************************************************************
 * Copyright (c) 2016 Hung Le
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *******************************************************************************/
package com.hungle.sunriise.json;

import java.io.IOException;
import java.util.List;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.hungle.sunriise.mnyobject.Transaction;

// TODO: Auto-generated Javadoc
/**
 * The Class AccountTransactionsSerializer.
 */
public class AccountTransactionsSerializer extends JsonSerializer<List<Transaction>> {

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.fasterxml.jackson.databind.JsonSerializer#serialize(java.lang.Object,
     * com.fasterxml.jackson.core.JsonGenerator,
     * com.fasterxml.jackson.databind.SerializerProvider)
     */
    @Override
    public void serialize(List<Transaction> transactions, JsonGenerator gen, SerializerProvider serializers)
            throws IOException, JsonProcessingException {
        if (transactions == null) {
            gen.writeNull();
        } else {
            serializeSimpleAccount(transactions, gen);
        }
    }

    /**
     * Serialize simple account.
     *
     * @param transactions the transactions
     * @param gen          the gen
     * @throws IOException Signals that an I/O exception has occurred.
     */
    private void serializeSimpleAccount(List<Transaction> transactions, JsonGenerator gen) throws IOException {
        gen.writeStartObject();
        try {
            gen.writeFieldName("count");
            gen.writeObject(transactions.size());
        } finally {
            gen.writeEndObject();
        }
    }
}

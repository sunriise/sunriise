/*******************************************************************************
 * Copyright (c) 2010 Hung Le
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *******************************************************************************/
package com.hungle.sunriise.json;

import java.io.BufferedWriter;
import java.io.CharArrayWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.Writer;
import java.util.Date;

import org.apache.logging.log4j.Logger;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.joda.JodaModule;
import com.hungle.sunriise.io.FileUtils;

// TODO: Auto-generated Javadoc
/**
 * The Class JSONUtils.
 */
public class JSONUtils {

    /** The Constant LOGGER. */
    private static final Logger LOGGER = org.apache.logging.log4j.LogManager.getLogger(JSONUtils.class);

    /** The Constant DEFAULT_SUFFIX. */
    private static final String DEFAULT_SUFFIX = ".json";

    /** The Constant DEFAULT_PREFIX. */
    private static final String DEFAULT_PREFIX = "test-";

    /**
     * Creates the default object mapper.
     *
     * @return the object mapper
     */
    public static ObjectMapper createDefaultObjectMapper() {
        ObjectMapper mapper;
        mapper = new ObjectMapper();
        mapper.registerModule(new JodaModule());
        mapper.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false);
        mapper.configure(JsonParser.Feature.ALLOW_COMMENTS, true);
        // mapper.disable(MapperFeature.DEFAULT_VIEW_INCLUSION);
        mapper.configure(MapperFeature.SORT_PROPERTIES_ALPHABETICALLY, true);

//        mapper.configure(JsonGenerator.Feature.AUTO_CLOSE_JSON_CONTENT, false);

        return mapper;
    }

    /**
     * Deserialize from src.
     *
     * @param <T>       the generic type
     * @param src       the src
     * @param valueType the value type
     * @return the t
     * @throws IOException Signals that an I/O exception has occurred.
     */
    public static <T> T deserializeFromSrc(Object src, Class<T> valueType) throws IOException {
        InputStream stream = getTestResourceAsStream(src, valueType);
        return deserializeFromStream(stream, valueType);
    }

    /**
     * Deserialize from stream.
     *
     * @param <T>       the generic type
     * @param stream    the stream
     * @param valueType the value type
     * @return the t
     * @throws IOException          Signals that an I/O exception has occurred.
     * @throws JsonParseException   the json parse exception
     * @throws JsonMappingException the json mapping exception
     */
    public static <T> T deserializeFromStream(InputStream stream, Class<T> valueType)
            throws IOException, JsonParseException, JsonMappingException {
        T object = null;
        if (stream != null) {
            ObjectMapper mapper = new ObjectMapper();
            mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
            object = mapper.readValue(stream, valueType);
        }
        return object;
    }

    /**
     * Gets the default resource name.
     *
     * @param <T>       the generic type
     * @param valueType the value type
     * @return the default resource name
     */
    private static <T> String getDefaultResourceName(Class<T> valueType) {
        return DEFAULT_PREFIX + valueType.getSimpleName() + DEFAULT_SUFFIX;
    }

    /**
     * Gets the test resource as stream.
     *
     * @param <T>       the generic type
     * @param src       the src
     * @param valueType the value type
     * @return the test resource as stream
     * @throws FileNotFoundException the file not found exception
     */
    public static <T> InputStream getTestResourceAsStream(Object src, Class<T> valueType) throws FileNotFoundException {
        String resourceName = getDefaultResourceName(valueType);
        LOGGER.info("resourceName=" + resourceName);
        InputStream stream = FileUtils.getResourceAsStream(src, resourceName);
        return stream;
    }

    /**
     * Serialize.
     *
     * @param <T>   the generic type
     * @param value the value
     * @return the string
     * @throws IOException Signals that an I/O exception has occurred.
     */
    public static <T> String serialize(T value) throws IOException {
        ObjectMapper mapper = null;
//        Class<?> serializationView = Views.Public.class;
        Class<?> serializationView = null;
        return serialize(value, mapper, serializationView);
    }

    /**
     * Serialize.
     *
     * @param <T>    the generic type
     * @param value  the value
     * @param mapper the mapper
     * @return the string
     * @throws IOException Signals that an I/O exception has occurred.
     */
    public static <T> String serialize(T value, ObjectMapper mapper) throws IOException {
        Class<?> serializationView = null;
        return serialize(value, mapper, serializationView);
    }

    /**
     * Serialize.
     *
     * @param <T>               the generic type
     * @param value             the value
     * @param serializationView the serialization view
     * @return the string
     * @throws IOException Signals that an I/O exception has occurred.
     */
    public static <T> String serialize(T value, Class<?> serializationView) throws IOException {
        ObjectMapper mapper = null;
        return serialize(value, mapper, serializationView);
    }

    /**
     * Serialize.
     *
     * @param <T>               the generic type
     * @param value             the value
     * @param mapper            the mapper
     * @param serializationView TODO
     * @return the string
     * @throws IOException Signals that an I/O exception has occurred.
     */
    public static <T> String serialize(T value, ObjectMapper mapper, Class<?> serializationView) throws IOException {
        String str = null;
        CharArrayWriter writer = new CharArrayWriter();
        try {
            try {
                JSONUtils.writeValue(value, writer, mapper, serializationView);
            } catch (JsonGenerationException e) {
                throw new IOException(e);
            } catch (JsonMappingException e) {
                throw new IOException(e);
            }
        } finally {
            if (writer != null) {
                try {
                    writer.close();
                    str = new String(writer.toCharArray());
                } finally {
                    writer = null;
                }
            }
        }
        return str;
    }

    /**
     * Write value.
     *
     * @param <T>   the generic type
     * @param value the value
     * @throws IOException Signals that an I/O exception has occurred.
     */
    public static <T> void writeValue(T value) throws IOException {
        Writer writer = new PrintWriter(new OutputStreamWriter(System.out));
        writeValue(value, writer);
    }

    /**
     * Write value.
     *
     * @param <T>     the generic type
     * @param value   the value
     * @param outFile the out file
     * @throws IOException Signals that an I/O exception has occurred.
     */
    public static <T> void writeValue(T value, File outFile) throws IOException {
        writeValue(value, outFile, null);
    }

    /**
     * Write value.
     *
     * @param <T>    the generic type
     * @param value  the value
     * @param file   the file
     * @param mapper the mapper
     * @throws IOException Signals that an I/O exception has occurred.
     */
    private static <T> void writeValue(T value, File file, ObjectMapper mapper) throws IOException {
        Writer writer = null;
        try {
            writer = new BufferedWriter(new FileWriter(file));
            writeValue(value, writer, mapper, null);
        } catch (JsonGenerationException e) {
            throw new IOException(e);
        } catch (JsonMappingException e) {
            throw new IOException(e);
        } finally {
            if (writer != null) {
                try {
                    writer.close();
                } catch (IOException e) {
                    LOGGER.warn(e);
                } finally {
                    writer = null;
                }
            }
        }
    }

    /**
     * Write value.
     *
     * @param <T>    the generic type
     * @param value  the value
     * @param writer the writer
     * @throws JsonGenerationException the json generation exception
     * @throws JsonMappingException    the json mapping exception
     * @throws IOException             Signals that an I/O exception has occurred.
     */
    public static <T> void writeValue(T value, Writer writer)
            throws JsonGenerationException, JsonMappingException, IOException {
        ObjectMapper mapper = null;
        writeValue(value, writer, mapper, null);
    }

    /**
     * Write value as string.
     *
     * @param value the value
     * @return the string
     * @throws JsonGenerationException the json generation exception
     * @throws JsonMappingException    the json mapping exception
     * @throws IOException             Signals that an I/O exception has occurred.
     */
    public static String writeValueAsString(Object value)
            throws JsonGenerationException, JsonMappingException, IOException {
        StringWriter writer = new StringWriter();
        writeValue(value, writer);
        writer.flush();
        return writer.toString();
    }

    /**
     * Write value.
     *
     * @param <T>               the generic type
     * @param value             the value
     * @param writer            the writer
     * @param prettyPrint       the pretty print
     * @param mapper            the mapper
     * @param serializationView TODO
     * @throws JsonGenerationException the json generation exception
     * @throws JsonMappingException    the json mapping exception
     * @throws IOException             Signals that an I/O exception has occurred.
     */
    public static <T> void writeValue(T value, Writer writer, boolean prettyPrint, ObjectMapper mapper,
            Class<?> serializationView) throws JsonGenerationException, JsonMappingException, IOException {
        if (mapper == null) {
            mapper = createDefaultObjectMapper();
        }

        ObjectWriter objectWriter = null;
        if (serializationView != null) {
            objectWriter = mapper.writerWithView(serializationView);
        } else {
            objectWriter = mapper.writer();
        }

        if (prettyPrint) {
            objectWriter = objectWriter.withDefaultPrettyPrinter();
        }
        boolean skipWriteValue = false;
        if (!skipWriteValue) {
            objectWriter.writeValue(writer, value);
        }
    }

    /**
     * Write value.
     *
     * @param <T>               the generic type
     * @param value             the value
     * @param writer            the writer
     * @param mapper            the mapper
     * @param serializationView TODO
     * @throws JsonGenerationException the json generation exception
     * @throws JsonMappingException    the json mapping exception
     * @throws IOException             Signals that an I/O exception has occurred.
     */
    private static <T> void writeValue(T value, Writer writer, ObjectMapper mapper, Class<?> serializationView)
            throws JsonGenerationException, JsonMappingException, IOException {
        boolean prettyPrint = true;
        writeValue(value, writer, prettyPrint, mapper, serializationView);
    }

    /**
     * Write value.
     *
     * @param <T>    the generic type
     * @param value  the value
     * @param writer the writer
     * @param mapper the mapper
     * @throws JsonGenerationException the json generation exception
     * @throws JsonMappingException    the json mapping exception
     * @throws IOException             Signals that an I/O exception has occurred.
     */
    public static <T> void writeValue(T value, Writer writer, ObjectMapper mapper)
            throws JsonGenerationException, JsonMappingException, IOException {
        Class<?> serializationView = null;
        writeValue(value, writer, mapper, serializationView);
    }

    /**
     * Write value.
     *
     * @param <T>         the generic type
     * @param value       the value
     * @param writer      the writer
     * @param prettyPrint the pretty print
     * @throws JsonGenerationException the json generation exception
     * @throws JsonMappingException    the json mapping exception
     * @throws IOException             Signals that an I/O exception has occurred.
     */
    public static <T> void writeValue(T value, Writer writer, boolean prettyPrint)
            throws JsonGenerationException, JsonMappingException, IOException {
        ObjectMapper mapper = null;
        Class<?> serializationView = null;
        writeValue(value, writer, prettyPrint, mapper, serializationView);
    }

    /**
     * Write value.
     *
     * @param value       the value
     * @param writer      the writer
     * @param prettyPrint the pretty print
     * @param mapper      the mapper
     * @throws JsonGenerationException the json generation exception
     * @throws JsonMappingException    the json mapping exception
     * @throws IOException             Signals that an I/O exception has occurred.
     */
    public static void writeValue(Object value, Writer writer, boolean prettyPrint, ObjectMapper mapper)
            throws JsonGenerationException, JsonMappingException, IOException {
        Class<?> serializationView = null;
        writeValue(value, writer, prettyPrint, mapper, serializationView);
    }

    /**
     * To ISO date time format string.
     *
     * @param date the date
     * @return the string
     */
    public static final String toISODateTimeFormatString(Date date) {
//        String string = ISODateTimeFormat.dateTime().print(new DateTime(date));
        String string = MnyDateSerializer.iso8601DateFormatter.format(date);
        return string;
    }

}

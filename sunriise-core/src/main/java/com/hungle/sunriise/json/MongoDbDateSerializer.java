package com.hungle.sunriise.json;

import java.io.IOException;
import java.util.Date;

import org.apache.logging.log4j.Logger;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;

// TODO: Auto-generated Javadoc
/**
 * The Class MongoDbDateSerializer.
 */
public final class MongoDbDateSerializer extends StdSerializer<Date> {

    /** The Constant LOGGER. */
    private static final Logger LOGGER = org.apache.logging.log4j.LogManager.getLogger(MongoDbDateSerializer.class);

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 1L;

    /**
     * Instantiates a new mongo db date serializer.
     *
     * @param t the t
     */
    public MongoDbDateSerializer(Class<Date> t) {
        super(t);
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.fasterxml.jackson.databind.ser.std.StdSerializer#serialize(java.lang.
     * Object, com.fasterxml.jackson.core.JsonGenerator,
     * com.fasterxml.jackson.databind.SerializerProvider)
     */
    @Override
    public void serialize(Date date, JsonGenerator jgen, SerializerProvider provider) throws IOException {
        try {
            if (date == null) {
                jgen.writeNull();
            } else {
                // writeAsFormattedString(date, jgen);
                writeAsNumberLong(date, jgen);
            }
        } catch (Exception ex) {
            LOGGER.error("Couldn't format timestamp " + date + ", writing 'null'", ex);
            jgen.writeNull();
        }
    }

    /**
     * Write as number long.
     *
     * @param date the date
     * @param jgen the jgen
     * @throws IOException Signals that an I/O exception has occurred.
     */
    private void writeAsNumberLong(Date date, JsonGenerator jgen) throws IOException {
        // { "$date" : { "$numberLong" : "<dateAsMilliseconds>" } }
        jgen.writeStartObject();
        jgen.writeFieldName("$date");

        jgen.writeStartObject();
        jgen.writeFieldName("$numberLong");

        jgen.writeString("" + date.getTime());
        jgen.writeEndObject();
        jgen.writeEndObject();

    }

    /**
     * Write as formatted string.
     *
     * @param date the date
     * @param jgen the jgen
     * @throws IOException Signals that an I/O exception has occurred.
     */
    private void writeAsFormattedString(Date date, JsonGenerator jgen) throws IOException {
        // { "$date": "<date>" }
        jgen.writeStartObject();
        jgen.writeFieldName("$date");
        String isoDate = JSONUtils.toISODateTimeFormatString(date);
        jgen.writeString(isoDate);
        jgen.writeEndObject();
    }
}
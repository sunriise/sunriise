package com.hungle.sunriise.script.js;

import java.io.File;
import java.io.IOException;

import javax.script.Invocable;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;

import org.junit.Assert;
import org.junit.Test;

import com.hungle.sunriise.io.sample.MnySampleFile;

public class JavascriptTest {
    @Test
    public void addTest() throws ScriptException {
        ScriptEngine engine = new ScriptEngineManager().getEngineByName(JsScriptUtils.NASHORN);

        Object result = engine.eval("1 + 2");
        Assert.assertNotNull(result);

        Assert.assertTrue(result instanceof Integer);
        Integer value = (Integer) result;
        Assert.assertEquals(3, value.intValue());
    }

    @Test
    public void evalTest1() throws ScriptException, IOException, NoSuchMethodException {
        String resourceName = "myMultiplier.js";
        Assert.assertNotNull(resourceName);

        ScriptEngine engine = JsScriptUtils.getScriptEngine(this, resourceName);

        Invocable runner = (Invocable) engine;

        Object result = runner.invokeFunction("myMultiplier", 2, 3);
        Assert.assertNotNull(result);
        Assert.assertTrue(result instanceof Double);
        Assert.assertEquals(new Double(6), result);
    }

    @Test
    public void evalTest3() throws ScriptException, IOException, NoSuchMethodException {
        String resourceName = "evalTest3.js";
        Assert.assertNotNull(resourceName);

        JsScriptRunner scriptRunner = new JsScriptRunner();
        scriptRunner.sourceScript(resourceName);

        for (MnySampleFile sampleFile : MnySampleFile.SAMPLE_FILES) {
            if (sampleFile.isBackup()) {
                continue;
            }

            File file = MnySampleFile.getSampleFileFromModuleProject(sampleFile);
            scriptRunner.setFileName(file.getAbsolutePath());
            scriptRunner.setPassword(sampleFile.getPassword());
            scriptRunner.doVisit();
        }
    }

    @Test
    public void evalTest4() throws ScriptException, IOException, NoSuchMethodException {
        String resourceName = "evalTest4.js";
        Assert.assertNotNull(resourceName);

        JsScriptRunner scriptRunner = new JsScriptRunner();
        scriptRunner.sourceScript(resourceName);
        scriptRunner.doVisit();
    }

    @Test
    public void printInvestmentTransactions() throws ScriptException, IOException, NoSuchMethodException {
        String resourceName = "printInvestmentTransactions.js";
        Assert.assertNotNull(resourceName);

        JsScriptRunner scriptRunner = new JsScriptRunner();
        scriptRunner.sourceScript(resourceName);
        scriptRunner.doVisit();
    }
}

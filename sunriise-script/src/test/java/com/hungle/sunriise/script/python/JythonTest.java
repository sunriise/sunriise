package com.hungle.sunriise.script.python;

import java.io.IOException;
import java.io.InputStream;
import java.util.Map;

import org.junit.Assert;
import org.junit.Test;
import org.python.core.PyString;
import org.python.core.PySystemState;
import org.python.util.PythonInterpreter;

import com.hungle.sunriise.dbutil.TableAccountUtils;
import com.hungle.sunriise.io.FileUtils;
import com.hungle.sunriise.io.MnyDb;
import com.hungle.sunriise.io.sample.MnySampleFile;
import com.hungle.sunriise.mnyobject.Account;
import com.hungle.sunriise.mnyobject.MnyContext;
import com.hungle.sunriise.util.MnyContextUtils;

public class JythonTest {
    private PythonInterpreter pythonInterpreter = new PythonInterpreter();

    public JythonTest() {
        super();
        // Properties p = new Properties();
        // p.setProperty("python.path", "PATH OF JYTHON");
        // p.setProperty("python.home", "PATH OF JYTHON");
        // p.setProperty("python.prefix", "PATH OF JYTHON");
        // PythonInterpreter.initialize(System.getProperties(), p, new String[]
        // {});
        // interpreter = new PythonInterpreter();
        pythonInterpreter = new PythonInterpreter(null, new PySystemState());
        PySystemState sys = pythonInterpreter.getSystemState();
        String rootPath = JythonTest.class.getProtectionDomain().getCodeSource().getLocation().getPath();
        // System.out.println(rootPath);
        sys.path.append(new PyString(rootPath));
//        sys.setdefaultencoding("utf8");
//        interpr.exec("import sys");
//        interpr.exec("sys.setdefaultencoding(\"utf8\")");
//        interpr.exec("# coding=utf-8");
    }

    @Test
    public void testHelloWorld() throws IOException {
        String resourceName = "helloWorld.py";
        try (InputStream inputStream = FileUtils.getResourceAsStream(this, resourceName)) {
            pythonInterpreter.execfile(inputStream);
        }
    }

    @Test
    public void testExportToCsv() throws IOException {
        MnySampleFile mnySampleFile = MnySampleFile.getMnySampleFile(MnySampleFile.SUNSET_SAMPLE_PWD_MNY);
        Assert.assertNotNull(mnySampleFile);
        try (MnyDb mnyDb = MnySampleFile.openSampleFileFromModuleProject(mnySampleFile)) {

            Assert.assertNotNull(mnyDb);

            MnyContext mnyContext = MnyContextUtils.createMnyContext(mnyDb);
            TableAccountUtils.populateAccounts(mnyContext);
            pythonInterpreter.set("mnyContext", mnyContext);

            Map<Integer, Account> accounts = mnyContext.getAccounts();
            pythonInterpreter.set("accounts", accounts.values());

            String resourceName = "exportToCsv.py";
            try (InputStream inputStream = FileUtils.getResourceAsStream(this, resourceName)) {
                pythonInterpreter.execfile(inputStream);
            }
            pythonInterpreter.exec("print(accounts.size())");
        }
    }
}

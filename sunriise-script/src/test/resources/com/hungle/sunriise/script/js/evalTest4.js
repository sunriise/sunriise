// Function: init(scriptRunner)
//   ScriptRunner scriptRunner
function init(scriptRunner) {
    print('> init() - ' + scriptRunner.mnyContext.db.file);
}

// Function initConfiguration
// String fileName: name of the *.mny file
// String password: password or empty if no password
// String outputFileName:
function initConfiguration() {
    var configuration = {
	fileName : '../sunriise-core/src/test/data/mny/sunset-sample-pwd.mny',
	password : '123@abc!',
	outputFileName : '',
    }

    return configuration;
}

function visitAccount(account) {
    var name = account.name;
    print('# account.name=' + name);
    return true;
}

function visitTransaction(account, transaction) {
    if (transaction.isVoid()) {
	printTransaction('VOID', transaction);
    }

    if (transaction.isUnaccepted()) {
	printTransaction('UNACCEPTED', transaction);
    }
    return true;
}

function destroy() {
    print('> destroy()');
}

function printTransaction(label, transaction) {
    var sep = '\t';
    print(label + sep + transaction.getId() + sep + transaction.getDate() + sep
	    + transaction.getAmount());
}

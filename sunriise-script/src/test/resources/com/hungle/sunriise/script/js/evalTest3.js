function initConfiguration() {
    var configuration = {}

    return configuration;
}

function init(scriptRunner) {
    print('> init() - ' + scriptRunner.mnyContext.db.file);
}

function visitAccount(account) {
    var name = account.name;
    print('# account.name=' + name);
    return true;
}

function visitTransaction(account, transaction) {
    if (transaction.isVoid()) {
	printTransaction('VOID', transaction);
    }

    if (transaction.isUnaccepted()) {
	printTransaction('UNACCEPTED', transaction);
    }
    return true;
}

function destroy() {
    print('> destroy()');
}

function printTransaction(label, transaction) {
    var sep = '\t';
    print(label + sep + transaction.getId() + sep + transaction.getDate() + sep
	    + transaction.getAmount());
}

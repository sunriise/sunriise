// Function: init(scriptRunner)
//   ScriptRunner scriptRunner
function init(scriptRunner) {
    print('> init() - ' + scriptRunner.mnyContext.db.file);
}

// Function initConfiguration()
// String fileName: name of the *.mny file
// String password: password or empty if no password
// String outputFileName:
function initConfiguration() {
    var configuration = {
	fileName : '../sunriise-core/src/test/data/mny/sunset-sample-pwd.mny',
	password : '123@abc!',
	outputFileName : '',
    }

    return configuration;
}

function visitAccount(account) {
    var EnumAccountType = Java.type('com.hungle.sunriise.mnyobject.EnumAccountType');
    if (EnumAccountType.isInvestment(account)) {
	print('# account.name=' + account.name);
	return true;
    } else {
	return false;
    }
}

function visitTransaction(account, transaction) {
    if (transaction.isInvestment()) {
	printTransaction(account.name, transaction);
    }
    return true;
}

function destroy() {
    print('> destroy()');
}

function printTransaction(label, transaction) {
    var sep = '\t';
    var investmentInfo = transaction.investmentInfo;

    var str = label;
    str = str + sep + investmentInfo.activity + sep
	    + investmentInfo.security.name + sep
	    + investmentInfo.security.symbol + sep
	    + investmentInfo.transaction.quantity + sep + transaction.amount;
    print(str);
}

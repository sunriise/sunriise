import sys
import os
import csv
import re

import com.hungle.sunriise.mnyobject.EnumAccountType as AccountType

if os.name == 'java':
    from org.python.core import codecs
    codecs.setDefaultEncoding('utf-8')

    print('Jython {}'.format(sys.version))
else:
    print('Python {}'.format(sys.version))
    
writer = csv.writer(sys.stdout, quoting=csv.QUOTE_ALL)

# how to iterate through all the accounts
for account in accounts:
    # example on how to filter on accountType
    if (account.getAccountType() == AccountType.BANKING):
        name = account.getName()
        # filter by account name
        if (re.match(r'.*Bond.*', name)):
            print('#{}'.format(name))
            transactions = account.getTransactions()
            # print('#{}'.format(len(transactions)))
            for transaction in transactions:
                # only show transaction with positive amount
                amount = transaction.getAmount()
                # amount is of type Java BidDecimal so we need to get doubleValue
                if (amount.doubleValue() > 0.00):
                    row = [transaction.getDate(),
                           transaction.getAmount(),
                           transaction.getPayee().getName(),
                           transaction.getCategory().getFullName()
                           ]
                    writer.writerow([str(s) for s in row])
        

import sys
import os

import com.hungle.sunriise.mnyobject
import com.hungle.sunriise.util

if os.name == 'java':
    from org.python.core import codecs
    codecs.setDefaultEncoding('utf-8')

    print 'Jython', sys.version
else:
    print 'Python', sys.version
    
categories = mnyContext.categories;
for key, category in categories.iteritems():
    print MnyObjectUtil.getCategoryFullName(key, categories)

import sys
import os
import csv

if os.name == 'java':
    from org.python.core import codecs
    codecs.setDefaultEncoding('utf-8')

    print 'Jython', sys.version
else:
    print 'Python', sys.version
    
writer = csv.writer(sys.stdout, quoting=csv.QUOTE_ALL)

for account in accounts:
    print '#', account.getName()
    transactions = account.getTransactions()
    for transaction in transactions:
        row = [transaction.getDate(),
               transaction.getAmount(),
               transaction.getPayee().getName(),
               transaction.getCategory().getFullName()
               ]
        writer.writerow([str(s) for s in row])
    

import sys
import os

import com.hungle.sunriise.mnyobject
import com.hungle.sunriise.util

if os.name == 'java':
    from org.python.core import codecs
    codecs.setDefaultEncoding('utf-8')

    print 'Jython', sys.version
else:
    print 'Python', sys.version
    
securities = mnyContext.securities
for key, security in securities.iteritems():
    print("name={}, symbol={}".format(security.name, security.symbol))

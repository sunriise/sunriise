package com.hungle.sunriise.script.python;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JPanel;

import org.fife.ui.rsyntaxtextarea.RSyntaxTextArea;
import org.fife.ui.rsyntaxtextarea.SyntaxConstants;
import org.fife.ui.rtextarea.RTextScrollPane;

public class DefaultInputView extends JPanel {
    private RSyntaxTextArea textArea;
    private JButton runButton;

    public DefaultInputView() {
        super();
        setLayout(new BorderLayout());

        textArea = new RSyntaxTextArea();
        textArea.setSyntaxEditingStyle(SyntaxConstants.SYNTAX_STYLE_PYTHON);
        RTextScrollPane sp = new RTextScrollPane(textArea);
        textArea.setEditable(true);
        add(sp, BorderLayout.CENTER);

        JPanel commandView = new JPanel();
        commandView.setLayout(new BoxLayout(commandView, BoxLayout.LINE_AXIS));
        commandView.setBorder(BorderFactory.createEmptyBorder(3, 3, 3, 3));
        commandView.add(Box.createHorizontalGlue());
        runButton = new JButton(new AbstractAction("Run") {

            @Override
            public void actionPerformed(ActionEvent e) {
                // TODO Auto-generated method stub

            }
        });
        runButton.setEnabled(false);
        commandView.add(runButton);
        commandView.add(Box.createHorizontalGlue());
        add(commandView, BorderLayout.SOUTH);
    }

    public JButton getRunButton() {
        return runButton;
    }

    public void setRunButton(JButton runButton) {
        this.runButton = runButton;
    }

}

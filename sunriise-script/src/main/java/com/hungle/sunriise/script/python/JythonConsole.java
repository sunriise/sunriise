package com.hungle.sunriise.script.python;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.IOException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.prefs.Preferences;

import javax.swing.AbstractAction;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JSplitPane;
import javax.swing.SwingUtilities;

import org.apache.logging.log4j.Logger;

import com.hungle.sunriise.io.FileUtils;
import com.hungle.sunriise.io.MnyDb;

public class JythonConsole implements PropertyChangeListener {
    private static final Logger LOGGER = org.apache.logging.log4j.LogManager.getLogger(JythonConsole.class);

    static final String PROPERTY_MNY_DB = "mnyDb";

    private static final Preferences prefs = Preferences.userNodeForPackage(JythonConsole.class);

    private JFrame frame;

    private MnyDb mnyDb;

    private final PropertyChangeSupport propertyChangeSupport;

    private ExecutorService threadPool = Executors.newCachedThreadPool();

    /**
     * Launch the application.
     */
    public static void main(String[] args) {
        EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                try {
                    JythonConsole window = new JythonConsole();
                    window.getFrame().setLocationRelativeTo(null);
                    window.getFrame().pack();
                    window.getFrame().setVisible(true);
                } catch (Exception e) {
                    LOGGER.error(e, e);
                }
            }
        });
    }

    /**
     * Create the application.
     */
    public JythonConsole() {
        super();

        this.propertyChangeSupport = new PropertyChangeSupport(this);
        this.propertyChangeSupport.addPropertyChangeListener(this);

        initialize();
    }

    /**
     * Initialize the contents of the frame.
     */
    private void initialize() {
        setFrame(new JFrame());
        getFrame().setBounds(100, 100, 800, 800);
        Dimension preferredSize = new Dimension(800, 800);
        getFrame().setPreferredSize(preferredSize);
        getFrame().setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        Container mainView = getFrame().getContentPane();
        mainView.setLayout(new BorderLayout());

        final JPanel topView = new JPanel();
        topView.setLayout(new BorderLayout());
        topView.add(new JythonMainView(propertyChangeSupport, threadPool), BorderLayout.CENTER);
//        topView.add(new JythonStatusView(), BorderLayout.SOUTH);

        final JPanel bottomView = new JPanel();
        bottomView.setLayout(new BorderLayout());
        final JythonOutputView outputView = new JythonOutputView(propertyChangeSupport);
        bottomView.add(outputView, BorderLayout.CENTER);

        JSplitPane splitPane = new JSplitPane(JSplitPane.VERTICAL_SPLIT, topView, bottomView);
        splitPane.setDividerLocation(0.50);
        splitPane.setResizeWeight(0.50);

        mainView.add(splitPane, BorderLayout.CENTER);

        initMainMenuBar();

        ComponentListener componentListener = new ComponentAdapter() {
            @Override
            public void componentShown(ComponentEvent e) {
                LOGGER.info("> componentShown");

                if (getMnyDb() == null) {
                    outputView.setText("To open sample file, use menu File -> Open sample file");
                }
            }
        };
        getFrame().addComponentListener(componentListener);
    }

    private void initMainMenuBar() {
        JMenuBar menuBar = new JMenuBar();
        getFrame().setJMenuBar(menuBar);

        JMenu fileMenu = new JMenu("File");
        menuBar.add(fileMenu);

        JMenuItem fileOpenMenuItem = new JMenuItem("Open");
        MyOpenDbAction listener = new MyOpenDbAction(JythonConsole.this.getFrame(), propertyChangeSupport, prefs,
                getMnyDb());
        fileOpenMenuItem.addActionListener(listener);
        fileMenu.add(fileOpenMenuItem);

        JMenuItem menuItem = new JMenuItem(new AbstractAction("Open sample file") {

            @Override
            public void actionPerformed(ActionEvent event) {
                Runnable command = new Runnable() {

                    @Override
                    public void run() {
                        try {
                            propertyChangeSupport.firePropertyChange(JythonOutputView.OPENING_FILE, null,
                                    "Sample file");

                            MnyDb newMnyDb = FileUtils.openSampleDb();
                            propertyChangeSupport.firePropertyChange(JythonConsole.PROPERTY_MNY_DB, null, newMnyDb);
                        } catch (IOException e) {
                            LOGGER.error(e, e);
                        }
                    }
                };
                Thread t = new Thread(command);
                t.start();
            }
        });
        fileMenu.add(menuItem);

        fileMenu.addSeparator();

        JMenuItem exitMenuItem = new JMenuItem("Exit");
        exitMenuItem.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                LOGGER.info("User selects File -> Exit");
                System.exit(0);
            }
        });
        fileMenu.add(exitMenuItem);
    }

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        JythonConsoleEvents events = JythonConsoleEvents.fromString(evt.getPropertyName());
        switch (events) {
        case NEW_MNY_DB:
            Object newValue = evt.getNewValue();
            if (newValue instanceof MnyDb) {
                MnyDb mnyDb = (MnyDb) newValue;
                if (this.getMnyDb() != null) {
                    try {
                        this.getMnyDb().close();
                    } catch (IOException e) {
                        LOGGER.warn(e);
                    }
                }

                this.setMnyDb(mnyDb);
                SwingUtilities.invokeLater(new Runnable() {
                    @Override
                    public void run() {
                        getFrame().setTitle(getMnyDb().getDbFile().getName());
                    }
                });
            }
            break;
        case UNKNOWN:
            break;
        default:
            break;
        }
    }

    private JFrame getFrame() {
        return frame;
    }

    private void setFrame(JFrame frame) {
        this.frame = frame;
    }

    private MnyDb getMnyDb() {
        return mnyDb;
    }

    private void setMnyDb(MnyDb mnyDb) {
        this.mnyDb = mnyDb;
    }

}

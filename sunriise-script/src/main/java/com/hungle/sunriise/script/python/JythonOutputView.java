package com.hungle.sunriise.script.python;

import java.awt.BorderLayout;
import java.awt.Color;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;

import javax.swing.JPanel;
import javax.swing.SwingUtilities;

import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.Logger;
import org.fife.ui.rsyntaxtextarea.RSyntaxTextArea;
import org.fife.ui.rtextarea.RTextScrollPane;

public class JythonOutputView extends JPanel implements PropertyChangeListener {
    static final String OPENING_FILE = "openingFile";

    private static final Logger LOGGER = org.apache.logging.log4j.LogManager.getLogger(JythonOutputView.class);

    private RSyntaxTextArea textArea;
    private PropertyChangeSupport propertyChangeSupport;

    public JythonOutputView(PropertyChangeSupport propertyChangeSupport) {
        super();
        this.propertyChangeSupport = propertyChangeSupport;
        this.propertyChangeSupport.addPropertyChangeListener(this);

        setLayout(new BorderLayout());

        textArea = new RSyntaxTextArea();
        // textArea.setSyntaxEditingStyle(SyntaxConstants.SYNTAX_STYLE_PYTHON);
        RTextScrollPane sp = new RTextScrollPane(textArea);
        textArea.setEditable(true);
        add(sp, BorderLayout.CENTER);

        MessageConsole messageConsole = new MessageConsole(textArea);
        messageConsole.redirectOut();
        messageConsole.redirectErr(Color.RED, null);
        messageConsole.setMessageLines(1000);
    }

    public void setText(String string) {
        textArea.setText(string);
    }

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        LOGGER.info("propertyChange, evt=" + evt);
        if (evt == null) {
            return;
        }

        String propertyName = evt.getPropertyName();
        if (StringUtils.isEmpty(propertyName)) {
            return;
        }

        if (propertyName.compareToIgnoreCase(JythonOutputView.OPENING_FILE) == 0) {
            String str = (String) evt.getNewValue();
            Runnable doRun = new Runnable() {

                @Override
                public void run() {
                    textArea.append("\nOpening file " + str);
                    textArea.append("\nPlease wait ...");
                }
            };
            SwingUtilities.invokeLater(doRun);
        }
    }
}

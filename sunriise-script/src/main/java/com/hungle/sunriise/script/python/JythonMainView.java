package com.hungle.sunriise.script.python;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.IOException;
import java.io.InputStream;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.atomic.AtomicInteger;

import javax.swing.AbstractAction;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.SwingUtilities;

import org.apache.logging.log4j.Logger;
import org.python.core.PyCode;
import org.python.util.PythonInterpreter;

import com.hungle.sunriise.dbutil.TableAccountUtils;
import com.hungle.sunriise.io.MnyDb;
import com.hungle.sunriise.mnyobject.Account;
import com.hungle.sunriise.mnyobject.MnyContext;
import com.hungle.sunriise.util.MnyContextUtils;

public class JythonMainView extends JPanel implements PropertyChangeListener {
    private static final Logger LOGGER = org.apache.logging.log4j.LogManager.getLogger(JythonMainView.class);

    private static final String DEFAULT_SCRIPT_NAME = "Script-";

    private final PropertyChangeSupport propertyChangeSupport;

    private JButton runButton;

    private AtomicInteger tabCounter = new AtomicInteger();

    private JTabbedPane jTabbedPane;

    private final JythonExecutor jythonExecutor;

    private ExecutorService threadPool;

    public JythonMainView(PropertyChangeSupport propertyChangeSupport, ExecutorService threadPool) {
        super();

        this.propertyChangeSupport = propertyChangeSupport;
        this.propertyChangeSupport.addPropertyChangeListener(this);
        this.threadPool = threadPool;

        this.jythonExecutor = JythonExecutor.getInstance();

        initialize();
    }

    private void initialize() {
        setLayout(new BorderLayout());

        jTabbedPane = new JTabbedPane();
        add(jTabbedPane, BorderLayout.CENTER);

        JPanel commandView = new JPanel();
        commandView.setLayout(new BoxLayout(commandView, BoxLayout.LINE_AXIS));
        commandView.setBorder(BorderFactory.createEmptyBorder(5, 5, 0, 0));
        commandView.add(Box.createHorizontalGlue());
        runButton = new JButton(new AbstractAction("Run") {

            @Override
            public void actionPerformed(ActionEvent e) {
                DefaultScriptView scriptView = (DefaultScriptView) jTabbedPane.getSelectedComponent();
                if (scriptView == null) {
                    return;
                }

                final String text = scriptView.getText();
                Runnable command = new Runnable() {

                    @Override
                    public void run() {
                        try {
                            PythonInterpreter pythonInterpreter = jythonExecutor.getPythonInterpreter();
                            PyCode pyCode = pythonInterpreter.compile(text);
                            if (pyCode != null) {
                                pythonInterpreter.exec(pyCode);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                };
                threadPool.execute(command);
            }
        });
        runButton.setEnabled(false);
        commandView.add(runButton);

        JButton addButton = new JButton(new AbstractAction("New Tab") {

            @Override
            public void actionPerformed(ActionEvent event) {
                DefaultScriptView scriptView = new DefaultScriptView();
                jTabbedPane.add(DEFAULT_SCRIPT_NAME + tabCounter.incrementAndGet(), scriptView);
                if (jTabbedPane.getTabCount() == 1) {
                    String resourceName = "default.py";
                    InputStream stream = JythonMainView.this.getClass().getResourceAsStream(resourceName);
                    // populate with sample python file
                    if (stream != null) {
                        try {
                            scriptView.setContentFromStream(stream);
                        } catch (IOException e) {
                            LOGGER.error(e);
                        }
                    } else {
                        LOGGER.warn("Cannot find resource=" + resourceName);
                    }
                }

            }
        });
        commandView.add(addButton);

        commandView.add(Box.createHorizontalGlue());

        add(commandView, BorderLayout.SOUTH);
    }

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        JythonConsoleEvents events = JythonConsoleEvents.fromString(evt.getPropertyName());
        switch (events) {
        case NEW_MNY_DB:
            handleNewMnyDbEvent(evt);
            break;
        case UNKNOWN:
            break;
        default:
            break;
        }
    }

    protected void handleNewMnyDbEvent(PropertyChangeEvent evt) {
        Object newValue = evt.getNewValue();
        if (newValue instanceof MnyDb) {
            MnyDb mnyDb = (MnyDb) newValue;
            handleNewMnyDbEvent(mnyDb);
        }
    }

    protected void handleNewMnyDbEvent(MnyDb mnyDb) {
        try {
            MnyContext mnyContext = MnyContextUtils.createMnyContext(mnyDb);
            TableAccountUtils.populateAccounts(mnyContext);

            PythonInterpreter pythonInterpreter = jythonExecutor.getPythonInterpreter();
            pythonInterpreter.set("mnyContext", mnyContext);
            Map<Integer, Account> accounts = mnyContext.getAccounts();
            pythonInterpreter.set("accounts", accounts.values());
        } catch (IOException e) {
            LOGGER.error(e, e);
        }

        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                runButton.setEnabled(true);
                // to console
                System.out.println("READY");
            }
        });
    }
}

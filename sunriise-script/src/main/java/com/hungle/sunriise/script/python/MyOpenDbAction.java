package com.hungle.sunriise.script.python;

import java.awt.Component;
import java.beans.PropertyChangeSupport;
import java.util.prefs.Preferences;

import org.apache.logging.log4j.Logger;

import com.hungle.sunriise.io.MnyDb;
import com.hungle.sunriise.viewer.open.OpenDbAction;
import com.hungle.sunriise.viewer.open.OpenDbDialog;

public class MyOpenDbAction extends OpenDbAction {
    private static final Logger LOGGER = org.apache.logging.log4j.LogManager.getLogger(MyOpenDbAction.class);

    private final PropertyChangeSupport propertyChangeSupport;

    public MyOpenDbAction(Component locationRelativeTo, PropertyChangeSupport propertyChangeSupport, Preferences prefs,
            MnyDb mnyDb) {
        super(locationRelativeTo, prefs, mnyDb);
        this.propertyChangeSupport = propertyChangeSupport;
        setDisableReadOnlyCheckBox(true);
    }

    @Override
    public void dbFileOpened(MnyDb newMnyDb, OpenDbDialog dialog) {
        String propertyName = JythonConsole.PROPERTY_MNY_DB;

        MnyDb oldValue = null;
        MnyDb newValue = newMnyDb;
        LOGGER.info("FIRE - propertyName=" + propertyName + ", old=" + oldValue + ", new=" + newValue);
        propertyChangeSupport.firePropertyChange(propertyName, oldValue, newValue);
    }

}

/*******************************************************************************
 * Copyright (c) 2016 Hung Le
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *******************************************************************************/
package com.hungle.sunriise.script.js;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Map;

import javax.script.Invocable;
import javax.script.ScriptEngine;
import javax.script.ScriptException;

import com.hungle.sunriise.dbutil.TableAccountUtils;
import com.hungle.sunriise.io.MnyDb;
import com.hungle.sunriise.mnyobject.Account;
import com.hungle.sunriise.mnyobject.MnyContext;
import com.hungle.sunriise.mnyobject.Transaction;
import com.hungle.sunriise.util.MnyContextUtils;

/**
 * The Class ScriptRunner.
 */
public class JsScriptRunner {

    private static final String FUNCTION_INIT_CONFIGURATION = "initConfiguration";
    private static final String FUNCTION_INIT = "init";
    private static final String FUNCTION_VISIT_ACCOUNT = "visitAccount";
    private static final String FUNCTION_VISIT_TRANSACTION = "visitTransaction";
    private static final String FUNCTION_DESTROY = "destroy";
    private ScriptEngine engine;
    private Invocable runner;
    private String fileName;
    private String password;
    private Map<?, ?> configuration;
    private MnyContext mnyContext;

    public JsScriptRunner(String resourceName) throws IOException {
        this.engine = JsScriptUtils.getScriptEngine(this, resourceName);
        this.runner = (Invocable) engine;

    }

    public JsScriptRunner() throws IOException {
        this(null);
    }

    public void sourceScript(String resourceName) throws IOException {
        JsScriptUtils.eval(engine, this, resourceName);

        initConfiguration();
    }

    private void initConfiguration() throws IOException {
        try {
            String functionName = FUNCTION_INIT_CONFIGURATION;
            configuration = (Map<?, ?>) runner.invokeFunction(functionName);
            if (configuration == null) {
                throw new IOException("Cannot find function=");
            }
            this.fileName = (String) configuration.get("fileName");
            // Assert.assertNotNull(fileName);
            this.password = (String) configuration.get("password");
            if (password != null) {
                password.trim();
                if (password.length() <= 0) {
                    password = null;
                }
            }
        } catch (NoSuchMethodException | ScriptException e) {
            throw new IOException(e);
        }
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPassword() {
        return password;
    }

    public MnyContext getMnyContext() {
        return mnyContext;
    }

    public void setMnyContext(MnyContext mnyContext) {
        this.mnyContext = mnyContext;
    }

    public void doVisit() throws IOException {
        doVisit(getFileName(), getPassword());
    }

    private void doVisit(String fileName, String password) throws IOException {
        MnyDb mnyDb = null;
        try {
            mnyDb = new MnyDb(new File(fileName), password);
            MnyContext context = MnyContextUtils.createMnyContext(mnyDb);
            setMnyContext(context);

            scriptInit(this);

            for (Account account : context.getAccounts().values()) {
                scriptVisitAccount(mnyDb, account);
            }
        } catch (NoSuchMethodException e) {
            throw new IOException(e);
        } catch (ScriptException e) {
            throw new IOException(e);
        } finally {
            if (mnyDb != null) {
                mnyDb.close();
            }

            scriptDestroy();
        }
    }

    private void scriptInit(JsScriptRunner scriptRunner) throws ScriptException, NoSuchMethodException {
        String functionName = FUNCTION_INIT;
        runner.invokeFunction(functionName, scriptRunner);
    }

    private void scriptVisitAccount(MnyDb mnyDb, Account account) throws IOException {
        String functionName = FUNCTION_VISIT_ACCOUNT;
        Boolean result = Boolean.FALSE;
        try {
            result = (Boolean) runner.invokeFunction(functionName, account);
            if (isTrue(result)) {
                TableAccountUtils.addTransactionsToAccount(mnyDb.getDb(), account, mnyContext);

                scriptVisitTransactions(account, account.getTransactions());
            }
        } catch (NoSuchMethodException | ScriptException e) {
            throw new IOException(e);
        }
    }

    protected boolean isTrue(Boolean result) {
        return (result != null) && (result == Boolean.TRUE);
    }

    private void scriptVisitTransactions(Account account, List<Transaction> transactions)
            throws ScriptException, NoSuchMethodException {
        String functionName = FUNCTION_VISIT_TRANSACTION;
        Boolean result = Boolean.FALSE;
        for (Transaction transaction : transactions) {
            result = (Boolean) runner.invokeFunction(functionName, account, transaction);
            if (!isTrue(result)) {
                break;
            }
        }
    }

    private void scriptDestroy() throws IOException {
        String functionName = FUNCTION_DESTROY;
        try {
            runner.invokeFunction(functionName);
        } catch (NoSuchMethodException | ScriptException e) {
            throw new IOException(e);
        }
    }
}

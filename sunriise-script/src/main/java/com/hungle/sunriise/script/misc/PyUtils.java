package com.hungle.sunriise.script.misc;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.logging.log4j.Logger;
import org.python.core.Py;
import org.python.core.PyDictionary;
import org.python.core.PyList;
import org.python.core.PyObject;
import org.python.core.PyTuple;
import org.python.util.PythonInterpreter;

// TODO: Auto-generated Javadoc
/**
 * The Class PyUtils.
 */
public class PyUtils {

    /** The Constant LOGGER. */
    private static final Logger LOGGER = org.apache.logging.log4j.LogManager.getLogger(PyUtils.class);

    /**
     * Extract a Python tuple or array into a Java List (which can be converted into
     * other kinds of lists and sets inside Java).
     *
     * @param interp The Python interpreter object
     * @param pyName The id of the python list object
     * @return the list
     */
    public static List toList(PythonInterpreter interp, String pyName) {
        return new ArrayList(Arrays.asList(interp.get(pyName, Object[].class)));
    }

    /**
     * Extract a Python dictionary into a Java Map.
     *
     * @param interp The Python interpreter object
     * @param pyName The id of the python dictionary
     * @return the map
     */
    public static Map toMap(PythonInterpreter interp, String pyName) {
        PyList pa = ((PyDictionary) interp.get(pyName)).items();
        Map map = new HashMap();
        while (pa.__len__() != 0) {
            PyTuple po = (PyTuple) pa.pop();
            Object first = po.__finditem__(0).__tojava__(Object.class);
            Object second = po.__finditem__(1).__tojava__(Object.class);
            map.put(first, second);
        }
        return map;
    }

    /**
     * Turn a Java Map into a PyDictionary, suitable for placing into a
     * PythonInterpreter.
     *
     * @param <V> the value type
     * @param <K> the key type
     * @param map The Java Map object
     * @return the py dictionary
     */
    public static <V, K> PyDictionary toPyDictionary(Map<K, V> map) {
        Map<PyObject, PyObject> m = new HashMap<PyObject, PyObject>();
        Iterator<Entry<K, V>> it = map.entrySet().iterator();
        while (it.hasNext()) {
            Entry<K, V> e = it.next();
            PyObject key = Py.java2py(e.getKey());
            LOGGER.info("key=" + key);
            PyObject value = Py.java2py(e.getValue());
            LOGGER.info("value=" + value);
            m.put(key, value);
        }
        return new PyDictionary(m);
    }
}

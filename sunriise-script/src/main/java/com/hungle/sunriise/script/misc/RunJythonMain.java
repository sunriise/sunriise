package com.hungle.sunriise.script.misc;

import java.io.File;
import java.io.IOException;
import java.util.Map;

import org.apache.logging.log4j.Logger;
import org.python.util.PythonInterpreter;

import com.hungle.sunriise.dbutil.TableAccountUtils;
import com.hungle.sunriise.io.MnyDb;
import com.hungle.sunriise.mnyobject.Account;
import com.hungle.sunriise.mnyobject.MnyContext;
import com.hungle.sunriise.script.python.JythonUtils;
import com.hungle.sunriise.util.MnyContextUtils;

public class RunJythonMain {
    private static final Logger LOGGER = org.apache.logging.log4j.LogManager.getLogger(RunJythonMain.class);

    public static void main(String[] args) {
        String dbFileName = null;
        String dbPassword = null;
        String scriptName = null;

        if (args.length == 2) {
            scriptName = args[0];
            dbFileName = args[1];
            dbPassword = null;
        } else if (args.length == 3) {
            scriptName = args[0];
            dbFileName = args[1];
            dbPassword = args[2];
        } else {
            Class<RunJythonMain> clz = RunJythonMain.class;
            System.err.println("Usage: java " + clz.getName() + " scriptName.py");
            System.exit(1);
        }

        MnyDb mnyDb = null;
        try {
            mnyDb = new MnyDb(new File(dbFileName), dbPassword);
            MnyContext mnyContext = MnyContextUtils.createMnyContext(mnyDb);
            TableAccountUtils.populateAccounts(mnyContext);

            PythonInterpreter instance = JythonUtils.getInstance();
            instance.set("mnyContext", mnyContext);
            Map<Integer, Account> accounts = mnyContext.getAccounts();
            instance.set("accounts", accounts.values());

            instance.execfile(scriptName);
        } catch (IOException e) {
            LOGGER.error(e, e);
        } finally {
            if (mnyDb != null) {
                try {
                    mnyDb.close();
                } catch (IOException e) {
                    LOGGER.warn(e);
                }
            }
        }
    }
}

package com.hungle.sunriise.script.misc;

import java.io.IOException;
import java.io.InputStream;
import java.util.Map;

import org.apache.logging.log4j.Logger;
import org.python.core.PyString;
import org.python.core.PySystemState;
import org.python.util.PythonInterpreter;

import com.hungle.sunriise.dbutil.TableAccountUtils;
import com.hungle.sunriise.io.FileUtils;
import com.hungle.sunriise.io.MnyDb;
import com.hungle.sunriise.io.sample.MnySampleFile;
import com.hungle.sunriise.mnyobject.Account;
import com.hungle.sunriise.mnyobject.MnyContext;
import com.hungle.sunriise.util.MnyContextUtils;

public class JythonMain {
    private static final Logger LOGGER = org.apache.logging.log4j.LogManager.getLogger(JythonMain.class);

    public static void main(String[] args) throws IOException {
        PythonInterpreter interpr = new PythonInterpreter();

        // Properties p = new Properties();
        // p.setProperty("python.path", "PATH OF JYTHON");
        // p.setProperty("python.home", "PATH OF JYTHON");
        // p.setProperty("python.prefix", "PATH OF JYTHON");
        // PythonInterpreter.initialize(System.getProperties(), p, new String[]
        // {});
        // interpreter = new PythonInterpreter();
        interpr = new PythonInterpreter(null, new PySystemState());
        PySystemState sys = interpr.getSystemState();
        String rootPath = JythonMain.class.getProtectionDomain().getCodeSource().getLocation().getPath();
        // System.out.println(rootPath);
        sys.path.append(new PyString(rootPath));
        // sys.setdefaultencoding("utf8");
        // interpr.exec("import sys");
        // interpr.exec("sys.setdefaultencoding(\"utf8\")");
        // interpr.exec("# coding=utf-8");

        MnySampleFile mnyTestFile = MnySampleFile.getMnySampleFile(MnySampleFile.SUNSET_SAMPLE_PWD_MNY);
        MnyDb mnyDb = null;
        try {
            mnyDb = MnySampleFile.openSampleFileFromModuleProject(mnyTestFile);

            MnyContext mnyContext = MnyContextUtils.createMnyContext(mnyDb);
            TableAccountUtils.populateAccounts(mnyContext);

            interpr.set("mnyContext", mnyContext);
            Map<Integer, Account> accounts = mnyContext.getAccounts();
            interpr.set("accounts", accounts.values());

            JythonMain main = new JythonMain();
            String resourceName = "test02.py";
            try (InputStream inputStream = FileUtils.getResourceAsStream(main, resourceName)) {
                interpr.execfile(inputStream);
            }
            // interpr.exec("print(accounts.size())");
            // interpr.exec("for k in accounts : print repr(k),
            // repr(accounts[k])");
        } finally {
            if (mnyDb != null) {
                mnyDb.close();
            }
        }
    }
}

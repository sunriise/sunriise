package com.hungle.sunriise.script.python;

public enum JythonConsoleEvents {
    NEW_MNY_DB(JythonConsole.PROPERTY_MNY_DB), UNKNOWN("Unknown");

    private final String stringValue;

    JythonConsoleEvents(final String s) {
        stringValue = s;
    }

    public String toString() {
        return stringValue;
    }

    public static JythonConsoleEvents fromString(String propertyName) {
        if (propertyName == null) {
            return JythonConsoleEvents.UNKNOWN;
        }

        if (propertyName.compareToIgnoreCase(JythonConsole.PROPERTY_MNY_DB) == 0) {
            return JythonConsoleEvents.NEW_MNY_DB;
        } else {
            return JythonConsoleEvents.UNKNOWN;
        }
    }

}

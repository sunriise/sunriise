package com.hungle.sunriise.script.python;

import java.awt.BorderLayout;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import javax.swing.JPanel;

import org.fife.ui.rsyntaxtextarea.RSyntaxTextArea;
import org.fife.ui.rsyntaxtextarea.SyntaxConstants;
import org.fife.ui.rtextarea.RTextScrollPane;

public class DefaultScriptView extends JPanel {

    private RSyntaxTextArea textArea;

    public DefaultScriptView() {
        super();
        initialize();
    }

    private void initialize() {
        setLayout(new BorderLayout());

        textArea = new RSyntaxTextArea();
        textArea.setSyntaxEditingStyle(SyntaxConstants.SYNTAX_STYLE_PYTHON);
        RTextScrollPane sp = new RTextScrollPane(textArea);
        add(sp, BorderLayout.CENTER);
    }

    public String getText() {
        return textArea.getText();
    }

    public void setContentFromStream(InputStream stream) throws IOException {
        textArea.setText("");
        textArea.read(new InputStreamReader(stream), null);
    }
}

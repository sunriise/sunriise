package com.hungle.sunriise.script.js;

import java.io.IOException;

import org.apache.logging.log4j.Logger;

public class JsScriptRunnerMain {
    private static final Logger LOGGER = org.apache.logging.log4j.LogManager.getLogger(JsScriptRunnerMain.class);

    public static void main(String[] args) {
        String resourceName = null;

        JsScriptRunner scriptRunner = null;
        try {
            scriptRunner = new JsScriptRunner();
            scriptRunner.sourceScript(resourceName);
            scriptRunner.doVisit();
        } catch (IOException e) {
            LOGGER.error(e, e);
        }
    }

}

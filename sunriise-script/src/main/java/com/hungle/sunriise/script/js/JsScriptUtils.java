package com.hungle.sunriise.script.js;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.nio.charset.Charset;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;

import org.apache.logging.log4j.Logger;

import com.hungle.sunriise.io.FileUtils;

public class JsScriptUtils {
    private static final Logger LOGGER = org.apache.logging.log4j.LogManager.getLogger(JsScriptUtils.class);

    static final String NASHORN = "nashorn";

    public static ScriptEngine getScriptEngine(Object object, String resourceName) throws IOException {
        ScriptEngine engine = new ScriptEngineManager().getEngineByName(JsScriptUtils.NASHORN);

        if (resourceName != null) {
            eval(engine, object, resourceName);
        }

        return engine;
    }

    public static void eval(ScriptEngine engine, Object object, String resourceName) throws IOException {
        InputStream stream = FileUtils.getResourceAsStream(object, resourceName);
        eval(engine, stream);
    }

    private static void eval(ScriptEngine engine, InputStream stream) throws IOException {
        Reader reader = null;
        try {
            reader = new BufferedReader(new InputStreamReader(stream, Charset.forName("UTF-8")));

            try {
                engine.eval(reader);
            } catch (ScriptException e) {
                throw new IOException(e);
            }
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                    LOGGER.warn(e);
                } finally {
                    reader = null;
                }
            }
        }

    }

}

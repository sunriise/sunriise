package com.hungle.sunriise.script.python;

import java.awt.Dimension;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class DefaultStatusView extends JPanel {
    private JLabel status;

    public DefaultStatusView() {
        super();
        initialize();
    }

    private void initialize() {
        setLayout(new BoxLayout(this, BoxLayout.LINE_AXIS));
        setBorder(BorderFactory.createEmptyBorder(5, 5, 0, 0));
        add(Box.createRigidArea(new Dimension(5, 0)));

        JLabel label = new JLabel();
        label.setText("Status: ");
        add(label);

        status = new JLabel();
        add(status);
    }
}

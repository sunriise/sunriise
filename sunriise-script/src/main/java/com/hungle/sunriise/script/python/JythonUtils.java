package com.hungle.sunriise.script.python;

import java.util.Properties;

import org.python.core.PyObject;
import org.python.core.PyString;
import org.python.core.PySystemState;
import org.python.util.PythonInterpreter;

public class JythonUtils {
    private static final PythonInterpreter INSTANCE = createInstance();

    public static PythonInterpreter getInstance() {
        return INSTANCE;
    }

    private static PythonInterpreter createInstance() {
        // PythonInterpreter interpr = new PythonInterpreter();

        Properties preProperties = System.getProperties();

        Properties postProperties = new Properties();
        // postProperties.setProperty("python.path", "PATH OF JYTHON");
        // postProperties.setProperty("python.home", "PATH OF JYTHON");
        // postProperties.setProperty("python.prefix", "PATH OF JYTHON");

        String[] jythonArgv = new String[] {};

        PythonInterpreter.initialize(preProperties, postProperties, jythonArgv);

        PyObject dict = null;
        PythonInterpreter interpr = new PythonInterpreter(dict, new PySystemState());

        PySystemState sys = interpr.getSystemState();
        String rootPath = JythonUtils.class.getProtectionDomain().getCodeSource().getLocation().getPath();

        // System.out.println(rootPath);
        sys.path.append(new PyString(rootPath));

        interpr.exec("import sys");
        interpr.exec("from org.python.core import codecs");
        interpr.exec("codecs.setDefaultEncoding('utf-8')");

        interpr.exec("print 'Jython', sys.version");

        return interpr;
    }
}

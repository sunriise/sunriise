package com.hungle.sunriise.script.python;

import java.util.Properties;

import org.apache.logging.log4j.Logger;
import org.python.core.PyObject;
import org.python.core.PyString;
import org.python.core.PySystemState;
import org.python.util.PythonInterpreter;

public class JythonExecutor {
    private static final Logger LOGGER = org.apache.logging.log4j.LogManager.getLogger(JythonExecutor.class);

    private static final JythonExecutor instance = JythonExecutor.createIntance();

    private PythonInterpreter pythonInterpreter;

    private JythonExecutor() {
        super();
    }

    protected PythonInterpreter createPythonInterpreter() {
        // interpreter = new PythonInterpreter();
        PyObject dict = null;
        PythonInterpreter pythonInterpreter = new PythonInterpreter(dict, new PySystemState());
        PySystemState sys = pythonInterpreter.getSystemState();
        String rootPath = JythonExecutor.class.getProtectionDomain().getCodeSource().getLocation().getPath();

        // System.out.println(rootPath);
        sys.path.append(new PyString(rootPath));
        // sys.setdefaultencoding("utf8");
        pythonInterpreter.exec("import sys");
        // interpr.exec("sys.setdefaultencoding(\"utf8\")");
        // interpr.exec("# coding=utf-8");
        pythonInterpreter.exec("from org.python.core import codecs");
        pythonInterpreter.exec("codecs.setDefaultEncoding('utf-8')");

        pythonInterpreter.exec("print 'Jython', sys.version");
        return pythonInterpreter;
    }

    public final synchronized PythonInterpreter getPythonInterpreter() {
        if (pythonInterpreter == null) {
            pythonInterpreter = createPythonInterpreter();
        }
        return pythonInterpreter;
    }

    public static final JythonExecutor getInstance() {
        return instance;
    }

    private static JythonExecutor createIntance() {
        JythonExecutor instance = null;
        LOGGER.info("> JYTHON_INITIALIZE - initialize ...");
        try {
            Properties postProperties = new Properties();
            // p.setProperty("python.path", "PATH OF JYTHON");
            // p.setProperty("python.home", "PATH OF JYTHON");
            // p.setProperty("python.prefix", "PATH OF JYTHON");

            Properties preProperties = System.getProperties();

            String[] jythonArgv = new String[] {};
            PythonInterpreter.initialize(preProperties, postProperties, jythonArgv);

            instance = new JythonExecutor();
        } finally {
            LOGGER.info("< JYTHON_INITIALIZE - initialize DONE ...");
        }

        return instance;
    }
}

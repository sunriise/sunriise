package com.hungle.sunriise.ucanaccess;

import java.sql.Connection;
import java.sql.Driver;
import java.sql.DriverPropertyInfo;
import java.sql.SQLException;
import java.sql.SQLFeatureNotSupportedException;
import java.util.Properties;
import java.util.logging.Logger;

import net.ucanaccess.jdbc.UcanaccessDriver;

public class MnyDriver implements Driver {

    private final UcanaccessDriver delegate;

    public MnyDriver() {
        super();
        this.delegate = new UcanaccessDriver();
    }

    @Override
    public Connection connect(String url, Properties info) throws SQLException {
        String jackcessOpener = JackcessEncryptOpener.class.getCanonicalName();
        info.setProperty("jackcessOpener", jackcessOpener);

        return delegate.connect(url, info);
    }

    @Override
    public boolean acceptsURL(String url) throws SQLException {
        return delegate.acceptsURL(url);
    }

    @Override
    public DriverPropertyInfo[] getPropertyInfo(String url, Properties info) throws SQLException {
        return delegate.getPropertyInfo(url, info);
    }

    @Override
    public int getMajorVersion() {
        return delegate.getMajorVersion();
    }

    @Override
    public int getMinorVersion() {
        return delegate.getMinorVersion();
    }

    @Override
    public boolean jdbcCompliant() {
        return delegate.jdbcCompliant();
    }

    @Override
    public Logger getParentLogger() throws SQLFeatureNotSupportedException {
        return delegate.getParentLogger();
    }

}

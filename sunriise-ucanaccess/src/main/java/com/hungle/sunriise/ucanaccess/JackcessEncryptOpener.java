package com.hungle.sunriise.ucanaccess;

import java.io.File;
import java.io.IOException;

import com.healthmarketscience.jackcess.CryptCodecProvider;
import com.healthmarketscience.jackcess.Database;
import com.healthmarketscience.jackcess.DatabaseBuilder;
import com.healthmarketscience.jackcess.DateTimeType;

import net.ucanaccess.jdbc.JackcessOpenerInterface;

public final class JackcessEncryptOpener implements JackcessOpenerInterface {
    public static final String NET_UCANACCESS_JDBC_UCANACCESS_DRIVER = "net.ucanaccess.jdbc.UcanaccessDriver";

    @Override
    public Database open(File file, String password) throws IOException {
        DatabaseBuilder dbd = new DatabaseBuilder(file);
//        boolean autoSync = false;
        boolean autoSync = true;
        dbd.setAutoSync(autoSync);
        dbd.setCodecProvider(new CryptCodecProvider(password));
        dbd.setReadOnly(true);
        Database db = dbd.open();
        if (db != null) {
            DateTimeType dateTimeType = DateTimeType.DATE;
            db.setDateTimeType(dateTimeType);
        }
        return db;
    }
    // Notice that the parameter setting autosync =true is recommended with
    // UCanAccess for performance reasons.
    // UCanAccess flushes the updates to disk at transaction end.
    // For more details about autosync parameter (and related tradeoff), see
    // the Jackcess documentation.
}
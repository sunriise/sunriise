package com.hungle.sunriise.ucanaccess;

import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;

import org.apache.logging.log4j.Logger;
import org.junit.Assert;
import org.junit.Test;

import com.hungle.sunriise.io.sample.MnySampleFile;
import com.hungle.sunriise.util.StopWatch;

// TODO: Auto-generated Javadoc
/**
 * The Class ConnectionTest.
 */
public class ConnectionTest {

    /** The Constant LOGGER. */
    private static final Logger LOGGER = org.apache.logging.log4j.LogManager.getLogger(ConnectionTest.class);

    /**
     * Test connect.
     *
     * @throws ClassNotFoundException the class not found exception
     * @throws SQLException           the SQL exception
     */
    @Test
    public void testConnect() throws ClassNotFoundException, SQLException {
        Class<?> clz = Class.forName(JackcessEncryptOpener.NET_UCANACCESS_JDBC_UCANACCESS_DRIVER);
        LOGGER.info("JDBC clz.name=" + clz.getCanonicalName());

        for (MnySampleFile dbFile : MnySampleFile.SAMPLE_FILES) {
            if (dbFile.isBackup()) {
                continue;
            }

            testConnectAndLogTiming(dbFile);
        }
    }

    /**
     * Test large file.
     *
     * @throws SQLException the SQL exception
     * @throws IOException
     */
    @Test
    public void testLargeFile() throws SQLException, IOException {
        String fileName = MnySampleFile.LOCAL_LARGE_FILE_MNY;
        File file = new File(fileName);
        file = file.getAbsoluteFile().getCanonicalFile();
        if (file.exists()) {
            fileName = file.getAbsolutePath();
            MnySampleFile mnyTestFile = new MnySampleFile(fileName);
            testConnectAndLogTiming(mnyTestFile);
        }
    }

    /**
     * Test connect and log timing.
     *
     * @param mnyTestFile the mny test file
     * @throws SQLException the SQL exception
     */
    private void testConnectAndLogTiming(MnySampleFile mnyTestFile) throws SQLException {
        StopWatch stopWatch = new StopWatch();
        Connection connection = null;
        try {
            connection = testConnect(mnyTestFile);
            stopWatch.logTiming(LOGGER, "CONNECT - " + mnyTestFile.getFileName());

            if (connection != null) {
                testQuery(connection);
            }
            stopWatch.logTiming(LOGGER, "QUERY - " + mnyTestFile.getFileName());

        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        } finally {
            if (connection != null) {
                connection.close();
            }
        }
    }

    /**
     * Test connect.
     *
     * @param mnyTestFile the mny test file
     * @return the connection
     * @throws SQLException the SQL exception
     */
    private Connection testConnect(MnySampleFile mnyTestFile) throws SQLException {
        String jdbcUrl = getJdbcUrl(mnyTestFile);
        String userName = "sa";
        String password = mnyTestFile.getPassword();

        Connection connection = DriverManager.getConnection(jdbcUrl, userName, password);
        Assert.assertNotNull(connection);

        return connection;
    }

    /**
     * Test query.
     *
     * @param connection the connection
     * @throws SQLException the SQL exception
     */
    private void testQuery(Connection connection) throws SQLException {
        Statement statement = connection.createStatement();
        String tableName = "trn";
        ResultSet resultSet = statement.executeQuery("select * from " + tableName);
        Assert.assertNotNull(resultSet);

        ResultSetMetaData metaData = resultSet.getMetaData();
        while (resultSet.next()) {
            int columnCount = metaData.getColumnCount();
            for (int columnIndex = 0; columnIndex < columnCount; columnIndex++) {
                resultSet.getObject(columnIndex + 1);
            }
        }
    }

    /**
     * Gets the jdbc url.
     *
     * @param mnyTestFile the mny test file
     * @return the jdbc url
     */
    private static final String getJdbcUrl(MnySampleFile mnyTestFile) {
        File file = MnySampleFile.getSampleFileFromModuleProject(mnyTestFile);
        String path = file.getAbsolutePath();
        return getJdbcUrl(path);
    }

    /**
     * Gets the jdbc url.
     *
     * @param path the path
     * @return the jdbc url
     */
    private static final String getJdbcUrl(String path) {
        LOGGER.info("path=" + path);
        String jackcessOpener = JackcessEncryptOpener.class.getCanonicalName();
        LOGGER.info("jackcessOpener=" + jackcessOpener);
        String jdbcUrl = "jdbc:ucanaccess://" + path + ";" + "jackcessOpener=" + jackcessOpener;
        return jdbcUrl;
    }
}

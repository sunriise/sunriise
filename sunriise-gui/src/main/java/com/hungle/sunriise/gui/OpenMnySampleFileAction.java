package com.hungle.sunriise.gui;

import java.awt.event.ActionEvent;
import java.io.File;
import java.io.IOException;

import javax.swing.AbstractAction;

import org.apache.logging.log4j.Logger;

import com.hungle.sunriise.io.MnyDb;
import com.hungle.sunriise.io.sample.MnySampleFile;

/**
 * The Class OpenMnyTestFileAction.
 */
abstract class OpenMnySampleFileAction extends AbstractAction {
    private static final Logger LOGGER = org.apache.logging.log4j.LogManager.getLogger(OpenMnySampleFileAction.class);

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 1L;

    /** The db file. */
    private MnySampleFile sampleFile;

    /**
     * Instantiates a new open mny test file action.
     * 
     * @param sampleFile the db file
     */
    public OpenMnySampleFileAction(MnySampleFile sampleFile) {
        this.sampleFile = sampleFile;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.
     * ActionEvent)
     */
    @Override
    public void actionPerformed(ActionEvent event) {
        File file = MnySampleFile.getSampleFileFromModuleProject(sampleFile);

        MnyDb mnyDb;
        try {
            mnyDb = new MnyDb(file, sampleFile.getPassword());
            mnyDbOpened(mnyDb);
        } catch (IOException e) {
            LOGGER.error(e);
        }
    }

    abstract void mnyDbOpened(MnyDb newMnyDb);
}
package com.hungle.sunriise.gui;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.HeadlessException;
import java.awt.event.ActionEvent;
import java.io.File;
import java.io.IOException;

import javax.swing.AbstractAction;
import javax.swing.BorderFactory;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.TransferHandler;

import org.apache.logging.log4j.Logger;
import org.fife.ui.rsyntaxtextarea.RSyntaxTextArea;
import org.fife.ui.rtextarea.RTextScrollPane;

import com.hungle.sunriise.io.MnyDb;

public abstract class AbstractFileDropFrame extends JFrame {
    private static final Logger LOGGER = org.apache.logging.log4j.LogManager.getLogger(AbstractFileDropFrame.class);

    private final class MyFileDropHandler extends FileDropHandler {
        private final JPanel view;

        private MyFileDropHandler(JPanel view) {
            this.view = view;
        }

        @Override
        public void handleFile(File file) {
            if (!file.isFile()) {
                String message = String.format("'%s' is not a file.", file.getName());
                String title = "File error";
                JOptionPane.showMessageDialog(view, message, title, JOptionPane.ERROR_MESSAGE);
                return;
            }
            String name = file.getName();
            if (!name.endsWith(".mny")) {
                String message = String.format("'%s' is not a *.mny file.", file.getName());
                String title = "File error";
                JOptionPane.showMessageDialog(view, message, title, JOptionPane.ERROR_MESSAGE);
                return;
            }

            try {
                final MnyDb srcDb = openMnyDb(file, view);
                if (srcDb == null) {
                    return;
                }

                handleMnyDb(srcDb, view);

            } catch (Exception e) {
                LOGGER.error(e.getMessage(), e);
                String message = e.getMessage();
                String title = "Exception";
                JOptionPane.showMessageDialog(view, message, title, JOptionPane.ERROR_MESSAGE);
            }
        }

        private MnyDb openMnyDb(File file, Component parentComponent) throws IOException {
            String password = null;
            MnyDb mnyDb = null;

            while (mnyDb == null) {
                try {
                    mnyDb = new MnyDb(file, password);
                } catch (com.healthmarketscience.jackcess.InvalidCredentialsException e) {
                    String text = PasswordPanel.showDialog(parentComponent, "Enter Your Password");
                    if (text == null) {
                        // Cancel
                        LOGGER.warn("User cancel");
                        mnyDb = null;
                        break;
                    } else {
                        password = text;
                        mnyDb = null;
                    }
                }
            }

            return mnyDb;
        }
    }

    public AbstractFileDropFrame(String title) throws HeadlessException {
        super(title);

        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        Dimension preferredSize = new Dimension(400, 400);
        setPreferredSize(preferredSize);

        init(getContentPane());
    }

    public abstract void handleMnyDb(MnyDb mnyDb, JPanel view) throws IOException;

    private void init(Container contentPane) {
        addMenus(contentPane);

        final JPanel view = new JPanel();
        view.setLayout(new BorderLayout());
        view.setBorder(BorderFactory.createEmptyBorder(3, 3, 3, 3));
        contentPane.add(view);

        addMainView(view);
    }

    protected void addMainView(final JPanel view) {
        addCenter(view);
    }

    protected void addCenter(JPanel parent) {
        String eol = "\r\n";
        JPanel view = new JPanel();
        view.setLayout(new BorderLayout());

        parent.add(view, BorderLayout.CENTER);

        RSyntaxTextArea textArea = new RSyntaxTextArea();
        textArea.setTransferHandler(createTransferHandler(view));

        // textArea.setSyntaxEditingStyle(SyntaxConstants.SYNTAX_STYLE_JAVASCRIPT);
        RTextScrollPane sp = new RTextScrollPane(textArea);
        // accountJsonTextArea = textArea;
        textArea.setEditable(false);
        textArea.setLineWrap(true);

        addTextToMainView(textArea, eol);

        view.add(sp, BorderLayout.CENTER);
    }

    protected abstract void addTextToMainView(RSyntaxTextArea textArea, String eol);

    protected JMenuBar addMenus(Container contentPane) {
        JMenuBar menuBar = new JMenuBar();

        JMenu menu = null;
        JMenuItem menuItem = null;

        menu = new JMenu("File");
        menuBar.add(menu);

        menu.addSeparator();

        menuItem = new JMenuItem(new AbstractAction("Exit") {

            @Override
            public void actionPerformed(ActionEvent e) {
                System.exit(0);

            }
        });
        menu.add(menuItem);

        setJMenuBar(menuBar);

        return menuBar;
    }

    protected TransferHandler createTransferHandler(JPanel view) {
        return new MyFileDropHandler(view);
    }

    protected void showMainFrame() {
        setLocation(100, 100);
        pack();
        setVisible(true);
    }

}

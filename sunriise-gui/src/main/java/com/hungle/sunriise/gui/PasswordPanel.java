package com.hungle.sunriise.gui;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;

// TODO: Auto-generated Javadoc
/**
 * Class that creates a panel with a password field. Extension of Adamski's
 * class
 *
 * Author adamski https://stackoverflow.com/users/127479/adamski Author
 * agi-hammerthief https://stackoverflow.com/users/2225787/agi-hammerthief See
 * https://stackoverflow.com/a/8881370/2225787
 */
public class PasswordPanel extends JPanel {

    /** The J field pass. */
    private final JPasswordField password;

    /** The J lbl pass. */
    private JLabel label;

    /** The gained focus before. */
    private boolean gainedFocusBefore;

    /**
     * "Hook" method that causes the JPasswordField to request focus when method is
     * first called.
     */
    public void gainedFocus() {
        if (!gainedFocusBefore) {
            gainedFocusBefore = true;
            password.requestFocusInWindow();
        }
    }

    /**
     * Instantiates a new password panel.
     *
     * @param length the length
     */
    public PasswordPanel(int length) {
        super(new FlowLayout());
        gainedFocusBefore = false;
        if (length > 0) {
            password = new JPasswordField(length);
        } else {
            password = new JPasswordField();

        }
        Dimension d = new Dimension();
        d.setSize(30, 22);
        password.setMinimumSize(d);
        password.setColumns(10);
        label = new JLabel("Password: ");
        add(label);
        add(password);
    }

    /**
     * Instantiates a new password panel.
     */
    public PasswordPanel() {
        this(0);
    }

    /**
     * Gets the password.
     *
     * @return the password
     */
    public char[] getPassword() {
        return password.getPassword();
    }

    /**
     * Gets the password string.
     *
     * @return the password string
     */
    public String getPasswordString() {
        StringBuilder sb = new StringBuilder();

        char[] pwd = this.getPassword();
        if (pwd.length > 0) {
            for (char c : pwd) {
                sb.append(c);
            }
        }

        return sb.toString();
    }

    /**
     * Display dialog.
     *
     * @param parent the parent
     * @param panel  the panel
     * @param title  the title
     * @return the string
     */
    private static String displayDialog(Component parent, final PasswordPanel panel, String title) {
        String password = null;
        /*
         * For some reason, using `JOptionPane(panel, JOptionPane.OK_CANCEL_OPTION,
         * JOptionPane.QUESTION_MESSAGE)` does not give the same results as setting
         * values after creation, which is weird
         */
        JOptionPane op = new JOptionPane(panel);
        op.setMessageType(JOptionPane.QUESTION_MESSAGE);
        op.setOptionType(JOptionPane.OK_CANCEL_OPTION);
        JDialog dialog = op.createDialog(parent, title);
        // Ensure the JPasswordField is able to request focus when the dialog is
        // first shown.
        dialog.addWindowFocusListener(new WindowAdapter() {
            @Override
            public void windowGainedFocus(WindowEvent e) {
                panel.gainedFocus();
            }
        });
        dialog.setDefaultCloseOperation(JOptionPane.OK_OPTION); // necessary?

        dialog.setVisible(true);

        Object value = op.getValue();
        if (null != value && value.equals(JOptionPane.OK_OPTION)) {
            password = panel.getPasswordString();
        }

        return password;
    }

    /**
     * Show dialog.
     *
     * @param parent the parent
     * @param title  the title
     * @return the string
     */
    public static String showDialog(Component parent, String title) {
        final PasswordPanel panel = new PasswordPanel();
        return displayDialog(parent, panel, title);
    }

    /**
     * Show dialog.
     *
     * @param parent         the parent
     * @param title          the title
     * @param passwordLength the password length
     * @return the string
     */
    public static String showDialog(Component parent, String title, int passwordLength) {
        final PasswordPanel panel = new PasswordPanel(passwordLength);

        return displayDialog(parent, panel, title);
    }

    /**
     * Show dialog.
     *
     * @param title the title
     * @return the string
     */
    public static String showDialog(String title) {
        return showDialog(null, title);
    }

    /**
     * Show dialog.
     *
     * @param title          the title
     * @param passwordLength the password length
     * @return the string
     */
    public static String showDialog(String title, int passwordLength) {
        return showDialog(null, title, passwordLength);
    }
}

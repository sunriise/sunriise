package com.hungle.sunriise.gui;

import java.io.File;

import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;

import com.hungle.sunriise.io.MnyDb;
import com.hungle.sunriise.io.sample.MnySampleFile;

public abstract class AbstractSamplesMenu {
    public void addSamplesMenu(JMenuBar menuBar, File sampleTopDir) {
        if (sampleTopDir.exists() && (sampleTopDir.isDirectory())) {
            JMenu sampleMenu = new JMenu("Samples");
            menuBar.add(sampleMenu);

            for (MnySampleFile dbFile : MnySampleFile.SAMPLE_FILES) {
                if (dbFile.isBackup()) {
                    continue;
                }

                File sampleFile = MnySampleFile.getSampleFileFromModuleProject(dbFile);

                String fileName = sampleFile.getName();

                JMenuItem menuItem = new JMenuItem(fileName);
                sampleMenu.add(menuItem);
                OpenMnySampleFileAction action = new OpenMnySampleFileAction(dbFile) {
                    @Override
                    void mnyDbOpened(MnyDb newMnyDb) {
                        mnyDbSelected(newMnyDb);
                    }
                };
                menuItem.addActionListener(action);
            }
        }
    }

    protected abstract void mnyDbSelected(MnyDb newMnyDb);

    public void addSamplesMenu(JMenuBar menuBar) {
        File sampleTopDir = new File("../sunriise-core");

        addSamplesMenu(menuBar, sampleTopDir);
    }
}

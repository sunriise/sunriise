package com.hungle.sunriise.viewer.model;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;

import com.hungle.sunriise.viewer.misc.TableListItem;

public class MnyViewerDataModelTest {
    @Test
    public void testAspect() {
        MnyViewerDataModel model = new MnyViewerDataModel();
        PropertyChangeListener listener = new PropertyChangeListener() {

            @Override
            public void propertyChange(PropertyChangeEvent evt) {
                Assert.assertNotNull(evt);
                Assert.assertEquals("tables", evt.getPropertyName());
                Assert.assertNotNull(evt.getSource());
                Assert.assertNotNull(evt.getNewValue());
                Assert.assertTrue(evt.getNewValue() instanceof List<?>);
                List<?> tables = (List<?>) evt.getNewValue();
                Assert.assertTrue(tables.size() == 0);
            }
        };
        model.addPropertyChangeListener(listener);

        List<TableListItem> tables = new ArrayList<TableListItem>();
        model.setTables(tables);

    }
}

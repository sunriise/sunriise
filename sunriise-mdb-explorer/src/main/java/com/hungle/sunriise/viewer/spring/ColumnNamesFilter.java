package com.hungle.sunriise.viewer.spring;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import com.healthmarketscience.jackcess.util.SimpleExportFilter;

final class ColumnNamesFilter extends SimpleExportFilter {
    private final Set<String> columnsView;

    ColumnNamesFilter(Set<String> columnsView) {
        this.columnsView = columnsView;
    }

    @Override
    public List<com.healthmarketscience.jackcess.Column> filterColumns(
            List<com.healthmarketscience.jackcess.Column> columns) throws IOException {
        List<com.healthmarketscience.jackcess.Column> filterColumns = new ArrayList<>();
        for (com.healthmarketscience.jackcess.Column column : columns) {
            if (columnsView.contains(column.getName())) {
                filterColumns.add(column);
            }
        }
        return filterColumns;
    }
}
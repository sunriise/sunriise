/*******************************************************************************
 * Copyright (c) 2016 Hung Le
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *******************************************************************************/
package com.hungle.sunriise.viewer.misc;

import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.ByteBuffer;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;

import org.apache.logging.log4j.Logger;

// TODO: Auto-generated Javadoc
/**
 * The Class ROMemoryMappedFileChannel.
 */
public class ROMemoryMappedFileChannel extends JackcessFileChannelAdapter {

    /** The Constant log. */
    private static final Logger log = org.apache.logging.log4j.LogManager.getLogger(ROMemoryMappedFileChannel.class);

    /** The db file. */
    private File dbFile;

    /** The random access file. */
    private RandomAccessFile randomAccessFile;

    /** The channel. */
    private FileChannel channel;

    /** The mapped byte buffer. */
    private MappedByteBuffer mappedByteBuffer;

    /**
     * Instantiates a new RO memory mapped file channel.
     *
     * @param dbFile the db file
     * @throws IOException Signals that an I/O exception has occurred.
     */
    public ROMemoryMappedFileChannel(File dbFile) throws IOException {
        super();
        this.dbFile = dbFile;
        this.randomAccessFile = new RandomAccessFile(dbFile, "r");
        this.channel = this.randomAccessFile.getChannel();
        this.mappedByteBuffer = this.channel.map(FileChannel.MapMode.READ_ONLY, 0, this.randomAccessFile.length());
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.hungle.sunriise.viewer.misc.JackcessFileChannelAdapter#size()
     */
    @Override
    public long size() throws IOException {
        return mappedByteBuffer.limit();
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.hungle.sunriise.viewer.misc.JackcessFileChannelAdapter#force(boolean)
     */
    @Override
    public void force(boolean metaData) throws IOException {
        if (mappedByteBuffer != null) {
            mappedByteBuffer.force();
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.hungle.sunriise.viewer.misc.JackcessFileChannelAdapter#read(java.nio.
     * ByteBuffer, long)
     */
    @Override
    public int read(ByteBuffer dst, long position) throws IOException {
        int n = 0;

        if (log.isDebugEnabled()) {
            log.debug("> read, position=" + position);
        }
        if (position < 0) {
            n = 0;
            if (log.isDebugEnabled()) {
                log.debug("< read, n=" + n);
            }
            return n;
        }

        if (position >= mappedByteBuffer.limit()) {
            n = -1;
            if (log.isDebugEnabled()) {
                log.debug("< read, n=" + n);
            }
            return n;
        }

        mappedByteBuffer.mark();
        try {
            mappedByteBuffer.position((int) position);
            n = transferAsMuchAsPossible(mappedByteBuffer, dst);
        } finally {
            mappedByteBuffer.reset();
        }
        if (log.isDebugEnabled()) {
            log.debug("< read, n=" + n);
        }
        return n;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.hungle.sunriise.viewer.misc.JackcessFileChannelAdapter#
     * implCloseChannel()
     */
    @Override
    protected void implCloseChannel() throws IOException {
        log.info("> MemoryMappedFileChannel close");

        if (mappedByteBuffer != null) {
            mappedByteBuffer.force();
            mappedByteBuffer = null;
        }
        if (channel != null) {
            channel.close();
            channel = null;
        }
        if (randomAccessFile != null) {
            randomAccessFile.close();
            randomAccessFile = null;
        }
        dbFile = null;
    }

    /**
     * Transfer as much as possible.
     *
     * @param src  the src
     * @param dest the dest
     * @return the int
     */
    private static int transferAsMuchAsPossible(ByteBuffer src, ByteBuffer dest) {
        int srcRemaining = src.remaining();
        int destRemaining = dest.remaining();
        if (log.isDebugEnabled()) {
            log.debug("> transferAsMuchAsPossible, srcRemaining=" + srcRemaining + ", destRemaining=" + destRemaining);
        }

        int nTransfer = Math.min(srcRemaining, destRemaining);
        if (log.isDebugEnabled()) {
            log.debug("> transferAsMuchAsPossible, nTransfer=" + nTransfer);
        }
        if (nTransfer > 0) {
            for (int i = 0; i < nTransfer; i++) {
                dest.put(src.get());
            }
        }
        return nTransfer;
    }

    /**
     * Gets the db file.
     *
     * @return the db file
     */
    public File getDbFile() {
        return dbFile;
    }

    /**
     * Sets the db file.
     *
     * @param dbFile the new db file
     */
    public void setDbFile(File dbFile) {
        this.dbFile = dbFile;
    }

}

package com.hungle.sunriise.viewer.spring;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.hungle.sunriise.viewer.MnyViewer;

@Configuration
public class MnyViewerConfiguration {
    @Bean
    public MnyViewerSpringHelper mnyViewerHolder() {
        return new MnyViewerSpringHelper(MnyViewer.getInstance());
    }
}

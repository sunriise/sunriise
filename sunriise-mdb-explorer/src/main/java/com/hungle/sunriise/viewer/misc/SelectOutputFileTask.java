package com.hungle.sunriise.viewer.misc;

import java.awt.Component;
import java.io.File;

import javax.swing.JFileChooser;

import org.apache.logging.log4j.Logger;

// TODO: Auto-generated Javadoc
/**
 * The Class SelectOutputFileTask.
 */
final class SelectOutputFileTask implements Runnable {

    /** The Constant LOGGER. */
    private static final Logger LOGGER = org.apache.logging.log4j.LogManager.getLogger(SelectOutputFileTask.class);

    /** The output file. */
    private File outputFile;

    /**
     * Instantiates a new select output file task.
     *
     * @param outputFile the output file
     */
    SelectOutputFileTask(File outputFile) {
        this.outputFile = outputFile;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Runnable#run()
     */
    @Override
    public void run() {
        outputFile = uiSelectOutputFile();
    }

    /**
     * Gets the output file.
     *
     * @return the output file
     */
    public File getOutputFile() {
        return outputFile;
    }

    /**
     * Ui select output file.
     *
     * @return the file
     */
    private static File uiSelectOutputFile() {
        LOGGER.info("> Using fileChooser ...");
        File outputFile = null;

        JFileChooser fc = new JFileChooser();
        fc.setDialogTitle("Select file to save result ...");
        Component parent = null;
        int showSaveDialog = fc.showSaveDialog(parent);
        if (showSaveDialog != JFileChooser.CANCEL_OPTION) {
            outputFile = fc.getSelectedFile();
        }
        return outputFile;
    }
}
/*******************************************************************************
 * Copyright (c) 2016 Hung Le
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *******************************************************************************/
/**
 * 
 */
package com.hungle.sunriise.viewer.menu;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JPopupMenu;

import org.apache.logging.log4j.Logger;

// TODO: Auto-generated Javadoc
/**
 * The listener interface for receiving popup events. The class that is
 * interested in processing a popup event implements this interface, and the
 * object created with that class is registered with a component using the
 * component's <code>addPopupListener</code> method. When the popup event
 * occurs, that object's appropriate method is invoked.
 */
public class PopupListener extends MouseAdapter {

    /** The Constant log. */
    private static final Logger log = org.apache.logging.log4j.LogManager.getLogger(PopupListener.class);

    /** The popup. */
    private JPopupMenu popup;

    /**
     * Gets the popup.
     *
     * @return the popup
     */
    public JPopupMenu getPopup() {
        return popup;
    }

    /**
     * Sets the popup.
     *
     * @param popup the new popup
     */
    public void setPopup(JPopupMenu popup) {
        this.popup = popup;
    }

    /**
     * Instantiates a new popup listener.
     *
     * @param popup the popup
     */
    public PopupListener(JPopupMenu popup) {
        super();
        this.popup = popup;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.awt.event.MouseAdapter#mousePressed(java.awt.event.MouseEvent)
     */
    @Override
    public void mousePressed(MouseEvent e) {
        maybeShowPopup(e);
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.awt.event.MouseAdapter#mouseReleased(java.awt.event.MouseEvent)
     */
    @Override
    public void mouseReleased(MouseEvent e) {
        maybeShowPopup(e);
    }

    /**
     * Maybe show popup.
     *
     * @param event the e
     */
    protected void maybeShowPopup(MouseEvent event) {
        if (event.isPopupTrigger()) {
            if (vetoPopup(event)) {
                log.warn("NOT showing popup menu.");
            } else {
                if (popup != null) {
                    popup.show(event.getComponent(), event.getX(), event.getY());
                }
            }
        }
    }

    /**
     * Veto popup.
     *
     * @param event the event
     * @return true, if successful
     */
    protected boolean vetoPopup(MouseEvent event) {
        return false;
    }
}
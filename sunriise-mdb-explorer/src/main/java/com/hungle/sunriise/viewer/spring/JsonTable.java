package com.hungle.sunriise.viewer.spring;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonPropertyOrder({ "name", "rowCount", "columnCount", "columns" })
public class JsonTable {
    private String name;

    private int rowCount;

    private List<JsonColumn> columns;

    private int columnCount;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getRowCount() {
        return rowCount;
    }

    public void setRowCount(int rowCount) {
        this.rowCount = rowCount;
    }

    public List<JsonColumn> getColumns() {
        return columns;
    }

    public void setColumns(List<JsonColumn> columns) {
        this.columns = columns;
    }

    public int getColumnCount() {
        return columnCount;
    }

    public void setColumnCount(int columnCount) {
        this.columnCount = columnCount;
    }
}

package com.hungle.sunriise.viewer.spring;

import java.io.IOException;
import java.util.List;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.PrettyPrinter;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;

public class JsonTableOneResultSerializer extends StdSerializer<JsonTableOneResult> {
    public JsonTableOneResultSerializer() {
        this(null);
    }

    public JsonTableOneResultSerializer(Class<JsonTableOneResult> t) {
        super(t);
    }

    @Override
    public void serialize(JsonTableOneResult jsonTableOneResult, JsonGenerator jgen, SerializerProvider provider)
            throws IOException {
        PrettyPrinter prettyPrinter = jgen.getPrettyPrinter();
        System.out.println("prettyPrinter=" + prettyPrinter);

        final String eol = "\r\n";
        List<JsonRow> rows = jsonTableOneResult.getRows();
        for (JsonRow row : rows) {
            try {
                jgen.writeObject(row);
            } finally {
                jgen.writeRaw(eol);
            }
        }

    }
}

package com.hungle.sunriise.viewer.menu;

import java.awt.event.KeyEvent;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;

import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.JTable;
import javax.swing.table.TableModel;
import javax.swing.text.DefaultEditorKit;

import org.apache.logging.log4j.Logger;

import com.hungle.sunriise.viewer.table.MnyTableModel;

// TODO: Auto-generated Javadoc
/**
 * The Class MenuUtil.
 */
public class MenuUtils {

    /** The Constant log. */
    private static final Logger log = org.apache.logging.log4j.LogManager.getLogger(MenuUtils.class);

    /**
     * Update goto column menu.
     *
     * @param table          the table
     * @param gotoColumnMenu the goto column menu
     */
    public static final void updateGotoColumnMenu(final JTable table, JMenu gotoColumnMenu) {
        if (log.isDebugEnabled()) {
            log.debug("> updateGotoColumnMenu");
        }

        final TableModel tableModel = table.getModel();

        SortedSet<String> columnNames = new TreeSet<String>();

        int count = tableModel.getColumnCount();
        for (int i = 0; i < count; i++) {
            String columnName = tableModel.getColumnName(i);
            columnNames.add(columnName);
        }

        AbstractGotoToColumnContext context = new AbstractGotoToColumnContext() {
            @Override
            public JTable getTable() {
                return table;
            }

            @Override
            public int getColumnIndex(String columnName) {
                return MnyTableModel.getColumnIndex(tableModel, columnName);
            }

            @Override
            public int getSelectedRow() {
                return table.getSelectedRow();
            }
        };

        updateGotoColumnMenu(gotoColumnMenu, columnNames, context);

        if (log.isDebugEnabled()) {
            log.debug("< updateGotoColumnMenu" + ", count=" + count);
        }
    }

    /**
     * Update goto column menu.
     *
     * @param menu        the menu
     * @param columnNames the column names
     * @param context     the context
     */
    private static void updateGotoColumnMenu(JMenu menu, Set<String> columnNames, AbstractGotoToColumnContext context) {
        int count = columnNames.size();

        menu.removeAll();

        int maxItemsPerMenu = 10;
        if (count < maxItemsPerMenu) {
            for (String columnName : columnNames) {
                JMenuItem menuItem = new JMenuItem(new GotoToColumnAction(context, columnName));
                menu.add(menuItem);
            }
        } else {
            JMenu parentMenu = null;
            int i = 0;
            for (String columnName : columnNames) {
                if ((i % maxItemsPerMenu) == 0) {
                    parentMenu = new JMenu(columnName + " ...");
                    menu.add(parentMenu);
                }
                JMenuItem menuItem = new JMenuItem(new GotoToColumnAction(context, columnName));
                parentMenu.add(menuItem);
                i++;
            }
        }
    }

    /**
     * Populate ccp menu.
     *
     * @param menu the main menu
     */
    public static final void populateCCPMenu(JPopupMenu menu) {
        JMenuItem menuItem = null;

        menuItem = new JMenuItem(new DefaultEditorKit.CutAction());
        menuItem.setText("Cut");
        menuItem.setMnemonic(KeyEvent.VK_T);
        menu.add(menuItem);

        menuItem = new JMenuItem(new DefaultEditorKit.CopyAction());
        menuItem.setText("Copy");
        menuItem.setMnemonic(KeyEvent.VK_C);
        menu.add(menuItem);

        menuItem = new JMenuItem(new DefaultEditorKit.PasteAction());
        menuItem.setText("Paste");
        menuItem.setMnemonic(KeyEvent.VK_P);
        menu.add(menuItem);
    }

}

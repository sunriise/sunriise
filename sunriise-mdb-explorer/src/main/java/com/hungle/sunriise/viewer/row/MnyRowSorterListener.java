/*******************************************************************************
 * Copyright (c) 2016 Hung Le
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *******************************************************************************/
package com.hungle.sunriise.viewer.row;

import javax.swing.event.RowSorterEvent;
import javax.swing.event.RowSorterEvent.Type;
import javax.swing.event.RowSorterListener;

import org.apache.logging.log4j.Logger;

// TODO: Auto-generated Javadoc
/**
 * The listener interface for receiving mnyRowSorter events. The class that is
 * interested in processing a mnyRowSorter event implements this interface, 37
 * and the object created with that class is registered with a component using
 * the component's <code>addMnyRowSorterListener</code> method. When the
 * mnyRowSorter event occurs, that object's appropriate method is invoked.
 *
 */
public final class MnyRowSorterListener implements RowSorterListener {

    /** The Constant log. */
    private static final Logger log = org.apache.logging.log4j.LogManager.getLogger(MnyRowSorterListener.class);

    /** The start time. */
    private long startTime = -1L;

    /*
     * (non-Javadoc)
     * 
     * @see javax.swing.event.RowSorterListener#sorterChanged(javax.swing.event.
     * RowSorterEvent)
     */
    @Override
    public void sorterChanged(RowSorterEvent event) {
        log.info("> sorterChanged");
        Type type = event.getType();
        log.info("  " + type);
        switch (type) {
        case SORT_ORDER_CHANGED:
            startTime = System.currentTimeMillis();
            break;
        case SORTED:
            if (startTime > 0) {
                long now = System.currentTimeMillis();
                long delta = now - startTime;
                startTime = -1L;
                log.info("sorterChanged, delta=" + delta);
            }
            break;
        default:
            break;
        }
    }
}
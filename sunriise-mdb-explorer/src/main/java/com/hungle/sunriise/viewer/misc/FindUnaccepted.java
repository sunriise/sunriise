/*******************************************************************************
 * Copyright (c) 2016 Hung Le
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *******************************************************************************/
package com.hungle.sunriise.viewer.misc;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.prefs.Preferences;

import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;

import org.apache.logging.log4j.Logger;

import com.healthmarketscience.jackcess.Cursor;
import com.healthmarketscience.jackcess.CursorBuilder;
import com.healthmarketscience.jackcess.Database;
import com.healthmarketscience.jackcess.Row;
import com.healthmarketscience.jackcess.Table;
import com.hungle.sunriise.dbutil.TableAccountUtils;
import com.hungle.sunriise.dbutil.TableCategoryUtils;
import com.hungle.sunriise.dbutil.TableTransactionUtils;
import com.hungle.sunriise.io.MnyDb;
import com.hungle.sunriise.mnyobject.Account;
import com.hungle.sunriise.mnyobject.AccountLink;
import com.hungle.sunriise.mnyobject.MnyContext;
import com.hungle.sunriise.mnyobject.Transaction;
import com.hungle.sunriise.util.AbstractAccountAction;
import com.hungle.sunriise.util.MnyContextUtils;
import com.hungle.sunriise.viewer.open.OpenDbAction;
import com.hungle.sunriise.viewer.open.OpenDbDialog;

// TODO: Auto-generated Javadoc
/**
 * The Class FindUnaccepted.
 */
public class FindUnaccepted extends AbstractAccountAction {

    /** The Constant LOGGER. */
    private static final Logger LOGGER = org.apache.logging.log4j.LogManager.getLogger(FindUnaccepted.class);

    /** The transactions. */
    private List<Transaction> transactions = new ArrayList<Transaction>();

    /**
     * The main method.
     *
     * @param args the arguments
     */
    public static void main(String[] args) {
        String outFileName = null;
        String inFileName = null;
        String password = null;

        if (args.length == 3) {
            outFileName = args[0];
            inFileName = args[1];
            password = args[2];
        } else if (args.length == 2) {
            outFileName = args[0];
            inFileName = args[1];
            password = null;
        } else if (args.length == 0) {
            // OK, will prompt user for info
        } else {
            Class<FindUnaccepted> clz = FindUnaccepted.class;
            System.out.println("Usage: java " + clz.getName() + " out.csv file.mny [password]");
            System.exit(1);
        }

        LOGGER.info("ARG outFileName=" + outFileName);
        LOGGER.info("ARG inFileName=" + inFileName);

        final JFrame mainFrame = new JFrame();

        String out = outFileName;
        String in = inFileName;
        String pw = password;

        SwingUtilities.invokeLater(new Runnable() {

            @Override
            public void run() {
                Dimension preferredSize = new Dimension(100, 100);
                mainFrame.setPreferredSize(preferredSize);
                mainFrame.setTitle(FindUnaccepted.class.getName());

                mainFrame.pack();
                mainFrame.setLocationRelativeTo(null);
                mainFrame.setVisible(true);

                doIt(out, in, pw, mainFrame);
            }
        });

    }

    private static void doIt(String outFileName, String inFileName, String password, final JFrame mainFrame) {
        MnyDb mnyDb = null;
        if ((inFileName == null) || (inFileName.length() <= 0)) {
            Component locationRelativeTo = null;
            Preferences prefs = Preferences.userNodeForPackage(FindUnaccepted.class);
            mnyDb = new MnyDb();

            OpenDbAction openDbAction = new OpenDbAction(locationRelativeTo, prefs, mnyDb) {

                @Override
                public void dbFileOpened(MnyDb newMnyDb, OpenDbDialog dialog) {
                }

            };
            Object source = mnyDb;
            int id = 0;
            String command = "command";
            ActionEvent event = new ActionEvent(source, id, command);
            openDbAction.actionPerformed(event);
            mnyDb = openDbAction.getMnyDb();
        }

        try {
            if (mnyDb == null) {
                File dbFile = new File(inFileName);
                mnyDb = new MnyDb(dbFile, password);
            }

            MnyContext mnyContext = MnyContextUtils.createMnyContext(mnyDb);

            List<Transaction> transactions = findUnaccepted(mnyContext);
            // List<Transaction> transactions =
            // findUnacceptedUsingForEach(mnyContext);

            JOptionPane.showMessageDialog(mainFrame, "Found " + transactions.size() + " UNACCEPTED transaction(s) ...");

            Map<Integer, List<Transaction>> map = new HashMap<>();
            if (transactions.size() > 0) {
                for (Transaction transaction : transactions) {
                    AccountLink ac = transaction.getAccount();
                    if (ac != null) {
                        Integer id = ac.getId();
                        if (id != null) {
                            List<Transaction> list = map.get(id);
                            if (list == null) {
                                list = new ArrayList<Transaction>();
                                map.put(id, list);
                            }
                            list.add(transaction);
                        }
                    }
                }
                for (Integer key : map.keySet()) {
                    List<Transaction> value = map.get(key);
                    LOGGER.info(key + ", list.size=" + value.size());
                }

                saveResult(outFileName, mainFrame, mnyContext, transactions);
            }

        } catch (IOException e) {
            LOGGER.error(e, e);
        } finally {
            if (mnyDb != null) {
                try {
                    mnyDb.close();
                } catch (IOException e) {
                    LOGGER.warn(e);
                } finally {
                    mnyDb = null;
                }
            }
            LOGGER.info("> DONE");
            System.exit(0);
        }
    }

    private static void saveResult(String outFileName, final JFrame mainFrame, MnyContext mnyContext,
            List<Transaction> transactions) throws IOException {
        File outputFile = null;
        if (outFileName == null) {
            JFileChooser fc = new JFileChooser();
            fc.setDialogTitle("Select file to save result ...");
            Component parent = mainFrame;
            if (fc.showSaveDialog(parent) != JFileChooser.CANCEL_OPTION) {
                outputFile = fc.getSelectedFile();
            }
        } else {
            outputFile = new File(outFileName);
        }

        LOGGER.info("outputFile=" + outputFile);
        if (outputFile == null) {
            LOGGER.warn("SKIP saving result, no outputFile specified.");
        } else {
            saveResult(transactions, outputFile, mnyContext);
        }
    }

    /**
     * Save result.
     *
     * @param transactions the transactions
     * @param outputFile   the output file
     * @param mnyContext   the mny context
     * @throws IOException Signals that an I/O exception has occurred.
     */
    private static void saveResult(List<Transaction> transactions, File outputFile, MnyContext mnyContext)
            throws IOException {
        PrintWriter writer = null;

        // File file = new File(outFileName);
        try {
            LOGGER.info("Saving result to file=" + outputFile.getAbsolutePath());
            writer = new PrintWriter(outputFile);
            writer.println(toCsvHeaderString());
            for (Transaction transaction : transactions) {
                String str = toCsvString(transaction, mnyContext);
                writer.println(str);
            }
        } finally {
            if (writer != null) {
                writer.close();
                writer = null;
            }
            LOGGER.info("DONE - Saving result to file=" + outputFile.getAbsolutePath());
        }

    }

    /**
     * To csv header string.
     *
     * @return the string
     */
    private static String toCsvHeaderString() {
        StringBuilder sb = new StringBuilder();
        String sep = "\t";

        String[] headers = { "accountName", "date", "amount", "category", "meno", };

        int i = 0;
        for (String header : headers) {
            if (i > 0) {
                sb.append(sep);
            }
            sb.append(header);
            i++;
        }

        return sb.toString();
    }

    /**
     * To csv string.
     *
     * @param transaction the transaction
     * @param mnyContext  the mny context
     * @return the string
     */
    private static String toCsvString(Transaction transaction, MnyContext mnyContext) {
        StringBuilder sb = new StringBuilder();
        String sep = "\t";

        Account account = TableAccountUtils.getTransactionAccount(transaction, mnyContext);
        sb.append(account.getName());

        sb.append(sep);
        sb.append(transaction.getDate());

        sb.append(sep);
        sb.append(transaction.getAmount());

        String categoryName = TableCategoryUtils.getCategoryFullName(transaction.getCategory().getId(),
                mnyContext.getCategories());
        sb.append(sep);
        sb.append(categoryName);

        sb.append(sep);
        sb.append(transaction.getMemo());

        return sb.toString();
    }

    /**
     * Find unaccepted.
     *
     * @param mnyContext the mny context
     * @return the list
     * @throws IOException Signals that an I/O exception has occurred.
     */
    private static List<Transaction> findUnaccepted(MnyContext mnyContext) throws IOException {
        List<Transaction> transactions = new ArrayList<Transaction>();
        Database db = mnyContext.getDb();
        Table table = TableTransactionUtils.getTable(db);
        Cursor cursor = CursorBuilder.createCursor(table);
        Row row = null;
        long count = 0;
        while ((row = cursor.getNextRow()) != null) {
            Integer unaccepted = TableTransactionUtils.getUnaccepted(row);
            if (unaccepted == null) {
                continue;
            }
            if (unaccepted != -1) {
                LOGGER.info("unaccepted=" + unaccepted + ", binary=" + Integer.toBinaryString(unaccepted));
                Transaction transaction = TableTransactionUtils.createTransaction(db, row, mnyContext);
                transactions.add(transaction);
            }
            count++;
            if ((count % 100) == 0) {
                LOGGER.info("Read " + count + " rows ...");
            }
        }
        return transactions;
    }

    /**
     * Find unaccepted using for each.
     *
     * @param mnyContext the mny context
     * @return the list
     * @throws IOException Signals that an I/O exception has occurred.
     */
    private static List<Transaction> findUnacceptedUsingForEach(MnyContext mnyContext) throws IOException {
        FindUnaccepted accountAction = new FindUnaccepted();
        mnyContext.forEachAccount(accountAction);
        return accountAction.getTransactions();
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.hungle.sunriise.utils.AbstractAccountAction#execute()
     */
    @Override
    public void execute() {
        for (Transaction transaction : getAccount().getTransactions()) {
            if (transaction.isUnaccepted()) {
                transactions.add(transaction);
            }
        }
    }

    /**
     * Gets the transactions.
     *
     * @return the transactions
     */
    public List<Transaction> getTransactions() {
        return transactions;
    }

    /**
     * Sets the transactions.
     *
     * @param transactions the new transactions
     */
    public void setTransactions(List<Transaction> transactions) {
        this.transactions = transactions;
    }
}

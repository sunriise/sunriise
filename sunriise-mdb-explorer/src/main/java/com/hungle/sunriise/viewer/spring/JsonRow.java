package com.hungle.sunriise.viewer.spring;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonPropertyOrder({ "rowIndex", "values" })
public class JsonRow {
    private int rowIndex;

    private List<JsonCell> values;

    public int getRowIndex() {
        return rowIndex;
    }

    public void setRowIndex(int index) {
        this.rowIndex = index;
    }

    public List<JsonCell> getValues() {
        return values;
    }

    public void setValues(List<JsonCell> columns) {
        this.values = columns;
    }
}

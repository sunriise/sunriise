/*******************************************************************************
 * Copyright (c) 2016 Hung Le
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *******************************************************************************/
package com.hungle.sunriise.viewer.row;

import java.awt.Component;
import java.awt.Cursor;
import java.util.List;
import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;

import javax.swing.SwingUtilities;
import javax.swing.table.TableModel;
import javax.swing.table.TableRowSorter;

import org.apache.logging.log4j.Logger;

import com.hungle.sunriise.util.StopWatch;
import com.hungle.sunriise.viewer.MnyViewer;
import com.hungle.sunriise.viewer.table.MnyTableModel;
import com.hungle.sunriise.viewer.table.TableUtils;

// TODO: Auto-generated Javadoc
/**
 * The Class MnyTableRowSorter.
 */
public final class MnyTableRowSorter extends TableRowSorter<TableModel> {

    /** The Constant log. */
    private static final Logger log = org.apache.logging.log4j.LogManager.getLogger(MnyTableRowSorter.class);

    /** The myn viewer. */
    private final MnyViewer mynViewer;

    /** The table model. */
    private final MnyTableModel tableModel;

    /**
     * Instantiates a new mny table row sorter.
     *
     * @param mynViewer  the myn viewer
     * @param model      the model
     * @param tableModel the table model
     */
    public MnyTableRowSorter(MnyViewer mynViewer, TableModel model, MnyTableModel tableModel) {
        super(model);
        this.mynViewer = mynViewer;
        this.tableModel = tableModel;
        setMaxSortKeys(1);
    }

    /*
     * (non-Javadoc)
     * 
     * @see javax.swing.DefaultRowSorter#setSortKeys(java.util.List)
     */
    @Override
    public void setSortKeys(final List<? extends SortKey> sortKeys) {
        log.info("> setSortKeys, sortKeys.size=" + sortKeys.size());
        for (SortKey key : sortKeys) {
            log.info("  column=" + key.getColumn() + ", order=" + key.getSortOrder());
        }

        // JOptionPane.showConfirmDialog(frame, "setSortKeys");
        boolean background = true;
        log.info("Background sorting:  " + background);

        if (background) {
            int parties = 2;
            Runnable barrierAction = new Runnable() {

                @Override
                public void run() {
                    log.info("Background sorting thread is DONE.");
                }
            };
            final CyclicBarrier barrier = new CyclicBarrier(parties, barrierAction);
            Runnable command = new Runnable() {

                @Override
                public void run() {
                    try {
                        parentSetSortKeys(sortKeys);
                        barrier.await();
                    } catch (InterruptedException e) {
                        log.warn(e);
                    } catch (BrokenBarrierException e) {
                        log.warn(e);
                    }
                }
            };

            Component parent = SwingUtilities.getRoot(this.mynViewer.getFrame());
            Cursor waitCursor = TableUtils.setWaitCursor(parent);
            try {
                MnyViewer.getThreadPool().execute(command);

                barrier.await();
            } catch (InterruptedException e) {
                log.warn(e);
            } catch (BrokenBarrierException e) {
                log.warn(e);
            } finally {
                TableUtils.clearWaitCursor(parent, waitCursor);
            }
        } else {
            Component parent = SwingUtilities.getRoot(this.mynViewer.getFrame());
            Cursor waitCursor = TableUtils.setWaitCursor(parent);
            try {
                parentSetSortKeys(sortKeys);
            } finally {
                TableUtils.clearWaitCursor(parent, waitCursor);
            }
        }
    }

    /**
     * Parent set sort keys.
     *
     * @param sortKeys the sort keys
     */
    private void parentSetSortKeys(List<? extends SortKey> sortKeys) {
        super.setSortKeys(sortKeys);
    }

    /*
     * (non-Javadoc)
     * 
     * @see javax.swing.DefaultRowSorter#toggleSortOrder(int)
     */
    @Override
    public void toggleSortOrder(int column) {
        tableModel.setIsSorting(true);
        StopWatch stopWatch = new StopWatch();

        String columnName = tableModel.getColumnName(column);
        log.info("> toggleSortOrder, count=" + getViewRowCount() + ", column=" + column + ", columnName=" + columnName);
        try {
            // JOptionPane.showConfirmDialog(frame, "Hello");
            this.mynViewer.toggleRowSortOrderStarted(column);
            super.toggleSortOrder(column);
        } finally {
            tableModel.setIsSorting(false);
            long delta = stopWatch.click();
            this.mynViewer.changeRightStatusText("sort: rows=" + getViewRowCount() + ", millisecond=" + delta);
            log.info("< toggleSortOrder, delta=" + delta);
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see javax.swing.DefaultRowSorter#sort()
     */
    @Override
    public void sort() {
        StopWatch stopWatch = new StopWatch();
        log.info("> sort");

        String message = "### STARTING to sort " + " " + this.getViewRowCount() + "/" + this.getModelRowCount()
                + " ... please wait ...";
        log.info(message);

        // JOptionPane.showConfirmDialog(frame, message);

        // rightStatusLabel.setText(message);

        // Component parentComponent = MnyViewer.this.frame;
        // JOptionPane.showConfirmDialog(parentComponent, message);

        // Frame owner = MnyViewer.this.frame;
        // JDialog dialog = new JDialog(owner);
        // JPanel dialogMainView = new JPanel();
        // dialogMainView.setLayout(new BorderLayout());
        // JLabel dialogLabel = new JLabel(message);
        // dialogMainView.add(dialogLabel, BorderLayout.CENTER);
        // dialog.getContentPane().add(dialogMainView);
        // dialog.setModal(false);
        // dialog.pack();
        // dialog.setLocationRelativeTo(null);
        // dialog.show();

        // Component parent = SwingUtilities.getRoot(MynViewer.this.frame);
        // parent = MnyViewer.this.frame.getTopLevelAncestor();
        // Cursor waitCursor = setWaitCursor(parent);
        try {
            super.sort();
        } finally {
            // dialog.dispose();
            // clearWaitCursor(parent, waitCursor);

            final long delta = stopWatch.click();
            log.info("< sort, delta=" + delta);
        }
    }
}
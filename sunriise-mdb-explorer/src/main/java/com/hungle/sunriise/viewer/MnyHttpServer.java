package com.hungle.sunriise.viewer;

import org.springframework.boot.SpringApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.core.env.ConfigurableEnvironment;

public class MnyHttpServer {
//    private SpringApplication springApplication;
    private ConfigurableApplicationContext springApplicationContext;
    private Class<?>[] primarySources;

    public MnyHttpServer(Class<?>... primarySources) {
        super();
        this.primarySources = primarySources;
    }

    public void restartHttpServer(String[] args) {
        stopHttpServer();
        startHttpServer(args);
    }

    protected void startHttpServer(String[] args) {
        SpringApplication springApplication = new SpringApplication(primarySources);
        springApplicationContext = springApplication.run(args);
    }

    protected void stopHttpServer() {
        if (springApplicationContext != null) {
            SpringApplication.exit(springApplicationContext);
            springApplicationContext = null;
        }
    }

    public ConfigurableApplicationContext getSpringApplicationContext() {
        return springApplicationContext;
    }

    public void setSpringApplicationContext(ConfigurableApplicationContext springApplicationContext) {
        this.springApplicationContext = springApplicationContext;
    }

    public int getMongoDbPort() {
        int port = 0;

        ConfigurableEnvironment environment = getSpringApplicationContext().getEnvironment();
        port = Integer.valueOf(environment.getProperty("spring.data.mongodb.port", "37017"));
        // @Value("${spring.data.mongodb.port:37017}")
        //
        return port;
    }
}

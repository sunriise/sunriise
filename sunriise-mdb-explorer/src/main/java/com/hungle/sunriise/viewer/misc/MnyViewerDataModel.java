/*******************************************************************************
 * Copyright (c) 2010 Hung Le
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *******************************************************************************/
package com.hungle.sunriise.viewer.misc;

import java.util.List;

import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;

import com.healthmarketscience.jackcess.Table;

// TODO: Auto-generated Javadoc
/**
 * The Class MnyViewerDataModel.
 */
public class MnyViewerDataModel {

    /** The tables. */
    private List<TableListItem> tables;

    /** The table. */
    private Table table;

    /** The table model. */
    private TableModel tableModel = new DefaultTableModel();

    /** The table name. */
    private String tableName;

    /** The table meta data. */
    private String tableMetaData;

    /** The header info. */
    private String headerInfo;

    /** The key info. */
    private String keyInfo;

    /** The index info. */
    private String indexInfo;

    /**
     * Gets the tables.
     *
     * @return the tables
     */
    public List<TableListItem> getTables() {
        return tables;
    }

    /**
     * Sets the tables.
     *
     * @param tables the new tables
     */
    public void setTables(List<TableListItem> tables) {
        this.tables = tables;
    }

    /**
     * Gets the table model.
     *
     * @return the table model
     */
    public TableModel getTableModel() {
        return tableModel;
    }

    /**
     * Sets the table model.
     *
     * @param tableModel the new table model
     */
    public void setTableModel(TableModel tableModel) {
        this.tableModel = tableModel;
    }

    /**
     * Gets the table name.
     *
     * @return the table name
     */
    public String getTableName() {
        return tableName;
    }

    /**
     * Sets the table name.
     *
     * @param tableName the new table name
     */
    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    /**
     * Gets the table.
     *
     * @return the table
     */
    public Table getTable() {
        return table;
    }

    /**
     * Sets the table.
     *
     * @param table the new table
     */
    public void setTable(Table table) {
        this.table = table;
    }

    /**
     * Gets the table meta data.
     *
     * @return the table meta data
     */
    public String getTableMetaData() {
        return tableMetaData;
    }

    /**
     * Sets the table meta data.
     *
     * @param tableMetaData the new table meta data
     */
    public void setTableMetaData(String tableMetaData) {
        this.tableMetaData = tableMetaData;
    }

    /**
     * Gets the header info.
     *
     * @return the header info
     */
    public String getHeaderInfo() {
        return headerInfo;
    }

    /**
     * Sets the header info.
     *
     * @param headerInfo the new header info
     */
    public void setHeaderInfo(String headerInfo) {
        this.headerInfo = headerInfo;
    }

    /**
     * Gets the key info.
     *
     * @return the key info
     */
    public String getKeyInfo() {
        return keyInfo;
    }

    /**
     * Sets the key info.
     *
     * @param keyInfo the new key info
     */
    public void setKeyInfo(String keyInfo) {
        this.keyInfo = keyInfo;
    }

    /**
     * Gets the index info.
     *
     * @return the index info
     */
    public String getIndexInfo() {
        return indexInfo;
    }

    /**
     * Sets the index info.
     *
     * @param indexInfo the new index info
     */
    public void setIndexInfo(String indexInfo) {
        this.indexInfo = indexInfo;
    }
}

/*******************************************************************************
 * Copyright (c) 2010 Hung Le
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *******************************************************************************/
/**
 * 
 */
package com.hungle.sunriise.viewer.cell;

import java.awt.Color;
import java.awt.Component;
import java.math.BigDecimal;
import java.util.Date;

import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableCellRenderer;

import org.apache.logging.log4j.Logger;

// TODO: Auto-generated Javadoc
/**
 * The Class MyTableCellRenderer.
 */
public class MyTableCellRenderer extends DefaultTableCellRenderer {

    /** The Constant log. */
    private static final Logger LOGGER = org.apache.logging.log4j.LogManager.getLogger(MyTableCellRenderer.class);

    /** The even rows color. */
    private final Color evenRowsColor;

    /** The odd rows color. */
    private final Color oddRowsColor;

    /** The parent cell renderer. */
    private TableCellRenderer parentCellRenderer = null;

    /**
     * Instantiates a new my table cell renderer.
     *
     * @param cellRenderer  the cell renderer
     * @param evenRowsColor the even rows color
     * @param oddRowsColor  the odd rows color
     */
    private MyTableCellRenderer(TableCellRenderer cellRenderer, Color evenRowsColor, Color oddRowsColor) {
        super();
        this.parentCellRenderer = cellRenderer;
        this.evenRowsColor = evenRowsColor;
        this.oddRowsColor = oddRowsColor;
    }

    /**
     * Instantiates a new my table cell renderer.
     */
    public MyTableCellRenderer() {
        this(null);
    }

    /**
     * Instantiates a new my table cell renderer.
     *
     * @param cellRenderer the cell renderer
     */
    public MyTableCellRenderer(TableCellRenderer cellRenderer) {
        this(cellRenderer, new Color(204, 255, 204), Color.WHITE);
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * javax.swing.table.DefaultTableCellRenderer#getTableCellRendererComponent(
     * javax.swing.JTable, java.lang.Object, boolean, boolean, int, int)
     */
    @Override
    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus,
            int row, int column) {
        Component rendererComponent = null;
        Class<? extends Object> valueClz = null;
        if (value != null) {
            valueClz = value.getClass();
            TableCellRenderer defaultRenderer = table.getDefaultRenderer(valueClz);

            if (defaultRenderer != null) {
                if (LOGGER.isDebugEnabled()) {
                    LOGGER.debug("clz=" + valueClz.getName() + ", row=" + row + ", column=" + column
                            + ", defaultRenderer=" + defaultRenderer.getClass());
                }
                rendererComponent = defaultRenderer.getTableCellRendererComponent(table, value, isSelected, hasFocus,
                        row, column);
            } else {
                if (LOGGER.isDebugEnabled()) {
                    LOGGER.debug("clz=" + valueClz.getName() + ", row=" + row + ", column=" + column
                            + ", defaultRenderer=" + defaultRenderer);
                }
            }
        }

        if (rendererComponent == null) {
            if (parentCellRenderer != null) {
                rendererComponent = parentCellRenderer.getTableCellRendererComponent(table, value, isSelected, hasFocus,
                        row, column);
            } else {
                rendererComponent = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row,
                        column);
            }
        }

        // striped
        if (!isSelected) {
            if (row % 2 == 0) {
                rendererComponent.setBackground(evenRowsColor);
            } else {
                rendererComponent.setBackground(oddRowsColor);
            }
        }

        setCellHorizontalAlignment(rendererComponent, column, value);

        return rendererComponent;
    }

    /**
     * Sets the cell horizontal alignment.
     *
     * @param rendererComponent the renderer component
     * @param column            the column
     * @param value             the value
     */
    protected void setCellHorizontalAlignment(Component rendererComponent, int column, Object value) {
        // right-align if it is a Date
        if ((value != null) && (value instanceof Date)) {
            if (rendererComponent instanceof JLabel) {
                JLabel label = (JLabel) rendererComponent;
                label.setHorizontalAlignment(SwingConstants.RIGHT);
            }
        }
        if ((value != null) && (value instanceof BigDecimal)) {
            if (rendererComponent instanceof JLabel) {
                JLabel label = (JLabel) rendererComponent;
                label.setHorizontalAlignment(SwingConstants.RIGHT);
            }
        }
    }
}
package com.hungle.sunriise.viewer.spring;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.healthmarketscience.jackcess.Column;
import com.healthmarketscience.jackcess.Cursor;
import com.healthmarketscience.jackcess.CursorBuilder;
import com.healthmarketscience.jackcess.Database;
import com.healthmarketscience.jackcess.Row;
import com.healthmarketscience.jackcess.Table;
import com.healthmarketscience.jackcess.impl.ByteUtil;
import com.healthmarketscience.jackcess.util.ExportFilter;
import com.healthmarketscience.jackcess.util.SimpleExportFilter;
import com.hungle.sunriise.io.MnyDb;

@RestController
@RequestMapping(value = "/json")
public class MnyViewerJsonController {
    @Autowired
    private MnyViewerSpringHelper mnyViewerHolder;

    @RequestMapping(value = "schemas")
    public List<JsonTable> schemasAll() throws IOException {
        Database db = getDb();

        List<JsonTable> jsonTables = new ArrayList<>();

        for (String tableName : db.getTableNames()) {
            Table table = db.getTable(tableName);

            JsonTable jsonTable = createJsonTable(table);

            jsonTables.add(jsonTable);
        }

        return jsonTables;
    }

    private JsonTable createJsonTable(Table table) {
        JsonTable jsonTable = new JsonTable();
        jsonTable.setName(table.getName());
        jsonTable.setColumns(getColumns(table.getColumns()));
        jsonTable.setColumnCount(table.getColumnCount());
        jsonTable.setRowCount(table.getRowCount());
        return jsonTable;
    }

    @RequestMapping(value = "schemas/{tableName}")
    public JsonTable schemasOne(@PathVariable String tableName) throws IOException {
        Database db = getDb();

        Table table = db.getTable(tableName);
        if (table == null) {
            throw new IOException("Table not found, tableName=" + tableName);
        }

        JsonTable jsonTable = createJsonTable(table);

        return jsonTable;
    }

    @RequestMapping(value = "tables")
    public List<JsonTable> tablesAll() throws IOException {
        return schemasAll();
    }

    @RequestMapping(value = "tables/{tableName}")
    public JsonTableOneResult tablesOne(@PathVariable String tableName,
            @RequestParam(value = "col", required = false) List<String> colNames) throws IOException {
        Database db = getDb();

        Table table = db.getTable(tableName);
        if (table == null) {
            throw new IOException("Table not found, tableName=" + tableName);
        }
        JsonTableOneResult jsonTableOneResult = new JsonTableOneResult();
        jsonTableOneResult.setTableName(tableName);

        List<JsonRow> jsonRows = getJsonRows(table, colNames);

        jsonTableOneResult.setRowCount(jsonRows.size());
        jsonTableOneResult.setRows(jsonRows);

        return jsonTableOneResult;
    }

    private List<JsonRow> getJsonRows(Table table, List<String> colNames) throws IOException {
        List<JsonRow> jsonRows = new ArrayList<>();

        Cursor cursor = CursorBuilder.createCursor(table);

        ExportFilter filter = getExportFilter(colNames);

        List<? extends Column> origCols = cursor.getTable().getColumns();

        List<Column> columns = getFilterColums(filter, origCols);

        Collection<String> columnNames = getFilterColumnNames(origCols, columns);

        int index = 0;
        // interate the data rows
        Object[] unfilteredRowData = new Object[columns.size()];
        Row row;
        while ((row = cursor.getNextRow(columnNames)) != null) {

            // fill raw row data in array
            for (int i = 0; i < columns.size(); i++) {
                unfilteredRowData[i] = columns.get(i).getRowValue(row);
            }

            // apply filter
            Object[] rowData = filter.filterRow(unfilteredRowData);
            if (rowData == null) {
                continue;
            }

            JsonRow jsonRow = new JsonRow();
            jsonRow.setRowIndex(index++);

            List<JsonCell> cells = new ArrayList<>();
            jsonRow.setValues(cells);

            // print row
            for (int i = 0; i < columns.size(); i++) {
                JsonCell jsonCell = new JsonCell();

                Object cellValue = rowData[i];
                Column column = columns.get(i);

                jsonCell.setColumnName(column.getName());
                jsonCell.setColumnType(column.getType());

                if (cellValue != null) {
                    String value = null;
                    if (cellValue instanceof byte[]) {
                        value = ByteUtil.toHexString((byte[]) cellValue);
                    } else {
                        value = String.valueOf(cellValue);
                    }
                    jsonCell.setValue(value);
                } else {
                    // null object
                }

                cells.add(jsonCell);
            }

            jsonRows.add(jsonRow);
        }

        return jsonRows;
    }

    private Collection<String> getFilterColumnNames(List<? extends Column> origCols, List<Column> columns) {
        Collection<String> columnNames = null;
        if (!origCols.equals(columns)) {
            // columns have been filtered
            columnNames = new HashSet<String>();
            for (Column c : columns) {
                columnNames.add(c.getName());
            }
        }
        return columnNames;
    }

    private List<Column> getFilterColums(ExportFilter filter, List<? extends Column> origCols) throws IOException {
        List<Column> columns = new ArrayList<Column>(origCols);
        columns = filter.filterColumns(columns);
        return columns;
    }

    private ExportFilter getExportFilter(List<String> colNames) {
        ExportFilter filter = null;
        if ((colNames != null) && (colNames.size()) > 0) {
            Set<String> columnsView = new HashSet<String>();
            columnsView.addAll(colNames);
            filter = new ColumnNamesFilter(columnsView);
        } else {
            filter = new SimpleExportFilter();
        }
        return filter;
    }

    private List<JsonColumn> getColumns(List<? extends Column> columns) {
        List<JsonColumn> list = new ArrayList<>();
        for (Column column : columns) {
            JsonColumn jsonColumn = new JsonColumn();
            jsonColumn.setName(column.getName());
            jsonColumn.setType(column.getType());

            list.add(jsonColumn);
        }
        return list;
    }

    private Database getDb() {
        if (mnyViewerHolder == null) {
            throw new IllegalStateException("INVALID_STATE - mnyViewerHolder is null.");
        }
        MnyDb mnyDb = mnyViewerHolder.getViewer().getMnyDb();
        if (mnyDb == null) {
            throw new IllegalStateException("INVALID_STATE - mnyDb is null.");
        }
        Database db = mnyDb.getDb();
        if (db == null) {
            throw new IllegalStateException("INVALID_STATE - db is null.");
        }
        return db;
    }
}

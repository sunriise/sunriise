/*******************************************************************************
 * Copyright (c) 2016 Hung Le
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *******************************************************************************/
package com.hungle.sunriise.viewer.table;

import java.awt.Component;
import java.awt.Cursor;
import java.awt.Rectangle;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.JTable;
import javax.swing.JViewport;
import javax.swing.RowSorter;
import javax.swing.SwingUtilities;
import javax.swing.event.MouseInputAdapter;
import javax.swing.table.JTableHeader;

import org.apache.logging.log4j.Logger;

// TODO: Auto-generated Javadoc
/**
 * The Class TableUtil.
 */
public class TableUtils {

    /** The Constant log. */
    private static final Logger log = org.apache.logging.log4j.LogManager.getLogger(TableUtils.class);

    /**
     * Insert listener to head.
     *
     * @param tableHeader    the table header
     * @param mouseListeners the mouse listeners
     */
    public static final void insertListenerToHead(final JTableHeader tableHeader, MouseListener[] mouseListeners) {
        for (MouseListener mouseListener : mouseListeners) {
            tableHeader.removeMouseListener(mouseListener);
        }
        MouseListener l = null;
        l = new MouseInputAdapter() {

            @Override
            public void mouseClicked(MouseEvent e) {
                if (e.getClickCount() % 2 == 1 && SwingUtilities.isLeftMouseButton(e)) {
                    JTable table = tableHeader.getTable();
                    RowSorter sorter;
                    if (table != null && (sorter = table.getRowSorter()) != null) {
                        int columnIndex = tableHeader.columnAtPoint(e.getPoint());
                        if (columnIndex != -1) {
                            columnIndex = table.convertColumnIndexToModel(columnIndex);
                            // sorter.toggleSortOrder(columnIndex);
                            log.info("> mouseClicked to sort");
                        }
                    }
                }
            }

        };
        tableHeader.addMouseListener(l);
        for (MouseListener mouseListener : mouseListeners) {
            tableHeader.addMouseListener(mouseListener);
        }
    }

    /**
     * Insert listener to head.
     *
     * @param table the table
     */
    private static final void insertListenerToHead(JTable table) {
        final JTableHeader tableHeader = table.getTableHeader();
        MouseListener[] mouseListeners = tableHeader.getMouseListeners();
        if (mouseListeners != null) {
            insertListenerToHead(tableHeader, mouseListeners);
        }
    }

    /**
     * Clear wait cursor.
     *
     * @param parent     the parent
     * @param waitCursor the wait cursor
     */
    public static final void clearWaitCursor(Component parent, Cursor waitCursor) {
        if (waitCursor != null) {
            log.info("YES CLEAR setCusror");
            parent.setCursor(null);
        } else {
            log.info("NO CLEAR setCusror");
        }
    }

    /**
     * Scroll to center.
     *
     * @param table       the table
     * @param rowIndex    the row index
     * @param columnIndex the v col index
     */
    public static final void scrollToCenter(JTable table, int rowIndex, int columnIndex) {
        if (!(table.getParent() instanceof JViewport)) {
            return;
        }
        JViewport viewport = (JViewport) table.getParent();

        // This rectangle is relative to the table where the
        // northwest corner of cell (0,0) is always (0,0).
        Rectangle rect = table.getCellRect(rowIndex, columnIndex, true);

        // The location of the view relative to the table
        Rectangle viewRect = viewport.getViewRect();

        // Translate the cell location so that it is relative
        // to the view, assuming the northwest corner of the
        // view is (0,0).
        rect.setLocation(rect.x - viewRect.x, rect.y - viewRect.y);

        // Calculate location of rect if it were at the center of view
        int centerX = (viewRect.width - rect.width) / 2;
        int centerY = (viewRect.height - rect.height) / 2;

        // Fake the location of the cell so that scrollRectToVisible
        // will move the cell to the center
        if (rect.x < centerX) {
            centerX = -centerX;
        }
        if (rect.y < centerY) {
            centerY = -centerY;
        }
        rect.translate(centerX, centerY);

        // Scroll the area into view.
        viewport.scrollRectToVisible(rect);
    }

    /**
     * Sets the wait cursor.
     *
     * @param parent the parent
     * @return the cursor
     */
    public static final Cursor setWaitCursor(Component parent) {
        Cursor waitCursor = null;
        if ((parent != null) && (parent.isShowing())) {
            waitCursor = Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR);
            log.info("YES setCursor=" + waitCursor);
            parent.setCursor(waitCursor);
        } else {
            log.info("NO setCursor=" + waitCursor);
        }
        return waitCursor;
    }

    /**
     * Row values are same.
     *
     * @param value1 the value1
     * @param value2 the value2
     * @return true, if successful
     */
    public static final boolean rowValuesAreSame(Object value1, Object value2) {
        boolean same = false;
        if ((value1 == null) && (value2 == null)) {
            same = true;
        } else if (value1 == null) {
            same = false;
        } else if (value2 == null) {
            same = false;
        } else {
            if ((value1 instanceof Comparable) && (value2 instanceof Comparable)) {
                Comparable comp1 = (Comparable) value1;
                Comparable comp2 = (Comparable) value2;
                same = (comp1.compareTo(comp2) == 0);
            } else {
                same = value1.equals(value2);
            }
        }
        return same;
    }

}

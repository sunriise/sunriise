/*******************************************************************************
 * Copyright (c) 2016 Hung Le
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *******************************************************************************/
package com.hungle.sunriise.viewer.menu;

import javax.swing.JTable;

// TODO: Auto-generated Javadoc
/**
 * The Class AbstractGotoToColumnContext.
 */
public abstract class AbstractGotoToColumnContext {

    /**
     * Gets the table.
     *
     * @return the table
     */
    public abstract JTable getTable();

    /**
     * Gets the column index.
     *
     * @param columnName the column name
     * @return the column index
     */
    public abstract int getColumnIndex(String columnName);

    /**
     * Gets the selected row.
     *
     * @return the selected row
     */
    public abstract int getSelectedRow();

}

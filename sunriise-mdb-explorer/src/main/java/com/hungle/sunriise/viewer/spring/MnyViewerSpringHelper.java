package com.hungle.sunriise.viewer.spring;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import org.apache.logging.log4j.Logger;

import com.hungle.sunriise.io.MnyDb;
import com.hungle.sunriise.viewer.MnyViewer;

public class MnyViewerSpringHelper implements PropertyChangeListener {
    private static final Logger LOGGER = org.apache.logging.log4j.LogManager.getLogger(MnyViewerSpringHelper.class);

    private final MnyViewer viewer;

    private MnyDb mnyDb;

    public MnyViewerSpringHelper(MnyViewer viewer) {
        this.viewer = viewer;
        this.viewer.getSwingPropertyChangeSupport().addPropertyChangeListener(this);
    }

    public MnyViewer getViewer() {
        return viewer;
    }

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        if (evt == null) {
            return;
        }
        MnyViewerProperty mnyViewerProperty = MnyViewerProperty.get(evt.getPropertyName());
        LOGGER.info("> propertyChange, name=" + mnyViewerProperty.getPropertyName() + ", old=" + evt.getOldValue()
                + ", new=" + evt.getNewValue());

        switch (mnyViewerProperty) {
        case MNY_DB:
            if (evt.getNewValue() instanceof MnyDb) {
                this.mnyDb = (MnyDb) evt.getNewValue();
            }
            break;
        default:
            break;
        }
    }
}

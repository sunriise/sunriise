package com.hungle.sunriise.viewer.misc;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.lang.reflect.InvocationTargetException;
import java.util.prefs.Preferences;

import javax.swing.SwingUtilities;

import org.apache.logging.log4j.Logger;

import com.hungle.sunriise.io.MnyDb;
import com.hungle.sunriise.viewer.open.OpenDbAction;
import com.hungle.sunriise.viewer.open.OpenDbDialog;

// TODO: Auto-generated Javadoc
/**
 * The Class FindUnacceptedUtils.
 */
public class FindUnacceptedUtils {

    /** The Constant LOGGER. */
    private static final Logger LOGGER = org.apache.logging.log4j.LogManager.getLogger(FindUnacceptedUtils.class);

    /**
     * To csv header string.
     *
     * @param headers the headers
     * @return the string
     */
    static String toCsvHeaderString(String[] headers) {
        String sep = "\t";
        return toCsvHeaderString(headers, sep);
    }

    /**
     * To csv header string.
     *
     * @param headers the headers
     * @param sep     the sep
     * @return the string
     */
    private static String toCsvHeaderString(String[] headers, String sep) {
        StringBuilder sb = new StringBuilder();

        int i = 0;
        for (String header : headers) {
            if (i > 0) {
                sb.append(sep);
            }
            sb.append(header);
            i++;
        }

        return sb.toString();
    }

    /**
     * Ui prompt to open mny db.
     *
     * @return the mny db
     */
    static MnyDb uiPromptToOpenMnyDb() {
        MnyDb mnyDb;
        Component locationRelativeTo = null;
        Preferences prefs = Preferences.userNodeForPackage(FindUnaccepted.class);
        mnyDb = new MnyDb();

        OpenDbAction openDbAction = new OpenDbAction(locationRelativeTo, prefs, mnyDb) {

            @Override
            public void dbFileOpened(MnyDb newMnyDb, OpenDbDialog dialog) {
            }

        };
        Object source = mnyDb;
        int id = 0;
        String command = "command";
        ActionEvent event = new ActionEvent(source, id, command);
        openDbAction.actionPerformed(event);
        mnyDb = openDbAction.getMnyDb();
        return mnyDb;
    }

    /**
     * Save results.
     *
     * @param outFileName      the out file name
     * @param saveResultWriter the save result writer
     * @throws IOException Signals that an I/O exception has occurred.
     */
    static void saveResults(String outFileName, SaveResultWriter saveResultWriter) throws IOException {
        LOGGER.info("> Saving ...");

        File outputFile = null;
        if (outFileName == null) {
            SelectOutputFileTask doRun = new SelectOutputFileTask(outputFile);
            try {
                SwingUtilities.invokeAndWait(doRun);
                outputFile = doRun.getOutputFile();
            } catch (InvocationTargetException e) {
                throw new IOException(e);
            } catch (InterruptedException e) {
                throw new IOException(e);
            }
        } else {
            outputFile = new File(outFileName);
        }

        if (outputFile == null) {
            LOGGER.warn("SKIP saving result, no outputFile specified.");
        } else {
            FindUnacceptedUtils.saveResultToFile(outputFile, saveResultWriter);
        }
    }

    /**
     * Save result.
     *
     * @param outputFile       the output file
     * @param saveResultWriter the save result writer
     * @throws IOException Signals that an I/O exception has occurred.
     */
    private static void saveResultToFile(File outputFile, SaveResultWriter saveResultWriter) throws IOException {
        PrintWriter writer = null;

        try {
            LOGGER.info("Saving result to file=" + outputFile.getAbsolutePath());
            writer = new PrintWriter(outputFile);
            saveResultWriter.writeResult(writer);
        } finally {
            if (writer != null) {
                writer.close();
                writer = null;
            }
            LOGGER.info("DONE - Saving result to file=" + outputFile.getAbsolutePath());
        }

    }

    static MnyDb openMnyDb(String inFileName, String password) throws IOException {
        MnyDb mnyDb = null;
        if ((inFileName == null) || (inFileName.length() <= 0)) {
            mnyDb = uiPromptToOpenMnyDb();
        } else {
            File dbFile = new File(inFileName);
            mnyDb = new MnyDb(dbFile, password);
        }
        return mnyDb;
    }

}

/*******************************************************************************
 * Copyright (c) 2010 Hung Le
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *******************************************************************************/
package com.hungle.sunriise.viewer;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.prefs.Preferences;

import javax.swing.AbstractAction;
import javax.swing.AbstractButton;
import javax.swing.AbstractListModel;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JCheckBox;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.JSplitPane;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.ListModel;
import javax.swing.ListSelectionModel;
import javax.swing.RowFilter;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.event.RowSorterListener;
import javax.swing.event.SwingPropertyChangeSupport;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import javax.swing.table.TableModel;
import javax.swing.table.TableRowSorter;

import org.apache.logging.log4j.Logger;
import org.fife.ui.rsyntaxtextarea.RSyntaxTextArea;
import org.fife.ui.rsyntaxtextarea.SyntaxConstants;
import org.fife.ui.rtextarea.RTextScrollPane;
import org.jdesktop.beansbinding.AutoBinding;
import org.jdesktop.beansbinding.AutoBinding.UpdateStrategy;
import org.jdesktop.beansbinding.BeanProperty;
import org.jdesktop.beansbinding.Bindings;
import org.jdesktop.beansbinding.ELProperty;
import org.jdesktop.swingbinding.JListBinding;
import org.jdesktop.swingbinding.SwingBindings;

import com.healthmarketscience.jackcess.Database;
import com.healthmarketscience.jackcess.Table;
import com.hungle.sunriise.diskusage.DiskUsageView;
import com.hungle.sunriise.export.ui.ExportToContext;
import com.hungle.sunriise.export.ui.ExportToCsvAction;
import com.hungle.sunriise.export.ui.ExportToJSONAction;
import com.hungle.sunriise.export.ui.ExportToMdbAction;
import com.hungle.sunriise.gui.AbstractSamplesMenu;
import com.hungle.sunriise.io.MnyDb;
import com.hungle.sunriise.io.sample.MnySampleFile;
import com.hungle.sunriise.util.JackcessImplUtils;
import com.hungle.sunriise.util.JavaInfo;
import com.hungle.sunriise.util.StopWatch;
import com.hungle.sunriise.util.SunriiseBuildNumber;
import com.hungle.sunriise.viewer.cell.DateCellRenderer;
import com.hungle.sunriise.viewer.cell.DialogCellEditor;
import com.hungle.sunriise.viewer.cell.MyTableCellRenderer;
import com.hungle.sunriise.viewer.header.MyTabletHeaderRenderer;
import com.hungle.sunriise.viewer.menu.MenuUtils;
import com.hungle.sunriise.viewer.menu.PopupListener;
import com.hungle.sunriise.viewer.misc.DatabaseUtils;
import com.hungle.sunriise.viewer.misc.TableListItem;
import com.hungle.sunriise.viewer.model.MnyViewerDataModel;
import com.hungle.sunriise.viewer.open.OpenDbAction;
import com.hungle.sunriise.viewer.open.OpenDbDialog;
import com.hungle.sunriise.viewer.row.DiffData;
import com.hungle.sunriise.viewer.row.MnyRowSorterListener;
import com.hungle.sunriise.viewer.row.MnyTableRowSorter;
import com.hungle.sunriise.viewer.spring.MnyViewerProperty;
import com.hungle.sunriise.viewer.spring.MnyViewerSpringApplication;
import com.hungle.sunriise.viewer.table.JackcessTableUtils;
import com.hungle.sunriise.viewer.table.MnyTableModel;
import com.hungle.sunriise.viewer.table.TableUtils;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.FormSpecs;
import com.jgoodies.forms.layout.RowSpec;

// TODO: Auto-generated Javadoc
/**
 * The Class MnyViewer.
 */
public class MnyViewer {

    /** The Constant log. */
    private static final Logger LOGGER = org.apache.logging.log4j.LogManager.getLogger(MnyViewer.class);

    /** The Constant prefs. */
    private static final Preferences prefs = Preferences.userNodeForPackage(MnyViewer.class);

    /** The Constant threadPool. */
    private static final Executor threadPool = Executors.newCachedThreadPool();

    /** The Constant TITLE_NO_OPENED_DB. */
    public static final String TITLE_NO_OPENED_DB = "No opened db";

    /** The frame. */
    private JFrame frame;

    /** The opened db. */
    private MnyDb mnyDb = new MnyDb();

    /** The data model. */
    private MnyViewerDataModel dataModel = new MnyViewerDataModel();

    /** The table. */
    private JTable table;

    /** The table model. */
    private MnyTableModel tableModel;

    /** The text field. */
    private JTextField textField;

    /** The db read only. */
    private boolean dbReadOnly = true;

    // private Pattern tableNamePattern =
    /** The text area. */
    // Pattern.compile("^(.*) \\([0-9]+\\)$");
    private JTextArea metaDataTextArea;

    /** The header text area. */
    private JTextArea headerTextArea;

    /** The duplicate menu item. */
    private JMenuItem duplicateMenuItem;

    /** The delete menu item. */
    private JMenuItem deleteMenuItem;

    /** The key info text area. */
    private JTextArea keyInfoTextArea;

    /** The index info text area. */
    private JTextArea indexInfoTextArea;

    /** The filter text field. */
    private JTextField filterTextField;

    /** The sorter. */
    private TableRowSorter<TableModel> sorter;

    /** The allow table sorting. */
    private boolean allowTableSorting = false;

    /** The filter on selected column check box. */
    private JCheckBox filterOnSelectedColumnCheckBox;

    /** The right status label. */
    private JLabel rightStatusLabel;

    /** The select row values1. */
    private Map<String, Object> selectRowValues1 = null;

    /** The select row values2. */
    private Map<String, Object> selectRowValues2 = null;

    /** The tools menu. */
    private JMenu toolsMenu;

    /** The goto column menu. */
    private JMenu gotoColumnMenu;

    /** The label column index. */
    private int labelColumnIndex;

    /** The setting new sorter. */
    private AtomicBoolean settingNewSorter = new AtomicBoolean(false);

    /** The filter ccp popup menu. */
    private JPopupMenu filterCcpPopupMenu;

    /** The table names list. */
    private JList<Object> tableNamesList;

    /** The export menu. */
    private JMenu exportMenu;

    /** The instance. */
    private static MnyViewer instance;
    
    /** The swing property change support. */
    private final SwingPropertyChangeSupport swingPropertyChangeSupport;

    /** The mny http server. */
    private final MnyHttpServer mnyHttpServer;

//    private JMenuItem diskUsageMenuItem;

    /** The disk usage menu. */
private JMenu diskUsageMenu;

    /**
     * The Class MnyViewerOpenDbAction.
     */
    private final class MnyViewerOpenDbAction extends OpenDbAction {
        
        /**
         * Instantiates a new mny viewer open db action.
         *
         * @param locationRelativeTo the location relative to
         * @param prefs the prefs
         * @param mnyDb the mny db
         */
        private MnyViewerOpenDbAction(Component locationRelativeTo, Preferences prefs, MnyDb mnyDb) {
            super(locationRelativeTo, prefs, mnyDb);
        }

        /**
         * Db file opened.
         *
         * @param newMnyDb the new mny db
         * @param dialog the dialog
         */
        @Override
        public void dbFileOpened(MnyDb newMnyDb, OpenDbDialog dialog) {
            boolean readOnly = dialog.getReadOnlyCheckBox().isSelected();

            openMnyDb(newMnyDb, readOnly);
        }
    }

    /**
     * The Class OpenMnyTestFileAction.
     */
    private final class OpenMnyTestFileAction extends AbstractAction {

        /** The db file. */
        private MnySampleFile mnySampleFile;

        /** The Constant serialVersionUID. */
        private static final long serialVersionUID = 1L;

        /**
         * Instantiates a new open mny test file action.
         *
         * @param mnySampleFile the db file
         */
        public OpenMnyTestFileAction(MnySampleFile mnySampleFile) {
            this.mnySampleFile = mnySampleFile;
        }

        /**
         * Action performed.
         *
         * @param event the event
         */
        /*
         * (non-Javadoc)
         * 
         * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.
         * ActionEvent)
         */
        @Override
        public void actionPerformed(ActionEvent event) {
            File sampleFile = MnySampleFile.getSampleFileFromModuleProject(mnySampleFile);

            MnyDb mnyDb;
            try {
                mnyDb = new MnyDb(sampleFile, mnySampleFile.getPassword());
                openMnyDb(mnyDb, true);
            } catch (IOException e) {
                LOGGER.error(e);
            }
        }

    }

    /**
     * Gets the tables.
     *
     * @return the tables
     * @throws IOException Signals that an I/O exception has occurred.
     */
    private List<TableListItem> getTables() throws IOException {
        List<TableListItem> tables = new ArrayList<TableListItem>();

        Database db = getDb();

        DatabaseUtils.logDbInfo(db);

        Set<String> tableNames = db.getTableNames();
        for (String tableName : tableNames) {
            try {
                Table table = db.getTable(tableName);
                TableListItem item = new TableListItem();
                item.setTable(table);
                tables.add(item);
            } catch (IOException e) {
                LOGGER.warn(e);
            }
        }

        boolean showSystemCatalog = true;
        if (showSystemCatalog) {
            Table table = JackcessImplUtils.getSystemCatalog(db);
            if (table != null) {
                TableListItem item = new TableListItem();
                item.setTable(table);
                tables.add(item);
            }
        }
        return tables;
    }

    /**
     * Launch the application.
     *
     * @param args the arguments
     */
    public static void main(String[] args) {
        // EventQueue waitQueue = new WaitCursorEventQueue(500);
        // Toolkit.getDefaultToolkit().getSystemEventQueue().push(waitQueue);

        // String builNumber = BuildNumber.findBuilderNumber();

        EventQueue.invokeLater(new Runnable() {

            @Override
            public void run() {
                try {
                    JavaInfo.logInfo();

                    LOGGER.info("> Starting MynViewer");
                    MnyViewer mnyViewer = new MnyViewer();
                    MnyViewer.setInstance(mnyViewer);

                    showMainFrame(mnyViewer);

                    String buildNumber = SunriiseBuildNumber.getBuildnumber();
                    LOGGER.info("BuildNumber: " + buildNumber);

                    MnyViewer.threadPool.execute(new Runnable() {
                        @Override
                        public void run() {
                            boolean httpServer = Boolean.valueOf(System.getProperty("httpServer", "false"));
                            LOGGER.info("httpServer={}", httpServer);
                            if (httpServer) {
                                mnyViewer.mnyHttpServer.restartHttpServer(args);
                            }
                        }
                    });

                } catch (Exception e) {
                    LOGGER.error(e, e);
                }
            }

            protected void showMainFrame(MnyViewer mnyViewer) {
                JFrame mainFrame = mnyViewer.getFrame();

                String title = com.hungle.sunriise.viewer.MnyViewer.TITLE_NO_OPENED_DB;
                mainFrame.setTitle(title);

                Dimension preferredSize = new Dimension(900, 700);
                mainFrame.setPreferredSize(preferredSize);

                mainFrame.pack();

                mainFrame.setLocationRelativeTo(null);

                mainFrame.setVisible(true);
                LOGGER.info("setVisible to true");
            }
        });
    }

    /**
     * Create the application.
     */
    public MnyViewer() {
        initialize();

        this.swingPropertyChangeSupport = new SwingPropertyChangeSupport(this, true);

        this.mnyHttpServer = new MnyHttpServer(MnyViewerSpringApplication.class);
    }

    /**
     * Change right status text.
     *
     * @param text the text
     */
    public void changeRightStatusText(String text) {
        this.rightStatusLabel.setText(text);
    }

    /**
     * Initialize the contents of the frame.
     */
    private void initialize() {
        setFrame(new JFrame());
        // getFrame().setBounds(100, 100, 800, 600);
        getFrame().setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        getFrame().addWindowListener(new WindowAdapter() {

            @Override
            public void windowClosing(WindowEvent e) {
                super.windowClosing(e);
                LOGGER.info("> windowClosing");
                if (getMnyDb() != null) {
                    appClosed();
                }
            }

            @Override
            public void windowClosed(WindowEvent e) {
                super.windowClosed(e);
                LOGGER.info("> windowClosed");
            }
        });

        JMenuBar menuBar = new JMenuBar();
        getFrame().setJMenuBar(menuBar);
        getFrame().setTitle(com.hungle.sunriise.viewer.MnyViewer.TITLE_NO_OPENED_DB);
        JMenu mnNewMenu = new JMenu("File");
        menuBar.add(mnNewMenu);

        JMenuItem mntmNewMenuItem = new JMenuItem("Exit");
        mntmNewMenuItem.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent event) {
                if (getMnyDb() != null) {
                    appClosed();
                }
                LOGGER.info("XXX User selects File -> Exit");
                System.exit(0);
            }
        });

        JMenuItem mntmNewMenuItem_1 = new JMenuItem("Open");
        mntmNewMenuItem_1.addActionListener(new MnyViewerOpenDbAction(MnyViewer.this.getFrame(), prefs, getMnyDb()));
        mnNewMenu.add(mntmNewMenuItem_1);

        exportMenu = new JMenu("Export DB");
        exportMenu.setEnabled(false);
        mnNewMenu.add(exportMenu);

        ExportToContext exportToContext = null;

        JMenuItem mntmNewMenuItem_3 = new JMenuItem("To CSV");
        exportToContext = new ExportToContext() {

            @Override
            public Component getParentComponent() {
                return getFrame();
            }

            @Override
            public MnyDb getMnyDb() {
                return MnyViewer.this.getMnyDb();
            }

            @Override
            public Executor getThreadPool() {
                return MnyViewer.getThreadPool();
            }
        };
        mntmNewMenuItem_3.addActionListener(new ExportToCsvAction(exportToContext));
        exportMenu.add(mntmNewMenuItem_3);

        JMenuItem mntmNewMenuItem_4 = new JMenuItem("To *.mdb");
        exportToContext = new ExportToContext() {

            @Override
            public Component getParentComponent() {
                return getFrame();
            }

            @Override
            public MnyDb getMnyDb() {
                return MnyViewer.this.getMnyDb();
            }

            @Override
            public Executor getThreadPool() {
                return MnyViewer.getThreadPool();
            }
        };
        mntmNewMenuItem_4.addActionListener(new ExportToMdbAction(exportToContext));
        exportMenu.add(mntmNewMenuItem_4);

        JMenuItem mntmTojson = new JMenuItem("To *.json");
        exportToContext = new ExportToContext() {

            @Override
            public Component getParentComponent() {
                return getFrame();
            }

            @Override
            public MnyDb getMnyDb() {
                return MnyViewer.this.getMnyDb();
            }

            @Override
            public Executor getThreadPool() {
                return MnyViewer.getThreadPool();
            }
        };
        mntmTojson.addActionListener(new ExportToJSONAction(exportToContext));
        exportMenu.add(mntmTojson);

        JSeparator separator_1 = new JSeparator();
        mnNewMenu.add(separator_1);
        mnNewMenu.add(mntmNewMenuItem);

        AbstractSamplesMenu abstractSamplesMenu = new AbstractSamplesMenu() {

            @Override
            protected void mnyDbSelected(MnyDb newMnyDb) {
                openMnyDb(newMnyDb, true);
            }
        };
        abstractSamplesMenu.addSamplesMenu(menuBar);

//        File sampleTopDir = new File("../sunriise-core");
//        if (sampleTopDir.exists() && (sampleTopDir.isDirectory())) {
//            JMenu sampleMenu = new JMenu("Samples");
//            menuBar.add(sampleMenu);
//
//            for (MnySampleFile dbFile : MnySampleFile.SAMPLE_FILES) {
//                if (dbFile.isBackup()) {
//                    continue;
//                }
//
//                File sampleFile = MnySampleFile.getSampleFileFromModuleProject(dbFile);
//
//                String fileName = sampleFile.getName();
//
//                JMenuItem menuItem = new JMenuItem(fileName);
//                sampleMenu.add(menuItem);
//                menuItem.addActionListener(new OpenMnyTestFileAction(dbFile));
//            }
//        }

        addToolsMenu(menuBar);

        // Launcher.addHelpMenu(frame, menuBar);

        JPanel statusPane = new JPanel();
        frame.getContentPane().add(statusPane, BorderLayout.SOUTH);
        statusPane.setLayout(new BorderLayout(0, 0));

        rightStatusLabel = new JLabel("...");
        // rightStatusLabel = new JTextField(50);
        // rightStatusLabel.setEditable(false);
        int alignment = SwingConstants.RIGHT;
        rightStatusLabel.setHorizontalAlignment(alignment);
        statusPane.add(rightStatusLabel, BorderLayout.EAST);

        JSplitPane splitPane = new JSplitPane();
        splitPane.setResizeWeight(0.33);
        splitPane.setDividerLocation(0.33);
        getFrame().getContentPane().add(splitPane, BorderLayout.CENTER);

        JPanel leftView = new JPanel();
        leftView.setPreferredSize(new Dimension(80, -1));
        splitPane.setLeftComponent(leftView);
        leftView.setLayout(new BorderLayout(0, 0));

        JScrollPane scrollPane = new JScrollPane();
        leftView.add(scrollPane);

        ListModel<Object> tableNamesModel = new AbstractListModel<Object>() {

            /**
             * 
             */
            private static final long serialVersionUID = -5124249465195778867L;

            @Override
            public int getSize() {
                // TODO Auto-generated method stub
                return 0;
            }

            @Override
            public Object getElementAt(int index) {
                // TODO Auto-generated method stub
                return null;
            }
        };
        tableNamesList = new JList<Object>(tableNamesModel);
        tableNamesList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        tableNamesList.addListSelectionListener(createTableNameSelectedListener());
        tableNamesList.setVisibleRowCount(-1);
        scrollPane.setViewportView(tableNamesList);

        JPanel panel = new JPanel();
        splitPane.setRightComponent(panel);
        panel.setLayout(new BorderLayout(0, 0));

        JPanel panel_1 = new JPanel();
        panel.add(panel_1, BorderLayout.NORTH);
        // panel_1.setPreferredSize(new Dimension(100, 100));
        panel_1.setLayout(new FormLayout(
                new ColumnSpec[] { FormSpecs.UNRELATED_GAP_COLSPEC, FormSpecs.DEFAULT_COLSPEC,
                        FormSpecs.LABEL_COMPONENT_GAP_COLSPEC, ColumnSpec.decode("default:grow"),
                        FormSpecs.UNRELATED_GAP_COLSPEC, },
                new RowSpec[] { FormSpecs.RELATED_GAP_ROWSPEC, FormSpecs.DEFAULT_ROWSPEC,
                        FormSpecs.RELATED_GAP_ROWSPEC, }));

        JLabel lblNewLabel = new JLabel("Table Name");
        panel_1.add(lblNewLabel, "2, 2, right, default");

        textField = new JTextField();
        textField.setEditable(false);
        panel_1.add(textField, "4, 2, fill, default");
        textField.setColumns(10);

        JTabbedPane tabbedPane = new JTabbedPane(SwingConstants.TOP);
//        panel.add(tabbedPane, BorderLayout.CENTER);

        JPopupMenu tablePopupMenu = createTablePopupMenu();
        MouseListener tablePopupListener = new PopupListener(tablePopupMenu);

        JPanel panel_6 = new JPanel();
        tabbedPane.addTab("Rows", null, panel_6, null);
        panel_6.setLayout(new BorderLayout(0, 0));

        JScrollPane scrollPane_1 = new JScrollPane();
        panel_6.add(scrollPane_1);

        table = new JTable() {

            @Override
            public void valueChanged(ListSelectionEvent e) {
                super.valueChanged(e);

                if (e.getValueIsAdjusting()) {
                    return;
                }

                if (settingNewSorter.get()) {
                    return;
                }

                ListSelectionModel model = (ListSelectionModel) e.getSource();
                int selectedIndex = model.getMinSelectionIndex();

                updateRowLabel(selectedIndex, null);
            }

            @Override
            public void setModel(TableModel dataModel) {
                super.setModel(dataModel);
                TableColumnModel columnModel = this.getColumnModel();
                int cols = columnModel.getColumnCount();

                MnyTableModel mnyTableModel = MnyViewer.this.tableModel;
                // IndexLookup indexLookup = new IndexLookup();

                for (int i = 0; i < cols; i++) {
                    TableColumn column = columnModel.getColumn(i);
                    MyTableCellRenderer renderer = new MyTableCellRenderer(column.getCellRenderer());
                    column.setCellRenderer(renderer);

                    if (mnyTableModel.columnIsDateType(i)) {
                        if (LOGGER.isDebugEnabled()) {
                            LOGGER.debug("columnIsDateType, i=" + i);
                        }
                        // TableCellEditor cellEditor = new
                        // TableCellDateEditor();
                        // TableCellEditor cellEditor = new
                        // DatePickerTableEditor();
                        TableCellEditor cellEditor = new DialogCellEditor();
                        column.setCellEditor(cellEditor);
                    }

                    if (mnyTableModel.isPrimaryKeyColumn(i)) {
                        TableCellRenderer headerRenderer = new MyTabletHeaderRenderer(table, column.getHeaderRenderer(),
                                Color.RED);
                        column.setHeaderRenderer(headerRenderer);
                    }
                    if (mnyTableModel.isForeignKeyColumn(i)) {
                        TableCellRenderer headerRenderer = new MyTabletHeaderRenderer(table, column.getHeaderRenderer(),
                                Color.BLUE);
                        column.setHeaderRenderer(headerRenderer);
                    }
                }
            }

        };
        // table.setAutoCreateRowSorter(true);
        // RowSorter<TableModel> sorter = new
        // TableRowSorter<TableModel>(tableModel);
        // table.setRowSorter(sorter);

        table.setDefaultRenderer(Date.class, new DateCellRenderer());
        table.addMouseListener(tablePopupListener);
        table.addMouseListener(new MouseAdapter() {

            @Override
            public void mousePressed(MouseEvent e) {
                super.mousePressed(e);

                Point point = e.getPoint();
                int rowIndex = table.rowAtPoint(point);
                int columnIndex = table.columnAtPoint(point);

                // if (labelColumnIndex >= 0) {
                // columnIndex = labelColumnIndex;
                // }
                // rowIndex = getRowIndex(rowIndex);
                // Object value = tableModel.getValueAt(rowIndex, columnIndex);
                // String text = value.toString();
                // rightStatusLabel.setText(text);
            }

        });

        // TODO: try to install our mouselistener first
        // insertListenerToHead();

        table.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
        scrollPane_1.setViewportView(table);

        JPanel panel_7 = new JPanel();
        panel_7.setBorder(new EmptyBorder(3, 3, 3, 3));
        panel_6.add(panel_7, BorderLayout.SOUTH);
        panel_7.setLayout(new BoxLayout(panel_7, BoxLayout.LINE_AXIS));

        JLabel lblNewLabel_1 = new JLabel("Filter Text");
        panel_7.add(lblNewLabel_1);

        JPopupMenu filterPopupMenu = new JPopupMenu();
        JCheckBoxMenuItem checkBoxMenuItem = new JCheckBoxMenuItem(new AbstractAction("Enable Sorting/Filtering") {

            @Override
            public void actionPerformed(ActionEvent e) {
                AbstractButton aButton = (AbstractButton) e.getSource();
                boolean selected = aButton.getModel().isSelected();
                if (selected == allowTableSorting) {
                    // no change
                    LOGGER.warn("No change in allowTableSorting=" + allowTableSorting);
                    return;
                }

                allowTableSorting = selected;

                toggleTableSorting();
            }
        });
        checkBoxMenuItem.setSelected(allowTableSorting);
        filterPopupMenu.add(checkBoxMenuItem);
        lblNewLabel_1.addMouseListener(new PopupListener(filterPopupMenu));

        Component horizontalStrut = Box.createHorizontalStrut(5);
        panel_7.add(horizontalStrut);

        filterTextField = new JTextField();
        filterTextField.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent event) {
                JTextField tf = (JTextField) event.getSource();
                String text = tf.getText();

                RowFilter<Object, Object> rf = null;
                if ((text == null) || (text.length() <= 0)) {
                    rf = null;
                } else {
                    // If current expression doesn't parse, don't update.
                    try {
                        if (filterOnSelectedColumnCheckBox.isSelected()) {
                            int selectedColumn = table.getSelectedColumn();
                            if (selectedColumn >= 0) {
                                LOGGER.info("filter for text=" + text + ", selectedColumn=" + selectedColumn);
                                rf = RowFilter.regexFilter(text, selectedColumn);
                            } else {
                                LOGGER.info("filter for text=" + text);
                                rf = RowFilter.regexFilter(text);
                            }
                        } else {
                            LOGGER.info("filter for text=" + text);
                            rf = RowFilter.regexFilter(text);
                        }
                    } catch (java.util.regex.PatternSyntaxException e) {
                        LOGGER.warn(e);
                        return;
                    }
                }
                if (sorter != null) {
                    StopWatch stopwatch = new StopWatch();
                    int preViewRowCount = sorter.getViewRowCount();
                    try {
                        LOGGER.info("> setRowFilter");

                        boolean background = true;
                        if (background) {
                            int parties = 2;
                            Runnable barrierAction = new Runnable() {

                                @Override
                                public void run() {
                                    LOGGER.info("Background filtering thread is DONE.");
                                }
                            };
                            final CyclicBarrier barrier = new CyclicBarrier(parties, barrierAction);
                            final RowFilter<Object, Object> rf2 = rf;

                            Runnable command = new Runnable() {

                                @Override
                                public void run() {
                                    try {
                                        sorter.setRowFilter(rf2);
                                        barrier.await();
                                    } catch (InterruptedException e) {
                                        LOGGER.warn(e);
                                    } catch (BrokenBarrierException e) {
                                        LOGGER.warn(e);
                                    }
                                }
                            };

                            Component parent = SwingUtilities.getRoot(MnyViewer.this.frame);
                            Cursor waitCursor = TableUtils.setWaitCursor(parent);
                            try {
                                getThreadPool().execute(command);

                                barrier.await();
                            } catch (InterruptedException e) {
                                LOGGER.warn(e);
                            } catch (BrokenBarrierException e) {
                                LOGGER.warn(e);
                            } finally {
                                TableUtils.clearWaitCursor(parent, waitCursor);
                            }
                        } else {
                            sorter.setRowFilter(rf);
                        }
                    } finally {
                        long delta = stopwatch.click();
                        int postViewRowCount = sorter.getViewRowCount();
                        rightStatusLabel.setText(
                                "filter: rows=" + postViewRowCount + "/" + preViewRowCount + ", millisecond=" + delta);
                        LOGGER.info("< setRowFilter, delta=" + delta);
                    }
                }
            }
        });
        filterCcpPopupMenu = new JPopupMenu();
        MenuUtils.populateCCPMenu(filterCcpPopupMenu);
        filterTextField.addMouseListener(new PopupListener(filterCcpPopupMenu) {

            @Override
            public void mousePressed(MouseEvent e) {
                if (!filterTextField.isEnabled()) {
                    return;
                }
                super.mousePressed(e);
            }

            @Override
            public void mouseReleased(MouseEvent e) {
                if (!filterTextField.isEnabled()) {
                    return;
                }

                super.mouseReleased(e);
            }

        });
        panel_7.add(filterTextField);
        filterTextField.setColumns(10);

        Component horizontalStrut_1 = Box.createHorizontalStrut(5);
        panel_7.add(horizontalStrut_1);

        filterOnSelectedColumnCheckBox = new JCheckBox("on selected column");
        filterOnSelectedColumnCheckBox.setSelected(true);
        panel_7.add(filterOnSelectedColumnCheckBox);

        toggleTableSorting();

        createMetaDataView(tabbedPane);

        createHeaderView(tabbedPane);

        createKeysView(tabbedPane);

        createIndexesView(tabbedPane);

        Component topComponent = tabbedPane;
        Component bottomComponent = new JPanel();
        JSplitPane splitPane2 = new JSplitPane(JSplitPane.VERTICAL_SPLIT, topComponent, bottomComponent);
        splitPane2.setResizeWeight(0.50);
        splitPane2.setDividerLocation(0.50);
        panel.add(splitPane2, BorderLayout.CENTER);

        initDataBindings();
    }

    /**
     * Adds the tools menu.
     *
     * @param menuBar the menu bar
     */
    private void addToolsMenu(JMenuBar menuBar) {
        toolsMenu = new JMenu("Tools");
        menuBar.add(toolsMenu);
        
        diskUsageMenu = new JMenu("Disk usage");
        toolsMenu.add(diskUsageMenu);

        JMenuItem diskUsageMenuItem = new JMenuItem(new AbstractAction("Show") {
            public void actionPerformed(ActionEvent event) {
                Component parentComponent = MnyViewer.this.getFrame();
                Object message = null;

                MnyDb mnyDb = MnyViewer.this.getMnyDb();
                if ((mnyDb != null) && (mnyDb.getDb() != null)) {
                    try {
                        DiskUsageView view = new DiskUsageView(mnyDb);
                        message = view;
                    } catch (IOException e) {
                        message = e.getMessage();
                        LOGGER.error(e.getMessage());
                    }
                    if (message == null) {
                        message = "No data available";
                    }
                } else {
                    message = "No db data available";
                }

                JOptionPane.showMessageDialog(parentComponent, message, "Disk usage", JOptionPane.INFORMATION_MESSAGE);
            }
        });
        diskUsageMenu.setEnabled(false);
        diskUsageMenu.add(diskUsageMenuItem);
    }

    /**
     * Creates the indexes view.
     *
     * @param tabbedPane the tabbed pane
     */
    private void createIndexesView(JTabbedPane tabbedPane) {
        JPanel panel_5 = new JPanel();
        tabbedPane.addTab("Indexes", null, panel_5, null);
        panel_5.setLayout(new BorderLayout(0, 0));

        RSyntaxTextArea textArea = new RSyntaxTextArea();
        // textArea.setSyntaxEditingStyle(SyntaxConstants.SYNTAX_STYLE_PROPERTIES_FILE);
        RTextScrollPane sp = new RTextScrollPane(textArea);

        // JScrollPane scrollPane_5 = new JScrollPane();
        // panel_5.add(scrollPane_5);
        panel_5.add(sp);

        // indexInfoTextArea = new JTextArea();
        indexInfoTextArea = textArea;
        // scrollPane_5.setViewportView(indexInfoTextArea);
    }

    /**
     * Creates the keys view.
     *
     * @param tabbedPane the tabbed pane
     */
    private void createKeysView(JTabbedPane tabbedPane) {
        JPanel panel_4 = new JPanel();
        tabbedPane.addTab("Keys", null, panel_4, null);
        panel_4.setLayout(new BorderLayout(0, 0));

        RSyntaxTextArea textArea = new RSyntaxTextArea();
        // textArea.setSyntaxEditingStyle(SyntaxConstants.SYNTAX_STYLE_PROPERTIES_FILE);
        RTextScrollPane sp = new RTextScrollPane(textArea);

        // JScrollPane scrollPane_4 = new JScrollPane();
        // panel_4.add(scrollPane_4, BorderLayout.CENTER);
        panel_4.add(sp, BorderLayout.CENTER);

        // keyInfoTextArea = new JTextArea();
        keyInfoTextArea = textArea;
        // scrollPane_4.setViewportView(keyInfoTextArea);
    }

    /**
     * Creates the header view.
     *
     * @param tabbedPane the tabbed pane
     */
    private void createHeaderView(JTabbedPane tabbedPane) {
        JPanel panel_3 = new JPanel();
        tabbedPane.addTab("Misc", null, panel_3, null);
        panel_3.setLayout(new BorderLayout(0, 0));

        RSyntaxTextArea textArea = new RSyntaxTextArea();
        textArea.setSyntaxEditingStyle(SyntaxConstants.SYNTAX_STYLE_PROPERTIES_FILE);
        RTextScrollPane sp = new RTextScrollPane(textArea);

        // JScrollPane scrollPane_3 = new JScrollPane();
        // panel_3.add(scrollPane_3, BorderLayout.CENTER);
        panel_3.add(sp, BorderLayout.CENTER);

        // headerTextArea = new JTextArea();
        headerTextArea = textArea;
        // scrollPane_3.setViewportView(headerTextArea);
    }

    /**
     * Creates the meta data view.
     *
     * @param tabbedPane the tabbed pane
     */
    private void createMetaDataView(JTabbedPane tabbedPane) {
        JPanel panel_2 = new JPanel();
        tabbedPane.addTab("Meta Data", null, panel_2, null);
        panel_2.setLayout(new BorderLayout(0, 0));

        RSyntaxTextArea textArea = new RSyntaxTextArea();
        textArea.setSyntaxEditingStyle(SyntaxConstants.SYNTAX_STYLE_JSON_WITH_COMMENTS);
        RTextScrollPane sp = new RTextScrollPane(textArea);

        // JScrollPane scrollPane_2 = new JScrollPane();
        // panel_2.add(scrollPane_2, BorderLayout.CENTER);
        panel_2.add(sp, BorderLayout.CENTER);

        // textArea = new JTextArea();
        metaDataTextArea = textArea;
        // scrollPane_2.setViewportView(textArea);
    }

    /**
     * Creates the table popup menu.
     *
     * @return the j popup menu
     */
    private JPopupMenu createTablePopupMenu() {
        JPopupMenu tablePopupMenu = new JPopupMenu();
        JMenuItem menuItem = null;
        menuItem = new JMenuItem(new AbstractAction("Duplicate") {

            @Override
            public void actionPerformed(ActionEvent e) {
                int rowIndex = table.getSelectedRow();
                duplicateRow(rowIndex);
            }
        });
        tablePopupMenu.add(menuItem);
        this.duplicateMenuItem = menuItem;

        tablePopupMenu.addSeparator();

        menuItem = new JMenuItem(new AbstractAction("Delete") {

            @Override
            public void actionPerformed(ActionEvent e) {
                int rowIndex = table.getSelectedRow();
                deleteRow(rowIndex);
            }
        });
        tablePopupMenu.add(menuItem);
        this.deleteMenuItem = menuItem;

        tablePopupMenu.addSeparator();

        menuItem = new JMenuItem(new AbstractAction("Copy Selected Cell Value") {

            @Override
            public void actionPerformed(ActionEvent e) {
                int rowIndex = table.getSelectedRow();
                int columnIndex = table.getSelectedColumn();
                copyColumn(rowIndex, columnIndex);
            }
        });
        tablePopupMenu.add(menuItem);

        menuItem = new JMenuItem(new AbstractAction("Select Column as Label") {

            @Override
            public void actionPerformed(ActionEvent e) {
                int rowIndex = table.getSelectedRow();
                int columnIndex = table.getSelectedColumn();

                if (LOGGER.isDebugEnabled()) {
                    LOGGER.debug("Select Column as Label: rowIndex=" + rowIndex + ", columnIndex=" + columnIndex);
                }

                selectColumnAsLabel(rowIndex, columnIndex);
            }
        });
        tablePopupMenu.add(menuItem);

        gotoColumnMenu = new JMenu("Scroll to Column");
        tablePopupMenu.add(gotoColumnMenu);

        tablePopupMenu.addSeparator();
        JMenu diffMenu = new JMenu("Diff Two Rows");
        menuItem = new JMenuItem(new AbstractAction("Select as Row 1") {

            @Override
            public void actionPerformed(ActionEvent event) {
                int rowIndex = table.getSelectedRow();
                selectAsRow1ForDiff(rowIndex);
            }
        });
        diffMenu.add(menuItem);
        menuItem = new JMenuItem(new AbstractAction("Select as Row 2") {

            @Override
            public void actionPerformed(ActionEvent event) {
                int rowIndex = table.getSelectedRow();
                JMenuItem source = (JMenuItem) event.getSource();
                selectAsRow2ForDiff(rowIndex, source);
            }
        });
        diffMenu.add(menuItem);
        tablePopupMenu.add(diffMenu);
        return tablePopupMenu;
    }

    /**
     * Creates the table name selected listener.
     *
     * @return the list selection listener
     */
    private ListSelectionListener createTableNameSelectedListener() {
        ListSelectionListener listSelectionListener = new ListSelectionListener() {

            @Override
            public void valueChanged(ListSelectionEvent event) {
                if (event.getValueIsAdjusting()) {
                    return;
                }
                JList<?> source = (JList<?>) event.getSource();

                try {
                    TableListItem item = (TableListItem) source.getSelectedValue();
                    if (item != null) {
                        tableSelected(item);
                    }
                } catch (IOException e) {
                    LOGGER.error(e);
                }
            }
        };

        return listSelectionListener;
    }

    /**
     * Clear data model.
     *
     * @param dataModel the data model
     */
    protected void clearDataModel(MnyViewerDataModel dataModel) {
        dataModel.clear();

    }

    /**
     * Copy column.
     *
     * @param rowIndex    the row index
     * @param columnIndex the column index
     */
    protected void copyColumn(int rowIndex, int columnIndex) {
        if (tableModel != null) {
            tableModel.copyColumn(getRowIndex(rowIndex), columnIndex);
        }
    }

    /**
     * Select column as label.
     *
     * @param rowIndex    the row index
     * @param columnIndex the column index
     */
    protected void selectColumnAsLabel(int rowIndex, int columnIndex) {
        setLabelColumnIndex(columnIndex);
    }

    /**
     * Gets the row index.
     *
     * @param rowIndex the row index
     * @return the row index
     */
    private int getRowIndex(int rowIndex) {
        return table.convertRowIndexToModel(rowIndex);
    }

    /**
     * Delete row.
     *
     * @param rowIndex the row index
     */
    protected void deleteRow(int rowIndex) {
        if (tableModel != null) {
            tableModel.deleteRow(getRowIndex(rowIndex));
        }
    }

    /**
     * Duplicate row.
     *
     * @param rowIndex the row index
     */
    protected void duplicateRow(int rowIndex) {
        if (tableModel != null) {
            tableModel.duplicateRow(getRowIndex(rowIndex), this.getFrame());
        }
    }

    /**
     * Sets the db.
     *
     * @param db the new db
     */
    public void setDb(Database db) {
        getMnyDb().setDb(db);
    }

    /**
     * Gets the db.
     *
     * @return the db
     */
    public Database getDb() {
        return getMnyDb().getDb();
    }

    /**
     * Sets the frame.
     *
     * @param frame the new frame
     */
    public void setFrame(JFrame frame) {
        this.frame = frame;
    }

    /**
     * Gets the frame.
     *
     * @return the frame
     */
    public JFrame getFrame() {
        return frame;
    }

    /**
     * Gets the thread pool.
     *
     * @return the thread pool
     */
    public static Executor getThreadPool() {
        return threadPool;
    }

    /**
     * Gets the opened db.
     *
     * @return the opened db
     */
    public MnyDb getMnyDb() {
        return mnyDb;
    }

    /**
     * Sets the opened db.
     *
     * @param mnyDb the new opened db
     */
    public void setMnyDb(MnyDb mnyDb) {
        this.mnyDb = mnyDb;
    }

    /**
     * Creates the table row sorter.
     *
     * @param tableModel the table model
     * @return the table row sorter
     */
    private TableRowSorter<TableModel> createTableRowSorter(final MnyTableModel tableModel) {
        TableRowSorter<TableModel> sorter = new MnyTableRowSorter(this, tableModel, tableModel);
        RowSorterListener listener = new MnyRowSorterListener();
        sorter.addRowSorterListener(listener);
        return sorter;
    }

    /**
     * Toggle table sorting.
     */
    private void toggleTableSorting() {
        if (allowTableSorting) {
            filterTextField.setEnabled(true);
            filterTextField.setText("");
            filterOnSelectedColumnCheckBox.setEnabled(true);

            if (tableModel != null) {
                LOGGER.info("creating new sorter ...");
                sorter = createTableRowSorter(tableModel);
                LOGGER.info("DONE - creating new sorter ...");

                setNewRowSorter(sorter);
            }
        } else {
            filterTextField.setEnabled(false);
            filterTextField.setText("Filter is disable");
            filterOnSelectedColumnCheckBox.setEnabled(false);

            if (tableModel != null) {
                sorter = null;
                setNewRowSorter(sorter);
            }
        }
    }

    /**
     * Sets the new row sorter.
     *
     * @param sorter the new new row sorter
     */
    private void setNewRowSorter(TableRowSorter<TableModel> sorter) {
        settingNewSorter.set(true);
        try {
            MnyViewer.this.table.setRowSorter(sorter);
        } finally {
            settingNewSorter.set(false);
        }
    }

    /**
     * Select as row1 for diff.
     *
     * @param rowIndex the row index
     */
    private void selectAsRow1ForDiff(int rowIndex) {
        rowIndex = getRowIndex(rowIndex);

        LOGGER.info("Selected rowIndex=" + rowIndex + " as row #1");
        try {
            Map<String, Object> rowValues = tableModel.getRowValues(rowIndex);
            selectRowValues1 = rowValues;
        } catch (IOException e) {
            LOGGER.error(e, e);
        }
    }

    /**
     * Select as row2 for diff.
     *
     * @param rowIndex the row index
     * @param source   the source
     */
    private void selectAsRow2ForDiff(int rowIndex, JComponent source) {

        rowIndex = getRowIndex(rowIndex);

        LOGGER.info("Selected rowIndex=" + rowIndex + " as row #2");
        try {
            Map<String, Object> rowValues = tableModel.getRowValues(rowIndex);
            selectRowValues2 = rowValues;
            if (selectRowValues1 != null) {
                int n1 = selectRowValues1.size();
                int n2 = selectRowValues2.size();
                if (n1 != n2) {
                    LOGGER.warn("Two rows do not have same size!");
                    return;
                }
                List<DiffData> diffs = diffRowsValues(selectRowValues1, selectRowValues2);

                if (LOGGER.isDebugEnabled()) {
                    for (DiffData diff : diffs) {
                        if (diff.getValue1() instanceof byte[]) {
                            LOGGER.debug("DIFF columm=" + diff.getKey() + ", value1=" + "byte[]-instance" + ", value2="
                                    + "byte[]-instance");
                        } else {
                            LOGGER.debug("DIFF columm=" + diff.getKey() + ", value1=" + diff.getValue1() + ", value2="
                                    + diff.getValue2());
                        }
                    }
                }
                Component parentComponent = frame;
                final JTextArea textArea = new JTextArea();
                // textArea.setFont(new Font("Sans-Serif", Font.PLAIN, 10));
                textArea.setEditable(false);
                StringBuilder sb = new StringBuilder();
                for (DiffData diff : diffs) {
                    if (diff.getValue1() instanceof byte[]) {
                        sb.append("columm=" + diff.getKey() + ", value1=" + "byte[]-instance" + ", value2="
                                + "byte[]-instance");
                        sb.append("\n");
                        // sb.append("\n");
                    } else {
                        sb.append("columm=" + diff.getKey() + ", value1=" + diff.getValue1() + ", value2="
                                + diff.getValue2());
                        sb.append("\n");
                        // sb.append("\n");
                    }
                }
                textArea.setText(sb.toString());
                textArea.setCaretPosition(0);

                // stuff it in a scrollpane with a controlled size.
                JScrollPane scrollPane = new JScrollPane(textArea);
                scrollPane.setPreferredSize(new Dimension(350, 150));
                Object message = scrollPane;
                JOptionPane.showMessageDialog(parentComponent, message, "Diff Result", JOptionPane.INFORMATION_MESSAGE);
            } else {
                Component parentComponent = frame;
                String message = "Please select a first row";
                int messageType = JOptionPane.ERROR_MESSAGE;
                String title = "Missing required arguments";
                JOptionPane.showMessageDialog(parentComponent, message, title, messageType);
            }
        } catch (IOException e) {
            LOGGER.error(e, e);
        }
    }

    /**
     * Diff rows values.
     *
     * @param rowValues1 the row values1
     * @param rowValues2 the row values2
     * @return the list
     */
    private List<DiffData> diffRowsValues(Map<String, Object> rowValues1, Map<String, Object> rowValues2) {
        List<DiffData> diffs = new ArrayList<DiffData>();
        for (String key : rowValues1.keySet()) {

            Object value1 = rowValues1.get(key);
            Object value2 = rowValues2.get(key);

            if (LOGGER.isDebugEnabled()) {
                LOGGER.debug("key=" + key + ", value1=" + value1 + ", value1=" + value2);
            }

            boolean same = TableUtils.rowValuesAreSame(value1, value2);

            if (!same) {
                DiffData diffData = new DiffData(key, value1, value2);
                diffs.add(diffData);
            }
        }
        return diffs;
    }

    /**
     * Table selected.
     *
     * @param item the item
     * @throws IOException Signals that an I/O exception has occurred.
     */
    private void tableSelected(TableListItem item) throws IOException {
        final Table jackcessTable = item.getTable();
        String tableName = jackcessTable.getName();
        LOGGER.info("> new table is selected, table=" + tableName);

        getDataModel().setTable(jackcessTable);
        getDataModel().setTableName(tableName);
        getDataModel().setTableMetaData(JackcessTableUtils.parseTableMetaData(jackcessTable));
        getDataModel().setHeaderInfo(JackcessTableUtils.parseHeaderInfo(jackcessTable, getMnyDb()));
        getDataModel().setKeyInfo(JackcessTableUtils.parseKeyInfo(jackcessTable));
        getDataModel().setIndexInfo(JackcessTableUtils.parseIndexInfo(jackcessTable));
        selectRowValues1 = null;
        selectRowValues2 = null;

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("clearing old filter text ...");
        }
        filterTextField.setText("");
        if (tableModel != null) {
            try {
                tableModel.close();
            } finally {
                tableModel = null;
            }
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("creating new tableModel ...");
        }
        tableModel = new MnyTableModel(jackcessTable);
        tableModel.setDbReadOnly(dbReadOnly);

        toggleTableSorting();

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("setting new tableModel ...");
        }
        getDataModel().setTableModel(tableModel);

        MenuUtils.updateGotoColumnMenu(table, gotoColumnMenu);
        setLabelColumnIndex(jackcessTable);

        rightStatusLabel.setText("open table=" + jackcessTable.getName());
    }

    /**
     * Sets the label column index.
     *
     * @param jackcessTable the new label column index
     */
    private void setLabelColumnIndex(Table jackcessTable) {
        labelColumnIndex = -1;

        int index = JackcessTableUtils.findDefaultLabelColumnIndex(jackcessTable, tableModel);
        labelColumnIndex = index;

        setLabelColumnIndex(labelColumnIndex);
    }

    /**
     * Sets the label column index.
     *
     * @param columnIndex the new label column index
     */
    private void setLabelColumnIndex(int columnIndex) {
        labelColumnIndex = columnIndex;
        String columnName = getDataModel().getTableModel().getColumnName(columnIndex);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("setLabelColumnIndex, labelColumnIndex=" + labelColumnIndex + ", columnName=" + columnName);
        }

        int rowIndex = table.getSelectedRow();
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("setLabelColumnIndex, rowIndex=" + rowIndex);
        }
        if (rowIndex >= 0) {
            updateRowLabel(rowIndex, columnName);
        }
    }

    /**
     * Update row label.
     *
     * @param rowIndex   the row index
     * @param columnName the column name
     */
    private void updateRowLabel(int rowIndex, String columnName) {
        if (rowIndex < 0) {
            return;
        }

        int columnIndex = 0;
        if (labelColumnIndex >= 0) {
            columnIndex = labelColumnIndex;
        }
        rowIndex = getRowIndex(rowIndex);

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("updateRowLabel: rowIndex=" + rowIndex + ", columnIndex=" + columnIndex);
        }

        if ((columnIndex >= 0) && (columnIndex >= 0)) {
            Object value = tableModel.getValueAt(rowIndex, columnIndex);
            String valueStr = null;
            if (value != null) {
                valueStr = value.toString();
            } else {
                valueStr = "";
            }
            if (columnName != null) {
                rightStatusLabel.setText(columnName + "=" + valueStr);
            } else {
                rightStatusLabel.setText(valueStr);
            }
        }
    }

    /**
     * App closed.
     */
    protected void appClosed() {
        if (getMnyDb() != null) {
            try {
                getMnyDb().close();
            } catch (Exception e) {
                LOGGER.warn(e);
            }
        }
        setMnyDb(null);
    }

    /**
     * Toggle row sort order started.
     *
     * @param column the column
     */
    public void toggleRowSortOrderStarted(int column) {
        LOGGER.info("> toggleSortOrderStarted, column=" + column);
        tableModel.clearCache();
        tableModel.fillCacheForSorting(column);
    }

    /**
     * Inits the data bindings.
     */
    protected void initDataBindings() {
        BeanProperty<MnyViewerDataModel, TableModel> dataModelBeanProperty = BeanProperty.create("tableModel");
        ELProperty<JTable, Object> jTableEvalutionProperty = ELProperty.create("${model}");
        AutoBinding<MnyViewerDataModel, TableModel, JTable, Object> autoBinding = Bindings.createAutoBinding(
                UpdateStrategy.READ, getDataModel(), dataModelBeanProperty, table, jTableEvalutionProperty);
        autoBinding.bind();
        //
        BeanProperty<MnyViewerDataModel, String> dataModelBeanProperty_1 = BeanProperty.create("tableName");
        BeanProperty<JTextField, String> jTextFieldBeanProperty_1 = BeanProperty.create("text");
        AutoBinding<MnyViewerDataModel, String, JTextField, String> autoBinding_2 = Bindings.createAutoBinding(
                UpdateStrategy.READ, getDataModel(), dataModelBeanProperty_1, textField, jTextFieldBeanProperty_1);
        autoBinding_2.bind();
        //
        ELProperty<MnyViewerDataModel, Object> dataModelEvalutionProperty = ELProperty.create("${tableMetaData}");
        BeanProperty<JTextArea, String> jTextAreaBeanProperty = BeanProperty.create("text");
        AutoBinding<MnyViewerDataModel, Object, JTextArea, String> autoBinding_1 = Bindings.createAutoBinding(
                UpdateStrategy.READ, getDataModel(), dataModelEvalutionProperty, metaDataTextArea,
                jTextAreaBeanProperty);
        autoBinding_1.bind();
        //
        BeanProperty<MnyViewerDataModel, String> dataModelBeanProperty_2 = BeanProperty.create("headerInfo");
        BeanProperty<JTextArea, String> jTextAreaBeanProperty_1 = BeanProperty.create("text");
        AutoBinding<MnyViewerDataModel, String, JTextArea, String> autoBinding_3 = Bindings.createAutoBinding(
                UpdateStrategy.READ, getDataModel(), dataModelBeanProperty_2, headerTextArea, jTextAreaBeanProperty_1);
        autoBinding_3.bind();
        //
        BeanProperty<MnyViewerDataModel, String> dataModelBeanProperty_3 = BeanProperty.create("keyInfo");
        BeanProperty<JTextArea, String> jTextAreaBeanProperty_2 = BeanProperty.create("text");
        AutoBinding<MnyViewerDataModel, String, JTextArea, String> autoBinding_4 = Bindings.createAutoBinding(
                UpdateStrategy.READ, getDataModel(), dataModelBeanProperty_3, keyInfoTextArea, jTextAreaBeanProperty_2);
        autoBinding_4.bind();
        //
        BeanProperty<MnyViewerDataModel, String> dataModelBeanProperty_4 = BeanProperty.create("indexInfo");
        BeanProperty<JTextArea, String> jTextAreaBeanProperty_3 = BeanProperty.create("text");
        AutoBinding<MnyViewerDataModel, String, JTextArea, String> autoBinding_5 = Bindings.createAutoBinding(
                UpdateStrategy.READ, getDataModel(), dataModelBeanProperty_4, indexInfoTextArea,
                jTextAreaBeanProperty_3);
        autoBinding_5.bind();
        //
        BeanProperty<MnyViewerDataModel, List<TableListItem>> mnyViewerDataModelBeanProperty = BeanProperty
                .create("tables");
        JListBinding<TableListItem, MnyViewerDataModel, JList> jListBinding = SwingBindings.createJListBinding(
                UpdateStrategy.READ, getDataModel(), mnyViewerDataModelBeanProperty, tableNamesList);
        jListBinding.bind();
    }

    /**
     * Open opened db.
     *
     * @param newMnyDb the new opened db
     * @param readOnly the read only
     */
    private void openMnyDb(MnyDb newMnyDb, boolean readOnly) {
        this.dbReadOnly = readOnly;
        MnyDb oldMnyDb = getMnyDb();
        if (newMnyDb != null) {
            MnyViewer.this.setMnyDb(newMnyDb);
        }

        String title = TITLE_NO_OPENED_DB;
        File dbFile = getMnyDb().getDbFile();
        if (dbFile != null) {
            title = dbFile.getAbsolutePath();
        }
        getFrame().setTitle(title);

        List<TableListItem> tables = null;
        try {
            tables = getTables();
        } catch (IOException e) {
            LOGGER.warn(e);
        }

        LOGGER.info("Found " + tables.size() + " tables.");

        if (duplicateMenuItem != null) {
            duplicateMenuItem.setEnabled(!dbReadOnly);
        }
        if (deleteMenuItem != null) {
            deleteMenuItem.setEnabled(!dbReadOnly);
        }

        exportMenu.setEnabled(true);
        diskUsageMenu.setEnabled(true);

        MnyViewer.this.getDataModel().setTables(tables);

        clearDataModel(MnyViewer.this.getDataModel());

        String propertyName = MnyViewerProperty.MNY_DB.getPropertyName();
        getSwingPropertyChangeSupport().firePropertyChange(propertyName, oldMnyDb, newMnyDb);
    }

    /**
     * Gets the single instance of MnyViewer.
     *
     * @return single instance of MnyViewer
     */
    public static MnyViewer getInstance() {
        return instance;
    }

    /**
     * Sets the instance.
     *
     * @param instance the new instance
     */
    private static void setInstance(MnyViewer instance) {
        MnyViewer.instance = instance;
    }

    /**
     * Gets the data model.
     *
     * @return the data model
     */
    private MnyViewerDataModel getDataModel() {
        return dataModel;
    }

    /**
     * Sets the data model.
     *
     * @param dataModel the new data model
     */
    private void setDataModel(MnyViewerDataModel dataModel) {
        this.dataModel = dataModel;
    }

    /**
     * Gets the swing property change support.
     *
     * @return the swing property change support
     */
    public SwingPropertyChangeSupport getSwingPropertyChangeSupport() {
        return swingPropertyChangeSupport;
    }
}

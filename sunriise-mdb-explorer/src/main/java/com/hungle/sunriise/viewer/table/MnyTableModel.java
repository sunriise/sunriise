/*******************************************************************************
 * Copyright (c) 2010 Hung Le
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *******************************************************************************/
package com.hungle.sunriise.viewer.table;

import java.awt.Component;
import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.ClipboardOwner;
import java.awt.datatransfer.StringSelection;
import java.awt.datatransfer.Transferable;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.swing.table.AbstractTableModel;
import javax.swing.table.TableModel;

import org.apache.logging.log4j.Logger;

import com.healthmarketscience.jackcess.Column;
import com.healthmarketscience.jackcess.Cursor;
import com.healthmarketscience.jackcess.CursorBuilder;
import com.healthmarketscience.jackcess.DataType;
import com.healthmarketscience.jackcess.Table;
import com.healthmarketscience.jackcess.impl.ByteUtil;
import com.healthmarketscience.jackcess.impl.ColumnImpl;
import com.hungle.sunriise.util.IndexLookup;
import com.hungle.sunriise.util.JackcessImplUtils;
import com.hungle.sunriise.viewer.cell.CellValueCache;

// TODO: Auto-generated Javadoc
/**
 * The Class MnyTableModel.
 */
public class MnyTableModel extends AbstractTableModel {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 1L;

    /** The Constant log. */
    private static final Logger log = org.apache.logging.log4j.LogManager.getLogger(MnyTableModel.class);

    /** The table. */
    private final Table table;

    /** The current row. */
    private int currentRow = 0;

    /** The cursor. */
    private final Cursor cursor;

    /** The db read only. */
    private boolean dbReadOnly = false;

    /** The index lookup. */
    private final IndexLookup indexLookup = new IndexLookup();

    /** The columns. */
    private final List<? extends Column> columns;

    /** The columns array. */
    private final Column[] columnsArray;

    /** The cell value cache. */
    private CellValueCache cellValueCache;

    /** The is sorting. */
    private boolean isSorting = false;

    /**
     * Instantiates a new mny table model.
     *
     * @param table the table
     * @throws IOException Signals that an I/O exception has occurred.
     */
    public MnyTableModel(Table table) throws IOException {
        this.table = table;
        this.columns = JackcessImplUtils.getColumns(table);
        this.columnsArray = new Column[this.columns.size()];
        this.columns.toArray(this.columnsArray);
        this.cursor = CursorBuilder.createCursor(table);
        this.cursor.reset();
        this.cursor.moveToNextRow();

        this.cellValueCache = new CellValueCache();
    }

    /*
     * (non-Javadoc)
     * 
     * @see javax.swing.table.TableModel#getRowCount()
     */
    @Override
    public int getRowCount() {
        return table.getRowCount();
    }

    /*
     * (non-Javadoc)
     * 
     * @see javax.swing.table.TableModel#getColumnCount()
     */
    @Override
    public int getColumnCount() {
        return table.getColumnCount();
    }

    /*
     * (non-Javadoc)
     * 
     * @see javax.swing.table.TableModel#getValueAt(int, int)
     */
    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {

        Object value = null;
        String cachedKey = createCachedKey(rowIndex, columnIndex);
        Object cachedValue = null;

        if (cellValueCache != null) {
            cachedValue = cellValueCache.getIfPresent(cachedKey);
            if (cachedValue != null) {
                cachedValue = cellValueCache.get(cachedKey);
                if (log.isDebugEnabled()) {
                    log.debug("cached HIT");
                }
                return cachedValue;
            }
        }

        try {
            boolean cacheRow = false;
            if (cacheRow) {
                value = getValueAtWithCacheRow(rowIndex, columnIndex);
            } else {
                value = getValueAtWithNoCacheRow(rowIndex, columnIndex);
            }
        } catch (IOException e) {
            log.error(e, e);
        }

        if (cellValueCache != null) {
            if (isSorting) {
                log.info("cached MISSED, rowIndex=" + rowIndex + ", columnIndex=" + columnIndex);
                log.info("  cachedKey=" + cachedKey + ", value=" + value);
            }
            cellValueCache.put(cachedKey, value);
        }
        return value;
    }

    /*
     * (non-Javadoc)
     * 
     * @see javax.swing.table.AbstractTableModel#getColumnName(int)
     */
    @Override
    public String getColumnName(int column) {
        return columnsArray[column].getName();
    }

    /*
     * (non-Javadoc)
     * 
     * @see javax.swing.table.AbstractTableModel#isCellEditable(int, int)
     */
    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        return !dbReadOnly;
    }

    /*
     * (non-Javadoc)
     * 
     * @see javax.swing.table.AbstractTableModel#getColumnClass(int)
     */
    @Override
    public Class<?> getColumnClass(int columnIndex) {
        if (table == null) {
            log.info("getColumnClass, t=" + table + ", " + columnIndex);
            return super.getColumnClass(columnIndex);
        }
        List<ColumnImpl> cols = JackcessImplUtils.getColumns(table);
        Column column = cols.get(columnIndex);
        Class<?> clz = MnyTableModel.getColumnJavaClass(column);
        if (log.isDebugEnabled()) {
            log.debug("getColumnClass, " + columnIndex + ", " + clz);
        }
        return clz;
    }

    /*
     * (non-Javadoc)
     * 
     * @see javax.swing.table.AbstractTableModel#setValueAt(java.lang.Object, int,
     * int)
     */
    @Override
    public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
        if (dbReadOnly) {
            log.warn("Try to setValueAt when dbReadOnly=" + dbReadOnly);
            return;
        }

        try {
            moveCursorToRow(rowIndex);

            Column column = table.getColumn(getColumnName(columnIndex));
            Object oldValue = cursor.getCurrentRowValue(column);
            cursor.setCurrentRowValue(column, aValue);

            log.info("setValueAt: oldValue=" + oldValue + ", newValue=" + aValue);

            cacheCellValue(aValue, rowIndex, columnIndex);

            fireTableCellUpdated(rowIndex, columnIndex);
        } catch (IOException e) {
            log.error(e, e);
        }
    }

    /**
     * Creates the cached key.
     *
     * @param rowIndex    the row index
     * @param columnIndex the column index
     * @return the string
     */
    private String createCachedKey(int rowIndex, int columnIndex) {
        return rowIndex + "*" + columnIndex;
    }

    /**
     * Move cursor to row.
     *
     * @param rowIndex the row index
     * @throws IOException Signals that an I/O exception has occurred.
     */
    private void moveCursorToRow(int rowIndex) throws IOException {
        int delta = rowIndex - currentRow;
        currentRow = rowIndex;
        if (delta == 0) {
        } else if (delta < 0) {
            cursor.movePreviousRows(-delta);
        } else {
            cursor.moveNextRows(delta);
        }
    }

    /**
     * Gets the row values.
     *
     * @param rowIndex the row index
     * @return the row values
     * @throws IOException Signals that an I/O exception has occurred.
     */
    public Map<String, Object> getRowValues(int rowIndex) throws IOException {
        moveCursorToRow(rowIndex);
        Map<String, Object> rowData = cursor.getCurrentRow();
        return rowData;
    }

    /**
     * Checks if is db read only.
     *
     * @return true, if is db read only
     */
    public boolean isDbReadOnly() {
        return dbReadOnly;
    }

    /**
     * Sets the db read only.
     *
     * @param dbReadOnly the new db read only
     */
    public void setDbReadOnly(boolean dbReadOnly) {
        this.dbReadOnly = dbReadOnly;
    }

    /**
     * Delete row.
     *
     * @param rowIndex the row index
     */
    public void deleteRow(int rowIndex) {
        log.info("> deleteRow rowIndex=" + rowIndex);
        if (dbReadOnly) {
            return;
        }
        try {
            moveCursorToRow(rowIndex);
            cursor.deleteCurrentRow();
            resetCursor();
            closeCache();
            fireTableRowsDeleted(rowIndex, rowIndex);
        } catch (IOException e) {
            log.error(e, e);
        }
    }

    /**
     * Reset cursor.
     *
     * @throws IOException Signals that an I/O exception has occurred.
     */
    private void resetCursor() throws IOException {
        currentRow = 0;
        // rowData = null;
        cursor.reset();
        cursor.moveToNextRow();
    }

    /**
     * Copy column.
     *
     * @param rowIndex    the row index
     * @param columnIndex the column index
     */
    public void copyColumn(int rowIndex, int columnIndex) {
        if (rowIndex < 0) {
            return;
        }
        if (columnIndex < 0) {
            return;
        }

        if (log.isDebugEnabled()) {
            log.debug("> copyColumn rowIndex=" + rowIndex + ", columnIndex=" + columnIndex);
        }
        try {
            Object value = getValueAt(rowIndex, columnIndex);

            String columnName = getColumnName(columnIndex);
            log.info("columnName=" + columnName + ", value=" + value + ", className="
                    + ((value == null) ? null : value.getClass().getName()));
            if (valueIsFlag(columnName)) {
                int num = Integer.valueOf(value.toString());
                StringBuffer sb = new StringBuffer();
                int maxBits = Integer.SIZE;
                for (int i = 0; i < maxBits; i++) {
                    sb.append(((num & 1) == 1) ? '1' : '0');
                    num >>= 1;
                }

                String binaryString = sb.reverse().toString();
                log.info("    value (binary)=" + binaryString);
            }
            StringSelection stringSelection = new StringSelection(value.toString());
            Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
            ClipboardOwner owner = new ClipboardOwner() {

                @Override
                public void lostOwnership(Clipboard clipboard, Transferable contents) {
                }
            };
            clipboard.setContents(stringSelection, owner);
        } finally {
            // TODO: not needed?
        }

    }

    /**
     * Value is flag.
     *
     * @param columnName the column name
     * @return true, if successful
     */
    private boolean valueIsFlag(String columnName) {
        String[] flagColumns = { "grftt", };

        if (columnName == null) {
            return false;
        }

        for (String flagColumn : flagColumns) {
            if (columnName.compareToIgnoreCase(flagColumn) == 0) {
                return true;
            }
        }
        return false;
    }

    /**
     * Duplicate row.
     *
     * @param rowIndex            the row index
     * @param locationRealativeTo the location realative to
     */
    public void duplicateRow(int rowIndex, Component locationRealativeTo) {
        log.info("> duplicateRow rowIndex=" + rowIndex);
        if (dbReadOnly) {
            return;
        }
        try {
            moveCursorToRow(rowIndex);
            Map<String, Object> rowData = cursor.getCurrentRow();
            Table table = cursor.getTable();
            IndexLookup indexLooker = new IndexLookup();
            List<ColumnImpl> columns = JackcessImplUtils.getColumns(table);
            Object[] dataArray = rowData.values().toArray();
            for (int i = 0; i < dataArray.length; i++) {
                Column column = columns.get(i);
                if (indexLooker.isPrimaryKeyColumn(column)) {
                    Long max = indexLooker.getMax(column);
                    max = max + 1;
                    dataArray[i] = max.toString();
                }
            }

            int rowCount = table.getRowCount();
            table.addRow(dataArray);
            resetCursor();
            closeCache();
            fireTableRowsInserted(rowCount, rowCount);
        } catch (IOException e) {
            log.error(e, e);
        }
    }

    /**
     * Column is date type.
     *
     * @param i the i
     * @return true, if successful
     */
    public boolean columnIsDateType(int i) {
        List<ColumnImpl> columns = JackcessImplUtils.getColumns(table);
        if (columns == null) {
            return false;
        }
        if (i >= columns.size()) {
            return false;
        }
        Column column = columns.get(i);
        DataType dataType = column.getType();
        if (dataType == DataType.SHORT_DATE_TIME) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Gets the column index.
     *
     * @param tableModel the table model
     * @param columnName the column name
     * @return the column index
     */
    public static final int getColumnIndex(TableModel tableModel, String columnName) {
        int count = tableModel.getColumnCount();
        for (int i = 0; i < count; i++) {
            String aName = tableModel.getColumnName(i);
            if (log.isDebugEnabled()) {
                log.debug("aName=" + aName);
            }
            if (columnName.compareTo(aName) == 0) {
                if (log.isDebugEnabled()) {
                    log.debug("MATCHED: column=" + i);
                }
                return i;
            }
        }
        return -1;
    }

    /**
     * Checks if is primary key column.
     *
     * @param i the i
     * @return true, if is primary key column
     */
    public boolean isPrimaryKeyColumn(int i) {
        List<ColumnImpl> columns = JackcessImplUtils.getColumns(table);
        if (columns == null) {
            return false;
        }
        if (i >= columns.size()) {
            return false;
        }
        Column column = columns.get(i);
        return indexLookup.isPrimaryKeyColumn(column);
    }

    /**
     * Checks if is foreign key column.
     *
     * @param i the i
     * @return true, if is foreign key column
     */
    public boolean isForeignKeyColumn(int i) {
        List<ColumnImpl> columns = JackcessImplUtils.getColumns(table);
        if (columns == null) {
            return false;
        }
        if (i >= columns.size()) {
            return false;
        }
        Column column = columns.get(i);
        List<Column> referenced = null;

        try {
            referenced = indexLookup.getReferencedColumns(column);
        } catch (IOException e) {
            log.warn(e);
        }
        if (referenced == null) {
            return false;
        }
        return referenced.size() > 0;
    }

    /**
     * Close.
     */
    public void close() {
        closeCache();
    }

    /**
     * Sets the checks if is sorting.
     *
     * @param b the new checks if is sorting
     */
    public void setIsSorting(boolean b) {
        this.isSorting = b;
    }

    /**
     * Gets the value at with cache row.
     *
     * @param rowIndex    the row index
     * @param columnIndex the column index
     * @return the value at with cache row
     * @throws IOException Signals that an I/O exception has occurred.
     */
    private Object getValueAtWithCacheRow(int rowIndex, int columnIndex) throws IOException {
        Object value;
        Map<String, Object> rowData = null;
        if (cellValueCache != null) {
            rowData = cellValueCache.getRowsCache(rowIndex);
        }
        if (rowData == null) {
            moveCursorToRow(rowIndex);
            rowData = cursor.getCurrentRow();
            if (cellValueCache != null) {
                cellValueCache.putRowsCache(rowIndex, rowData);
            }
        }
        String columnName = getColumnName(columnIndex);
        value = rowData.get(columnName);
        if (value instanceof byte[]) {
            value = ByteUtil.toHexString((byte[]) value);
        }
        return value;
    }

    /**
     * Gets the value at with no cache row.
     *
     * @param rowIndex    the row index
     * @param columnIndex the column index
     * @return the value at with no cache row
     * @throws IOException Signals that an I/O exception has occurred.
     */
    private Object getValueAtWithNoCacheRow(int rowIndex, int columnIndex) throws IOException {
        Object value;
        moveCursorToRow(rowIndex);
        List<ColumnImpl> cols = JackcessImplUtils.getColumns(table);
        Column column = cols.get(columnIndex);
        value = cursor.getCurrentRowValue(column);
        if (value instanceof byte[]) {
            value = ByteUtil.toHexString((byte[]) value);
        }
        return value;
    }

    /**
     * Cache cell value.
     *
     * @param aValue      the a value
     * @param rowIndex    the row index
     * @param columnIndex the column index
     */
    private void cacheCellValue(Object aValue, int rowIndex, int columnIndex) {
        if (cellValueCache != null) {
            String cachedKey = createCachedKey(rowIndex, columnIndex);
            cellValueCache.put(cachedKey, aValue);
        }
    }

    /**
     * Close cache.
     */
    private void closeCache() {
        if (cellValueCache == null) {
            return;
        }

        cellValueCache.close();
    }

    /**
     * Fill cache for sorting.
     *
     * @param table       the table
     * @param columnIndex the column index
     */
    private void fillCacheForSorting(Table table, int columnIndex) {
        log.info("> fillCacheForSorting, columnIndex=" + columnIndex);

        if (cellValueCache == null) {
            return;
        }

        Cursor cursor = null;

        try {
            cursor = CursorBuilder.createCursor(table);
            List<ColumnImpl> cols = JackcessImplUtils.getColumns(table);
            Column column = cols.get(columnIndex);
            Object value;
            int rowIndex = 0;
            while (cursor.moveToNextRow()) {
                value = cursor.getCurrentRowValue(column);
                cacheCellValue(value, rowIndex, columnIndex);
                rowIndex++;
            }
        } catch (IOException e) {
            log.warn(e);
        } finally {
            if (cursor != null) {
                cursor = null;
            }
        }
    }

    /**
     * Clear cache.
     */
    public void clearCache() {
        if (cellValueCache == null) {
            return;
        }
        cellValueCache.invalidateAll();
    }

    /**
     * Fill cache for sorting.
     *
     * @param column the column
     */
    public void fillCacheForSorting(int column) {
        fillCacheForSorting(table, column);
    }

    /**
     * Gets the column java class.
     *
     * @param column the column
     * @return the column java class
     */
    private static Class<?> getColumnJavaClass(Column column) {
        Class<?> clz = null;
        DataType type = column.getType();
        if (type == DataType.BOOLEAN) {
            clz = Boolean.class;
        } else if (type == DataType.BYTE) {
            clz = Byte.class;
        } else if (type == DataType.INT) {
            clz = Integer.class;
        } else if (type == DataType.LONG) {
            clz = Long.class;
        } else if (type == DataType.DOUBLE) {
            clz = Double.class;
        } else if (type == DataType.FLOAT) {
            clz = Float.class;
        } else if (type == DataType.SHORT_DATE_TIME) {
            clz = Date.class;
        } else if (type == DataType.BINARY) {
            clz = byte[].class;
        } else if (type == DataType.TEXT) {
            clz = String.class;
        } else if (type == DataType.MONEY) {
            clz = BigDecimal.class;
        } else if (type == DataType.OLE) {
            clz = byte[].class;
        } else if (type == DataType.MEMO) {
            clz = String.class;
        } else if (type == DataType.NUMERIC) {
            clz = BigDecimal.class;
        } else if (type == DataType.GUID) {
            clz = String.class;
        } else if ((type == DataType.UNKNOWN_0D) || (type == DataType.UNKNOWN_11)) {
            clz = Object.class;
        } else {
            clz = Object.class;
            log.warn("Unrecognized data type: " + type);
        }
        return clz;
    }
}
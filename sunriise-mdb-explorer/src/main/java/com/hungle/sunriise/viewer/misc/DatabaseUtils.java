/*******************************************************************************
 * Copyright (c) 2016 Hung Le
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *******************************************************************************/
package com.hungle.sunriise.viewer.misc;

import java.io.IOException;
import java.util.Set;

import org.apache.logging.log4j.Logger;

import com.healthmarketscience.jackcess.Database;
import com.healthmarketscience.jackcess.PropertyMap;
import com.healthmarketscience.jackcess.Table;
import com.hungle.sunriise.util.JackcessImplUtils;

// TODO: Auto-generated Javadoc
/**
 * The Class DatabaseUtils.
 */
public class DatabaseUtils {

    /** The Constant log. */
    private static final Logger log = org.apache.logging.log4j.LogManager.getLogger(DatabaseUtils.class);

    /**
     * Log db info.
     *
     * @param db the db
     */
    public static void logDbInfo(Database db) {
        try {
            log.info("### (openned) db=" + db.getFile());

            log.info("  fileFormat=" + db.getFileFormat());
            // log.info(" format=" + db.getFormat());

            log.info("  timeZone=" + db.getTimeZone().getDisplayName());
            log.info("  charset=" + db.getCharset());

            log.info("  tableNames=" + db.getTableNames().size());

            log.info("  defaultCodePage=" + JackcessImplUtils.getDefaultCodePage(db));

            Table table = null;
            String label = null;

            log.info("  ");
            table = JackcessImplUtils.getSystemCatalog(db);
            label = "systemCatalog";
            logSystemTable(label, table);

            log.info("  ");
            Set<String> systemTableNames = db.getSystemTableNames();
            log.info("  systemTableNames=" + systemTableNames.size());
            int i = 0;
            int size = systemTableNames.size();
            for (String systemTableName : systemTableNames) {
                log.info("  " + "(" + i + "/" + size + ") " + "systemTableName=" + systemTableName);
                table = db.getSystemTable(systemTableName);
                label = "systemTable." + systemTableName;
                logSystemTable(label, table);

                i++;
            }

            // log.info(" columnOrder=" + db.getColumnOrder());

            // logPropertyMaps(db);

        } catch (IOException e) {
            log.warn(e);
        }
    }

    /**
     * Log system table.
     *
     * @param label the label
     * @param table the table
     */
    protected static void logSystemTable(String label, Table table) {
        if (table == null) {
            log.info("  " + label + "=" + table);
        } else {
            log.info("  " + label + "=" + table.getName());
            log.info("  " + label + ".rows=" + table.getRowCount());
            log.info("  " + label + ".columns=" + table.getColumnCount());
        }
    }

    /**
     * Log property maps.
     *
     * @param db the db
     * @throws IOException Signals that an I/O exception has occurred.
     */
    protected static void logPropertyMaps(Database db) throws IOException {
        PropertyMap map = null;
        String label = null;

        map = db.getDatabaseProperties();
        label = "databaseProperties";
        logPropertyMap(label, map);

        map = db.getSummaryProperties();
        label = "summaryProperties";
        logPropertyMap(label, map);

        map = db.getUserDefinedProperties();
        label = "userDefinedProperties";
        logPropertyMap(label, map);
    }

    /**
     * Log property map.
     *
     * @param label the label
     * @param map   the map
     */
    protected static void logPropertyMap(String label, PropertyMap map) {

        if (map != null) {
            log.info("  " + label + "=" + map.getSize());
            for (PropertyMap.Property prop : map) {
                log.info("  " + prop);
            }
        } else {
            log.info("  " + label + "=" + map);
        }
    }

}

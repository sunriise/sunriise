package com.hungle.sunriise.viewer.cell;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

import javax.swing.table.DefaultTableCellRenderer;

import org.apache.logging.log4j.Logger;

// TODO: Auto-generated Javadoc
/**
 * The Class DateCellRenderer.
 */
public final class DateCellRenderer extends DefaultTableCellRenderer {

    /** The Constant log. */
    private static final Logger LOGGER = org.apache.logging.log4j.LogManager.getLogger(DateCellRenderer.class);

    // private DateFormat formatter = new
    // SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ");
    // private DateFormat formatter = new
    // SimpleDateFormat("yyyy/MM/dd HH:mm:ss.SSS");
    // private DateFormat formatter = new
    /** The formatter. */
    // SimpleDateFormat("yyyy/MM/dd");
    private DateFormat formatter = new SimpleDateFormat("MMM dd, yyyy HH:mm");

    /*
     * (non-Javadoc)
     * 
     * @see javax.swing.table.DefaultTableCellRenderer#setValue(java.lang.Object)
     */
    @Override
    public void setValue(Object value) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("cellRenderer: value=" + value + ", " + value.getClass().getName());
        }
        if (formatter == null) {
            formatter = DateFormat.getDateInstance();
        }
        String renderedValue = (value == null) ? "" : formatter.format(value);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("cellRenderer: renderedValue=" + renderedValue);
        }

        setText(renderedValue);
    }
}
package com.hungle.sunriise.viewer.spring;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.healthmarketscience.jackcess.Database;
import com.healthmarketscience.jackcess.Table;
import com.healthmarketscience.jackcess.util.ExportFilter;
import com.healthmarketscience.jackcess.util.ExportUtil;
import com.healthmarketscience.jackcess.util.SimpleExportFilter;
import com.hungle.sunriise.io.MnyDb;

@RestController
public class MnyViewerCsvController {
    @Autowired
    private MnyViewerSpringHelper mnyViewerHolder;

    @RequestMapping(value = "/mny/{tableName}", produces = "text/csv")
    public void one(@PathVariable String tableName,
            @RequestParam(value = "col", required = false) List<String> colNames,
            @RequestParam(value = "sep", required = false) String sep, HttpServletResponse response)
            throws IOException {
        Database db = getDb();

        PrintWriter writer = response.getWriter();
        try {
            exportRows(db, tableName, colNames, writer, sep);
        } finally {
            writer.flush();
        }
    }

    private Database getDb() {
        if (mnyViewerHolder == null) {
            throw new IllegalStateException("INVALID_STATE - mnyViewerHolder is null.");
        }
        MnyDb mnyDb = mnyViewerHolder.getViewer().getMnyDb();
        if (mnyDb == null) {
            throw new IllegalStateException("INVALID_STATE - mnyDb is null.");
        }
        Database db = mnyDb.getDb();
        if (db == null) {
            throw new IllegalStateException("INVALID_STATE - db is null.");
        }
        return db;
    }

    private void exportRows(Database db, String tableName, List<String> colNames, PrintWriter writer, String sep)
            throws IOException {
        Table table = db.getTable(tableName);
        if (table == null) {
            throw new IOException("NOT_FOUND - tableName=" + tableName);
        }

        ExportFilter exportFilter = new SimpleExportFilter();
        if ((colNames != null) && (colNames.size()) > 0) {
            Set<String> columnsView = new HashSet<String>();
            columnsView.addAll(colNames);
            exportFilter = new ColumnNamesFilter(columnsView);
        } else {
            exportFilter = new SimpleExportFilter();
        }

//        String defaultDelimiter = ExportUtil.DEFAULT_DELIMITER;
        if (StringUtils.isEmpty(sep)) {
            sep = ExportUtil.DEFAULT_DELIMITER;
        }
        ExportUtil.exportWriter(db, table.getName(), new BufferedWriter(writer), true, sep,
                ExportUtil.DEFAULT_QUOTE_CHAR, exportFilter);
    }
}

package com.hungle.sunriise.viewer.spring;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@JsonSerialize(using = JsonTableOneResultSerializer.class)
@JsonPropertyOrder({ "tableName", "rowCount", "rows" })
public class JsonTableOneResult {
    private String tableName;

    private int rowCount;

    private List<JsonRow> rows;

    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    public int getRowCount() {
        return rowCount;
    }

    public void setRowCount(int rowCount) {
        this.rowCount = rowCount;
    }

    public List<JsonRow> getRows() {
        return rows;
    }

    public void setRows(List<JsonRow> rows) {
        this.rows = rows;
    }
}

/*******************************************************************************
 * Copyright (c) 2016 Hung Le
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *******************************************************************************/
package com.hungle.sunriise.viewer.row;

// TODO: Auto-generated Javadoc
/**
 * The Class DiffData.
 */
public class DiffData {

    /** The key. */
    private String key;

    /** The value1. */
    private Object value1;

    /** The value2. */
    private Object value2;

    /**
     * Gets the key.
     *
     * @return the key
     */
    public String getKey() {
        return key;
    }

    /**
     * Sets the key.
     *
     * @param key the new key
     */
    public void setKey(String key) {
        this.key = key;
    }

    /**
     * Gets the value1.
     *
     * @return the value1
     */
    public Object getValue1() {
        return value1;
    }

    /**
     * Sets the value1.
     *
     * @param value1 the new value1
     */
    public void setValue1(Object value1) {
        this.value1 = value1;
    }

    /**
     * Gets the value2.
     *
     * @return the value2
     */
    public Object getValue2() {
        return value2;
    }

    /**
     * Sets the value2.
     *
     * @param value2 the new value2
     */
    public void setValue2(Object value2) {
        this.value2 = value2;
    }

    /**
     * Instantiates a new diff data.
     *
     * @param key    the key
     * @param value1 the value1
     * @param value2 the value2
     */
    public DiffData(String key, Object value1, Object value2) {
        this.key = key;
        this.value1 = value1;
        this.value2 = value2;
    }
}

package com.hungle.sunriise.viewer.misc;

import java.io.PrintWriter;

// TODO: Auto-generated Javadoc
/**
 * The Interface SaveResultWriter.
 */
public interface SaveResultWriter {

    /**
     * Write.
     *
     * @param writer the writer
     */
    void writeResult(PrintWriter writer);

}

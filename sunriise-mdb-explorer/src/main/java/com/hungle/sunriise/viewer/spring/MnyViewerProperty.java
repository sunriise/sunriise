package com.hungle.sunriise.viewer.spring;

import java.util.HashMap;
import java.util.Map;

public enum MnyViewerProperty {
    MNY_DB("mny.db");

    private final String propertyName;

    public String getPropertyName() {
        return propertyName;
    }

    MnyViewerProperty(String propertyName) {
        this.propertyName = propertyName;
    }

    // Lookup table
    private static final Map<String, MnyViewerProperty> lookup = new HashMap<>();

    // Populate the lookup table on loading time
    static {
        for (MnyViewerProperty env : MnyViewerProperty.values()) {
            lookup.put(env.getPropertyName(), env);
        }
    }

    // This method can be used for reverse lookup purpose
    public static MnyViewerProperty get(String propertyName) {
        return lookup.get(propertyName);
    }
}

/*******************************************************************************
 * Copyright (c) 2016 Hung Le
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *******************************************************************************/
package com.hungle.sunriise.viewer.menu;

import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;

import org.apache.logging.log4j.Logger;

import com.hungle.sunriise.viewer.table.TableUtils;

// TODO: Auto-generated Javadoc
/**
 * The Class GotoToColumnAction.
 */
public class GotoToColumnAction extends AbstractAction {

    /** The Constant log. */
    private static final Logger log = org.apache.logging.log4j.LogManager.getLogger(GotoToColumnAction.class);

    /** The context. */
    private final AbstractGotoToColumnContext context;

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 448689485546251630L;

    /** The column name. */
    private String columnName;

    /**
     * Instantiates a new goto to column action.
     *
     * @param context    the context
     * @param columnName the name
     */
    public GotoToColumnAction(AbstractGotoToColumnContext context, String columnName) {
        super(columnName);
        this.context = context;
        this.columnName = columnName;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.
     * ActionEvent)
     */
    @Override
    public void actionPerformed(ActionEvent event) {
        int rowIndex = context.getSelectedRow();
        if (rowIndex < 0) {
            rowIndex = 0;
        }

        int columnIndex = 0;
        columnIndex = context.getColumnIndex(columnName);

        log.info("GotoToColumnAction" + ", columnName=" + columnName + ", rowIndex=" + rowIndex + ", columnIndex="
                + columnIndex);

        TableUtils.scrollToCenter(context.getTable(), rowIndex, columnIndex);
    }
}
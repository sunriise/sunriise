package com.hungle.sunriise.viewer.spring;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.data.mongo.MongoDataAutoConfiguration;
import org.springframework.boot.autoconfigure.mongo.MongoAutoConfiguration;
import org.springframework.boot.autoconfigure.orm.jpa.HibernateJpaAutoConfiguration;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.ComponentScan;

@ComponentScan({ "com.hungle.sunriise.viewer.spring", "com.hungle.sunriise.spring" })
@SpringBootApplication(exclude = { MongoAutoConfiguration.class, MongoDataAutoConfiguration.class,
        HibernateJpaAutoConfiguration.class })
public class MnyViewerSpringApplication {
    public static void main(String[] args) {
        SpringApplication app = null;
        try {
            app = new SpringApplication(MnyViewerSpringApplication.class);
            ConfigurableApplicationContext applicationContext = app.run(args);
        } finally {
            if (app != null) {
                app = null;
            }
        }
    }
}

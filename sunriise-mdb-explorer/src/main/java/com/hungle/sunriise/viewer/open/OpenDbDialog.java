/*******************************************************************************
 * Copyright (c) 2010 Hung Le
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *******************************************************************************/
package com.hungle.sunriise.viewer.open;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.prefs.Preferences;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.SwingConstants;
import javax.swing.WindowConstants;
import javax.swing.border.EmptyBorder;

import org.apache.logging.log4j.Logger;
import org.jdesktop.beansbinding.AutoBinding.UpdateStrategy;
import org.jdesktop.beansbinding.BeanProperty;
import org.jdesktop.swingbinding.JComboBoxBinding;
import org.jdesktop.swingbinding.SwingBindings;

import com.healthmarketscience.jackcess.Database;
import com.healthmarketscience.jackcess.impl.DatabaseImpl;
import com.hungle.sunriise.backup.BackupFileUtils;
import com.hungle.sunriise.crypt.HeaderPage;
import com.hungle.sunriise.gui.CreateMnyDbPlugin;
import com.hungle.sunriise.io.MnyDb;
import com.hungle.sunriise.viewer.model.OpenDbDialogDataModel;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.FormSpecs;
import com.jgoodies.forms.layout.RowSpec;

// TODO: Auto-generated Javadoc
/**
 * The Class OpenDbDialog.
 */
public class OpenDbDialog extends JDialog {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 1L;

    /** The Constant log. */
    private static final Logger log = org.apache.logging.log4j.LogManager.getLogger(OpenDbDialog.class);

    /** The content panel. */
    private final JPanel contentPanel = new JPanel();

    /** The cancel. */
    private boolean cancel = false;

    /** The password field. */
    private JPasswordField passwordField;

    /** The read only check box. */
    private JCheckBox readOnlyCheckBox;

    /** The encrypted check box. */
    private JCheckBox encryptedCheckBox;

    /** The data model. */
    private OpenDbDialogDataModel dataModel = new OpenDbDialogDataModel();

    /** The db file names. */
    private JComboBox<?> dbFileNames;

    /** The opened db. */
    private MnyDb mnyDb;

    /** The ok button. */
    private JButton okButton;

    /** The cancel button. */
    private JButton cancelButton;

    /** The hide. */
    private boolean hide = true;

    /**
     * The Class CancelAction.
     */
    private final class CancelAction implements ActionListener {

        /*
         * (non-Javadoc)
         * 
         * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.
         * ActionEvent)
         */
        @Override
        public void actionPerformed(ActionEvent e) {
            setCancel(true);

            if (preHideDialog()) {
                if (hide) {
                    setVisible(false);
                } else {
                    dispose();
                }
            }
        }
    }

    /**
     * The Class OkAction.
     */
    private final class OkAction implements ActionListener {

        /*
         * (non-Javadoc)
         * 
         * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.
         * ActionEvent)
         */
        @Override
        public void actionPerformed(ActionEvent event) {
            String dbFileName = (String) dbFileNames.getSelectedItem();
            if ((dbFileName == null) || (dbFileName.length() <= 0)) {
                JOptionPane.showMessageDialog(dbFileNames, "Please enter a database filename.",
                        "Missing database filename", JOptionPane.ERROR_MESSAGE);
                return;
            }
            if (BackupFileUtils.isMnyBackupFile(dbFileName) && (!readOnlyCheckBox.isSelected())) {
                JOptionPane.showMessageDialog(dbFileNames, "Cannot open Money backup file write-mode.",
                        "Cannot open in write-mode", JOptionPane.ERROR_MESSAGE);
                return;
            }

            if (BackupFileUtils.isMnyBackupFile(dbFileName)) {
                try {
                    File dbFile = new File(dbFileName);
                    dbFile = BackupFileUtils.createBackupAsTempFile(dbFile, true, -1L);
                    dbFileName = dbFile.getAbsolutePath();
                } catch (IOException e) {
                    log.error(e, e);
                }
            }
            try {
                if (OpenDbDialog.this.mnyDb != null) {
                    OpenDbDialog.this.mnyDb.close();
                }

                OpenDbDialog.this.mnyDb = openDb(dbFileName, passwordField.getPassword(), readOnlyCheckBox.isSelected(),
                        encryptedCheckBox.isSelected());

                dbOpenedCallback();
            } catch (Exception e) {
                log.error(e, e);
                JOptionPane.showMessageDialog(dbFileNames, dbFileName + " \n" + e.toString(), "Error open db file",
                        JOptionPane.ERROR_MESSAGE);
                return;
            }
            File file = OpenDbDialog.this.mnyDb.getDbFile();
            if (file != null) {
                List<String> list = dataModel.getRecentOpenFileNames();
                if (list.contains(file.getAbsolutePath())) {
                    list.remove(file.getAbsolutePath());
                }
                list.add(0, file.getAbsolutePath());
                if (log.isDebugEnabled()) {
                    log.debug(list);
                }
            }
            setCancel(false);

            if (preHideDialog()) {
                if (hide) {
                    setVisible(false);
                } else {
                    dispose();
                }
            }
        }

    }

    /**
     * Show dialog.
     *
     * @param mnyDb                   the opend db
     * @param recentOpenFileNames     the recent open file names
     * @param locationRelativeTo      the location relative to
     * @param disableReadOnlyCheckBox the disable read only check box
     * @param plugin                  the plugin
     * @return the open db dialog
     */
    static OpenDbDialog showDialog(MnyDb mnyDb, List<String> recentOpenFileNames, Component locationRelativeTo,
            boolean disableReadOnlyCheckBox, final CreateMnyDbPlugin plugin) {
        String title = null;
        OpenDbDialog dialog = new OpenDbDialog(mnyDb, title, recentOpenFileNames) {

            @Override
            protected MnyDb openDb(String dbFileName, char[] passwordChars, boolean readOnly, boolean encrypted)
                    throws IOException {
                if (plugin != null) {
                    return plugin.openDb(dbFileName, passwordChars, readOnly, encrypted);
                } else {
                    return super.openDb(dbFileName, passwordChars, readOnly, encrypted);
                }
            }
        };

        showDialog(dialog, locationRelativeTo, disableReadOnlyCheckBox);

        return dialog;
    }

    /**
     * Db opened callback.
     */
    protected void dbOpenedCallback() {
        log.info("Opened dbFile=" + mnyDb.getDbFile());
        log.info("    isMemoryMapped=" + mnyDb.isMemoryMapped());

        Database db = mnyDb.getDb();
        if ((db != null) && (((DatabaseImpl) db).getSystemCatalog() == null)) {
            // go into scanvenger mode
            try {
                HeaderPage headerPage = new HeaderPage(mnyDb.getDbFile());
                int pageSize = headerPage.getJetFormat().PAGE_SIZE;
                log.info("pageSize=" + pageSize);
            } catch (IOException e) {
                log.error(e);
            }
        }
    }

    /**
     * Pre hide dialog.
     *
     * @return true, if successful
     */
    protected boolean preHideDialog() {
        return true;
    }

    /**
     * Show dialog.
     *
     * @param dialog                  the dialog
     * @param locationRelativeTo      the location relative to
     * @param disableReadOnlyCheckBox the disable read only check box
     */
    public static void showDialog(OpenDbDialog dialog, Component locationRelativeTo, boolean disableReadOnlyCheckBox) {
        dialog.setDefaultCloseOperation(WindowConstants.HIDE_ON_CLOSE);
        dialog.setModalityType(ModalityType.APPLICATION_MODAL);

        if (disableReadOnlyCheckBox) {
            dialog.readOnlyCheckBox.setEnabled(false);
        } else {
            dialog.readOnlyCheckBox.setEnabled(true);
        }

        dialog.pack();
        dialog.setLocationRelativeTo(locationRelativeTo);
        dialog.setVisible(true);
    }

    /**
     * Create the dialog.
     *
     * @param mnyDb               the opened db
     * @param title               the title
     * @param recentOpenFileNames the recent open file names
     */
    public OpenDbDialog(MnyDb mnyDb, String title, final List<String> recentOpenFileNames) {
        if ((title == null)) {
            title = "Open";
        }
        setTitle(title);
        // setModalityType(ModalityType.APPLICATION_MODAL);
        this.mnyDb = mnyDb;
        this.dataModel.setRecentOpenFileNames(recentOpenFileNames);
        // setBounds(100, 100, 450, 300);
        getContentPane().setLayout(new BorderLayout());
        contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
        getContentPane().add(contentPanel, BorderLayout.CENTER);
        contentPanel.setLayout(new FormLayout(
                new ColumnSpec[] { FormSpecs.UNRELATED_GAP_COLSPEC, FormSpecs.DEFAULT_COLSPEC,
                        FormSpecs.LABEL_COMPONENT_GAP_COLSPEC, ColumnSpec.decode("default:grow"),
                        FormSpecs.RELATED_GAP_COLSPEC, FormSpecs.DEFAULT_COLSPEC, FormSpecs.UNRELATED_GAP_COLSPEC, },
                new RowSpec[] { FormSpecs.RELATED_GAP_ROWSPEC, FormSpecs.DEFAULT_ROWSPEC, FormSpecs.RELATED_GAP_ROWSPEC,
                        FormSpecs.DEFAULT_ROWSPEC, FormSpecs.RELATED_GAP_ROWSPEC, FormSpecs.DEFAULT_ROWSPEC,
                        FormSpecs.RELATED_GAP_ROWSPEC, FormSpecs.DEFAULT_ROWSPEC, }));
        {
            JLabel lblNewLabel = new JLabel("DB Filename");
            lblNewLabel.setHorizontalAlignment(SwingConstants.TRAILING);
            contentPanel.add(lblNewLabel, "2, 2, right, default");
        }
        {
            JButton btnNewButton = new JButton("...");
            btnNewButton.addActionListener(new ActionListener() {
                private JFileChooser fc = null;

                @Override
                public void actionPerformed(ActionEvent event) {
                    Component component = (Component) event.getSource();
                    if (fc == null) {
                        File currentDirectory = new File(".");

                        String fileName = (String) dbFileNames.getSelectedItem();
                        if ((fileName != null) && (fileName.length() > 0)) {
                            File file = new File(fileName);
                            if (file.exists()) {
                                if (file.isDirectory()) {
                                    currentDirectory = file.getAbsoluteFile();
                                } else {
                                    currentDirectory = file.getParentFile().getAbsoluteFile();
                                }
                            }
                        }
                        fc = new JFileChooser(currentDirectory);
                    }
                    if (fc.showOpenDialog(JOptionPane.getFrameForComponent(component)) == JFileChooser.CANCEL_OPTION) {
                        return;
                    }
                    File selectedFile = fc.getSelectedFile();
                    selectedFile = selectedFile.getAbsoluteFile();
                    // String fileName= selectedFile.getName();
                    // if (fileName.endsWith(".mny")) {
                    // encryptedCheckBox.setSelected(true);
                    // } else {
                    // encryptedCheckBox.setSelected(false);
                    // }
                    dbFileNames.setSelectedItem(selectedFile.getAbsolutePath());

                }
            });
            {
                dbFileNames = new JComboBox();
                dbFileNames.addItemListener(new ItemListener() {

                    @Override
                    public void itemStateChanged(ItemEvent e) {
                        if (e.getStateChange() == ItemEvent.SELECTED) {
                            String fileName = (String) dbFileNames.getSelectedItem();
                            if (fileName == null) {
                                return;
                            }
                            if (fileName.length() <= 0) {
                                return;
                            }
                            if (BackupFileUtils.isMnyFiles(fileName)) {
                                encryptedCheckBox.setSelected(true);
                            } else {
                                encryptedCheckBox.setSelected(false);
                            }
                        } else {
                        }
                    }
                });
                dbFileNames.setEditable(true);
                contentPanel.add(dbFileNames, "4, 2, fill, default");
            }
            contentPanel.add(btnNewButton, "6, 2");
        }
        {
            JLabel lblNewLabel_1 = new JLabel("Password");
            lblNewLabel_1.setHorizontalAlignment(SwingConstants.TRAILING);
            contentPanel.add(lblNewLabel_1, "2, 4, right, default");
        }
        {
            passwordField = new JPasswordField();
            contentPanel.add(passwordField, "4, 4, fill, default");
        }
        {
            readOnlyCheckBox = new JCheckBox("Read only");
            readOnlyCheckBox.addItemListener(new ItemListener() {

                @Override
                public void itemStateChanged(ItemEvent e) {
                    if (log.isDebugEnabled()) {
                        log.debug("readOnlyCheckBox=" + readOnlyCheckBox.isSelected());
                    }
                }
            });
            readOnlyCheckBox.setSelected(true);
            contentPanel.add(readOnlyCheckBox, "4, 6");
        }
        {
            encryptedCheckBox = new JCheckBox("Encrypted (keep selected for *.mny file)");
            encryptedCheckBox.addItemListener(new ItemListener() {

                @Override
                public void itemStateChanged(ItemEvent e) {
                    if (log.isDebugEnabled()) {
                        log.debug("encryptedCheckBox=" + encryptedCheckBox.isSelected());
                    }
                }
            });
            encryptedCheckBox.setSelected(true);
            contentPanel.add(encryptedCheckBox, "4, 8");
        }
        {
            JPanel buttonPane = new JPanel();
            buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
            getContentPane().add(buttonPane, BorderLayout.SOUTH);
            {
                okButton = new JButton("OK");
                okButton.addActionListener(new OkAction());
                okButton.setActionCommand("OK");
                buttonPane.add(okButton);
                getRootPane().setDefaultButton(okButton);
            }
            {
                cancelButton = new JButton("Cancel");
                cancelButton.addActionListener(new CancelAction());
                cancelButton.setActionCommand("Cancel");
                buttonPane.add(cancelButton);
            }
        }

        initDataBindings();
    }

    /**
     * Checks if is cancel.
     *
     * @return true, if is cancel
     */
    public boolean isCancel() {
        return cancel;
    }

    /**
     * Sets the cancel.
     *
     * @param cancel the new cancel
     */
    public void setCancel(boolean cancel) {
        this.cancel = cancel;
    }

    // public Database getDb() {
    // return opendDb.getDb();
    // }
    //
    // public File getDbFile() {
    // return opendDb.getDbFile();
    // }

    /**
     * Gets the read only check box.
     *
     * @return the read only check box
     */
    public JCheckBox getReadOnlyCheckBox() {
        return readOnlyCheckBox;
    }

    /**
     * Inits the data bindings.
     */
    protected void initDataBindings() {
        BeanProperty<OpenDbDialogDataModel, List<String>> openDbDialogDataModelBeanProperty = BeanProperty
                .create("recentOpenFileNames");
        JComboBoxBinding<String, OpenDbDialogDataModel, JComboBox> jComboBinding = SwingBindings
                .createJComboBoxBinding(UpdateStrategy.READ, dataModel, openDbDialogDataModelBeanProperty, dbFileNames);
        jComboBinding.bind();
    }

    /**
     * Gets the opened db.
     *
     * @return the opened db
     */
    public MnyDb getMnyDb() {
        return mnyDb;
    }

    /**
     * Update recent open file names.
     *
     * @param recentOpenFileNames the recent open file names
     * @param preferences         the preferences
     */
    public static void updateRecentOpenFileNames(List<String> recentOpenFileNames, Preferences preferences) {
        int size;
        size = recentOpenFileNames.size();
        size = Math.min(size, 10);
        if (log.isDebugEnabled()) {
            log.debug("prefs: recentOpenFileNames_size=" + size);
        }
        preferences.putInt("recentOpenFileNames_size", size);
        for (int i = 0; i < size; i++) {
            if (log.isDebugEnabled()) {
                log.debug("prefs: recentOpenFileNames_" + i + ", value=" + recentOpenFileNames.get(i));
            }
            preferences.put("recentOpenFileNames_" + i, recentOpenFileNames.get(i));
        }
    }

    /**
     * Gets the recent open file names.
     *
     * @param prefs the prefs
     * @return the recent open file names
     */
    public static List<String> getRecentOpenFileNames(Preferences prefs) {
        List<String> recentOpenFileNames = new ArrayList<String>();
        int size = prefs.getInt("recentOpenFileNames_size", 0);
        size = Math.min(size, 10);
        for (int i = 0; i < size; i++) {
            String value = prefs.get("recentOpenFileNames_" + i, null);
            if (value != null) {
                recentOpenFileNames.add(value);
            }
        }
        return recentOpenFileNames;
    }

    /**
     * Gets the ok button.
     *
     * @return the ok button
     */
    public JButton getOkButton() {
        return okButton;
    }

    /**
     * Gets the cancel button.
     *
     * @return the cancel button
     */
    public JButton getCancelButton() {
        return cancelButton;
    }

    /**
     * Checks if is hide.
     *
     * @return true, if is hide
     */
    public boolean isHide() {
        return hide;
    }

    /**
     * Sets the hide.
     *
     * @param hide the new hide
     */
    public void setHide(boolean hide) {
        this.hide = hide;
    }

    /**
     * Open db.
     *
     * @param dbFileName    the db file name
     * @param passwordChars the password chars
     * @param readOnly      the read only
     * @param encrypted     the encrypted
     * @return the opened db
     * @throws IOException Signals that an I/O exception has occurred.
     */
    protected MnyDb openDb(String dbFileName, char[] passwordChars, boolean readOnly, boolean encrypted)
            throws IOException {
        return new MnyDb(dbFileName, passwordChars, readOnly, encrypted);
    }
}

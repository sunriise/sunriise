/*******************************************************************************
 * Copyright (c) 2010 Hung Le
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *******************************************************************************/
package com.hungle.sunriise.viewer.open;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;
import java.util.prefs.Preferences;

import org.apache.logging.log4j.Logger;

import com.hungle.sunriise.gui.CreateMnyDbPlugin;
import com.hungle.sunriise.io.MnyDb;

// TODO: Auto-generated Javadoc
/**
 * The Class OpenDbAction.
 */
public abstract class OpenDbAction implements ActionListener {

    /** The Constant log. */
    private static final Logger LOGGER = org.apache.logging.log4j.LogManager.getLogger(OpenDbAction.class);

    /** The location relative to. */
    private Component locationRelativeTo;

    /** The prefs. */
    private Preferences prefs;

    /** The opened db. */
    private MnyDb mnyDb;

    /** The disable read only check box. */
    private boolean disableReadOnlyCheckBox = false;

    /** The plugin. */
    private CreateMnyDbPlugin plugin;

    /**
     * Instantiates a new open db action.
     *
     * @param locationRelativeTo the location relative to
     * @param prefs              the prefs
     * @param mnyDb              the opened db
     */
    public OpenDbAction(Component locationRelativeTo, Preferences prefs, MnyDb mnyDb) {
        super();
        this.locationRelativeTo = locationRelativeTo;
        this.prefs = prefs;
        this.mnyDb = mnyDb;
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
     */
    @Override
    public void actionPerformed(ActionEvent event) {
        Component locationRelativeTo = getLocationRelativeTo();
        openDb(locationRelativeTo);
    }

    /**
     * Open db.
     *
     * @param locationRelativeTo the location relative to
     */
    private void openDb(Component locationRelativeTo) {
        List<String> recentOpenFileNames = OpenDbDialog.getRecentOpenFileNames(prefs);

        OpenDbDialog dialog = OpenDbDialog.showDialog(mnyDb, recentOpenFileNames, locationRelativeTo,
                disableReadOnlyCheckBox, getPlugin());
        if (!dialog.isCancel()) {
            mnyDb = dialog.getMnyDb();

            dbFileOpened(mnyDb, dialog);

            OpenDbDialog.updateRecentOpenFileNames(recentOpenFileNames, prefs);
        }
    }

    /**
     * Db file opened.
     *
     * @param newMnyDb the new opened db
     * @param dialog   the dialog
     */
    public abstract void dbFileOpened(MnyDb newMnyDb, OpenDbDialog dialog);

    /**
     * Gets the location relative to.
     *
     * @return the location relative to
     */
    public Component getLocationRelativeTo() {
        return locationRelativeTo;
    }

    /**
     * Sets the location relative to.
     *
     * @param locationRelativeTo the new location relative to
     */
    public void setLocationRelativeTo(Component locationRelativeTo) {
        this.locationRelativeTo = locationRelativeTo;
    }

    /**
     * Gets the prefs.
     *
     * @return the prefs
     */
    public Preferences getPrefs() {
        return prefs;
    }

    /**
     * Sets the prefs.
     *
     * @param prefs the new prefs
     */
    public void setPrefs(Preferences prefs) {
        this.prefs = prefs;
    }

    /**
     * Checks if is disable read only check box.
     *
     * @return true, if is disable read only check box
     */
    public boolean isDisableReadOnlyCheckBox() {
        return disableReadOnlyCheckBox;
    }

    /**
     * Sets the disable read only check box.
     *
     * @param disableReadOnlyCheckBox the new disable read only check box
     */
    public void setDisableReadOnlyCheckBox(boolean disableReadOnlyCheckBox) {
        this.disableReadOnlyCheckBox = disableReadOnlyCheckBox;
    }

    /**
     * Gets the plugin.
     *
     * @return the plugin
     */
    public CreateMnyDbPlugin getPlugin() {
        return plugin;
    }

    /**
     * Sets the plugin.
     *
     * @param plugin the new plugin
     */
    public void setPlugin(CreateMnyDbPlugin plugin) {
        this.plugin = plugin;
    }

    public MnyDb getMnyDb() {
        return mnyDb;
    }
}
package com.hungle.sunriise.viewer.spring;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.healthmarketscience.jackcess.DataType;

//@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "colName", "columnType", "value" })
public class JsonCell {
    @JsonProperty("colName")
    private String columnName;

    @JsonProperty("colType")
    private DataType columnType;

    private String value;

    public String getColumnName() {
        return columnName;
    }

    public void setColumnName(String columnName) {
        this.columnName = columnName;
    }

    public DataType getColumnType() {
        return columnType;
    }

    public void setColumnType(DataType columnType) {
        this.columnType = columnType;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}

/*******************************************************************************
 * Copyright (c) 2010 Hung Le
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *******************************************************************************/
package com.hungle.sunriise.viewer.table;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import javax.swing.table.TableModel;

import org.apache.logging.log4j.Logger;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ser.impl.SimpleBeanPropertyFilter;
import com.fasterxml.jackson.databind.ser.impl.SimpleFilterProvider;
import com.healthmarketscience.jackcess.Column;
import com.healthmarketscience.jackcess.Database;
import com.healthmarketscience.jackcess.Index;
import com.healthmarketscience.jackcess.Table;
import com.healthmarketscience.jackcess.impl.IndexData;
import com.healthmarketscience.jackcess.impl.JetFormat;
import com.healthmarketscience.jackcess.impl.JetFormat.CodecType;
import com.healthmarketscience.jackcess.impl.PageChannel;
import com.hungle.sunriise.crypt.EncryptionUtils;
import com.hungle.sunriise.diskusage.CalculateDiskUsage;
import com.hungle.sunriise.diskusage.GetDiskUsageCmd;
import com.hungle.sunriise.io.MnyDb;
import com.hungle.sunriise.json.JSONUtils;
import com.hungle.sunriise.json.TableMixin;
import com.hungle.sunriise.util.IndexLookup;
import com.hungle.sunriise.util.JackcessImplUtils;

// TODO: Auto-generated Javadoc
/**
 * The Class JackcessTableUtil.
 */
public class JackcessTableUtils {

    /** The Constant log. */
    private static final Logger LOGGER = org.apache.logging.log4j.LogManager.getLogger(JackcessTableUtils.class);

    /**
     * Parses the index info.
     *
     * @param table the table
     * @return the string
     * @throws IOException Signals that an I/O exception has occurred.
     */
    public static String parseIndexInfo(Table table) throws IOException {
        StringBuilder sb = new StringBuilder();

        sb.append("# Index info");
        sb.append("\n");
        sb.append("\n");

        sb.append("table: " + table.getName());
        sb.append("\n");
        sb.append("\n");

        List<? extends Index> indexes = table.getIndexes();
        sb.append("# size: " + indexes.size());
        sb.append("\n");

        int i = 1;
        for (Index index : indexes) {
            List<? extends com.healthmarketscience.jackcess.Index.Column> columns = index.getColumns();
            sb.append("  " + i++ + ". " + index.getName()
            // + " (columns: " + columns.size() + ")"
            );
            sb.append("\n");

            IndexData indexData = JackcessImplUtils.getIndexData(index);
            sb.append("    type: " + indexData.getClass().getName());
            sb.append("\n");
            sb.append("    uniqueEntryCount: " + JackcessImplUtils.getUniqueEntryCount(index));
            sb.append("\n");

            // isUnique
            sb.append("    unique: " + index.isUnique());
            sb.append("\n");
            sb.append("    shouldIgnoreNulls: " + index.shouldIgnoreNulls());
            sb.append("\n");

            // columns
            int j = 1;
            for (com.healthmarketscience.jackcess.Index.Column column : columns) {
                String columnName = column.getColumn().getTable().getName() + "." + column.getColumn().getName();
                sb.append("    column." + j++ + ": " + columnName);
                sb.append("\n");
            }

            // references
            // ForeignKeyReference reference =
            // JackcessImplUtils.getReference(index);
            // sb.append(" reference: " + reference);
            // sb.append("\n");
            // sb.append(" referencedIndex: " + index.getReferencedIndex());
            // sb.append("\n");

            sb.append("\n");
        }
        sb.append("\n");

        return sb.toString();
    }

    /**
     * Parses the key info.
     *
     * @param table the table
     * @return the string
     * @throws IOException Signals that an I/O exception has occurred.
     */
    public static String parseKeyInfo(Table table) throws IOException {
        StringBuilder sb = new StringBuilder();

        sb.append("# Key info");
        sb.append("\n");
        sb.append("\n");

        sb.append("table: " + table.getName());
        sb.append("\n");
        sb.append("\n");

        sb.append("# Primary keys:");
        sb.append("\n");
        IndexLookup indexLookup = new IndexLookup();
        for (Column column : table.getColumns()) {
            if (indexLookup.isPrimaryKeyColumn(column)) {
                sb.append("(PK) " + table.getName() + "." + column.getName() + ", " + indexLookup.getMax(column));
                sb.append("\n");

                List<Column> referencing = indexLookup.getReferencing(column);
                for (Column col : referencing) {
                    sb.append("    (referencing-FK) " + col.getTable().getName() + "." + col.getName());
                    sb.append("\n");
                }
            }
        }
        sb.append("\n");

        sb.append("# Foreign keys:");
        sb.append("\n");
        for (Column column : table.getColumns()) {
            List<Column> referenced = indexLookup.getReferencedColumns(column);
            for (Column col : referenced) {
                sb.append("(FK) " + table.getName() + "." + column.getName() + " -> " + col.getTable().getName() + "."
                        + col.getName());
                sb.append("\n");
            }
        }
        sb.append("\n");

        return sb.toString();
    }

    /**
     * Parses the table meta data.
     *
     * @param table the table
     * @return the string
     */
    public static String parseTableMetaData(final Table table) {
        boolean json = true;

        if (json) {
            return parseTableMetaDataJSON(table);
        } else {
            return parseTableMetaDataPlainText(table);
        }
    }

    /**
     * Parses the table meta data json.
     *
     * @param table the table
     * @return the string
     */
    private static String parseTableMetaDataJSON(final Table table) {
        String str = null;
        try {
            ObjectMapper mapper = new ObjectMapper();
            mapper.configure(JsonParser.Feature.ALLOW_COMMENTS, true); // Jackson
                                                                       // 1.2+

            SimpleFilterProvider filterProvider = new SimpleFilterProvider();
            mapper.setFilterProvider(filterProvider);

            mapper.addMixIn(Table.class, TableMixin.class);
            Set<String> tableProperties = new TreeSet<String>();
            tableProperties.add("name");
            tableProperties.add("columnCount");
            tableProperties.add("rowCount");
            tableProperties.add("hidden");
            tableProperties.add("system");
            tableProperties.add("columns");
            filterProvider.addFilter("TableMixinFilter", SimpleBeanPropertyFilter.filterOutAllExcept(tableProperties));

            mapper.addMixIn(Column.class, ColumnMixin.class);
            Set<String> columnProperties = new TreeSet<String>();
            columnProperties.add("name");
            columnProperties.add("type");
            // columnProperties.add("sqltype");
            columnProperties.add("variableLength");
            columnProperties.add("length");
            filterProvider.addFilter("ColumnMixinFilter",
                    SimpleBeanPropertyFilter.filterOutAllExcept(columnProperties));

            str = JSONUtils.serialize(table, mapper);
        } catch (IOException e) {
            LOGGER.error(e, e);
        }
        return str;
    }

    /**
     * Parses the table meta data plain text.
     *
     * @param table the table
     * @return the string
     */
    private static String parseTableMetaDataPlainText(final Table table) {
        StringBuilder sb = new StringBuilder();

        int pageCount = JackcessImplUtils.getApproximateOwnedPageCount(table);

        sb.append("pageCount=" + pageCount);
        sb.append("\n");

        sb.append(table.toString());

        return sb.toString();
    }

    /**
     * Parses the header info.
     *
     * @param table the table
     * @param mnyDb the opened db
     * @return the string
     * @throws IOException Signals that an I/O exception has occurred.
     */
    public static String parseHeaderInfo(Table table, MnyDb mnyDb) throws IOException {
        StringBuilder sb = new StringBuilder();

        sb.append("# Header info");
        sb.append("\n");
        sb.append("\n");

        sb.append("table: " + table.getName());
        sb.append("\n");
        sb.append("\n");

        Database db = table.getDatabase();

        PageChannel pageChannel = JackcessImplUtils.getPageChannel(db);
        ByteBuffer buffer = pageChannel.createPageBuffer();
        pageChannel.readPage(buffer, 0);

        JetFormat format = pageChannel.getFormat();
        sb.append("format: " + format.toString());
        sb.append("\n");

        CodecType msisam = CodecType.MSISAM;
        if (format.CODEC_TYPE == msisam) {
            EncryptionUtils.appendMSISAMInfo(buffer, mnyDb.getPassword(), mnyDb.getDb().getCharset(), format, sb);
        }

        // 0x00 4
        // ENGINE_NAME_OFFSET 0x04 15
        // OFFSET_VERSION 20 1
        // SALT_OFFSET 0x72 4
        // ENCRYPTION_FLAGS_OFFSET 0x298 1

        sb.append("\n");
        sb.append("\n");
        sb.append("# Size info");
        sb.append("\n");
        sb.append("\n");

        int bytes = CalculateDiskUsage.calculateTableByteCount(table);
        sb.append("Bytes: " + GetDiskUsageCmd.humanReadableByteCount(bytes));
        sb.append("\n");
        sb.append("Rows: " + table.getRowCount());
        sb.append("\n");
        sb.append("Columns: " + table.getColumnCount());
        sb.append("\n");

        return sb.toString();
    }

    /**
     * Find defaul llabel column index.
     *
     * @param jackcessTable the jackcess table
     * @param tableModel    the table model
     * @return the int
     */
    public static final int findDefaultLabelColumnIndex(Table jackcessTable, TableModel tableModel) {
        String[] columnNames = { "szFull", "szName" };
        int primaryKeyColumn = -1;
        int columnIndex = -1;
        if (jackcessTable != null) {
            IndexLookup indexLookup = new IndexLookup();
            for (Column column : jackcessTable.getColumns()) {
                if (indexLookup.isPrimaryKeyColumn(column)) {
                    if (primaryKeyColumn < 0) {
                        primaryKeyColumn = MnyTableModel.getColumnIndex(tableModel, column.getName());
                    }
                }
                if (columnNames != null) {
                    for (String name : columnNames) {
                        if (column.getName().compareTo(name) == 0) {
                            if (columnIndex < 0) {
                                columnIndex = column.getColumnIndex();
                            }
                        }
                    }
                }
            }
        }

        int index = -1;
        if (columnIndex >= 0) {
            index = columnIndex;
        }
        if (index < 0) {
            index = primaryKeyColumn;
        }
        if (index < 0) {
            index = 0;
        }
        return index;
    }

}

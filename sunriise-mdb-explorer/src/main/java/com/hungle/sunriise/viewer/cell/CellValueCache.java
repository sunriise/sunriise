/*******************************************************************************
 * Copyright (c) 2016 Hung Le
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *******************************************************************************/
package com.hungle.sunriise.viewer.cell;

import java.util.Map;

import org.apache.logging.log4j.Logger;

import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;

// TODO: Auto-generated Javadoc
/**
 * The Class CellValueCache.
 */
public class CellValueCache {

    /** The Constant log. */
    private static final Logger log = org.apache.logging.log4j.LogManager.getLogger(CellValueCache.class);

    /** The cells cache. */
    private final Cache<String, Object> cellsCache;

    /** The rows cache. */
    private final Cache<Integer, Map<String, Object>> rowsCache;

    /**
     * The Class NullCellValue.
     */
    private class NullCellValue {

        /** The cached key. */
        private String cachedKey;

        /**
         * Gets the cached key.
         *
         * @return the cached key
         */
        public String getCachedKey() {
            return cachedKey;
        }

        /**
         * Sets the cached key.
         *
         * @param cachedKey the new cached key
         */
        public void setCachedKey(String cachedKey) {
            this.cachedKey = cachedKey;
        }

        /**
         * Instantiates a new null cell value.
         *
         * @param cachedKey the cached key
         */
        public NullCellValue(String cachedKey) {
            super();
            this.cachedKey = cachedKey;
        }
    }

    /** The default cell cache maximum size. */
    private final int defaultCellCacheMaximumSize = 100000;

    /** The default row cache maximum size. */
    private final int defaultRowCacheMaximumSize = 50000;

    /**
     * Instantiates a new cell value cache.
     */
    public CellValueCache() {
        super();

        this.cellsCache = CacheBuilder.newBuilder().maximumSize(defaultCellCacheMaximumSize).build();

        this.rowsCache = CacheBuilder.newBuilder().maximumSize(defaultRowCacheMaximumSize).build();
    }

    /**
     * Put.
     *
     * @param cachedKey the cached key
     * @param aValue    the a value
     */
    public void put(String cachedKey, Object aValue) {
        if (aValue == null) {
            aValue = new NullCellValue(cachedKey);
            if (log.isDebugEnabled()) {
                log.debug("put NullCellValue, cachedKey=" + cachedKey);
            }
        }
        cellsCache.put(cachedKey, aValue);
    }

    /**
     * Gets the if present.
     *
     * @param cachedKey the cached key
     * @return the if present
     */
    public Object getIfPresent(String cachedKey) {
        return cellsCache.getIfPresent(cachedKey);
    }

    /**
     * Gets the.
     *
     * @param cachedKey the cached key
     * @return the object
     */
    public Object get(String cachedKey) {
        Object value = null;

        if (log.isDebugEnabled()) {
            log.debug("> get, cachedKey=" + cachedKey);
        }
        value = cellsCache.getIfPresent(cachedKey);
        if (value == null) {
            return null;
        }

        if (value instanceof NullCellValue) {
            if (log.isDebugEnabled()) {
                log.debug("get NullCellValue, cachedKey=" + cachedKey);
            }
            value = null;
        }
        return value;
    }

    /**
     * Put rows cache.
     *
     * @param rowIndex the row index
     * @param rowData  the row data
     */
    public void putRowsCache(int rowIndex, Map<String, Object> rowData) {
        rowsCache.put(rowIndex, rowData);
    }

    /**
     * Gets the rows cache.
     *
     * @param rowIndex the row index
     * @return the rows cache
     */
    public Map<String, Object> getRowsCache(int rowIndex) {
        if (log.isDebugEnabled()) {
            log.debug("> getRowsCache, rowIndex=" + rowIndex);
        }

        return rowsCache.getIfPresent(rowIndex);
    }

    /**
     * Invalidate all.
     */
    public void invalidateAll() {
        if (log.isDebugEnabled()) {
            log.debug("> invalidateAll");

            if (cellsCache != null) {
                cellsCache.invalidateAll();
            }
            if (rowsCache != null) {
                rowsCache.invalidateAll();
            }
        }
    }

    /**
     * Close.
     */
    public void close() {
        invalidateAll();
    }
}

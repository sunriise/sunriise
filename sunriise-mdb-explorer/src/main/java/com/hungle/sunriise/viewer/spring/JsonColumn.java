package com.hungle.sunriise.viewer.spring;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.healthmarketscience.jackcess.DataType;

@JsonPropertyOrder({ "name", "type" })
public class JsonColumn {
    private String name;

    private DataType type;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public DataType getType() {
        return type;
    }

    public void setType(DataType type) {
        this.type = type;
    }

}

/*******************************************************************************
 * Copyright (c) 2010 Hung Le
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *******************************************************************************/
package com.hungle.sunriise.viewer.misc;

import com.healthmarketscience.jackcess.Table;
import com.hungle.sunriise.diskusage.CalculateDiskUsage;
import com.hungle.sunriise.diskusage.GetDiskUsageCmd;

// TODO: Auto-generated Javadoc
/**
 * The Class TableListItem.
 */
public class TableListItem {

    /** The table. */
    private Table table;
    private int bytes;
    private String bytesString;

    /**
     * Gets the table.
     *
     * @return the table
     */
    public Table getTable() {
        return table;
    }

    /**
     * Sets the table.
     *
     * @param table the new table
     */
    public void setTable(Table table) {
        this.table = table;
        this.bytes = CalculateDiskUsage.calculateTableByteCount(table);
        this.bytesString = GetDiskUsageCmd.humanReadableByteCount(this.bytes);
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return table.getName() + " - (" + table.getRowCount() + "/" + this.bytesString + ")";
    }
}

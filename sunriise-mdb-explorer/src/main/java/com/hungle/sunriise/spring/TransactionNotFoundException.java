package com.hungle.sunriise.spring;

public class TransactionNotFoundException extends RuntimeException {
    TransactionNotFoundException(Integer id, Integer tid) {
        super("Could not find transaction " + tid + " for account " + id);
    }
}

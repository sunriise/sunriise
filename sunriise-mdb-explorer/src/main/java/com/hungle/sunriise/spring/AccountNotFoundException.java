package com.hungle.sunriise.spring;

public class AccountNotFoundException extends RuntimeException {
    AccountNotFoundException(Integer id) {
        super("Could not find account " + id);
    }
}

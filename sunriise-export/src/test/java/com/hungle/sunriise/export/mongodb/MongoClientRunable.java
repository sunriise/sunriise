package com.hungle.sunriise.export.mongodb;

import java.io.IOException;

import org.junit.Assert;

import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;

public interface MongoClientRunable {
    void run(MongoClient mongoClient) throws IOException;

    public static void connectAndRun(MongoClientRunable runable) throws IOException {
        String host = EmbeddedMongodServer.DEFAULT_HOST;
        int port = EmbeddedMongodServer.DEFAULT_PORT;

        MongoClient mongoClient = null;
        try {
            String uriString = "mongodb://" + host + ":" + port;
            MongoClientURI mongoClientURI = ExportToMongoDb.createMongoClientURI(uriString);
            mongoClient = ExportToMongoDb.createMongoClient(mongoClientURI);
            Assert.assertNotNull(mongoClient);

            runable.run(mongoClient);
        } finally {
            if (mongoClient != null) {
                mongoClient.close();
                mongoClient = null;
            }
        }
    }

}

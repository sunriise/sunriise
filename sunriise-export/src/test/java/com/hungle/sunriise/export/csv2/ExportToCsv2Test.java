package com.hungle.sunriise.export.csv2;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.apache.logging.log4j.Logger;
import org.junit.Ignore;
import org.junit.Test;

import com.hungle.sunriise.export.ExportNoPerAccounts;
import com.hungle.sunriise.export.cvs2.ExportToCsv2;
import com.hungle.sunriise.io.sample.MnySampleFile;
import com.hungle.sunriise.io.sample.MnySampleFileFactory;

import app.ModuleExportMainTest;

public class ExportToCsv2Test {
    private static final Logger LOGGER = org.apache.logging.log4j.LogManager.getLogger(ExportToCsv2Test.class);

    @Test
    @Ignore
    public void testOpenCsvWrite() throws IOException {
        MnySampleFile testFile = MnySampleFileFactory.getSunsetSampleFile();
        File sampleFile = MnySampleFile.getSampleFileFromModuleProject(testFile);

        String password = testFile.getPassword();

        Path tempDirPath = Paths.get("/tmp/hle");
        LOGGER.info("  tempDir=" + tempDirPath);

        testExporter(sampleFile, password, tempDirPath);
    }

    @Test
    public void testExportOneToCsv2() throws IOException {
        MnySampleFile dbFile = MnySampleFile.getMnySampleFile(MnySampleFile.SUNSET_SAMPLE_PWD_MNY);

        boolean deleteDir = true;

        testExporter(dbFile, deleteDir);
    }

    @Test
    public void testExportToCsv2() throws IOException {
        boolean deleteDir = true;

        for (MnySampleFile dbFile : MnySampleFile.SAMPLE_FILES) {
            if (dbFile.isBackup()) {
                continue;
            }

            testExporter(dbFile, deleteDir);
        }
    }

    private void testExporter(MnySampleFile testFile, boolean deleteDir) throws IOException {
        File sampleFile = MnySampleFile.getSampleFileFromModuleProject(testFile);

        String password = testFile.getPassword();

        Path tempDirPath = Files.createTempDirectory("ExportToCsv2Test");
        try {
            LOGGER.info("  tempDir=" + tempDirPath);

            testExporter(sampleFile, password, tempDirPath);
        } finally {
            if (deleteDir) {
                ModuleExportMainTest.deleteTempDir(tempDirPath.toFile());
            } else {
                LOGGER.info("SKIP Deleting directory=" + tempDirPath.toAbsolutePath().toString());
            }
        }
    }

    private void testExporter(File file, String password, Path path) throws IOException {
        ExportNoPerAccounts exporter = new ExportToCsv2();
        File outDir = path.toFile();
        outDir.mkdirs();
        exporter.setOutDir(outDir);

        exporter.export(file, password);

        checkExport(path);

        boolean cleanup = false;
        if (cleanup) {
            // clean up
            Files.walkFileTree(path, new DeleteFileVisitor());
        }
    }

    private void checkExport(Path outDir) throws IOException {
    }
}

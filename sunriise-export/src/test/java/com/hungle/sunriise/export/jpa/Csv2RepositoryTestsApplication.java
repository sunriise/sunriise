package com.hungle.sunriise.export.jpa;

import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * The Class SimpleConfiguration.
 */
@SpringBootApplication
class Csv2RepositoryTestsApplication {
}
package com.hungle.sunriise.export.mongodb;

import java.io.File;
import java.io.IOException;

import org.apache.logging.log4j.Logger;
import org.junit.Test;

import com.hungle.sunriise.io.FileUtils;
import com.hungle.sunriise.io.MnyDb;
import com.hungle.sunriise.io.sample.MnySampleFile;
import com.hungle.sunriise.util.StopWatch;

public class ExportToMongoFormatTest {
    private static final Logger LOGGER = org.apache.logging.log4j.LogManager.getLogger(ExportToMongoFormatTest.class);

    @Test
    public void testExportToMongoFormatSampleDb() {
        String outDirName = "target/export";
        MnyDb mnyDb = null;
        try {
            mnyDb = FileUtils.openSampleDb();
            MongoUtils.exportToMongoImportFormat(mnyDb, outDirName);
        } catch (IOException e) {
            LOGGER.error(e);
        } finally {
            if (mnyDb != null) {
                try {
                    mnyDb.close();
                } catch (IOException e) {
                    LOGGER.warn(e);
                } finally {
                    mnyDb = null;
                }
            }
        }
    }

    @Test
    public void testExportToMongoFormatLargeDb() {
        StopWatch stopWatch = new StopWatch();
        try {
            File testFile = new File(MnySampleFile.LOCAL_LARGE_FILE_MNY);
            String password = null;
            if (testFile.exists()) {
                String outDirName = "target/export2";
                MongoUtils.exportToMongoImportFormat(testFile, password, outDirName);
            }
        } finally {
            stopWatch.logTiming(LOGGER, "testExportToMongoFormatLargeDb");
        }
    }
}

package com.hungle.sunriise.export.mongodb;

import java.io.IOException;
import java.net.UnknownHostException;

import org.apache.logging.log4j.Logger;

import de.flapdoodle.embed.mongo.MongodExecutable;
import de.flapdoodle.embed.mongo.MongodProcess;
import de.flapdoodle.embed.mongo.MongodStarter;
import de.flapdoodle.embed.mongo.config.IMongoCmdOptions;
import de.flapdoodle.embed.mongo.config.IMongodConfig;
import de.flapdoodle.embed.mongo.config.MongoCmdOptionsBuilder;
import de.flapdoodle.embed.mongo.config.MongodConfigBuilder;
import de.flapdoodle.embed.mongo.config.Net;
import de.flapdoodle.embed.mongo.distribution.Version;
import de.flapdoodle.embed.process.runtime.Network;

// TODO: Auto-generated Javadoc
/**
 * The Class MongodServer.
 */
public class EmbeddedMongodServer {

    /** The Constant LOGGER. */
    private static final Logger LOGGER = org.apache.logging.log4j.LogManager.getLogger(EmbeddedMongodServer.class);

    /** The Constant DEFAULT_PORT. */
    public static final int DEFAULT_PORT = 37017;

    /** The Constant DEFAULT_BIND_IP. */
    private static final String DEFAULT_BIND_IP = "127.0.0.1";

    /** The _mongod exe. */
    private MongodExecutable _mongodExe;

    /** The _mongod. */
    private MongodProcess _mongod;

    /** The Constant DEFAULT_HOST. */
    public static final String DEFAULT_HOST = "localhost";

    /**
     * Instantiates a new mongod server.
     *
     * @throws IOException Signals that an I/O exception has occurred.
     */
    public EmbeddedMongodServer() throws IOException {
        super();
        // one-time initialization code
        setup();
    }

    public void stop() {
        // one-time cleanup code
        teardown();
    }

    private void setup() throws UnknownHostException, IOException {
        String OS = System.getProperty("os.name").toLowerCase();
        MongoCmdOptionsBuilder builder = new MongoCmdOptionsBuilder();
        if (OS.indexOf("win") >= 0) {
            String storageEngineName = "mmapv1";
            LOGGER.info("storageEngineName=" + storageEngineName);
            builder = builder.useStorageEngine(storageEngineName);
        }

        IMongoCmdOptions cmdOptions = builder.build();

        String bindIp = DEFAULT_BIND_IP;
        int port = DEFAULT_PORT;
        Net net = new Net(bindIp, port, Network.localhostIsIPv6());

        IMongodConfig config = new MongodConfigBuilder().version(Version.Main.PRODUCTION).cmdOptions(cmdOptions)
                .net(net).build();

//    	Command command = Command.MongoD;
//        IRuntimeConfig runtimeConfig = new RuntimeConfigBuilder()
//        		.defaults(command)
//        		.artifactStore(new ExtractedArtifactStoreBuilder()
//        			.defaults(command)
//        			.download(new DownloadConfigBuilder()
//        					.defaultsForCommand(command).build())
//        			.executableNaming(new UserTempNaming()))
//        		.build();

        MongodStarter starter = MongodStarter.getDefaultInstance();
//    	MongodStarter starter = MongodStarter.getInstance(runtimeConfig);

        _mongodExe = starter.prepare(config);
        _mongod = _mongodExe.start();
    }

    private void teardown() {
        if (_mongod != null) {
            try {
                _mongod.stop();
            } finally {
                _mongod = null;
            }
        }
        if (_mongodExe != null) {
            try {
                _mongodExe.stop();
            } finally {
                _mongodExe = null;
            }
        }
    }

}
/*******************************************************************************
 * Copyright (c) 2016 Hung Le
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *******************************************************************************/
package com.hungle.sunriise.export.json;

import java.io.File;
import java.io.IOException;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.SimpleFileVisitor;

import org.apache.logging.log4j.Logger;
import org.junit.Assert;
import org.junit.Test;

import com.hungle.sunriise.export.AbstractExportToDir;
import com.hungle.sunriise.io.sample.MnySampleFile;
import com.hungle.sunriise.io.sample.MnySampleFileFactory;

import app.ModuleExportMainTest;

// TODO: Auto-generated Javadoc
/**
 * The Class ExportToJSONTest.
 */
public class ExportToJSONTest {

    /** The Constant log. */
    private static final Logger LOGGER = org.apache.logging.log4j.LogManager.getLogger(ExportToJSONTest.class);

    /**
     * Test export.
     *
     * @throws IOException Signals that an I/O exception has occurred.
     */
    @Test
    public void testExportToJSON() throws IOException {
        for (MnySampleFile dbFile : MnySampleFile.SAMPLE_FILES) {
            if (dbFile.isBackup()) {
                continue;
            }

            testExporter(dbFile);
        }
    }

    @Test
    public void testExportOneToJSON() throws IOException {
        MnySampleFile testFile = MnySampleFileFactory.getSunsetSampleFile();
        Assert.assertNotNull(testFile);

        File sampleFile = MnySampleFile.getSampleFileFromModuleProject(testFile);

        String password = testFile.getPassword();

        File dir = new File("target/testExportOneToJSON");
        Path tempDirPath = dir.toPath();
        if (dir.exists()) {
            ModuleExportMainTest.deleteTempDir(tempDirPath.toFile());
        }
        tempDirPath = Files.createDirectory(tempDirPath);
        LOGGER.info("  tempDir=" + tempDirPath.toFile().getAbsolutePath());
        testExporter(sampleFile, password, tempDirPath);
    }

    /**
     * Test export.
     *
     * @param testFile the db file
     * @throws IOException Signals that an I/O exception has occurred.
     */
    private void testExporter(MnySampleFile testFile) throws IOException {
        File sampleFile = MnySampleFile.getSampleFileFromModuleProject(testFile);

        String password = testFile.getPassword();

        Path tempDirPath = Files.createTempDirectory("ExportToJSONTest");
        try {
            LOGGER.info("  tempDir=" + tempDirPath);

            testExporter(sampleFile, password, tempDirPath);
        } finally {
            ModuleExportMainTest.deleteTempDir(tempDirPath.toFile());
        }
    }

    /**
     * Test export.
     *
     * @param file     the file
     * @param password the password
     * @param path     the path
     * @throws IOException Signals that an I/O exception has occurred.
     */
    private void testExporter(File file, String password, Path path) throws IOException {
        AbstractExportToDir exporter = new ExportToJSON();
        File outDir = path.toFile();
        outDir.mkdirs();
        exporter.setOutDir(outDir);

        exporter.export(file, password);

        checkExport(path);

        // clean up
        // Files.walkFileTree(path, new SimpleFileVisitor<Path>() {
        // @Override
        // public FileVisitResult visitFile(Path file, BasicFileAttributes
        // attrs) throws IOException {
        // Files.delete(file);
        // return FileVisitResult.CONTINUE;
        // }
        //
        // @Override
        // public FileVisitResult postVisitDirectory(Path dir, IOException exc)
        // throws IOException {
        // Files.delete(dir);
        // return FileVisitResult.CONTINUE;
        // }
        // });
    }

    private void checkExport(Path outDir) throws IOException {
        File file = null;

        final String[] topLevelFileNames = { "accounts.json", "categories.json", "currencies.json", "payees.json",
                "securities.json", };
        for (String topLevelFilesName : topLevelFileNames) {
            file = new File(outDir.toFile(), topLevelFilesName);
            Assert.assertTrue(file.exists());
        }

        final String[] fileNames = { "account.json", "filteredTransactions.json", "transactions.json", };
        final File accountsDir = new File(outDir.toFile(), "accounts");
        Assert.assertTrue(accountsDir.isDirectory());
        Files.walkFileTree(accountsDir.toPath(), new SimpleFileVisitor<Path>() {
            @Override
            public FileVisitResult postVisitDirectory(Path dir, IOException exc) throws IOException {
                if (dir.toFile().compareTo(accountsDir) == 0) {
                    return FileVisitResult.CONTINUE;
                }

                FileVisitResult result = super.postVisitDirectory(dir, exc);
                if (result == FileVisitResult.CONTINUE) {
                    File file = null;
                    for (String fileName : fileNames) {
                        file = new File(dir.toFile(), fileName);
                        if (LOGGER.isDebugEnabled()) {
                            LOGGER.debug("  file=" + file.getAbsolutePath());
                        }
                        Assert.assertTrue(file.exists());
                    }
                }

                return FileVisitResult.CONTINUE;
            }
        });

    }
}

package com.hungle.sunriise.export.ofx;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.logging.log4j.Logger;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.xmlunit.diff.Comparison;
import org.xmlunit.diff.ComparisonResult;
import org.xmlunit.diff.DifferenceEvaluator;

public class OfxDifferenceEvaluator implements DifferenceEvaluator {
    private static final Logger LOGGER = org.apache.logging.log4j.LogManager.getLogger(OfxDifferenceEvaluator.class);

    private static final String[] DEFAULT_IGNORE_NODES = { "DTSERVER", "TRNUID", "DTASOF", "DTPRICEASOF", "DTSTART",
            "DTEND", "DTPOSTED" };

    private String[] ignoreNodes = DEFAULT_IGNORE_NODES;

    @Override
    public ComparisonResult evaluate(Comparison comparison, ComparisonResult outcome) {
        if (outcome == ComparisonResult.EQUAL)
            return outcome; // only evaluate differences.
        final Node controlNode = comparison.getControlDetails().getTarget();
        final Node testNode = comparison.getTestDetails().getTarget();
        if (controlNode.getParentNode() instanceof Element && testNode.getParentNode() instanceof Element) {
            Element controlElement = (Element) controlNode.getParentNode();
            Element testElement = (Element) testNode.getParentNode();
            if (isIgnorable(controlElement, testElement)) {
                if (isDateNode(testElement)) {
                    if (checkDateNode(testElement)) {
                        return ComparisonResult.SIMILAR;
                    } else {
                        return outcome;
                    }
                }
                return ComparisonResult.SIMILAR;
            }
        }
        return outcome;
    }

    private boolean checkDateNode(Element testElement) {
        final String testValue = testElement.getTextContent();
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("testValue=" + testValue);
        }
        // 20171113104504.993[-0800:PST]
        Pattern pattern = Pattern.compile("\\d{14}\\.\\d{1,3}\\[[\\+\\-0-9]+:[A-Z]{3}\\]");
        Matcher matcher = pattern.matcher(testValue);
        return matcher.matches();
    }

    private boolean isDateNode(Element testElement) {
        String nodeName = testElement.getNodeName();
        if (nodeName.startsWith("DT")) {
            return true;
        }
        return false;
    }

    private boolean isIgnorable(Element controlElement, Element testElement) {
        for (String elementName : ignoreNodes) {
            boolean matched = controlElement.getNodeName().equals(elementName);
            if (matched) {
                return true;
            }
        }
        return false;
    }

    public String[] getIgnoreNodes() {
        return ignoreNodes;
    }

    public void setIgnoreNodes(String[] ignoreNodes) {
        this.ignoreNodes = ignoreNodes;
    }

}
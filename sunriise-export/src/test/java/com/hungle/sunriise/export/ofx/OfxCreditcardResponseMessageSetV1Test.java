package com.hungle.sunriise.export.ofx;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.xml.transform.Source;

import org.apache.logging.log4j.Logger;
import org.junit.Assert;
import org.junit.Test;
import org.xmlunit.builder.DiffBuilder;
import org.xmlunit.builder.Input;
import org.xmlunit.diff.Diff;

public class OfxCreditcardResponseMessageSetV1Test {
    private static final Logger LOGGER = org.apache.logging.log4j.LogManager
            .getLogger(OfxCreditcardResponseMessageSetV1Test.class);

    @Test
    public void test001() throws IOException {
        File file = File.createTempFile("sunriise-", ".ofx");
        file.deleteOnExit();
        LOGGER.info("file=" + file);

        List<CreditCardStatementTransaction> transactions = new ArrayList<>();
        CreditCardStatementTransaction transaction = null;
        transaction = new CreditCardStatementTransaction();
        transaction.setType("INT");
        transaction.setId("219867");
        transaction.setDtPostedDate(new Date());
        transaction.setAmount(-23.00);
        transaction.setName("Interest Charge");

        transactions.add(transaction);

        CreditCardStatement statement = new CreditCardStatement();
        statement.setAccountId("999988");
        statement.setCurrency("USD");
        statement.setDtStartDate(new Date());
        statement.setDtEndDate(new Date());
        statement.setLedgerBalanceBalanceAmount(200.29);
        statement.setLedgerBalanceDateAsOf(new Date());
        statement.setTransactions(transactions);

        OfxCreditcardResponseMessageSetV1 response = new OfxCreditcardResponseMessageSetV1(statement);
        Assert.assertNotNull(response);

        response.save(file);

        String controlFilename = "OfxCreditcardResponseMessageSetV1Test-test001.ofx";
        checkSavedFile(controlFilename, file);
    }

    protected void checkSavedFile(String controlFilename, File file) throws IOException {
        InputStream controlStream = this.getClass().getResourceAsStream(controlFilename);
        if (controlStream == null) {
            throw new IOException("Missing controlFile=" + controlFilename);
        }
        Source controlSource = Input.fromStream(controlStream).build();

        Source testSource = Input.fromFile(file).build();

        Diff myDiff = DiffBuilder.compare(controlSource).withTest(testSource)
                .withDifferenceEvaluator(new OfxDifferenceEvaluator()).ignoreComments().ignoreWhitespace()
                .checkForSimilar().build();
        Assert.assertNotNull(myDiff);
        Assert.assertFalse(myDiff.toString(), myDiff.hasDifferences());
    }

}

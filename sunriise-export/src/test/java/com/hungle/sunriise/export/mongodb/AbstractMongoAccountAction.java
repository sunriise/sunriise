/*******************************************************************************
 * Copyright (c) 2016 Hung Le
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *******************************************************************************/
package com.hungle.sunriise.export.mongodb;

import java.io.IOException;
import java.util.List;

import org.apache.logging.log4j.Logger;
import org.bson.Document;

import com.hungle.sunriise.mnyobject.Account;
import com.hungle.sunriise.mnyobject.Transaction;
import com.hungle.sunriise.util.AbstractAccountAction;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;

// TODO: Auto-generated Javadoc
/**
 * The Class AbstractMongoAccountAction.
 */
abstract class AbstractMongoAccountAction extends AbstractAccountAction {

    /** The Constant LOGGER. */
    private static final Logger LOGGER = org.apache.logging.log4j.LogManager
            .getLogger(AbstractMongoAccountAction.class);

    /** The collection. */
    protected MongoCollection<Document> collection = null;

    private final MongoDatabase mongoDb;

    static final String ID = "_id";

    static final String ACCOUNT_ID = "account." + ID;

    /*
     * (non-Javadoc)
     * 
     * @see com.hungle.sunriise.utils.AbstractAccountAction#execute()
     */
    public AbstractMongoAccountAction(MongoDatabase mongoDb) {
        this.mongoDb = mongoDb;
    }

    @Override
    public void execute() throws IOException {
        Account account = getAccount();

        this.collection = MongoUtils.getTransactionsCollection(mongoDb);

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("XXX account: id=" + account.getId() + ", name=" + account.getName());
        }

        List<Transaction> transactions = account.getTransactions();
        try {
            insertTransactions(transactions);
        } finally {
            afterInsertTransactions(account, transactions);
        }
    }

    /**
     * After insert transactions.
     *
     * @param account      the account
     * @param transactions the transactions
     */
    protected abstract void afterInsertTransactions(Account account, List<Transaction> transactions);

    /**
     * Insert transactions.
     *
     * @param transactions the transactions
     * @throws IOException Signals that an I/O exception has occurred.
     */
    protected abstract void insertTransactions(List<Transaction> transactions) throws IOException;

    /**
     * Gets the result count.
     *
     * @param account the account
     * @return the result count
     */
    protected int getResultCount(Account account) {
        FindIterable<Document> results = collection.find(new Document(ACCOUNT_ID, account.getId()));
        int resultCount = 0;
        for (Document result : results) {
            resultCount++;
        }
        return resultCount;
    }
}
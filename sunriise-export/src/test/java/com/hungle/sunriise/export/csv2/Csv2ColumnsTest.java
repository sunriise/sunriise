package com.hungle.sunriise.export.csv2;

import java.lang.reflect.Field;
import java.util.HashSet;
import java.util.Set;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Assert;
import org.junit.Test;

import com.hungle.sunriise.export.cvs2.AbstractCsv2Wrapper;
import com.hungle.sunriise.export.cvs2.model.Csv2Account;
import com.hungle.sunriise.export.cvs2.model.Csv2Category;
import com.hungle.sunriise.export.cvs2.model.Csv2Currency;
import com.hungle.sunriise.export.cvs2.model.Csv2Payee;
import com.hungle.sunriise.export.cvs2.model.Csv2Security;
import com.hungle.sunriise.export.cvs2.model.Csv2Transaction;
import com.opencsv.bean.CsvBindByPosition;

public class Csv2ColumnsTest {
    private static final Logger LOGGER = LogManager.getLogger(Csv2ColumnsTest.class);

    private static final int ACCOUNT_COUNT = 10;

    private static final int CATEGORY_COUNT = 5;

    private static final int CURRENCY_COUNT = 3;

    private static final int PAYEE_COUNT = 3;

    private static final int SECURITY_COUNT = 3;

    private static final int TRANSACTION_COUNT = 33;

    @Test
    public void testCsv2AccountHeaders() throws Exception {
        Class<Csv2Account> clz = Csv2Account.class;
        int count = ACCOUNT_COUNT;

        checkHeaderCount2(clz, count);
    }

    private <T extends AbstractCsv2Wrapper<U>, U> void checkHeaderCount2(Class<T> clz, int count) throws Exception {
        String[] headers = createInstance(clz).getHeaders();

        Assert.assertEquals(count, headers.length);

        checkHeaderCount(clz, count);
    }

    @Test
    public void testCsv2CategoryHeaders() throws Exception {
        Class<Csv2Category> clz = Csv2Category.class;
        int count = CATEGORY_COUNT;

        checkHeaderCount2(clz, count);
    }

    @Test
    public void testCsv2CurrencyHeaders() throws Exception {
        Class<Csv2Currency> clz = Csv2Currency.class;
        int count = CURRENCY_COUNT;

        checkHeaderCount2(clz, count);
    }

    @Test
    public void testCsv2PayeeHeaders() throws Exception {
        Class<Csv2Payee> clz = Csv2Payee.class;
        int count = PAYEE_COUNT;

        checkHeaderCount2(clz, count);
    }

    @Test
    public void testCsv2SecurityHeaders() throws Exception {
        Class<Csv2Security> clz = Csv2Security.class;
        int count = SECURITY_COUNT;

        checkHeaderCount2(clz, count);
    }

    @Test
    public void testCsv2TransactionHeaders() throws Exception {
        Class<Csv2Transaction> clz = Csv2Transaction.class;
        int count = TRANSACTION_COUNT;

        checkHeaderCount2(clz, count);
    }

    private <T> T createInstance(Class<T> clz) throws Exception {
        return clz.getDeclaredConstructor().newInstance();
    }

    private <T> void checkHeaderCount(Class<T> clz, int count) {
        Set<Integer> positions = new HashSet<>();

        for (Field field : clz.getDeclaredFields()) {
            if (field.isAnnotationPresent(CsvBindByPosition.class)) {
                CsvBindByPosition annotation = field.getAnnotation(CsvBindByPosition.class);
                int position = annotation.position();
                Assert.assertFalse(positions.contains(position));
                positions.add(position);
            }
        }
        Assert.assertEquals(count, positions.size());

        int previous = -1;
        for (Integer position : positions) {
            Assert.assertEquals(previous + 1, position.intValue());
            previous = position;
        }
    }
}

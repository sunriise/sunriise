package com.hungle.sunriise.export.qif;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;

import org.apache.logging.log4j.Logger;
import org.junit.Test;

import com.hungle.sunriise.io.sample.MnySampleFile;
import com.hungle.sunriise.io.sample.MnySampleFileFactory;

import app.ModuleExportMainTest;

public class ExportToQifTest {
    private static final Logger LOGGER = org.apache.logging.log4j.LogManager.getLogger(ExportToQifTest.class);

    @Test
    public void testExportToQif() throws IOException {
        List<MnySampleFile> sampleFiles = MnySampleFile.SAMPLE_FILES;

        for (MnySampleFile dbFile : sampleFiles) {
            if (dbFile.isBackup()) {
                continue;
            }
            boolean deleteDir = true;

            testExporter(dbFile, true);
        }
    }

    @Test
    public void testExportOneToQif() throws IOException {
        MnySampleFile testFile = MnySampleFileFactory.getSunsetSampleFile();
        File sampleFile = MnySampleFile.getSampleFileFromModuleProject(testFile);

        String password = testFile.getPassword();

        File dir = new File("target/testExportOneToQif");
        Path path = dir.toPath();
        testExporter(sampleFile, password, path);
    }

    private void testExporter(MnySampleFile testFile, boolean deleteDir) throws IOException {
        File sampleFile = MnySampleFile.getSampleFileFromModuleProject(testFile);

        String password = testFile.getPassword();

//        boolean deleteDir = true;
        Path tempDirPath = Files.createTempDirectory("ExportToQifTest");
        try {
            LOGGER.info("  tempDir=" + tempDirPath);
            testExporter(sampleFile, password, tempDirPath);
        } finally {
            if (deleteDir) {
                ModuleExportMainTest.deleteTempDir(tempDirPath.toFile());
            } else {
                LOGGER.info("SKIP Deleting directory=" + tempDirPath.toAbsolutePath().toString());
            }
        }
    }

    private void testExporter(File file, String password, Path path) throws IOException {
        ExportToQif exporter = new ExportToQif();
        File outDir = path.toFile();
        outDir.mkdirs();
        exporter.setOutDir(outDir);
        exporter.export(file, password);
    }

}

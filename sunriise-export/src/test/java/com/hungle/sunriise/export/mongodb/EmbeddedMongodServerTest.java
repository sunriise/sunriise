package com.hungle.sunriise.export.mongodb;

import java.io.IOException;

import org.apache.logging.log4j.Logger;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import com.mongodb.MongoClient;
import com.mongodb.client.MongoDatabase;

public class EmbeddedMongodServerTest {
    /** The Constant LOGGER. */
    private static final Logger LOGGER = org.apache.logging.log4j.LogManager.getLogger(EmbeddedMongodServerTest.class);

    /** The _mongod exe. */
    private static EmbeddedMongodServer _mongodServer;

    /**
     * One time set up.
     *
     * @throws IOException Signals that an I/O exception has occurred.
     */
    @BeforeClass
    public static void oneTimeSetUp() throws IOException {
        _mongodServer = new EmbeddedMongodServer();
    }

    /**
     * One time tear down.
     */
    @AfterClass
    public static void oneTimeTearDown() {
        // one-time cleanup code
        if (_mongodServer != null) {
            try {
                _mongodServer.stop();
            } finally {
                _mongodServer = null;
            }
        }
    }

    /**
     * Test connect.
     * 
     * @throws IOException
     */
    @Test
    public void testConnect() throws IOException {

        MongoClientRunable runable = new MongoClientRunable() {
            @Override
            public void run(MongoClient mongoClient) {
                LOGGER.info("db.connectPoint=" + mongoClient.getConnectPoint());

                String databaseName = ExportToMongoDb.DEFAULT_DATABASE_NAME;
                MongoDatabase db = mongoClient.getDatabase(databaseName);
                Assert.assertNotNull(db);

                LOGGER.info("db.name=" + db.getName());
            }
        };

        MongoClientRunable.connectAndRun(runable);
    }
}

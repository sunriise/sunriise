package com.hungle.sunriise.export;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

import org.junit.Assert;
import org.junit.Test;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.hungle.sunriise.json.JSONUtils;
import com.hungle.sunriise.mnyobject.impl.DefaultTransaction;

public class DeserializeTest {
    @Test
    public void testTransation() throws JsonParseException, JsonMappingException, IOException {
        Class<DefaultTransaction> valueType = DefaultTransaction.class;
        URL resource = this.getClass().getResource("t-splits.json");
        Assert.assertNotNull(resource);

        try (InputStream stream = resource.openStream()) {
            DefaultTransaction t = JSONUtils.deserializeFromStream(stream, valueType);
            Assert.assertNotNull(t);
        }
    }
}

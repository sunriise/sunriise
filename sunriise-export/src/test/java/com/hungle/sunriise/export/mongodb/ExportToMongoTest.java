/*******************************************************************************
 * Copyright (c) 2016 Hung Le
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *******************************************************************************/
package com.hungle.sunriise.export.mongodb;

import java.io.File;
import java.io.IOException;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.Logger;
import org.bson.Document;
import org.bson.types.ObjectId;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import com.healthmarketscience.jackcess.Database;
import com.hungle.sunriise.dbutil.TableAccountUtils;
import com.hungle.sunriise.io.MnyDb;
import com.hungle.sunriise.io.sample.MnySampleFile;
import com.hungle.sunriise.mnyobject.Account;
import com.hungle.sunriise.mnyobject.Category;
import com.hungle.sunriise.mnyobject.Currency;
import com.hungle.sunriise.mnyobject.MnyContext;
import com.hungle.sunriise.mnyobject.Payee;
import com.hungle.sunriise.mnyobject.Security;
import com.hungle.sunriise.mnyobject.Transaction;
import com.hungle.sunriise.util.AbstractAccountAction;
import com.hungle.sunriise.util.MnyContextUtils;
import com.mongodb.BasicDBObject;
import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;

// TODO: Auto-generated Javadoc
/**
 * The Class MongoTest.
 */
public class ExportToMongoTest {

    /** The Constant LOGGER. */
    private static final Logger LOGGER = org.apache.logging.log4j.LogManager.getLogger(ExportToMongoTest.class);

    private static final int DEFAULT_BATCH_SIZE = 100;

    public static final String DEFAULT_MONGO_CLIENT_URI = "mongodb://" + EmbeddedMongodServer.DEFAULT_HOST + ":"
            + EmbeddedMongodServer.DEFAULT_PORT;;

    /** The _mongod exe. */
    private static EmbeddedMongodServer _mongodServer;

    /**
     * One time set up.
     *
     * @throws IOException Signals that an I/O exception has occurred.
     */
    @BeforeClass
    public static void oneTimeSetUp() throws IOException {
        _mongodServer = new EmbeddedMongodServer();
    }

    /**
     * One time tear down.
     */
    @AfterClass
    public static void oneTimeTearDown() {
        // one-time cleanup code
        if (_mongodServer != null) {
            try {
                _mongodServer.stop();
            } finally {
                _mongodServer = null;
            }
        }
    }

    private abstract class AbstractMongoClientRunable implements MongoClientRunable {
        @Override
        public void run(MongoClient mongoClient) throws IOException {
            LOGGER.info("db.connectPoint=" + mongoClient.getConnectPoint());
            MnyDb mnyDb = null;
            try {
                String databaseName = ExportToMongoDb.DEFAULT_DATABASE_NAME;
                MongoDatabase mongoDb = mongoClient.getDatabase(databaseName);
                Assert.assertNotNull(mongoDb);

                LOGGER.info("db.name=" + mongoDb.getName());
                LOGGER.info("> PRE MongoClientRunable.run");
                MongoUtils.removeAllCollections(mongoDb);
                LOGGER.info("< PRE MongoClientRunable.run");

                mnyDb = getMnyFile();
                MnyContext mnyContext = MnyContextUtils.createMnyContext(mnyDb);

                run(mnyContext, mongoDb);
            } finally {
                if (mnyDb != null) {
                    try {
                        mnyDb.close();
                    } catch (IOException e) {
                        LOGGER.warn(e);
                    }
                }
            }
        }

        protected abstract void run(MnyContext mnyContext, MongoDatabase mongoDb) throws IOException;
    }

    /**
     * Gets the opened file.
     *
     * @return the opened file
     * @throws IOException Signals that an I/O exception has occurred.
     */
    private MnyDb getMnyFile() throws IOException {
        MnySampleFile mnyTestFile = MnySampleFile.getMnySampleFile(MnySampleFile.SUNSET_SAMPLE_PWD_MNY);
        MnyDb mnyDb = MnySampleFile.openSampleFileFromModuleProject(mnyTestFile);
        return mnyDb;
    }

    /**
     * Test insert one export to mongo.
     *
     * @throws IOException Signals that an I/O exception has occurred.
     */
    @Test
    public void testInsertOneByOneToMongo() throws IOException {
        MongoClientRunable runable = new AbstractMongoClientRunable() {
            @Override
            protected void run(MnyContext mnyContext, MongoDatabase mongoDb) throws IOException {
                insertOneByOneToMongo(mnyContext, mongoDb);
                validateInsertToMongo(mnyContext, mongoDb);
            }
        };
        MongoClientRunable.connectAndRun(runable);
    }

    private void insertOneByOneToMongo(MnyContext mnyContext, MongoDatabase mongoDb) throws IOException {
        // Accounts
        insertAccountsOneByOne(mongoDb, mnyContext);

        // Transactions
        insertTransactionsOneByOne(mnyContext, mongoDb);

        // Categories
        Collection<Category> categoryItems = mnyContext.getCategories().values();
        MongoUtils.insertItems(categoryItems, MongoUtils.CATEGORIES_COLLECTION_NAME, mongoDb);

        // Currencies
        Collection<Currency> currencyItems = mnyContext.getCurrencies().values();
        MongoUtils.insertItems(currencyItems, MongoUtils.CURRENCIES_COLLECTION_NAME, mongoDb);

        // Payees
        Collection<Payee> payeeItems = mnyContext.getPayees().values();
        MongoUtils.insertItems(payeeItems, MongoUtils.PAYEES_COLLECTION_NAME, mongoDb);

        // Securities
        Collection<Security> securityItems = mnyContext.getSecurities().values();
        MongoUtils.insertItems(securityItems, MongoUtils.SECURITIES_COLLECTION_NAME, mongoDb);
    }

    private void insertAccountsOneByOne(MongoDatabase mongoDb, MnyContext mnyContext) throws IOException {
        Map<Integer, Account> accountsMap = mnyContext.getAccounts();

        Collection<Account> accounts = accountsMap.values();
        final MongoCollection<Document> accountsCollection = MongoUtils.insertItems(accounts,
                MongoUtils.ACCOUNTS_COLLECTION_NAME, mongoDb);
        Assert.assertEquals(accounts.size(), accountsCollection.count());
    }

    /**
     * Transactions insert one.
     * 
     * @param mnyContext the mny context
     * @param mongoDb    the mongo db
     *
     * @throws IOException Signals that an I/O exception has occurred.
     */
    private void insertTransactionsOneByOne(MnyContext mnyContext, MongoDatabase mongoDb) throws IOException {
        // final MongoCollection<Document> collection =
        // MongoUtil.getTransactionsCollection(mongoDb);
        // transactionsCollection.withWriteConcern(WriteConcern.JOURNALED);

        // collection.createIndex(new
        // BasicDBObject(MongoUtil.TRANSACTION_ACCOUNT_ID_INDEX_NAME, 1));

        AbstractAccountAction accountAction = new AbstractMongoAccountAction(mongoDb) {
            @Override
            protected void insertTransactions(List<Transaction> transactions) throws IOException {
                LOGGER.info("  transactions.size=" + transactions.size());
                for (Transaction transaction : transactions) {
                    Document document = MongoUtils.serializeToDocument(transaction);
                    collection.insertOne(document);
                }
            }

            @Override
            protected void afterInsertTransactions(Account account, List<Transaction> transactions) {
                int resultCount = getResultCount(account);
                Assert.assertEquals(transactions.size(), resultCount);
            }
        };
        mnyContext.forEachAccount(accountAction);
    }

    /**
     * Test bulk write export to mongo.
     *
     * @throws IOException Signals that an I/O exception has occurred.
     */
    @Test
    public void testInsertBulkWriteToMongo() throws IOException {
        MongoClientRunable runable = new AbstractMongoClientRunable() {
            @Override
            protected void run(MnyContext mnyContext, MongoDatabase mongoDb) throws IOException {
                insertBulkWriteToMongo(mnyContext, mongoDb);

                validateInsertToMongo(mnyContext, mongoDb);
            }
        };
        MongoClientRunable.connectAndRun(runable);
    }

    private static final void validateInsertToMongo(MnyContext mnyContext, MongoDatabase mongoDb) {
        doQueries(mnyContext, mongoDb);
    }

    private void insertBulkWriteToMongo(MnyContext mnyContext, MongoDatabase mongoDb) throws IOException {
        int batchSize = DEFAULT_BATCH_SIZE;

        // Accounts
        Map<Integer, Account> accounts = mnyContext.getAccounts();

        Collection<Account> accountItems = accounts.values();
        MongoCollection<Document> accountsCollection = MongoUtils.bulkWriteItems(accountItems,
                MongoUtils.ACCOUNTS_COLLECTION_NAME, mongoDb, batchSize);
        Assert.assertEquals(accountItems.size(), accountsCollection.count());

        // Transactions
        transactionsBulkWrite(mnyContext, mongoDb, batchSize);

        // Categories
        Collection<Category> categoryItems = mnyContext.getCategories().values();
        MongoUtils.bulkWriteItems(categoryItems, MongoUtils.CATEGORIES_COLLECTION_NAME, mongoDb, batchSize);

        // Currencies
        Collection<Currency> currencyItems = mnyContext.getCurrencies().values();
        MongoUtils.bulkWriteItems(currencyItems, MongoUtils.CURRENCIES_COLLECTION_NAME, mongoDb, batchSize);

        // Payees
        Collection<Payee> payeeItems = mnyContext.getPayees().values();
        MongoUtils.bulkWriteItems(payeeItems, MongoUtils.PAYEES_COLLECTION_NAME, mongoDb, batchSize);

        // Securities
        Collection<Security> securityItems = mnyContext.getSecurities().values();
        MongoUtils.bulkWriteItems(securityItems, MongoUtils.SECURITIES_COLLECTION_NAME, mongoDb, batchSize);
    }

    /**
     * Transactions bulk write.
     *
     * @param mnyContext the mny context
     * @param mongoDb    the mongo db
     * @param batchSize  the batch size
     * @throws IOException Signals that an I/O exception has occurred.
     */
    private void transactionsBulkWrite(MnyContext mnyContext, final MongoDatabase mongoDb, final int batchSize)
            throws IOException {
        // final MongoCollection<Document> collection =
        // MongoUtil.getTransactionsCollection(mongoDb);

        // collection.createIndex(new
        // BasicDBObject(MongoUtil.TRANSACTION_ACCOUNT_ID_INDEX_NAME, 1));

        AbstractAccountAction accountAction = new AbstractMongoAccountAction(mongoDb) {
            @Override
            protected void insertTransactions(List<Transaction> transactions) throws IOException {
                LOGGER.info("  transactions.size=" + transactions.size());
                LOGGER.info("  batchSize=" + batchSize);

                MongoUtils.bulkWriteItems(transactions, collection, batchSize);
            }

            @Override
            protected void afterInsertTransactions(Account account, List<Transaction> transactions) {
                int resultCount = getResultCount(account);
                Assert.assertEquals(transactions.size(), resultCount);
            }
        };

        mnyContext.forEachAccount(accountAction);
    }

    /**
     * Do queries.
     *
     * @param mnyContext the mny context
     * @param mongoDb    the mongo db
     */
    private static final void doQueries(MnyContext mnyContext, MongoDatabase mongoDb) {
        // check accounts
        // checkAccounts(mnyContext, mongoDb);
        doQueryAccounts(mnyContext, mongoDb);

        MongoCollection<Document> categories = MongoUtils.getCategoriesCollection(mongoDb);
        Assert.assertNotNull(categories);
        Assert.assertEquals(mnyContext.getCategories().size(), categories.count());

        MongoCollection<Document> currencies = MongoUtils.getCurrenciesCollection(mongoDb);
        Assert.assertNotNull(currencies);
        Assert.assertEquals(mnyContext.getCurrencies().size(), currencies.count());

        MongoCollection<Document> payees = MongoUtils.getPayeesCollection(mongoDb);
        Assert.assertNotNull(payees);
        Assert.assertEquals(mnyContext.getPayees().size(), payees.count());

        MongoCollection<Document> securities = MongoUtils.getSecuritiesCollection(mongoDb);
        Assert.assertNotNull(securities);
        Assert.assertEquals(mnyContext.getSecurities().size(), securities.count());
    }

    private static final void doQueryAccounts(MnyContext mnyContext, MongoDatabase mongoDb) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("mongoDb=" + mongoDb.getName());
        }

        MongoCollection<Document> accountsDoc = MongoUtils.getAccountsCollection(mongoDb);
        Assert.assertNotNull(accountsDoc);
        Assert.assertEquals(mnyContext.getAccounts().size(), accountsDoc.count());

        // check transactions
        MongoCollection<Document> transactionsDoc = MongoUtils.getTransactionsCollection(mongoDb);
        Assert.assertNotNull(transactionsDoc);
        int totalTransactions = calculateTotalTransactions(mnyContext);
        Assert.assertEquals(totalTransactions, transactionsDoc.count());

        checkTransactionCountPerAccount(mnyContext, accountsDoc, transactionsDoc);
    }

    private static void checkTransactionCountPerAccount(MnyContext mnyContext, MongoCollection<Document> accountsDoc,
            MongoCollection<Document> transactionsDoc) {

        FindIterable<Document> accountDocs = accountsDoc.find();
        for (Document accountDoc : accountDocs) {
            Integer accountId = accountDoc.getInteger(AbstractMongoAccountAction.ID);

            Class<Document> resultClass = Document.class;
            int transactionDocCount = countTransactionsDoc(accountId, transactionsDoc, resultClass);

            int expected = mnyContext.getAccounts().get(accountId).getTransactions().size();
            int actual = transactionDocCount;
            Assert.assertEquals(expected, actual);
        }
    }

    private static <T> int countTransactionsDoc(Integer accountId, MongoCollection<Document> transactionsDoc,
            Class<T> resultClass) {
        int count = 0;

        BasicDBObject whereQuery = new BasicDBObject();
        whereQuery.put(AbstractMongoAccountAction.ACCOUNT_ID, accountId);
        // Class<T> resultClass = T.class;
        FindIterable<T> transactionDocs = transactionsDoc.find(whereQuery, resultClass);
        for (T transactionDoc : transactionDocs) {
            Assert.assertNotNull(transactionDoc);
            count++;
        }
        return count;
    }

    private static int calculateTotalTransactions(MnyContext mnyContext) {
        int totalTransactions = 0;
        for (Account account : mnyContext.getAccounts().values()) {
            List<Transaction> transaction = account.getTransactions();
            if ((transaction != null) && (transaction.size() > 0)) {
                totalTransactions += transaction.size();
            }
        }
        return totalTransactions;
    }

    private static final void checkAccounts(MnyContext mnyContext, MongoDatabase mongoDb) {
        MongoCollection<Document> accountsDoc = MongoUtils.getAccountsCollection(mongoDb);
        Assert.assertNotNull(accountsDoc);
        Map<Integer, Account> accounts = mnyContext.getAccounts();
        Assert.assertEquals(accounts.size(), accountsDoc.count());

        MongoCollection<Document> transactionsDoc = MongoUtils.getTransactionsCollection(mongoDb);
        Assert.assertNotNull(transactionsDoc);

        for (Account account : accounts.values()) {
            int found = 0;
            BasicDBObject query = new BasicDBObject();
            String hexString = Integer.toHexString(account.getId());
            LOGGER.info("hexString=" + hexString);
            query.put("_id", new ObjectId("0x" + hexString));
            FindIterable<Document> results = transactionsDoc.find(query);
            for (Document result : results) {
                found++;
            }
            Assert.assertEquals(account.getTransactions().size(), found);
        }
    }

    @Test
    public void testExportToMongoDb() throws IOException {
        List<MnySampleFile> sampleFiles = MnySampleFile.SAMPLE_FILES;

        // List<MnyTestFile> sampleFiles = new ArrayList<MnyTestFile>();
        // sampleFiles.add(MnyTestFile.getSampleMnyTestFile(MnyTestFile.SUNSET01_MNY));

        for (MnySampleFile dbFile : sampleFiles) {
            if (dbFile.isBackup()) {
                continue;
            }

            testExporter(dbFile);
        }
    }

    private void testExporter(MnySampleFile testFile) throws IOException {
        File sampleFile = MnySampleFile.getSampleFileFromModuleProject(testFile);

        String password = testFile.getPassword();

        testExporter(sampleFile, password);
    }

    private void testExporter(File file, String password) throws IOException {
        ExportToMongoDb exporter = new ExportToMongoDb();

        String uriString = ExportToMongoTest.DEFAULT_MONGO_CLIENT_URI;
        MongoClientURI mongoClientURI = ExportToMongoDb.createMongoClientURI(uriString);

        MongoClient mongoClient = null;
        MnyDb mnyDb = null;
        try {

            mongoClient = ExportToMongoDb.createMongoClient(mongoClientURI, exporter);

            mnyDb = new MnyDb(file, password);
            Database db = exporter.export(mnyDb);
            Assert.assertNotNull(db);

            validateInsertToMongo(mnyDb, mongoClient);
        } finally {
            if (mnyDb != null) {
                mnyDb.close();
                mnyDb = null;
            }

            if (mongoClient != null) {
                mongoClient.close();
                mongoClient = null;
            }
        }
    }

    private void validateInsertToMongo(MnyDb mnyDb, MongoClient mongoClient) throws IOException {
        MnyContext mnyContext = MnyContextUtils.createMnyContext(mnyDb);
        TableAccountUtils.populateAccounts(mnyContext);

        MongoDatabase mongoDb = mongoClient.getDatabase(ExportToMongoDb.DEFAULT_DATABASE_NAME);
        // LOGGER.info("> ZZZ export");
        // MongoUtil.listAllCollections(mongoDb);
        // LOGGER.info("> ZZZ export");

        validateInsertToMongo(mnyContext, mongoDb);
    }

}

/*******************************************************************************
 * Copyright (c) 2016 Hung Le
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *******************************************************************************/
package com.hungle.sunriise.export.mdb;

import java.io.File;
import java.io.IOException;
import java.util.Set;

import org.apache.logging.log4j.Logger;
import org.junit.Assert;
import org.junit.Test;

import com.healthmarketscience.jackcess.Table;
import com.hungle.sunriise.io.MnyDb;
import com.hungle.sunriise.io.sample.MnySampleFile;
import com.hungle.sunriise.util.StopWatch;

// TODO: Auto-generated Javadoc
/**
 * The Class ExportToMdbTest.
 */
public class ExportToMdbTest {

    /** The Constant LOGGER. */
    private static final Logger LOGGER = org.apache.logging.log4j.LogManager.getLogger(ExportToMdbTest.class);

    /**
     * Test export.
     *
     * @throws IOException Signals that an I/O exception has occurred.
     */
    @Test
    public void testExport() throws IOException {
        StopWatch stopWatch = new StopWatch();

        for (MnySampleFile dbFile : MnySampleFile.SAMPLE_FILES) {
            if (dbFile.isBackup()) {
                continue;
            }

            ExportToMdb exporter = new ExportToMdb();

            File sampleFile = MnySampleFile.getSampleFileFromModuleProject(dbFile);

            String password = dbFile.getPassword();
            String prefix = "exportToMdbTest-";
            String suffix = ".mny";
            File destFile = File.createTempFile(prefix, suffix);
            LOGGER.info("  destFile=" + destFile);

            destFile.deleteOnExit();
            stopWatch.click();
            try {
                exporter.export(sampleFile, password, destFile);
            } finally {
                stopWatch.logTiming(LOGGER, "Export sampleFile=" + sampleFile);
            }

            MnyDb srcDb = null;
            MnyDb destDb = null;
            try {
                stopWatch.click();

                srcDb = new MnyDb(sampleFile, password);
                destDb = new MnyDb(destFile, null);
                compareDatabases(srcDb, destDb);
            } finally {
                if (srcDb != null) {
                    srcDb.close();
                }
                srcDb = null;

                if (destDb != null) {
                    destDb.close();
                }
                destDb = null;
                stopWatch.logTiming(LOGGER, "Compared exportFile=" + destFile + " ");
            }
        }

    }

    /**
     * Compare databases.
     *
     * @param srcDb  the src db
     * @param destDb the dest db
     * @throws IOException Signals that an I/O exception has occurred.
     */
    private void compareDatabases(MnyDb srcDb, MnyDb destDb) throws IOException {
        Assert.assertNotNull(srcDb);
        Assert.assertNotNull(destDb);

        Assert.assertEquals(srcDb.getDb().getTableNames().size(), destDb.getDb().getTableNames().size());

        Set<String> tableNames = srcDb.getDb().getTableNames();
        for (String tableName : tableNames) {
            Table srcTable = srcDb.getDb().getTable(tableName);
            Assert.assertNotNull(srcTable);

            Table destTable = srcDb.getDb().getTable(tableName);
            Assert.assertNotNull(destTable);

            Assert.assertEquals(srcTable.getRowCount(), destTable.getRowCount());
        }
    }
}

package com.hungle.sunriise.export.jpa;

import java.io.File;
import java.io.IOException;

import org.apache.logging.log4j.Logger;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import com.hungle.sunriise.io.sample.MnySampleFile;
import com.hungle.sunriise.mnyobject.Account;
import com.hungle.sunriise.mnyobject.MnyContext;

@RunWith(SpringRunner.class)
//@Transactional
@SpringBootTest
@EntityScan("com.hungle.sunriise.export.cvs2.model")
//@DirtiesContext(classMode = ClassMode.BEFORE_EACH_TEST_METHOD)
//@AutoConfigureTestDatabase(replace = Replace.ANY)
//@TestPropertySource(properties = "spring.autoconfigure.exclude=org.springframework.boot.autoconfigure.mongo.embedded.EmbeddedMongoAutoConfiguration")
@TestPropertySource
public class Csv2RepositoryTests {
    private static final Logger LOGGER = org.apache.logging.log4j.LogManager.getLogger(Csv2RepositoryTests.class);

    @Autowired
    private MnyJpaExporter exporter;

    @Test
    public void testExport() throws IOException {
        for (MnySampleFile dbFile : MnySampleFile.SAMPLE_FILES) {
            if (dbFile.isBackup()) {
                continue;
            }

            testExport(dbFile);
        }
    }

//    @Test
//    public void test_MONEY2001_PWD_MNY() throws IOException {
//        MnySampleFile dbFile = MnySampleFile.getMnySampleFile(MnySampleFile.MONEY2001_PWD_MNY);
//        testExport(dbFile);
//    }
//
//    @Test
//    public void test_MONEY2002_MNY() throws IOException {
//        MnySampleFile dbFile = MnySampleFile.getMnySampleFile(MnySampleFile.MONEY2002_MNY);
//        testExport(dbFile);
//    }
//
//    @Test
//    public void test_MONEY2004_PWD_MNY() throws IOException {
//        MnySampleFile dbFile = MnySampleFile.getMnySampleFile(MnySampleFile.MONEY2004_PWD_MNY);
//        testExport(dbFile);
//    }
//
//    @Test
//    public void test_MONEY2005_PWD_MNY() throws IOException {
//        MnySampleFile dbFile = MnySampleFile.getMnySampleFile(MnySampleFile.MONEY2005_PWD_MNY);
//        testExport(dbFile);
//    }
//
//    @Test
//    public void test_MONEY2008_PWD_MNY() throws IOException {
//        MnySampleFile dbFile = MnySampleFile.getMnySampleFile(MnySampleFile.MONEY2008_PWD_MNY);
//        testExport(dbFile);
//    }
//
//    @Test
//    public void test_SUNSET01_MNY() throws IOException {
//        MnySampleFile dbFile = MnySampleFile.getMnySampleFile(MnySampleFile.SUNSET01_MNY);
//        testExport(dbFile);
//    }
//
//    @Test
//    public void test_SUNSET02_MNY() throws IOException {
//        MnySampleFile dbFile = MnySampleFile.getMnySampleFile(MnySampleFile.SUNSET02_MNY);
//        testExport(dbFile);
//    }
//
//    @Test
//    public void test_SUNSET03_XFER_MNY() throws IOException {
//        MnySampleFile dbFile = MnySampleFile.getMnySampleFile(MnySampleFile.SUNSET03_XFER_MNY);
//        testExport(dbFile);
//    }
//
//    @Test
//    public void test_SUNSET_401K_MNY() throws IOException {
//        MnySampleFile dbFile = MnySampleFile.getMnySampleFile(MnySampleFile.SUNSET_401K_MNY);
//        testExport(dbFile);
//    }
//
//    @Test
//    public void test_SUNSET_BILL_MNY() throws IOException {
//        MnySampleFile dbFile = MnySampleFile.getMnySampleFile(MnySampleFile.SUNSET_BILL_MNY);
//        testExport(dbFile);
//    }

    private void testExport(MnySampleFile dbFile) throws IOException {
        LOGGER.info("> testExport, dbFile={}", dbFile.getFileName());

        File mnyFile = MnySampleFile.getSampleFileFromModuleProject(dbFile);

        String password = dbFile.getPassword();

        try {
            exporter.deleteAll();
            Assert.assertEquals(0, exporter.getAccountRepository().count());
            Assert.assertEquals(0, exporter.getCategoryRepository().count());
            Assert.assertEquals(0, exporter.getCurrencyRepository().count());
            Assert.assertEquals(0, exporter.getPayeeRepository().count());
            Assert.assertEquals(0, exporter.getSecurityRepository().count());
            Assert.assertEquals(0, exporter.getTransactionRepository().count());

            testExport(mnyFile, password);
        } finally {
            exporter.deleteAll();
            Assert.assertEquals(0, exporter.getAccountRepository().count());
            Assert.assertEquals(0, exporter.getCategoryRepository().count());
            Assert.assertEquals(0, exporter.getCurrencyRepository().count());
            Assert.assertEquals(0, exporter.getPayeeRepository().count());
            Assert.assertEquals(0, exporter.getSecurityRepository().count());
            Assert.assertEquals(0, exporter.getTransactionRepository().count());
        }

    }

    private void testExport(File mnyFile, String password) throws IOException {
        exporter.export(mnyFile, password);

        MnyContext mnyContext = exporter.getMnyContext();
        Assert.assertEquals(mnyContext.getAccounts().values().size(), exporter.getAccountRepository().count());
        Assert.assertEquals(mnyContext.getCategories().values().size(), exporter.getCategoryRepository().count());
        Assert.assertEquals(mnyContext.getCurrencies().values().size(), exporter.getCurrencyRepository().count());
        Assert.assertEquals(mnyContext.getPayees().values().size(), exporter.getPayeeRepository().count());
        Assert.assertEquals(mnyContext.getSecurities().values().size(), exporter.getSecurityRepository().count());
        int count = 0;
        for (Account account : mnyContext.getAccounts().values()) {
            count += account.getTransactions().size();
        }
        Assert.assertEquals(count, exporter.getTransactionRepository().count());

//        LOGGER.info("account={}, category={}, currency={}, payee={}, security={}, transaction={}",
//                getCount(), exporter.getCategoryRepository().count(),
//                exporter.getCurrencyRepository().count(), exporter.getPayeeRepository().count(),
//                exporter.getSecurityRepository().count(), exporter.getTransactionRepository().count());
    }

//    private <T, ID> int getCount(CrudRepository<T, ID> repo) {
//        LOGGER.info("> getCount(), repo={}", repo.getClass());
//        int count = 0;
//
//        Iterable<T> all = repo.findAll();
//        for (T item : all) {
//            count++;
//        }
//
//        LOGGER.info("> getCount(), count={}", count);
//        return count;
//    }
}

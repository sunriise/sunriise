package app;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Collection;
import java.util.List;
import java.util.Set;

import org.apache.commons.io.FileUtils;
import org.apache.logging.log4j.Logger;
import org.junit.Assert;
import org.junit.Test;

import com.healthmarketscience.jackcess.Database;
import com.hungle.sunriise.export.mongodb.EmbeddedMongodServer;
import com.hungle.sunriise.export.mongodb.ExportToMongoDb;
import com.hungle.sunriise.io.MnyDb;
import com.hungle.sunriise.prices.DbInfo;
import com.mongodb.DB;
import com.mongodb.MongoClient;
import com.opencsv.bean.CsvBindByName;
import com.opencsv.bean.CsvToBeanBuilder;

public class ModuleExportMainTest {
    private static final Logger LOGGER = org.apache.logging.log4j.LogManager.getLogger(ModuleExportMainTest.class);

    public static final class SecPrice {
        public SecPrice() {
            super();
        }

        @CsvBindByName
        private String symbol;

        @CsvBindByName
        private String name;

        @CsvBindByName
        private Double price;

        @CsvBindByName(column = "date (yyyy-MM-dd)")
        private String date;

        @CsvBindByName(column = "time (HH:mm:ss)")
        private String time;

        @CsvBindByName(column = "tz offset")
        private String tzOffset;

        @CsvBindByName(column = "readable date")
        private String readableDate;

        @CsvBindByName
        private String type;

        public String getSymbol() {
            return symbol;
        }

        public void setSymbol(String symbol) {
            this.symbol = symbol;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public Double getPrice() {
            return price;
        }

        public void setPrice(Double price) {
            this.price = price;
        }

        public String getDate() {
            return date;
        }

        public void setDate(String date) {
            this.date = date;
        }

        public String getTime() {
            return time;
        }

        public void setTime(String time) {
            this.time = time;
        }

        public String getTzOffset() {
            return tzOffset;
        }

        public void setTzOffset(String tzOffset) {
            this.tzOffset = tzOffset;
        }

        public String getReadableDate() {
            return readableDate;
        }

        public void setReadableDate(String readableDate) {
            this.readableDate = readableDate;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }
    }

    @Test
    public void testExportCsv() throws IOException {
        final Path tempDirPath = Files.createTempDirectory("msmoney");

        File tempDir = tempDirPath.toFile();
        try {
            String[] args = { app.ModuleExportMain.CMD_EXPORT_CSV, DbInfo.SUNSET_SAMPLE, tempDir.getAbsolutePath() };

            LOGGER.info("args=" + args);
            app.ModuleExportMain.main(args);

            String[] fileNames = { "db.txt", "ACCT", "TRN", };
            checkFilesExist(tempDir, fileNames);
        } finally {
            ModuleExportMainTest.deleteTempDir(tempDir);
        }
    }

    @Test
    public void testExportJson() throws IOException {
        final Path tempDirPath = Files.createTempDirectory("msmoney");

        File tempDir = tempDirPath.toFile();
        try {
            String[] args = { app.ModuleExportMain.CMD_EXPORT_JSON, DbInfo.SUNSET_SAMPLE, tempDir.getAbsolutePath() };

            LOGGER.info("args=" + args);
            app.ModuleExportMain.main(args);

            String[] fileNames = { "accounts", "accounts.json", "categories.json", "currencies.json", "payees.json",
                    "securities.json", };
            checkFilesExist(tempDir, fileNames);
        } finally {
            ModuleExportMainTest.deleteTempDir(tempDir);
        }
    }

    @Test
    public void testExportJsonToMongoDb() throws IOException {
        String host = EmbeddedMongodServer.DEFAULT_HOST;
        int port = EmbeddedMongodServer.DEFAULT_PORT;
        String mongoClientURI = "mongodb://" + host + ":" + port;

        String[] args = { app.ModuleExportMain.CMD_EXPORT_JSON, DbInfo.SUNSET_SAMPLE, mongoClientURI };

        LOGGER.info("args=" + args);

        EmbeddedMongodServer _mongodServer = null;
        try {
            _mongodServer = new EmbeddedMongodServer();

            app.ModuleExportMain.main(args);

            MongoClient mongoClient = null;
            try {
                mongoClient = new MongoClient(mongoClientURI);

                Collection<DB> dbs = mongoClient.getUsedDatabases();
                for (DB db : dbs) {
                    String name = db.getName();
                    if (name.compareToIgnoreCase(ExportToMongoDb.DEFAULT_DATABASE_NAME) == 0) {
                        Set<String> collectionNames = db.getCollectionNames();
                        String[] expectedNames = { "accounts", "categories", "currencies", "filteredTransactions",
                                "payees", "securities", "transactions", };
                        for (String expectedName : expectedNames) {
                            Assert.assertTrue(collectionNames.contains(expectedName));
                        }
                    }

                }
            } finally {
                if (mongoClient != null) {
                    mongoClient.close();
                }
            }
        } finally {
            if (_mongodServer != null) {
                try {
                    _mongodServer.stop();
                } finally {
                    _mongodServer = null;
                }
            }
        }

    }

    @Test
    public void testExportMdb() throws IOException {

        File tempFile = File.createTempFile("msmoney", ".mdb");
        String[] args = { app.ModuleExportMain.CMD_EXPORT_MDB, DbInfo.SUNSET_SAMPLE, tempFile.getAbsolutePath() };

        LOGGER.info("args=" + args);
        app.ModuleExportMain.main(args);

        Assert.assertTrue(tempFile.exists());

        MnyDb mnyDb = null;
        try {
            mnyDb = new MnyDb(tempFile.getAbsolutePath(), null, true, false);
            Assert.assertNotNull(mnyDb);
            Database db = mnyDb.getDb();
            Assert.assertNotNull(db);
            Set<String> tableNames = db.getTableNames();
            Assert.assertNotNull(tableNames);
            Assert.assertTrue(tableNames.size() > 0);

            String[] expectedTableNames = { "ACCT", "TRN", };
            for (String expectedTableName : expectedTableNames) {
                Assert.assertTrue(tableNames.contains(expectedTableName));
            }

        } finally {
            if (mnyDb != null) {
                mnyDb.close();
                mnyDb = null;
            }
        }

        tempFile.delete();
    }

    @Test
    public void testReportSecPrices() throws IOException {

        File tempFile = File.createTempFile("msmoney", ".csv");
        String[] args = { app.ModuleExportMain.CMD_REPORT_SEC_PRICES, DbInfo.SUNSET_SAMPLE,
                tempFile.getAbsolutePath() };

        LOGGER.info("args=" + args);
        app.ModuleExportMain.main(args);
        Assert.assertTrue(tempFile.exists());

        List<SecPrice> beans = new CsvToBeanBuilder<SecPrice>(new FileReader(tempFile)).withType(SecPrice.class).build()
                .parse();
        Assert.assertNotNull(beans);
        Assert.assertTrue(beans.size() > 0);
        boolean found = false;
        for (SecPrice bean : beans) {
            if (bean.getName() != null) {
                if (bean.getSymbol().compareToIgnoreCase("ADM") == 0) {
                    found = true;
                }
            }
        }
        Assert.assertTrue(found);

        tempFile.delete();
    }

    private void checkFilesExist(File tempDir, String[] fileNames) {
        for (String fileName : fileNames) {
            File file = new File(tempDir, fileName);
            Assert.assertTrue(file.exists());
        }
    }

    public static final void deleteTempDir(File tempDir) {
        try {
            File dir = tempDir;
            LOGGER.info("Deleting directory=" + dir.getAbsolutePath());
            FileUtils.deleteDirectory(dir);
            // dir.delete();
        } catch (IOException ex) {
            LOGGER.error(ex, ex);
        }
    }
}

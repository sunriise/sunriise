/*******************************************************************************
 * Copyright (c) 2010 Hung Le
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *******************************************************************************/
package com.hungle.sunriise.export.ui;

import java.awt.Component;
import java.io.File;
import java.io.IOException;

import javax.swing.JFileChooser;
import javax.swing.ProgressMonitor;
import javax.swing.SwingUtilities;

import org.apache.commons.lang3.time.DurationFormatUtils;
import org.apache.logging.log4j.Logger;

import com.healthmarketscience.jackcess.Database;
import com.hungle.sunriise.export.mdb.ExportToMdb;
import com.hungle.sunriise.io.MnyDb;
import com.hungle.sunriise.util.StopWatch;

// TODO: Auto-generated Javadoc
/**
 * The Class ExportToMdbAction.
 */
public class ExportToMdbAction extends AbstractExportToAction {

    /** The Constant log. */
    private static final Logger LOGGER = org.apache.logging.log4j.LogManager.getLogger(ExportToMdbAction.class);

    private static final String EXPORT_TYPE_MDB = "*.mdb";

    private final class ExportToMdbWithProgressMonitor extends ExportToMdb {
        public ExportToMdbWithProgressMonitor(ProgressMonitor progressMonitor) {
            super();
            this.progressMonitor = progressMonitor;
        }

        private final ProgressMonitor progressMonitor;
        private int progressCount = 0;
        private int maxCount = 0;
        private String currentTable = null;
        private int maxRows;

        @Override
        protected void startCopyTables(int maxCount) {
            if (progressMonitor.isCanceled()) {
                return;
            }
            this.maxCount = maxCount;
            Runnable doRun = new Runnable() {

                @Override
                public void run() {
                    progressMonitor.setProgress(0);
                }
            };
            SwingUtilities.invokeLater(doRun);
            super.startCopyTables(maxCount);
        }

        @Override
        protected void endCopyTables(int count) {
            if (progressMonitor.isCanceled()) {
                return;
            }
            Runnable doRun = new Runnable() {

                @Override
                public void run() {
                    progressMonitor.setProgress(100);
                }
            };
            SwingUtilities.invokeLater(doRun);
            super.endCopyTables(count);
        }

        @Override
        protected boolean startCopyTable(String name) {
            if (progressMonitor.isCanceled()) {
                return false;
            }
            this.currentTable = name;
            Runnable doRun = new Runnable() {

                @Override
                public void run() {
                    progressMonitor.setNote("Table: " + currentTable);
                }
            };
            SwingUtilities.invokeLater(doRun);
            return super.startCopyTable(name);
        }

        @Override
        protected void endCopyTable(String name) {
            if (progressMonitor.isCanceled()) {
                return;
            }
            progressCount++;
            Runnable doRun = new Runnable() {

                @Override
                public void run() {
                    progressMonitor.setProgress((progressCount * 100) / maxCount);
                }
            };
            SwingUtilities.invokeLater(doRun);
            super.endCopyTable(name);
        }

        @Override
        protected boolean startAddingRows(int max) {
            if (progressMonitor.isCanceled()) {
                return false;
            }
            this.maxRows = max;
            return super.startAddingRows(max);
        }

        @Override
        protected boolean addedRow(int count) {
            if (progressMonitor.isCanceled()) {
                return false;
            }
            final String str = " (Copying rows: " + ((count * 100) / this.maxRows) + "%" + ")";
            Runnable doRun = new Runnable() {

                @Override
                public void run() {
                    progressMonitor.setNote("Table: " + currentTable + str);
                }
            };
            SwingUtilities.invokeLater(doRun);
            return super.addedRow(count);
        }

        @Override
        protected void endAddingRows(int count, long delta) {
            if (progressMonitor.isCanceled()) {
                return;
            }
            super.endAddingRows(count, delta);
        }
    }

    /**
     * The Class ExportToMdbTask.
     */
    private final class ExportToMdbTask implements Runnable {
        /** The progress monitor. */
        private final ProgressMonitor progressMonitor;

        /** The dest file. */
        private final File destFile;

        /** The source. */
        private final Component source;

        /**
         * Instantiates a new export to mdb task.
         *
         * @param progressMonitor the progress monitor
         * @param destFile        the dest file
         * @param source          the source
         */
        private ExportToMdbTask(ProgressMonitor progressMonitor, File destFile, Component source) {
            this.progressMonitor = progressMonitor;
            this.destFile = destFile;
            this.source = source;
        }

        /*
         * (non-Javadoc)
         * 
         * @see java.lang.Runnable#run()
         */
        @Override
        public void run() {
            StopWatch stopWatch = new StopWatch();
            stopWatch.click();

            MnyDb mnyDb = null;
            Database destDb = null;
            Exception exception = null;

            try {
                mnyDb = getExportToContext().getMnyDb();

                ExportToMdb exporter = new ExportToMdbWithProgressMonitor(progressMonitor);
                destDb = exporter.export(mnyDb, destFile);
            } catch (IOException e) {
                LOGGER.error(e);
                exception = e;
            } finally {
                if (destDb != null) {
                    try {
                        destDb.close();
                    } catch (IOException e1) {
                        LOGGER.warn(e1);
                    } finally {
                        destDb = null;
                    }
                }
                long durationMillis = stopWatch.click();
                String durationWords = DurationFormatUtils.formatDurationWords(durationMillis, true, true);
                LOGGER.info("  took {}", durationWords);
                LOGGER.info("< DONE, exported to file=" + destFile.getAbsolutePath());
                ExportToDirTask.doFinally(exception, getExportType(), source);
            }
        }
    }

    public ExportToMdbAction(ExportToContext exportToContext) {
        super(exportToContext);
        setExportType(EXPORT_TYPE_MDB);
        setDestinationMode(JFileChooser.FILES_ONLY);
    }

    @Override
    protected Runnable createExportTask(ProgressMonitor progressMonitor, File destDir, Component source) {
        Runnable task = new ExportToMdbTask(progressMonitor, destDir, source);
        return task;
    }
}
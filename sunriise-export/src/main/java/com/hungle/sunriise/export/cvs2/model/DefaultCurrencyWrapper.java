package com.hungle.sunriise.export.cvs2.model;

import java.io.File;
import java.io.IOException;

import com.hungle.sunriise.mnyobject.Currency;

public class DefaultCurrencyWrapper extends AbstractExportToCsv2Wrapper<Currency, Csv2Currency> {
    public DefaultCurrencyWrapper(File outFile, boolean appendMode) throws IOException {
        super(outFile, appendMode);
        getMappingStrategy().setType(Csv2Currency.class);
    }

    public DefaultCurrencyWrapper(File outFile) throws IOException {
        super(outFile);
        getMappingStrategy().setType(Csv2Currency.class);
    }

    @Override
    protected Csv2Currency wrap(Currency value) {
        Csv2Currency wrappedType = new Csv2Currency(value);
        return wrappedType;
    }

    @Override
    protected String[] getWrapperHeaders() {
        Csv2Currency wrappedType = new Csv2Currency(null);

        String[] headers = wrappedType.getHeaders();

        return headers;
    }
}
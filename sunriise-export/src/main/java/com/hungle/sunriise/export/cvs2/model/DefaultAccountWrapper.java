package com.hungle.sunriise.export.cvs2.model;

import java.io.File;
import java.io.IOException;
import java.io.Writer;

import com.hungle.sunriise.mnyobject.Account;

public class DefaultAccountWrapper extends AbstractExportToCsv2Wrapper<Account, Csv2Account> {
    public DefaultAccountWrapper(File outFile, boolean appendMode) throws IOException {
        super(outFile, appendMode);
        getMappingStrategy().setType(Csv2Account.class);
    }

    public DefaultAccountWrapper(File outFile) throws IOException {
        super(outFile);
        getMappingStrategy().setType(Csv2Account.class);
    }

    public DefaultAccountWrapper(Writer writer) throws IOException {
        super(writer);
        getMappingStrategy().setType(Csv2Account.class);
    }

    @Override
    protected Csv2Account wrap(Account value) {
        Csv2Account wrappedType = new Csv2Account(value);
        return wrappedType;
    }

    @Override
    protected String[] getWrapperHeaders() {
        Csv2Account wrappedType = new Csv2Account(null);

        String[] headers = wrappedType.getHeaders();

        return headers;
    }
}
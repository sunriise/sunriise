package com.hungle.sunriise.export.json;

import java.io.File;

import org.apache.logging.log4j.Logger;

import com.kjetland.jackson.jsonSchema.JsonSchemaGenerator;

// TODO: Auto-generated Javadoc
/**
 * The Class ExportJSONSchemaMbknor.
 */
public class ExportJSONSchemaMbknor extends AbstractExportJSONSchema {

    /** The Constant LOGGER. */
    private static final Logger LOGGER = org.apache.logging.log4j.LogManager.getLogger(ExportJSONSchemaMbknor.class);

    /** The json schema generator. */
    private final JsonSchemaGenerator jsonSchemaGenerator;

    /**
     * Instantiates a new export JSON schema mbknor.
     */
    public ExportJSONSchemaMbknor() {
        super();
        // configure mapper, if necessary, then create schema generator
        jsonSchemaGenerator = new JsonSchemaGenerator(mapper);
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.hungle.sunriise.export.AbstractExportJSONSchema#generateJsonSchema(java.
     * lang.Class)
     */
    @Override
    protected Object generateJsonSchema(Class clz) {
        return jsonSchemaGenerator.generateJsonSchema(clz);
    }

    /**
     * The main method.
     *
     * @param args the arguments
     */
    public static void main(String[] args) {
        AbstractExportJSONSchema export = new ExportJSONSchemaMbknor();
        String parentDirName = "target/jsonSchema/v4";
        if (args.length == 1) {
            parentDirName = args[0];
        }
        File parentDir = new File(parentDirName);

        // https://github.com/mbknor/mbknor-jackson-jsonSchema
        export.generateJsonSchemaToDir(parentDir);
    }

}

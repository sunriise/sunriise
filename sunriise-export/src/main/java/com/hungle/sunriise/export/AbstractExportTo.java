package com.hungle.sunriise.export;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.function.Supplier;

import org.apache.logging.log4j.Logger;

import com.healthmarketscience.jackcess.Database;
import com.hungle.sunriise.account.DefaultAccountVisitor;
import com.hungle.sunriise.io.MnyDb;
import com.hungle.sunriise.mnyobject.Account;

public abstract class AbstractExportTo extends DefaultAccountVisitor {
    private static final Logger LOGGER = org.apache.logging.log4j.LogManager.getLogger(AbstractExportTo.class);
    private ExecutorService threadPool;
    private List<CompletableFuture<Boolean>> futures;

    public AbstractExportTo() {
        super();
    }

    protected void shutdownThreadPool() {
        if (getThreadPool() != null) {
            List<Runnable> runnables = getThreadPool().shutdownNow();
            int size = runnables.size();
            if (size > 0) {
                LOGGER.warn("Threadpool is shutdown with the following number of active tasks=" + size);
            }
        }
    }

    protected void initThreadPool() {
        boolean enableThreadPool = false;
        if (!enableThreadPool) {
            LOGGER.info("ThreadPool is NOT enable.");
            return;
        }

        setThreadPool(Executors.newFixedThreadPool(4));
        if (getThreadPool() != null) {
            setFutures(new ArrayList<>());
        }
    }

    protected void clearFutures() {
        if (getThreadPool() != null) {
            if (getFutures() != null) {
                if (getFutures().size() > 0) {
                    LOGGER.warn("Futures list is non-empty. Clearing ...");
                }
                getFutures().clear();
            }
        }
    }

    protected void waitFutures(List<CompletableFuture<Boolean>> futures) throws IOException {
        if (getFutures() == null) {
            return;
        }

        Class<CompletableFuture> clz = CompletableFuture.class;
        CompletableFuture<Boolean>[] type = (CompletableFuture<Boolean>[]) Array.newInstance(clz, 0);
        CompletableFuture<Void> completedFuture = CompletableFuture.allOf(getFutures().toArray(type));
        try {
            completedFuture.join();
        } catch (Exception e) {
            throw new IOException(e);
        } finally {
            clearFutures();
        }
    }

    /**
     * To safe file name.
     *
     * @param accountName the account name
     * @return the string
     */
    protected String toSafeFileName(String accountName) {
        StringBuilder sb = new StringBuilder();

        int len = accountName.length();
        for (int i = 0; i < len; i++) {
            char c = accountName.charAt(i);
            sb.append(toSafeFileNameChar(c));
        }
        return sb.toString();
    }

    /**
     * To safe file name char.
     *
     * @param c the c
     * @return the char
     */
    private char toSafeFileNameChar(char c) {
        char safeC = c;

        if (c == ' ') {
            return '_';
        }
        if (c == '(') {
            return '_';
        }
        if (c == ')') {
            return '_';
        }
        if (c == '&') {
            return '_';
        }
        if (c == '\'') {
            return '_';
        }
        if (c == '’') {
            return '_';
        }
        if (c == '/') {
            return '_';
        }
        if (c == '\\') {
            return '_';
        }
        if (c == '>') {
            return '_';
        }
        if (c == '<') {
            return '_';
        }
        if (c == '|') {
            return '_';
        }
        if (c == '"') {
            return '_';
        }
        if (c == '*') {
            return '_';
        }
        if (c == '*') {
            return '_';
        }
        if (c == '{') {
            return '_';
        }
        if (c == '}') {
            return '_';
        }

        return safeC;
    }

    public List<CompletableFuture<Boolean>> getFutures() {
        return futures;
    }

    public void setFutures(List<CompletableFuture<Boolean>> futures) {
        this.futures = futures;
    }

    public ExecutorService getThreadPool() {
        return threadPool;
    }

    public void setThreadPool(ExecutorService threadPool) {
        this.threadPool = threadPool;
    }

    /**
     * Export.
     *
     * @param dbFile   the db file
     * @param password the password
     * @return the database
     * @throws IOException Signals that an I/O exception has occurred.
     */
    public Database export(File dbFile, String password) throws IOException {
        MnyDb mnyDb = null;
        try {
            initThreadPool();

            mnyDb = new MnyDb(dbFile, password);
            return export(mnyDb);
        } finally {
            if (mnyDb != null) {
                mnyDb.close();
                mnyDb = null;
            }
            shutdownThreadPool();
        }
    }

    /**
     * Export.
     *
     * @param mnyDb the mny db
     * @return the database
     * @throws IOException Signals that an I/O exception has occurred.
     */
    protected abstract Database export(MnyDb mnyDb) throws IOException;

    /**
     * Export account names.
     *
     * @throws IOException Signals that an I/O exception has occurred.
     */
    protected abstract void exportAccountNames() throws IOException;

    /**
     * Export securities.
     *
     * @throws IOException Signals that an I/O exception has occurred.
     */
    protected abstract void exportSecurities() throws IOException;

    /**
     * Export currencies.
     *
     * @throws IOException Signals that an I/O exception has occurred.
     */
    protected abstract void exportCurrencies() throws IOException;

    /**
     * Export payees.
     *
     * @throws IOException Signals that an I/O exception has occurred.
     */
    protected abstract void exportPayees() throws IOException;

    /**
     * Export categories.
     *
     * @throws IOException Signals that an I/O exception has occurred.
     */
    protected abstract void exportCategories() throws IOException;

    @Override
    protected void visitPreAccountIteration() throws IOException {
        // clear the future list
        clearFutures();
    }

    @Override
    protected void visitPostAccountIteration() throws IOException {
        if (getThreadPool() != null) {
            waitFutures(getFutures());
        }
    }

    @Override
    public void visitAccount(Account account) throws IOException {
        super.visitAccount(account);

        String accountName = account.getName();
        LOGGER.info("> " + getAccountsIndex() + "/" + getAccountsCount() + " accountName=" + accountName);

        if (getThreadPool() != null) {
            Supplier<Boolean> supplier = new Supplier<Boolean>() {

                @Override
                public Boolean get() {
                    try {
                        LOGGER.info("> BEGIN exportAccount, activeTasks="
                                + ((ThreadPoolExecutor) getThreadPool()).getActiveCount());
                        exportAccount(account);
                    } catch (IOException e) {
                        throw new CompletionException(e);
                    } finally {
                        LOGGER.info("< END exportAccount ...");

                    }
                    return true;
                }
            };
            CompletableFuture<Boolean> future = CompletableFuture.supplyAsync(supplier, getThreadPool());
            getFutures().add(future);
        } else {
            exportAccount(account);
        }
    }

    @Override
    public void visitAccounts(Map<Integer, Account> accounts) throws IOException {
        super.visitAccounts(accounts);

        exportMnyContext();
    }

    /**
     * Export mny context.
     *
     * @throws IOException Signals that an I/O exception has occurred.
     */
    protected void exportMnyContext() throws IOException {
        exportCategories();

        exportPayees();

        exportCurrencies();

        exportSecurities();

        exportAccountNames();
    }

    /**
     * Export account.
     *
     * @param account the account
     * @throws IOException Signals that an I/O exception has occurred.
     */
    protected void exportAccount(Account account) throws IOException {
        exportAccountDetail(account);

        exportAccountTransactions(account);

        exportAccountFilteredTransactions(account);
    }

    /**
     * Export account filtered transactions.
     *
     * @param account the account
     * @throws IOException Signals that an I/O exception has occurred.
     */
    protected abstract void exportAccountFilteredTransactions(Account account) throws IOException;

    /**
     * Export account transactions.
     *
     * @param account the account
     * @throws IOException Signals that an I/O exception has occurred.
     */
    protected abstract void exportAccountTransactions(Account account) throws IOException;

    /**
     * Export account detail.
     *
     * @param account the account
     * @throws IOException Signals that an I/O exception has occurred.
     */
    protected abstract void exportAccountDetail(Account account) throws IOException;

}
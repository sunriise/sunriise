/*******************************************************************************
 * Copyright (c) 2010 Hung Le
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *******************************************************************************/
package com.hungle.sunriise.export.mdb;

import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.logging.log4j.Logger;

import com.healthmarketscience.jackcess.Column;
import com.healthmarketscience.jackcess.ColumnBuilder;
import com.healthmarketscience.jackcess.Cursor;
import com.healthmarketscience.jackcess.CursorBuilder;
import com.healthmarketscience.jackcess.DataType;
import com.healthmarketscience.jackcess.Database;
import com.healthmarketscience.jackcess.Table;
import com.healthmarketscience.jackcess.TableBuilder;
import com.hungle.sunriise.io.FileUtils;
import com.hungle.sunriise.io.MnyDb;
import com.hungle.sunriise.prices.DbInfo;
import com.hungle.sunriise.util.StopWatch;

// TODO: Auto-generated Javadoc
/**
 * The Class ExportToMdb.
 */
public class ExportToMdb {

    /** The Constant log. */
    private static final Logger LOGGER = org.apache.logging.log4j.LogManager.getLogger(ExportToMdb.class);

    /**
     * Export.
     *
     * @param srcFile     the src file
     * @param srcPassword the src password
     * @param destFile    the dest file
     * @throws IOException Signals that an I/O exception has occurred.
     */
    public void export(File srcFile, String srcPassword, File destFile) throws IOException {
        MnyDb srcDb = null;
        Database destDb = null;

        try {
            srcDb = new MnyDb(srcFile, srcPassword);
            destDb = export(srcDb, destFile);
        } finally {
            if (srcDb != null) {
                try {
                    srcDb.close();
                } finally {
                    srcDb = null;
                }
            }
            if (destDb != null) {
                try {
                    destDb.close();
                } catch (IOException e) {
                    LOGGER.warn(e);
                } finally {
                    destDb = null;
                }
            }
        }
    }

    /**
     * Export.
     *
     * @param srcDb    the src db
     * @param destFile the dest file
     * @return the database
     * @throws IOException Signals that an I/O exception has occurred.
     */
    public Database export(MnyDb srcDb, File destFile) throws IOException {
        Database destDb = FileUtils.createEmptyDb(destFile);
        copyDb(srcDb.getDb(), destDb);
        return destDb;
    }

    /**
     * Copy db.
     *
     * @param srcDb  the src db
     * @param destDb the dest db
     * @throws IOException Signals that an I/O exception has occurred.
     */
    private void copyDb(Database srcDb, Database destDb) throws IOException {
        Set<String> tableNames = srcDb.getTableNames();
        int tableCount = tableNames.size();
        int addedCount = 0;
        startCopyTables(tableCount);
        try {
            for (String tableName : tableNames) {
                Table table = srcDb.getTable(tableName);
                if (!copyTable(table, destDb)) {
                    break;
                }
                addedCount++;
            }
        } finally {
            endCopyTables(addedCount);
        }
    }

    /**
     * Start copy tables.
     *
     * @param maxCount the max count
     */
    protected void startCopyTables(int maxCount) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("> Adding tables=" + maxCount);
        }
    }

    /**
     * End copy tables.
     *
     * @param count the count
     */
    protected void endCopyTables(int count) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("< Added tables=" + count);
        }
    }

    /**
     * Start copy table.
     *
     * @param name the name
     * @return true, if successful
     */
    protected boolean startCopyTable(String name) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("> startCopyTable, name=" + name);
        }
        return true;
    }

    /**
     * End copy table.
     *
     * @param name the name
     */
    protected void endCopyTable(String name) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("< endCopyTable, name=" + name);
        }
    }

    /**
     * Start adding rows.
     *
     * @param max the max
     * @return true, if successful
     */
    protected boolean startAddingRows(int max) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("Adding rows=" + max + " ...");
        }
        return true;
    }

    /**
     * End adding rows.
     *
     * @param count the count
     * @param delta the delta
     */
    protected void endAddingRows(int count, long delta) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("Added rows=" + count + ", ms=" + delta);
        }
    }

    /**
     * Added row.
     *
     * @param count the count
     * @return true, if successful
     */
    protected boolean addedRow(int count) {
        return true;
    }

    /**
     * Copy table.
     *
     * @param srcTable the src table
     * @param destDb   the dest db
     * @return true, if successful
     * @throws IOException Signals that an I/O exception has occurred.
     */
    private boolean copyTable(Table srcTable, Database destDb) throws IOException {
        if (!startCopyTable(srcTable.getName())) {
            return false;
        }

        try {
            String tableName = srcTable.getName();
            Table destTable = destDb.getTable(tableName);
            if (destTable != null) {
                LOGGER.warn("tableName=" + tableName + " exists.");
            } else {
                try {
                    List<? extends Column> columns = srcTable.getColumns();
                    Database db = destDb;
                    boolean useExistingTable = false;
                    importColumns(columns, db, tableName, useExistingTable);
                    destTable = destDb.getTable(tableName);
                } catch (SQLException e) {
                    throw new IOException(e);
                }

                int batchSize = 200;
                List<Object[]> rows = new ArrayList<Object[]>(batchSize);
                StopWatch stopWatch = new StopWatch();
                int rowCount = srcTable.getRowCount();
                if (!startAddingRows(rowCount)) {
                    return false;
                }
                Cursor cursor = null;
                int count = 0;
                try {
                    cursor = CursorBuilder.createCursor(srcTable);
                    while (cursor.moveToNextRow()) {
                        Map<String, Object> row = cursor.getCurrentRow();
                        rows.add(row.values().toArray());
                        if (rows.size() >= batchSize) {
                            destTable.addRows(rows);
                            count += rows.size();
                            rows.clear();
                            if (!addedRow(count)) {
                                break;
                            }
                        }
                    }
                    if (rows.size() > 0) {
                        destTable.addRows(rows);
                        count += rows.size();
                        addedRow(count);
                        rows.clear();
                    }
                } finally {
                    long delta = stopWatch.click();
                    if (cursor != null) {
                        cursor = null;
                    }
                    endAddingRows(count, delta);
                }
            }
        } finally {
            endCopyTable(srcTable.getName());
        }

        return true;
    }

    /**
     * Import columns.
     *
     * @param srcColumns       the src columns
     * @param db               the db
     * @param tableName        the table name
     * @param useExistingTable the use existing table
     * @throws SQLException the SQL exception
     * @throws IOException  Signals that an I/O exception has occurred.
     */
    private static void importColumns(List<? extends Column> srcColumns, Database db, String tableName,
            boolean useExistingTable) throws SQLException, IOException {
        tableName = TableBuilder.escapeIdentifier(tableName);
        @SuppressWarnings("unused")
        Table table = null;
        if (!useExistingTable || ((table = db.getTable(tableName)) == null)) {
            LinkedList<ColumnBuilder> columnsBuilder = new LinkedList<ColumnBuilder>();
            for (int i = 0; i < srcColumns.size(); i++) {
                Column srcColumn = srcColumns.get(i);
                ColumnBuilder columnBuilder = new ColumnBuilder(TableBuilder.escapeIdentifier(srcColumn.getName()));
                int lengthInUnits = srcColumn.getLengthInUnits();
                columnBuilder.setType(srcColumn.getType());
                DataType type = columnBuilder.getType();
                // we check for isTrueVariableLength here to avoid setting the
                // length
                // for a NUMERIC column, which pretends to be var-len, even
                // though it
                // isn't
                if (type.isTrueVariableLength() && !type.isLongValue()) {
                    columnBuilder.setLengthInUnits((short) lengthInUnits);
                }
                if (type.getHasScalePrecision()) {
                    int scale = srcColumn.getScale();
                    int precision = srcColumn.getPrecision();
                    if (type.isValidScale(scale)) {
                        columnBuilder.setScale((byte) scale);
                    }
                    if (type.isValidPrecision(precision)) {
                        columnBuilder.setPrecision((byte) precision);
                    }
                }
                columnsBuilder.add(columnBuilder);
            }
            table = new TableBuilder(tableName).addColumns(columnsBuilder).toTable(db);
        }
    }

    /**
     * The main method.
     *
     * @param args the arguments
     */
    public static void main(String[] args) {
        File dbFile = null;

        String password = null;

        File outFile = null;

        String arg = null;
        if (args.length == 2) {
            arg = args[0];
            DbInfo dbInfo = DbInfo.getDbFile(arg);
            dbFile = dbInfo.getDbFile();

            password = dbInfo.getPassword();

            arg = args[1];
            outFile = new File(arg);
        } else if (args.length == 3) {
            arg = args[0];
            DbInfo dbInfo = DbInfo.getDbFile(arg);
            dbFile = dbInfo.getDbFile();

            arg = args[1];
            password = arg;

            arg = args[2];
            outFile = new File(arg);
        } else {
            Class<ExportToMdb> clz = ExportToMdb.class;
            System.out.println("Usage: java " + clz.getName() + " in.mny [password] out.mdb");
            FileUtils.printArgs(args);
            System.exit(1);
        }

        ExportToMdb exporter = new ExportToMdb();
        try {
            LOGGER.info("dbFile=" + dbFile);
            exporter.export(dbFile, password, outFile);
        } catch (IOException e) {
            LOGGER.error(e, e);
        } finally {
            LOGGER.info("destFile=" + outFile.getAbsolutePath());
            LOGGER.info("< DONE");
        }
    }
}

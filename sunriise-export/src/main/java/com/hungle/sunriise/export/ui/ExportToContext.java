package com.hungle.sunriise.export.ui;

import java.awt.Component;
import java.util.concurrent.Executor;

import com.hungle.sunriise.io.MnyDb;

// TODO: Auto-generated Javadoc
/**
 * The Interface ExportToContext.
 */
public interface ExportToContext {

    /**
     * Gets the parent component.
     *
     * @return the parent component
     */
    Component getParentComponent();

    /**
     * Gets the src db.
     *
     * @return the src db
     */
    MnyDb getMnyDb();

    Executor getThreadPool();

}

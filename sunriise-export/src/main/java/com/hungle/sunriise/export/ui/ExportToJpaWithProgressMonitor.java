package com.hungle.sunriise.export.ui;

import java.io.File;
import java.io.IOException;
import java.util.Map;

import javax.swing.ProgressMonitor;
import javax.swing.SwingUtilities;

import com.hungle.sunriise.export.jpa.MnyJpaExporter;
import com.hungle.sunriise.mnyobject.Account;
import com.hungle.sunriise.mnyobject.Transaction;

public final class ExportToJpaWithProgressMonitor extends MnyJpaExporter implements ExportToDirExporter {
    private ProgressMonitor progressMonitor;

    private String accountName = "";
    private int count = 0;
    private int maxAccounts = 0;

    public ExportToJpaWithProgressMonitor(ProgressMonitor progressMonitor) {
        super();
        this.progressMonitor = progressMonitor;
    }

    @Override
    public void visitAccounts(Map<Integer, Account> accounts) throws IOException {
        if (progressMonitor.isCanceled()) {
            return;
        }
        maxAccounts = accounts.size();
        super.visitAccounts(accounts);
    }

    @Override
    public void visitAccount(Account account) throws IOException {
        if (progressMonitor.isCanceled()) {
            return;
        }
        accountName = account.getName();
        count++;
        SwingUtilities.invokeLater(new Runnable() {

            @Override
            public void run() {
                progressMonitor.setNote("Account: " + accountName);
                progressMonitor.setProgress((count * 100) / maxAccounts);
            }
        });
        super.visitAccount(account);
    }

    @Override
    public void visitTransaction(Transaction transaction) throws IOException {
        if (progressMonitor.isCanceled()) {
            return;
        }
        super.visitTransaction(transaction);
    }

    @Override
    public void setOutDir(File destDir) {
        // TODO Auto-generated method stub

    }

    public ProgressMonitor getProgressMonitor() {
        return progressMonitor;
    }

    public void setProgressMonitor(ProgressMonitor progressMonitor) {
        this.progressMonitor = progressMonitor;
    }
}
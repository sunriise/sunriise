package com.hungle.sunriise.export.qif;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import org.apache.logging.log4j.Logger;

import com.hungle.sunriise.dbutil.TableCategoryUtils;
import com.hungle.sunriise.export.ExportSerializer;
import com.hungle.sunriise.mnyobject.Account;
import com.hungle.sunriise.mnyobject.Category;
import com.hungle.sunriise.mnyobject.Currency;
import com.hungle.sunriise.mnyobject.EnumAccountType;
import com.hungle.sunriise.mnyobject.MnyContext;
import com.hungle.sunriise.mnyobject.Payee;
import com.hungle.sunriise.mnyobject.PayeeLink;
import com.hungle.sunriise.mnyobject.Security;
import com.hungle.sunriise.mnyobject.Transaction;
import com.hungle.sunriise.mnyobject.TransactionSplit;
import com.hungle.sunriise.mnyobject.XferInfo;

// TODO: Auto-generated Javadoc
/**
 * The Class ExportToQifSerializer.
 */
public class ExportToQifSerializer implements ExportSerializer {
    private static final Logger LOGGER = org.apache.logging.log4j.LogManager.getLogger(ExportToQifSerializer.class);

    /** The Constant FILENAME_CATEGORIES. */
    private static final String FILENAME_CATEGORIES = "categories.qif";
    /** The Constant FILENAME_PAYEES. */
    private static final String FILENAME_PAYEES = "payees.qif";
    /** The Constant FILENAME_CURRENCIES. */
    private static final String FILENAME_CURRENCIES = "currencies.qif";
    /** The Constant FILENAME_SECURITIES. */
    private static final String FILENAME_SECURITIES = "securities.qif";
    /** The Constant FILENAME_ACCOUNTS. */
    private static final String FILENAME_ACCOUNTS = "accounts.qif";
    /** The Constant FILENAME_ACCOUNT. */
    private static final String FILENAME_ACCOUNT = "account.qif";
    /** The Constant FILENAME_TRANSACTIONS. */
    private static final String FILENAME_TRANSACTIONS = "transactions.qif";
    /** The Constant FILENAME_FILTERED_TRANSACTIONS. */
    private static final String FILENAME_FILTERED_TRANSACTIONS = "filteredTransactions.qif";

    // !Account Account list or which account follows
    private static final String QIF_HEADER_ACCOUNT = "!Account";

    // !Type:Cat Category list
    private static final String QIF_HEADER_CATEGORY = "!Type:Cat";

    /** The mny context. */
    private final MnyContext mnyContext;

    /**
     * Instantiates a new export to qif serializer.
     *
     * @param mnyContext the mny context
     */
    public ExportToQifSerializer(MnyContext mnyContext) {
        this.mnyContext = mnyContext;
    }

    /**
     * Serialize categories.
     *
     * @param categories the categories
     * @param outDir     the out dir
     * @throws IOException Signals that an I/O exception has occurred.
     */
    @Override
    public void serializeCategories(Collection<Category> categories, File outDir) throws IOException {
        final File outFile = new File(outDir, ExportToQifSerializer.FILENAME_CATEGORIES);
        LOGGER.info("> serializeCategories, outFile={}", outFile);
        try (PrintWriter writer = new PrintWriter(outFile)) {
            writer.println(QIF_HEADER_CATEGORY);
            for (Category category : categories) {
                ExportToQifSerializer.writeCategoryBlock(category, writer);
            }
        }
    }

    /**
     * Write category block.
     *
     * @param category the category
     * @param writer   the writer
     */
    private static final void writeCategoryBlock(Category category, PrintWriter writer) {
        // NDiv Income
        // DDividend Income
        // T
        // R286
        // I

        /*--
        Items for a Category List
        
        Field   Indicator Explanation
        N   Category name:subcategory name
        D   Description
        T   Tax related if included, not tax related if omitted
        I   Income category
        E   Expense category (if category type is unspecified, quicken assumes expense type)
        B   Budget amount (only in a Budget Amounts QIF file)
        R   Tax schedule information
        ^   End of entry
         */

        String fullName = category.getFullName();
        if (fullName == null) {
            return;
        }
        try {
            writer.println("N" + fullName);
            if (category.getExpense()) {
                writer.println("E");
            } else {
                writer.println("I");
            }
        } finally {
            writer.println("^");
        }
    }

    /**
     * Serialize payees.
     *
     * @param payees the payees
     * @param outDir the out dir
     * @throws IOException Signals that an I/O exception has occurred.
     */
    @Override
    public void serializePayees(Collection<Payee> payees, File outDir) throws IOException {
        final File outFile = new File(outDir, ExportToQifSerializer.FILENAME_PAYEES);
        LOGGER.info("> serializePayees, outFile={}", outFile);
        try (PrintWriter writer = new PrintWriter(outFile)) {
        }

    }

    /**
     * Serialize currencies.
     *
     * @param currencies the currencies
     * @param outDir     the out dir
     * @throws IOException Signals that an I/O exception has occurred.
     */
    @Override
    public void serializeCurrencies(Collection<Currency> currencies, File outDir) throws IOException {
        final File outFile = new File(outDir, ExportToQifSerializer.FILENAME_CURRENCIES);
        LOGGER.info("> serializeCurrencies, outFile={}", outFile);
        try (PrintWriter writer = new PrintWriter(outFile)) {

        }

    }

    /**
     * Serialize securities.
     *
     * @param securities the securities
     * @param outDir     the out dir
     * @throws IOException Signals that an I/O exception has occurred.
     */
    @Override
    public void serializeSecurities(Collection<Security> securities, File outDir) throws IOException {
        final File outFile = new File(outDir, ExportToQifSerializer.FILENAME_SECURITIES);
        LOGGER.info("> serializeSecurities, outFile={}", outFile);
        try (PrintWriter writer = new PrintWriter(outFile)) {
        }

    }

    /**
     * Serialize accounts.
     *
     * @param accounts the accounts
     * @param outDir   the out dir
     * @throws IOException Signals that an I/O exception has occurred.
     */
    @Override
    public void serializeAccounts(Collection<Account> accounts, File outDir) throws IOException {
        final File outFile = new File(outDir, ExportToQifSerializer.FILENAME_ACCOUNTS);
        LOGGER.info("> serializeAccounts, outFile={}", outFile);
        try (PrintWriter writer = new PrintWriter(outFile)) {
            writer.println(QIF_HEADER_ACCOUNT);
            for (Account account : accounts) {
                ExportToQifSerializer.writeAccountBlock(account, writer);
            }
        }
    }

    /**
     * Write account block.
     *
     * @param account the account
     * @param writer  the writer
     */
    private static final void writeAccountBlock(Account account, PrintWriter writer) {
        /*--
         Items for Account Information
        
        The account header !Account is used in two places-at the start of an account list and the start of a list of transactions to specify to which account they belong.
        Field   Indicator Explanation
        N   Name
        T   Type of account
        D   Description
        L   Credit limit (only for credit card accounts)
        /   Statement balance date
        $   Statement balance amount
        ^   End of entry
         */
        // !Account
        // NBank 001 Account
        // TBank
        // ^
        try {
            writer.println("N" + getAccountName(account));
            writer.println("T" + getAccountType(account));
        } finally {
            writer.println("^");
        }

    }

    /**
     * Serialize transactions.
     *
     * @param transactions the transactions
     * @param outDir       the out dir
     * @throws IOException Signals that an I/O exception has occurred.
     */
    @Override
    public void serializeTransactions(Account account, List<Transaction> transactions, File outDir) throws IOException {
        final File outFile = new File(outDir, ExportToQifSerializer.FILENAME_TRANSACTIONS);
        LOGGER.info("> serializeTransactions, outFile={}", outFile);
        try (PrintWriter writer = new PrintWriter(outFile)) {
            if (transactions != null) {
                for (Transaction transaction : transactions) {
                    ExportToQifSerializer.writeTransactionBlock(mnyContext, account, transaction, writer);
                }
            }
        }
    }

    /**
     * Write transaction block.
     *
     * @param context     the context
     * @param account
     * @param transaction the transaction
     * @param writer      the writer
     */
    static final void writeTransactionBlock(MnyContext context, Account account, Transaction transaction,
            PrintWriter writer) {
        // D6/ 1/94 Date
        // T-1,000.00 Amount
        // N1005 Check number
        // PBank Of Mortgage Payee
        // L[linda] Category
        // http://linuxfinances.info/info/financeformats.html
        try {
            if (account.getAccountType() == EnumAccountType.INVESTMENT) {
                writeInvestmentTransactionBlock(context, transaction, writer);
            } else {
                writeNonInvestmentTransactionBlock(context, transaction, writer);
            }
        } finally {
            writer.println("^");
        }
    }

    private static void writeInvestmentTransactionBlock(MnyContext context, Transaction transaction,
            PrintWriter writer) {
        /*--
        Items for Investment Accounts
        
        Field   Indicator Explanation
        D   Date
        N   Action
        Y   Security
        I   Price
        Q   Quantity (number of shares or split ratio)
        T   Transaction amount
        C   Cleared status
        P   Text in the first line for transfers and reminders
        M   Memo
        O   Commission
        L   Account for the transfer
        $   Amount transferred
        ^   End of the entry        
         */
    }

    private static void writeNonInvestmentTransactionBlock(MnyContext context, Transaction transaction,
            PrintWriter writer) {
        /*--
        Items for Non-Investment Accounts
        
        Each item in a bank, cash, credit card, other liability, or other asset account must begin with a letter that indicates the field in the Quicken register. The non-split items can be in any sequence:
        Field   Indicator Explanation
        D   Date
        T   Amount
        C   Cleared status
        N   Num (check or reference number)
        P   Payee
        M   Memo
        A   Address (up to five lines; the sixth line is an optional message)
        L   Category (Category/Subcategory/Transfer/Class)
        S   Category in split (Category/Transfer/Class)
        E   Memo in split
        $   Dollar amount of split
        ^   End of the entry
        Note: Repeat the S, E, and $ lines as many times as needed for additional items in a split. If an item is omitted from the transaction in the QIF file, Quicken treats it as a blank item.
        
         */
        writer.println("D" + getTransactionDate(transaction));
        writer.println("T" + getTransactionAmount(transaction));
        writer.println("N" + getTransactionCheckNumber(transaction));
        writer.println("P" + getTransactionPayee(transaction));
        writer.println("L" + getTransactionCategory(context, transaction));

        if (transaction.hasSplits()) {
            // SBudget:Food
            // $50.00
            // SAssets:Budgeted Cash
            // $-50.00
            // SIncome
            // $100.00
            List<TransactionSplit> splits = transaction.getSplits();
            for (TransactionSplit split : splits) {
                Transaction txn = split.getTransaction();

                writer.println("S" + getTransactionCategory(context, txn));
                writer.println("$" + getTransactionAmount(txn));
            }
        }
    }

    /**
     * Serialize filtered transactions.
     *
     * @param transactions the transactions
     * @param outDir       the out dir
     * @throws IOException Signals that an I/O exception has occurred.
     */
    @Override
    public void serializeFilteredTransactions(Account account, List<Transaction> transactions, File outDir)
            throws IOException {
        final File outFile = new File(outDir, ExportToQifSerializer.FILENAME_FILTERED_TRANSACTIONS);
        LOGGER.info("> serializeFilteredTransactions, outFile={}", outFile);
        try (PrintWriter writer = new PrintWriter(outFile)) {
            if (transactions != null) {
                for (Transaction transaction : transactions) {
                    ExportToQifSerializer.writeTransactionBlock(mnyContext, account, transaction, writer);
                }
            }
        }

    }

    /**
     * Serialize all transactions.
     *
     * @param accounts   the accounts
     * @param outDir     the out dir
     * @param appendMode the append mode
     * @throws IOException Signals that an I/O exception has occurred.
     */
    @Override
    public void serializeAllTransactions(Collection<Account> accounts, File outDir, boolean appendMode)
            throws IOException {
        final File outFile = new File(outDir, ExportToQifSerializer.FILENAME_TRANSACTIONS);
        LOGGER.info("> serializeAllTransactions, outFile={}", outFile);

        final File outFile2 = new File(outDir, ExportToQifSerializer.FILENAME_FILTERED_TRANSACTIONS);
        LOGGER.info("> serializeAllTransactions, outFile2={}", outFile2);

        try (PrintWriter writer = new PrintWriter(outFile)) {
            try (PrintWriter writer2 = new PrintWriter(outFile2)) {
                for (Account account : accounts) {
                    writer.println(QIF_HEADER_ACCOUNT);
                    ExportToQifSerializer.writeAccountBlock(account, writer);

                    List<Transaction> transactions = account.getTransactions();
                    if (transactions != null) {
                        for (Transaction transaction : transactions) {
                            ExportToQifSerializer.writeTransactionBlock(mnyContext, account, transaction, writer);
                        }
                    }

                    transactions = account.getFilteredTransactions();
                    if (transactions != null) {
                        for (Transaction transaction : transactions) {
                            ExportToQifSerializer.writeTransactionBlock(mnyContext, account, transaction, writer2);
                        }
                    }
                }
            }
        }
    }

    /**
     * Gets the account type.
     *
     * @param account the account
     * @return the account type
     */
    private static final String getAccountType(Account account) {
        String type = "Bank";

        EnumAccountType accountType = account.getAccountType();
        switch (accountType) {
        case ASSET:
            type = "Oth A";
            break;
        case BANKING:
            type = "Bank";
            break;
        case CASH:
            type = "Cash";
            break;
        case CREDIT_CARD:
            type = "CCard";
            break;
        case INVESTMENT:
            type = "Invst";
            break;
        default:
            break;
        }

        return type;
    }

    /**
     * Gets the account name.
     *
     * @param account the account
     * @return the account name
     */
    private static final String getAccountName(Account account) {
        return account.getName();
    }

    /**
     * Gets the transaction date.
     *
     * @param transaction the transaction
     * @return the transaction date
     */
    private static final String getTransactionDate(Transaction transaction) {
        Date date = transaction.getDate();
        // D12/17'19
        // D12/17/19
        LocalDate localDate = date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
        int month = localDate.getMonthValue();
        int dayOfMonth = localDate.getDayOfMonth();
        int year = localDate.getYear();
        int year2 = (year % 1000) % 100;

        String str = null;

        if (year < 2000) {
            str = String.format("%02d/%02d/%02d", month, dayOfMonth, year2);
        } else {
            str = String.format("%02d/%02d/'%02d", month, dayOfMonth, year2);
        }

        return str;
    }

    /**
     * Gets the transaction amount.
     *
     * @param transaction the transaction
     * @return the transaction amount
     */
    private static final String getTransactionAmount(Transaction transaction) {
        BigDecimal amount = transaction.getAmount();

        return String.format("%.2f", amount);
    }

    /**
     * Gets the transaction check number.
     *
     * @param transaction the transaction
     * @return the transaction check number
     */
    private static final String getTransactionCheckNumber(Transaction transaction) {
        String number = transaction.getNumber();
        if (number == null) {
            number = "";
        }
        return number;
    }

    /**
     * Gets the transaction payee.
     *
     * @param transaction the transaction
     * @return the transaction payee
     */
    private static final String getTransactionPayee(Transaction transaction) {
        String str = "";
        PayeeLink payee = transaction.getPayee();
        if (payee != null) {
            str = payee.getName();
            if (str == null) {
                str = "";
            }
        }
        return str;
    }

    /**
     * Gets the transaction category.
     *
     * @param context     the context
     * @param transaction the transaction
     * @return the transaction category
     */
    private static final String getTransactionCategory(MnyContext context, Transaction transaction) {
        String value = null;

        XferInfo xferInfo = transaction.getXferInfo();
        Integer xferAccountId = xferInfo.getXferAccountId();
//        MnyContext context = mnyContext;

        if (xferAccountId != null) {
            value = TableCategoryUtils.getCategoryFullNameForXfer(xferAccountId, transaction, context, false);
            if (value != null) {
                value = "[" + value + "]";
            }
        } else {
            Integer categoryId = transaction.getCategory().getId();
            String categoryName = TableCategoryUtils.getCategoryFullName(categoryId, context);
            value = categoryName;
        }

        if (value == null) {
            if (transaction.hasSplits()) {
                // SBudget:Food
                // $50.00
                // SAssets:Budgeted Cash
                // $-50.00
                // SIncome
                // $100.00
                List<TransactionSplit> splits = transaction.getSplits();
                value = "(" + splits.size() + ") Split Transaction";
            }
        }

        if (transaction.isInvestment()) {
            value = TableCategoryUtils.getCategoryFullNameForInvestment(transaction, context);
        }

        return value;
    }
}

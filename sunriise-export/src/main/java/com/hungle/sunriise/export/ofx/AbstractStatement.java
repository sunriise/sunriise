package com.hungle.sunriise.export.ofx;

import java.util.Date;
import java.util.List;

public class AbstractStatement<T> {
    private static final String DEFAULT_CURRENCY = "USD";

    private String currency = DEFAULT_CURRENCY;

    private String accountId = null;

    private Date dtStartDate = null;
    private Date dtEndDate = null;

    protected Double ledgerBalanceBalanceAmount = null;
    private Date ledgerBalanceDateAsOf = null;

    private List<T> transactions = null;

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getAccountId() {
        return accountId;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    public Date getDtStartDate() {
        return dtStartDate;
    }

    public void setDtStartDate(Date dtStartDate) {
        this.dtStartDate = dtStartDate;
    }

    public Date getDtEndDate() {
        return dtEndDate;
    }

    public void setDtEndDate(Date dtEndDate) {
        this.dtEndDate = dtEndDate;
    }

    public Double getLedgerBalanceBalanceAmount() {
        return ledgerBalanceBalanceAmount;
    }

    public void setLedgerBalanceBalanceAmount(Double ledgerBalanceBalanceAmount) {
        this.ledgerBalanceBalanceAmount = ledgerBalanceBalanceAmount;
    }

    public Date getLedgerBalanceDateAsOf() {
        return ledgerBalanceDateAsOf;
    }

    public void setLedgerBalanceDateAsOf(Date ledgerBalanceDateAsOf) {
        this.ledgerBalanceDateAsOf = ledgerBalanceDateAsOf;
    }

    public List<T> getTransactions() {
        return transactions;
    }

    public void setTransactions(List<T> transactions) {
        this.transactions = transactions;
    }

}

package com.hungle.sunriise.export.cvs2.model;

import java.io.File;
import java.io.IOException;
import java.io.Writer;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.hungle.sunriise.export.cvs2.AbstractCsv2Wrapper;
import com.hungle.sunriise.mnyobject.Transaction;
import com.hungle.sunriise.mnyobject.TransactionSplit;
import com.opencsv.exceptions.CsvDataTypeMismatchException;
import com.opencsv.exceptions.CsvRequiredFieldEmptyException;

public class DefaultTransactionWrapper extends AbstractExportToCsv2Wrapper<Transaction, Csv2Transaction> {
    private static final Logger LOGGER = LogManager.getLogger(DefaultTransactionWrapper.class);

    public DefaultTransactionWrapper(File outFile, boolean appendMode) throws IOException {
        super(outFile, appendMode);
        getMappingStrategy().setType(Csv2Transaction.class);
    }

    public DefaultTransactionWrapper(File outFile) throws IOException {
        super(outFile);
        getMappingStrategy().setType(Csv2Transaction.class);
    }

    public DefaultTransactionWrapper(Writer writer) throws IOException {
        super(writer);
        getMappingStrategy().setType(Csv2Transaction.class);
    }

    @Override
    protected void writeBean(Csv2Transaction wrappedType)
            throws CsvDataTypeMismatchException, CsvRequiredFieldEmptyException {
        super.writeBean(wrappedType);

        // write splits
        writeSplits(wrappedType);
    }

    private void writeSplits(Csv2Transaction parentTxn)
            throws CsvDataTypeMismatchException, CsvRequiredFieldEmptyException {
        List<TransactionSplit> splits = parentTxn.listOfSplits();
        if (splits != null) {
            for (TransactionSplit split : splits) {
                Transaction transaction = split.getTransaction();
                Csv2Transaction bean = new Csv2Transaction(transaction);
                bean.setSplitParentId(parentTxn.getId());

                getSbc().write(bean);
            }
        }
    }

    @Override
    protected Csv2Transaction wrap(Transaction value) {
        Csv2Transaction wrappedType = new Csv2Transaction(value);
        return wrappedType;
    }

    @Override
    protected String[] getWrapperHeaders() {
        AbstractCsv2Wrapper<Transaction> wrappedType = new Csv2Transaction(null);

        String[] headers = wrappedType.getHeaders();

        return headers;
    }

}
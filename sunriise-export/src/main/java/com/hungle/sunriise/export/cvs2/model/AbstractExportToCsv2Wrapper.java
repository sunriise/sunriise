package com.hungle.sunriise.export.cvs2.model;

import java.io.BufferedWriter;
import java.io.Closeable;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.Collection;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.opencsv.CSVWriter;
import com.opencsv.bean.ColumnPositionMappingStrategy;
import com.opencsv.bean.MappingStrategy;
import com.opencsv.bean.StatefulBeanToCsv;
import com.opencsv.bean.StatefulBeanToCsvBuilder;
import com.opencsv.exceptions.CsvDataTypeMismatchException;
import com.opencsv.exceptions.CsvRequiredFieldEmptyException;

public abstract class AbstractExportToCsv2Wrapper<T, WT> implements Closeable {
    private static final Logger LOGGER = LogManager.getLogger(AbstractExportToCsv2Wrapper.class);

    private final StatefulBeanToCsv<WT> sbc;

    private Writer writer;

    private final MappingStrategy<WT> mappingStrategy;

    public AbstractExportToCsv2Wrapper(File outFile, boolean appendMode) throws IOException {
        this(new BufferedWriter(new FileWriter(outFile, appendMode)));
    }

    public AbstractExportToCsv2Wrapper(Writer writer) throws IOException {
        super();
        this.writer = writer;
        mappingStrategy = new ColumnPositionMappingStrategy<WT>() {
            @Override
            public String[] generateHeader(WT bean) throws CsvRequiredFieldEmptyException {
                String[] headers = super.generateHeader(bean);
                if ((headers == null) || (headers.length <= 0)) {
                    headers = getWrapperHeaders();
                }
                if (LOGGER.isDebugEnabled()) {
                    LOGGER.debug("XXX headers.length={}", headers.length);
                }
                return headers;
            }
        };
        this.sbc = new StatefulBeanToCsvBuilder<WT>(writer).withSeparator(CSVWriter.DEFAULT_SEPARATOR)
                .withMappingStrategy(mappingStrategy).build();
    }

    protected abstract String[] getWrapperHeaders();

    public AbstractExportToCsv2Wrapper(File outFile) throws IOException {
        this(outFile, false);
    }

    public int writeBeans(Collection<T> values) throws IOException {
        int errors = 0;
        for (T value : values) {
            try {
                WT wrappedType = wrap(value);
                writeBean(wrappedType);
            } catch (CsvDataTypeMismatchException e) {
                errors++;
                LOGGER.warn(e);
            } catch (CsvRequiredFieldEmptyException e) {
                errors++;
                LOGGER.warn(e);
            }
        }

        return errors;
    }

    protected void writeBean(WT wrappedType) throws CsvDataTypeMismatchException, CsvRequiredFieldEmptyException {
        sbc.write(wrappedType);
    }

    @Override
    public void close() throws IOException {
        if (writer != null) {
            writer.close();
        }
        writer = null;

    }

    protected abstract WT wrap(T value);

    public MappingStrategy<WT> getMappingStrategy() {
        return mappingStrategy;
    }

    public StatefulBeanToCsv<WT> getSbc() {
        return sbc;
    }
}
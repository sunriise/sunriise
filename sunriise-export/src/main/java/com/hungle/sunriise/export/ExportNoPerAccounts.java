package com.hungle.sunriise.export;

import java.io.File;
import java.io.IOException;
import java.util.Collection;
import java.util.List;

import com.hungle.sunriise.io.MnyDb;
import com.hungle.sunriise.mnyobject.Account;
import com.hungle.sunriise.mnyobject.Category;
import com.hungle.sunriise.mnyobject.Currency;
import com.hungle.sunriise.mnyobject.Payee;
import com.hungle.sunriise.mnyobject.Security;
import com.hungle.sunriise.mnyobject.Transaction;

public abstract class ExportNoPerAccounts extends AbstractExportToDir {

    private ExportSerializer serializer;

    public ExportNoPerAccounts() {
        super();
    }

    protected ExportSerializer getSerializer() {
        return serializer;
    }

    protected void setSerializer(ExportSerializer serializer) {
        this.serializer = serializer;
    }

    protected abstract void initSerializer();

    @Override
    protected void visitPostAccountIteration() throws IOException {
        super.visitPostAccountIteration();

        File outDir = getOutDir();
        boolean appendMode = true;
        Collection<Account> accounts = getAccounts();

        getSerializer().serializeAllTransactions(accounts, outDir, appendMode);
    }

    @Override
    protected File createAccountDir(Account account) {
        // don't create per account directory.
        return null;
    }

    @Override
    protected void exportAccountNamesToDir(File outDir) throws IOException {
        Collection<Account> accounts = this.getMnyContext().getAccounts().values();
        if (accounts == null) {
            return;
        }

        getSerializer().serializeAccounts(accounts, outDir);
    }

    @Override
    protected void exportSecuritiesToDir(File outDir) throws IOException {
        Collection<Security> securities = this.getMnyContext().getSecurities().values();
        if (securities == null) {
            return;
        }
        getSerializer().serializeSecurities(securities, outDir);
    }

    @Override
    protected void exportCurrenciesToDir(File outDir) throws IOException {
        Collection<Currency> currencies = this.getMnyContext().getCurrencies().values();
        if (currencies == null) {
            return;
        }
        getSerializer().serializeCurrencies(currencies, outDir);
    }

    @Override
    protected void exportPayeesToDir(File outDir) throws IOException {
        Collection<Payee> payees = this.getMnyContext().getPayees().values();
        if (payees == null) {
            return;
        }
        getSerializer().serializePayees(payees, outDir);
    }

    @Override
    protected void exportCategoriesToDir(File outDir) throws IOException {
        Collection<Category> categories = this.getMnyContext().getCategories().values();
        if (categories == null) {
            return;
        }
        getSerializer().serializeCategories(categories, outDir);
    }

    @Override
    protected void exportAccountDetailToDir(Account account, File outDir) throws IOException {
        // File outFile = new File(outDir,
        // ExportToCsv2Serializer.FILENAME_ACCOUNT);
        // account
    }

    @Override
    protected void exportFilteredTransactionsToDir(Account account, File outDir) throws IOException {
        List<Transaction> transactions = account.getFilteredTransactions();
        if (transactions == null) {
            return;
        }
        getSerializer().serializeFilteredTransactions(account, transactions, outDir);
    }

    @Override
    protected void exportTransactionsToDir(Account account, File outDir) throws IOException {
        List<Transaction> transactions = account.getTransactions();
        if (transactions == null) {
            return;
        }
        getSerializer().serializeTransactions(account, transactions, outDir);
    }

    @Override
    public void visit(MnyDb mnyDb) throws IOException {
        super.visit(mnyDb);

        initSerializer();
    }

}
/*******************************************************************************
 * Copyright (c) 2010 Hung Le
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *******************************************************************************/
package com.hungle.sunriise.export.ui;

import java.awt.Component;
import java.io.File;

import javax.swing.ProgressMonitor;
import javax.swing.SwingUtilities;

import org.apache.logging.log4j.Logger;

import com.hungle.sunriise.export.csv.ExportToCsv;

// TODO: Auto-generated Javadoc
/**
 * The Class ExportToCsvAction.
 */
public class ExportToCsvAction extends AbstractExportToAction {

    private static final String EXPORT_TYPE_CSV = "*.csv";
    /** The Constant log. */
    private static final Logger LOGGER = org.apache.logging.log4j.LogManager.getLogger(ExportToCsvAction.class);

    private final class ExportToCsvWithProgressMonitor extends ExportToCsv {
        private final ProgressMonitor progressMonitor;

        private int maxTables = 0;

        public ExportToCsvWithProgressMonitor(ProgressMonitor progressMonitor) {
            super();
            this.progressMonitor = progressMonitor;
        }

        @Override
        protected void startExport(File outDir) {
            if (progressMonitor.isCanceled()) {
                return;
            }
            SwingUtilities.invokeLater(new Runnable() {

                @Override
                public void run() {
                    progressMonitor.setProgress(progressMonitor.getMinimum());
                }
            });
            super.startExport(outDir);
        }

        @Override
        protected void endExport(File outDir) {
            if (progressMonitor.isCanceled()) {
                return;
            }
            SwingUtilities.invokeLater(new Runnable() {

                @Override
                public void run() {
                    progressMonitor.setProgress(progressMonitor.getMaximum());
                }
            });
            super.endExport(outDir);
        }

        @Override
        protected void startExportTables(int size) {
            if (progressMonitor.isCanceled()) {
                return;
            }
            maxTables = size;
            super.startExportTables(size);
        }

        @Override
        protected boolean exportedTable(final String tableName, final int count) {
            if (progressMonitor.isCanceled()) {
                return false;
            }
            SwingUtilities.invokeLater(new Runnable() {

                @Override
                public void run() {
                    progressMonitor.setNote("Table: " + tableName);
                    progressMonitor.setProgress((count * 100) / maxTables);
                }
            });
            return super.exportedTable(tableName, count);
        }

        @Override
        protected void endExportTables(int count) {
            if (progressMonitor.isCanceled()) {
                return;
            }
            super.endExportTables(count);
        }
    }

    /**
     * The Class ExportToCsvTask.
     */
    private final class ExportToCsvTask implements Runnable {

        /** The progress monitor. */
        private final ProgressMonitor progressMonitor;

        /** The dir. */
        private final File destDir;

        /** The source. */
        private final Component source;

        /**
         * Instantiates a new export to csv task.
         * 
         * @param progressMonitor the progress monitor
         * @param destDir         the dir
         * @param source          the source
         */
        private ExportToCsvTask(ProgressMonitor progressMonitor, File destDir, Component source) {
            this.progressMonitor = progressMonitor;
            this.destDir = destDir;
            this.source = source;
        }

        /*
         * (non-Javadoc)
         * 
         * @see java.lang.Runnable#run()
         */
        @Override
        public void run() {
            Exception exception = null;
            try {
                ExportToCsv exporter = new ExportToCsvWithProgressMonitor(progressMonitor);
                exporter.setMnyDb(getExportToContext().getMnyDb());
                exporter.export(destDir);
            } catch (Exception e) {
                LOGGER.error(e);
                exception = e;
            } finally {
                LOGGER.info("< DONE, exported to dir=" + destDir);
                ExportToDirTask.doFinally(exception, getExportType(), source);
            }
        }
    }

    public ExportToCsvAction(ExportToContext exportToContext) {
        super(exportToContext);
        setExportType(EXPORT_TYPE_CSV);
    }

    @Override
    protected Runnable createExportTask(ProgressMonitor progressMonitor, File destDir, Component source) {
        Runnable task = new ExportToCsvTask(progressMonitor, destDir, source);
        return task;
    }
}
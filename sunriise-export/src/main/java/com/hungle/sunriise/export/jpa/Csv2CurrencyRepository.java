package com.hungle.sunriise.export.jpa;

import org.springframework.data.jpa.repository.JpaRepository;

import com.hungle.sunriise.export.cvs2.model.Csv2Currency;

/**
 * The Interface Csv2CurrencyRepository.
 */
public interface Csv2CurrencyRepository extends JpaRepository<Csv2Currency, Integer> {
}

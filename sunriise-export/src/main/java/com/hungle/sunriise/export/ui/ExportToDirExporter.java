package com.hungle.sunriise.export.ui;

import java.io.File;
import java.io.IOException;

import com.healthmarketscience.jackcess.Database;

public interface ExportToDirExporter {

    Database export(File dbFile, String password) throws IOException;

    void setOutDir(File destDir);

}

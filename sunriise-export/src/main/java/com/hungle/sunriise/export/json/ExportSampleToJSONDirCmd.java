package com.hungle.sunriise.export.json;

import java.io.File;
import java.io.IOException;

import org.apache.logging.log4j.Logger;

import com.hungle.sunriise.export.AbstractExportToDir;
import com.hungle.sunriise.io.sample.MnySampleFile;
import com.hungle.sunriise.io.sample.MnySampleFileFactory;

// TODO: Auto-generated Javadoc
/**
 * The Class ExportToJSONDirCmd.
 */
public class ExportSampleToJSONDirCmd {

    /** The Constant LOGGER. */
    private static final Logger LOGGER = org.apache.logging.log4j.LogManager.getLogger(ExportSampleToJSONDirCmd.class);

    /**
     * The main method.
     *
     * @param args the arguments
     */
    public static void main(String[] args) {
        File dbFile = null;
        String password = null;
        File outDir = null;

        try {
            MnySampleFile sampleFile = MnySampleFileFactory.getSunsetSampleFile();
            dbFile = MnySampleFile.getSampleFileFromModuleProject(sampleFile);
            password = sampleFile.getPassword();
            outDir = new File("target/exportJson");
            outDir.mkdirs();

            export(dbFile, password, outDir);
        } catch (IOException e) {
            LOGGER.error(e.getMessage(), e);
        }
    }

    /**
     * Export.
     *
     * @param dbFile   the db file
     * @param password the password
     * @param outDir   the out dir
     */
    public static void export(File dbFile, String password, File outDir) {
        LOGGER.info("dbFile=" + dbFile);
        LOGGER.info("outDir=" + outDir);

        outDir.mkdirs();
        if (!outDir.isDirectory()) {
            LOGGER.error("Not a directory, dir=" + outDir);
            return;
        }

        try {
            AbstractExportToDir cmd = new ExportToJSON();
            cmd.setOutDir(outDir);

            cmd.export(dbFile, password);
        } catch (IOException e) {
            LOGGER.error(e, e);
        } finally {
            LOGGER.info("< DONE");
        }
    }

}

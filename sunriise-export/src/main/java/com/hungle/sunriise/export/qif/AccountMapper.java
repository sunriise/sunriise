package com.hungle.sunriise.export.qif;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Collection;
import java.util.List;

import org.apache.logging.log4j.Logger;

import com.hungle.sunriise.mnyobject.Account;
import com.hungle.sunriise.mnyobject.EnumAccountType;
import com.hungle.sunriise.mnyobject.MnyContext;
import com.hungle.sunriise.mnyobject.Transaction;

// TODO: Auto-generated Javadoc
/**
 * The Class AccountWrapper.
 */
public class AccountMapper {
    
    /** The Constant LOGGER. */
    private static final Logger LOGGER = org.apache.logging.log4j.LogManager.getLogger(AccountMapper.class);
    
    /** The Constant QIF_HEADER_ACCOUNT. */
    // !Account Account list or which account follows
    private static final String QIF_HEADER_ACCOUNT = "!Account";

    /** The mny context. */
    private final MnyContext mnyContext;
    
    /** The account. */
    private final Account account;
    /**
     * Instantiates a new account wrapper.
     *
     * @param mnyContext the mny context
     * @param account the account
     */
    public AccountMapper(MnyContext mnyContext, Account account) {
        this.mnyContext = mnyContext;
        this.account = account;
    }

    /**
     * Serialize all transactions.
     *
     * @param transactionsWriter the writer
     * @param filteredTransactionsWriter the writer 2
     */
    public void writeAllTransactions(PrintWriter transactionsWriter, PrintWriter filteredTransactionsWriter) {
        transactionsWriter.println(AccountMapper.QIF_HEADER_ACCOUNT);
        AccountMapper.writeAccountBlock(account, transactionsWriter);

        List<Transaction> transactions = null;

        transactions = account.getTransactions();
        if (transactions != null) {
            for (Transaction transaction : transactions) {
                TransactionMapper.writeTransactionBlock(mnyContext, account, transaction, transactionsWriter);
            }
        }

        transactions = account.getFilteredTransactions();
        if (transactions != null) {
            for (Transaction transaction : transactions) {
                TransactionMapper.writeTransactionBlock(mnyContext, account, transaction, filteredTransactionsWriter);
            }
        }
    }

    /**
     * Serialize.
     *
     * @param writer the writer
     */
    public void writeValue(PrintWriter writer) {
        AccountMapper.writeAccountBlock(account, writer);
    }

    /**
     * Gets the account block.
     *
     * @param account the account
     * @return the account block
     */
    public static final String getAccountBlock(Account account) {
        String str = "";
    
        try (StringWriter sw = new StringWriter()) {
            try (PrintWriter writer = new PrintWriter(sw)) {
                writeAccountBlock(account, writer);
                writer.flush();
                str = writer.toString();
            }
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
        }
    
        return str;
    }

    /**
     * Serialize accounts.
     *
     * @param mnyContext the mny context
     * @param accounts the accounts
     * @param writer the writer
     */
    static void serializeAccounts(MnyContext mnyContext, Collection<Account> accounts, PrintWriter writer) {
        writer.println(QIF_HEADER_ACCOUNT);
        for (Account account : accounts) {
            AccountMapper wrapper = new AccountMapper(mnyContext, account);
            wrapper.writeValue(writer);
        }
    }

    /**
     * Gets the account name.
     *
     * @param account the account
     * @return the account name
     */
    static final String getName(Account account) {
        return account.getName();
    }

    /**
     * Gets the account type.
     *
     * @param account the account
     * @return the account type
     */
    static final String getType(Account account) {
        String type = "Bank";
    
        EnumAccountType accountType = account.getAccountType();
        switch (accountType) {
        case ASSET:
            type = "Oth A";
            break;
        case BANKING:
            type = "Bank";
            break;
        case CASH:
            type = "Cash";
            break;
        case CREDIT_CARD:
            type = "CCard";
            break;
        case INVESTMENT:
            type = "Invst";
            break;
        default:
            break;
        }
    
        return type;
    }

    /**
     * Write account block.
     *
     * @param account the account
     * @param writer  the writer
     */
    public static final void writeAccountBlock(Account account, PrintWriter writer) {
        /*--
         Items for Account Information
        
        The account header !Account is used in two places-at the start of an account list and the start of a list of transactions to specify to which account they belong.
        Field   Indicator Explanation
        N   Name
        T   Type of account
        D   Description
        L   Credit limit (only for credit card accounts)
        /   Statement balance date
        $   Statement balance amount
        ^   End of entry
         */
        // !Account
        // NBank 001 Account
        // TBank
        // ^
        try {
            writer.println("N" + AccountMapper.getName(account));
            writer.println("T" + AccountMapper.getType(account));
        } finally {
            writer.println("^");
        }
    }

}

/*******************************************************************************
 * Copyright (c) 2010 Hung Le
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *******************************************************************************/
package com.hungle.sunriise.export.ui;

import java.awt.Component;
import java.io.File;
import java.io.IOException;
import java.util.Map;

import javax.swing.ProgressMonitor;
import javax.swing.SwingUtilities;

import org.apache.logging.log4j.Logger;

import com.hungle.sunriise.export.AbstractExportToDir;
import com.hungle.sunriise.export.json.ExportToJSON;
import com.hungle.sunriise.mnyobject.Account;
import com.hungle.sunriise.mnyobject.Transaction;
//import com.hungle.sunriise.viewer.MnyViewer;

// TODO: Auto-generated Javadoc
/**
 * The Class ExportToJSONAction.
 */
public class ExportToJSONAction extends AbstractExportToAction {

    private static final String EXPORT_TYPE_JSON = "*.json";
    /** The Constant log. */
    private static final Logger LOGGER = org.apache.logging.log4j.LogManager.getLogger(ExportToJSONAction.class);

    private final class ExportToJsonWithProgressMonitor extends ExportToJSON {
        private final ProgressMonitor progressMonitor;

        private String accountName = "";
        private int count = 0;
        private int maxAccounts = 0;

        public ExportToJsonWithProgressMonitor(ProgressMonitor progressMonitor) {
            super();
            this.progressMonitor = progressMonitor;
        }

        @Override
        public void visitAccounts(Map<Integer, Account> accounts) throws IOException {
            if (progressMonitor.isCanceled()) {
                return;
            }
            maxAccounts = accounts.size();
            super.visitAccounts(accounts);
        }

        @Override
        protected void startExportToDir(File outDir) {
            if (progressMonitor.isCanceled()) {
                return;
            }
            accountName = "";
            count = 0;
            maxAccounts = 0;
            SwingUtilities.invokeLater(new Runnable() {

                @Override
                public void run() {
                    progressMonitor.setProgress(progressMonitor.getMinimum());
                }
            });
            super.startExportToDir(outDir);
        }

        @Override
        public void visitAccount(Account account) throws IOException {
            if (progressMonitor.isCanceled()) {
                return;
            }
            accountName = account.getName();
            count++;
            SwingUtilities.invokeLater(new Runnable() {

                @Override
                public void run() {
                    progressMonitor.setNote("Account: " + accountName);
                    progressMonitor.setProgress((count * 100) / maxAccounts);
                }
            });
            super.visitAccount(account);
        }

        @Override
        public void visitTransaction(Transaction transaction) throws IOException {
            if (progressMonitor.isCanceled()) {
                return;
            }
            super.visitTransaction(transaction);
        }

        @Override
        protected void endExportToDir(File outDir) {
            if (progressMonitor.isCanceled()) {
                return;
            }
            SwingUtilities.invokeLater(new Runnable() {

                @Override
                public void run() {
                    progressMonitor.setProgress(progressMonitor.getMaximum());
                }
            });
            super.endExportToDir(outDir);
        }
    }

    /**
     * Instantiates a new export to json action.
     *
     * @param exportToContext the export to context
     */
    public ExportToJSONAction(ExportToContext exportToContext) {
        super(exportToContext);
        setExportType(EXPORT_TYPE_JSON);
    }

    @Override
    protected Runnable createExportTask(ProgressMonitor progressMonitor, File destDir, Component source) {
        AbstractExportToDir exporter = new ExportToJsonWithProgressMonitor(progressMonitor);
        Runnable task = new ExportToDirTask(this, exporter, destDir, source);
        return task;
    }
}
package com.hungle.sunriise.export;

import java.io.File;
import java.io.IOException;
import java.util.Collection;
import java.util.List;

import com.hungle.sunriise.mnyobject.Account;
import com.hungle.sunriise.mnyobject.Category;
import com.hungle.sunriise.mnyobject.Currency;
import com.hungle.sunriise.mnyobject.Payee;
import com.hungle.sunriise.mnyobject.Security;
import com.hungle.sunriise.mnyobject.Transaction;

public interface ExportSerializer {

    void serializeAllTransactions(Collection<Account> accounts, File outDir, boolean appendMode) throws IOException;

    void serializeAccounts(Collection<Account> accounts, File outDir) throws IOException;

    void serializeCategories(Collection<Category> categories, File outDir) throws IOException;

    void serializeCurrencies(Collection<Currency> currencies, File outDir) throws IOException;

    void serializeFilteredTransactions(Account account, List<Transaction> transactions, File outDir) throws IOException;

    void serializePayees(Collection<Payee> payees, File outDir) throws IOException;

    void serializeSecurities(Collection<Security> securities, File outDir) throws IOException;

    void serializeTransactions(Account account, List<Transaction> transactions, File outDir) throws IOException;

}

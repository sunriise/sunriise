package com.hungle.sunriise.export.mongodb;

import java.io.File;
import java.io.IOException;

import org.apache.logging.log4j.Logger;

import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;

// TODO: Auto-generated Javadoc
/**
 * The Class ExportToMongoDbCmd.
 */
public class ExportToMongoDbCmd {

    /** The Constant LOGGER. */
    private static final Logger LOGGER = org.apache.logging.log4j.LogManager.getLogger(ExportToMongoDbCmd.class);

    /**
     * The main method.
     *
     * @param args the arguments
     */
    public static void main(String[] args) {
        File dbFile = null;
        String password = null;
        MongoClientURI mongoClientURI = null;

        if (args.length == 1) {
            dbFile = new File(args[0]);
            mongoClientURI = ExportToMongoDb.createMongoClientURI(ExportToMongoDb.DEFAULT_MONGO_CLIENT_URI);
        } else if (args.length == 2) {
            dbFile = new File(args[0]);
            mongoClientURI = ExportToMongoDb.createMongoClientURI(args[1]);
        } else if (args.length == 3) {
            dbFile = new File(args[0]);
            password = args[1];
            mongoClientURI = ExportToMongoDb.createMongoClientURI(args[2]);
        } else {
            Class<ExportToMongoDbCmd> clz = ExportToMongoDbCmd.class;
            System.out.println("Usage: java " + clz.getName() + " in.mny [password] mongoClientURI");
            System.exit(1);
        }

        export(dbFile, password, mongoClientURI);
    }

    /**
     * Export.
     *
     * @param dbFile         the db file
     * @param password       the password
     * @param mongoClientURI the mongo client URI
     */
    public static void export(File dbFile, String password, MongoClientURI mongoClientURI) {
        LOGGER.info("dbFile=" + dbFile);
        LOGGER.info("mongoClientURI=" + mongoClientURI);

        MongoClient mongoClient = null;
        try {
            ExportToMongoDb exporter = new ExportToMongoDb();

            mongoClient = ExportToMongoDb.createMongoClient(mongoClientURI, exporter);

            exporter.export(dbFile, password);
        } catch (IOException e) {
            LOGGER.error(e, e);
        } finally {
            if (mongoClient != null) {
                mongoClient.close();
                mongoClient = null;
            }
            LOGGER.info("< DONE");
        }
    }

    /** The Constant MONGODB_PREFIX. */
    static final String MONGODB_PREFIX = "mongodb:";

}

package com.hungle.sunriise.export.ui;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

import javax.swing.JFileChooser;
import javax.swing.ProgressMonitor;

import org.apache.logging.log4j.Logger;

public abstract class AbstractExportToAction implements ActionListener {
    private static final Logger LOGGER = org.apache.logging.log4j.LogManager.getLogger(AbstractExportToAction.class);

    /** The export to context. */
    private ExportToContext exportToContext = null;

    private String exportType;

    /** The fc. */
    private JFileChooser fileChooser = null;

    private File destination;

    private int destinationMode = JFileChooser.DIRECTORIES_ONLY;

    public AbstractExportToAction(ExportToContext exportToContext) {
        super();
        this.setExportToContext(exportToContext);
    }

    protected ProgressMonitor initProgressMonitor(Component parentComponent, Object message) {
        String note = "";
        int min = 0;
        int max = 100;
        final ProgressMonitor progressMonitor = new ProgressMonitor(parentComponent, message, note, min, max);
        progressMonitor.setProgress(0);
        return progressMonitor;
    }

    private File selectDestination(final Component source, int mode) {
        if (getFileChooser() == null) {
            setFileChooser(new JFileChooser(new File(".")));
            getFileChooser().setFileSelectionMode(mode);
        }
        if (getFileChooser().showSaveDialog(source) == JFileChooser.CANCEL_OPTION) {
            return null;
        }
        final File destination = getFileChooser().getSelectedFile();
        if (mode == JFileChooser.DIRECTORIES_ONLY) {
            LOGGER.info("> Export as " + exportType + " to dir=" + destination);
            if (!destination.exists()) {
                destination.mkdirs();
            }
        } else {
            LOGGER.info("> Export as " + exportType + " to file=" + destination);

        }
        return destination;
    }

    public ExportToContext getExportToContext() {
        return exportToContext;
    }

    public void setExportToContext(ExportToContext exportToContext) {
        this.exportToContext = exportToContext;
    }

    public String getExportType() {
        return exportType;
    }

    public void setExportType(String exportType) {
        this.exportType = exportType;
    }

    public JFileChooser getFileChooser() {
        return fileChooser;
    }

    public void setFileChooser(JFileChooser fc) {
        this.fileChooser = fc;
    }

    protected abstract Runnable createExportTask(ProgressMonitor progressMonitor, File destDir, Component source);

    @Override
    public void actionPerformed(ActionEvent event) {
        final Component source = (Component) event.getSource();
        if (getDestination() == null) {
            File destination = selectDestination(source, getDestinationMode());
            setDestination(destination);
        }

        if (getDestination() == null) {
            return;
        }

        if (source != null) {
            source.setEnabled(false);
        }

        Component parentComponent = getExportToContext().getParentComponent();
        Object message = "Exporting to " + getExportType() + " ...";
        final ProgressMonitor progressMonitor = initProgressMonitor(parentComponent, message);

        Runnable exportTask = createExportTask(progressMonitor, getDestination(), source);
        runExportTask(exportTask);
    }

    private void runExportTask(Runnable exportTask) {
        getExportToContext().getThreadPool().execute(exportTask);
    }

    public int getDestinationMode() {
        return destinationMode;
    }

    public void setDestinationMode(int destinationMode) {
        this.destinationMode = destinationMode;
    }

    public File getDestination() {
        return destination;
    }

    public void setDestination(File destination) {
        this.destination = destination;
    }

}
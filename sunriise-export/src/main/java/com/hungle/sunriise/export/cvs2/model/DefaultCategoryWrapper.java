package com.hungle.sunriise.export.cvs2.model;

import java.io.File;
import java.io.IOException;

import com.hungle.sunriise.mnyobject.Category;

public class DefaultCategoryWrapper extends AbstractExportToCsv2Wrapper<Category, Csv2Category> {
    public DefaultCategoryWrapper(File outFile, boolean appendMode) throws IOException {
        super(outFile, appendMode);
        getMappingStrategy().setType(Csv2Category.class);
    }

    public DefaultCategoryWrapper(File outFile) throws IOException {
        super(outFile);
        getMappingStrategy().setType(Csv2Category.class);
    }

    @Override
    protected Csv2Category wrap(Category value) {
        Csv2Category wrappedType = new Csv2Category(value);
        return wrappedType;

    }

    @Override
    protected String[] getWrapperHeaders() {
        Csv2Category wrappedType = new Csv2Category(null);

        String[] headers = wrappedType.getHeaders();

        return headers;
    }
}
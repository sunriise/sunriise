package com.hungle.sunriise.export.jpa;

import org.springframework.data.jpa.repository.JpaRepository;

import com.hungle.sunriise.export.cvs2.model.Csv2Account;

/**
 * The Interface Csv2AccountRepository.
 */
public interface Csv2AccountRepository extends JpaRepository<Csv2Account, Integer> {
}

package com.hungle.sunriise.export.ui;

import java.awt.Component;
import java.io.File;
import java.io.IOException;

import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;

import org.apache.commons.lang3.time.DurationFormatUtils;
import org.apache.logging.log4j.Logger;

import com.healthmarketscience.jackcess.Database;
import com.hungle.sunriise.io.MnyDb;
import com.hungle.sunriise.util.StopWatch;

/**
 * The Class ExportToCSVTask.
 */
final class ExportToDirTask implements Runnable {
    private static final Logger LOGGER = org.apache.logging.log4j.LogManager.getLogger(ExportToDirTask.class);

    /**
     * 
     */
    private final AbstractExportToAction action;

    /** The progress monitor. */
    private final ExportToDirExporter exporter;

    /** The dest dir. */
    private final File destDir;

    /** The source. */
    private final Component source;

    /**
     * Instantiates a new export to csv task.
     *
     * @param progressMonitor    the progress monitor
     * @param destDir            the dest dir
     * @param source             the source
     * @param exportToCsv2Action TODO
     */
    ExportToDirTask(AbstractExportToAction action, ExportToDirExporter exporter, File destDir, Component source) {
        this.action = action;
        this.exporter = exporter;
        this.destDir = destDir;
        this.source = source;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Runnable#run()
     */
    @Override
    public void run() {
        StopWatch stopWatch = new StopWatch();
        stopWatch.click();

        Database destDb = null;
        Exception exception = null;

        try {
            destDir.mkdirs();
            exporter.setOutDir(destDir);
            MnyDb mnyDb = action.getExportToContext().getMnyDb();
            destDb = exporter.export(mnyDb.getDbFile(), mnyDb.getPassword());
            // XXX - don't close it
            destDb = null;
        } catch (IOException e) {
            LOGGER.error(e);
            exception = e;
        } finally {
            if (destDb != null) {
                try {
                    destDb.close();
                } catch (IOException e1) {
                    LOGGER.warn(e1);
                } finally {
                    destDb = null;
                }
            }
            long durationMillis = stopWatch.click();
            String durationWords = DurationFormatUtils.formatDurationWords(durationMillis, true, true);
            LOGGER.info("  took {}", durationWords);
            LOGGER.info("< DONE, exported to dir=" + destDir.getAbsolutePath());
            ExportToDirTask.doFinally(exception, action.getExportType(), source);
        }
    }

    /**
     * If there is exception, show it to user. Then enable source.
     *
     * @param exception  the exception
     * @param exportType the export type
     * @param source     the source
     */
    public static final void doFinally(final Exception exception, final String exportType, final Component source) {
        Runnable swingRun = new Runnable() {

            @Override
            public void run() {
                if (exception != null) {
                    Component parentComponent = JOptionPane.getFrameForComponent(source);
                    String message = exception.toString();
                    // String exportType = exportToDirAction.getExportType();
                    String title = "Error export to " + exportType;
                    JOptionPane.showMessageDialog(parentComponent, message, title, JOptionPane.ERROR_MESSAGE);
                }
                if (source != null) {
                    source.setEnabled(true);
                }
            }
        };
        SwingUtilities.invokeLater(swingRun);
    }
}
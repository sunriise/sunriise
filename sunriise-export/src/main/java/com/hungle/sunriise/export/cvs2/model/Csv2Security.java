package com.hungle.sunriise.export.cvs2.model;

import java.util.Objects;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.apache.logging.log4j.Logger;

import com.hungle.sunriise.export.cvs2.AbstractCsv2Wrapper;
import com.hungle.sunriise.mnyobject.Security;
import com.opencsv.bean.CsvBindByPosition;

@Entity
@Table(name = "Security")
public class Csv2Security extends AbstractCsv2Wrapper<Security> {
    private static final Logger LOGGER = org.apache.logging.log4j.LogManager.getLogger(Csv2Security.class);

    private static final String[] HEADERS = { "id", "name", "symbol" };

    @Id
    @CsvBindByPosition(position = 0)
    private Integer id;

    @CsvBindByPosition(position = 1)
    private String name;

    @CsvBindByPosition(position = 2)
    private String symbol;

    public String[] getHeaders() {
        String[] headers = HEADERS;
        return headers;
    }

    public Csv2Security() {
        this(null);
    }

    public Csv2Security(Security value) {
        super(value);

        if (value == null) {
            return;
        }

        this.id = value.getId();
        this.name = value.getName();

        this.symbol = value.getSymbol();

    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSymbol() {
        return symbol;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Csv2Security)) {
            return false;
        }
        Csv2Security security = (Csv2Security) o;
        return Objects.equals(getId(), security.getId());
    }

    @Override
    public int hashCode() {
        return getClass().hashCode();
    }
}

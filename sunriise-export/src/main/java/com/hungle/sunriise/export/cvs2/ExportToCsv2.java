package com.hungle.sunriise.export.cvs2;

import java.io.File;
import java.io.IOException;

import org.apache.logging.log4j.Logger;

import com.hungle.sunriise.export.ExportNoPerAccounts;

public class ExportToCsv2 extends ExportNoPerAccounts {
    private static final Logger LOGGER = org.apache.logging.log4j.LogManager.getLogger(ExportToCsv2.class);

    protected void initSerializer() {
        this.setSerializer(new ExportToCsv2Serializer());
    }

    @Override
    protected void visitPreAccountIteration() throws IOException {
        super.visitPreAccountIteration();

        deleteExistingTransactionFiles();
    }

    private void deleteExistingTransactionFiles() {
        File outDir = getOutDir();

        File outFile = null;

        outFile = new File(outDir, ExportToCsv2Serializer.FILENAME_TRANSACTIONS);
        if (outFile.exists()) {
            outFile.delete();
        }

        outFile = new File(outDir, ExportToCsv2Serializer.FILENAME_FILTERED_TRANSACTIONS);
        if (outFile.exists()) {
            outFile.delete();
        }
    }
}

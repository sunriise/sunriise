package com.hungle.sunriise.export.jpa;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

import org.apache.logging.log4j.Logger;

import com.hungle.sunriise.mnyobject.impl.InvestmentActivity;

// TODO: Auto-generated Javadoc
/**
 * The Class InvestmentActivityConverter.
 */
@Converter
public class InvestmentActivityConverter implements AttributeConverter<InvestmentActivity, String> {

    /** The Constant LOGGER. */
    private static final Logger LOGGER = org.apache.logging.log4j.LogManager
            .getLogger(InvestmentActivityConverter.class);

    /** The Constant SEPARATOR. */
    private static final String SEPARATOR = ", ";

    /**
     * Convert to database column.
     *
     * @param investmentActivity the investment activity
     * @return the string
     */
    @Override
    public String convertToDatabaseColumn(InvestmentActivity investmentActivity) {
        if (investmentActivity == null) {
            return null;
        }

        StringBuilder sb = new StringBuilder();
        Integer type = investmentActivity.getType();

        if (type == null) {
            type = Integer.valueOf(0);
        }

        sb.append(type.toString());

        return sb.toString();
    }

    /**
     * Convert to entity attribute.
     *
     * @param dbInvestmentActivity the db investment activity
     * @return the investment activity
     */
    @Override
    public InvestmentActivity convertToEntityAttribute(String dbInvestmentActivity) {
        if (dbInvestmentActivity == null || dbInvestmentActivity.isEmpty()) {
            return null;
        }

        String[] pieces = dbInvestmentActivity.split(SEPARATOR);

        if (pieces == null || pieces.length == 0) {
            return null;
        }

        Integer type = Integer.valueOf(0);
        try {
            type = Integer.valueOf(pieces[0].trim());
        } catch (NumberFormatException e) {
            type = Integer.valueOf(0);
            LOGGER.warn(e);
        }

        InvestmentActivity investmentActivity = new InvestmentActivity(type);

        return investmentActivity;
    }

}

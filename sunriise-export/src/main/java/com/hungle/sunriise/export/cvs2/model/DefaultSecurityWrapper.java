package com.hungle.sunriise.export.cvs2.model;

import java.io.File;
import java.io.IOException;

import com.hungle.sunriise.mnyobject.Security;

public class DefaultSecurityWrapper extends AbstractExportToCsv2Wrapper<Security, Csv2Security> {
    public DefaultSecurityWrapper(File outFile, boolean appendMode) throws IOException {
        super(outFile, appendMode);
        getMappingStrategy().setType(Csv2Security.class);
    }

    public DefaultSecurityWrapper(File outFile) throws IOException {
        super(outFile);
        getMappingStrategy().setType(Csv2Security.class);
    }

    @Override
    protected Csv2Security wrap(Security value) {
        Csv2Security wrappedType = new Csv2Security(value);
        return wrappedType;
    }

    @Override
    protected String[] getWrapperHeaders() {
        Csv2Security wrappedType = new Csv2Security(null);

        String[] headers = wrappedType.getHeaders();

        return headers;
    }
}
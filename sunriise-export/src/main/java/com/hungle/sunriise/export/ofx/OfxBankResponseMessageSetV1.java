package com.hungle.sunriise.export.ofx;

import java.util.Date;
import java.util.List;

import org.apache.logging.log4j.Logger;

import net.ofx.types.x2003.x04.BankAccount;
import net.ofx.types.x2003.x04.BankTransactionList;
import net.ofx.types.x2003.x04.LedgerBalance;
import net.ofx.types.x2003.x04.StatementResponse;
import net.ofx.types.x2003.x04.StatementTransaction;

/**
 * You can use the &lt;STMTRQ&gt; … &lt;STMTRS&gt; request and response pair to
 * download transactions and balances for checking, savings, money market, CD,
 * and line of credit accounts.
 * 
 * @author hungle
 *
 */
public class OfxBankResponseMessageSetV1 extends OfxAbstractResponseMessageSetV1 {
    private static final Logger LOGGER = org.apache.logging.log4j.LogManager
            .getLogger(OfxBankResponseMessageSetV1.class);

    public OfxBankResponseMessageSetV1(BankStatement statement) {
        super();

        createPriceFormatter();

        StatementResponse statementResponse = createStatementResponse(statement);

        // <BANKACCTFROM><!-- Identify the account -->
        // </BANKACCTFROM><!-- End of account ID -->
        BankAccount bankAcctFrom = statementResponse.addNewBANKACCTFROM();

        // <BANKID>121099999</BANKID><!-- Routing transit or other
        // FI ID -->
        String bankId = statement.getBankId();
        bankAcctFrom.setBANKID(bankId);

        // <ACCTID>999988</ACCTID><!-- Account number -->
        String acctId = statement.getAccountId();
        bankAcctFrom.setACCTID(acctId);

        // <ACCTTYPE>CHECKING</ACCTTYPE><!-- Account type -->
        net.ofx.types.x2003.x04.AccountEnum.Enum acctType = getAcctType(statement.getAccountType(),
                net.ofx.types.x2003.x04.AccountEnum.CHECKING);
        bankAcctFrom.setACCTTYPE(acctType);

        // <BANKTRANLIST><!-- Begin list of statement
        // trans. -->
        BankTransactionList bankTransactionList = statementResponse.addNewBANKTRANLIST();

        setDtStartEnd(statement, bankTransactionList);

        List<BankStatementTransaction> transactions = statement.getTransactions();
        for (BankStatementTransaction transaction : transactions) {
            addSTMTTRN(transaction, bankTransactionList);
        }

        addLedgerBalance(statement, statementResponse);

        localizeXmlFragment();
    }

    private void addSTMTTRN(BankStatementTransaction transaction, BankTransactionList bankTransactionList) {
        // <STMTTRN><!-- First statement transaction -->
        // </STMTTRN><!-- End statement transaction -->
        StatementTransaction statementTransaction = bankTransactionList.addNewSTMTTRN();

        net.ofx.types.x2003.x04.TransactionEnum.Enum trnType = getTrnType(transaction.getType(),
                net.ofx.types.x2003.x04.TransactionEnum.CHECK);
        // <TRNTYPE>CHECK</TRNTYPE><!--Check -->
        statementTransaction.setTRNTYPE(trnType);

        // <DTPOSTED>20051004</DTPOSTED><!-- Posted on Oct. 4, 2005 -->
        Date dtPostedDate = transaction.getDtPostedDate();
        String dtPosted = OfxDateTime.formatLocalTime(dtPostedDate);
        statementTransaction.setDTPOSTED(dtPosted);

        // <TRNAMT>-200.00</TRNAMT><!-- $200.00 -->
        String trnAmt = formatAmount(transaction.getAmount());
        statementTransaction.setTRNAMT(trnAmt);

        // <FITID>00002</FITID><!-- Unique ID -->
        String fitId = transaction.getId();
        statementTransaction.setFITID(fitId);

        // <CHECKNUM>1000</CHECKNUM><!-- Check number -->
        String checkNum = transaction.getCheckNumber();
        statementTransaction.setCHECKNUM(checkNum);

    }

    private void addLedgerBalance(BankStatement statement, StatementResponse statementResponse) {
        LedgerBalance ledgerBalance = statementResponse.addNewLEDGERBAL();

        addLedgerBalance(statement, ledgerBalance);
    }

    private void addLedgerBalance(BankStatement statement, LedgerBalance ledgerBalance) {
        // <LEDGERBAL><!-- Ledger balance aggregate -->
        // <BALAMT>200.29</BALAMT><!-- Bal amount: $200.29 -->
        // <DTASOF>200510291120</DTASOF><!-- Bal date: 10/29/05, 11:20 am -->
        // </LEDGERBAL><!-- End ledger balance -->
        Double balamtDouble = statement.getLedgerBalanceBalanceAmount();
        String balamt = formatAmount(balamtDouble);
        ledgerBalance.setBALAMT(balamt);

        Date dtAsOfDate = statement.getLedgerBalanceDateAsOf();
        String dtasof = OfxDateTime.formatLocalTime(dtAsOfDate);
        ledgerBalance.setDTASOF(dtasof);
    }
}

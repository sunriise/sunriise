package com.hungle.sunriise.export.ofx;

import java.io.File;
import java.io.IOException;
import java.text.NumberFormat;
import java.util.Date;
import java.util.UUID;

import javax.xml.namespace.QName;

import org.apache.logging.log4j.Logger;
import org.apache.xmlbeans.XmlCursor;
import org.apache.xmlbeans.XmlObject;
import org.apache.xmlbeans.XmlOptions;

import net.ofx.types.x2003.x04.BankResponseMessageSetV1;
import net.ofx.types.x2003.x04.BankTransactionList;
import net.ofx.types.x2003.x04.CreditCardStatementResponse;
import net.ofx.types.x2003.x04.CreditCardStatementTransactionResponse;
import net.ofx.types.x2003.x04.CreditcardResponseMessageSetV1;
import net.ofx.types.x2003.x04.CurrencyEnum;
import net.ofx.types.x2003.x04.CurrencyEnum.Enum;
import net.ofx.types.x2003.x04.LanguageEnum;
import net.ofx.types.x2003.x04.OFX;
import net.ofx.types.x2003.x04.OFXDocument;
import net.ofx.types.x2003.x04.SeverityEnum;
import net.ofx.types.x2003.x04.SignonResponse;
import net.ofx.types.x2003.x04.SignonResponseMessageSetV1;
import net.ofx.types.x2003.x04.StatementResponse;
import net.ofx.types.x2003.x04.StatementTransactionResponse;
import net.ofx.types.x2003.x04.Status;

public class OfxAbstractResponseMessageSetV1 {
    private static final Logger LOGGER = org.apache.logging.log4j.LogManager
            .getLogger(OfxBankResponseMessageSetV1.class);

    private NumberFormat priceFormatter;

    private boolean allowInsertComment = true;

    private int dateOffset = 0;

    private Date currentDateTime = new Date();

    private XmlOptions xmlOptions;

    private OFXDocument ofxDocument;

    protected StatementResponse createStatementResponse(BankStatement statement) {

        OFX ofx = createOFX();

        // <BANKMSGSRSV1>
        BankResponseMessageSetV1 bankResponseMessageSetV1 = ofx.addNewBANKMSGSRSV1();

        // <STMTTRNRS><!-- Begin response -->
        StatementTransactionResponse transactionResponse = bankResponseMessageSetV1.addNewSTMTTRNRS();

        // <TRNUID>1001</TRNUID><!-- Client ID sent in request -->
        String trnUid = UUID.randomUUID().toString();
        transactionResponse.setTRNUID(trnUid);

        // <STATUS><!-- Start status aggregate -->
        // <CODE>0</CODE><!-- OK -->
        // <SEVERITY>INFO</SEVERITY>
        // </STATUS>
        // Status aggregate
        Status status = transactionResponse.addNewSTATUS();
        status.setCODE("0");
        status.setSEVERITY(SeverityEnum.INFO);

        // <STMTRS><!-- Begin statement response -->
        StatementResponse statementResponse = transactionResponse.addNewSTMTRS();

        // <CURDEF>USD</CURDEF>
        Enum curDef = getCurDef(statement.getCurrency(), CurrencyEnum.USD);
        statementResponse.setCURDEF(curDef);

        return statementResponse;
    }

    private Enum getCurDef(String currency, Enum defaultValue) {
        Enum curDef = null;

        try {
            curDef = Enum.forString(currency);
        } catch (Exception e) {
            LOGGER.warn(e);
        }

        if (curDef == null) {
            curDef = defaultValue;
        }

        return curDef;
    }

    /**
     * Adds the signon response message set V 1.
     *
     * @param ofx the ofx
     * @return the signon response message set V 1
     */
    private SignonResponseMessageSetV1 addSignonResponseMessageSetV1(OFX ofx) {
        // The Signon Message Set
        SignonResponseMessageSetV1 root = ofx.addNewSIGNONMSGSRSV1();

        // Signon Response
        SignonResponse sonrs = root.addNewSONRS();

        // Error Reporting <STATUS>
        Status status = sonrs.addNewSTATUS();
        status.setCODE("0");
        status.setSEVERITY(SeverityEnum.INFO);
        // message is optional
        status.setMESSAGE(XmlBeansUtils.SUCCESSFUL_SIGN_ON);

        /**
         * Date and time of the server response, datetime (YYYYMMDDHHMMSS.XXX)
         * 19961005132200.124[-5:EST] represents October 5, 1996, at 1:22 and 124
         * milliseconds p.m., in Eastern Standard Time
         */
        // String dateTime = getCurrentDateTime();
        sonrs.setDTSERVER(OfxDateTimeUtils.getDtServer(currentDateTime));
        // String comment = "DTSERVER local time is " +
        // XmlBeansUtils.formatLocal(currentDateTime);
        String comment = "DTSERVER local time is " + currentDateTime;
        insertComment(status, comment);
        if (dateOffset != 0) {
            comment = "User requests to set trade date offset to: " + dateOffset;
            insertComment(status, comment);
        }

        // Language used in text responses, language
        sonrs.setLANGUAGE(LanguageEnum.ENG);

        /*
         * Date and time of last update to profile information for any service supported
         * by this FI (see Chapter 7, "FI Profile"), datetime
         */
        // sonrs.setDTPROFUP("TODO-20010918083000");
        // Financial-Institution-identification aggregate
        // sonrs.addNewFI().setORG("TODO-Vanguard");
        return root;
    }

    /**
     * Insert comment.
     *
     * @param xmlObject the xml object
     * @param comment   the comment
     */
    private final void insertComment(XmlObject xmlObject, String comment) {
        if (!allowInsertComment) {
            return;
        }
        XmlCursor cursor = null;
        try {
            cursor = xmlObject.newCursor();
            cursor.insertComment(comment);
        } finally {
            if (cursor != null) {
                try {
                    cursor.dispose();
                } finally {
                    cursor = null;
                }
            }
        }
    }

    private OFX createOFX() {
        xmlOptions = XmlBeansUtils.createXmlOptions();
        ofxDocument = OFXDocument.Factory.newInstance(xmlOptions);
        OFX ofx = ofxDocument.addNewOFX();

        XmlBeansUtils.insertProcInst(ofx);
        SignonResponseMessageSetV1 signonResponseMessageSetV1 = addSignonResponseMessageSetV1(ofx);
        insertComment(signonResponseMessageSetV1, "Created by hleOfxQuotes on: " + currentDateTime);
        return ofx;
    }

    protected String formatAmount(Double amount) {
        return priceFormatter.format(amount);
    }

    public void save(File file) throws IOException {
        ofxDocument.save(file, xmlOptions);
    }

    protected void createPriceFormatter() {
        priceFormatter = OfxPriceUtils.createPriceFormatter();
    }

    /**
     * Remove all the namespace stuffs.
     *
     * @param xmlObject the xml object
     * @return the xml object
     */
    private static XmlObject localizeXmlFragment(XmlObject xmlObject) {
        String s;
        XmlCursor c = xmlObject.newCursor();
        c.toNextToken();
        while (c.hasNextToken()) {
            if (c.isNamespace()) {
                c.removeXml();
            } else {
                if (c.isStart() || c.isAttr()) {
                    s = c.getName().getLocalPart();
                    c.setName(new QName(s));
                }
                c.toNextToken();
            }
        }
        c.dispose();
        return xmlObject;
    }

    protected void localizeXmlFragment() {
        localizeXmlFragment(ofxDocument);
    }

    protected void setDtStartEnd(BankStatement statement, BankTransactionList bankTransactionList) {
        Date dtStartDate = statement.getDtStartDate();
        Date dtEndDate = statement.getDtEndDate();

        String dtStart = OfxDateTime.formatLocalTime(dtStartDate);
        bankTransactionList.setDTSTART(dtStart);

        String dtEnd = OfxDateTime.formatLocalTime(dtEndDate);
        bankTransactionList.setDTEND(dtEnd);
    }

    protected net.ofx.types.x2003.x04.AccountEnum.Enum getAcctType(String accountType,
            net.ofx.types.x2003.x04.AccountEnum.Enum defaultValue) {
        net.ofx.types.x2003.x04.AccountEnum.Enum type = null;

        try {
            type = net.ofx.types.x2003.x04.AccountEnum.Enum.forString(accountType);
        } catch (Exception e) {
            LOGGER.warn(e);
        }

        if (type == null) {
            type = defaultValue;
        }

        return type;
    }

    protected net.ofx.types.x2003.x04.TransactionEnum.Enum getTrnType(String type,
            net.ofx.types.x2003.x04.TransactionEnum.Enum defaultValue) {
        net.ofx.types.x2003.x04.TransactionEnum.Enum trnType = null;

        try {
            trnType = net.ofx.types.x2003.x04.TransactionEnum.Enum.forString(type);
        } catch (Exception e) {
            LOGGER.warn(e);
        }

        if (trnType == null) {
            trnType = defaultValue;
        }

        return trnType;
    }

    protected CreditCardStatementResponse createCreditCardStatementResponse(CreditCardStatement statement) {

        OFX ofx = createOFX();

        // <BANKMSGSRSV1>
        CreditcardResponseMessageSetV1 creditcardResponseMessageSetV1 = ofx.addNewCREDITCARDMSGSRSV1();

        // <STMTTRNRS><!-- Begin response -->
        CreditCardStatementTransactionResponse transactionResponse = creditcardResponseMessageSetV1.addNewCCSTMTTRNRS();

        // <TRNUID>1001</TRNUID><!-- Client ID sent in request -->
        String trnUid = UUID.randomUUID().toString();
        transactionResponse.setTRNUID(trnUid);

        // <STATUS><!-- Start status aggregate -->
        // <CODE>0</CODE><!-- OK -->
        // <SEVERITY>INFO</SEVERITY>
        // </STATUS>
        // Status aggregate
        Status status = transactionResponse.addNewSTATUS();
        status.setCODE("0");
        status.setSEVERITY(SeverityEnum.INFO);

        // <STMTRS><!-- Begin statement response -->
        CreditCardStatementResponse statementResponse = transactionResponse.addNewCCSTMTRS();

        // <CURDEF>USD</CURDEF>
        Enum curDef = getCurDef(statement.getCurrency(), CurrencyEnum.USD);
        statementResponse.setCURDEF(curDef);

        return statementResponse;
    }

    protected void setDtStartEnd(CreditCardStatement statement, BankTransactionList bankTransactionList) {
        // <DTSTART>20050801</DTSTART>
        // <DTEND>20050831165153.000[-8:PST]</DTEND>
        Date dtStartDate = statement.getDtStartDate();
        Date dtEndDate = statement.getDtEndDate();

        String dtStart = OfxDateTime.formatLocalTime(dtStartDate);
        bankTransactionList.setDTSTART(dtStart);

        String dtEnd = OfxDateTime.formatLocalTime(dtEndDate);
        bankTransactionList.setDTEND(dtEnd);
    }

}

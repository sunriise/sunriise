package com.hungle.sunriise.export.cvs2;

public abstract class AbstractCsv2Wrapper<T> {
    protected T value;

    public AbstractCsv2Wrapper(T value) {
        super();
        this.value = value;
    }

    public abstract String[] getHeaders();
}

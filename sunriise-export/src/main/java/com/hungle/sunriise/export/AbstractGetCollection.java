package com.hungle.sunriise.export;

import java.util.Collection;

import com.hungle.sunriise.mnyobject.MnyContext;

// TODO: Auto-generated Javadoc
/**
 * The Class AbstractGetCollection.
 *
 * @param <T> the generic type
 */
public abstract class AbstractGetCollection<T> {

    /** The mny context. */
    private final MnyContext mnyContext;

    /**
     * Instantiates a new abstract get collection.
     *
     * @param mnyContext the mny context
     */
    public AbstractGetCollection(MnyContext mnyContext) {
        super();
        this.mnyContext = mnyContext;
    }

    /**
     * Gets the mny context.
     *
     * @return the mny context
     */
    public MnyContext getMnyContext() {
        return mnyContext;
    }

    /**
     * Gets the collection.
     *
     * @return the collection
     */
    public abstract Collection<T> getCollection();

}
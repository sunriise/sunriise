package com.hungle.sunriise.export.jpa;

import org.springframework.data.jpa.repository.JpaRepository;

import com.hungle.sunriise.export.cvs2.model.Csv2Category;

/**
 * The Interface Csv2CategoryRepository.
 */
public interface Csv2CategoryRepository extends JpaRepository<Csv2Category, Integer> {

}

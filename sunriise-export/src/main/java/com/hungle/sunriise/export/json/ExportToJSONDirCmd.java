package com.hungle.sunriise.export.json;

import java.io.File;
import java.io.IOException;

import org.apache.logging.log4j.Logger;

import com.hungle.sunriise.export.AbstractExportToDir;

// TODO: Auto-generated Javadoc
/**
 * The Class ExportToJSONDirCmd.
 */
public class ExportToJSONDirCmd {

    /** The Constant LOGGER. */
    private static final Logger LOGGER = org.apache.logging.log4j.LogManager.getLogger(ExportToJSONDirCmd.class);

    /**
     * The main method.
     *
     * @param args the arguments
     */
    public static void main(String[] args) {
        File dbFile = null;
        String password = null;
        File outDir = null;

        if (args.length == 2) {
            dbFile = new File(args[0]);
            outDir = new File(args[1]);
        } else if (args.length == 3) {
            dbFile = new File(args[0]);
            password = args[1];
            outDir = new File(args[2]);
        } else {
            Class<ExportToJSONDirCmd> clz = ExportToJSONDirCmd.class;
            System.out.println("Usage: java " + clz.getName() + " in.mny [password] outDir");
            System.exit(1);
        }

        export(dbFile, password, outDir);
    }

    /**
     * Export.
     *
     * @param dbFile   the db file
     * @param password the password
     * @param outDir   the out dir
     */
    public static void export(File dbFile, String password, File outDir) {
        LOGGER.info("dbFile=" + dbFile);
        LOGGER.info("outDir=" + outDir);

        outDir.mkdirs();
        if (!outDir.isDirectory()) {
            LOGGER.error("Not a directory, dir=" + outDir);
            return;
        }

        try {
            AbstractExportToDir cmd = new ExportToJSON();
            cmd.setOutDir(outDir);

            cmd.export(dbFile, password);
        } catch (IOException e) {
            LOGGER.error(e, e);
        } finally {
            LOGGER.info("< DONE");
        }
    }

}

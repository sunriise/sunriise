package com.hungle.sunriise.export.mongodb;

import static org.bson.codecs.configuration.CodecRegistries.fromProviders;
import static org.bson.codecs.configuration.CodecRegistries.fromRegistries;

import java.io.IOException;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.Logger;
import org.bson.Document;
import org.bson.codecs.configuration.CodecProvider;
import org.bson.codecs.configuration.CodecRegistry;
import org.bson.codecs.pojo.PojoCodecProvider;

import com.healthmarketscience.jackcess.Database;
import com.hungle.sunriise.export.AbstractExportTo;
import com.hungle.sunriise.io.MnyDb;
import com.hungle.sunriise.mnyobject.Account;
import com.hungle.sunriise.mnyobject.Category;
import com.hungle.sunriise.mnyobject.Currency;
import com.hungle.sunriise.mnyobject.Payee;
import com.hungle.sunriise.mnyobject.Security;
import com.hungle.sunriise.mnyobject.Transaction;
import com.hungle.sunriise.util.StopWatch;
import com.mongodb.MongoClient;
import com.mongodb.MongoClientOptions;
import com.mongodb.MongoClientOptions.Builder;
import com.mongodb.MongoClientURI;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;

// TODO: Auto-generated Javadoc
/**
 * The Class ExportToMongoDb.
 */
public class ExportToMongoDb extends AbstractExportTo {

    /** The Constant LOGGER. */
    private static final Logger LOGGER = org.apache.logging.log4j.LogManager.getLogger(ExportToMongoDb.class);

    /** The Constant DEFAULT_HOST. */
    private static final String DEFAULT_HOST = "localhost";

    /** The Constant DEFAULT_PORT. */
    private static final int DEFAULT_PORT = 27017;

    /** The Constant DEFAULT_MONGO_CLIENT_URI. */
    static final String DEFAULT_MONGO_CLIENT_URI = "mongodb://" + DEFAULT_HOST + ":" + DEFAULT_PORT;

    /** The Constant DEFAULT_DATABASE_NAME. */
    public static final String DEFAULT_DATABASE_NAME = "sunriiseExport";

    /** The stop watch. */
    private StopWatch stopWatch = new StopWatch();

    /** The mongo client. */
    private MongoClient mongoClient;

    /** The mongo db. */
    private MongoDatabase mongoDb;

    /** The Constant DEFAULT_BATCH_SIZE. */
    private static final int DEFAULT_BATCH_SIZE = 100;

    /** The batch size. */
    private int batchSize = DEFAULT_BATCH_SIZE;

    /** The database name. */
    private String databaseName = DEFAULT_DATABASE_NAME;

    /**
     * Export.
     *
     * @param mnyDb       the src db
     * @param mongoClient the mongo client URI
     * @return the database
     * @throws IOException Signals that an I/O exception has occurred.
     */
    public Database exportToMongo(MnyDb mnyDb, MongoClient mongoClient) throws IOException {
        if (mnyDb == null) {
            return null;
        }
        if (mnyDb.getDb() == null) {
            return null;
        }

        startExport(mongoClient);

        try {
            _visit(mnyDb);
        } finally {
            endExport(mongoClient);
        }

        return mnyDb.getDb();
    }

    /**
     * Start export.
     *
     * @param mongoClient the mongo client URI
     */
    protected void startExport(MongoClient mongoClient) {
        stopWatch.click();
        LOGGER.info("> startExport, mongoClient=" + mongoClient);
    }

    /**
     * End export.
     *
     * @param mongoClient the mongo client URI
     */
    protected void endExport(MongoClient mongoClient) {
        stopWatch.logTiming(LOGGER, "> endExport, mongoClient=" + mongoClient);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.hungle.sunriise.export.AbstractExportToJSON#export(com.hungle.
     * sunriise.io .MnyDb)
     */
    @Override
    protected Database export(MnyDb mnyDb) throws IOException {
        Database db = null;

        if (mongoClient == null) {
            throw new IOException("mongoClient is null");
        }

        try {
            // this.mongoClient = new MongoClient(mongoClientURI);
            LOGGER.info("db.connectPoint=" + mongoClient.getConnectPoint());
            if (databaseName == null) {
                databaseName = DEFAULT_DATABASE_NAME;
            }
            this.mongoDb = mongoClient.getDatabase(databaseName);

            LOGGER.info("Export to db.name=" + mongoDb.getName());
            MongoUtils.removeAllCollections(mongoDb);

            db = exportToMongo(mnyDb, mongoClient);

            if (LOGGER.isDebugEnabled()) {
                MongoUtils.listAllCollections(mongoDb);
            }
        } finally {
        }

        return db;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.hungle.sunriise.export.AbstractExportToJSON#exportCategories()
     */
    @Override
    protected void exportCategories() throws IOException {
        LOGGER.info("> exportCategories");
        // Categories
        Collection<Category> categoryItems = getMnyContext().getCategories().values();
        MongoUtils.bulkWriteItems(categoryItems, MongoUtils.CATEGORIES_COLLECTION_NAME, mongoDb, batchSize);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.hungle.sunriise.export.AbstractExportToJSON#exportPayees()
     */
    @Override
    protected void exportPayees() throws IOException {
        LOGGER.info("> exportPayees");
        // Payees
        Collection<Payee> payeeItems = getMnyContext().getPayees().values();
        MongoUtils.bulkWriteItems(payeeItems, MongoUtils.PAYEES_COLLECTION_NAME, mongoDb, batchSize);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.hungle.sunriise.export.AbstractExportToJSON#exportCurrencies()
     */
    @Override
    protected void exportCurrencies() throws IOException {
        LOGGER.info("> exportCurrencies");
        // Currencies
        Collection<Currency> currencyItems = getMnyContext().getCurrencies().values();
        MongoUtils.bulkWriteItems(currencyItems, MongoUtils.CURRENCIES_COLLECTION_NAME, mongoDb, batchSize);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.hungle.sunriise.export.AbstractExportToJSON#exportSecurities()
     */
    @Override
    protected void exportSecurities() throws IOException {
        LOGGER.info("> exportSecurities");
        // Securities
        Collection<Security> securityItems = getMnyContext().getSecurities().values();
        MongoUtils.bulkWriteItems(securityItems, MongoUtils.SECURITIES_COLLECTION_NAME, mongoDb, batchSize);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.hungle.sunriise.export.AbstractExportToJSON#exportAccountNames()
     */
    @Override
    protected void exportAccountNames() throws IOException {
        LOGGER.info("> exportAccountNames");
        // Accounts
        Map<Integer, Account> accounts = getMnyContext().getAccounts();
        Collection<Account> accountItems = accounts.values();
        MongoUtils.bulkWriteItems(accountItems, MongoUtils.ACCOUNTS_COLLECTION_NAME, mongoDb, batchSize);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.hungle.sunriise.export.AbstractExportToJSON#exportAccountDetail(com.
     * hungle.sunriise.mnyobject.Account)
     */
    @Override
    protected void exportAccountDetail(Account account) throws IOException {
        LOGGER.info("> exportAccountDetail, account=" + account.getName());
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.hungle.sunriise.export.AbstractExportToJSON#exportAccountTransactions(com
     * .hungle.sunriise.mnyobject.Account)
     */
    @Override
    protected void exportAccountTransactions(Account account) throws IOException {
        LOGGER.info("> exportAccountTransactions, account=" + account.getName());
        List<Transaction> transactions = account.getTransactions();

        final MongoCollection<Document> collection = MongoUtils.getTransactionsCollection(mongoDb);
        LOGGER.info("  collection=" + collection.getNamespace());

        MongoUtils.bulkWriteItems(transactions, collection, batchSize);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.hungle.sunriise.export.AbstractExportToJSON#
     * exportAccountFilteredTransactions(com.hungle.sunriise.mnyobject.Account)
     */
    @Override
    protected void exportAccountFilteredTransactions(Account account) throws IOException {
        LOGGER.info("> exportAccountFilteredTransactions, account=" + account.getName());
        List<Transaction> transactions = account.getTransactions();

        final MongoCollection<Document> collection = MongoUtils.getFilteredTransactionsCollection(mongoDb);
        LOGGER.info("  collection=" + collection.getNamespace());

        MongoUtils.bulkWriteItems(transactions, collection, batchSize);
    }

    /**
     * Gets the batch size.
     *
     * @return the batch size
     */
    public int getBatchSize() {
        return batchSize;
    }

    /**
     * Sets the batch size.
     *
     * @param batchSize the new batch size
     */
    public void setBatchSize(int batchSize) {
        this.batchSize = batchSize;
    }

    /**
     * Sets the mongo db.
     *
     * @param mongoDb the new mongo db
     */
    public void setMongoDb(MongoDatabase mongoDb) {
        this.mongoDb = mongoDb;
    }

    /**
     * Gets the mongo client.
     *
     * @return the mongo client
     */
    public MongoClient getMongoClient() {
        return mongoClient;
    }

    /**
     * Sets the mongo client.
     *
     * @param mongoClient the new mongo client
     */
    public void setMongoClient(MongoClient mongoClient) {
        this.mongoClient = mongoClient;
    }

    /**
     * Creates the mongo client URI.
     *
     * @param uriString the uri string
     * @return the mongo client URI
     */
    public static final MongoClientURI createMongoClientURI(String uriString) {
        CodecRegistry defaultCodecRegistry = MongoClient.getDefaultCodecRegistry();
        CodecProvider pojoCodecProvider = PojoCodecProvider.builder().automatic(true).build();
        CodecRegistry pojoCodecRegistry = fromRegistries(defaultCodecRegistry, fromProviders(pojoCodecProvider));
        Builder mongoClientOptions = MongoClientOptions.builder().codecRegistry(pojoCodecRegistry);
        if (uriString == null) {
            uriString = DEFAULT_MONGO_CLIENT_URI;
        }
        MongoClientURI mongoClientURI = new MongoClientURI(uriString, mongoClientOptions);
        return mongoClientURI;
    }

    /**
     * Gets the database name.
     *
     * @return the database name
     */
    public String getDatabaseName() {
        return databaseName;
    }

    /**
     * Sets the database name.
     *
     * @param databaseName the new database name
     */
    public void setDatabaseName(String databaseName) {
        this.databaseName = databaseName;
    }

    /**
     * Checks if is mongo client URI.
     *
     * @param arg the arg
     * @return true, if is mongo client URI
     */
    public static boolean isMongoClientURI(String arg) {
        return arg.startsWith(ExportToMongoDbCmd.MONGODB_PREFIX);
    }

    /**
     * Creates the mongo client.
     *
     * @param mongoClientURI the mongo client URI
     * @param exporter       the exporter
     * @return the mongo client
     */
    public static MongoClient createMongoClient(MongoClientURI mongoClientURI, ExportToMongoDb exporter) {
        MongoClient mongoClient;
        mongoClient = new MongoClient(mongoClientURI);
        if (exporter != null) {
            exporter.setMongoClient(mongoClient);
            String databaseName = mongoClientURI.getDatabase();
            if (databaseName == null) {
                databaseName = DEFAULT_DATABASE_NAME;
            }
            exporter.setDatabaseName(databaseName);
        }
        return mongoClient;
    }

    /**
     * Creates the mongo client.
     *
     * @param mongoClientURI the mongo client URI
     * @return the mongo client
     */
    public static MongoClient createMongoClient(MongoClientURI mongoClientURI) {
        return createMongoClient(mongoClientURI, null);
    }

}

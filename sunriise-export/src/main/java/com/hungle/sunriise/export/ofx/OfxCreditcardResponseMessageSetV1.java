package com.hungle.sunriise.export.ofx;

import java.util.Date;
import java.util.List;

import org.apache.logging.log4j.Logger;

import net.ofx.types.x2003.x04.BankTransactionList;
import net.ofx.types.x2003.x04.CreditCardAccount;
import net.ofx.types.x2003.x04.CreditCardStatementResponse;
import net.ofx.types.x2003.x04.LedgerBalance;
import net.ofx.types.x2003.x04.StatementTransaction;

/**
 * 11.4.3 Credit Card Statement Download The credit card download request is
 * semantically identical to the bank statement download request. However, the
 * &lt;CCSTMTRQ&gt; aggregate contains the credit card request, not the
 * &lt;STMTRQ&gt; aggregate. All notes and conditions from 11.4.2 , "Bank
 * Statement Download" apply to credit card downloads including pending
 * transactions.
 * 
 * @author hungle
 *
 */
public class OfxCreditcardResponseMessageSetV1 extends OfxAbstractResponseMessageSetV1 {
    private static final Logger LOGGER = org.apache.logging.log4j.LogManager
            .getLogger(OfxCreditcardResponseMessageSetV1.class);

    public OfxCreditcardResponseMessageSetV1(CreditCardStatement statement) {
        super();

        createPriceFormatter();

        CreditCardStatementResponse statementResponse = createCreditCardStatementResponse(statement);

        // <CCACCTFROM>
        // <ACCTID>123412341234</ACCTID>
        // </CCACCTFROM>
        CreditCardAccount bankAcctFrom = statementResponse.addNewCCACCTFROM();

        String acctId = statement.getAccountId();
        bankAcctFrom.setACCTID(acctId);

        // <BANKTRANLIST><!-- Begin list of statement
        // trans. -->
        BankTransactionList bankTransactionList = statementResponse.addNewBANKTRANLIST();

        setDtStartEnd(statement, bankTransactionList);

        List<CreditCardStatementTransaction> transactions = statement.getTransactions();
        for (CreditCardStatementTransaction transaction : transactions) {
            addSTMTTRN(transaction, bankTransactionList);
        }

        addLedgerBalance(statement, statementResponse);

        localizeXmlFragment();
    }

    private void addSTMTTRN(CreditCardStatementTransaction transaction, BankTransactionList bankTransactionList) {
        // <STMTTRN><!-- First statement transaction -->
        // </STMTTRN><!-- End statement transaction -->
        StatementTransaction statementTransaction = bankTransactionList.addNewSTMTTRN();

        net.ofx.types.x2003.x04.TransactionEnum.Enum trnType = getTrnType(transaction.getType(),
                net.ofx.types.x2003.x04.TransactionEnum.INT);
        // <TRNTYPE>INT</TRNTYPE>
        statementTransaction.setTRNTYPE(trnType);

        // <DTPOSTED>20050811080000</DTPOSTED>
        Date dtPostedDate = transaction.getDtPostedDate();
        String dtPosted = OfxDateTime.formatLocalTime(dtPostedDate);
        statementTransaction.setDTPOSTED(dtPosted);

        // <TRNAMT>-23.00</TRNAMT>
        String trnAmt = formatAmount(transaction.getAmount());
        statementTransaction.setTRNAMT(trnAmt);

        // <FITID>00002</FITID><!-- Unique ID -->
        String fitId = transaction.getId();
        statementTransaction.setFITID(fitId);

        // <NAME>Interest Charge</NAME>
        String name = transaction.getName();
        statementTransaction.setNAME(name);
    }

    private void addLedgerBalance(CreditCardStatement statement, CreditCardStatementResponse statementResponse) {
        // <LEDGERBAL><!-- Ledger balance aggregate -->
        // <BALAMT>200.29</BALAMT><!-- Bal amount: $200.29 -->
        // <DTASOF>200510291120</DTASOF><!-- Bal date: 10/29/05, 11:20 am -->
        // </LEDGERBAL><!-- End ledger balance -->
        LedgerBalance ledgerBalance = statementResponse.addNewLEDGERBAL();
        addLedgerBalance(statement, ledgerBalance);
    }

    private void addLedgerBalance(CreditCardStatement statement, LedgerBalance ledgerBalance) {
        Double balamtDouble = statement.getLedgerBalanceBalanceAmount();
        String balamt = formatAmount(balamtDouble);
        ledgerBalance.setBALAMT(balamt);
        Date dtAsOfDate = statement.getLedgerBalanceDateAsOf();
        String dtasof = OfxDateTime.formatLocalTime(dtAsOfDate);
        ledgerBalance.setDTASOF(dtasof);
    }

}

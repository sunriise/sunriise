package com.hungle.sunriise.export.cvs2;

import java.io.File;
import java.io.IOException;

import org.apache.logging.log4j.Logger;

import com.hungle.sunriise.export.AbstractExportToDir;
import com.hungle.sunriise.io.sample.MnySampleFile;
import com.hungle.sunriise.io.sample.MnySampleFileFactory;

// TODO: Auto-generated Javadoc
/**
 * The Class ExportToJSONDirCmd.
 */
public class ExportSampleToCsv2Cmd {

    /** The Constant LOGGER. */
    private static final Logger LOGGER = org.apache.logging.log4j.LogManager.getLogger(ExportSampleToCsv2Cmd.class);

    /**
     * The main method.
     *
     * @param args the arguments
     */
    public static void main(String[] args) {
        File dbFile = null;
        String password = null;
        File outDir = null;

        try {
            MnySampleFile sampleFile = MnySampleFileFactory.getSunsetSampleFile();
            dbFile = MnySampleFile.getSampleFileFromModuleProject(sampleFile);
            password = sampleFile.getPassword();
            outDir = new File("target/exportCsv");
            outDir.mkdirs();

            ExportSampleToCsv2Cmd.export(dbFile, password, outDir);
        } catch (IOException e) {
            LOGGER.error(e.getMessage(), e);
        }
    }

    /**
     * Export.
     *
     * @param dbFile   the db file
     * @param password the password
     * @param outDir   the out dir
     */
    public static void export(File dbFile, String password, File outDir) {
        LOGGER.info("dbFile=" + dbFile);
        LOGGER.info("outDir=" + outDir);

        outDir.mkdirs();
        if (!outDir.isDirectory()) {
            LOGGER.error("Not a directory, dir=" + outDir);
            return;
        }

        try {
            AbstractExportToDir cmd = new ExportToCsv2();
            cmd.setOutDir(outDir);

            cmd.export(dbFile, password);
        } catch (IOException e) {
            LOGGER.error(e, e);
        } finally {
            LOGGER.info("< DONE");
        }
    }

}

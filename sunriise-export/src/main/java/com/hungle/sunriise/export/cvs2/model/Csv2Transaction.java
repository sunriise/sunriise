package com.hungle.sunriise.export.cvs2.model;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Objects;

import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.Logger;

import com.hungle.sunriise.export.cvs2.AbstractCsv2Wrapper;
import com.hungle.sunriise.export.jpa.FrequencyConverter;
import com.hungle.sunriise.export.jpa.InvestmentActivityConverter;
import com.hungle.sunriise.mnyobject.AccountLink;
import com.hungle.sunriise.mnyobject.CategoryLink;
import com.hungle.sunriise.mnyobject.Classification1Link;
import com.hungle.sunriise.mnyobject.Frequency;
import com.hungle.sunriise.mnyobject.InvestmentInfo;
import com.hungle.sunriise.mnyobject.InvestmentTransaction;
import com.hungle.sunriise.mnyobject.PayeeLink;
import com.hungle.sunriise.mnyobject.Security;
import com.hungle.sunriise.mnyobject.Transaction;
import com.hungle.sunriise.mnyobject.TransactionSplit;
import com.hungle.sunriise.mnyobject.XferInfo;
import com.hungle.sunriise.mnyobject.impl.InvestmentActivity;
import com.hungle.sunriise.util.BalanceUtils;
import com.opencsv.bean.CsvBindByPosition;
import com.opencsv.bean.CsvDate;

@Entity
@Table(name = "Transaction")
public class Csv2Transaction extends AbstractCsv2Wrapper<Transaction> {
    private static final Logger LOGGER = org.apache.logging.log4j.LogManager.getLogger(Csv2Transaction.class);

    private static final String[] HEADERS = { "id", "date", "number", "amount", "categoryId", "category", "memo",
            "payeeId", "payee", "splitsCount", "splits", "splitParentId", "accountId", "account", "void", "cleared",
            "reconciled", "unaccepted", "transfer", "xferAccountId", "xferTransactionId", "investment", "invActivity",
            "invSecId", "invSecName", "invSecSymbol", "invTxPrice", "invTxQuantity", "recurring", "frequency",
            "classification1Id", "classification1Name", "fiTid", };

    @Id
    @CsvBindByPosition(position = 0)
    private Integer id;

    @CsvBindByPosition(position = 1)
    // @CsvDate(value = "yyyy-MM-dd'T'HH:mm:ss.SSSZ")
    @CsvDate(value = "yyyy/MM/dd")
    private Date date;

    @CsvBindByPosition(position = 2)
    private String number;

    @CsvBindByPosition(position = 3)
    private BigDecimal amount;

    @CsvBindByPosition(position = 4)
    private Integer categoryId;
    @CsvBindByPosition(position = 5)
    private String category;

    @CsvBindByPosition(position = 6)
    private String memo;

    @CsvBindByPosition(position = 7)
    private Integer payeeId;
    @CsvBindByPosition(position = 8)
    private String payee;

    @CsvBindByPosition(position = 9)
    private int splitsCount;
    @CsvBindByPosition(position = 10)
    private String splits;
    @CsvBindByPosition(position = 11)
    private int splitParentId;

    @CsvBindByPosition(position = 12)
    private Integer accountId;

    @CsvBindByPosition(position = 13)
    private String account;

    @CsvBindByPosition(position = 14)
    private boolean voided;

    @CsvBindByPosition(position = 15)
    private boolean cleared;

    @CsvBindByPosition(position = 16)
    private boolean reconciled;

    @CsvBindByPosition(position = 17)
    private boolean unaccepted;

    @CsvBindByPosition(position = 18)
    private boolean transfer;

    @CsvBindByPosition(position = 19)
    private Integer xferAccountId;

    @CsvBindByPosition(position = 20)
    private Integer xferTransactionId;

    @CsvBindByPosition(position = 21)
    private boolean investment;

    @CsvBindByPosition(position = 22)
    @Convert(converter = InvestmentActivityConverter.class)
    private InvestmentActivity investmentActivity;

    @CsvBindByPosition(position = 23)
    private Integer investmentSecurityId;

    @CsvBindByPosition(position = 24)
    private String investmentSecurityName;

    @CsvBindByPosition(position = 25)
    private String investmentSecuritySymbol;

    @CsvBindByPosition(position = 26)
    private Double investmentTransactionPrice;

    @CsvBindByPosition(position = 27)
    private Double investmentTransactionQuantity;

    @CsvBindByPosition(position = 28)
    private boolean recurring;

    @CsvBindByPosition(position = 29)
    @Convert(converter = FrequencyConverter.class)
    private Frequency frequency;

    @CsvBindByPosition(position = 30)
    private Integer classification1Id;

    @CsvBindByPosition(position = 31)
    private String classification1Name;

    @CsvBindByPosition(position = 32)
    private String fiTid;

    public Csv2Transaction() {
        this(null);
    }

    public Csv2Transaction(Transaction value) {
        super(value);

        if (value == null) {
            return;
        }

        this.id = value.getId();

        this.date = value.getDate();

        this.number = value.getNumber();
        this.amount = value.getAmount();
        if (this.amount != null) {
            this.amount = BalanceUtils.formatBalance(this.amount);
        }
        CategoryLink category = value.getCategory();
        if (category != null) {
            this.categoryId = category.getId();
            this.category = category.getFullName();
            if (StringUtils.isEmpty(this.category)) {
                this.category = category.getName();
            }
        }

        this.memo = value.getMemo();

        PayeeLink payee = value.getPayee();
        if (payee != null) {
            this.payeeId = payee.getId();
            this.payee = payee.getName();
        }

        StringBuilder sb = new StringBuilder();
        int count = 0;
        List<TransactionSplit> splits = value.getSplits();
        if (splits != null) {
            splits = Csv2Transaction.sortByRowId(splits);
            for (TransactionSplit split : splits) {
                if (count > 0) {
                    sb.append("/");
                }
                sb.append(split.getRowId() + ":" + split.getTransaction().getId());
                count++;
            }
        }
        this.splitsCount = count;
        this.splits = sb.toString();
        if (this.splits.length() > 0) {
            if (LOGGER.isDebugEnabled()) {
                LOGGER.debug("splits={}", this.splits);
            }
        }
        this.splitParentId = -1;

        AccountLink account = value.getAccount();
        if (account != null) {
            this.accountId = account.getId();
            this.account = account.getName();
        }

        this.voided = value.isVoid();
        this.cleared = value.isCleared();
        this.reconciled = value.isReconciled();
        this.unaccepted = value.isUnaccepted();

        this.transfer = value.isTransfer();
        XferInfo xferInfo = value.getXferInfo();
        if (xferInfo != null) {
            this.xferAccountId = xferInfo.getXferAccountId();
            this.xferTransactionId = xferInfo.getXferTransactionId();
        }

        this.investment = value.isInvestment();
        InvestmentInfo investmentInfo = value.getInvestmentInfo();
        if (investmentInfo != null) {
            this.investmentActivity = investmentInfo.getActivity();
            Security security = investmentInfo.getSecurity();
            if (security != null) {
                this.investmentSecurityId = security.getId();
                this.investmentSecurityName = security.getName();
                this.investmentSecuritySymbol = security.getSymbol();
            }
            InvestmentTransaction investmentTransaction = investmentInfo.getTransaction();
            if (investmentTransaction != null) {
                this.investmentTransactionPrice = investmentTransaction.getPrice();
                this.investmentTransactionQuantity = investmentTransaction.getQuantity();
            }

        }

        this.recurring = value.isRecurring();
        this.frequency = value.getFrequency();

        Classification1Link classification1 = value.getClassification1();
        if (classification1 != null) {
            this.classification1Id = classification1.getId();
            this.classification1Name = classification1.getName();
        }

        this.fiTid = value.getFiTransactionId();
    }

    public String[] getHeaders() {
        String[] headers = HEADERS;
        return headers;
    }

    public Integer getId() {
        return id;
    }

    public Date getDate() {
        return date;
    }

    public String getNumber() {
        return number;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public Integer getCategoryId() {
        return categoryId;
    }

    public String getCategory() {
        return category;
    }

    public String getMemo() {
        return memo;
    }

    public Integer getPayeeId() {
        return payeeId;
    }

    public String getPayee() {
        return payee;
    }

    public String getSplits() {
        return splits;
    }

    public int getSplitsCount() {
        return splitsCount;
    }

    public int getSplitParentId() {
        return splitParentId;
    }

    public void setSplitParentId(int splitParentId) {
        this.splitParentId = splitParentId;
    }

    public Integer getAccountId() {
        return accountId;
    }

    public String getAccount() {
        return account;
    }

    public boolean isVoided() {
        return voided;
    }

    public boolean isCleared() {
        return cleared;
    }

    public boolean isReconciled() {
        return reconciled;
    }

    public boolean isUnaccepted() {
        return unaccepted;
    }

    public boolean isTransfer() {
        return transfer;
    }

    public Integer getXferAccountId() {
        return xferAccountId;
    }

    public Integer getXferTransactionId() {
        return xferTransactionId;
    }

    public boolean isInvestment() {
        return investment;
    }

    public InvestmentActivity getInvestmentActivity() {
        return investmentActivity;
    }

    public Integer getInvestmentSecurityId() {
        return investmentSecurityId;
    }

    public String getInvestmentSecurityName() {
        return investmentSecurityName;
    }

    public String getInvestmentSecuritySymbol() {
        return investmentSecuritySymbol;
    }

    public boolean isRecurring() {
        return recurring;
    }

    public Double getInvestmentTransactionPrice() {
        return investmentTransactionPrice;
    }

    public Double getInvestmentTransactionQuantity() {
        return investmentTransactionQuantity;
    }

    public Frequency getFrequency() {
        return frequency;
    }

    public Integer getClassification1Id() {
        return classification1Id;
    }

    public String getClassification1Name() {
        return classification1Name;
    }

    public String getFiTid() {
        return fiTid;
    }

    List<TransactionSplit> listOfSplits() {
        return value.getSplits();
    }

    static final List<TransactionSplit> sortByRowId(List<TransactionSplit> splits) {
        Comparator<TransactionSplit> c = new Comparator<TransactionSplit>() {
            @Override
            public int compare(TransactionSplit o1, TransactionSplit o2) {
                return o1.getRowId().compareTo(o2.getRowId());
            }
        };

        if (LOGGER.isDebugEnabled()) {
            LOGGER.info("SPLITS - Pre sorting, splits");
            for (TransactionSplit split : splits) {
                LOGGER.info("  split={}", split.getRowId());
            }
        }

        Collections.sort(splits, c);

        if (LOGGER.isDebugEnabled()) {
            LOGGER.info("SPLITS - Post sorting, splits");
            for (TransactionSplit split : splits) {
                LOGGER.info("  split={}", split.getRowId());
            }
        }

        return splits;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Csv2Transaction)) {
            return false;
        }
        Csv2Transaction transaction = (Csv2Transaction) o;
        return Objects.equals(getId(), transaction.getId());
    }

    @Override
    public int hashCode() {
        return getClass().hashCode();
    }
}

package com.hungle.sunriise.export.json;

import java.io.File;

import org.apache.logging.log4j.Logger;

import com.hungle.sunriise.export.mongodb.ExportToMongoDb;
import com.hungle.sunriise.export.mongodb.ExportToMongoDbCmd;
import com.hungle.sunriise.io.FileUtils;
import com.hungle.sunriise.prices.DbInfo;
import com.mongodb.MongoClientURI;

// TODO: Auto-generated Javadoc
/**
 * The Class ExportToJSONCmd.
 */
public class ExportToJSONCmd {

    /** The Constant LOGGER. */
    private static final Logger LOGGER = org.apache.logging.log4j.LogManager.getLogger(ExportToJSONCmd.class);

    /**
     * The main method.
     *
     * @param args the arguments
     */
    public static void main(String[] args) {
        File dbFile = null;

        String password = null;

        File outDir = null;
        MongoClientURI mongoClientURI = null;

        String arg = null;
        if (args.length == 2) {
            arg = args[0];
            DbInfo dbInfo = DbInfo.getDbFile(arg);
            dbFile = dbInfo.getDbFile();

            password = dbInfo.getPassword();

            arg = args[1];
            if (ExportToMongoDb.isMongoClientURI(arg)) {
                mongoClientURI = ExportToMongoDb.createMongoClientURI(arg);
            } else {
                outDir = new File(arg);
            }
        } else if (args.length == 3) {
            arg = args[0];
            DbInfo dbInfo = DbInfo.getDbFile(arg);
            dbFile = dbInfo.getDbFile();

            arg = args[1];
            password = arg;

            arg = args[2];
            if (ExportToMongoDb.isMongoClientURI(arg)) {
                mongoClientURI = ExportToMongoDb.createMongoClientURI(arg);
            } else {
                outDir = new File(arg);
            }
        } else {
            Class<ExportToJSONCmd> clz = ExportToJSONCmd.class;
            System.out.println("Usage: java " + clz.getName() + " in.mny [password] outDir|mongoClientURI");
            FileUtils.printArgs(args);
            System.exit(1);
        }

        if (dbFile == null) {
            LOGGER.error("Cannot export null dbFile");
            return;
        }

        if (mongoClientURI != null) {
            ExportToMongoDbCmd.export(dbFile, password, mongoClientURI);
        } else if (outDir != null) {
            ExportToJSONDirCmd.export(dbFile, password, outDir);
        } else {
            LOGGER.error("No valid exporter found for args=" + args);
        }
    }

}

package com.hungle.sunriise.export.json;

import java.io.File;
import java.io.IOException;

import org.apache.logging.log4j.Logger;

import com.fasterxml.jackson.module.jsonSchema.JsonSchema;
import com.fasterxml.jackson.module.jsonSchema.JsonSchemaGenerator;

// TODO: Auto-generated Javadoc
/**
 * The Class ExportJSONSchemaJackson.
 */
public class ExportJSONSchemaJackson extends AbstractExportJSONSchema {

    /** The Constant LOGGER. */
    private static final Logger LOGGER = org.apache.logging.log4j.LogManager.getLogger(ExportJSONSchemaJackson.class);

    /** The schema gen. */
    private JsonSchemaGenerator schemaGen;

    /**
     * Instantiates a new export JSON schema jackson.
     */
    public ExportJSONSchemaJackson() {
        super();
        this.schemaGen = new JsonSchemaGenerator(mapper);
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.hungle.sunriise.export.AbstractExportJSONSchema#generateJsonSchema(java.
     * lang.Class)
     */
    @Override
    protected Object generateJsonSchema(Class clz) throws IOException {
        JsonSchema schema = schemaGen.generateSchema(clz);
        return schema;
    }

    /**
     * The main method.
     *
     * @param args the arguments
     */
    public static void main(String[] args) {
        AbstractExportJSONSchema export = new ExportJSONSchemaJackson();
        // https://github.com/FasterXML/jackson-module-jsonSchema
        String pathname = "target/jsonSchema/v3";
        if (args.length > 0) {
            pathname = args[0];
        }
        File parentDir = new File(pathname);

        export.generateJsonSchemaToDir(parentDir);
    }

}

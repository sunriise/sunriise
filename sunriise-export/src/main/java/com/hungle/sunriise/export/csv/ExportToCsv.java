/*******************************************************************************
 * Copyright (c) 2010 Hung Le
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *******************************************************************************/
package com.hungle.sunriise.export.csv;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import java.util.Set;

import org.apache.logging.log4j.Logger;

import com.healthmarketscience.jackcess.Column;
import com.healthmarketscience.jackcess.Database;
import com.healthmarketscience.jackcess.Table;
import com.healthmarketscience.jackcess.impl.ColumnImpl;
import com.healthmarketscience.jackcess.query.Query;
import com.healthmarketscience.jackcess.util.ExportFilter;
import com.healthmarketscience.jackcess.util.ExportUtil;
import com.healthmarketscience.jackcess.util.SimpleExportFilter;
import com.hungle.sunriise.io.FileUtils;
import com.hungle.sunriise.io.MnyDb;
import com.hungle.sunriise.prices.DbInfo;
import com.hungle.sunriise.util.JackcessImplUtils;

// TODO: Auto-generated Javadoc
/**
 * The Class ExportToCsv.
 */
public class ExportToCsv {

    /** The Constant log. */
    private static final Logger LOGGER = org.apache.logging.log4j.LogManager.getLogger(ExportToCsv.class);

    /** The opened db. */
    private MnyDb mnyDb = null;

    /**
     * Write to dir.
     *
     * @param outDir the out dir
     * @throws IOException Signals that an I/O exception has occurred.
     */
    public void export(File outDir) throws IOException {
        PrintWriter writer = null;
        startExport(outDir);
        try {
            File file = new File(outDir, "db.txt");
            writer = new PrintWriter(new BufferedWriter(new FileWriter(file)));
            // writer.println("file=" + dbFile);
            // writer.println("fileFormat: " + db.getFileFormat());
            Database db = mnyDb.getDb();
            writer.println(db.toString());

            writer.println("");
            List<Query> queries = db.getQueries();
            writer.println("getQueries, " + queries.size());
            for (Query query : queries) {
                writer.println(query.toSQLString());
            }
            Set<String> tableNames = db.getTableNames();
            // writer.println("tableNames.size: " + tableNames.size());
            int count = 0;
            startExportTables(tableNames.size());
            try {
                for (String tableName : tableNames) {
                    try {
                        LOGGER.info("tableName=" + tableName);
                        if (!exportedTable(tableName, count)) {
                            break;
                        }
                        writeTableInfo(db, tableName, outDir);
                        count++;
                    } catch (IOException e) {
                        LOGGER.warn("Cannot write table info for tableName=" + tableName);
                    }
                }
            } finally {
                endExportTables(count);
            }
        } finally {
            if (writer != null) {
                writer.close();
            }
            writer = null;
            endExport(outDir);
        }
    }

    /**
     * Start export.
     *
     * @param outDir the out dir
     */
    protected void startExport(File outDir) {
    }

    /**
     * End export.
     *
     * @param outDir the out dir
     */
    protected void endExport(File outDir) {
    }

    /**
     * Start export tables.
     *
     * @param size the size
     */
    protected void startExportTables(int size) {
    }

    /**
     * Exported table.
     *
     * @param tableName the table name
     * @param count     the count
     * @return true, if successful
     */
    protected boolean exportedTable(String tableName, int count) {
        return true;
    }

    /**
     * End export tables.
     *
     * @param count the count
     */
    protected void endExportTables(int count) {
    }

    /**
     * Write table info.
     *
     * @param db        the db
     * @param tableName the table name
     * @param outDir    the out dir
     * @throws IOException Signals that an I/O exception has occurred.
     */
    private static void writeTableInfo(Database db, String tableName, File outDir) throws IOException {
        File dir = new File(outDir, tableName);
        if ((!dir.exists()) && (!dir.mkdirs())) {
            throw new IOException("Cannot create directory, dir=" + dir);
        }

        Table table = db.getTable(tableName);

        File file = new File(dir, "table.txt");
        PrintWriter writer = null;
        try {
            writer = new PrintWriter(new BufferedWriter(new FileWriter(file)));
            writer.println(table.toString());
        } finally {
            if (writer != null) {
                writer.close();
                writer = null;
            }
        }

        writeColumnsInfo(table, dir);

        writeRowsInfo(db, table, dir);
    }

    /**
     * Write columns info.
     *
     * @param table the table
     * @param dir   the dir
     * @throws IOException Signals that an I/O exception has occurred.
     */
    // TODO
    @SuppressWarnings("unused")
    private static void writeColumnsInfo(Table table, File dir) throws IOException {
        // Table table = db.getTable(tableName);
        File columnsFile = null;
        // columnsFile = new File(dir, "columns.txt");
        if (columnsFile != null) {
            PrintWriter writer = null;
            try {
                writer = new PrintWriter(new BufferedWriter(new FileWriter(columnsFile)));
                writer.println("#name: " + table.getName());
                writer.println("#columns: " + table.getColumnCount());
                writer.println("");
                List<ColumnImpl> columns = JackcessImplUtils.getColumns(table);
                for (Column column : columns) {
                    writer.println(column.getName() + "," + column.getType().toString());
                }
            } finally {
                if (writer != null) {
                    writer.close();
                    writer = null;
                }
            }
        }

        File columnsDir = new File(dir, "columns.d");
        if ((!columnsDir.exists()) && (!columnsDir.mkdirs())) {
            throw new IOException("Cannot create directory, dir=" + columnsDir);
        }
        List<ColumnImpl> columns = JackcessImplUtils.getColumns(table);
        for (Column column : columns) {
            writeColumnsInfo(columnsDir, column);
        }
    }

    /**
     * Write columns info.
     *
     * @param columnsDir the columns dir
     * @param column     the column
     * @throws IOException Signals that an I/O exception has occurred.
     */
    private static void writeColumnsInfo(File columnsDir, Column column) throws IOException {
        File file = new File(columnsDir, column.getName() + ".txt");
        PrintWriter writer = null;
        try {
            writer = new PrintWriter(new BufferedWriter(new FileWriter(file)));
            writer.println(column.toString());
        } finally {
            if (writer != null) {
                writer.close();
                writer = null;
            }
        }
    }

    /**
     * Write rows info.
     *
     * @param db    the db
     * @param table the table
     * @param dir   the dir
     * @throws IOException Signals that an I/O exception has occurred.
     */
    private static void writeRowsInfo(Database db, Table table, File dir) throws IOException {
        File file = new File(dir, table.getName() + "-" + "rows.csv");
        BufferedWriter writer = null;
        try {
            writer = new BufferedWriter(new FileWriter(file));
            ExportFilter filter = SimpleExportFilter.INSTANCE;
            ExportUtil.exportWriter(db, table.getName(), writer, true, ExportUtil.DEFAULT_DELIMITER,
                    ExportUtil.DEFAULT_QUOTE_CHAR, filter);
        } finally {
            if (writer != null) {
                writer.close();
                writer = null;
            }
        }

    }

    /**
     * Close.
     */
    private void close() {
        if (mnyDb != null) {
            try {
                mnyDb.close();
            } catch (IOException e) {
                LOGGER.error(e);
            } finally {
                mnyDb = null;
            }
        }
    }

    /**
     * Gets the opened db.
     *
     * @return the opened db
     */
    public MnyDb getMnyDb() {
        return mnyDb;
    }

    /**
     * Sets the opened db.
     *
     * @param mnyDb the new opened db
     */
    public void setMnyDb(MnyDb mnyDb) {
        this.mnyDb = mnyDb;
    }

    /**
     * The main method.
     *
     * @param args the arguments
     */
    public static void main(String[] args) {
        File dbFile = null;

        String password = null;

        File outDir = null;

        String arg = null;
        if (args.length == 2) {
            arg = args[0];
            DbInfo dbInfo = DbInfo.getDbFile(arg);
            dbFile = dbInfo.getDbFile();

            password = dbInfo.getPassword();

            arg = args[1];
            outDir = new File(arg);
        } else if (args.length == 3) {
            arg = args[0];
            DbInfo dbInfo = DbInfo.getDbFile(arg);
            dbFile = dbInfo.getDbFile();

            arg = args[1];
            password = arg;

            arg = args[2];
            outDir = new File(arg);
        } else {
            Class<ExportToCsv> clz = ExportToCsv.class;
            System.out.println("Usage: " + clz.getName() + " sample.mny [password] outDir");
            FileUtils.printArgs(args);
            System.exit(1);
        }

        LOGGER.info("dbFile=" + dbFile);
        LOGGER.info("outDir=" + outDir);

        ExportToCsv exportTo = null;
        try {
            if (!dbFile.exists()) {
                throw new IOException("File=" + dbFile.getAbsoluteFile().getAbsolutePath() + " does not exist.");
            }
            exportTo = new ExportToCsv();
            exportTo.setMnyDb(new MnyDb(dbFile, password));
            if ((!outDir.exists()) && (!outDir.mkdirs())) {
                throw new IOException("Cannot create directory, outDir=" + outDir);
            }
            exportTo.export(outDir);
        } catch (IllegalStateException e) {
            // java.lang.IllegalStateException: Incorrect password provided
            LOGGER.error(e);
        } catch (IOException e) {
            LOGGER.error(e);
        } finally {
            if (exportTo != null) {
                exportTo.close();
            }
        }
        LOGGER.info("< DONE");
    }
}

package com.hungle.sunriise.export.ofx;

import java.util.Date;

public class AbstractStatementTransaction {

    private String type;
    private Date dtPostedDate;
    private Double amount;
    private String id;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Date getDtPostedDate() {
        return dtPostedDate;
    }

    public void setDtPostedDate(Date dtPostedDate) {
        this.dtPostedDate = dtPostedDate;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

}

package com.hungle.sunriise.export.jpa;

import java.io.IOException;

import com.healthmarketscience.jackcess.Database;
import com.hungle.sunriise.export.AbstractExportTo;
import com.hungle.sunriise.io.MnyDb;
import com.hungle.sunriise.mnyobject.Account;

// TODO: Auto-generated Javadoc
/**
 * The Class AbstractCrudRepositoryExporter.
 */
public abstract class AbstractCrudRepositoryExporter extends AbstractExportTo {

    /**
     * Instantiates a new abstract crud repository exporter.
     */
    public AbstractCrudRepositoryExporter() {
        super();
    }

    /**
     * Export.
     *
     * @param mnyDb the mny db
     * @return the database
     * @throws IOException Signals that an I/O exception has occurred.
     */
    @Override
    public Database export(MnyDb mnyDb) throws IOException {
        if (mnyDb == null) {
            return null;
        }
        if (mnyDb.getDb() == null) {
            return null;
        }

        this.startExport(mnyDb);

        try {
            _visit(mnyDb);
        } finally {
            this.endExport(mnyDb);
        }

        return mnyDb.getDb();
    }

    /**
     * Start export.
     *
     * @param mnyDb the mny db
     */
    protected void startExport(MnyDb mnyDb) {
    }

    /**
     * End export.
     *
     * @param mnyDb the mny db
     */
    protected void endExport(MnyDb mnyDb) {
    }

    /**
     * Export account names.
     *
     * @throws IOException Signals that an I/O exception has occurred.
     */
    @Override
    protected void exportAccountNames() throws IOException {
    }

    /**
     * Export securities.
     *
     * @throws IOException Signals that an I/O exception has occurred.
     */
    @Override
    protected void exportSecurities() throws IOException {
    }

    /**
     * Export currencies.
     *
     * @throws IOException Signals that an I/O exception has occurred.
     */
    @Override
    protected void exportCurrencies() throws IOException {
    }

    /**
     * Export payees.
     *
     * @throws IOException Signals that an I/O exception has occurred.
     */
    @Override
    protected void exportPayees() throws IOException {
    }

    /**
     * Export categories.
     *
     * @throws IOException Signals that an I/O exception has occurred.
     */
    @Override
    protected void exportCategories() throws IOException {
    }

    /**
     * Export account filtered transactions.
     *
     * @param account the account
     * @throws IOException Signals that an I/O exception has occurred.
     */
    @Override
    protected void exportAccountFilteredTransactions(Account account) throws IOException {
    }

    /**
     * Export account transactions.
     *
     * @param account the account
     * @throws IOException Signals that an I/O exception has occurred.
     */
    @Override
    protected void exportAccountTransactions(Account account) throws IOException {
    }

    /**
     * Export account detail.
     *
     * @param account the account
     * @throws IOException Signals that an I/O exception has occurred.
     */
    @Override
    protected void exportAccountDetail(Account account) throws IOException {
        // TODO Auto-generated method stub

    }
}
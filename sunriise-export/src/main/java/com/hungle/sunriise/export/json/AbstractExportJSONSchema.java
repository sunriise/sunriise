package com.hungle.sunriise.export.json;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

import org.apache.logging.log4j.Logger;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.hungle.sunriise.json.JSONUtils;
import com.hungle.sunriise.mnyobject.Account;
import com.hungle.sunriise.mnyobject.Bill;
import com.hungle.sunriise.mnyobject.Category;
import com.hungle.sunriise.mnyobject.Currency;
import com.hungle.sunriise.mnyobject.EnumAccountType;
import com.hungle.sunriise.mnyobject.EnumInvestmentSubType;
import com.hungle.sunriise.mnyobject.EnumTransactionState;
import com.hungle.sunriise.mnyobject.Frequency;
import com.hungle.sunriise.mnyobject.InvestmentInfo;
import com.hungle.sunriise.mnyobject.InvestmentTransaction;
import com.hungle.sunriise.mnyobject.Payee;
import com.hungle.sunriise.mnyobject.Security;
import com.hungle.sunriise.mnyobject.SecurityHolding;
import com.hungle.sunriise.mnyobject.Transaction;
import com.hungle.sunriise.mnyobject.TransactionInfo;
import com.hungle.sunriise.mnyobject.TransactionSplit;
import com.hungle.sunriise.mnyobject.TransactionXfer;
import com.hungle.sunriise.mnyobject.XferInfo;

// TODO: Auto-generated Javadoc
/**
 * The Class AbstractExportJSONSchema.
 */
public abstract class AbstractExportJSONSchema {

    /** The Constant LOGGER. */
    private static final Logger LOGGER = org.apache.logging.log4j.LogManager.getLogger(AbstractExportJSONSchema.class);

    /** The Constant MNY_OBJECT_CLASSES. */
    protected static final Class[] MNY_OBJECT_CLASSES = { Account.class, Bill.class, Category.class, Currency.class,
            EnumAccountType.class, EnumInvestmentSubType.class, EnumTransactionState.class, Frequency.class,
            InvestmentInfo.class, InvestmentTransaction.class,
            // MnyContext.class,
//            MnyCurrency.class, 
//            MnyObject.class, 
            Payee.class, SecurityHolding.class, Security.class,
//            TransactionFilter.class,
            TransactionInfo.class, Transaction.class, TransactionSplit.class, TransactionXfer.class, XferInfo.class, };

    /** The mapper. */
    protected final ObjectMapper mapper;

    /**
     * Instantiates a new abstract export JSON schema.
     */
    public AbstractExportJSONSchema() {
        super();
        mapper = JSONUtils.createDefaultObjectMapper();
    }

    /**
     * Generate json schema.
     *
     * @param clz the clz
     * @return the object
     * @throws IOException Signals that an I/O exception has occurred.
     */
    protected abstract Object generateJsonSchema(Class clz) throws IOException;

    /**
     * Generate json schema to dir.
     *
     * @param parentDir the parent dir
     */
    protected void generateJsonSchemaToDir(File parentDir) {
        boolean prettyPrint = true;
        for (Class clz : MNY_OBJECT_CLASSES) {
            PrintWriter writer = null;
            try {
                LOGGER.info("clz=" + clz);

                parentDir.mkdirs();
                File file = new File(parentDir, clz.getSimpleName() + ".json");
                LOGGER.info("  out=" + file.getAbsolutePath());
                writer = new PrintWriter(new FileWriter(file));

                Object jsonSchema = generateJsonSchema(clz);

                JSONUtils.writeValue(jsonSchema, writer, prettyPrint, mapper);
            } catch (IOException e) {
                LOGGER.error(e);
            } finally {
                if (writer != null) {
                    writer.close();
                    writer = null;
                }
            }
        }
    }

}
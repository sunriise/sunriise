package com.hungle.sunriise.export.qif;

import java.io.PrintWriter;
import java.util.Collection;

import com.hungle.sunriise.mnyobject.Category;
import com.hungle.sunriise.mnyobject.MnyContext;

// TODO: Auto-generated Javadoc
/**
 * The Class CategoryWrapper.
 */
public class CategoryMapper {

    /** The category. */
    private final Category category;

    /** The Constant QIF_HEADER_CATEGORY. */
    // !Type:Cat Category list
    private static final String QIF_HEADER_CATEGORY = "!Type:Cat";

    /**
     * Instantiates a new category wrapper.
     *
     * @param category the category
     */
    public CategoryMapper(Category category) {
        this.category = category;
    }

    /**
     * Serialize.
     *
     * @param writer the writer
     */
    public void writeValue(PrintWriter writer) {
        CategoryMapper.writeCategory(category, writer);
    }

    static void writeCategories(MnyContext mnyContext, Collection<Category> categories, PrintWriter writer) {
        writer.println(QIF_HEADER_CATEGORY);
        for (Category category : categories) {
            CategoryMapper mapper = new CategoryMapper(category);
            mapper.writeValue(writer);
        }
    }

    /**
     * Write category block.
     *
     * @param category the category
     * @param writer   the writer
     */
    public static final void writeCategory(Category category, PrintWriter writer) {
        // NDiv Income
        // DDividend Income
        // T
        // R286
        // I

        /*--
        Items for a Category List
        
        Field   Indicator Explanation
        N   Category name:subcategory name
        D   Description
        T   Tax related if included, not tax related if omitted
        I   Income category
        E   Expense category (if category type is unspecified, quicken assumes expense type)
        B   Budget amount (only in a Budget Amounts QIF file)
        R   Tax schedule information
        ^   End of entry
         */

        String fullName = category.getFullName();
        if (fullName == null) {
            return;
        }
        try {
            writer.println("N" + fullName);

            if (category.getExpense()) {
                writer.println("E");
            } else {
                writer.println("I");
            }
        } finally {
            writer.println("^");
        }
    }

}

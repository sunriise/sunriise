package com.hungle.sunriise.export.jpa;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

import org.apache.logging.log4j.Logger;

import com.hungle.sunriise.mnyobject.Frequency;
import com.hungle.sunriise.mnyobject.impl.DefaultFrequency;

// TODO: Auto-generated Javadoc
/**
 * The Class FrequencyConverter.
 */
@Converter
public class FrequencyConverter implements AttributeConverter<Frequency, String> {

    /** The Constant LOGGER. */
    private static final Logger LOGGER = org.apache.logging.log4j.LogManager.getLogger(FrequencyConverter.class);

    /** The Constant SEPARATOR. */
    private static final String SEPARATOR = ", ";

    /**
     * Convert to database column.
     *
     * @param frequency the frequency
     * @return the string
     */
    @Override
    public String convertToDatabaseColumn(Frequency frequency) {
        if (frequency == null) {
            return null;
        }

        StringBuilder sb = new StringBuilder();
        Integer type = frequency.getType();
        Double frqInst = frequency.getcFrqInst();

        if (type == null) {
            type = Integer.valueOf(0);
        }

        if (frqInst == null) {
            frqInst = Double.valueOf(0.00);
        }

        sb.append(type.toString());
        sb.append(SEPARATOR);
        sb.append(frqInst.toString());

        return sb.toString();
    }

    /**
     * Convert to entity attribute.
     *
     * @param dbFrequency the db frequency
     * @return the frequency
     */
    @Override
    public Frequency convertToEntityAttribute(String dbFrequency) {
        if (dbFrequency == null || dbFrequency.isEmpty()) {
            return null;
        }

        String[] pieces = dbFrequency.split(SEPARATOR);

        if (pieces == null || pieces.length == 0) {
            return null;
        }

        Integer type = Integer.valueOf(0);
        try {
            type = Integer.valueOf(pieces[0].trim());
        } catch (NumberFormatException e) {
            type = Integer.valueOf(0);
            LOGGER.warn(e);
        }

        Double frqInst = Double.valueOf(0.0);
        try {
            frqInst = Double.valueOf(pieces[0].trim());
        } catch (NumberFormatException e) {
            frqInst = Double.valueOf(0.0);
            LOGGER.warn(e);
        }

        Frequency frequency = new DefaultFrequency(type, frqInst);

        return frequency;
    }

}

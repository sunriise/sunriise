package com.hungle.sunriise.export.cvs2.model;

import java.math.BigDecimal;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.apache.logging.log4j.Logger;

import com.hungle.sunriise.export.cvs2.AbstractCsv2Wrapper;
import com.hungle.sunriise.mnyobject.Account;
import com.hungle.sunriise.mnyobject.Currency;
import com.hungle.sunriise.mnyobject.EnumAccountType;
import com.hungle.sunriise.mnyobject.EnumInvestmentSubType;
import com.hungle.sunriise.util.BalanceUtils;
import com.opencsv.bean.CsvBindByPosition;

@Entity
@Table(name = "Account")
public class Csv2Account extends AbstractCsv2Wrapper<Account> {
    private static final Logger LOGGER = org.apache.logging.log4j.LogManager.getLogger(Csv2Account.class);

    private static final String[] HEADERS = { "id", "name", "accountType", "currency", "startingBalance",
            "relatedToAccountId", "relatedToAccountName", "closed", "retirement", "investmentSubType", };

    @Id
    @CsvBindByPosition(position = 0)
    @Column(name = "id")
    private Integer id;

    @CsvBindByPosition(position = 1)
    @Column(name = "name")
    private String name;

    @CsvBindByPosition(position = 2)
    @Column(name = "accountType")
    private EnumAccountType accountType;

    @CsvBindByPosition(position = 3)
    @Column(name = "currency")
    private String currencyIsoCode;

    @CsvBindByPosition(position = 4)
    @Column(name = "startingBalance")
    private BigDecimal startingBalance;

    @CsvBindByPosition(position = 5)
    @Column(name = "relatedToAccountId")
    private Integer relatedToAccountId;

    @CsvBindByPosition(position = 6)
    @Column(name = "relatedToAccountName")
    private String relatedToAccountName;

    @CsvBindByPosition(position = 7)
    @Column(name = "closed")
    private Boolean closed;

    @CsvBindByPosition(position = 8)
    @Column(name = "retirement")
    private Boolean retirement;

    @CsvBindByPosition(position = 9)
    @Column(name = "investmentSubType")
    private EnumInvestmentSubType investmentSubType;

    public String[] getHeaders() {
        String[] headers = HEADERS;
        return headers;
    }

    public Csv2Account() {
        this(null);
    }

    public Csv2Account(Account value) {
        super(value);

        if (value == null) {
            return;
        }

        this.id = value.getId();

        this.name = value.getName();

        this.accountType = value.getAccountType();

        Currency currency = value.getCurrency();
        if (currency != null) {
            this.currencyIsoCode = currency.getIsoCode();
        }

        this.startingBalance = value.getStartingBalance();
        if (this.startingBalance != null) {
            this.startingBalance = BalanceUtils.formatBalance(this.startingBalance);
        }

        Account relatedToAccount = value.getRelatedToAccount();
        if (relatedToAccount != null) {
            this.relatedToAccountId = relatedToAccount.getId();
            this.relatedToAccountName = relatedToAccount.getName();
        }

        this.closed = value.getClosed();
        this.retirement = value.getRetirement();
        this.investmentSubType = value.getInvestmentSubType();

    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public EnumAccountType getAccountType() {
        return accountType;
    }

    public void setAccountType(EnumAccountType accountType) {
        this.accountType = accountType;
    }

    public String getCurrencyIsoCode() {
        return currencyIsoCode;
    }

    public void setCurrencyIsoCode(String currencyIsoCode) {
        this.currencyIsoCode = currencyIsoCode;
    }

    public BigDecimal getStartingBalance() {
        return startingBalance;
    }

    public Integer getRelatedToAccountId() {
        return relatedToAccountId;
    }

    public String getRelatedToAccountName() {
        return relatedToAccountName;
    }

    public Boolean getClosed() {
        return closed;
    }

    public Boolean getRetirement() {
        return retirement;
    }

    public EnumInvestmentSubType getInvestmentSubType() {
        return investmentSubType;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Csv2Account)) {
            return false;
        }
        Csv2Account account = (Csv2Account) o;
        return Objects.equals(getId(), account.getId());
    }

    @Override
    public int hashCode() {
        return getClass().hashCode();
    }
}

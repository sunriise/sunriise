package com.hungle.sunriise.export.mdb;

import java.awt.Container;
import java.awt.Dimension;
import java.awt.HeadlessException;
import java.io.File;
import java.io.IOException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

import org.apache.logging.log4j.Logger;

import com.hungle.sunriise.gui.FileDropHandler;

public class ExportToMdbView extends JFrame {
    private static final Logger LOGGER = org.apache.logging.log4j.LogManager.getLogger(ExportToMdbView.class);

    private ExecutorService threadPool = Executors.newCachedThreadPool();

    public ExportToMdbView() throws HeadlessException {
        super();
        Dimension preferredSize = new Dimension(400, 400);
        setPreferredSize(preferredSize);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        Container pane = getContentPane();
        createMainView(pane);

    }

    private void createMainView(Container pane) {
        JPanel view = new JPanel();
        pane.add(view);

        view.setTransferHandler(new FileDropHandler() {
            @Override
            public void handleFile(File file) {
                exportToMdb(file);
            }
        });
    }

    protected void exportToMdb(File srcFile) {
        String name = srcFile.getName();
        int index = name.lastIndexOf('.');
        if (index < 0) {
            LOGGER.error("No suffix");
            return;
        }
        String prefix = name.substring(0, index);
        String suffix = name.substring(index + 1);
        if (suffix.compareToIgnoreCase("mny") != 0) {
            LOGGER.warn("Cannot export non .mny file.");
            return;
        }

        Runnable command = new Runnable() {
            @Override
            public void run() {
                try {
                    LOGGER.info("> EXPORTING START");
                    ExportToMdb exporter = new ExportToMdb() {

                        @Override
                        protected void startCopyTables(int count) {
                            LOGGER.info("> startCopyTables, count=" + count);
                        }

                        @Override
                        protected void endCopyTables(int count) {
                            LOGGER.info("> endCopyTables, count=" + count);
                        }
                    };

                    String srcPassword = null;
                    File destFile = getDestFile(srcFile, prefix, suffix);
                    LOGGER.info("EXPORTING -" + " srcFile=" + srcFile.getAbsolutePath() + ", destFile="
                            + destFile.getAbsolutePath());
                    exporter.export(srcFile, srcPassword, destFile);
                } catch (

                IOException e) {
                    LOGGER.error(e, e);
                } finally {
                    LOGGER.info("< EXPORTING DONE");
                }
            }
        };
        threadPool.execute(command);
    }

    private File getDestFile(File srcFile, String prefix, String suffix) {
        File destFile = null;

        File parentFile = srcFile.getAbsoluteFile().getParentFile();

        String fileName = prefix + "." + "mdb";

        destFile = new File(parentFile, fileName);

        return destFile;
    }

    public static void main(String[] args) {
        final ExportToMdbView view = new ExportToMdbView();
        Runnable doRun = new Runnable() {

            @Override
            public void run() {
                view.showMainFrame();
            }
        };
        SwingUtilities.invokeLater(doRun);
    }

    private void showMainFrame() {
        setLocationRelativeTo(null);
        pack();
        setVisible(true);
    }

}

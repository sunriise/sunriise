package com.hungle.sunriise.export.cvs2.model;

import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.apache.logging.log4j.Logger;

import com.hungle.sunriise.export.cvs2.AbstractCsv2Wrapper;
import com.hungle.sunriise.mnyobject.Category;
import com.opencsv.bean.CsvBindByPosition;

@Entity
@Table(name = "Category")
public class Csv2Category extends AbstractCsv2Wrapper<Category> {
    private static final Logger LOGGER = org.apache.logging.log4j.LogManager.getLogger(Csv2Category.class);

    private static final String[] HEADERS = { "id", "name", "parentId", "level", "classificationId" };

    @Id
    @CsvBindByPosition(position = 0)
    @Column(name = "id")
    private Integer id;

    @CsvBindByPosition(position = 1)
    @Column(name = "name")
    private String name;

    @CsvBindByPosition(position = 2)
    @Column(name = "parentId")
    private Integer parentId;

    @CsvBindByPosition(position = 3)
    @Column(name = "level")
    private Integer level;

    @CsvBindByPosition(position = 4)
    @Column(name = "classificationId")
    private Integer classificationId;

    public String[] getHeaders() {
        String[] headers = HEADERS;
        return headers;
    }

    public Csv2Category() {
        this(null);
    }

    public Csv2Category(Category value) {
        super(value);

        if (value == null) {
            return;
        }

        this.id = value.getId();
        this.name = value.getName();

        this.parentId = value.getParentId();
        this.level = value.getLevel();
        this.classificationId = value.getClassificationId();

    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getParentId() {
        return parentId;
    }

    public Integer getLevel() {
        return level;
    }

    public Integer getClassificationId() {
        return classificationId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Csv2Category)) {
            return false;
        }
        Csv2Category category = (Csv2Category) o;
        return Objects.equals(getId(), category.getId());
    }

    @Override
    public int hashCode() {
        return getClass().hashCode();
    }
}

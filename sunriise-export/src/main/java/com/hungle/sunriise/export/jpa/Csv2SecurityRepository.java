package com.hungle.sunriise.export.jpa;

import org.springframework.data.jpa.repository.JpaRepository;

import com.hungle.sunriise.export.cvs2.model.Csv2Security;

/**
 * The Interface Csv2SecurityRepository.
 */
public interface Csv2SecurityRepository extends JpaRepository<Csv2Security, Integer> {

}

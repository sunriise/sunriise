package com.hungle.sunriise.export;

import java.io.File;
import java.io.IOException;

import org.apache.logging.log4j.Logger;

import com.healthmarketscience.jackcess.Database;
import com.hungle.sunriise.export.ui.ExportToDirExporter;
import com.hungle.sunriise.io.MnyDb;
import com.hungle.sunriise.mnyobject.Account;
import com.hungle.sunriise.util.StopWatch;

// TODO: Auto-generated Javadoc
/**
 * The Class AbstractExportToJSON.
 */
public abstract class AbstractExportToDir extends AbstractExportTo implements ExportToDirExporter {

    /** The Constant LOGGER. */
    private static final Logger LOGGER = org.apache.logging.log4j.LogManager.getLogger(AbstractExportToDir.class);

    /** The out dir. */
    private File outDir;
    /** The stop watch. */
    private StopWatch stopWatch = new StopWatch();
    /** The account dir. */
    private File accountDir;

    /**
     * Instantiates a new abstract export to JSON.
     */
    public AbstractExportToDir() {
        super();
    }

    /**
     * Gets the out dir.
     *
     * @return the out dir
     */
    public File getOutDir() {
        return outDir;
    }

    /**
     * Sets the out dir.
     *
     * @param outDir the new out dir
     */
    public void setOutDir(File outDir) {
        this.outDir = outDir;
    }

    @Override
    protected Database export(MnyDb mnyDb) throws IOException {
        if (outDir == null) {
            throw new IOException("outDir=null");
        }
        return exportToDir(mnyDb, outDir);
    }

    /**
     * Export.
     *
     * @param mnyDb  the src db
     * @param outDir the out dir
     * @return the database
     * @throws IOException Signals that an I/O exception has occurred.
     */
    protected Database exportToDir(MnyDb mnyDb, File outDir) throws IOException {
        if (mnyDb == null) {
            return null;
        }
        if (mnyDb.getDb() == null) {
            return null;
        }

        startExportToDir(outDir);
        try {
            _visit(mnyDb);
        } finally {
            endExportToDir(outDir);
        }

        return mnyDb.getDb();
    }

    /**
     * Start export.
     *
     * @param outDir the out dir
     */
    protected void startExportToDir(File outDir) {
        stopWatch.click();
        LOGGER.info("> startExport, outDir=" + outDir);
        setOutDir(outDir);
    }

    /**
     * End export.
     *
     * @param outDir the out dir
     */
    protected void endExportToDir(File outDir) {
        stopWatch.logTiming(LOGGER, "> endExport, outDir=" + outDir);
    }

    @Override
    protected void exportCategories() throws IOException {
        final File dir = getOutDir();
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("> 000 exportCategories dir={}", dir.getAbsolutePath());
        }
        exportCategoriesToDir(dir);
    }

    protected abstract void exportCategoriesToDir(File outDir) throws IOException;

    @Override
    protected void exportPayees() throws IOException {
        final File dir = getOutDir();
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("> 111 exportPayees dir={}", dir.getAbsolutePath());
        }
        exportPayeesToDir(dir);
    }

    protected abstract void exportPayeesToDir(File outDir) throws IOException;

    @Override
    protected void exportCurrencies() throws IOException {
        final File dir = getOutDir();
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("> 222 exportCurrencies dir={}", dir.getAbsolutePath());
        }
        exportCurrenciesToDir(dir);
    }

    protected abstract void exportCurrenciesToDir(File outDir) throws IOException;

    @Override
    protected void exportSecurities() throws IOException {
        final File dir = getOutDir();
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("> 333 exportSecurities dir={}", dir.getAbsolutePath());
        }
        exportSecuritiesToDir(dir);
    }

    protected abstract void exportSecuritiesToDir(File outDir) throws IOException;

    @Override
    protected void exportAccountNames() throws IOException {
        final File dir = getOutDir();
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("> 444 exportAccountNames dir={}", dir.getAbsolutePath());
        }
        exportAccountNamesToDir(dir);
    }

    protected abstract void exportAccountNamesToDir(File outDir) throws IOException;

    @Override
    protected void exportAccount(Account account) throws IOException {
        final File dir = createAccountDir(account);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("> 555 exportAccount dir={}", dir);
        }
        this.setAccountDir(dir);
        super.exportAccount(account);
    }

    @Override
    protected void exportAccountDetail(Account account) throws IOException {
        final File dir = getAccountDir();
        if (dir != null) {
            if (LOGGER.isDebugEnabled()) {
                LOGGER.debug("> 666 exportAccountDetail dir={}", dir);
            }
            exportAccountDetailToDir(account, dir);
        }
    }

    protected abstract void exportAccountDetailToDir(Account account, File accountDir) throws IOException;

    @Override
    protected void exportAccountTransactions(Account account) throws IOException {
        final File dir = getAccountDir();
        if (dir != null) {
            if (LOGGER.isDebugEnabled()) {
                LOGGER.debug("> 777 exportAccountTransactions dir={}", dir);
            }
            exportTransactionsToDir(account, dir);
        }
    }

    protected abstract void exportTransactionsToDir(Account account, File accountDir) throws IOException;

    @Override
    protected void exportAccountFilteredTransactions(Account account) throws IOException {
        final File dir = getAccountDir();
        if (dir != null) {
            if (LOGGER.isDebugEnabled()) {
                LOGGER.debug("> 888 exportAccountFilteredTransactions dir={}", dir);
            }
            exportFilteredTransactionsToDir(account, dir);
        }
    }

    protected abstract void exportFilteredTransactionsToDir(Account account, File accountDir) throws IOException;

    /**
     * Gets the account dir.
     *
     * @return the account dir
     */
    public File getAccountDir() {
        return accountDir;
    }

    /**
     * Sets the account dir.
     *
     * @param accountDir the new account dir
     */
    public void setAccountDir(File accountDir) {
        this.accountDir = accountDir;
    }

    /**
     * Creates the account dir.
     *
     * @param account the account
     * @return the file
     */
    protected File createAccountDir(Account account) {
        File accountDir;
        // String suffix = ".d";
        String suffix = null;

        String accountName = account.getName();
        // String fileName = accountName;
        String fileName = toSafeFileName(accountName);
        if (suffix != null) {
            fileName = fileName + suffix;
        }

        File d = new File(outDir, "accounts");
        accountDir = new File(d, fileName);
        accountDir.mkdirs();
        return accountDir;
    }
}
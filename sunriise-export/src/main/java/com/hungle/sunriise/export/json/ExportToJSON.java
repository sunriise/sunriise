/*******************************************************************************
 * Copyright (c) 2013 Hung Le
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *******************************************************************************/
package com.hungle.sunriise.export.json;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.Logger;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.hungle.sunriise.export.AbstractExportToDir;
import com.hungle.sunriise.json.JSONUtils;
import com.hungle.sunriise.mnyobject.Account;
import com.hungle.sunriise.mnyobject.Category;
import com.hungle.sunriise.mnyobject.Currency;
import com.hungle.sunriise.mnyobject.Payee;
import com.hungle.sunriise.mnyobject.Security;

// TODO: Auto-generated Javadoc
/**
 * The Class ExportToJSON.
 */
public class ExportToJSON extends AbstractExportToDir {

    /** The Constant LOGGER. */
    private static final Logger LOGGER = org.apache.logging.log4j.LogManager.getLogger(ExportToJSON.class);

    /** The Constant FILENAME_ACCOUNTS. */
    private static final String FILENAME_ACCOUNTS = "accounts.json";

    /** The Constant FILENAME_ACCOUNT. */
    private static final String FILENAME_ACCOUNT = "account.json";

    /** The Constant FILENAME_TRANSACTIONS. */
    private static final String FILENAME_TRANSACTIONS = "transactions.json";

    /** The Constant FILENAME_FILTERED_TRANSACTIONS. */
    private static final String FILENAME_FILTERED_TRANSACTIONS = "filteredTransactions.json";

    /** The Constant FILENAME_SECURITIES. */
    private static final String FILENAME_SECURITIES = "securities.json";

    /** The Constant FILENAME_CURRENCIES. */
    private static final String FILENAME_CURRENCIES = "currencies.json";

    /** The Constant FILENAME_PAYEES. */
    private static final String FILENAME_PAYEES = "payees.json";

    /** The Constant FILENAME_CATEGORIES. */
    private static final String FILENAME_CATEGORIES = "categories.json";

    /**
     * Export account names to dir.
     *
     * @param outDir the out dir
     * @throws IOException Signals that an I/O exception has occurred.
     */
    protected void exportAccountNamesToDir(File outDir) throws IOException {
        Collection<AccountEntry> accountNames = createAccountNamesList();
        File outFile = new File(outDir, ExportToJSON.FILENAME_ACCOUNTS);
        JSONUtils.writeValue(accountNames, outFile);
    }

    /**
     * Export securities to dir.
     *
     * @param outDir the out dir
     * @throws IOException Signals that an I/O exception has occurred.
     */
    protected void exportSecuritiesToDir(File outDir) throws IOException {
        File outFile = new File(outDir, ExportToJSON.FILENAME_SECURITIES);
        Collection<Security> securities = this.getMnyContext().getSecurities().values();
        JSONUtils.writeValue(securities, outFile);
    }

    /**
     * Export currencies to dir.
     *
     * @param outDir the out dir
     * @throws IOException Signals that an I/O exception has occurred.
     */

    protected void exportCurrenciesToDir(File outDir) throws IOException {
        File outFile = new File(outDir, ExportToJSON.FILENAME_CURRENCIES);
        Collection<Currency> currencies = this.getMnyContext().getCurrencies().values();
        JSONUtils.writeValue(currencies, outFile);
    }

    /**
     * Export payees to dir.
     *
     * @param outDir the out dir
     * @throws IOException Signals that an I/O exception has occurred.
     */
    protected void exportPayeesToDir(File outDir) throws IOException {
        File outFile = new File(outDir, ExportToJSON.FILENAME_PAYEES);
        Collection<Payee> payees = this.getMnyContext().getPayees().values();
        JSONUtils.writeValue(payees, outFile);
    }

    /**
     * Export categories to dir.
     *
     * @param outDir the out dir
     * @throws IOException Signals that an I/O exception has occurred.
     */
    protected void exportCategoriesToDir(File outDir) throws IOException {
        File outFile = new File(outDir, ExportToJSON.FILENAME_CATEGORIES);
        Collection<Category> categories = this.getMnyContext().getCategories().values();
        JSONUtils.writeValue(categories, outFile);
    }

    /**
     * Export filtered transactions to dir.
     *
     * @param account the account
     * @param outDir  the out dir
     * @throws IOException Signals that an I/O exception has occurred.
     */
    protected void exportFilteredTransactionsToDir(Account account, File outDir) throws IOException {
        File outFile = new File(outDir, ExportToJSON.FILENAME_FILTERED_TRANSACTIONS);
        JSONUtils.writeValue(account.getFilteredTransactions(), outFile);
    }

    /**
     * Export transactions to dir.
     *
     * @param account the account
     * @param outDir  the out dir
     * @throws IOException Signals that an I/O exception has occurred.
     */
    protected void exportTransactionsToDir(Account account, File outDir) throws IOException {
        File outFile = new File(outDir, ExportToJSON.FILENAME_TRANSACTIONS);
        JSONUtils.writeValue(account.getTransactions(), outFile);
    }

    /**
     * Export detail to dir.
     *
     * @param account the account
     * @param outDir  the out dir
     * @throws IOException Signals that an I/O exception has occurred.
     */
    protected void exportAccountDetailToDir(Account account, File outDir) throws IOException {
        File outFile = new File(outDir, ExportToJSON.FILENAME_ACCOUNT);
        JSONUtils.writeValue(account, outFile);
    }

    /**
     * Creates the account names list.
     *
     * @return the list
     */
    protected List<AccountEntry> createAccountNamesList() {
        Map<Integer, Account> accounts = this.getMnyContext().getAccounts();
        List<AccountEntry> accountNames = new ArrayList<AccountEntry>();
        if (accounts != null) {
            Collection<Account> values = accounts.values();
            for (Account value : values) {
                AccountEntry accountEntry = new AccountEntry();
                accountEntry.setName(value.getName());
                accountEntry.setSafeName(toSafeFileName(value.getName()));
                accountEntry.setId(value.getId());
                accountNames.add(accountEntry);
            }
        }
        return accountNames;
    }

    /**
     * The Class AccountEntry.
     */
    public class AccountEntry {

        /** The id. */
        @JsonProperty("_id")
        private Integer id;

        /** The name. */
        private String name;

        /** The safe name. */
        private String safeName;

        /**
         * Gets the name.
         *
         * @return the name
         */
        public String getName() {
            return name;
        }

        /**
         * Sets the name.
         *
         * @param name the new name
         */
        public void setName(String name) {
            this.name = name;
        }

        /**
         * Gets the safe name.
         *
         * @return the safe name
         */
        public String getSafeName() {
            return safeName;
        }

        /**
         * Sets the safe name.
         *
         * @param safeName the new safe name
         */
        public void setSafeName(String safeName) {
            this.safeName = safeName;
        }

        /**
         * Gets the id.
         *
         * @return the id
         */
        public Integer getId() {
            return id;
        }

        /**
         * Sets the id.
         *
         * @param id the new id
         */
        public void setId(Integer id) {
            this.id = id;
        }
    }

}

package com.hungle.sunriise.export.cvs2;

import java.io.File;
import java.io.IOException;
import java.util.Collection;
import java.util.List;

import org.apache.logging.log4j.Logger;

import com.hungle.sunriise.export.ExportSerializer;
import com.hungle.sunriise.export.cvs2.model.AbstractExportToCsv2Wrapper;
import com.hungle.sunriise.export.cvs2.model.Csv2Account;
import com.hungle.sunriise.export.cvs2.model.DefaultAccountWrapper;
import com.hungle.sunriise.export.cvs2.model.DefaultCategoryWrapper;
import com.hungle.sunriise.export.cvs2.model.DefaultCurrencyWrapper;
import com.hungle.sunriise.export.cvs2.model.DefaultPayeeWrapper;
import com.hungle.sunriise.export.cvs2.model.DefaultSecurityWrapper;
import com.hungle.sunriise.export.cvs2.model.DefaultTransactionWrapper;
import com.hungle.sunriise.mnyobject.Account;
import com.hungle.sunriise.mnyobject.Category;
import com.hungle.sunriise.mnyobject.Currency;
import com.hungle.sunriise.mnyobject.Payee;
import com.hungle.sunriise.mnyobject.Security;
import com.hungle.sunriise.mnyobject.Transaction;

public class ExportToCsv2Serializer implements ExportSerializer {
    private static final Logger LOGGER = org.apache.logging.log4j.LogManager.getLogger(ExportSerializer.class);

    /** The Constant FILENAME_CATEGORIES. */
    private static final String FILENAME_CATEGORIES = "categories.csv";
    /** The Constant FILENAME_PAYEES. */
    private static final String FILENAME_PAYEES = "payees.csv";
    /** The Constant FILENAME_CURRENCIES. */
    private static final String FILENAME_CURRENCIES = "currencies.csv";
    /** The Constant FILENAME_SECURITIES. */
    private static final String FILENAME_SECURITIES = "securities.csv";
    /** The Constant FILENAME_ACCOUNTS. */
    private static final String FILENAME_ACCOUNTS = "accounts.csv";
    /** The Constant FILENAME_ACCOUNT. */
    private static final String FILENAME_ACCOUNT = "account.csv";
    /** The Constant FILENAME_TRANSACTIONS. */
    static final String FILENAME_TRANSACTIONS = "transactions.csv";
    /** The Constant FILENAME_FILTERED_TRANSACTIONS. */
    static final String FILENAME_FILTERED_TRANSACTIONS = "filteredTransactions.csv";

    @Override
    public void serializeCategories(Collection<Category> categories, File outDir) throws IOException {
        final File outFile = new File(outDir, ExportToCsv2Serializer.FILENAME_CATEGORIES);
        LOGGER.info("> serializeCategories, outFile={}", outFile);
        try (DefaultCategoryWrapper wrapper = new DefaultCategoryWrapper(outFile)) {
            wrapper.writeBeans(categories);
        }
    }

    @Override
    public void serializePayees(Collection<Payee> payees, File outDir) throws IOException {
        final File outFile = new File(outDir, ExportToCsv2Serializer.FILENAME_PAYEES);
        LOGGER.info("> serializePayees, outFile={}", outFile);
        try (DefaultPayeeWrapper wrapper = new DefaultPayeeWrapper(outFile)) {
            wrapper.writeBeans(payees);
        }

    }

    @Override
    public void serializeCurrencies(Collection<Currency> currencies, File outDir) throws IOException {
        final File outFile = new File(outDir, ExportToCsv2Serializer.FILENAME_CURRENCIES);
        LOGGER.info("> serializeCurrencies, outFile={}", outFile);
        try (DefaultCurrencyWrapper wrapper = new DefaultCurrencyWrapper(outFile)) {
            wrapper.writeBeans(currencies);
        }
    }

    @Override
    public void serializeSecurities(Collection<Security> securities, File outDir) throws IOException {
        final File outFile = new File(outDir, ExportToCsv2Serializer.FILENAME_SECURITIES);
        LOGGER.info("> serializeSecurities, outFile={}", outFile);
        try (DefaultSecurityWrapper wrapper = new DefaultSecurityWrapper(outFile)) {
            wrapper.writeBeans(securities);
        }

    }

    @Override
    public void serializeAccounts(Collection<Account> accounts, File outDir) throws IOException {
        final File outFile = new File(outDir, ExportToCsv2Serializer.FILENAME_ACCOUNTS);
        LOGGER.info("> serializeAccounts, outFile={}", outFile);
        try (AbstractExportToCsv2Wrapper<Account, Csv2Account> wrapper = new DefaultAccountWrapper(outFile)) {
            wrapper.writeBeans(accounts);
        }
    }

    @Override
    public void serializeTransactions(Account account, List<Transaction> transactions, File outDir) throws IOException {
        final File outFile = new File(outDir, ExportToCsv2Serializer.FILENAME_TRANSACTIONS);
        LOGGER.info("> serializeTransactions, outFile={}", outFile);
        try (DefaultTransactionWrapper wrapper = new DefaultTransactionWrapper(outFile)) {
            wrapper.writeBeans(transactions);
        }
    }

    @Override
    public void serializeFilteredTransactions(Account account, List<Transaction> transactions, File outDir)
            throws IOException {
        final File outFile = new File(outDir, ExportToCsv2Serializer.FILENAME_FILTERED_TRANSACTIONS);
        LOGGER.info("> serializeFilteredTransactions, outFile={}", outFile);
        try (DefaultTransactionWrapper wrapper = new DefaultTransactionWrapper(outFile)) {
            wrapper.writeBeans(transactions);
        }
    }

    @Override
    public void serializeAllTransactions(Collection<Account> accounts, File outDir, boolean appendMode)
            throws IOException {
        List<Transaction> transactions = null;

        final File outFile = new File(outDir, ExportToCsv2Serializer.FILENAME_TRANSACTIONS);
        LOGGER.info("> serializeAllTransactions, outFile={}", outFile);

        try (DefaultTransactionWrapper wrapper = new DefaultTransactionWrapper(outFile, appendMode)) {
            final File outFile2 = new File(outDir, ExportToCsv2Serializer.FILENAME_FILTERED_TRANSACTIONS);
            LOGGER.info("> serializeAllTransactions, outFile2={}", outFile2);

            try (DefaultTransactionWrapper wrapper2 = new DefaultTransactionWrapper(outFile2, appendMode)) {
                // Collection<Account> accounts = getAccounts();
                for (Account account : accounts) {
                    transactions = account.getTransactions();
                    if (transactions != null) {
                        wrapper.writeBeans(transactions);
                    }

                    transactions = account.getFilteredTransactions();
                    if (transactions != null) {
                        wrapper2.writeBeans(transactions);
                    }
                }
            }
        }
    }
}

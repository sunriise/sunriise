package com.hungle.sunriise.export.mongodb;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.Logger;
import org.bson.Document;

import com.fasterxml.jackson.core.Version;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;
import com.hungle.sunriise.dbutil.TableAccountUtils;
import com.hungle.sunriise.export.AbstractGetCollection;
import com.hungle.sunriise.io.MnyDb;
import com.hungle.sunriise.json.JSONUtils;
import com.hungle.sunriise.json.MongoDbDateSerializer;
import com.hungle.sunriise.mnyobject.Account;
import com.hungle.sunriise.mnyobject.Category;
import com.hungle.sunriise.mnyobject.Currency;
import com.hungle.sunriise.mnyobject.MnyContext;
import com.hungle.sunriise.mnyobject.Payee;
import com.hungle.sunriise.mnyobject.Security;
import com.hungle.sunriise.mnyobject.Transaction;
import com.hungle.sunriise.util.MnyContextUtils;
import com.mongodb.bulk.BulkWriteResult;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.InsertOneModel;
import com.mongodb.client.model.WriteModel;

// TODO: Auto-generated Javadoc
/**
 * The Class MongoUtil.
 */
public class MongoUtils {

    /**
     * The Class WorkaroundNoCloseWriter.
     */
    private static final class WorkaroundNoCloseWriter extends BufferedWriter {

        /**
         * Instantiates a new workaround no close writer.
         *
         * @param writer the writer
         * @throws IOException Signals that an I/O exception has occurred.
         */
        private WorkaroundNoCloseWriter(Writer writer) throws IOException {
            super(writer);
        }

        /*
         * (non-Javadoc)
         * 
         * @see java.io.BufferedWriter#close()
         */
        @Override
        public void close() throws IOException {
            // TODO: work-around: don't close
            // super.close();
        }

        /**
         * Close 2.
         *
         * @throws IOException Signals that an I/O exception has occurred.
         */
        public void close2() throws IOException {
            super.close();
        }
    }

    /** The Constant LOGGER. */
    private static final Logger LOGGER = org.apache.logging.log4j.LogManager.getLogger(MongoUtils.class);

    /** The Constant TRANSACTION_ACCOUNT_ID_INDEX_NAME. */
    public static final String TRANSACTION_ACCOUNT_ID_INDEX_NAME = "accountId";

    /** The Constant COLLECTION_SYSTEM_NAME_PREFIX. */
    static final String COLLECTION_SYSTEM_NAME_PREFIX = "system.";

    /** The Constant ACCOUNTS_COLLECTION_NAME. */
    public static final String ACCOUNTS_COLLECTION_NAME = "accounts";

    /** The Constant TRANSACTIONS_COLLECTION_NAME. */
    public static final String TRANSACTIONS_COLLECTION_NAME = "transactions";

    /** The Constant FILTERED_TRANSACTIONS_COLLECTION_NAME. */
    public static final String FILTERED_TRANSACTIONS_COLLECTION_NAME = "filteredTransactions";

    /** The Constant SECURITIES_COLLECTION_NAME. */
    public static final String SECURITIES_COLLECTION_NAME = "securities";

    /** The Constant PAYEES_COLLECTION_NAME. */
    public static final String PAYEES_COLLECTION_NAME = "payees";

    /** The Constant CURRENCIES_COLLECTION_NAME. */
    public static final String CURRENCIES_COLLECTION_NAME = "currencies";

    /** The Constant CATEGORIES_COLLECTION_NAME. */
    public static final String CATEGORIES_COLLECTION_NAME = "categories";

    /**
     * Removes the all collections.
     *
     * @param db the db
     */
    public static void removeAllCollections(MongoDatabase db) {
        LOGGER.info("Dropping all collections ...");

        for (String name : db.listCollectionNames()) {
            if (name.startsWith(COLLECTION_SYSTEM_NAME_PREFIX)) {
                continue;
            }
            LOGGER.info("Dropping collection.name=" + name + " ...");
            db.getCollection(name).drop();
        }
    }

    /**
     * List all collections.
     *
     * @param db the db
     */
    public static void listAllCollections(MongoDatabase db) {
        LOGGER.info("Listing all collections ...");

        for (String collectionName : db.listCollectionNames()) {
            if (collectionName.startsWith(COLLECTION_SYSTEM_NAME_PREFIX)) {
                continue;
            }
            LOGGER.info("LIST collection.name=" + collectionName + " ...");
            MongoCollection<Document> collection = db.getCollection(collectionName);
            LOGGER.info("  count=" + collection.count());

        }
    }

    /**
     * Flush bulk write.
     *
     * @param writes     the writes
     * @param collection the collection
     * @return the bulk write result
     * @throws IOException Signals that an I/O exception has occurred.
     */
    public static BulkWriteResult flushBulkWrite(List<WriteModel<Document>> writes,
            MongoCollection<Document> collection) throws IOException {
        LOGGER.info("flush: count=" + writes.size());

        BulkWriteResult result = collection.bulkWrite(writes);
        writes.clear();

        return result;
    }

    /**
     * Serialize to document.
     *
     * @param <T>  the generic type
     * @param item the item
     * @return the document
     * @throws IOException Signals that an I/O exception has occurred.
     */
    public static <T> Document serializeToDocument(T item) throws IOException {
        String json = JSONUtils.serialize(item);
        // LOGGER.info(json);
        Document document = Document.parse(json);
        return document;
    }

    /**
     * Bulk write items.
     *
     * @param <T>            the generic type
     * @param items          the items
     * @param collectionName the collection name
     * @param mongoDb        the mongo db
     * @param batchSize      the batch size
     * @return the mongo collection
     * @throws IOException Signals that an I/O exception has occurred.
     */
    public static <T> MongoCollection<Document> bulkWriteItems(Collection<T> items, String collectionName,
            MongoDatabase mongoDb, final int batchSize) throws IOException {
        final MongoCollection<Document> collection = mongoDb.getCollection(collectionName);

        return bulkWriteItems(items, collection, batchSize);
    }

    /**
     * Bulk write items.
     *
     * @param <T>        the generic type
     * @param items      the items
     * @param collection the collection
     * @param batchSize  the batch size
     * @return the mongo collection
     * @throws IOException Signals that an I/O exception has occurred.
     */
    public static <T> MongoCollection<Document> bulkWriteItems(Collection<T> items,
            final MongoCollection<Document> collection, final int batchSize) throws IOException {
        List<WriteModel<Document>> writes = new ArrayList<WriteModel<Document>>();
        BulkWriteResult result = null;
        for (T item : items) {
            Document document = serializeToDocument(item);
            writes.add(new InsertOneModel<Document>(document));
            if (writes.size() >= batchSize) {
                int expected = writes.size();
                result = flushBulkWrite(writes, collection);
                if (result.getInsertedCount() != expected) {
                    throw new IOException(
                            "Failed assert: expected=" + expected + ", actual=" + result.getInsertedCount());
                }
            }
        }

        if (writes.size() > 0) {
            int expected = writes.size();
            result = flushBulkWrite(writes, collection);
            if (result.getInsertedCount() != expected) {
                throw new IOException("Failed assert: expected=" + expected + ", actual=" + result.getInsertedCount());
            }
        }

        return collection;
    }

    /**
     * Gets the accounts collection.
     *
     * @param mongoDb the mongo db
     * @return the accounts collection
     */
    public static final MongoCollection<Document> getAccountsCollection(MongoDatabase mongoDb) {
        return mongoDb.getCollection(ACCOUNTS_COLLECTION_NAME);
    }

    /**
     * Gets the transactions collection.
     * 
     * @param mongoDb the mongo db
     *
     * @return the transactions collection
     */
    public static final MongoCollection<Document> getTransactionsCollection(MongoDatabase mongoDb) {
        return mongoDb.getCollection(TRANSACTIONS_COLLECTION_NAME);
    }

    /**
     * Gets the filtered transactions collection.
     *
     * @param mongoDb the mongo db
     * @return the filtered transactions collection
     */
    public static final MongoCollection<Document> getFilteredTransactionsCollection(MongoDatabase mongoDb) {
        return mongoDb.getCollection(FILTERED_TRANSACTIONS_COLLECTION_NAME);
    }

    /**
     * Creates the export collections map.
     *
     * @param mnyContext the mny context
     * @return the collections map
     */
    public static final Map<String, AbstractGetCollection<? extends Object>> createExportCollectionsMap(
            MnyContext mnyContext) {
        Map<String, AbstractGetCollection<? extends Object>> map = new HashMap<String, AbstractGetCollection<? extends Object>>();

        map.put(ACCOUNTS_COLLECTION_NAME, new AbstractGetCollection<Account>(mnyContext) {
            @Override
            public Collection<Account> getCollection() {
                return getMnyContext().getAccounts().values();
            }
        });

        map.put(TRANSACTIONS_COLLECTION_NAME, new AbstractGetCollection<Transaction>(mnyContext) {
            @Override
            public Collection<Transaction> getCollection() {
                Collection<Transaction> transactions = new ArrayList<Transaction>();
                Map<Integer, Account> accounts = getMnyContext().getAccounts();
                for (Account account : accounts.values()) {
                    try {
                        TableAccountUtils.addTransactionsToAccount(account, getMnyContext());
                        transactions.addAll(account.getTransactions());
                    } catch (IOException e) {
                        LOGGER.warn(e);
                    }
                }
                return transactions;
            }
        });

        map.put(SECURITIES_COLLECTION_NAME, new AbstractGetCollection<Security>(mnyContext) {
            @Override
            public Collection<Security> getCollection() {
                return getMnyContext().getSecurities().values();
            }
        });

        map.put(PAYEES_COLLECTION_NAME, new AbstractGetCollection<Payee>(mnyContext) {
            @Override
            public Collection<Payee> getCollection() {
                return getMnyContext().getPayees().values();
            }
        });

        map.put(CURRENCIES_COLLECTION_NAME, new AbstractGetCollection<Currency>(mnyContext) {
            @Override
            public Collection<Currency> getCollection() {
                return getMnyContext().getCurrencies().values();
            }
        });

        map.put(CATEGORIES_COLLECTION_NAME, new AbstractGetCollection<Category>(mnyContext) {
            @Override
            public Collection<Category> getCollection() {
                return getMnyContext().getCategories().values();
            }
        });

        return map;
    }

    /**
     * Export to mongo import format.
     *
     * @param mnyDb      the opened db
     * @param outDirName the out dir name
     * @throws IOException Signals that an I/O exception has occurred.
     */
    static final void exportToMongoImportFormat(MnyDb mnyDb, String outDirName) throws IOException {
        MnyContext mnyContext = MnyContextUtils.createMnyContext(mnyDb);
        TableAccountUtils.populateAccounts(mnyContext);

        File dir = new File(outDirName);
        dir.mkdirs();

        Map<String, AbstractGetCollection<? extends Object>> collectionsMap = createExportCollectionsMap(mnyContext);

        for (String name : collectionsMap.keySet()) {
            Collection<? extends Object> collection = collectionsMap.get(name).getCollection();
            File outFile = new File(dir, name + ".json");
            LOGGER.info("Exporting to file=" + outFile + ", collection.size=" + collection.size());
            WorkaroundNoCloseWriter writer = null;
            try {
                writer = new WorkaroundNoCloseWriter(new FileWriter(outFile));
                ObjectMapper mapper = JSONUtils.createDefaultObjectMapper();
                SimpleModule mongodbModule = new SimpleModule("MongodbModule", new Version(1, 0, 0, null, null, null));
                StdSerializer<Date> stdSerializer = new MongoDbDateSerializer(Date.class);
                mongodbModule.addSerializer(Date.class, stdSerializer);
                mapper.registerModule(mongodbModule);
                boolean prettyPrint = false;
                for (Object item : collection) {
                    JSONUtils.writeValue(item, writer, prettyPrint, mapper);
                    writer.write("\r\n");
                }
            } finally {
                if (writer != null) {
                    writer.flush();
                    writer.close2();
                    writer = null;
                }
            }
        }
    }

    /**
     * Export to mongo import format.
     *
     * @param dbFile     the db file
     * @param password   the password
     * @param outDirName the out dir name
     */
    public static void exportToMongoImportFormat(File dbFile, String password, String outDirName) {
        MnyDb mnyDb = null;
        try {
            mnyDb = new MnyDb(dbFile, password);
            MongoUtils.exportToMongoImportFormat(mnyDb, outDirName);
        } catch (IOException e) {
            LOGGER.error(e);
        } finally {
            if (mnyDb != null) {
                try {
                    mnyDb.close();
                } catch (IOException e) {
                    LOGGER.warn(e);
                } finally {
                    mnyDb = null;
                }
            }
        }
    }

    /**
     * Accounts insert one.
     *
     * @param <T>            the generic type
     * @param items          the accounts
     * @param collectionName the collection name
     * @param mongoDb        the mongo db
     * @return the mongo collection
     * @throws IOException Signals that an I/O exception has occurred.
     */
    public static final <T> MongoCollection<Document> insertItems(Collection<T> items, String collectionName,
            MongoDatabase mongoDb) throws IOException {
        final MongoCollection<Document> collection = mongoDb.getCollection(collectionName);

        return insertItems(items, collection);
    }

    /**
     * Insert one.
     *
     * @param <T>        the generic type
     * @param items      the items
     * @param collection the collection
     * @return the mongo collection
     * @throws IOException Signals that an I/O exception has occurred.
     */
    private static final <T> MongoCollection<Document> insertItems(Collection<T> items,
            final MongoCollection<Document> collection) throws IOException {
        for (T item : items) {
            Document document = serializeToDocument(item);
            collection.insertOne(document);
        }

        return collection;
    }

    /**
     * Gets the securities collection.
     *
     * @param mongoDb the mongo db
     * @return the securities collection
     */
    public static final MongoCollection<Document> getSecuritiesCollection(MongoDatabase mongoDb) {
        return mongoDb.getCollection(SECURITIES_COLLECTION_NAME);
    }

    /**
     * Gets the payees collection.
     *
     * @param mongoDb the mongo db
     * @return the payees collection
     */
    public static final MongoCollection<Document> getPayeesCollection(MongoDatabase mongoDb) {
        return mongoDb.getCollection(PAYEES_COLLECTION_NAME);
    }

    /**
     * Gets the currencies collection.
     *
     * @param mongoDb the mongo db
     * @return the currencies collection
     */
    public static final MongoCollection<Document> getCurrenciesCollection(MongoDatabase mongoDb) {
        return mongoDb.getCollection(CURRENCIES_COLLECTION_NAME);
    }

    /**
     * Gets the categories collection.
     *
     * @param mongoDb the mongo db
     * @return the categories collection
     */
    public static final MongoCollection<Document> getCategoriesCollection(MongoDatabase mongoDb) {
        return mongoDb.getCollection(CATEGORIES_COLLECTION_NAME);
    }
}

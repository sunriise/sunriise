package com.hungle.sunriise.export.ofx;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.function.Supplier;

import org.apache.logging.log4j.Logger;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.healthmarketscience.jackcess.Database;
import com.hungle.sunriise.account.DefaultAccountVisitor;
import com.hungle.sunriise.io.MnyDb;
import com.hungle.sunriise.mnyobject.Account;

// TODO: Auto-generated Javadoc
/**
 * The Class AbstractExportToJSON.
 */
public abstract class AbstractExportToOfx extends DefaultAccountVisitor {

    /** The Constant LOGGER. */
    private static final Logger LOGGER = org.apache.logging.log4j.LogManager.getLogger(AbstractExportToOfx.class);

    /** The Constant FILENAME_ACCOUNTS. */
    static final String FILENAME_ACCOUNTS = "accounts.json";
    /** The Constant FILENAME_ACCOUNT. */
    static final String FILENAME_ACCOUNT = "account.json";
    /** The Constant FILENAME_TRANSACTIONS. */
    static final String FILENAME_TRANSACTIONS = "transactions.json";
    /** The Constant FILENAME_FILTERED_TRANSACTIONS. */
    static final String FILENAME_FILTERED_TRANSACTIONS = "filteredTransactions.json";
    /** The Constant FILENAME_SECURITIES. */
    static final String FILENAME_SECURITIES = "securities.json";
    /** The Constant FILENAME_CURRENCIES. */
    static final String FILENAME_CURRENCIES = "currencies.json";
    /** The Constant FILENAME_PAYEES. */
    static final String FILENAME_PAYEES = "payees.json";
    /** The Constant FILENAME_CATEGORIES. */
    static final String FILENAME_CATEGORIES = "categories.json";

    private ExecutorService threadPool;

    private List<CompletableFuture<Boolean>> futures;

    /**
     * The Class AccountEntry.
     */
    public class AccountEntry {

        /** The id. */
        @JsonProperty("_id")
        private Integer id;

        /** The name. */
        private String name;

        /** The safe name. */
        private String safeName;

        /**
         * Gets the name.
         *
         * @return the name
         */
        public String getName() {
            return name;
        }

        /**
         * Sets the name.
         *
         * @param name the new name
         */
        public void setName(String name) {
            this.name = name;
        }

        /**
         * Gets the safe name.
         *
         * @return the safe name
         */
        public String getSafeName() {
            return safeName;
        }

        /**
         * Sets the safe name.
         *
         * @param safeName the new safe name
         */
        public void setSafeName(String safeName) {
            this.safeName = safeName;
        }

        /**
         * Gets the id.
         *
         * @return the id
         */
        public Integer getId() {
            return id;
        }

        /**
         * Sets the id.
         *
         * @param id the new id
         */
        public void setId(Integer id) {
            this.id = id;
        }
    }

    /**
     * Instantiates a new abstract export to JSON.
     */
    public AbstractExportToOfx() {
        super();
    }

    /**
     * Export mny context.
     *
     * @throws IOException Signals that an I/O exception has occurred.
     */
    protected void exportMnyContext() throws IOException {
        exportCategories();

        exportPayees();

        exportCurrencies();

        exportSecurities();

        exportAccountNames();
    }

    /**
     * Export account names.
     *
     * @throws IOException Signals that an I/O exception has occurred.
     */
    protected abstract void exportAccountNames() throws IOException;

    /**
     * Export securities.
     *
     * @throws IOException Signals that an I/O exception has occurred.
     */
    protected abstract void exportSecurities() throws IOException;

    /**
     * Export currencies.
     *
     * @throws IOException Signals that an I/O exception has occurred.
     */
    protected abstract void exportCurrencies() throws IOException;

    /**
     * Export payees.
     *
     * @throws IOException Signals that an I/O exception has occurred.
     */
    protected abstract void exportPayees() throws IOException;

    /**
     * Export categories.
     *
     * @throws IOException Signals that an I/O exception has occurred.
     */
    protected abstract void exportCategories() throws IOException;

    /**
     * Export.
     *
     * @param dbFile   the db file
     * @param password the password
     * @return the database
     * @throws IOException Signals that an I/O exception has occurred.
     */
    public Database export(File dbFile, String password) throws IOException {
        MnyDb mnyDb = null;
        try {
            initThreadPool();

            mnyDb = new MnyDb(dbFile, password);
            return export(mnyDb);
        } finally {
            if (mnyDb != null) {
                mnyDb.close();
                mnyDb = null;
            }
            shutdownThreadPool();
        }
    }

    private void shutdownThreadPool() {
        if (threadPool != null) {
            List<Runnable> runnables = threadPool.shutdownNow();
            int size = runnables.size();
            if (size > 0) {
                LOGGER.warn("Threadpool is shutdown with the following number of active tasks=" + size);
            }
        }
    }

    private void initThreadPool() {
        boolean enableThreadPool = false;
        if (!enableThreadPool) {
            LOGGER.info("ThreadPool is NOT enable.");
            return;
        }

        threadPool = Executors.newFixedThreadPool(4);
        if (threadPool != null) {
            futures = new ArrayList<>();
        }
    }

    /**
     * Export.
     *
     * @param mnyDb the mny db
     * @return the database
     * @throws IOException Signals that an I/O exception has occurred.
     */
    protected abstract Database export(MnyDb mnyDb) throws IOException;

    /**
     * Export account.
     *
     * @param account the account
     * @throws IOException Signals that an I/O exception has occurred.
     */
    protected void exportAccount(Account account) throws IOException {
        exportAccountDetail(account);

        exportAccountTransactions(account);

        exportAccountFilteredTransactions(account);
    }

    /**
     * Export account filtered transactions.
     *
     * @param account the account
     * @throws IOException Signals that an I/O exception has occurred.
     */
    protected abstract void exportAccountFilteredTransactions(Account account) throws IOException;

    /**
     * Export account transactions.
     *
     * @param account the account
     * @throws IOException Signals that an I/O exception has occurred.
     */
    protected abstract void exportAccountTransactions(Account account) throws IOException;

    /**
     * Export account detail.
     *
     * @param account the account
     * @throws IOException Signals that an I/O exception has occurred.
     */
    protected abstract void exportAccountDetail(Account account) throws IOException;

    /*
     * (non-Javadoc)
     * 
     * @see com.hungle.sunriise.account.DefaultAccountVisitor#visitAccounts(java.
     * util. Map)
     */
    @Override
    public void visitAccounts(Map<Integer, Account> accounts) throws IOException {
        super.visitAccounts(accounts);

        exportMnyContext();
    }

    @Override
    protected void visitPreAccountIteration() throws IOException {
        // clear the future list
        clearFutures();
    }

    private void clearFutures() {
        if (threadPool != null) {
            if (futures != null) {
                if (futures.size() > 0) {
                    LOGGER.warn("Futures list is non-empty. Clearing ...");
                }
                futures.clear();
            }
        }
    }

    @Override
    protected void visitPostAccountIteration() throws IOException {
        if (threadPool != null) {
            waitFutures(futures);
        }
    }

    private void waitFutures(List<CompletableFuture<Boolean>> ç) throws IOException {
        if (futures == null) {
            return;
        }

        Class<CompletableFuture> clz = CompletableFuture.class;
        CompletableFuture<Boolean>[] type = (CompletableFuture<Boolean>[]) Array.newInstance(clz, 0);
        CompletableFuture<Void> completedFuture = CompletableFuture.allOf(futures.toArray(type));
        try {
            completedFuture.join();
        } catch (Exception e) {
            throw new IOException(e);
        } finally {
            clearFutures();
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.hungle.sunriise.account.DefaultAccountVisitor#visitAccount(com.
     * hungle. sunriise.mnyobject.Account)
     */
    @Override
    public void visitAccount(Account account) throws IOException {
        super.visitAccount(account);

        String accountName = account.getName();
        LOGGER.info("> " + getAccountsIndex() + "/" + getAccountsCount() + " accountName=" + accountName);

        if (threadPool != null) {
            Supplier<Boolean> supplier = new Supplier<Boolean>() {

                @Override
                public Boolean get() {
                    try {
                        LOGGER.info("> BEGIN exportAccount, activeTasks="
                                + ((ThreadPoolExecutor) threadPool).getActiveCount());
                        exportAccount(account);
                    } catch (IOException e) {
                        throw new CompletionException(e);
                    } finally {
                        LOGGER.info("< END exportAccount ...");

                    }
                    return true;
                }
            };
            CompletableFuture<Boolean> future = CompletableFuture.supplyAsync(supplier, threadPool);
            futures.add(future);
        } else {
            exportAccount(account);
        }
    }

    /**
     * To safe file name.
     *
     * @param accountName the account name
     * @return the string
     */
    protected String toSafeFileName(String accountName) {
        StringBuilder sb = new StringBuilder();

        int len = accountName.length();
        for (int i = 0; i < len; i++) {
            char c = accountName.charAt(i);
            sb.append(toSafeFileNameChar(c));
        }
        return sb.toString();
    }

    /**
     * To safe file name char.
     *
     * @param c the c
     * @return the char
     */
    private char toSafeFileNameChar(char c) {
        char safeC = c;

        if (c == ' ') {
            return '_';
        }
        if (c == '(') {
            return '_';
        }
        if (c == ')') {
            return '_';
        }
        if (c == '&') {
            return '_';
        }
        if (c == '\'') {
            return '_';
        }
        if (c == '’') {
            return '_';
        }
        if (c == '/') {
            return '_';
        }
        if (c == '\\') {
            return '_';
        }
        if (c == '>') {
            return '_';
        }
        if (c == '<') {
            return '_';
        }
        if (c == '|') {
            return '_';
        }
        if (c == '"') {
            return '_';
        }
        if (c == '*') {
            return '_';
        }
        if (c == '*') {
            return '_';
        }
        if (c == '{') {
            return '_';
        }
        if (c == '}') {
            return '_';
        }

        return safeC;
    }

    /**
     * Creates the account names list.
     *
     * @return the list
     */
    protected List<AccountEntry> createAccountNamesList() {
        Map<Integer, Account> accounts = this.getMnyContext().getAccounts();
        List<AccountEntry> accountNames = new ArrayList<AccountEntry>();
        if (accounts != null) {
            Collection<Account> values = accounts.values();
            for (Account value : values) {
                AccountEntry accountEntry = new AccountEntry();
                accountEntry.setName(value.getName());
                accountEntry.setSafeName(toSafeFileName(value.getName()));
                accountEntry.setId(value.getId());
                accountNames.add(accountEntry);
            }
        }
        return accountNames;
    }
}
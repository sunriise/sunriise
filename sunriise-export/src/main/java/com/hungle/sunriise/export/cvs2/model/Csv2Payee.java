package com.hungle.sunriise.export.cvs2.model;

import java.util.Objects;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.apache.logging.log4j.Logger;

import com.hungle.sunriise.export.cvs2.AbstractCsv2Wrapper;
import com.hungle.sunriise.mnyobject.Payee;
import com.opencsv.bean.CsvBindByPosition;

@Entity
@Table(name = "Payee")
public class Csv2Payee extends AbstractCsv2Wrapper<Payee> {
    private static final Logger LOGGER = org.apache.logging.log4j.LogManager.getLogger(Csv2Payee.class);

    private static final String[] HEADERS = { "id", "name", "parentId" };

    @Id
    @CsvBindByPosition(position = 0)
    private Integer id;

    @CsvBindByPosition(position = 1)
    private String name;

    @CsvBindByPosition(position = 2)
    private Integer parentId;

    public String[] getHeaders() {
        String[] headers = HEADERS;
        return headers;
    }

    public Csv2Payee() {
        this(null);
    }

    public Csv2Payee(Payee value) {
        super(value);

        if (value == null) {
            return;
        }

        this.id = value.getId();
        this.name = value.getName();

        this.parentId = value.getParent();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getParentId() {
        return parentId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Csv2Payee)) {
            return false;
        }
        Csv2Payee payee = (Csv2Payee) o;
        return Objects.equals(getId(), payee.getId());
    }

    @Override
    public int hashCode() {
        return getClass().hashCode();
    }
}

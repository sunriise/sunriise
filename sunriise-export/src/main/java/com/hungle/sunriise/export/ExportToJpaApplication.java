package com.hungle.sunriise.export;

import java.io.File;

import javax.swing.ProgressMonitor;

import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.Banner.Mode;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.WebApplicationType;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.data.mongo.MongoDataAutoConfiguration;
import org.springframework.boot.autoconfigure.mongo.MongoAutoConfiguration;
import org.springframework.boot.autoconfigure.web.servlet.WebMvcAutoConfiguration;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.ApplicationContextInitializer;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.support.GenericApplicationContext;

import com.healthmarketscience.jackcess.Database;
import com.hungle.sunriise.export.jpa.MnyJpaExporter;
import com.hungle.sunriise.io.MnyDb;

@SpringBootApplication(exclude = { MongoAutoConfiguration.class, MongoDataAutoConfiguration.class,
        WebMvcAutoConfiguration.class, })
public class ExportToJpaApplication implements CommandLineRunner {
    private static final Logger LOGGER = org.apache.logging.log4j.LogManager.getLogger(ExportToJpaApplication.class);

    public static final String JDBC_H2_URL_PREFIX = "jdbc:h2:file:";

    /** The category repository. */
    @Autowired
    private MnyJpaExporter exporter;

    @Value("${sunriise.fileName}")
    private String dbFileName;

    @Value("${sunriise.password}")
    private String password;

    @Autowired
    private ProgressMonitor progressMonitor;
    
    private Database db;

    public static void main(String[] args) {
        File mnyFile = null;
        String password = null;
        String dbFileName = "./MnyJpaExporter";

        String dbUrl = JDBC_H2_URL_PREFIX + dbFileName;

        if (args.length == 2) {
            mnyFile = new File(args[0]);
            dbUrl = args[1];
        } else if (args.length == 3) {
            mnyFile = new File(args[0]);
            password = args[1];
            dbUrl = args[2];
        } else {
            Class<ExportToJpaApplication> clz = ExportToJpaApplication.class;
            System.out.println("Usage: " + clz.getName() + " *.mny [password] url");
            System.exit(1);
        }

//        LOGGER.info("file={}", dbFile.getAbsolutePath());
        LOGGER.info("dbUrl={}", dbUrl);

        ExportToJpaApplication.export(mnyFile, password, dbUrl);
    }

    public static Database export(File mnyFile, String password, String dbUrl) {
        String[] args = new String[0];
        ProgressMonitor progressMonitor = null;
        return ExportToJpaApplication.export(args, mnyFile, password, dbUrl, progressMonitor);
    }

    public static Database exportToFile(File mnyFile, String password, File dbFile, ProgressMonitor progressMonitor) {
        String dbUrl = JDBC_H2_URL_PREFIX + dbFile.getAbsolutePath();
        return ExportToJpaApplication.export(mnyFile, password, dbUrl, progressMonitor);
    }

    public static Database export(File mnyFile, String password, String dbUrl, ProgressMonitor progressMonitor) {
        String[] args = new String[0];
        return ExportToJpaApplication.export(args, mnyFile, password, dbUrl, progressMonitor);
    }

    public static Database export(String[] args, File mnyFile, String password, String dbUrl,
            final ProgressMonitor progressMonitor) {
        LOGGER.info("JPA, dbUrl={}", dbUrl);
        
        String fileName = mnyFile.getAbsolutePath();
        SpringApplicationBuilder builder = new SpringApplicationBuilder(ExportToJpaApplication.class)
                .web(WebApplicationType.NONE).bannerMode(Mode.OFF).properties("spring.jpa.hibernate.ddl-auto=create")
                .properties("spring.datasource.url=" + dbUrl).properties("sunriise.fileName=" + fileName)
                .properties("sunriise.password=" + ((password == null) ? "" : password))
                .initializers(new ApplicationContextInitializer<GenericApplicationContext>() {
                    @Override
                    public void initialize(GenericApplicationContext applicationContext) {
                        applicationContext.registerBean("progressMonitor", 
                                ProgressMonitor.class,
                                () -> progressMonitor
                                );
                    }
                })
                ;

        ConfigurableApplicationContext context = null;
        try {
            SpringApplication application = builder.build();
            if ((progressMonitor != null) && (progressMonitor.isCanceled())) {
                // cancel
            } else {
                context = application.run(args);
            }
        } finally {
            if (context != null) {
                context.close();
                context = null;
            }
            LOGGER.info("< DONE");
        }

        Database db = null;
        return db;
    }

    @Override
    public void run(String... args) throws Exception {
        LOGGER.info("> run");

        // ../sunriise-core/src/test/data/mny/sunset-sample-pwd.mny 123@abc! file:./abc
//        String fileName = "../sunriise-core/src/test/data/mny/sunset-sample-pwd.mny";
        String fileName = this.dbFileName;
        File dbFile = new File(fileName);
        LOGGER.info("file={}", dbFile.getAbsolutePath());

//        String password = "123@abc!";
        MnyDb mnyDb = new MnyDb(dbFile, this.password);
        if (mnyDb != null) {
            if (exporter != null) {
                this.setDb(exporter.export(mnyDb, progressMonitor));
            }
        }
    }

    public Database getDb() {
        return db;
    }

    public void setDb(Database db) {
        this.db = db;
    }
}

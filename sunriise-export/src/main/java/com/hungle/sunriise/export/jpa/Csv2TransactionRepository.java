package com.hungle.sunriise.export.jpa;

import org.springframework.data.jpa.repository.JpaRepository;

import com.hungle.sunriise.export.cvs2.model.Csv2Transaction;

/**
 * The Interface Csv2TransactionRepository.
 */
public interface Csv2TransactionRepository extends JpaRepository<Csv2Transaction, Integer> {

}

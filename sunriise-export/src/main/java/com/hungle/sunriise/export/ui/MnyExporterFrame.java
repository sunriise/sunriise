package com.hungle.sunriise.export.ui;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Container;
import java.awt.Desktop;
import java.awt.Dimension;
import java.awt.HeadlessException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.URI;
import java.util.List;
import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import javax.swing.AbstractAction;
import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.SwingUtilities;
import javax.swing.TransferHandler;

import org.apache.logging.log4j.Logger;
import org.fife.ui.rsyntaxtextarea.RSyntaxTextArea;
import org.fife.ui.rtextarea.RTextScrollPane;

import com.healthmarketscience.jackcess.Table;
import com.hungle.sunriise.diskusage.DiskUsageView;
import com.hungle.sunriise.diskusage.GetDiskUsageCmd;
import com.hungle.sunriise.gui.AbstractSamplesMenu;
import com.hungle.sunriise.gui.FileDropHandler;
import com.hungle.sunriise.gui.PasswordPanel;
import com.hungle.sunriise.io.MnyDb;
import com.hungle.sunriise.util.DefaultGetDiskUsageCollector;

public class MnyExporterFrame extends JFrame {
    private static final Logger LOGGER = org.apache.logging.log4j.LogManager.getLogger(MnyExporterFrame.class);

    private static final String OPEN_EXPORTS_DIRECTORY = "Open exports directory";

    private static final String OPEN_WIKI_PAGE = "Open wiki page";

    private MnyDb mnyDb;

    private ExecutorService threadPool = Executors.newCachedThreadPool();

    private enum ExportType {
        CSV("CSV"), JSON("JSON"), MDB("MDB"), JPA("JPA");

        private String str;

        ExportType(String str) {
            this.str = str;
        }

        public String getStr() {
            return str;
        }

        public void setStr(String str) {
            this.str = str;
        }
    }

    private ExportType exportType = ExportType.CSV;

    private File exportDir;

    private JMenu toolsMenu;

    private JMenu diskUsageMenu;

    private ExportFileDropHandler transferHandler;

    private final class ExportFileDropHandler extends FileDropHandler {
        private final JPanel view;

        private ExportFileDropHandler(JPanel view) {
            this.view = view;
        }

        @Override
        public void handleFile(File file) {
            if (!file.isFile()) {
                String message = String.format("'%s' is not a file.", file.getName());
                String title = "File error";
                JOptionPane.showMessageDialog(view, message, title, JOptionPane.ERROR_MESSAGE);
                return;
            }
            String name = file.getName();
            if (!name.endsWith(".mny")) {
                String message = String.format("'%s' is not a *.mny file.", file.getName());
                String title = "File error";
                JOptionPane.showMessageDialog(view, message, title, JOptionPane.ERROR_MESSAGE);
                return;
            }

            try {

                final MnyDb mnyDb = openMnyDb(file, view);
                if (handleMnyDb(mnyDb) == null) {
                    return;
                }

            } catch (Exception e) {
                LOGGER.error(e.getMessage(), e);
                String message = e.getMessage();
                String title = "Exception";
                JOptionPane.showMessageDialog(view, message, title, JOptionPane.ERROR_MESSAGE);
            }
        }

        protected MnyDb handleMnyDb(final MnyDb mnyDb) {
            setMnyDb(mnyDb);
            if (mnyDb == null) {
                return mnyDb;
            }

            export(mnyDb, view);
            
            return mnyDb;
        }
    }

    public MnyExporterFrame() throws HeadlessException {
        super("MnyExporter");

        File cwd = new File(".");
        exportDir = new File(cwd, "exports");
        if (!exportDir.exists()) {
            exportDir.mkdirs();
        }

        // setTitle("MnyExporter" + " " + exportDir.getAbsolutePath());

        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        Dimension preferredSize = new Dimension(400, 400);
        setPreferredSize(preferredSize);

        init(getContentPane());
    }

    private MnyDb openMnyDb(File file, Component parentComponent) throws IOException {
        String password = null;
        MnyDb mnyDb = null;

        while (mnyDb == null) {
            try {
                mnyDb = new MnyDb(file, password);
            } catch (com.healthmarketscience.jackcess.InvalidCredentialsException e) {
                String text = PasswordPanel.showDialog(parentComponent, "Enter Your Password");
                if (text == null) {
                    // Cancel
                    LOGGER.warn("User cancel");
                    mnyDb = null;
                    break;
                } else {
                    password = text;
                    mnyDb = null;
                }
            }
        }

        return mnyDb;
    }

    private void exportToCsv(ExportToContext exportToContext) {
        final AbstractExportToAction action = new ExportToCsv2Action(exportToContext);
        File destination = new File(exportDir, "csv");
        if (!destination.exists()) {
            destination.mkdirs();
        }
        action.setDestination(destination);
        int id = ActionEvent.ACTION_FIRST;
        String command = "ExportToCsv2Action";
        final ActionEvent event = new ActionEvent(exportToContext.getParentComponent(), id, command);
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                action.actionPerformed(event);
            }
        });
    }

    private void exportToJson(ExportToContext exportToContext) {
        final AbstractExportToAction action = new ExportToJSONAction(exportToContext);
        File destination = new File(exportDir, "json");
        if (!destination.exists()) {
            destination.mkdirs();
        }
        action.setDestination(destination);
        int id = ActionEvent.ACTION_FIRST;
        String command = "ExportToJSONAction";
        final ActionEvent event = new ActionEvent(exportToContext.getParentComponent(), id, command);
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                action.actionPerformed(event);
            }
        });
    }

    private void exportToJpa(ExportToContext exportToContext) {
        final AbstractExportToAction action = new ExportToJpaAction(exportToContext);
        File destination = new File(exportDir, "jpa");
        if (!destination.exists()) {
            destination.mkdirs();
        }
        action.setDestination(destination);
        int id = ActionEvent.ACTION_FIRST;
        String command = "ExportToJpaAction";
        final ActionEvent event = new ActionEvent(exportToContext.getParentComponent(), id, command);
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                action.actionPerformed(event);
            }
        });
    }

    private void exportToMdb(ExportToContext exportToContext) {
        final AbstractExportToAction action = new ExportToMdbAction(exportToContext);
        String fileName = exportToContext.getMnyDb().getDbFile().getName();
        int index = fileName.lastIndexOf('.');
        String prefix = fileName;
        if (index > 0) {
            prefix = fileName.substring(0, index);
        }
        File destination = new File(exportDir, prefix + ".mdb");

        action.setDestination(destination);
        int id = ActionEvent.ACTION_FIRST;
        String command = "ExportToMdbAction";
        final ActionEvent event = new ActionEvent(exportToContext.getParentComponent(), id, command);
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                action.actionPerformed(event);
            }
        });
    }

    private void calculateDiskUsage(final ExportToContext exportToContext) {

        Runnable doRun = new Runnable() {
            @Override
            public void run() {
                LOGGER.info("> calculateDiskUsage");
                MnyDb db = exportToContext.getMnyDb();
                File file = exportToContext.getMnyDb().getDbFile();

                try {
                    DefaultGetDiskUsageCollector collector = new DefaultGetDiskUsageCollector();
                    GetDiskUsageCmd.calculate(file, db.getDb(), collector);

                    String fileName = exportToContext.getMnyDb().getDbFile().getName();
                    int index = fileName.lastIndexOf('.');
                    String prefix = fileName;
                    if (index > 0) {
                        prefix = fileName.substring(0, index);
                    }
                    File destination = new File(exportDir, prefix + "-du.csv");
                    try (PrintWriter writer = new PrintWriter(destination)) {

                        String separator = ",";
                        String header = "" + quote("name") + separator + quote("rows") + separator + quote("indexes")
                                + separator + quote("bytes");
                        writer.println(header);

                        List<Table> tables = collector.getTables();
                        for (Table table : tables) {
                            String name = table.getName();
                            int rowCount = table.getRowCount();
                            int indexCount = table.getIndexes().size();
                            long byteCount = GetDiskUsageCmd.getBytesCount(table);

                            String line = "" + quote(name) + separator + quote(rowCount) + separator + quote(indexCount)
                                    + separator + quote(byteCount);
                            writer.println(line);
                        }
                    }
                    LOGGER.info("Wrote to diskUsage file={}", destination.getAbsolutePath());
                } catch (IOException e) {
                    LOGGER.error(e.getMessage());
                } finally {
                    LOGGER.info("< calculateDiskUsage");
                }
            }

            private String quote(Object obj) {
                return "\"" + obj.toString() + "\"";
            }
        };
        SwingUtilities.invokeLater(doRun);
    }

    private void init(Container contentPane) {
        addMenus(contentPane);

        final JPanel view = new JPanel();
        view.setLayout(new BorderLayout());
        view.setBorder(BorderFactory.createEmptyBorder(3, 3, 3, 3));
        contentPane.add(view);

        addCenter(view);
        addBottomBox(view);
    }

    private void addMenus(Container contentPane) {
        JMenuBar menuBar = new JMenuBar();

        JMenu menu = null;
        JMenuItem menuItem = null;

        menu = new JMenu("File");
        menuBar.add(menu);

        menuItem = new JMenuItem(new AbstractAction(OPEN_EXPORTS_DIRECTORY) {

            @Override
            public void actionPerformed(ActionEvent event) {
                try {
                    Desktop.getDesktop().open(exportDir);
                } catch (IOException e) {
                    LOGGER.error(e.getMessage(), e);
                }

            }
        });
        menu.add(menuItem);

        menuItem = new JMenuItem(new AbstractAction(OPEN_WIKI_PAGE) {

            @Override
            public void actionPerformed(ActionEvent event) {
                try {
                    URI uri = new URI("https://bitbucket.org/hleofxquotesteam/dist-sunriise/wiki/Home");
                    Desktop.getDesktop().browse(uri);
                } catch (Exception e) {
                    LOGGER.error(e.getMessage(), e);
                }

            }
        });
        menu.add(menuItem);

        menu.addSeparator();

        menuItem = new JMenuItem(new AbstractAction("Exit") {

            @Override
            public void actionPerformed(ActionEvent e) {
                System.exit(0);

            }
        });
        menu.add(menuItem);

        AbstractSamplesMenu abstractSamplesMenu = new AbstractSamplesMenu() {

            @Override
            protected void mnyDbSelected(MnyDb newMnyDb) {
//                openMnyDb(newMnyDb, true);
                transferHandler.handleMnyDb(newMnyDb);
            }
        };
        abstractSamplesMenu.addSamplesMenu(menuBar);

        addToolsMenu(menuBar);

        setJMenuBar(menuBar);
    }

    private void addCenter(JPanel parent) {
        String eol = "\r\n";
        JPanel view = new JPanel();
        view.setLayout(new BorderLayout());

        parent.add(view, BorderLayout.CENTER);

        RSyntaxTextArea textArea = new RSyntaxTextArea();
        transferHandler = createTransferHandler(view);
        textArea.setTransferHandler(transferHandler);

        // textArea.setSyntaxEditingStyle(SyntaxConstants.SYNTAX_STYLE_JAVASCRIPT);
        RTextScrollPane sp = new RTextScrollPane(textArea);
        // accountJsonTextArea = textArea;
        textArea.setEditable(false);
        textArea.setLineWrap(true);

        textArea.append("To export, just drag-and-drop a *.mny file here." + eol);

        textArea.append(eol);
        textArea.append("To select different export type:" + eol);
        textArea.append("  * " + ExportType.CSV + ": export to *.csv files" + eol);
        textArea.append("  * " + ExportType.JSON + ": export to *.json files" + eol);
        textArea.append("  * " + ExportType.MDB + ": export to *.mdb file" + eol);

        textArea.append(eol);
        textArea.append("To see the export file(s):" + eol);
        textArea.append("  Select menu 'File -> " + OPEN_EXPORTS_DIRECTORY + "'" + eol);
        // textArea.append(" to open the export directory." + eol);

        textArea.append(eol);
        textArea.append("To get additional info:" + eol);
        textArea.append("  Select menu 'File -> " + OPEN_WIKI_PAGE + "'" + eol);

        // textArea.append(eol);
        // textArea.append("For more information see:" + eol);
        // textArea.append("
        // https://bitbucket.org/hleofxquotesteam/dist-sunriise/wiki/Home" +
        // eol);

        view.add(sp, BorderLayout.CENTER);
    }

    protected ExportFileDropHandler createTransferHandler(JPanel view) {
        return new ExportFileDropHandler(view);
    }

    private void addBottomBox(JPanel parent) {
        JPanel view = new JPanel();
        view.setLayout(new BoxLayout(view, BoxLayout.LINE_AXIS));
        parent.add(view, BorderLayout.SOUTH);

        ActionListener listener = new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String str = e.getActionCommand();
                exportType = ExportType.valueOf(str);
                LOGGER.info("exportType={}", exportType);
            }
        };

        JLabel label = new JLabel("Export to: ");
        view.add(label);

        ButtonGroup group = new ButtonGroup();
        for (ExportType exportType : ExportType.values()) {
            String csvLabel = exportType.toString();
            JRadioButton button = new JRadioButton(csvLabel);
            // csvButton.setMnemonic(KeyEvent.VK_C);
            button.setActionCommand(csvLabel);
            button.setSelected(true);
            button.addActionListener(listener);
            view.add(button);
            group.add(button);
        }

//        String csvLabel = ExportType.CSV.toString();
//        JRadioButton csvButton = new JRadioButton(csvLabel);
//        // csvButton.setMnemonic(KeyEvent.VK_C);
//        csvButton.setActionCommand(csvLabel);
//        csvButton.setSelected(true);
//        csvButton.addActionListener(listener);
//        view.add(csvButton);
//
//        String jsonLabel = ExportType.JSON.toString();
//        JRadioButton jsonButton = new JRadioButton(jsonLabel);
//        // jsonButton.setMnemonic(KeyEvent.VK_J);
//        jsonButton.setActionCommand(jsonLabel);
//        // jsonButton.setSelected(true);
//        jsonButton.addActionListener(listener);
//        view.add(jsonButton);
//
//        String mdbLabel = ExportType.MDB.toString();
//        JRadioButton mdbButton = new JRadioButton(mdbLabel);
//        // mdbButton.setMnemonic(KeyEvent.VK_M);
//        mdbButton.setActionCommand(mdbLabel);
//        // jsonButton.setSelected(true);
//        mdbButton.addActionListener(listener);
//        view.add(mdbButton);

//        ButtonGroup group = new ButtonGroup();
//        group.add(csvButton);
//        group.add(jsonButton);
//        group.add(mdbButton);
    }

    public static void main(String[] args) {
        final MnyExporterFrame mainView = new MnyExporterFrame();
        Runnable doRun = new Runnable() {
            @Override
            public void run() {
                mainView.showMainFrame();
            }
        };
        SwingUtilities.invokeLater(doRun);
        // doRun.run();
    }

    private void showMainFrame() {
        setLocation(100, 100);
        pack();
        setVisible(true);
    }

    private void export(final MnyDb mnyDb, Component parentComponent) {
        ExportToContext exportToContext = new ExportToContext() {

            @Override
            public Component getParentComponent() {
                return parentComponent;
            }

            @Override
            public MnyDb getMnyDb() {
                return mnyDb;
            }

            @Override
            public Executor getThreadPool() {
                return threadPool;
            }

        };

        switch (exportType) {
        case CSV:
            exportToCsv(exportToContext);
            break;
        case JSON:
            exportToJson(exportToContext);
            break;
        case MDB:
            exportToMdb(exportToContext);
            break;
        case JPA:
            exportToJpa(exportToContext);
            break;
        default:
            exportToCsv(exportToContext);
            break;
        }

        calculateDiskUsage(exportToContext);

        SwingUtilities.invokeLater(new Runnable() {

            @Override
            public void run() {
                MnyDb mnyDb = getMnyDb();
                if (mnyDb != null) {
                    String title = mnyDb.getDbFile().getName();
                    setTitle(title);
                }

                if (diskUsageMenu != null) {
                    diskUsageMenu.setEnabled(true);
                }
            }
        });
    }

    /**
     * Adds the tools menu.
     *
     * @param menuBar the menu bar
     */
    private void addToolsMenu(JMenuBar menuBar) {
        LOGGER.info("> addToolsMenu");

        toolsMenu = new JMenu("Tools");
        menuBar.add(toolsMenu);

        diskUsageMenu = new JMenu("Disk usage");
        toolsMenu.add(diskUsageMenu);

        JMenuItem diskUsageMenuItem = new JMenuItem(new AbstractAction("Show") {
            public void actionPerformed(ActionEvent event) {
                Component parentComponent = MnyExporterFrame.this.getFrame();
                Object message = null;

                String title = "Disk usage";

                MnyDb mnyDb = MnyExporterFrame.this.getMnyDb();
                if ((mnyDb != null) && (mnyDb.getDb() != null)) {
                    try {
                        title = mnyDb.getDbFile().getName();
                        DiskUsageView view = new DiskUsageView(mnyDb);
                        message = view;
                    } catch (IOException e) {
                        message = e.getMessage();
                        LOGGER.error(e.getMessage());
                    }
                    if (message == null) {
                        message = "No data available";
                    }
                } else {
                    message = "No db data available";
                }

                JOptionPane.showMessageDialog(parentComponent, message, title, JOptionPane.INFORMATION_MESSAGE);
            }
        });
        diskUsageMenu.setEnabled(false);
        diskUsageMenu.add(diskUsageMenuItem);
    }

    protected JFrame getFrame() {
        return this;
    }

    public MnyDb getMnyDb() {
        return mnyDb;
    }

    public void setMnyDb(MnyDb mnyDb) {
        this.mnyDb = mnyDb;
    }
}

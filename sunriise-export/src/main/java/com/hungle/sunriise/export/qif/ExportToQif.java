package com.hungle.sunriise.export.qif;

import org.apache.logging.log4j.Logger;

import com.hungle.sunriise.export.ExportNoPerAccounts;

// TODO: Auto-generated Javadoc
/**
 * The Class ExportToQif.
 */
public class ExportToQif extends ExportNoPerAccounts {

    /** The Constant LOGGER. */
    private static final Logger LOGGER = org.apache.logging.log4j.LogManager.getLogger(ExportToQif.class);

    /**
     * Inits the serializer.
     */
    @Override
    protected void initSerializer() {
        setSerializer(new ExportToQifSerializer(getMnyContext()));
    }
}

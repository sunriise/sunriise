package com.hungle.sunriise.export.ofx;

public class CreditCardStatementTransaction extends AbstractStatementTransaction {
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}

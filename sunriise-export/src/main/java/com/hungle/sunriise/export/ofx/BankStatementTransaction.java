package com.hungle.sunriise.export.ofx;

public class BankStatementTransaction extends AbstractStatementTransaction {
    private String checkNumber;

    public String getCheckNumber() {
        return checkNumber;
    }

    public void setCheckNumber(String checkNumber) {
        this.checkNumber = checkNumber;
    }

}

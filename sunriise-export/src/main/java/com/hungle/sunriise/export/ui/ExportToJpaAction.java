/*******************************************************************************
 * Copyright (c) 2010 Hung Le
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *******************************************************************************/
package com.hungle.sunriise.export.ui;

import java.awt.Component;
import java.io.File;
import java.io.IOException;

import javax.swing.ProgressMonitor;
import javax.swing.SwingUtilities;

import org.apache.logging.log4j.Logger;

import com.healthmarketscience.jackcess.Database;
import com.hungle.sunriise.export.ExportToJpaApplication;

// TODO: Auto-generated Javadoc
/**
 * The Class ExportToCSVAction.
 */
public class ExportToJpaAction extends AbstractExportToAction {

    /** The Constant log. */
    private static final Logger LOGGER = org.apache.logging.log4j.LogManager.getLogger(ExportToJpaAction.class);

    private static final String EXPORT_TYPE_JPA = "*.jpa";

    /**
     * Instantiates a new export to csv action.
     *
     * @param exportToContext the export to context
     */
    public ExportToJpaAction(ExportToContext exportToContext) {
        super(exportToContext);
        this.setExportType(EXPORT_TYPE_JPA);
    }

    @Override
    protected Runnable createExportTask(final ProgressMonitor progressMonitor, File destDir, Component source) {
//        ExportToJpaWithProgressMonitor exporter = new ExportToJpaWithProgressMonitor(progressMonitor);
        ExportToDirExporter exporter = new ExportToDirExporter() {
            private File outDir;

            @Override
            public void setOutDir(File destDir) {
                LOGGER.info("setOutDir - destDir={}", destDir.getAbsolutePath());
                this.outDir = destDir;
            }

            @Override
            public Database export(File mnyFile, String password) throws IOException {
                LOGGER.info("export - mnyFile={}", mnyFile.getAbsolutePath());

                Database db = null;

                File dbFile = new File(outDir, "sunriiseExport");
                String dbUrl = ExportToJpaApplication.JDBC_H2_URL_PREFIX + dbFile.getAbsolutePath();
                if (progressMonitor != null) {
                    try {
                        SwingUtilities.invokeAndWait(new Runnable() {

                            @Override
                            public void run() {
                                progressMonitor.setNote("Starting ...");
                                progressMonitor.setProgress(0);
                            }
                        });
                    } catch (Exception e) {
                        LOGGER.error(e.getMessage(), e);
                    }
                }

                db = ExportToJpaApplication.export(mnyFile, password, dbUrl, progressMonitor);

                return db;
            }
        };

        Runnable task = new ExportToDirTask(this, exporter, destDir, source);
        return task;
    }

}
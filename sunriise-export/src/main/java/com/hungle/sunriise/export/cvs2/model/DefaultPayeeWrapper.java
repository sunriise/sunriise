package com.hungle.sunriise.export.cvs2.model;

import java.io.File;
import java.io.IOException;

import com.hungle.sunriise.mnyobject.Payee;

public class DefaultPayeeWrapper extends AbstractExportToCsv2Wrapper<Payee, Csv2Payee> {
    public DefaultPayeeWrapper(File outFile, boolean appendMode) throws IOException {
        super(outFile, appendMode);
        getMappingStrategy().setType(Csv2Payee.class);
    }

    public DefaultPayeeWrapper(File outFile) throws IOException {
        super(outFile);
        getMappingStrategy().setType(Csv2Payee.class);
    }

    @Override
    protected Csv2Payee wrap(Payee value) {
        Csv2Payee wrappedType = new Csv2Payee(value);
        return wrappedType;

    }

    @Override
    protected String[] getWrapperHeaders() {
        Csv2Payee wrappedType = new Csv2Payee(null);

        String[] headers = wrappedType.getHeaders();

        return headers;
    }
}
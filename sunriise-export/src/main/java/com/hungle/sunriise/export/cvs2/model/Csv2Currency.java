package com.hungle.sunriise.export.cvs2.model;

import java.util.Objects;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.apache.logging.log4j.Logger;

import com.hungle.sunriise.export.cvs2.AbstractCsv2Wrapper;
import com.hungle.sunriise.mnyobject.Currency;
import com.opencsv.bean.CsvBindByPosition;

@Entity
@Table(name = "Currency")
public class Csv2Currency extends AbstractCsv2Wrapper<Currency> {
    private static final Logger LOGGER = org.apache.logging.log4j.LogManager.getLogger(Csv2Currency.class);

    private static final String[] HEADERS = { "id", "name", "isoCode", };

    @Id
    @CsvBindByPosition(position = 0)
    private Integer id;

    @CsvBindByPosition(position = 1)
    private String name;

    @CsvBindByPosition(position = 2)
    private String isoCode;

    public String[] getHeaders() {
        String[] headers = HEADERS;
        return headers;
    }

    public Csv2Currency() {
        this(null);
    }

    public Csv2Currency(Currency value) {
        super(value);

        if (value == null) {
            return;
        }

        this.id = value.getId();
        this.name = value.getName();

        this.isoCode = value.getIsoCode();

    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIsoCode() {
        return isoCode;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Csv2Currency)) {
            return false;
        }
        Csv2Currency currency = (Csv2Currency) o;
        return Objects.equals(getId(), currency.getId());
    }

    @Override
    public int hashCode() {
        return getClass().hashCode();
    }
}

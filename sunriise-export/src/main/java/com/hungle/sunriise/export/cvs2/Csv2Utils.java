package com.hungle.sunriise.export.cvs2;

import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;

import com.hungle.sunriise.export.cvs2.model.DefaultAccountWrapper;
import com.hungle.sunriise.export.cvs2.model.DefaultTransactionWrapper;
import com.hungle.sunriise.mnyobject.Account;
import com.hungle.sunriise.mnyobject.Transaction;
import com.opencsv.CSVReader;

public class Csv2Utils {
    private static final String eol = "\r\n";

    // Transaction
    public static String serialize(Transaction transaction) throws IOException {
        StringBuilder sb = new StringBuilder();

        // main transaction
        sb.append("# Transaction" + eol);
        String csvFormat = Csv2Utils.toCsvFormat(transaction);
        String propertiesFormat = Csv2Utils.toPropertiesFormat(csvFormat);
        sb.append(propertiesFormat);

        return sb.toString();
    }

    private static String toCsvFormat(Transaction transaction) throws IOException {
        String csvFormat = null;
        try (StringWriter writer = new StringWriter()) {
            try (DefaultTransactionWrapper wrapper = new DefaultTransactionWrapper(writer)) {
                List<Transaction> list = new ArrayList<>();
                list.add(transaction);
//                List<TransactionSplit> splits = transaction.getSplits();
//                if (splits != null) {
//                    splits = Csv2Transaction.sortByRowId(splits);
//
//                    for (TransactionSplit split : splits) {
//                        Transaction splitTxn = split.getTransaction();
//                        if (splitTxn != null) {
//                            list.add(splitTxn);
//                        }
//                    }
//                }
                wrapper.writeBeans(list);
            } finally {
                writer.flush();
                csvFormat = writer.toString();
            }
        }
        return csvFormat;
    }

    // Account
    public static String serialize(Account account) throws IOException {
        String csvFormat = Csv2Utils.toCsvFormat(account);

        String propertiesFormat = Csv2Utils.toPropertiesFormat(csvFormat);
        return propertiesFormat;
    }

    private static String toCsvFormat(Account account) throws IOException {
        String csvFormat = null;
        try (StringWriter writer = new StringWriter()) {
            try (DefaultAccountWrapper wrapper = new DefaultAccountWrapper(writer)) {
                List<Account> list = new ArrayList<>();
                list.add(account);
                wrapper.writeBeans(list);
            } finally {
                writer.flush();
                csvFormat = writer.toString();
            }
        }
        return csvFormat;
    }

    private static String toPropertiesFormat(String csvFormat) throws IOException {
        StringBuilder sb = new StringBuilder();
        try (CSVReader reader = new CSVReader(new StringReader(csvFormat))) {
            List<String[]> rows = reader.readAll();
            String[] headers = rows.get(0);

            for (int i = 1; i < rows.size(); i++) {
                String[] values = rows.get(i);
                if (i > 1) {
                    sb.append(eol);
                    sb.append("###" + eol);
                }
                for (int j = 0; j < headers.length; j++) {
                    String header = headers[j];
                    String value = values[j];

                    sb.append(header + ":" + " " + value + eol);
                }
            }
        }

        String propertiesFormat = sb.toString();
        return propertiesFormat;
    }

}

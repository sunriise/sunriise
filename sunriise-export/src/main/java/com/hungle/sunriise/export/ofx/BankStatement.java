package com.hungle.sunriise.export.ofx;

public class BankStatement extends AbstractStatement<BankStatementTransaction> {
    private String bankId = null;

    private String accountType = null;

    public String getBankId() {
        return bankId;
    }

    public void setBankId(String bankId) {
        this.bankId = bankId;
    }

    public String getAccountType() {
        return accountType;
    }

    public void setAccountType(String accountType) {
        this.accountType = accountType;
    }
}

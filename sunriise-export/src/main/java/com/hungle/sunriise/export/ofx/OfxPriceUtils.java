package com.hungle.sunriise.export.ofx;

import java.text.NumberFormat;
import java.util.Locale;

public class OfxPriceUtils {

    public static final NumberFormat createPriceFormatter(Locale locale) {
        NumberFormat priceFormatter = null;
        if (locale == null) {
            priceFormatter = NumberFormat.getNumberInstance();
        } else {
            priceFormatter = NumberFormat.getNumberInstance(locale);
        }
        priceFormatter.setGroupingUsed(false);

        Integer minimumFractionDigits = 4;
        Integer maximumFractionDigits = 4;

        priceFormatter.setMinimumFractionDigits(minimumFractionDigits);
        priceFormatter.setMaximumFractionDigits(maximumFractionDigits);

        return priceFormatter;
    }

    public static NumberFormat createPriceFormatter() {
        return createPriceFormatter(null);
    }

}

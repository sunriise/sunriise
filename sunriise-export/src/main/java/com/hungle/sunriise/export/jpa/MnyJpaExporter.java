package com.hungle.sunriise.export.jpa;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import javax.swing.ProgressMonitor;
import javax.swing.SwingUtilities;

import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.healthmarketscience.jackcess.Database;
import com.hungle.sunriise.export.cvs2.model.Csv2Account;
import com.hungle.sunriise.export.cvs2.model.Csv2Category;
import com.hungle.sunriise.export.cvs2.model.Csv2Currency;
import com.hungle.sunriise.export.cvs2.model.Csv2Payee;
import com.hungle.sunriise.export.cvs2.model.Csv2Security;
import com.hungle.sunriise.export.cvs2.model.Csv2Transaction;
import com.hungle.sunriise.io.MnyDb;
import com.hungle.sunriise.mnyobject.Account;
import com.hungle.sunriise.mnyobject.Category;
import com.hungle.sunriise.mnyobject.Currency;
import com.hungle.sunriise.mnyobject.Payee;
import com.hungle.sunriise.mnyobject.Security;
import com.hungle.sunriise.mnyobject.Transaction;

// TODO: Auto-generated Javadoc
/**
 * The Class MnyJpaExporter.
 */
@Component
public class MnyJpaExporter extends AbstractCrudRepositoryExporter {
    private static final Logger LOGGER = org.apache.logging.log4j.LogManager.getLogger(MnyJpaExporter.class);

    /** The account repository. */
    @Autowired
    private Csv2AccountRepository accountRepository;

    /** The category repository. */
    @Autowired
    private Csv2CategoryRepository categoryRepository;

    /** The currency repository. */
    @Autowired
    private Csv2CurrencyRepository currencyRepository;

    /** The payee repository. */
    @Autowired
    private Csv2PayeeRepository payeeRepository;

    /** The security repository. */
    @Autowired
    private Csv2SecurityRepository securityRepository;

    /** The transaction repository. */
    @Autowired
    private Csv2TransactionRepository transactionRepository;

    private ProgressMonitor progressMonitor;

    private int maxAccounts;

    private String accountName;

    private int count;

    /**
     * Instantiates a new mny jpa exporter.
     */
    public MnyJpaExporter() {
        super();
    }

    /**
     * Export account names.
     *
     * @throws IOException Signals that an I/O exception has occurred.
     */
    @Override
    protected void exportAccountNames() throws IOException {
        if ((progressMonitor != null) && (progressMonitor.isCanceled())) {
            return;
        }

        Collection<Account> collection = getMnyContext().getAccounts().values();
        List<Csv2Account> entities = new ArrayList<>();
        for (Account item : collection) {
            Csv2Account entity = new Csv2Account(item);
            entities.add(entity);
        }
        Csv2AccountRepository repo = this.accountRepository;
        repo.saveAll(entities);
        repo.flush();

        LOGGER.info("exportAccountNames, collection={}, entities={}, repo={}", collection.size(), entities.size(),
                repo.count());
    }

    /**
     * Export categories.
     *
     * @throws IOException Signals that an I/O exception has occurred.
     */
    @Override
    protected void exportCategories() throws IOException {
        if ((progressMonitor != null) && (progressMonitor.isCanceled())) {
            return;
        }

        Collection<Category> collection = getMnyContext().getCategories().values();
        Csv2CategoryRepository repo = this.categoryRepository;

//        Set<Integer> ids = new HashSet<>();
        List<Csv2Category> entities = new ArrayList<>();
        for (Category item : collection) {
            Csv2Category entity = new Csv2Category(item);
//            ids.add(entity.getId());
            entities.add(entity);
//            repo.save(entity);
        }
        repo.saveAll(entities);
        repo.flush();

//        int getOneCount=0;
//        for(Csv2Category entity: entities) {
//            Csv2Category one = repo.getOne(entity.getId());
//            if (one == null) {
//                LOGGER.warn("Canot find id={}", entity.getId());
//            } else {
//                getOneCount++;
//            }
//        }
//        List<Csv2Category> allEntitiy = repo.findAll();
//        Set<Integer> ids2 = new HashSet<>();
//        for(Csv2Category csv2Category: allEntitiy) {
//            ids2.add(csv2Category.getId());
//        }
//        for(Integer id: ids) {
//            if (! ids2.contains(id)) {
//                LOGGER.warn("Canot find id={}", id);
//            }
////        }
//        LOGGER.info("exportCategories, collection={}, entities={}, repo={}, ids={}, getOne={}", collection.size(),
//                entities.size(), repo.count(), ids.size(), getOneCount);
        LOGGER.info("exportCategories, collection={}, entities={}, repo={}", collection.size(), entities.size(),
                repo.count());

    }

    /**
     * Export securities.
     *
     * @throws IOException Signals that an I/O exception has occurred.
     */
    @Override
    protected void exportSecurities() throws IOException {
        if ((progressMonitor != null) && (progressMonitor.isCanceled())) {
            return;
        }

        Collection<Security> collection = getMnyContext().getSecurities().values();
        Csv2SecurityRepository repo = this.securityRepository;

        List<Csv2Security> entities = new ArrayList<>();
        for (Security item : collection) {
            Csv2Security entity = new Csv2Security(item);
            entities.add(entity);
        }
        repo.saveAll(entities);
        repo.flush();

        LOGGER.info("exportSecurities, collection={}, entities={}, repo={}", collection.size(), entities.size(),
                repo.count());
    }

    /**
     * Export currencies.
     *
     * @throws IOException Signals that an I/O exception has occurred.
     */
    @Override
    protected void exportCurrencies() throws IOException {
        if ((progressMonitor != null) && (progressMonitor.isCanceled())) {
            return;
        }

//        Set<Entry<Integer, Currency>> entries = getMnyContext().getCurrencies().entrySet();
//        for(Entry<Integer, Currency> entry: entries) {
//            LOGGER.info("XXX key={}, value={}", entry.getKey(), entry.getValue().getId());
//        }

        Collection<Currency> collection = getMnyContext().getCurrencies().values();
        Csv2CurrencyRepository repo = this.currencyRepository;

        List<Csv2Currency> entities = new ArrayList<>();
        for (Currency item : collection) {
            Csv2Currency entity = new Csv2Currency(item);
//            LOGGER.info("PRE-SAVE item.id={}, entity.id={}", item.getId(), entity.getId());

//            Optional<Csv2Currency> found = repo.findById(entity.getId());
//            if (found.isPresent()) {
//                LOGGER.warn("FOUND XXX entity.id={}", entity.getId());
//            }
            entities.add(entity);
//            entity = repo.save(entity);
//            LOGGER.info("SAVED entity.id={}, {}, {}", entity.getId(), entity.getIsoCode(), entity.getName());
        }
        repo.saveAll(entities);
        repo.flush();
//        for (Currency item : collection) {
//            Csv2Currency entity = repo.getOne(item.getId());
//            if (! entity.getId().equals(item.getId())) {
//                LOGGER.error("XXX, id={}", entity.getId());
//            }
//        }

        LOGGER.info("exportCurrencies, collection={}, entities={}, repo={}", collection.size(), entities.size(),
                repo.count());

    }

    /**
     * Export payees.
     *
     * @throws IOException Signals that an I/O exception has occurred.
     */
    @Override
    protected void exportPayees() throws IOException {
        if ((progressMonitor != null) && (progressMonitor.isCanceled())) {
            return;
        }

        Collection<Payee> collection = getMnyContext().getPayees().values();
        List<Csv2Payee> entities = new ArrayList<>();
        Csv2PayeeRepository repo = this.payeeRepository;

        for (Payee item : collection) {
            Csv2Payee entity = new Csv2Payee(item);
            entities.add(entity);
        }
        repo.saveAll(entities);
        repo.flush();

        LOGGER.info("exportCurrencies, collection={}, entities={}, repo={}", collection.size(), entities.size(),
                repo.count());
    }

    /**
     * Export account detail.
     *
     * @param account the account
     * @throws IOException Signals that an I/O exception has occurred.
     */
    @Override
    protected void exportAccountDetail(Account account) throws IOException {
        if ((progressMonitor != null) && (progressMonitor.isCanceled())) {
            return;
        }

        List<Transaction> collection = account.getTransactions();
        Csv2TransactionRepository repo = this.transactionRepository;

        List<Csv2Transaction> entities = new ArrayList<>();
        for (Transaction item : collection) {
            Csv2Transaction entity = new Csv2Transaction(item);
            entities.add(entity);
        }
        repo.saveAll(entities);
        repo.flush();

        LOGGER.info("exportAccountDetail, collection={}, entities={}, repo={}", collection.size(), entities.size(),
                repo.count());
    }

    @Override
    public void visitAccounts(Map<Integer, Account> accounts) throws IOException {
        if ((progressMonitor != null) && (progressMonitor.isCanceled())) {
            return;
        }

        maxAccounts = accounts.size();
        super.visitAccounts(accounts);
    }

    @Override
    public void visitAccount(Account account) throws IOException {
        if ((progressMonitor != null) && (progressMonitor.isCanceled())) {
            return;
        }
        accountName = account.getName();
        count++;
        SwingUtilities.invokeLater(new Runnable() {

            @Override
            public void run() {
                if (progressMonitor == null) {
                    return;
                }
                progressMonitor.setNote("Account: " + accountName);
                progressMonitor.setProgress((count * 100) / maxAccounts);
            }
        });
        super.visitAccount(account);
    }

    @Override
    public void visitTransaction(Transaction transaction) throws IOException {
        if ((progressMonitor != null) && (progressMonitor.isCanceled())) {
            return;
        }
        super.visitTransaction(transaction);
    }

    /**
     * Delete all.
     */
    public void deleteAll() {
        accountRepository.deleteAll();
        accountRepository.flush();

        categoryRepository.deleteAll();
        categoryRepository.flush();

        currencyRepository.deleteAll();
        currencyRepository.flush();

        payeeRepository.deleteAll();
        payeeRepository.flush();

        securityRepository.deleteAll();
        securityRepository.flush();

        transactionRepository.deleteAll();
        transactionRepository.flush();
    }

    /**
     * Gets the account repository.
     *
     * @return the account repository
     */
    public Csv2AccountRepository getAccountRepository() {
        return accountRepository;
    }

    /**
     * Gets the category repository.
     *
     * @return the category repository
     */
    public Csv2CategoryRepository getCategoryRepository() {
        return categoryRepository;
    }

    /**
     * Gets the currency repository.
     *
     * @return the currency repository
     */
    public Csv2CurrencyRepository getCurrencyRepository() {
        return currencyRepository;
    }

    /**
     * Gets the payee repository.
     *
     * @return the payee repository
     */
    public Csv2PayeeRepository getPayeeRepository() {
        return payeeRepository;
    }

    /**
     * Gets the security repository.
     *
     * @return the security repository
     */
    public Csv2SecurityRepository getSecurityRepository() {
        return securityRepository;
    }

    /**
     * Gets the transaction repository.
     *
     * @return the transaction repository
     */
    public Csv2TransactionRepository getTransactionRepository() {
        return transactionRepository;
    }

    public Database export(MnyDb mnyDb, ProgressMonitor progressMonitor) throws IOException {
        this.progressMonitor = progressMonitor;

        return export(mnyDb);
    }
}
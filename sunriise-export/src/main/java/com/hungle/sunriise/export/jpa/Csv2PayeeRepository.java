package com.hungle.sunriise.export.jpa;

import org.springframework.data.jpa.repository.JpaRepository;

import com.hungle.sunriise.export.cvs2.model.Csv2Payee;

/**
 * The Interface Csv2PayeeRepository.
 */
public interface Csv2PayeeRepository extends JpaRepository<Csv2Payee, Integer> {

}

/*******************************************************************************
 * Copyright (c) 2013 Hung Le
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *******************************************************************************/
package com.hungle.sunriise.export.ofx;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import org.apache.logging.log4j.Logger;

import com.healthmarketscience.jackcess.Database;
import com.hungle.sunriise.io.MnyDb;
import com.hungle.sunriise.mnyobject.Account;
import com.hungle.sunriise.mnyobject.Category;
import com.hungle.sunriise.mnyobject.Currency;
import com.hungle.sunriise.mnyobject.EnumAccountType;
import com.hungle.sunriise.mnyobject.Payee;
import com.hungle.sunriise.mnyobject.Security;
import com.hungle.sunriise.mnyobject.Transaction;
import com.hungle.sunriise.util.StopWatch;

// TODO: Auto-generated Javadoc
/**
 * The Class ExportToJSON.
 */
public class ExportToOfx extends AbstractExportToOfx {

    /** The Constant LOGGER. */
    private static final Logger LOGGER = org.apache.logging.log4j.LogManager.getLogger(ExportToOfx.class);

    /** The out dir. */
    private File outDir;

    /** The stop watch. */
    private StopWatch stopWatch = new StopWatch();

    /** The account dir. */
    private File accountDir;

    /**
     * Gets the out dir.
     *
     * @return the out dir
     */
    public File getOutDir() {
        return outDir;
    }

    /**
     * Sets the out dir.
     *
     * @param outDir the new out dir
     */
    public void setOutDir(File outDir) {
        this.outDir = outDir;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.hungle.sunriise.export.AbstractExportToJSON#export(com.hungle.
     * sunriise.io .MnyDb)
     */
    @Override
    protected Database export(MnyDb mnyDb) throws IOException {
        if (outDir == null) {
            throw new IOException("outDir=null");
        }
        return exportToDir(mnyDb, outDir);
    }

    /**
     * Export.
     *
     * @param mnyDb  the src db
     * @param outDir the out dir
     * @return the database
     * @throws IOException Signals that an I/O exception has occurred.
     */
    protected Database exportToDir(MnyDb mnyDb, File outDir) throws IOException {
        if (mnyDb == null) {
            return null;
        }
        if (mnyDb.getDb() == null) {
            return null;
        }

        startExportToDir(outDir);
        try {
            _visit(mnyDb);
        } finally {
            endExportToDir(outDir);
        }

        return mnyDb.getDb();
    }

    /**
     * Start export.
     *
     * @param outDir the out dir
     */
    protected void startExportToDir(File outDir) {
        stopWatch.click();
        LOGGER.info("> startExport, outDir=" + outDir);
        setOutDir(outDir);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.hungle.sunriise.export.AbstractExportToJSON#exportAccountNames()
     */
    @Override
    protected void exportAccountNames() throws IOException {
        exportAccountNamesToDir(getOutDir());
    }

    /**
     * Export account names to dir.
     *
     * @param outDir the out dir
     * @throws IOException Signals that an I/O exception has occurred.
     */
    private void exportAccountNamesToDir(File outDir) throws IOException {
        Collection<AccountEntry> accountNames = createAccountNamesList();
        File outFile = new File(outDir, AbstractExportToOfx.FILENAME_ACCOUNTS);
        // JSONUtils.writeValue(accountNames, outFile);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.hungle.sunriise.export.AbstractExportToJSON#exportSecurities()
     */
    @Override
    protected void exportSecurities() throws IOException {
        exportSecuritiesToDir(getOutDir());
    }

    /**
     * Export securities to dir.
     *
     * @param outDir the out dir
     * @throws IOException Signals that an I/O exception has occurred.
     */
    private void exportSecuritiesToDir(File outDir) throws IOException {
        File outFile = new File(outDir, AbstractExportToOfx.FILENAME_SECURITIES);
        Collection<Security> securities = this.getMnyContext().getSecurities().values();
        // JSONUtils.writeValue(securities, outFile);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.hungle.sunriise.export.AbstractExportToJSON#exportCurrencies()
     */
    @Override
    protected void exportCurrencies() throws IOException {
        exportCurrenciesToDir(getOutDir());
    }

    /**
     * Export currencies to dir.
     *
     * @param outDir the out dir
     * @throws IOException Signals that an I/O exception has occurred.
     */
    private void exportCurrenciesToDir(File outDir) throws IOException {
        File outFile = new File(outDir, AbstractExportToOfx.FILENAME_CURRENCIES);
        Collection<Currency> currencies = this.getMnyContext().getCurrencies().values();
        // JSONUtils.writeValue(currencies, outFile);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.hungle.sunriise.export.AbstractExportToJSON#exportPayees()
     */
    @Override
    protected void exportPayees() throws IOException {
        exportPayeesToDir(getOutDir());
    }

    /**
     * Export payees to dir.
     *
     * @param outDir the out dir
     * @throws IOException Signals that an I/O exception has occurred.
     */
    private void exportPayeesToDir(File outDir) throws IOException {
        File outFile = new File(outDir, AbstractExportToOfx.FILENAME_PAYEES);
        Collection<Payee> payees = this.getMnyContext().getPayees().values();
        // JSONUtils.writeValue(payees, outFile);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.hungle.sunriise.export.AbstractExportToJSON#exportCategories()
     */
    @Override
    protected void exportCategories() throws IOException {
        exportCategoriesToDir(getOutDir());
    }

    /**
     * Export categories to dir.
     *
     * @param outDir the out dir
     * @throws IOException Signals that an I/O exception has occurred.
     */
    private void exportCategoriesToDir(File outDir) throws IOException {
        File outFile = new File(outDir, AbstractExportToOfx.FILENAME_CATEGORIES);
        Collection<Category> categories = this.getMnyContext().getCategories().values();
        // JSONUtils.writeValue(categories, outFile);
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.hungle.sunriise.export.AbstractExportToJSON#exportAccount(com.hungle.
     * sunriise.mnyobject.Account)
     */
    @Override
    protected void exportAccount(Account account) throws IOException {
        this.accountDir = createAccountDir(account);
        super.exportAccount(account);
    }

    /**
     * Creates the account dir.
     *
     * @param account the account
     * @return the file
     */
    private File createAccountDir(Account account) {
        File accountDir;
        // String suffix = ".d";
        String suffix = null;

        String accountName = account.getName();
        // String fileName = accountName;
        String fileName = toSafeFileName(accountName);
        if (suffix != null) {
            fileName = fileName + suffix;
        }

        File d = new File(outDir, "accounts");
        accountDir = new File(d, fileName);
        accountDir.mkdirs();
        return accountDir;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.hungle.sunriise.export.AbstractExportToJSON#
     * exportAccountFilteredTransactions(com.hungle.sunriise.mnyobject.Account)
     */
    @Override
    protected void exportAccountFilteredTransactions(Account account) throws IOException {
        exportFilteredTransactionsToDir(account, getAccountDir());
    }

    /**
     * Export filtered transactions to dir.
     *
     * @param account the account
     * @param outDir  the out dir
     * @throws IOException Signals that an I/O exception has occurred.
     */
    private void exportFilteredTransactionsToDir(Account account, File outDir) throws IOException {
        File outFile = new File(outDir, AbstractExportToOfx.FILENAME_FILTERED_TRANSACTIONS);
        // JSONUtils.writeValue(account.getFilteredTransactions(), outFile);
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.hungle.sunriise.export.AbstractExportToJSON#exportAccountTransactions
     * (com.hungle.sunriise.mnyobject.Account)
     */
    @Override
    protected void exportAccountTransactions(Account account) throws IOException {
        exportTransactionsToDir(account, getAccountDir());
    }

    /**
     * Export transactions to dir.
     *
     * @param account the account
     * @param outDir  the out dir
     * @throws IOException Signals that an I/O exception has occurred.
     */
    private void exportTransactionsToDir(Account account, File outDir) throws IOException {
        File outFile = new File(outDir, AbstractExportToOfx.FILENAME_TRANSACTIONS);
        // JSONUtils.writeValue(account.getTransactions(), outFile);
        EnumAccountType type = account.getAccountType();
        switch (type) {
        case BANKING:
            exportBankingTransactionsToDir(account, outFile);
            break;
        case CREDIT_CARD:
            exportCreditCardTransactionsToDir(account, outFile);
            break;
        default:
            break;
        }

    }

    private void exportCreditCardTransactionsToDir(Account account, File outFile) throws IOException {
        List<CreditCardStatementTransaction> transactions = toCreditCardStatement(account.getTransactions());
        CreditCardStatement statement = new CreditCardStatement();
        statement.setTransactions(transactions);
        // statement.setAccountType("CHECKING");
        // statement.setBankId("121099999");
        statement.setAccountId("999988");
        statement.setCurrency("USD");
        statement.setDtStartDate(new Date());
        statement.setDtEndDate(new Date());
        statement.setLedgerBalanceBalanceAmount(200.29);
        statement.setLedgerBalanceDateAsOf(new Date());
        OfxCreditcardResponseMessageSetV1 response = new OfxCreditcardResponseMessageSetV1(statement);
        response.save(outFile);
    }

    private void exportBankingTransactionsToDir(Account account, File outFile) throws IOException {
        List<BankStatementTransaction> transactions = toBankStatementTransactions(account.getTransactions());
        BankStatement statement = new BankStatement();
        statement.setTransactions(transactions);
        statement.setAccountType("CHECKING");
        statement.setBankId("121099999");
        statement.setAccountId("999988");
        statement.setCurrency("USD");
        statement.setDtStartDate(new Date());
        statement.setDtEndDate(new Date());
        statement.setLedgerBalanceBalanceAmount(200.29);
        statement.setLedgerBalanceDateAsOf(new Date());
        OfxBankResponseMessageSetV1 response = new OfxBankResponseMessageSetV1(statement);
        response.save(outFile);
    }

    private List<BankStatementTransaction> toBankStatementTransactions(List<Transaction> transactions) {
        List<BankStatementTransaction> list = new ArrayList<BankStatementTransaction>();

        for (Transaction transaction : transactions) {
            BankStatementTransaction t = null;
            t = new BankStatementTransaction();
            t.setType("CHECK");
            t.setId("" + transaction.getId());
            String number = transaction.getNumber();
            if (number == null) {
                number = "";
            }
            t.setCheckNumber(number);
            t.setDtPostedDate(transaction.getDate());
            t.setAmount(transaction.getAmount().doubleValue());
            list.add(t);
        }

        return list;
    }

    private List<CreditCardStatementTransaction> toCreditCardStatement(List<Transaction> transactions) {
        List<CreditCardStatementTransaction> list = new ArrayList<CreditCardStatementTransaction>();

        for (Transaction transaction : transactions) {
            CreditCardStatementTransaction t = null;
            t = new CreditCardStatementTransaction();
            // t.setType("CHECK");
            // t.setId("" + transaction.getId());
            String number = transaction.getNumber();
            if (number == null) {
                number = "";
            }
            // t.setCheckNumber(number);
            // t.setDtPostedDate(transaction.getDate());
            // t.setAmount(transaction.getAmount().doubleValue());
            list.add(t);
        }

        return list;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.hungle.sunriise.export.AbstractExportToJSON#exportAccountDetail(com.
     * hungle.sunriise.mnyobject.Account)
     */
    @Override
    protected void exportAccountDetail(Account account) throws IOException {
        exportDetailToDir(account, getAccountDir());
    }

    /**
     * Export detail to dir.
     *
     * @param account the account
     * @param outDir  the out dir
     * @throws IOException Signals that an I/O exception has occurred.
     */
    private void exportDetailToDir(Account account, File outDir) throws IOException {
        File outFile = new File(outDir, AbstractExportToOfx.FILENAME_ACCOUNT);
        // JSONUtils.writeValue(account, outFile);
    }

    /**
     * End export.
     *
     * @param outDir the out dir
     */
    protected void endExportToDir(File outDir) {
        stopWatch.logTiming(LOGGER, "> endExport, outDir=" + outDir);
    }

    /**
     * Gets the account dir.
     *
     * @return the account dir
     */
    public File getAccountDir() {
        return accountDir;
    }

    /**
     * Sets the account dir.
     *
     * @param accountDir the new account dir
     */
    public void setAccountDir(File accountDir) {
        this.accountDir = accountDir;
    }
}

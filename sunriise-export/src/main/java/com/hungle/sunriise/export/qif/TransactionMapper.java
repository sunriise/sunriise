package com.hungle.sunriise.export.qif;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;
import java.util.List;

import org.apache.logging.log4j.Logger;

import com.hungle.sunriise.dbutil.TableCategoryUtils;
import com.hungle.sunriise.mnyobject.Account;
import com.hungle.sunriise.mnyobject.EnumAccountType;
import com.hungle.sunriise.mnyobject.InvestmentInfo;
import com.hungle.sunriise.mnyobject.InvestmentTransaction;
import com.hungle.sunriise.mnyobject.MnyContext;
import com.hungle.sunriise.mnyobject.PayeeLink;
import com.hungle.sunriise.mnyobject.Security;
import com.hungle.sunriise.mnyobject.Transaction;
import com.hungle.sunriise.mnyobject.TransactionSplit;
import com.hungle.sunriise.mnyobject.XferInfo;
import com.hungle.sunriise.mnyobject.impl.InvestmentActivity;

// TODO: Auto-generated Javadoc
/**
 * The Class TransactionWrapper.
 */
public class TransactionMapper {

    // https://github.com/jemmyw/Qif/blob/master/QIF_references

    /** The Constant LOGGER. */
    private static final Logger LOGGER = org.apache.logging.log4j.LogManager.getLogger(TransactionMapper.class);

    /** The transaction. */
    private final Transaction transaction;

    /** The mny context. */
    private final MnyContext mnyContext;

    /** The account. */
    private final Account account;

    /**
     * Instantiates a new transaction wrapper.
     *
     * @param mnyContext  the mny context
     * @param account     the account
     * @param transaction the transaction
     */
    public TransactionMapper(MnyContext mnyContext, Account account, Transaction transaction) {
        super();
        this.mnyContext = mnyContext;
        this.account = account;
        this.transaction = transaction;
    }

    /**
     * Write investment transaction block.
     *
     * @param context     the context
     * @param transaction the transaction
     * @param writer      the writer
     */
    private static void writeInvestmentTransactionBlock(MnyContext context, Transaction transaction,
            PrintWriter writer) {
        /*--
        Items for Investment Accounts
        
        Field   Indicator Explanation
        D   Date
        N   Action
        Y   Security
        I   Price
        Q   Quantity (number of shares or split ratio)
        T   Transaction amount
        C   Cleared status
        P   Text in the first line for transfers and reminders
        M   Memo
        O   Commission
        L   Account for the transfer
        $   Amount transferred
        ^   End of the entry        
         */

        /*--
        !Type:Invst
        D8/19'2000
        T19,939.00
        NBuy
        YContoso Pharmaceuticals
        I49.75
        Q400
        O39.00
        ^
        D2/27'2001
        T292.50
        NDiv
        YConsolidated Messengers
        I
        Q
        LInvestment Income:Dividends
        ^
        D3/8'2001
        T36.00
        NDiv
        YContoso Pharmaceuticals
        I
        Q
        LInvestment Income:Dividends
        ^
        
         */

        InvestmentInfo investmentInfo = transaction.getInvestmentInfo();
        InvestmentTransaction investmentTransaction = investmentInfo.getTransaction();

        // D Date
        writer.println("D" + TransactionMapper.getDate(transaction));

        // T Transaction amount
        writer.println("T" + TransactionMapper.getAmount(transaction));

        if (investmentInfo != null) {
            // N Action
            writer.println("N" + TransactionMapper.getInvestmentAction(investmentInfo));

            // Y Security
            writer.println("Y" + TransactionMapper.getInvestmentSecurity(investmentInfo));
        }

        if (investmentTransaction != null) {
            // I Price
            writer.println("I" + TransactionMapper.getInvestmentPrice(investmentTransaction));

            // Q Quantity (number of shares or split ratio)
            writer.println("Q" + TransactionMapper.getInvestmentQuantity(investmentTransaction));
        }

        // L Account for the transfer
//        writer.println("L" + TransactionMapper.getCategory(context, transaction));
    }

    private static String getInvestmentQuantity(InvestmentTransaction investmentTransaction) {
        String value = "";

        if (investmentTransaction != null) {
            Double quantity = investmentTransaction.getQuantity();
            if (quantity != null) {
                value = String.format("%.2f", quantity);
            }
        }
        return value;
    }

    private static String getInvestmentPrice(InvestmentTransaction investmentTransaction) {
        String value = "";

        if (investmentTransaction != null) {
            Double price = investmentTransaction.getPrice();
            if (price != null) {
                value = String.format("%.2f", price);
            }
        }
        return value;
    }

    private static String getInvestmentSecurity(InvestmentInfo investmentInfo) {
        String value = "";
        Security security = investmentInfo.getSecurity();
        if (security != null) {
            value = security.getName();
        }
        return value;
    }

    private static String getInvestmentAction(InvestmentInfo investmentInfo) {
        String value = "";
        InvestmentActivity activity = investmentInfo.getActivity();
        if (activity != null) {
            value = activity.getLabel();
        }
        return value;
    }

    /**
     * Write non investment transaction block.
     *
     * @param context     the context
     * @param transaction the transaction
     * @param writer      the writer
     */
    private static void writeNonInvestmentTransactionBlock(MnyContext context, Transaction transaction,
            PrintWriter writer) {
        /*--
        Items for Non-Investment Accounts
        
        Each item in a bank, cash, credit card, other liability, or other asset account must begin with a letter that indicates the field in the Quicken register. The non-split items can be in any sequence:
        Field   Indicator Explanation
        D   Date
        T   Amount
        C   Cleared status
        N   Num (check or reference number)
        P   Payee
        M   Memo
        A   Address (up to five lines; the sixth line is an optional message)
        L   Category (Category/Subcategory/Transfer/Class)
        S   Category in split (Category/Transfer/Class)
        E   Memo in split
        $   Dollar amount of split
        ^   End of the entry
        Note: Repeat the S, E, and $ lines as many times as needed for additional items in a split. If an item is omitted from the transaction in the QIF file, Quicken treats it as a blank item.
        
         */
        writer.println("D" + TransactionMapper.getDate(transaction));
        writer.println("T" + TransactionMapper.getAmount(transaction));
        writer.println("N" + TransactionMapper.getCheckNumber(transaction));
        writer.println("P" + TransactionMapper.getPayee(transaction));
        writer.println("L" + TransactionMapper.getCategory(context, transaction));

        if (transaction.hasSplits()) {
            // SBudget:Food
            // $50.00
            // SAssets:Budgeted Cash
            // $-50.00
            // SIncome
            // $100.00
            List<TransactionSplit> splits = transaction.getSplits();
            for (TransactionSplit split : splits) {
                Transaction txn = split.getTransaction();

                writer.println("S" + TransactionMapper.getCategory(context, txn));
                writer.println("$" + TransactionMapper.getAmount(txn));
            }
        }
    }

    /**
     * Gets the transaction amount.
     *
     * @param transaction the transaction
     * @return the transaction amount
     */
    private static final String getAmount(Transaction transaction) {
        BigDecimal amount = transaction.getAmount();

        return String.format("%.2f", amount);
    }

    /**
     * Gets the transaction category.
     *
     * @param context     the context
     * @param transaction the transaction
     * @return the transaction category
     */
    private static final String getCategory(MnyContext context, Transaction transaction) {
        String value = null;

        XferInfo xferInfo = transaction.getXferInfo();
        Integer xferAccountId = xferInfo.getXferAccountId();
        // MnyContext context = mnyContext;

        if (xferAccountId != null) {
            value = TableCategoryUtils.getCategoryFullNameForXfer(xferAccountId, transaction, context, false);
            if (value != null) {
                value = "[" + value + "]";
            }
        } else {
            Integer categoryId = transaction.getCategory().getId();
            String categoryName = TableCategoryUtils.getCategoryFullName(categoryId, context);
            value = categoryName;
        }

        if (value == null) {
            if (transaction.hasSplits()) {
                // SBudget:Food
                // $50.00
                // SAssets:Budgeted Cash
                // $-50.00
                // SIncome
                // $100.00
                List<TransactionSplit> splits = transaction.getSplits();
                value = "(" + splits.size() + ") Split Transaction";
            }
        }

        if (transaction.isInvestment()) {
            value = TableCategoryUtils.getCategoryFullNameForInvestment(transaction, context);
        }

        return value;
    }

    /**
     * Gets the transaction check number.
     *
     * @param transaction the transaction
     * @return the transaction check number
     */
    private static final String getCheckNumber(Transaction transaction) {
        String number = transaction.getNumber();
        if (number == null) {
            number = "";
        }
        return number;
    }

    /**
     * Gets the transaction date.
     *
     * @param transaction the transaction
     * @return the transaction date
     */
    private static final String getDate(Transaction transaction) {
        Date date = transaction.getDate();
        // D12/17'19
        // D12/17/19
        LocalDate localDate = date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
        int month = localDate.getMonthValue();
        int dayOfMonth = localDate.getDayOfMonth();
        int year = localDate.getYear();
        int year2 = (year % 1000) % 100;

        String str = null;

        if (year < 2000) {
            str = String.format("%02d/%02d/%02d", month, dayOfMonth, year2);
        } else {
            str = String.format("%02d/%02d/'%02d", month, dayOfMonth, year2);
        }

        return str;
    }

    /**
     * Gets the transaction payee.
     *
     * @param transaction the transaction
     * @return the transaction payee
     */
    private static final String getPayee(Transaction transaction) {
        String str = "";
        PayeeLink payee = transaction.getPayee();
        if (payee != null) {
            str = payee.getName();
            if (str == null) {
                str = "";
            }
        }
        return str;
    }

    /**
     * Serialize.
     *
     * @param writer the writer
     */
    public void writeValue(PrintWriter writer) {
        // D6/ 1/94 Date
        // T-1,000.00 Amount
        // N1005 Check number
        // PBank Of Mortgage Payee
        // L[linda] Category
        // http://linuxfinances.info/info/financeformats.html
        try {
            if (account.getAccountType() == EnumAccountType.INVESTMENT) {
                TransactionMapper.writeInvestmentTransactionBlock(mnyContext, transaction, writer);
            } else {
                TransactionMapper.writeNonInvestmentTransactionBlock(mnyContext, transaction, writer);
            }
        } finally {
            writer.println("^");
        }

    }

    /**
     * Serialize.
     *
     * @param mnyContext  the mny context
     * @param account     the account
     * @param transaction the transaction
     * @return the string
     */
    public static String getTransactionBlock(MnyContext mnyContext, Account account, Transaction transaction) {
        String value = null;
        StringWriter writer = null;
        try {
            writer = new StringWriter();
            try (PrintWriter printWriter = new PrintWriter(writer)) {
                writeTransactionBlock(mnyContext, account, transaction, printWriter);
            }
        } finally {
            if (writer != null) {
                writer.flush();
                value = writer.getBuffer().toString();
                try {
                    writer.close();
                } catch (IOException e) {
                    LOGGER.error(e.getMessage());
                }
            }
        }

        return value;
    }

    /**
     * Write transaction block.
     *
     * @param mnyContext  the context
     * @param account     the account
     * @param transaction the transaction
     * @param writer      the writer
     */
    static final void writeTransactionBlock(MnyContext mnyContext, Account account, Transaction transaction,
            PrintWriter writer) {
        TransactionMapper wrapper = new TransactionMapper(mnyContext, account, transaction);
        wrapper.writeValue(writer);
    }
}

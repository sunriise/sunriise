package app;

import java.util.Arrays;

// TODO: Auto-generated Javadoc
/**
 * The Class ModuleExportMain.
 */
public class ModuleExportMain {

    /** The Constant CMD_EXPORT_CSV. */
    static final String CMD_EXPORT_CSV = "export.csv";

    /** The Constant CMD_EXPORT_JSON. */
    static final String CMD_EXPORT_JSON = "export.json";

    /** The Constant CMD_EXPORT_MDB. */
    static final String CMD_EXPORT_MDB = "export.mdb";

    /** The Constant CMD_EXPORT_CSV2. */
    static final String CMD_EXPORT_CSV2 = "export.csv2";

    static final String CMD_EXPORT_QIF = "export.qif";

    static final String CMD_EXPORT_JPA = "export.jpa";

    /** The Constant CMD_REPORT_SEC_PRICES. */
    static final String CMD_REPORT_SEC_PRICES = "report.secPrices";

    /**
     * Usage.
     */
    private static void usage() {
        Class<ModuleExportMain> clz = ModuleExportMain.class;
        System.out.println("Usage: java " + clz.getName() + " cmd cmdArgs ...");
        System.out.println("  " + CMD_EXPORT_CSV + " to export mdb rows to CSV");
        System.out.println("  " + CMD_EXPORT_CSV2 + " to export transactions to CSV");
        System.out.println("  " + CMD_EXPORT_JSON + " to export to JSON");
        System.out.println("  " + CMD_EXPORT_MDB + " to export to MDB");
        System.out.println("  " + CMD_EXPORT_QIF + " to export to QIF");
        System.out.println("  " + CMD_EXPORT_JPA + " to export to database (using JPA)");
        System.out.println("  " + CMD_REPORT_SEC_PRICES + " to generate a report of latest security prices");
    }

    /**
     * The main method.
     *
     * @param args the arguments
     */
    public static void main(String[] args) {
        String cmd = null;
        String[] newArgs = null;

        if (args.length == 0) {
            cmd = "";
            newArgs = new String[0];
        } else {
            cmd = args[0];
            newArgs = Arrays.copyOfRange(args, 1, args.length);
        }

        if (cmd.compareToIgnoreCase(CMD_EXPORT_CSV) == 0) {
            app.ExportToCsv.main(newArgs);
        } else if (cmd.compareToIgnoreCase(CMD_EXPORT_CSV2) == 0) {
            app.ExportToCsv2.main(newArgs);
        } else if (cmd.compareToIgnoreCase(CMD_EXPORT_JSON) == 0) {
            app.ExportToJSON.main(newArgs);
        } else if (cmd.compareToIgnoreCase(CMD_EXPORT_MDB) == 0) {
            app.ExportToMdb.main(newArgs);
        } else if (cmd.compareToIgnoreCase(CMD_EXPORT_QIF) == 0) {
            app.ExportToQif.main(newArgs);
        } else if (cmd.compareToIgnoreCase(CMD_EXPORT_JPA) == 0) {
            app.ExportToJpa.main(newArgs);
        } else if (cmd.compareToIgnoreCase(CMD_REPORT_SEC_PRICES) == 0) {
            app.GetLatestSecurityPrices.main(newArgs);
        } else if ((cmd.compareToIgnoreCase("--help") == 0) || (cmd.compareToIgnoreCase("-help") == 0)
                || (cmd.compareToIgnoreCase("-h") == 0)) {
            ModuleExportMain.usage();
        } else {
            app.MnyExporter.main(newArgs);
        }

    }

}

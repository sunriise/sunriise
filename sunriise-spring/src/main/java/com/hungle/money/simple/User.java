package com.hungle.money.simple;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.NamedQuery;

import org.springframework.data.jpa.domain.AbstractPersistable;

// TODO: Auto-generated Javadoc
/**
 * Sample user class.
 *
 * @author Oliver Gierke
 * @author Thomas Darimont
 */
@Entity
@NamedQuery(name = "User.findByTheUsersName", query = "from User u where u.username = ?1")
public class User extends AbstractPersistable<Long> {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = -2952735933715107252L;

    /** The username. */
    @Column(unique = true)
    private String username;

    /** The firstname. */
    private String firstname;

    /** The lastname. */
    private String lastname;

    /**
     * Instantiates a new user.
     */
    public User() {
        this(null);
    }

    /**
     * Creates a new user instance.
     *
     * @param id the id
     */
    public User(Long id) {
        this.setId(id);
    }

    /**
     * Returns the username.
     *
     * @return the username
     */
    public String getUsername() {

        return username;
    }

    /**
     * Sets the username.
     *
     * @param username the username to set
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     * Gets the firstname.
     *
     * @return the firstname
     */
    public String getFirstname() {
        return firstname;
    }

    /**
     * Sets the firstname.
     *
     * @param firstname the firstname to set
     */
    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    /**
     * Gets the lastname.
     *
     * @return the lastname
     */
    public String getLastname() {
        return lastname;
    }

    /**
     * Sets the lastname.
     *
     * @param lastname the lastname to set
     */
    public void setLastname(String lastname) {
        this.lastname = lastname;
    }
}
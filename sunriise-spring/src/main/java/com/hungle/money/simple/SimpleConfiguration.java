package com.hungle.money.simple;

import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * The Class SimpleConfiguration.
 */
@SpringBootApplication
class SimpleConfiguration {
}
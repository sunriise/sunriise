package com.hungle.money;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;

import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@SpringBootApplication
@EnableSwagger2
@ComponentScan("com.hungle.money.component,com.hungle.money.controller")
public class Application {
    private static final Logger LOGGER = LogManager.getLogger(Application.class);

    public static void main(String[] args) {
        // http://localhost:8080/api/msmoney/accounts

        SpringApplication app = null;
        try {
            app = new SpringApplication(Application.class);
            app.run(args);
        } finally {
            if (app != null) {
                app = null;
            }
        }

    }

    public static final Path findMnyFile(Path targetDir, String suffix) throws IOException {
        return Files.list(targetDir).filter((p) -> {
            if (Files.isRegularFile(p)) {
                return p.getFileName().toString().endsWith(suffix);
            } else {
                return false;
            }
        }).findFirst().orElse(null);
    }

    @Bean
    public Docket moneyApi() {
        // http://localhost:8080/swagger-ui.html
        return new Docket(DocumentationType.SWAGGER_2).select()
//                .apis(RequestHandlerSelectors.any())
                .apis(RequestHandlerSelectors.basePackage("com.hungle.money.component")).paths(PathSelectors.any())
                .build();
    }

    public static final String argsGetPassword(ApplicationArguments args) {
        String password = null;
        if (args.containsOption("password")) {
            List<String> values = args.getOptionValues("password");
            if (values.size() > 0) {
                password = values.get(0);
            }
        }
        return password;
    }

    public static final String argsGetFileName(ApplicationArguments args) {
        String fileName = null;
        if (args.containsOption("file")) {
            List<String> values = args.getOptionValues("file");
            if (values.size() > 0) {
                fileName = values.get(0);
            }
        }
        return fileName;
    }

    private static final void argsSetFileName(String absolutePath, ApplicationArguments args) {
        args.getSourceArgs();

    }
}

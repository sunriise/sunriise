package com.hungle.money.controller;

import java.io.IOException;
import java.util.Date;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.annotation.JsonView;
import com.hungle.money.component.AccountFilterArg;
import com.hungle.money.component.AccountNotFoundException;
import com.hungle.money.component.CategoryNotFoundException;
import com.hungle.money.component.CurrencyNotFoundException;
import com.hungle.money.component.EnumAccountStatus;
import com.hungle.money.component.MnyFilterFactory;
import com.hungle.money.component.MsMoneyModelInterface;
import com.hungle.money.component.PayeeNotFoundException;
import com.hungle.money.component.SecurityNotFoundException;
import com.hungle.money.component.TransactionFilterArg;
import com.hungle.money.component.TransactionNotFoundException;
import com.hungle.sunriise.mnyobject.Account;
import com.hungle.sunriise.mnyobject.Category;
import com.hungle.sunriise.mnyobject.Currency;
import com.hungle.sunriise.mnyobject.EnumAccountType;
import com.hungle.sunriise.mnyobject.Payee;
import com.hungle.sunriise.mnyobject.Security;
import com.hungle.sunriise.mnyobject.Transaction;
import com.hungle.sunriise.mnyobject.View;
import com.hungle.sunriise.util.MnyFilter;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

@RestController
@RequestMapping("/api/msmoney")
@Api(value = "API to query content of an *.mny file.")
public class MsMoneyController {
    private static final Logger LOGGER = LogManager.getLogger(MsMoneyController.class);

    @Autowired
    private MsMoneyModelInterface model;

    @ApiOperation(value = "View a list of accounts", response = List.class)
    @JsonView(View.Summary.class)
    @GetMapping("/accounts")
    public List<Account> getAccounts(
            @ApiParam(value = "Account status", required = false) @RequestParam(required = false, defaultValue = "ALL") EnumAccountStatus status,
            @ApiParam(value = "Account type", required = false) @RequestParam(required = false) EnumAccountType type,
            @ApiParam(value = "Account name", required = false) @RequestParam(required = false) String name)
            throws IOException {

        LOGGER.info("getAccounts, status={}, type={}, name={}", status, type, name);

//        AccountStatus accountStatus = AccountStatus.ALL;
//        if (StringUtils.hasText(status)) {
//            accountStatus = AccountStatus.valueOf(status.toUpperCase());
//        }
        AccountFilterArg arg = new AccountFilterArg(status, type, name);
        MnyFilter<Account> filter = MnyFilterFactory.createAndAccountFilter(arg);

        List<Account> accounts = model.getAccounts(filter);
        return accounts;
    }

    @ApiOperation(value = "View an account")
    @GetMapping("/account/{id}")
    public Account getAccount(@ApiParam(value = "Account id", required = true) @PathVariable Integer id)
            throws IOException {
        LOGGER.info("getAccount, id=" + id);

        Account account = model.getAccount(id);
        if (account == null) {
            throw new AccountNotFoundException(id);
        }
        return account;
    }

    @ApiOperation(value = "View a list of account's transactions", response = List.class)
    @JsonView(View.Summary.class)
    @GetMapping("/account/{id}/transactions")
    public List<Transaction> getTransactions(@ApiParam(value = "Account id", required = true) @PathVariable Integer id,
            @ApiParam(value = "Limit transaction created after than this date. Format yyyy-MM-dd, for example 2010-06-15.", required = false) @RequestParam(required = false) @DateTimeFormat(pattern = MnyFilterFactory.DATE_FORMAT_PATTERN) Date fromDate,
            @ApiParam(value = "Limit transaction created before than this date. Format yyyy-MM-dd, for example 2010-06-15.", required = false) @RequestParam(required = false) @DateTimeFormat(pattern = MnyFilterFactory.DATE_FORMAT_PATTERN) Date toDate,
            @ApiParam(value = "Limit transaction with amount greater than or equal to fromAmount.", required = false) @RequestParam(required = false) Double fromAmount,
            @ApiParam(value = "Limit transaction with amount less than or equal to toAmount.", required = false) @RequestParam(required = false) Double toAmount,
            @ApiParam(value = "Search for this substring in account.name, category.name, payee.name", required = false) @RequestParam(required = false) String text,
            @ApiParam(value = "Similar to text but is regular expression.", required = false) @RequestParam(required = false) String regex)
            throws IOException {

        LOGGER.info("getTransactions, id={}, fromDate={}, toDate={}" + ", fromAmount={}, toAmount={}"
                + ", text={}, regex={}", id, fromDate, toDate, fromAmount, toAmount, text, regex);
        TransactionFilterArg arg = new TransactionFilterArg(fromDate, toDate, fromAmount, toAmount, text, regex);
        MnyFilter<Transaction> filter = MnyFilterFactory.createAndTransactionFilter(arg);

        List<Transaction> transactions = model.getTransactions(id, filter);
        if (transactions == null) {
            throw new AccountNotFoundException(id);
        }

        return transactions;
    }

    @ApiOperation(value = "View a transaction")
    @GetMapping("/account/{id}/transaction/{tid}")
    public Transaction getTransaction(@ApiParam(value = "Account id", required = true) @PathVariable Integer id,
            @ApiParam(value = "Transaction id", required = true) @PathVariable Integer tid) throws IOException {
        LOGGER.info("getTransaction, id=" + id + ", tid=" + tid);

        Transaction transaction = null;

        transaction = model.getTransaction(id, tid);

        if (transaction == null) {
            throw new TransactionNotFoundException(tid);
        }

        return transaction;
    }

    // Category
    @ApiOperation(value = "View a list of category", response = List.class)
    @JsonView(View.Summary.class)
    @GetMapping("/categories")
    public List<Category> getCategories(
            @ApiParam(value = "Category name", required = false) @RequestParam(required = false) String name) {

        LOGGER.info("getCategories, name={}", name);

//        AccountFilterArg arg = new AccountFilterArg(status, type, name);
//        MnyFilter<Account> filter = MnyFilterFactory.createAndAccountFilter(arg);
        MnyFilter<Category> filter = null;

        List<Category> categories = model.getCategories(filter);
        return categories;
    }

    @ApiOperation(value = "View a category")
    @GetMapping("/category/{id}")
    public Category getCategory(@ApiParam(value = "Category id", required = true) @PathVariable Integer id) {
        LOGGER.info("getCategory, id=" + id);

        Category category = model.getCategory(id);
        if (category == null) {
            throw new CategoryNotFoundException(id);
        }
        return category;
    }

    // Currency
    @ApiOperation(value = "View a list of currency", response = List.class)
    @JsonView(View.Summary.class)
    @GetMapping("/currencies")
    public List<Currency> getCurrencies(
            @ApiParam(value = "Currency name", required = false) @RequestParam(required = false) String name) {

        LOGGER.info("getCurrencies, name={}", name);

//        AccountFilterArg arg = new AccountFilterArg(status, type, name);
//        MnyFilter<Account> filter = MnyFilterFactory.createAndAccountFilter(arg);
        MnyFilter<Currency> filter = null;

        List<Currency> currencies = model.getCurrencies(filter);
        return currencies;
    }

    @ApiOperation(value = "View a currency")
    @GetMapping("/currency/{id}")
    public Currency getCurrency(@ApiParam(value = "Currency id", required = true) @PathVariable Integer id) {
        LOGGER.info("getCurrency, id=" + id);

        Currency currency = model.getCurrency(id);
        if (currency == null) {
            throw new CurrencyNotFoundException(id);
        }
        return currency;
    }

    // Payee
    @ApiOperation(value = "View a list of payee", response = List.class)
    @JsonView(View.Summary.class)
    @GetMapping("/payees")
    public List<Payee> getPayees(
            @ApiParam(value = "Payee name", required = false) @RequestParam(required = false) String name) {

        LOGGER.info("getPayees, name={}", name);

//        AccountFilterArg arg = new AccountFilterArg(status, type, name);
//        MnyFilter<Account> filter = MnyFilterFactory.createAndAccountFilter(arg);
        MnyFilter<Payee> filter = null;

        List<Payee> payees = model.getPayees(filter);
        return payees;
    }

    @ApiOperation(value = "View a payee")
    @GetMapping("/payee/{id}")
    public Payee getPayee(@ApiParam(value = "Payee id", required = true) @PathVariable Integer id) {
        LOGGER.info("getPayee, id=" + id);

        Payee payee = model.getPayee(id);
        if (payee == null) {
            throw new PayeeNotFoundException(id);
        }
        return payee;
    }

    // Security
    @ApiOperation(value = "View a list of security", response = List.class)
    @JsonView(View.Summary.class)
    @GetMapping("/securities")
    public List<Security> getSecurities(
            @ApiParam(value = "Security name", required = false) @RequestParam(required = false) String name) {

        LOGGER.info("getSecurities, name={}", name);

//        AccountFilterArg arg = new AccountFilterArg(status, type, name);
//        MnyFilter<Account> filter = MnyFilterFactory.createAndAccountFilter(arg);
        MnyFilter<Security> filter = null;

        List<Security> payees = model.getSecurities(filter);
        return payees;
    }

    @ApiOperation(value = "View a security")
    @GetMapping("/security/{id}")
    public Security getSecurity(@ApiParam(value = "Security id", required = true) @PathVariable Integer id) {
        LOGGER.info("getSecurity, id=" + id);

        Security payee = model.getSecurity(id);
        if (payee == null) {
            throw new SecurityNotFoundException(id);
        }
        return payee;
    }

}

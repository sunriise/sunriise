package com.hungle.money.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class RedirectToHomePage {
    @RequestMapping({ "/", "/help", "/home", "/about" })
    public String index() {
        return "redirect:/api/msmoney/accounts";
    }
}

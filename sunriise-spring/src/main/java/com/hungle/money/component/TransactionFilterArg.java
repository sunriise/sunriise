package com.hungle.money.component;

import java.util.Date;

public class TransactionFilterArg {
    final private Date fromDate;
    final private Date toDate;
    final private Double fromAmount;
    final private Double toAmount;
    final private String text;
    final private String regex;

    public TransactionFilterArg(Date fromDate, Date toDate, Double fromAmount, Double toAmount, String text,
            String regex) {
        super();
        this.fromDate = fromDate;
        this.toDate = toDate;
        this.fromAmount = fromAmount;
        this.toAmount = toAmount;
        this.text = text;
        this.regex = regex;
    }

    public Date getFromDate() {
        return fromDate;
    }

    public Date getToDate() {
        return toDate;
    }

    public Double getFromAmount() {
        return fromAmount;
    }

    public Double getToAmount() {
        return toAmount;
    }

    public String getText() {
        return text;
    }

    public String getRegex() {
        return regex;
    }
}
package com.hungle.money.component;

import java.io.IOException;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.hungle.sunriise.mnyobject.Account;
import com.hungle.sunriise.mnyobject.AccountLink;
import com.hungle.sunriise.mnyobject.CategoryLink;
import com.hungle.sunriise.mnyobject.EnumAccountType;
import com.hungle.sunriise.mnyobject.PayeeLink;
import com.hungle.sunriise.mnyobject.Transaction;
import com.hungle.sunriise.util.AndMnyFilter;
import com.hungle.sunriise.util.BalanceUtils;
import com.hungle.sunriise.util.MnyFilter;

public class MnyFilterFactory {
    public static final String DATE_FORMAT_PATTERN = "yyyy-MM-dd";
    static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat(DATE_FORMAT_PATTERN);

    private MnyFilterFactory() {
        super();
    }

    private static MnyFilter<Account> createAccountStatusFilter(EnumAccountStatus status) {
        MnyFilter<Account> subFilter = new MnyFilter<Account>() {
            @Override
            public boolean accepts(Account account) {
                Boolean closed = account.getClosed();

                switch (status) {
                case ALL:
                    return true;

                case CLOSE:
                    return closed;

                case OPEN:
                    return !closed;
                }

                return false;
            }
        };
        return subFilter;
    }

    private static MnyFilter<Account> createAccountTypeFilter(final EnumAccountType type) {
        MnyFilter<Account> subFilter = new MnyFilter<Account>() {
            @Override
            public boolean accepts(Account account) {
                EnumAccountType accountType = account.getAccountType();
                if (type.equals(accountType)) {
                    return true;
                }
                return false;
            }
        };
        return subFilter;
    }

    private static MnyFilter<Account> createAccountNameFilter(final String name) {
        MnyFilter<Account> subFilter = new MnyFilter<Account>() {

            @Override
            public boolean accepts(Account account) {
                String accountName = account.getName();
                if (org.springframework.util.StringUtils.hasText(accountName)) {
                    return accountName.contains(name);
                }
                return false;
            }
        };
        return subFilter;
    }

    static MnyFilter<Transaction> createTransactionToDateFilter(final Date when) {
        MnyFilter<Transaction> subFilter = new MnyFilter<Transaction>() {

            @Override
            public boolean accepts(Transaction transaction) {
                Date transactionDate = transaction.getDate();
                // true if transactionDate <= when
                return (transactionDate.before(when) || transactionDate.equals(when));
            }
        };
        return subFilter;
    }

    static MnyFilter<Transaction> createTransactionFromDateFilter(final Date when) {
        MnyFilter<Transaction> subFilter = new MnyFilter<Transaction>() {

            @Override
            public boolean accepts(Transaction transaction) {
                Date transactionDate = transaction.getDate();
                // true if transactionDate >= when
                return (transactionDate.after(when) || transactionDate.equals(when));
            }
        };
        return subFilter;
    }

    static MnyFilter<Transaction> createTransactionToAmountFilter(Double toAmount) {
        MnyFilter<Transaction> subFilter = new MnyFilter<Transaction>() {
            final BigDecimal maximumAmount = BalanceUtils.formatBalance(new BigDecimal(toAmount));

            @Override
            public boolean accepts(Transaction transaction) {
                BigDecimal amount = transaction.getAmount();
                int i = amount.compareTo(maximumAmount);
                // true if amount <= criteria
                return (i <= 0);
            }
        };
        return subFilter;
    }

    static MnyFilter<Transaction> createTransactionFromAmount(Double fromAmount) {
        MnyFilter<Transaction> subFilter = new MnyFilter<Transaction>() {
            final BigDecimal miniumAmount = BalanceUtils.formatBalance(new BigDecimal(fromAmount));

            @Override
            public boolean accepts(Transaction transaction) {
                BigDecimal amount = transaction.getAmount();
                int i = amount.compareTo(miniumAmount);
                // true if amount >= criteria
                return (i >= 0);
            }
        };
        return subFilter;
    }

    static MnyFilter<Transaction> createTransactionRegexFilter(String regex) {
        MnyFilter<Transaction> subFilter = new MnyFilter<Transaction>() {

            @Override
            public boolean accepts(Transaction transaction) {
                String str = createTransactionText(transaction);

                return str.matches(regex);
            }
        };
        return subFilter;
    }

    static MnyFilter<Transaction> createTransactionTextFilter(String text) {
        MnyFilter<Transaction> subFilter = new MnyFilter<Transaction>() {

            @Override
            public boolean accepts(Transaction transaction) {
                String str = createTransactionText(transaction);

                return str.contains(text);
            }
        };
        return subFilter;
    }

    private static final void appendIfHasText(String str, StringBuilder sb) {
        if (org.springframework.util.StringUtils.hasText(str)) {
            sb.append(" ");
            sb.append(str);
        }
    }

    private static final String createTransactionText(Transaction transaction) {
        StringBuilder sb = new StringBuilder();

        AccountLink account = transaction.getAccount();
        if (account != null) {
            appendIfHasText(account.getName(), sb);
        }

        CategoryLink category = transaction.getCategory();
        if (category != null) {
            appendIfHasText(category.getFullName(), sb);
        }

        PayeeLink payee = transaction.getPayee();
        if (payee != null) {
            appendIfHasText(payee.getName(), sb);
        }

        String memo = transaction.getMemo();
        appendIfHasText(memo, sb);

        return sb.toString();
    }

    public static AndMnyFilter<Account> createAndAccountFilter(AccountFilterArg arg) {

        final EnumAccountStatus status = arg.getStatus();
        final EnumAccountType type = arg.getType();
        final String name = arg.getName();

        AndMnyFilter<Account> filter = new AndMnyFilter<Account>();

        // accountStatus
        if (status != null) {
            MnyFilter<Account> subFilter = createAccountStatusFilter(status);
            filter.addFilter(subFilter);
        }

        // accountType
        if (type != null) {
            MnyFilter<Account> subFilter = createAccountTypeFilter(type);
            filter.addFilter(subFilter);
        }

        // accountName
        if (org.springframework.util.StringUtils.hasText(name)) {
            MnyFilter<Account> subFilter = createAccountNameFilter(name);
            filter.addFilter(subFilter);
        }
        return filter;
    }

    public static MnyFilter<Transaction> createAndTransactionFilter(TransactionFilterArg arg) throws IOException {
        AndMnyFilter<Transaction> filter = new AndMnyFilter<Transaction>();
//        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");

        Date fromDate = arg.getFromDate();
        Date toDate = arg.getToDate();
        Double fromAmount = arg.getFromAmount();
        Double toAmount = arg.getToAmount();
        String text = arg.getText();
        String regex = arg.getRegex();

        // fromDate
        if (fromDate != null) {
//                final Date when = dateFormat.parse(fromDate);
            MnyFilter<Transaction> subFilter = createTransactionFromDateFilter(fromDate);
            filter.addFilter(subFilter);
        }

        // toDate
        if (toDate != null) {
//                final Date when = dateFormat.parse(toDate);
            MnyFilter<Transaction> subFilter = createTransactionToDateFilter(toDate);
            filter.addFilter(subFilter);
        }

        // fromAmount
        if (fromAmount != null) {
            MnyFilter<Transaction> subFilter = createTransactionFromAmount(fromAmount);
            filter.addFilter(subFilter);
        }
        // toAmount
        if (toAmount != null) {
            MnyFilter<Transaction> subFilter = createTransactionToAmountFilter(toAmount);
            filter.addFilter(subFilter);
        }

        // text
        if (org.springframework.util.StringUtils.hasText(text)) {
            MnyFilter<Transaction> subFilter = createTransactionTextFilter(text);
            filter.addFilter(subFilter);
        }

        // regex
        if (org.springframework.util.StringUtils.hasText(regex)) {
            MnyFilter<Transaction> subFilter = createTransactionRegexFilter(regex);
            filter.addFilter(subFilter);
        }

        return filter;
    }

    public static Date toDate(String source) throws IOException {
        Date date = null;

        try {
            date = DATE_FORMAT.parse(source);
        } catch (ParseException e) {
            throw new IOException(e);
        }

        return date;
    }

    public static final String formatDate(Date date) {
        return DATE_FORMAT.format(date);
    }
}

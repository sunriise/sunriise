package com.hungle.money.component;

import com.hungle.sunriise.mnyobject.EnumAccountType;

public class AccountFilterArgBuilder {
    private final AccountFilterArg arg;

    public AccountFilterArgBuilder() {
        super();
        this.arg = new AccountFilterArg();
    }

    public AccountFilterArg build() {
        return arg;
    }

    public AccountFilterArgBuilder setStatus(EnumAccountStatus status) {
        arg.setStatus(status);
        return this;
    }

    public AccountFilterArgBuilder setType(EnumAccountType type) {
        arg.setType(type);
        return this;
    }

    public AccountFilterArgBuilder setName(String name) {
        arg.setName(name);
        return this;
    }
}

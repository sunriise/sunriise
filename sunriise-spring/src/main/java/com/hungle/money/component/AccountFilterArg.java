package com.hungle.money.component;

import com.hungle.sunriise.mnyobject.EnumAccountType;

public final class AccountFilterArg {
    private EnumAccountStatus status;
    private EnumAccountType type;
    private String name;

    public AccountFilterArg(EnumAccountStatus status, EnumAccountType type, String name) {
        super();
        this.status = status;
        this.type = type;
        this.name = name;
    }

    public AccountFilterArg() {
        this(EnumAccountStatus.ALL, null, null);
    }

    public EnumAccountStatus getStatus() {
        return status;
    }

    public EnumAccountType getType() {
        return type;
    }

    public String getName() {
        return name;
    }

    public void setStatus(EnumAccountStatus status) {
        this.status = status;
    }

    public void setType(EnumAccountType type) {
        this.type = type;
    }

    public void setName(String name) {
        this.name = name;
    }
}
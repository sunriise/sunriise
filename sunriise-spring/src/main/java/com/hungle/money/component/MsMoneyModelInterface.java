package com.hungle.money.component;

import java.io.IOException;
import java.util.List;

import com.hungle.sunriise.mnyobject.Account;
import com.hungle.sunriise.mnyobject.Category;
import com.hungle.sunriise.mnyobject.Currency;
import com.hungle.sunriise.mnyobject.Payee;
import com.hungle.sunriise.mnyobject.Security;
import com.hungle.sunriise.mnyobject.Transaction;
import com.hungle.sunriise.util.MnyFilter;

public interface MsMoneyModelInterface {

    List<Account> getAccounts() throws IOException;

    List<Account> getAccounts(MnyFilter<Account> filter) throws IOException;

    Account getAccount(Integer accountId) throws IOException;

    List<Transaction> getTransactions(Integer accountId, MnyFilter<Transaction> filter) throws IOException;

    Transaction getTransaction(Integer id, Integer tid) throws IOException;

    // Category
    List<Category> getCategories();

    List<Category> getCategories(MnyFilter<Category> filter);

    Category getCategory(Integer id);

    // Currency
    List<Currency> getCurrencies();

    List<Currency> getCurrencies(MnyFilter<Currency> filter);

    Currency getCurrency(Integer id);

    // Payee
    List<Payee> getPayees();

    Payee getPayee(Integer id);

    List<Payee> getPayees(MnyFilter<Payee> filter);

    // Security
    List<Security> getSecurities();

    List<Security> getSecurities(MnyFilter<Security> filter);

    Security getSecurity(Integer id);

}
package com.hungle.money.component;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

@Component
public class LongConverter implements Converter<String, Long> {
    private static final Logger LOGGER = LogManager.getLogger(LongConverter.class);

    @Override
    public Long convert(String str) {
        Long type = null;

        try {
            if (StringUtils.hasText(str)) {
                type = Long.valueOf(str);
            }
        } catch (Exception e) {
            if (LOGGER.isDebugEnabled()) {
                LOGGER.warn(e.getMessage());
                type = null;
            }
        }
        return type;
    }

}

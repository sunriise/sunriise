package com.hungle.money.component;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.Logger;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Component;
import org.springframework.util.ConcurrentReferenceHashMap;

import com.hungle.sunriise.dbutil.TableAccountUtils;
import com.hungle.sunriise.mnyobject.Account;
import com.hungle.sunriise.mnyobject.Category;
import com.hungle.sunriise.mnyobject.Currency;
import com.hungle.sunriise.mnyobject.MnyContext;
import com.hungle.sunriise.mnyobject.Payee;
import com.hungle.sunriise.mnyobject.Security;
import com.hungle.sunriise.mnyobject.Transaction;
import com.hungle.sunriise.util.MnyContextUtils;
import com.hungle.sunriise.util.MnyFilter;

@Component
@ConditionalOnProperty(value = "msmoney.model", havingValue = "minimumly", matchIfMissing = true)
public class MinimumlyLoadedMsMoneyModel extends AbstractMsMoneyModel {
    private static final Logger LOGGER = org.apache.logging.log4j.LogManager
            .getLogger(MinimumlyLoadedMsMoneyModel.class);

    private final MnyContext mnyContext;

    private final ConcurrentReferenceHashMap<Integer, Transaction> transactionCache = new ConcurrentReferenceHashMap<>();

    private final List<Account> accounts;
    private final List<Category> categories;
    private final List<Currency> currencies;
    private final List<Payee> payees;
    private final List<Security> securities;

    public MinimumlyLoadedMsMoneyModel(ApplicationArguments args) throws IOException {
        super(args);

        this.mnyContext = new MnyContext();
        MnyContextUtils.initMnyContext(getMnyDb(), mnyContext);

        this.accounts = new ArrayList<Account>(mnyContext.getAccounts().values());
        this.categories = new ArrayList<Category>(mnyContext.getCategories().values());
        this.currencies = new ArrayList<Currency>(mnyContext.getCurrencies().values());
        this.payees = new ArrayList<Payee>(mnyContext.getPayees().values());
        this.securities = new ArrayList<Security>(mnyContext.getSecurities().values());
    }

    @Override
    public List<Account> getAccounts() throws IOException {
        return getAccounts(null);
    }

    @Override
    public List<Account> getAccounts(MnyFilter<Account> filter) throws IOException {
        List<Account> result = null;

        Map<Integer, Account> accountMap = mnyContext.getAccounts();

        if (filter == null) {
            result = this.accounts;
        } else {
            result = new ArrayList<>();
            for (Account account : accountMap.values()) {
                if (filter.accepts(account)) {
                    result.add(account);
                }
            }
        }

        return result;
    }

    @Override
    public Account getAccount(Integer accountId) throws IOException {
        Map<Integer, Account> accountMap = mnyContext.getAccounts();

        return accountMap.get(accountId);
    }

    @Override
    public List<Transaction> getTransactions(Integer accountId, MnyFilter<Transaction> filter) throws IOException {
        List<Transaction> result = new ArrayList<>();

        Map<Integer, Account> accountMap = mnyContext.getAccounts();
        Account account = accountMap.get(accountId);
        if (account == null) {
            return result;
        }

        TableAccountUtils.addTransactionsToAccount(account, mnyContext);
        List<Transaction> transactions = account.getTransactions();
        if (filter == null) {
            result = transactions;
        } else {
            for (Transaction transaction : transactions) {
                if (filter.accepts(transaction)) {
                    result.add(transaction);
                }
            }
        }

        return result;
    }

    @Override
    public Transaction getTransaction(Integer id, Integer tid) throws IOException {
        Transaction result = transactionCache.get(tid);
        if (result != null) {
            return result;
        }

        Map<Integer, Account> accountMap = mnyContext.getAccounts();
        Account account = accountMap.get(id);
        if (account == null) {
            LOGGER.warn("Cannot find account.id={}", id);
            return null;
        }

        List<Transaction> transactions = account.getTransactions();
        if (transactions == null) {
            TableAccountUtils.addTransactionsToAccount(account, mnyContext);
            transactions = account.getTransactions();
        }

        if (transactions == null) {
            LOGGER.warn("Cannot find transactions for account.id={}", id);
            return null;
        }

        for (Transaction transaction : transactions) {
            if (transaction.getId().equals(tid)) {
                LOGGER.info("Found account.id={}, transaction.id={}", id, tid);
                transactionCache.put(tid, transaction);
                return transaction;
            }
        }

        LOGGER.warn("Cannot find transaction.id={}", tid);
        return null;
    }

    // Category
    @Override
    public List<Category> getCategories() {
        return this.categories;
    }

    @Override
    public List<Category> getCategories(MnyFilter<Category> filter) {
        List<Category> result = null;

        if (filter == null) {
            result = this.categories;
        } else {
            result = new ArrayList<>();
            for (Category category : this.categories) {
                if (filter.accepts(category)) {
                    result.add(category);
                }
            }
        }
        return result;
    }

    @Override
    public Category getCategory(Integer id) {
        return mnyContext.getCategories().get(id);
    }

    // Currency
    @Override
    public List<Currency> getCurrencies() {
        return this.currencies;
    }

    @Override
    public List<Currency> getCurrencies(MnyFilter<Currency> filter) {
        List<Currency> result = null;

        if (filter == null) {
            result = this.currencies;
        } else {
            result = new ArrayList<>();
            for (Currency currency : this.currencies) {
                if (filter.accepts(currency)) {
                    result.add(currency);
                }
            }
        }
        return result;
    }

    @Override
    public Currency getCurrency(Integer id) {
        return mnyContext.getCurrencies().get(id);
    }

    // Payee
    @Override
    public List<Payee> getPayees() {
        return this.payees;
    }

    @Override
    public List<Payee> getPayees(MnyFilter<Payee> filter) {
        List<Payee> result = null;

        if (filter == null) {
            result = this.payees;
        } else {
            result = new ArrayList<>();
            for (Payee payee : this.payees) {
                if (filter.accepts(payee)) {
                    result.add(payee);
                }
            }
        }
        return result;
    }

    @Override
    public Payee getPayee(Integer id) {
        return mnyContext.getPayees().get(id);
    }

    // Security
    @Override
    public List<Security> getSecurities() {
        return this.securities;
    }

    @Override
    public List<Security> getSecurities(MnyFilter<Security> filter) {
        List<Security> result = null;

        if (filter == null) {
            result = this.securities;
        } else {
            result = new ArrayList<>();
            for (Security security : this.securities) {
                if (filter.accepts(security)) {
                    result.add(security);
                }
            }
        }
        return result;

    }

    @Override
    public Security getSecurity(Integer id) {
        return mnyContext.getSecurities().get(id);
    }

}

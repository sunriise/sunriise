package com.hungle.money.component;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.boot.ApplicationArguments;
import org.springframework.util.StringUtils;

import com.hungle.money.Application;
import com.hungle.sunriise.io.FileUtils;
import com.hungle.sunriise.io.MnyDb;

public abstract class AbstractMsMoneyModel implements MsMoneyModelInterface {
    private static final Logger LOGGER = LogManager.getLogger(AbstractMsMoneyModel.class);
    private final MnyDb mnyDb;

    public AbstractMsMoneyModel(ApplicationArguments args) throws IOException {
        super();
        this.mnyDb = openDb(args);
    }

    private MnyDb openDb(ApplicationArguments args) throws IOException {
        LOGGER.info("args={}", args);

        String fileName = Application.argsGetFileName(args);

        String password = Application.argsGetPassword(args);

        if (StringUtils.isEmpty(fileName)) {
            try {
                Path mnyFile = Application.findMnyFile(new File(".").toPath(), ".mny");
                if (mnyFile != null) {
                    fileName = mnyFile.toFile().getAbsolutePath();
                }
            } catch (IOException e) {
                LOGGER.warn(e.getMessage());
            }
        }
        // fileName = "xxx.mny";
        LOGGER.info("ARG, fileName={}", fileName);

        File dbFile = null;
        MnyDb mnyDb;
        if (StringUtils.isEmpty(fileName)) {
            mnyDb = openSample();
        } else {
            dbFile = new File(fileName);
            mnyDb = new MnyDb(dbFile, password);
        }

        return mnyDb;
    }

    private MnyDb openSample() throws IOException {
        File dbFile;
        MnyDb mnyDb;
        String prefix = "sample_";
        String suffix = ".mny";
        dbFile = File.createTempFile(prefix, suffix);
        LOGGER.info("Creating sample dbFile=" + dbFile.getAbsolutePath());
        mnyDb = FileUtils.openSampleDb(dbFile);
        dbFile.deleteOnExit();
        return mnyDb;
    }

    public MnyDb getMnyDb() {
        return mnyDb;
    }

}
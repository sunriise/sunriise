package com.hungle.money.component;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Component;
import org.springframework.util.ConcurrentReferenceHashMap;

import com.hungle.sunriise.dbutil.TableAccountUtils;
import com.hungle.sunriise.dbutil.TableCategoryUtils;
import com.hungle.sunriise.dbutil.TableCurrencyUtils;
import com.hungle.sunriise.dbutil.TablePayeeUtils;
import com.hungle.sunriise.dbutil.TableSecurityUtils;
import com.hungle.sunriise.mnyobject.Account;
import com.hungle.sunriise.mnyobject.Category;
import com.hungle.sunriise.mnyobject.Currency;
import com.hungle.sunriise.mnyobject.MnyContext;
import com.hungle.sunriise.mnyobject.Payee;
import com.hungle.sunriise.mnyobject.Security;
import com.hungle.sunriise.mnyobject.Transaction;
import com.hungle.sunriise.util.MnyContextUtils;
import com.hungle.sunriise.util.MnyFilter;

@Component
@ConditionalOnProperty(value = "msmoney.model", havingValue = "fully", matchIfMissing = false)
public class FullyLoadedMsMoneyModel extends AbstractMsMoneyModel {
    private static final Logger LOGGER = LogManager.getLogger(FullyLoadedMsMoneyModel.class);

    private final MnyContext mnyContext;

    private Map<Integer, Transaction> transactionCache = new ConcurrentReferenceHashMap<>();

    public FullyLoadedMsMoneyModel(ApplicationArguments args) throws IOException {
        super(args);

        LOGGER.info("> START loading db ...");
        this.mnyContext = MnyContextUtils.createMnyContext(getMnyDb());
        TableAccountUtils.populateAccounts(this.mnyContext);
        LOGGER.info("< DONE loading db ...");
    }

    public MnyContext getMnyContext() {
        return mnyContext;
    }

    @Override
    public List<Account> getAccounts() {
        MnyFilter<Account> filter = null;
        return getAccounts(filter);
    }

    @Override
    public List<Account> getAccounts(MnyFilter<Account> filter) {
        final Map<Integer, Account> map = mnyContext.getAccounts();

        List<Account> list = TableAccountUtils.toAccountsList(map, filter);

        return list;
    }

    @Override
    public Account getAccount(Integer accountId) {
        Account account = mnyContext.getAccounts().get(accountId);
        return account;
    }

    @Override
    public List<Transaction> getTransactions(Integer accountId, MnyFilter<Transaction> filter) throws IOException {
        Account account = getAccount(accountId);
        if (account == null) {
            return null;
        }
        List<Transaction> transactions = account.getTransactions();
        if (filter != null) {
            ArrayList<Transaction> filteredResult = new ArrayList<Transaction>();
            for (Transaction transaction : transactions) {
                if (filter.accepts(transaction)) {
                    filteredResult.add(transaction);
                }
            }
            transactions = filteredResult;
        }
        return transactions;
    }

    @Override
    public Transaction getTransaction(Integer id, Integer tid) throws IOException {
        Transaction transaction = null;
        transaction = transactionCache.get(tid);
        if (transaction != null) {
            return transaction;
        }

        Account account = getAccount(id);
        if (account == null) {
            return null;
        }

        List<Transaction> transactions = account.getTransactions();
        for (Transaction t : transactions) {
            if (t.getId().equals(tid)) {
                transaction = t;
                break;
            }
        }
        if (transaction != null) {
            transactionCache.put(tid, transaction);
        }

        return transaction;
    }

    // Category
    @Override
    public List<Category> getCategories() {
        MnyFilter<Category> filter = null;
        return getCategories(filter);
    }

    @Override
    public List<Category> getCategories(MnyFilter<Category> filter) {
        final Map<Integer, Category> map = mnyContext.getCategories();

        List<Category> list = TableCategoryUtils.toCategoriesList(map, filter);

        return list;
    }

    @Override
    public Category getCategory(Integer id) {
        Category category = mnyContext.getCategories().get(id);
        return category;
    }

    // Currency
    @Override
    public List<Currency> getCurrencies() {
        MnyFilter<Currency> filter = null;
        return getCurrencies(filter);
    }

    @Override
    public List<Currency> getCurrencies(MnyFilter<Currency> filter) {
        final Map<Integer, Currency> map = mnyContext.getCurrencies();

        List<Currency> list = TableCurrencyUtils.toCurrenciesList(map, filter);

        return list;
    }

    @Override
    public Currency getCurrency(Integer id) {
        Currency currency = mnyContext.getCurrencies().get(id);
        return currency;
    }

    // Payee
    @Override
    public List<Payee> getPayees() {
        MnyFilter<Payee> filter = null;
        return getPayees(filter);
    }

    @Override
    public Payee getPayee(Integer id) {
        Payee payee = mnyContext.getPayees().get(id);
        return payee;
    }

    @Override
    public List<Payee> getPayees(MnyFilter<Payee> filter) {
        final Map<Integer, Payee> map = mnyContext.getPayees();

        List<Payee> list = TablePayeeUtils.toPayeesList(map, filter);

        return list;
    }

    // Security
    @Override
    public List<Security> getSecurities() {
        MnyFilter<Security> filter = null;
        return getSecurities(filter);
    }

    @Override
    public List<Security> getSecurities(MnyFilter<Security> filter) {
        final Map<Integer, Security> map = mnyContext.getSecurities();

        List<Security> list = TableSecurityUtils.toSecuritiesList(map, filter);

        return list;
    }

    @Override
    public Security getSecurity(Integer id) {
        Security security = mnyContext.getSecurities().get(id);
        return security;
    }
}

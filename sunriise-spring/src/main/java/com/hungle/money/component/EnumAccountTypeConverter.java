package com.hungle.money.component;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import com.hungle.sunriise.mnyobject.EnumAccountType;

@Component
public class EnumAccountTypeConverter implements Converter<String, EnumAccountType> {
    private static final Logger LOGGER = LogManager.getLogger(EnumAccountTypeConverter.class);

    @Override
    public EnumAccountType convert(String str) {
        EnumAccountType type = null;

        try {
            if (StringUtils.hasText(str)) {
                type = EnumAccountType.valueOf(str);
            }
        } catch (Exception e) {
            if (LOGGER.isDebugEnabled()) {
                LOGGER.warn(e.getMessage());
                type = null;
            }
        }
        return type;
    }

}

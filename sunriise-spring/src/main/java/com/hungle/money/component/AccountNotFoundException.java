package com.hungle.money.component;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND, reason = "No such Account") // 404
public class AccountNotFoundException extends MoneyRestApiException {
    private final Integer id;

    public AccountNotFoundException(Integer id) {
        super();
        this.id = id;
    }

    public Integer getId() {
        return id;
    }
}

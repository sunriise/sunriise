package com.hungle.money.component;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND, reason = "No such Transaction") // 404
public class TransactionNotFoundException extends MoneyRestApiException {
    private final Integer transactionId;

    public TransactionNotFoundException(Integer transactionId) {
        super();
        this.transactionId = transactionId;
    }

    public Integer getTransactionId() {
        return transactionId;
    }
}

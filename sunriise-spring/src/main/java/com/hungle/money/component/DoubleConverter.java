package com.hungle.money.component;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

@Component
public class DoubleConverter implements Converter<String, Double> {
    private static final Logger LOGGER = LogManager.getLogger(DoubleConverter.class);

    @Override
    public Double convert(String str) {
        Double type = null;

        try {
            if (StringUtils.hasText(str)) {
                type = Double.valueOf(str);
            }
        } catch (Exception e) {
            if (LOGGER.isDebugEnabled()) {
                LOGGER.warn(e.getMessage());
                type = null;
            }
        }
        return type;
    }

}

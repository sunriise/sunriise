package com.hungle.money.component;

import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@Configuration
@EnableJpaRepositories("com.hungle.money.repositories")
class ApplicationConfiguration {
}

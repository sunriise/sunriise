package com.hungle.money.controller;

import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

import com.hungle.money.component.AccountFilterArg;
import com.hungle.money.component.EnumAccountStatus;
import com.hungle.money.component.MnyFilterFactory;
import com.hungle.money.component.MsMoneyModelTest;
import com.hungle.money.component.MsMoneyModelTest.AccountFilterExpect;
import com.hungle.money.component.MsMoneyModelTest.TransactionFilterExpect;
import com.hungle.money.component.TransactionFilterArg;
import com.hungle.sunriise.mnyobject.EnumAccountType;
import com.hungle.sunriise.mnyobject.impl.DefaultAccount;
import com.hungle.sunriise.mnyobject.impl.DefaultTransaction;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
public class MsMoneyControllerTest {
    private static final Logger LOGGER = LogManager.getLogger(MsMoneyControllerTest.class);

    private static final String API_MSMONEY = "/api/msmoney";

    private static final String TRANSACTION_QUERY_REGEX = "regex";

    private static final String TRANSACTION_QUERY_TEXT = "text";

    private static final String TRANSACTION_QUERY_TO_AMOUNT = "toAmount";

    private static final String TRANSACTION_QUERY_FROM_AMOUNT = "fromAmount";

    private static final String TRANSACTION_QUERY_TO_DATE = "toDate";

    private static final String TRANSACTION_QUERY_FROM_DATE = "fromDate";

    private static final String ACCOUNT_QUERY_NAME = "name";

    private static final String ACCOUNT_QUERY_TYPE = "type";

    private static final String ACCOUNT_QUERY_STATUS = "status";

    @Autowired
    private TestRestTemplate restTemplate;

    @Test
    public void testGetAccount() {
        Assert.assertNotNull(restTemplate);

        String url = null;

        url = API_MSMONEY + "/accounts";

        List<DefaultAccount> accounts = fetchAccounts(url);

        Assert.assertNotNull(accounts);

        int expectedAccountCount = MsMoneyModelTest.TOTAL_ACCOUNT_COUNT;
        Assert.assertEquals(expectedAccountCount, accounts.size());

        int accountCount = 0;
        for (DefaultAccount account : accounts) {
            url = API_MSMONEY + "/account/" + account.getId();
            LOGGER.info("ACCOUNT url={}", url);
            DefaultAccount detailAccount = restTemplate.getForObject(url, DefaultAccount.class);
            Assert.assertNotNull(detailAccount);
            accountCount++;

            url = API_MSMONEY + "/account/" + account.getId() + "/transactions";
            List<DefaultTransaction> transactions = fetchTransactions(url);
            for (DefaultTransaction transaction : transactions) {
                url = API_MSMONEY + "/account/" + account.getId() + "/transaction/" + transaction.getId();
                LOGGER.info("TRANSACTION url={}", url);
                DefaultTransaction detailTransaction = restTemplate.getForObject(url, DefaultTransaction.class);
                Assert.assertNotNull(detailTransaction);
            }

        }

        Assert.assertEquals(expectedAccountCount, accountCount);
    }

    private List<DefaultAccount> fetchAccounts(String url) {
        ResponseEntity<List<DefaultAccount>> response = restTemplate.exchange(url, HttpMethod.GET, null,
                new ParameterizedTypeReference<List<DefaultAccount>>() {
                });
        List<DefaultAccount> list = response.getBody();
        return list;
    }

    private List<DefaultTransaction> fetchTransactions(String url) {
        ResponseEntity<List<DefaultTransaction>> response = restTemplate.exchange(url, HttpMethod.GET, null,
                new ParameterizedTypeReference<List<DefaultTransaction>>() {
                });
        List<DefaultTransaction> list = response.getBody();
        return list;
    }

    @Test
    public void testAccountFilter() {
        String template = API_MSMONEY + "/accounts";
        String url = null;

        Map<String, String> variables = createAccountVariables();
        List<DefaultAccount> accounts = null;

        List<AccountFilterExpect> expects = MsMoneyModelTest.createAccountExpects();
        for (AccountFilterExpect expect : expects) {
            AccountFilterArg arg = expect.getArg();

            resetAccountVariables(variables);

            EnumAccountStatus status = arg.getStatus();
            variables.put(ACCOUNT_QUERY_STATUS, status == null ? null : status.toString());
            EnumAccountType type = arg.getType();
            variables.put(ACCOUNT_QUERY_TYPE, type == null ? null : type.toString());
            variables.put(ACCOUNT_QUERY_NAME, arg.getName());

            url = createAccountsUrl(template, variables);
            LOGGER.info("url={}", url);
            accounts = fetchAccounts(url);

            Assert.assertEquals(expect.getExpect().intValue(), accounts.size());
        }

    }

    private String createAccountsUrl(String template, Map<String, String> map) {
//        Charset utf8 = Charset.forName("UTF-8");
        UriComponents uriComponents = UriComponentsBuilder.fromUriString(template)
                .query(ACCOUNT_QUERY_STATUS + "={" + ACCOUNT_QUERY_STATUS + "}")
                .query(ACCOUNT_QUERY_TYPE + "={" + ACCOUNT_QUERY_TYPE + "}")
                .query(ACCOUNT_QUERY_NAME + "={" + ACCOUNT_QUERY_NAME + "}").buildAndExpand(map)
//                .encode(utf8)
        ;
        String url = uriComponents.toUriString();
        return url;
    }

    private String createTransactionUrl(String template, Map<String, String> map) {
//        Charset utf8 = Charset.forName("UTF-8");
        UriComponents uriComponents = UriComponentsBuilder.fromUriString(template)
                .query(TRANSACTION_QUERY_FROM_DATE + "={" + TRANSACTION_QUERY_FROM_DATE + "}")
                .query(TRANSACTION_QUERY_TO_DATE + "={" + TRANSACTION_QUERY_TO_DATE + "}")
                .query(TRANSACTION_QUERY_FROM_AMOUNT + "={" + TRANSACTION_QUERY_FROM_AMOUNT + "}")
                .query(TRANSACTION_QUERY_TO_AMOUNT + "={" + TRANSACTION_QUERY_TO_AMOUNT + "}")
                .query(TRANSACTION_QUERY_TEXT + "={" + TRANSACTION_QUERY_TEXT + "}")
                .query(TRANSACTION_QUERY_REGEX + "={" + TRANSACTION_QUERY_REGEX + "}").buildAndExpand(map)
//                .encode(utf8)
        ;
        String url = uriComponents.toUriString();
        return url;
    }

    private Map<String, String> createAccountVariables() {
        Map<String, String> variables = new HashMap<String, String>();
        resetAccountVariables(variables);
        return variables;
    }

    private Map<String, String> createTransactionVariables() {
        Map<String, String> variables = new HashMap<String, String>();
        resetTransactonVariables(variables);
        return variables;
    }

    private void resetAccountVariables(Map<String, String> variables) {
        variables.put(ACCOUNT_QUERY_STATUS, null);
        variables.put(ACCOUNT_QUERY_TYPE, null);
        variables.put(ACCOUNT_QUERY_NAME, null);
    }

    private void resetTransactonVariables(Map<String, String> variables) {
        variables.put(TRANSACTION_QUERY_FROM_DATE, null);
        variables.put(TRANSACTION_QUERY_TO_DATE, null);
        variables.put(TRANSACTION_QUERY_FROM_AMOUNT, null);
        variables.put(TRANSACTION_QUERY_TO_AMOUNT, null);
        variables.put(TRANSACTION_QUERY_TEXT, null);
        variables.put(TRANSACTION_QUERY_REGEX, null);
    }

    @Test
    public void testTransactionFilter() throws IOException {
        String template = API_MSMONEY + "/account/{id}/transactions";
        String url = null;

        Map<String, String> variables = createTransactionVariables();
        List<DefaultTransaction> transactions = null;

        List<TransactionFilterExpect> expects = MsMoneyModelTest.createTransactionExpects();
        for (TransactionFilterExpect expect : expects) {
            TransactionFilterArg arg = expect.getArg();

            resetTransactonVariables(variables);
            variables.put("id", "" + MsMoneyModelTest.ACCOUNT_ID);

            Date fromDate = arg.getFromDate();
            variables.put(TRANSACTION_QUERY_FROM_DATE, fromDate == null ? null : MnyFilterFactory.formatDate(fromDate));
            Date toDate = arg.getToDate();
            variables.put(TRANSACTION_QUERY_TO_DATE, toDate == null ? null : MnyFilterFactory.formatDate(toDate));
            Double fromAmount = arg.getFromAmount();
            variables.put(TRANSACTION_QUERY_FROM_AMOUNT, fromAmount == null ? null : fromAmount.toString());
            Double toAmount = arg.getToAmount();
            variables.put(TRANSACTION_QUERY_TO_AMOUNT, toAmount == null ? null : toAmount.toString());
            variables.put(TRANSACTION_QUERY_TEXT, arg.getText());
            variables.put(TRANSACTION_QUERY_REGEX, arg.getRegex());

            url = createTransactionUrl(template, variables);
            LOGGER.info("url={}", url);
            transactions = fetchTransactions(url);
            Assert.assertEquals(expect.getExpect().intValue(), transactions.size());

        }
    }
}

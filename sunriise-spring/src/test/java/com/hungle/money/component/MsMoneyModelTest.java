package com.hungle.money.component;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.DefaultApplicationArguments;

import com.hungle.sunriise.mnyobject.Account;
import com.hungle.sunriise.mnyobject.Category;
import com.hungle.sunriise.mnyobject.Currency;
import com.hungle.sunriise.mnyobject.EnumAccountType;
import com.hungle.sunriise.mnyobject.Payee;
import com.hungle.sunriise.mnyobject.Security;
import com.hungle.sunriise.mnyobject.Transaction;
import com.hungle.sunriise.util.AndMnyFilter;
import com.hungle.sunriise.util.BalanceUtils;
import com.hungle.sunriise.util.MnyFilter;

public class MsMoneyModelTest {
    private static MsMoneyModelInterface model;

    public static final int TOTAL_ACCOUNT_COUNT = 23;

    private static final int TOTAL_CATEGORY_COUNT = 141;

    private static final int TOTAL_CURRENCY_COUNT = 155;

    private static final int TOTAL_PAYEE_COUNT = 165;

    private static final int TOTAL_SECURITY_COUNT = 72;

    public static final int ACCOUNT_ID = 42;

    private static final int TRANSACTION_ID = 2299;

    public static final class AccountFilterExpect {
        final private AccountFilterArg arg;
        final private Integer expect;

        public AccountFilterExpect(AccountFilterArg arg, Integer expect) {
            super();
            this.arg = arg;
            this.expect = expect;
        }

        public AccountFilterArg getArg() {
            return arg;
        }

        public Integer getExpect() {
            return expect;
        }
    }

    public static final class TransactionFilterExpect {
        final private TransactionFilterArg arg;
        final private Integer expect;

        public TransactionFilterExpect(TransactionFilterArg arg, Integer expect) {
            super();
            this.arg = arg;
            this.expect = expect;
        }

        public TransactionFilterArg getArg() {
            return arg;
        }

        public Integer getExpect() {
            return expect;
        }
    }

    @BeforeClass
    public static final void init() throws IOException {
        String[] argsString = new String[0];
        ApplicationArguments args = new DefaultApplicationArguments(argsString);
        MsMoneyModelTest.model = new FullyLoadedMsMoneyModel(args);
    }

    @Test
    public void testSimple() throws IOException {
        Account account = null;

        int accountId = 0;

        // check non-existing account
        accountId = 0;
        account = model.getAccount(accountId);
        Assert.assertNull(account);

        List<Account> accounts = model.getAccounts();
        Assert.assertNotNull(accounts);
        Assert.assertEquals(TOTAL_ACCOUNT_COUNT, accounts.size());

        // check existing account
        // 42
        accountId = ACCOUNT_ID;
        account = model.getAccount(accountId);
        Assert.assertNotNull(account);

        Transaction transaction = null;

        int transactionId = 0;

        // check non-existing transaction
        accountId = 0;
        transactionId = 0;
        transaction = model.getTransaction(accountId, transactionId);
        Assert.assertNull(transaction);

        // check existing transaction
        // 2299
        accountId = ACCOUNT_ID;
        transactionId = TRANSACTION_ID;
        transaction = model.getTransaction(accountId, transactionId);
        Assert.assertNotNull(transaction);

        // -1928.11
        BigDecimal expectedAmount = BalanceUtils.formatBalance(new BigDecimal(-1928.11));
        BigDecimal actualAmount = BalanceUtils.formatBalance(transaction.getAmount());
        Assert.assertEquals(expectedAmount, actualAmount);

        List<Category> categories = model.getCategories();
        Assert.assertNotNull(categories);
        Assert.assertEquals(TOTAL_CATEGORY_COUNT, categories.size());

        List<Currency> currencies = model.getCurrencies();
        Assert.assertNotNull(currencies);
        Assert.assertEquals(TOTAL_CURRENCY_COUNT, currencies.size());

        List<Payee> payees = model.getPayees();
        Assert.assertNotNull(payees);
        Assert.assertEquals(TOTAL_PAYEE_COUNT, payees.size());

        List<Security> securities = model.getSecurities();
        Assert.assertNotNull(securities);
        Assert.assertEquals(TOTAL_SECURITY_COUNT, securities.size());

    }

    @Test
    public void testAccountFilter() throws IOException {
        List<AccountFilterExpect> expects = MsMoneyModelTest.createAccountExpects();

        for (AccountFilterExpect expect : expects) {
            AccountFilterArg arg = expect.getArg();

            AndMnyFilter<Account> filter = MnyFilterFactory.createAndAccountFilter(arg);
            List<Account> accounts = model.getAccounts(filter);
            Assert.assertEquals(expect.getExpect().intValue(), accounts.size());
        }

    }

    @Test
    public void testTransactionFilter() throws IOException {
        List<TransactionFilterExpect> expects = MsMoneyModelTest.createTransactionExpects();
        int accountId = 0;
        MnyFilter<Transaction> filter = null;
        List<Transaction> transactions = null;

        accountId = ACCOUNT_ID;
        for (TransactionFilterExpect expect : expects) {
            TransactionFilterArg arg = expect.getArg();
            filter = MnyFilterFactory.createAndTransactionFilter(arg);
            transactions = model.getTransactions(accountId, filter);
            Assert.assertEquals(expect.getExpect().intValue(), transactions.size());
        }
    }

    public static final List<AccountFilterExpect> createAccountExpects() {
        AccountFilterArg arg;
        List<AccountFilterExpect> expects = new ArrayList<MsMoneyModelTest.AccountFilterExpect>();
        expects.add(new AccountFilterExpect(new AccountFilterArg(), TOTAL_ACCOUNT_COUNT));

        Map<EnumAccountType, Integer> expectedCounts = MsMoneyModelTest.createExpectedAccountTypeCounts();
        for (EnumAccountType type : expectedCounts.keySet()) {
            arg = new AccountFilterArg(EnumAccountStatus.ALL, type, null);
            Integer expect = expectedCounts.get(type);
            expects.add(new AccountFilterExpect(arg, expect));
        }

        arg = new AccountFilterArg(EnumAccountStatus.ALL, EnumAccountType.BANKING, "Woodgrove");
        expects.add(new AccountFilterExpect(arg, 4));

        arg = new AccountFilterArg(EnumAccountStatus.ALL, EnumAccountType.BANKING, "WoodGrove");
        expects.add(new AccountFilterExpect(arg, 1));

        arg = new AccountFilterArg(EnumAccountStatus.OPEN, EnumAccountType.BANKING, "Woodgrove");
        expects.add(new AccountFilterExpect(arg, 4));

        arg = new AccountFilterArg(EnumAccountStatus.CLOSE, EnumAccountType.BANKING, "Woodgrove");
        expects.add(new AccountFilterExpect(arg, 0));

        return expects;
    }

    public static List<TransactionFilterExpect> createTransactionExpects() throws IOException {
        TransactionFilterArg arg;
        List<TransactionFilterExpect> expects = new ArrayList<MsMoneyModelTest.TransactionFilterExpect>();

        arg = new TransactionFilterArg(null, null, null, null, null, null);
        expects.add(new TransactionFilterExpect(arg, 1716));

        arg = new TransactionFilterArg(MnyFilterFactory.toDate("2012-01-01"), null, null, null, null, null);
        expects.add(new TransactionFilterExpect(arg, 0));

        arg = new TransactionFilterArg(null, MnyFilterFactory.toDate("2000-01-01"), null, null, null, null);
        expects.add(new TransactionFilterExpect(arg, 0));

        arg = new TransactionFilterArg(MnyFilterFactory.toDate("2002-01-01"), MnyFilterFactory.toDate("2002-01-31"),
                null, null, null, null);
        expects.add(new TransactionFilterExpect(arg, 13));

        arg = new TransactionFilterArg(MnyFilterFactory.toDate("2002-01-01"), MnyFilterFactory.toDate("2002-01-31"),
                null, new Double(0.00), null, null);
        expects.add(new TransactionFilterExpect(arg, 10));

        arg = new TransactionFilterArg(MnyFilterFactory.toDate("2002-01-01"), MnyFilterFactory.toDate("2002-01-31"),
                new Double(0.00), null, null, null);
        expects.add(new TransactionFilterExpect(arg, 3));

        arg = new TransactionFilterArg(MnyFilterFactory.toDate("2002-01-01"), MnyFilterFactory.toDate("2002-01-31"),
                null, new Double(0.00), "Automobile:Car Payment", null);
        expects.add(new TransactionFilterExpect(arg, 2));

        arg = new TransactionFilterArg(MnyFilterFactory.toDate("2002-01-01"), MnyFilterFactory.toDate("2002-01-31"),
                null, new Double(0.00), null, ".*Bills:.*");
        expects.add(new TransactionFilterExpect(arg, 4));

        return expects;
    }

    private static final Map<EnumAccountType, Integer> createExpectedAccountTypeCounts() {
        Map<EnumAccountType, Integer> map = new HashMap<>();
        map.put(EnumAccountType.ASSET, 2);
        map.put(EnumAccountType.BANKING, 9);
        map.put(EnumAccountType.CASH, 0);
        map.put(EnumAccountType.CREDIT_CARD, 3);
        map.put(EnumAccountType.INVESTMENT, 8);
        map.put(EnumAccountType.LIABILITY, 0);
        map.put(EnumAccountType.LOAN, 1);
        map.put(EnumAccountType.UNKNOWN, 0);
        return map;
    }
}

package com.hungle.sunriise.mnyobject;

import com.hungle.sunriise.mnyobject.impl.DefaultAccount;

public interface AccountRepository extends MnyObjectRepository<DefaultAccount> {

}

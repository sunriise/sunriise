package com.hungle.sunriise.mnyobject;

import com.hungle.sunriise.mnyobject.impl.DefaultCategory;

public interface CategoryRepository extends MnyObjectRepository<DefaultCategory> {

}

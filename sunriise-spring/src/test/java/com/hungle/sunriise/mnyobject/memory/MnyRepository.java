package com.hungle.sunriise.mnyobject.memory;

import org.springframework.dao.NonTransientDataAccessResourceException;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.NoRepositoryBean;

@NoRepositoryBean
public abstract class MnyRepository<T, ID> implements CrudRepository<T, ID> {

    @Override
    public <S extends T> S save(S entity) {
        throw new NonTransientDataAccessResourceException("Not supported");
    }

    @Override
    public <S extends T> Iterable<S> saveAll(Iterable<S> entities) {
        throw new NonTransientDataAccessResourceException("Not supported");
    }

    @Override
    public void deleteById(ID id) {
        throw new NonTransientDataAccessResourceException("Not supported");
    }

    @Override
    public void delete(T entity) {
        throw new NonTransientDataAccessResourceException("Not supported");
    }

    @Override
    public void deleteAll(Iterable<? extends T> entities) {
        throw new NonTransientDataAccessResourceException("Not supported");
    }

    @Override
    public void deleteAll() {
        throw new NonTransientDataAccessResourceException("Not supported");
    }

}

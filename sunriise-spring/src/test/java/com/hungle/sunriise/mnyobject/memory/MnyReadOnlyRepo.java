package com.hungle.sunriise.mnyobject.memory;

import java.util.ArrayList;
import java.util.Optional;

import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.healthmarketscience.jackcess.Database;

public abstract class MnyReadOnlyRepo<T, ID> extends MnyRepository<T, ID> {
    private static final Logger LOGGER = org.apache.logging.log4j.LogManager.getLogger(MnyReadOnlyRepo.class);

    private Database db;

    @Autowired
    public MnyReadOnlyRepo(Database db) {
        super();
        this.db = db;
    }

    public Database getDb() {
        return db;
    }

    @Override
    public Optional<T> findById(ID id) {
        return Optional.empty();
    }

    @Override
    public boolean existsById(ID id) {
        return false;
    }

    @Override
    public Iterable<T> findAll() {
        return new ArrayList<T>();
    }

    @Override
    public Iterable<T> findAllById(Iterable<ID> ids) {
        return new ArrayList<T>();
    }

    @Override
    public long count() {
        return 0;
    }

}
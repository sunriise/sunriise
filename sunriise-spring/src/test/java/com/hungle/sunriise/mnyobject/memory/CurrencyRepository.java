package com.hungle.sunriise.mnyobject.memory;

import java.io.IOException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Component;

import com.healthmarketscience.jackcess.Database;
import com.hungle.sunriise.dbutil.TableCurrencyUtils;
import com.hungle.sunriise.mnyobject.Currency;

@Component
public class CurrencyRepository extends MnyCacheRepo<Currency, Integer> {
    private static final Logger LOGGER = LogManager.getLogger(CurrencyRepository.class);

    public CurrencyRepository(Database db) throws IOException {
        super(db);
    }

    @Override
    protected void initMap() throws IOException {
        setMap(TableCurrencyUtils.getMap(getDb()));
    }
}

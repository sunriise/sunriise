package com.hungle.sunriise.mnyobject.memory;

import java.io.IOException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Component;

import com.healthmarketscience.jackcess.Database;
import com.hungle.sunriise.dbutil.TableTransactionUtils;
import com.hungle.sunriise.mnyobject.Transaction;

@Component
public class TransactionRepository extends MnyCacheRepo<Transaction, Integer> {
    private static final Logger LOGGER = LogManager.getLogger(TransactionRepository.class);

    public TransactionRepository(Database db) throws IOException {
        super(db);
    }

    @Override
    protected void initMap() throws IOException {
        setMap(TableTransactionUtils.getMap(getDb()));
    }
}

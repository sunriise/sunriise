package com.hungle.sunriise.mnyobject.memory;

import java.io.IOException;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import com.healthmarketscience.jackcess.Database;
import com.hungle.sunriise.io.MnyDb;
import com.hungle.sunriise.io.sample.MnySampleFileFactory;

@SpringBootApplication
class MnyConfiguration {

    @Bean
    public Database mnyDatabase() throws IOException {
        Database db = null;

        MnyDb sample = MnySampleFileFactory.getSunsetSample();
        db = sample.getDb();

        return db;
    }

}
package com.hungle.sunriise.mnyobject;

import com.hungle.sunriise.mnyobject.impl.DefaultCurrency;

public interface CurrencyRepository extends MnyObjectRepository<DefaultCurrency> {

}

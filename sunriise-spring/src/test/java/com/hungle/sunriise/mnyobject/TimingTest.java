package com.hungle.sunriise.mnyobject;

import java.io.File;
import java.io.IOException;
import java.util.Map;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.core.config.Configurator;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import com.hungle.sunriise.dbutil.TableAccountUtils;
import com.hungle.sunriise.io.MnyDb;
import com.hungle.sunriise.io.sample.MnySampleFile;
import com.hungle.sunriise.util.MnyContextUtils;
import com.hungle.sunriise.util.StopWatch;

public class TimingTest {
    private static final Logger LOGGER = org.apache.logging.log4j.LogManager.getLogger(TimingTest.class);

    @BeforeClass
    public static final void beforeClass() {
        Configurator.setLevel(LogManager.getLogger(TimingTest.class).getName(), Level.DEBUG);
    }

    @Test
    public void testLargeFile() throws IOException {
        StopWatch stopWatch = new StopWatch();
        MnyDb mnyDb = null;
        try {
            File file = new File(MnySampleFile.LOCAL_LARGE_FILE_MNY);
            if (!file.exists()) {
                return;
            }
            String password = null;
            mnyDb = new MnyDb(file, password);
            Assert.assertNotNull(mnyDb);

            stopWatch.click();
            MnyContext mnyContext = new MnyContext();
            MnyContextUtils.initMnyContext(mnyDb, mnyContext);
            stopWatch.logTiming(LOGGER, "POST initMnyContext");

            StopWatch stopWatch2 = new StopWatch();
            stopWatch2.click();
            Map<Integer, Account> accountMap = mnyContext.getAccounts();
            for (Account account : accountMap.values()) {
                TableAccountUtils.addTransactionsToAccount(account, mnyContext);
                stopWatch2.logTiming(LOGGER, "POST addTransactionsToAccount, account=" + account.getName());
            }
            stopWatch.logTiming(LOGGER, "POST addTransactionsToAccount(s), accounts.size=" + accountMap.size());
        } finally {
            if (mnyDb != null) {
                mnyDb.close();
                mnyDb = null;
            }
        }
    }
}

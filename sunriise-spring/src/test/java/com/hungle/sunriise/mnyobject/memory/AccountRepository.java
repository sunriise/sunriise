package com.hungle.sunriise.mnyobject.memory;

import java.io.IOException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Component;

import com.healthmarketscience.jackcess.Database;
import com.hungle.sunriise.dbutil.TableAccountUtils;
import com.hungle.sunriise.mnyobject.Account;

@Component
public class AccountRepository extends MnyCacheRepo<Account, Integer> {
    private static final Logger LOGGER = LogManager.getLogger(AccountRepository.class);

    public AccountRepository(Database db) throws IOException {
        super(db);
    }

    @Override
    protected void initMap() throws IOException {
        setMap(TableAccountUtils.getMap(getDb()));
    }
}

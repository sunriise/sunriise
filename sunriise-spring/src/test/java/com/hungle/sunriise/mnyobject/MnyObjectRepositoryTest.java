package com.hungle.sunriise.mnyobject;

import java.io.IOException;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import com.healthmarketscience.jackcess.Database;
import com.hungle.sunriise.dbutil.TableAccountUtils;
import com.hungle.sunriise.dbutil.TableCategoryUtils;
import com.hungle.sunriise.dbutil.TableCurrencyUtils;
import com.hungle.sunriise.dbutil.TablePayeeUtils;
import com.hungle.sunriise.dbutil.TableSecurityUtils;
import com.hungle.sunriise.dbutil.TableTransactionUtils;
import com.hungle.sunriise.io.MnyDb;
import com.hungle.sunriise.io.sample.MnySampleFileFactory;
import com.hungle.sunriise.mnyobject.impl.DefaultAccount;
import com.hungle.sunriise.mnyobject.impl.DefaultCategory;
import com.hungle.sunriise.mnyobject.impl.DefaultCurrency;
import com.hungle.sunriise.mnyobject.impl.DefaultPayee;
import com.hungle.sunriise.mnyobject.impl.DefaultSecurity;
import com.hungle.sunriise.mnyobject.impl.DefaultTransaction;

@RunWith(SpringRunner.class)
@Transactional
@SpringBootTest
public class MnyObjectRepositoryTest {
    private static final Logger LOGGER = LogManager.getLogger(MnyObjectRepositoryTest.class);

    @Autowired
    private PayeeRepository payeeRepository;

    @Autowired
    private CategoryRepository categoryRepository;

    @Autowired
    private CurrencyRepository currencyRepository;

    @Autowired
    private SecurityRepository securityRepository;

    @Autowired
    private AccountRepository accountRepository;

    @Autowired
    private TransactionRepository transactionRepository;

    @Test
    public void testMnyEntities() throws IOException {
        MnyDb mnyDb = MnySampleFileFactory.getSunsetSample();
        Database db = mnyDb.getDb();

        testPayees(db);

        testCategories(db);

        testCurrencies(db);

        testSecurities(db);

        testAccounts(db);

        testTransactions(db);
    }

    private void testPayees(Database db) throws IOException {
        Map<Integer, Payee> map = TablePayeeUtils.getMap(db);
        Class<DefaultPayee> clazz = DefaultPayee.class;
        PayeeRepository repo = payeeRepository;

        LOGGER.info("ENTITIES clazz={}", clazz);

        testSaveThenFindById(map, clazz, repo);
    }

    private void testCategories(Database db) throws IOException {
        Map<Integer, Category> map = TableCategoryUtils.getMap(db);
        Class<DefaultCategory> clazz = DefaultCategory.class;
        CategoryRepository repo = categoryRepository;

        LOGGER.info("ENTITIES clazz={}", clazz);

        testSaveThenFindById(map, clazz, repo);
    }

    private void testCurrencies(Database db) throws IOException {
        Map<Integer, Currency> map = TableCurrencyUtils.getMap(db);
        Class<DefaultCurrency> clazz = DefaultCurrency.class;
        CurrencyRepository repo = currencyRepository;

        LOGGER.info("ENTITIES clazz={}", clazz);

        testSaveThenFindById(map, clazz, repo);
    }

    private void testSecurities(Database db) throws IOException {
        Map<Integer, Security> map = TableSecurityUtils.getMap(db);
        Class<DefaultSecurity> clazz = DefaultSecurity.class;
        SecurityRepository repo = securityRepository;

        LOGGER.info("ENTITIES clazz={}", clazz);

        testSaveThenFindById(map, clazz, repo);
    }

    private void testAccounts(Database db) throws IOException {
        Map<Integer, Account> map = TableAccountUtils.getMap(db);
        Class<DefaultAccount> clazz = DefaultAccount.class;
        AccountRepository repo = accountRepository;

        LOGGER.info("ENTITIES clazz={}", clazz);

        testSaveThenFindById(map, clazz, repo);
    }

    private void testTransactions(Database db) throws IOException {
        Map<Integer, Transaction> map = TableTransactionUtils.getMap(db);
        Class<DefaultTransaction> clazz = DefaultTransaction.class;
        TransactionRepository repo = transactionRepository;

        LOGGER.info("ENTITIES clazz={}", clazz);

        testSaveThenFindById(map, clazz, repo);
    }

    private <T, U extends MnyObjectInterface> void testSaveThenFindById(Map<Integer, T> map, Class<U> clazz,
            MnyObjectRepository<U> repo) {
        U entityValue;
        // save
        for (T value : map.values()) {
            entityValue = kast(value, clazz);
            entityValue = repo.save(entityValue);
        }

        // find
        for (Integer key : map.keySet()) {
            entityValue = kast(map.get(key), clazz);
            Assert.assertEquals(repo.findById(key).get().getId(), entityValue.getId());
        }
    }

    private static <T> T kast(Object o, Class<T> clazz) {
        try {
            return clazz.cast(o);
        } catch (ClassCastException e) {
            return null;
        }
    }
}

package com.hungle.sunriise.mnyobject;

import com.hungle.sunriise.mnyobject.impl.DefaultTransaction;

public interface TransactionRepository extends MnyObjectRepository<DefaultTransaction> {

}

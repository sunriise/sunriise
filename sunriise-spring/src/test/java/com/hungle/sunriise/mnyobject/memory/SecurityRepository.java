package com.hungle.sunriise.mnyobject.memory;

import java.io.IOException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Component;

import com.healthmarketscience.jackcess.Database;
import com.hungle.sunriise.dbutil.TableSecurityUtils;
import com.hungle.sunriise.mnyobject.Security;

@Component
public class SecurityRepository extends MnyCacheRepo<Security, Integer> {
    private static final Logger LOGGER = LogManager.getLogger(SecurityRepository.class);

    public SecurityRepository(Database db) throws IOException {
        super(db);
    }

    @Override
    protected void initMap() throws IOException {
        setMap(TableSecurityUtils.getMap(getDb()));
    }
}

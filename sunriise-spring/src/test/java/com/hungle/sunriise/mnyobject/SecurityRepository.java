package com.hungle.sunriise.mnyobject;

import com.hungle.sunriise.mnyobject.impl.DefaultSecurity;

public interface SecurityRepository extends MnyObjectRepository<DefaultSecurity> {

}

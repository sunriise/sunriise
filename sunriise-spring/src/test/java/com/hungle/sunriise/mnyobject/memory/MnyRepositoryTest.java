package com.hungle.sunriise.mnyobject.memory;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import com.hungle.sunriise.dbutil.TableAccountUtilsTest;
import com.hungle.sunriise.dbutil.TablePayeeUtilsTest;
import com.hungle.sunriise.io.sample.MnySampleFile;
import com.hungle.sunriise.mnyobject.Account;
import com.hungle.sunriise.mnyobject.Category;
import com.hungle.sunriise.mnyobject.Currency;
import com.hungle.sunriise.mnyobject.MnyObjectInterface;
import com.hungle.sunriise.mnyobject.Payee;
import com.hungle.sunriise.mnyobject.Security;
import com.hungle.sunriise.mnyobject.Transaction;

@RunWith(SpringRunner.class)
@Transactional
@SpringBootTest
public class MnyRepositoryTest {
    private static final Logger LOGGER = LogManager.getLogger(MnyRepositoryTest.class);

    @Autowired
    private MnyRepository<Account, Integer> accountRepository;

    @Autowired
    private MnyRepository<Payee, Integer> payeeRepository;

    @Autowired
    private MnyRepository<Category, Integer> categoryRepository;

    @Autowired
    private MnyRepository<Currency, Integer> currencyRepository;

    @Autowired
    private MnyRepository<Security, Integer> securityRepository;

    @Autowired
    private MnyRepository<Transaction, Integer> transactionRepository;

    @Test
    public void testAccountRepository() {
        MnyRepository<Account, Integer> repo = accountRepository;
        int expectedCount = MnySampleFile.SAMPLE_ACCOUNT_COUNT;
        int expectedMin = MnySampleFile.SAMPLE_ACCOUNT_MIN;
        int expectedMax = MnySampleFile.SAMPLE_ACCOUNT_MAX;

        testRepo(repo, expectedCount, expectedMin, expectedMax);

        Iterable<Account> iter = repo.findAll();
        for (Account value : iter) {
//			LOGGER.info("value={}", JSONUtils.writeValueAsString(value));

            if (value.getId() == 42) {
                TableAccountUtilsTest.checkAccount42(value);
            }

            if (value.getId() == 72) {
                TableAccountUtilsTest.checkAccount72(value);
            }
        }
    }

    @Test
    public void testPayeeRepository() {
        MnyRepository<Payee, Integer> repo = payeeRepository;
        int expectedCount = MnySampleFile.SAMPLE_PAYEE_COUNT;
        int expectedMin = MnySampleFile.SAMPLE_PAYEE_MIN;
        int expectedMax = MnySampleFile.SAMPLE_PAYEE_MAX;

        testRepo(repo, expectedCount, expectedMin, expectedMax);

        Iterable<Payee> values = repo.findAll();
        for (Payee value : values) {
            if (value.getId() == 108) {
                TablePayeeUtilsTest.checkPayeeId108(value);
            }
        }
    }

    @Test
    public void testCategoryRepository() {
        MnyRepository<Category, Integer> repo = categoryRepository;
        int expectedCount = MnySampleFile.SAMPLE_CATEGORY_COUNT;
        int expectedMin = MnySampleFile.SAMPLE_CATEGORY_MIN;
        int expectedMax = MnySampleFile.SAMPLE_CATEGORY_MAX;

        testRepo(repo, expectedCount, expectedMin, expectedMax);

//        Iterable<Category> iter = repo.findAll();
//		for (Category value : iter) {
////			LOGGER.info("value={}", JSONUtils.writeValueAsString(value));
//			if (value.getId() == 234) {
//				TableCategoryUtilsTest.checkCategory234(mnyContext, value);
//			}
//
//			if (value.getId() == 239) {
//				TableCategoryUtilsTest.checkCategory239(mnyContext, value);
//			}
//		}
    }

    @Test
    public void testCurrencyRepository() {
        MnyRepository<Currency, Integer> repo = currencyRepository;
        int expectedCount = MnySampleFile.SAMPLE_CURRENCY_COUNT;
        int expectedMin = MnySampleFile.SAMPLE_CURRENCY_MIN;
        int expectedMax = MnySampleFile.SAMPLE_CURRENCY_MAX;

        testRepo(repo, expectedCount, expectedMin, expectedMax);
    }

    @Test
    public void testSecurityRepository() {
        MnyRepository<Security, Integer> repo = securityRepository;
        int expectedCount = MnySampleFile.SAMPLE_SECURITY_COUNT;
        int expectedMin = MnySampleFile.SAMPLE_SECURITY_MIN;
        int expectedMax = MnySampleFile.SAMPLE_SECURITY_MAX;

        testRepo(repo, expectedCount, expectedMin, expectedMax);
    }

    @Test
    public void testTransactionRepository() {
        MnyRepository<Transaction, Integer> repo = transactionRepository;
        int expectedCount = MnySampleFile.SAMPLE_TRANSACTION_COUNT;
        int expectedMin = MnySampleFile.SAMPLE_TRANSACTION_MIN;
        int expectedMax = MnySampleFile.SAMPLE_TRANSACTION_MAX;

        testRepo(repo, expectedCount, expectedMin, expectedMax);
    }

    private <T extends MnyObjectInterface> List<Integer> testFindAll(MnyRepository<T, Integer> repo,
            int expectedCount) {
        Iterable<T> iter = repo.findAll();
        int count = 0;

        List<Integer> ids = new ArrayList<>();
        count = 0;
        for (T item : iter) {
//            try {
//                LOGGER.info("currency={}", JSONUtils.writeValueAsString(item));
//            } catch (Exception e) {
//                LOGGER.error(e.getMessage(), e);
//            }
            Assert.assertNotNull(item);
            Integer id = item.getId();

            Assert.assertTrue(id > 0);

            Assert.assertTrue(repo.existsById(id));

            Optional<T> found = repo.findById(id);
            Assert.assertTrue(found.isPresent());
            Assert.assertEquals(id, found.get().getId());

            ids.add(id);
            count++;
        }
        Assert.assertEquals(expectedCount, count);

        return ids;
    }

    private <T extends MnyObjectInterface> void testRepo(MnyRepository<T, Integer> repo, int expectedCount,
            int expectedMin, int expectedMax) {
        Assert.assertNotNull(repo);

        List<Integer> ids = testFindAll(repo, expectedCount);

        Assert.assertEquals(expectedCount, repo.count());

        testFindAllById(repo, ids);

        testMinMax(repo, expectedMin, expectedMax);
    }

    private <T extends MnyObjectInterface> void testMinMax(MnyRepository<T, Integer> repo, int expectedMin,
            int expectedMax) {
        Iterable<T> iter;
        Integer min = null;
        Integer max = null;
        iter = repo.findAll();
        for (T item : iter) {
            Integer key = item.getId();
//          LOGGER.info("key={}", key);
            if (min == null) {
                min = key;
            } else {
                min = Math.min(min, key);
            }
            if (max == null) {
                max = key;
            } else {
                max = Math.max(max, key);
            }
        }
        Assert.assertEquals(new Integer(expectedMin), min);
        Assert.assertEquals(new Integer(expectedMax), max);
    }

    private <T extends MnyObjectInterface> void testFindAllById(MnyRepository<T, Integer> repo, List<Integer> ids) {
        Iterable<T> iter;
        int count;

        iter = repo.findAllById(ids);
        count = 0;
        for (T item : iter) {
//          LOGGER.info("currency={}", JSONUtils.writeValueAsString(currency));
            Assert.assertNotNull(item);
            count++;
        }
        Assert.assertEquals(ids.size(), count);
    }
}

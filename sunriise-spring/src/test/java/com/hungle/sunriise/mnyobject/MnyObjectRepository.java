package com.hungle.sunriise.mnyobject;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.NoRepositoryBean;

@NoRepositoryBean
public interface MnyObjectRepository<T> extends CrudRepository<T, Integer> {

}

package com.hungle.sunriise.mnyobject.memory;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.dao.DataRetrievalFailureException;

import com.healthmarketscience.jackcess.Database;

public abstract class MnyCacheRepo<T, ID> extends MnyReadOnlyRepo<T, ID> {

    private Map<ID, T> map;

    public MnyCacheRepo(Database db) throws IOException {
        super(db);
        initMap();
    }

    private Map<ID, T> getMap() throws IOException {
        return map;
    }

    protected void setMap(Map<ID, T> map) {
        this.map = map;
    }

    protected abstract void initMap() throws IOException;

    @Override
    public Iterable<T> findAll() {
        Iterable<T> iter = null;
        try {
            Map<ID, T> map = getMap();
            iter = map.values();

        } catch (IOException e) {
            throw new DataRetrievalFailureException(e.getMessage(), e);
        }

        return iter;
    }

    @Override
    public Optional<T> findById(ID id) {
        Optional<T> value = Optional.empty();
        try {
            Map<ID, T> map = getMap();
            value = Optional.ofNullable(map.get(id));
        } catch (IOException e) {
            throw new DataRetrievalFailureException(e.getMessage(), e);
        }
        return value;
    }

    @Override
    public boolean existsById(ID id) {
        // LOGGER.info("> existsById id={}", id);
        T value = null;
        try {
            Map<ID, T> map = getMap();
            value = map.get(id);
        } catch (IOException e) {
            throw new DataRetrievalFailureException(e.getMessage(), e);
        }
        // LOGGER.info("< existsById id={}, value={}", id, value);

        return value != null;
    }

    @Override
    public long count() {
        int count = 0;
        try {
            Map<ID, T> map = getMap();
            count = map.size();
        } catch (IOException e) {
            throw new DataRetrievalFailureException(e.getMessage(), e);
        }
        return count;
    }

    @Override
    public Iterable<T> findAllById(Iterable<ID> ids) {
        List<T> list = new ArrayList<>();
        try {
            Map<ID, T> map = getMap();
            for (ID id : ids) {
                T item = map.get(id);
                if (item != null) {
                    list.add(item);
                }
            }
        } catch (IOException e) {
            throw new DataRetrievalFailureException(e.getMessage(), e);
        }
        return list;
    }

}
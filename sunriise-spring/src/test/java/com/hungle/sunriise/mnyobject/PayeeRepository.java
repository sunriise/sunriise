package com.hungle.sunriise.mnyobject;

import com.hungle.sunriise.mnyobject.impl.DefaultPayee;

public interface PayeeRepository extends MnyObjectRepository<DefaultPayee> {

}

package com.hungle.sunriise.mnyobject.memory;

import java.io.IOException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Component;

import com.healthmarketscience.jackcess.Database;
import com.hungle.sunriise.dbutil.TableCategoryUtils;
import com.hungle.sunriise.mnyobject.Category;

@Component
public class CategoryRepository extends MnyCacheRepo<Category, Integer> {
    private static final Logger LOGGER = LogManager.getLogger(CategoryRepository.class);

    public CategoryRepository(Database db) throws IOException {
        super(db);
    }

    @Override
    protected void initMap() throws IOException {
        setMap(TableCategoryUtils.getMap(getDb()));
    }
}

package com.hungle.sunriise.mnyobject.memory;

import java.io.IOException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Component;

import com.healthmarketscience.jackcess.Database;
import com.hungle.sunriise.dbutil.TablePayeeUtils;
import com.hungle.sunriise.mnyobject.Payee;

@Component
public class PayeeRepository extends MnyCacheRepo<Payee, Integer> {
    private static final Logger LOGGER = LogManager.getLogger(PayeeRepository.class);

    public PayeeRepository(Database db) throws IOException {
        super(db);
    }

    @Override
    protected void initMap() throws IOException {
        setMap(TablePayeeUtils.getMap(getDb()));
    }
}
